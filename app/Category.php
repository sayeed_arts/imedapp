<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{   
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   
    protected $fillable = [
        'id','parent_id','name', 'tooltip', 'display_status','display_order', 'created_at','updated_at',
    ];
    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public function subcategory(){
        return $this->hasMany('App\Category', 'parent_id')->orderBy('name');
    }

    public function subcategoryactive(){
        return $this->hasMany('App\Category', 'parent_id')->where('display_status',1)->orderBy('name');
    }
}
