<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;
use Hash;
use Validator;
use App\Users;
use Carbon\Carbon; 


class AdminController extends Controller
{
    protected $currentUser;
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->currentUser = Auth::user();
            return $next($request);
        });
    }
    public function index()
    {
        $view_data['current_user']      = Auth::user();
        $view_data['hasTable']          = 'yes';
        $view_data['chartTitle']        = 'Orders';
        $view_data['chartTitle']        = 'Orders';
        $view_data['currPage']          = 'dashboard';

        $view_data['receivedOrder']     = DB::table('orders')->where('status','<>',0)->where('shipping_status',0)->count();
        $view_data['processingOrder']   = DB::table('orders')->where('status','<>',0)->where('shipping_status',3)->count();
        $view_data['voidedOrder']       = DB::table('orders')->where('status','<>',0)->where('shipping_status',2)->count();
        $view_data['shippedOrder']      = DB::table('orders')->where('status','<>',0)->where('shipping_status',1)->count();
        $view_data['completedOrder']    = DB::table('orders')->where('status','<>',0)->where('shipping_status',4)->count();


        $view_data['total_sales']       = DB::table('orders')->where('status',2)->sum('total');
        $view_data['total_orders']      = DB::table('orders')->where('status','<>',0)->count();
        $view_data['total_products']    = DB::table('products')->count();
        $view_data['total_users']       = DB::table('users')->where('is_admin',0)->count();
       
// Code by Yogesh Negi

        $view_data['total_vendors']       = DB::table('users')->where('is_admin',2)->where('is_approved',1)->count();
       
        $view_data['total_vendor_products']       = DB::table('products')
        ->select('products.*')
        ->join('users','products.user_id','=','users.id')
        ->where('users.is_admin',2)
        ->count();

          $view_data['total_vendor_orders']       = DB::table('orders')
        ->select('orders.*','users.name as customer_first_name','users.last_name as customer_last_name')
        ->leftjoin('users','orders.customer_id','=','users.id')
        ->leftjoin('order_items','orders.id','=','order_items.order_id')
        ->leftjoin('products','order_items.product_id','=','products.id')
        ->leftjoin('users as vendordetail','products.user_id','=','vendordetail.id')
        ->where('vendordetail.is_admin',2)
        ->where('orders.status','<>',0)
        ->count();

        $view_data['total_vendor_enquiries']       = DB::table('getaquote')
        ->select('getaquote.*')
        ->leftJoin('products','getaquote.product_id', '=', 'products.id')
        ->count();
       
       $view_data['total_vendor_pay_count']       = DB::table('order_items')
        ->leftJoin('orders','order_items.order_id','=','orders.id')
        ->leftJoin('products','order_items.product_id','=','products.id')
        ->leftJoin('users','products.user_id','=','users.id')
        ->where('users.is_admin',2)
        ->sum('order_items.total_price');
       
        $view_data['total_vendor_pending']       = DB::table('users')->where('is_admin',2)->where('is_approved',0)->count();
       

        $view_data['total_vendor_pending_products']       = DB::table('products')
        ->select('products.*')
        ->join('users','products.user_id','=','users.id')
        ->where('users.is_admin',2)
        ->where('products.display_status',0)
        ->count();

// Code by Yogesh Negi       

        $view_data['latest_users']      = DB::select("select * from users where is_admin='0' ORDER BY id DESC LIMIT 10");
        $view_data['latest_orders']     = DB::table('orders')
        ->select('orders.*','users.name as customer_first_name','users.last_name as customer_last_name')
        ->join('users','orders.customer_id','=','users.id')
        ->where('orders.status','<>',0)
        ->orderby('ordered_on','DESC')
        ->take(10)
        ->get();
        return view('admin.dashboard',$view_data);
    }
    public function editProfile()
    {
        $userID = $this->currentUser->id;
        $user   = DB::table('users')->where('id', $userID)->first();
        $view_data['currPage']  = 'editProfile';
        $view_data['user']      = $user;
        return view('admin.editProfile', $view_data);
    }
    public function updateprofile(Request $request)
    {
        $userId = Auth::id();      
        $validator = Validator::make($request->all(), [
          'name'            => 'required',
          'last_name'       => 'required',
          'profile_picture' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',            
          'email'           => 'required|email|unique:users,email,'.$userId,
          'password'        => 'sometimes|nullable|between:5,20',
          'confirm_password'=> 'same:password', 
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/editprofile')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $user = Auth::user();
            $image          = $request->file('profile_picture');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->profile_picture);
                if(strpos($path, 'uploaded_images') !== false){
                    $user->profile_picture   = s3Path().$path;
                    $old_path = $request->input('old_profile_picture');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    }  
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $user->profile_picture   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_profile_picture'));
                }
            }

            if($request->input('password')){
                $user->password = Hash::make($request->input('password')); 
            }
            $user->name     = $request->input('name');
            $user->last_name = $request->input('last_name');
            $user->email    = $request->input('email');
            $user->save();
           return redirect()->back()->withMessage('Your profile has been updated successfully!');
        }
    }
    public function settings()
    {
        $record   = DB::table('settings')->where('id', 1)->first();
        $view_data['currPage']  = 'settings';
        $view_data['record']    = $record;
        return view('admin.settings', $view_data);
    }
    public function updatesettings(Request $request)
    {
        $validator = Validator::make($request->all(), [            
          'admin_email'     => 'required|email'
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/settings')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at                         = currentDBDate();
            $data_array['admin_email']          = $request->input('admin_email');
            $data_array['meta_title']           = $request->input('meta_title');
            $data_array['meta_key']             = $request->input('meta_key');
            $data_array['meta_desc']            = $request->input('meta_desc');
            $data_array['footer_desc']          = $request->input('footer_desc');
            $data_array['fb_status']            = $request->input('fb_status');
            $data_array['fb_link']              = $request->input('fb_link');
            $data_array['tw_status']            = $request->input('tw_status');
            $data_array['tw_link']              = $request->input('tw_link');
            $data_array['in_status']            = $request->input('in_status');
            $data_array['in_link']              = $request->input('in_link');
            $data_array['li_status']            = $request->input('li_status');
            $data_array['li_link']              = $request->input('li_link');
            $data_array['gp_status']            = $request->input('gp_status');
            $data_array['gp_link']              = $request->input('gp_link');
            $data_array['vendor_commission']    = $request->input('vendor_commission');

            /*$data_array['service_fee']          = $request->input('service_fee');
            $data_array['stripe_secret_key']    = $request->input('stripe_secret_key');
            $data_array['stripe_publishable_key']= $request->input('stripe_publishable_key');*/
            $data_array['contact_address']      = $request->input('contact_address');
            $data_array['contact_phone']        = $request->input('contact_phone');
            $data_array['contact_email']        = $request->input('contact_email');
            /*$data_array['sms_api_secret']       = $request->input('sms_api_secret');
            $data_array['background_check']     = $request->input('background_check');*/
            $data_array['amazon_host']          = $request->input('amazon_host');
            $data_array['amazon_username']      = $request->input('amazon_username');
            $data_array['amazon_password']      = $request->input('amazon_password');
            /*$data_array['mailgun_host']         = $request->input('mailgun_host');
            $data_array['mailgun_username']     = $request->input('mailgun_username');
            $data_array['mailgun_password']     = $request->input('mailgun_password');*/
            $data_array['current_mail_provider']= $request->input('current_mail_provider');
            /*$data_array['first_acc_check_fee']  = $request->input('first_acc_check_fee');
            $data_array['second_acc_check_fee'] = $request->input('second_acc_check_fee');
            $data_array['mvr_acc_check_fee']    = $request->input('mvr_acc_check_fee');*/
            $data_array['from_email']           = $request->input('from_email');
            $data_array['product_name']         = $request->input('product_name');
            $data_array['mail_signature_name']  = $request->input('mail_signature_name');
            $data_array['mail_sender_name']     = $request->input('mail_sender_name');
            $data_array['tax']                  = $request->input('tax');
            $data_array['shipping_type']        = $request->input('shipping_type');
            $data_array['flat_rate_fee']        = $request->input('flat_rate_fee');

            $data_array['updated_at']           = $created_at;
            
            DB::table('settings')
            ->where('id', 1)
            ->update($data_array);
            return redirect()->back()->withMessage('Settings has been updated successfully!');
        }
    }

    public function reports(Request $request)
    {        
        $view_data['currPage']      = 'reports';
        $start_date     = $request->input('start_date');
        $end_date       = $request->input('end_date');
        $export_type    = $request->input('export_type');
        $export         = $request->input('export');
        $export_order_status                = $request->input('export_order_status');
        $view_data['start_date']            = $start_date;
        $view_data['end_date']              = $end_date;
        $view_data['export_type']           = $export_type;
        $view_data['export_order_status']   = $export_order_status;
       
        if(!empty($start_date) && !empty($end_date)){
            /*$from_date_array= explode('-', $start_date);
            $to_date_array  = explode('-', $end_date);
            $from_date      = $from_date_array[2].'-'.$from_date_array[1].'-'.$from_date_array[0];
            $to_date        = $to_date_array[2].'-'.$to_date_array[1].'-'.$to_date_array[0];*/
            $headers = array(
                "Content-type" => "text/csv",
                "Content-Disposition" => "attachment; filename=file.csv",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
            );
            if($export_type=='1'){
                //Export All Users
                $reviews = DB::table('users')
                ->where(array('is_admin'=>0))
                ->where('created_at','>=',$start_date)
                ->where('created_at','<=',$end_date)
                ->orderby('id','DESC')
                ->get(); 
                if($export=='Search'){
                    $view_data['hasTable']      = 'yes';
                    $view_data['allRecords']    = $reviews;
                    return view('admin.reportallusers', $view_data);
                }else{
                    $columns = array('Name', 'Email', 'Phone', 'Email Verified', 'Status', 'Created');

                    $filename = "AllUsers-".date('Y-m-d').".csv";
                    $file = fopen($filename, 'w+');
                    fputcsv($file, $columns);

                    foreach($reviews as $review) {
                        if($review->verified==1){
                            $verified="Yes";
                        }else{
                            $verified="No";
                        }
                        if($review->is_approved==1){
                            $is_approved="Active";
                        }else{
                            $is_approved="InActive";
                        }
                        fputcsv($file, array($review->name.' '.$review->last_name, $review->email, $review->phone_number,$verified, $is_approved, $review->created_at));
                    }
                    fclose($file); 
                }                  
            }else if($export_type==2){
                //Export All PO Orders
                $whereArray = array('orders.payment_option'=>1);
                if($export_order_status!=''){
                    if($export_order_status==-1){
                        $export_order_status = 0;
                    }
                    $whereArray['orders.shipping_status'] = $export_order_status;
                }
                $reviews = DB::table('orders')
                ->select('orders.*','users.name as customer_first_name','users.last_name as customer_last_name')
                ->join('users','orders.customer_id','=','users.id')
                ->where('orders.status','<>',0)
                ->where('orders.ordered_on','>=',$start_date)
                ->where('orders.ordered_on','<=',$end_date)
                ->where($whereArray)
                ->orderby('orders.id','DESC')
                ->get(); 
                if($export=='Search'){
                    $view_data['hasTable']      = 'yes';
                    $view_data['allRecords']    = $reviews;
                    return view('admin.reportallPOOrders', $view_data);
                }else{
                    $columns = array('Order No', 'Customer Purchase Order Number', 'Wire Bank information', 'Name', 'Email', 'Phone', 'Status', 'Sub Total', 'Tax', 'Discount', 'Shipping', 'Total', 'Date');

                    $filename = "AllPOOrders-".date('Y-m-d').".csv";
                    $file = fopen($filename, 'w+');
                    fputcsv($file, $columns);
                    foreach($reviews as $review) {                        
                        if($review->shipping_status==0){
                            $is_approved="Received";
                        }elseif($review->shipping_status==1){
                            $is_approved="Shipped";
                        }elseif($review->shipping_status==2){
                            $is_approved="Voided";
                        }elseif($review->shipping_status==3){
                            $is_approved="Processing";
                        }elseif($review->shipping_status==4){
                            $is_approved="Completed";
                        }
                        fputcsv($file, array('PRO-IMED-'.$review->id,$review->po_order_number,$review->po_order_qty,$review->customer_first_name.' '.$review->customer_last_name, $review->email_address, $review->phone,$is_approved, showAdminPrice($review->subtotal),showAdminPrice($review->tax), showAdminPrice($review->coupon_discount),showAdminPrice($review->shipping),showAdminPrice($review->total), $review->ordered_on));
                    }
                    fclose($file); 
                }          
            }else if($export_type==3){
                //Export All Sales Orders
                $whereArray = array('orders.payment_option'=>2);
                if($export_order_status!=''){
                    if($export_order_status==-1){
                        $export_order_status = 0;
                    }
                    $whereArray['orders.shipping_status'] = $export_order_status;
                }
                $reviews = DB::table('orders')
                ->select('orders.*','users.name as customer_first_name','users.last_name as customer_last_name')
                ->join('users','orders.customer_id','=','users.id')
                ->where('orders.status','<>',0)
                ->where('orders.ordered_on','>=',$start_date)
                ->where('orders.ordered_on','<=',$end_date)
                ->where($whereArray)
                ->orderby('orders.id','DESC')
                ->get(); 
                if($export=='Search'){
                    $view_data['hasTable']      = 'yes';
                    $view_data['allRecords']    = $reviews;
                    return view('admin.reportallSalesOrders', $view_data);
                }else{
                    $columns = array('Order No','Transaction Id', 'Name', 'Email', 'Phone', 'Status', 'Sub Total', 'Tax', 'Discount', 'Shipping', 'Total', 'Date');

                    $filename = "AllSalesOrders-".date('Y-m-d').".csv";
                    $file = fopen($filename, 'w+');
                    fputcsv($file, $columns);
                    foreach($reviews as $review) {                        
                        if($review->shipping_status==0){
                            $is_approved="Received";
                        }elseif($review->shipping_status==1){
                            $is_approved="Shipped";
                        }elseif($review->shipping_status==2){
                            $is_approved="Voided";
                        }elseif($review->shipping_status==3){
                            $is_approved="Processing";
                        }elseif($review->shipping_status==4){
                            $is_approved="Completed";
                        }
                        fputcsv($file, array('PRO-IMED-'.$review->id,$review->transaction_id,$review->customer_first_name.' '.$review->customer_last_name, $review->email_address, $review->phone,$is_approved, showAdminPrice($review->subtotal),showAdminPrice($review->tax), showAdminPrice($review->coupon_discount),showAdminPrice($review->shipping),showAdminPrice($review->total), $review->ordered_on));
                    }
                    fclose($file); 
                }            
            }
            return Response::download($filename, $filename, $headers);
        }
        return view('admin.reports', $view_data);
    }


   public function vendorreview(){
      $allRecords = DB::table('vendor_ratings')
        ->leftjoin('users', 'vendor_ratings.user_id', '=', 'users.id')
        ->leftjoin('users as vendor_detail', 'vendor_ratings.vendor_id', '=', 'vendor_detail.id')
        ->select('vendor_ratings.*', 'users.name','users.last_name','vendor_detail.Store')
        ->groupBy('vendor_ratings.id')
        ->orderby('vendor_ratings.id', 'DESC')

        ->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allvendorreviews';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allvendorreviews', $view_data);
   }
   
   public function productreview(){
       $allRecords = DB::table('product_ratings')
        ->join('users', 'product_ratings.user_id', '=', 'users.id')
        ->join('products', 'product_ratings.product_id', '=', 'products.id')
        ->select('product_ratings.*', 'users.name','users.last_name','products.name as product_name')
        ->orderby('product_ratings.id', 'DESC')
        ->get();

        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allproductreviews';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allproductreviews', $view_data);
   }

  public function updateprodreviewstatus(){
        if(!empty($_POST)){
                 DB::table('product_ratings')->where('id', $_POST['auction_id'])->update(array('status'=>$_POST['status'])); 
                 echo "1";
                  die;
              }
              else{
                echo "0";
              }
  }
  
  public function updatevenreviewstatus(){
       if(!empty($_POST)){
             DB::table('vendor_ratings')->where('id', $_POST['auction_id'])->update(array('status'=>$_POST['status'])); 
             echo "1";
              die;
          }
          else{
            echo "0";
          } 
  }

}
