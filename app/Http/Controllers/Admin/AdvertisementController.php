<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;

use Hash;
use Validator;

class AdvertisementController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {  
        $allRecords = DB::table('advertisement')->orderby('display_order', 'ASC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'alladvertisement';
        $view_data['allRecords']    = $allRecords;
        return view('admin.alladvertisements', $view_data);
    }

    public function add()
    {        
        $view_data['currPage']      = 'alladvertisement';
        $view_data['hasForm']       = 'yes';
        return view('admin.addadvertisement',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'company_name'      => 'required',
            'name'              => 'required',
            'image'             => 'required | mimes:jpeg,png,jpg,gif | max:2048',
            'location'          => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/advertisements/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                }else{
                    $image_path         = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath    = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;
                }
            }
            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['location']         = $request->input('location');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            
            DB::table('advertisement')
            ->insert($data_array);

            return redirect('admin/advertisements/')->withMessage('Advertisement has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('advertisement')->where('id', $id)->first();        
        if ($record != null){
            $view_data['currPage']      = 'alladvertisement';
            $view_data['hasForm']       = 'yes';
            $view_data['record']         = $record;
            return view('admin.editadvertisement', $view_data);
        }else{
            return redirect('admin/advertisements'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            //'company_name'      => 'required',
            'name'              => 'required',
            'image'             => 'nullable|mimes:jpeg,png,jpg,gif|max:2048',
            'location'          => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/advertisements/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                    $old_path = $request->input('old_image');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    }  
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_image'));
                }
            }

            //$data_array['company_name']     = $request->input('company_name');
            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['location']         = $request->input('location');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('advertisement')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/advertisements/')->withMessage('Advertisement has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('advertisement')->where('id', $id)->first();        
        if ($record != null){
            DB::table('advertisement')->where('id', $id)->delete();
            $old_path = $record->image;
            if(!empty($old_path)){
                $old_path = str_replace(s3Path(), '', $old_path);
                Storage::disk('s3')->delete($old_path);
                @unlink($old_path);
            } 
            return redirect('admin/advertisements/')->withMessage('Advertisement has been deleted successfully.');
        }else{
            return redirect('admin/advertisements'); 
        } 
    }    
}