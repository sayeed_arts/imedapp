<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;
use App\Users;
class AgentsController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->currentUser = Auth::user();
            return $next($request);
        });   
    }
    public function index()
    {   
        $userID = $this->currentUser->id;
        $allUsers = DB::select("select * from users where is_admin='1' AND id!='1' AND id!='".$userID."' ORDER BY id DESC");
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allagents';
        $view_data['allRecords']    = $allUsers;
        return view('admin.allagents', $view_data);
    }
    public function add()
    {        
        $view_data['hasForm']   = 'yes';
        $view_data['currPage']  = 'allagents';
        return view('admin.addagent',$view_data);
    }
    public function save(Request $request)
    {    
        $validator = Validator::make($request->all(), [
            'name'      => 'required|max:50',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required|between:5,20',
            'confirm_password'=> 'required|between:5,20|same:password',          
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/agents/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $hashpass           = Hash::make($request->input('password')); 
            $created_at         = currentDBDate();
            $allowed_sections   = '';
            if(isset($_POST['allowed_sections']) && count($_POST['allowed_sections'])>0){
                $allowed_sections = implode(',', $_POST['allowed_sections']);
            }
            $insert_array = array(
                'name'=>$request->input('name'),
                'last_name'=>$request->input('last_name'),
                'email'=>$request->input('email'),
                'verified'=>$request->input('verified'),
                'is_admin'=>1,
                'allowed_sections'=>$allowed_sections,                
                'is_approved' => $request->input('is_approved'), 
                'password'=>$hashpass,
                'created_at'=>$created_at,
                'updated_at'=>$created_at
                );
            DB::table('users')->insert($insert_array);

            return redirect('admin/agents/')->withMessage('Users details has been added successfully.');
        }
    }
    public function edit($id)
    {
        $userID = $this->currentUser->id;
        $user   = DB::table('users')
        ->where('id', $id)
        ->where('id', '<>',1)
        ->where('id', '<>',$userID)
        ->first();        
        if ($user != null){
            $view_data['hasForm']       = 'yes';
            $view_data['currPage']      = 'allagents';
            $view_data['record']        = $user;
            return view('admin.editagent', $view_data);
        }else{
            return redirect('admin/agents'); 
        } 
    }
    public function update(Request $request)
    {   
        $usrid      = $request->input('currid');
        $required_array = array(
            'name'      => 'required|max:50',
            'email'     => 'required|email|unique:users,email,'.$usrid,
            'password'        => 'sometimes|nullable|between:5,20',
            'confirm_password'=> 'same:password',         
        );
        $validator = Validator::make($request->all(), $required_array);
        if ($validator->fails()) {
            return redirect('admin/agents/edit/'.$usrid)
                        ->withErrors($validator)
                        ->withInput();
        }else{           
            $updated_at         = currentDBDate();
            $allowed_sections   = '';
            if(isset($_POST['allowed_sections']) && count($_POST['allowed_sections'])>0){
                $allowed_sections = implode(',', $_POST['allowed_sections']);
            }
            $data_array = array(
                'name'         => $request->input('name'),
                'last_name'    => $request->input('last_name'),
                'email'        => $request->input('email'),
                'verified'     => $request->input('verified'),                 
                'is_approved' => $request->input('is_approved'),                
                'updated_at'   => $updated_at,
                'allowed_sections'  => $allowed_sections
            );
            if($request->input('password')){
                $data_array['password'] = Hash::make($request->input('password')); 
            }
            
            $userID = $this->currentUser->id;

            DB::table('users')
                ->where('id', $usrid)
                ->where('id', '<>',1)
                ->where('id', '<>',$userID)
                ->update($data_array);
            return redirect('admin/agents/')->withMessage('User details has been updated successfully.');
        }
    }
   
    public function delete($id)
    {
        $userID = $this->currentUser->id;
        $users   = DB::table('users')
        ->where('id', $id)        
        ->where('id', '<>',1)
        ->where('id', '<>',$userID)
        ->first();        
        if ($users != null){
            @unlink($users->profile_picture);
            DB::table('users')
            ->where('id', $id)
            ->where('id', '<>',1)
            ->where('id', '<>',$userID)
            ->delete();
            return redirect('admin/agents/')->withMessage('User has been deleted successfully.');
        }else{
            return redirect('admin/agents'); 
        } 
    } 
}
