<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Hash;
use Validator;

class AttributesController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {   
        $searchTitle = isset($_REQUEST['search'])?$_REQUEST['search']:'';
        $perpage=20;
        $whereArray = array();
        if(!empty($searchTitle)){
            $whereArray[]=array( function ($query) use ($searchTitle) {
                $query->where('attributes.name', 'like', '%'.$searchTitle.'%');
            });
        }

        $allRecords = DB::table('attributes')
        ->select('attributes.*','attribute_sets.name as attributeSetsName')
        ->join('attribute_sets','attributes.set_id','=','attribute_sets.id')
        ->where($whereArray) 
        ->orderby('attributes.display_order', 'ASC')
        ->paginate($perpage);

        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allattributes';
        $view_data['allRecords']    = $allRecords;
        $view_data['searchTitle']   = $searchTitle;
        return view('admin.allattributes', $view_data);
    }

    public function add()
    {   
        //$allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
        $allRecords     = DB::table('attribute_sets')->orderby('display_order', 'ASC')->get();
        $view_data['currPage']      = 'allattributes';
        $view_data['allRecords']    = $allRecords;
        $view_data['hasForm']       = 'yes';
        //$view_data['allLanguages']      = $allLanguages;
        return view('admin.addattributes',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',            
            'set_id'            => 'required',            
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/attributes/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
              

            $data_array['slug']             = create_slug($request->input('name'),'','attributes','slug');           
            $data_array['set_id']           = $request->input('set_id');
            $data_array['name']             = $request->input('name');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            $data_array['values']           = '';
            
            $currid = DB::table('attributes')->insertGetId($data_array);
            if(!empty($request->input('values'))){
                $bulletArray = array();
                foreach ($request->input('values') as $key => $value) {
                    if(!empty($value)){
                        $data_value_array['set_id']           = $request->input('set_id');
                        $data_value_array['attribute_id']     = $currid;
                        $data_value_array['name']             = $value;
                        $data_value_array['display_order']    = $key;
                        $data_value_array['display_status']   = 1;
                        $data_value_array['created_at']       = $created_at;
                        $data_value_array['updated_at']       = $created_at;
                        DB::table('attribute_values')->insertGetId($data_value_array);
                    }
                }
            }

            /*if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name        = $request->input('name_'.$lang);
                    $bulletArray = array();
                    if(!empty($request->input('values_'.$lang))){                        
                        foreach ($request->input('values_'.$lang) as $value) {
                            if(!empty($value)){
                                $bulletArray[]=$value;
                            }
                        }                        
                    }
                    if(!empty($bulletArray))
                        $values = @implode('##~~##', $bulletArray);
                    else
                        $values = '';

                    if(!empty($name) || !empty($values)){
                        DB::table('attributes_translations')
                                ->insert(['attribute_id'=>$currid,'language'=>$lang,'name'=>$name,'values'=>$values,'created_at'=>$created_at,'updated_at'=>$created_at]);
                    }
                }
            }*/
            return redirect('admin/attributes/')->withMessage('Attribute has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('attributes')->where('id', $id)->first();        
        if ($record != null){
            $allRecords = DB::table('attribute_sets')->orderby('display_order', 'ASC')->get();
            /*$allOtherSlide  = DB::table('attributes_translations')->where('attribute_id', $id)->get();   
            $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
            $allOtherLang   = array();
            if(!empty($allOtherSlide)){
                foreach ($allOtherSlide as $singleLang) {
                    $allOtherLang[$singleLang->language]['name'] = $singleLang->name;
                    $allOtherLang[$singleLang->language]['values'] = $singleLang->values;
                }
            }*/

            $allRecordValues = DB::table('attribute_values')->where('attribute_id',$id)->orderby('id', 'ASC')->get();
            $view_data['currPage']      = 'allattributes';
            $view_data['hasForm']       = 'yes';
            $view_data['record']        = $record;
            $view_data['allRecordValues']= $allRecordValues;
            $view_data['allRecords']    = $allRecords;
            /*$view_data['allLanguages']  = $allLanguages;
            $view_data['allOtherLang']  = $allOtherLang;*/
            return view('admin.editattributes', $view_data);
        }else{
            return redirect('admin/attributes'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'set_id'            => 'required',
            'name'              => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $data_array['slug']             = create_slug($request->input('name'),$currid,'attributes','slug'); 
           /* if(!empty($request->input('values'))){
                $bulletArray = array();
                foreach ($request->input('values') as $value) {
                    if(!empty($value)){
                        $bulletArray[]=$value;
                    }
                }
                $data_array['values']         = @implode('##~~##', $bulletArray);
            }   */    
            $data_array['set_id']           = $request->input('set_id');
            $data_array['name']             = $request->input('name');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('attributes')
            ->where('id', $currid)
            ->update($data_array);

            DB::table('attribute_values')
            ->where('attribute_id', $currid)
            ->delete();

             if(!empty($request->input('values'))){
                $bulletArray = array();
                foreach ($request->input('values') as $key => $value) {
                    if(!empty($value)){
                        $data_value_array['set_id']           = $request->input('set_id');
                        $data_value_array['attribute_id']     = $currid;
                        $data_value_array['name']             = $value;
                        $data_value_array['display_order']    = $key;
                        $data_value_array['display_status']   = 1;
                        $data_value_array['created_at']       = $created_at;
                        $data_value_array['updated_at']       = $created_at;
                        DB::table('attribute_values')->insertGetId($data_value_array);
                    }
                }
            }

            /*if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $bulletArray = array();
                    if(!empty($request->input('values_'.$lang))){                        
                        foreach ($request->input('values_'.$lang) as $value) {
                            if(!empty($value)){
                                $bulletArray[]=$value;
                            }
                        }                        
                    }
                    if(!empty($bulletArray))
                        $values         = @implode('##~~##', $bulletArray);
                    else
                        $values = '';

                    if(!empty($name) || !empty($values)){
                        $getLangData = DB::table('attributes_translations')->where('attribute_id',$currid)->where('language',$lang)->first();
                        if(!empty($getLangData)){
                            DB::table('attributes_translations')
                                ->where('attribute_id',$currid)->where('language',$lang)
                                ->update(['name'=>$name,'values'=>$values,'updated_at'=>$created_at]);
                        }else{
                            DB::table('attributes_translations')
                                ->insert(['attribute_id'=>$currid,'language'=>$lang,'name'=>$name,'values'=>$values,'created_at'=>$created_at,'updated_at'=>$created_at]);
                        }
                    }else{
                        DB::table('attributes_translations')->where('attribute_id',$currid)->where('language',$lang)->delete();
                    }
                }
            }*/
            return redirect('admin/attributes/')->withMessage('Attribute has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('attributes')->where('id', $id)->first();        
        if ($record != null){
            DB::table('attributes')->where('id', $id)->delete();            
            //DB::table('attributes_translations')->where('attribute_id', $id)->delete();
            return redirect('admin/attributes/')->withMessage('Attribute has been deleted successfully.');
        }else{
            return redirect('admin/attributes'); 
        } 
    }    
}