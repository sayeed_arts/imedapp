<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;
use App\Users;
use App\VerifyUser;
use App\Category;

class AuctionsController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {
        $allauctions = DB::select("
        SELECT A.*, B.name, C.is_admin, (SELECT MAX(status) FROM auction_bids WHERE auction_id = A.id ) AS auction_awarded
        FROM auctions A 
        LEFT JOIN products B on A.product_id = B.id 
        LEFT JOIN users C on B.user_id = C.id 
        ORDER BY id DESC
        ");
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allauctions';
        $view_data['allRecords']    = $allauctions;
        return view('admin.allauctions', $view_data);
    }
    public function add($id="")
    {     

        $view_data['allCategory']    = Category::where('parent_id',0)->orderby('display_order','ASC')->get(); 
        $view_data['hasForm']       = 'yes';
        $view_data['currPage']  = 'addauction';
       if(!empty($id)){
        $view_data['auctiondetail'] = DB::table('auctions')->where('id', $id)->first();
        $view_data['faqlist'] = DB::table('auction_faq')->where('auction_id', $id)->get();
        $view_data['allProduct'] = DB::table('products')->where('category_id','like','%'.$view_data['auctiondetail']->category_id.'%')->pluck( 'name','id');


        // echo "<pre>";
        // print_r($view_data['faqlist']);
        // die;
        }
        return view('admin.addauction',$view_data);
    }

    public function save(Request $request)
    {    
    // echo "<pre>";
    // print_r($_POST);
    // die;
     if(!empty($request->input('vendor_id'))){
        $validator = Validator::make($request->all(), [
          'category_id'    => 'required',
          'product_id'    => 'required',
          'short_desc'    => 'required',
          'end_date'    => 'required',
          'end_time'    => 'required',
          'start_bid_price'    => 'required',
          'min_bid_price'    => 'required',
          'description'    => 'required',
          'conditions'    => 'required',
          'terms_and_conditions'    => 'required',
          'featured'    => 'required'
        ]);
        }
        else{
            $validator = Validator::make($request->all(), [
          'category_id'    => 'required',
          'product_id'    => 'required',
          'short_desc'    => 'required',
          'end_date'    => 'required',
          'end_time'    => 'required',
          'start_bid_price'    => 'required',
          'min_bid_price'    => 'required',
          'description'    => 'required',
          'conditions'    => 'required',
          'terms_and_conditions'    => 'required',
          'featured'    => 'required'
        ]);
            
        }
        if ($validator->fails()) {
                 if(empty($request->input('vendor_id'))){
                 
                        return redirect('admin/auctions/add')
                                    ->withErrors($validator)
                                    ->withInput();
                 }
                 else{
                        return redirect('admin/auctions/add/'.$request->input('vendor_id'))
                                    ->withErrors($validator)
                                    ->withInput();  
                 }
        }else{
            $created_at     = currentDBDate();
            $insert_array = array(
                'category_id'=>$request->input('category_id'),
                'product_id'=> $request->input('product_id'),
                'short_desc'=>$request->input('short_desc'),
                'end_date'=>date('Y-m-d',strtotime($request->input('end_date'))),
                'end_time'=>$request->input('end_time'),
                'start_bid_price'=>$request->input('start_bid_price'),
                'min_bid_price'=>$request->input('min_bid_price'),
                'description'=>$request->input('description'),
                'conditions'=>$request->input('conditions'),
                'terms_and_conditions'=>$request->input('terms_and_conditions'),
                'featured'=>$request->input('featured'),
                'created_at'=>$created_at,
                'created_by'=>Auth::user()->id
                );
           if(!empty($request->input('auctionid'))){
               DB::table('auctions')->where('id', $request->input('auctionid'))
              ->update($insert_array);
              
              DB::table('auction_faq')->where('auction_id', $request->input('auctionid'))->delete();

              if(!empty($request->input('auctionid')) && !empty($request->input('faq_ques') ) ){
                for($i=0;$i<count($request->input('faq_ques'));$i++){
                    if(!empty($_POST['faq_ques'][$i])){
                      $insertfaq = array();
                      $insertfaq['auction_id'] = $request->input('auctionid');
                      $insertfaq['faq_ques'] = $_POST['faq_ques'][$i];
                      $insertfaq['faq_ans'] = $_POST['faq_ans'][$i];
                      $insertfaq['created_at'] = $created_at;
                      $insertfaq['created_by'] = Auth::user()->id;
                      DB::table('auction_faq')->insertGetId($insertfaq);
                      
                    }
                }
            }


              return redirect('admin/auctions/')->withMessage('Auction details has been updated successfully.');
           }
           else{
            $lastid = DB::table('auctions')->insertGetId($insert_array);
            if(!empty($lastid) && !empty($request->input('faq_ques') ) ){
                for($i=0;$i<count($request->input('faq_ques'));$i++){
                    $insertfaq = array();
                    $insertfaq['auction_id'] = $lastid;
                    $insertfaq['faq_ques'] = $_POST['faq_ques'][$i];
                    $insertfaq['faq_ans'] = $_POST['faq_ans'][$i];
                    $insertfaq['created_at'] = $created_at;
                    $insertfaq['created_by'] = Auth::user()->id;
                    DB::table('auction_faq')->insertGetId($insertfaq);
                }
            }

            return redirect('admin/auctions/')->withMessage('Auction details has been added successfully.');
            
           }

        }
    }

    public function viewbids($id)
    {
        $allbids = DB::select("
        SELECT A.id, A.auction_id, A.user_id, FORMAT(A.bid_amount,2) AS bid_amount, A.created_date, A.status, A.order_status, B.name 
        FROM auction_bids A 
        LEFT JOIN users B on A.user_id = B.id 
        WHERE auction_id = '".$id."' 
        ORDER BY id DESC
        ");

        $winningbid = DB::select("SELECT id FROM auction_bids WHERE auction_id = '".$id."' AND status = '1' ");
        $auction_awarded = false;
        if($winningbid){
            $auction_awarded = true;
        }
        $view_data['auction_awarded']      = $auction_awarded;
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allbids';
        $view_data['allRecords']    = $allbids;
        return view('admin.allbids', $view_data);
    }

    public function award($id)
    {
        $winningbid = DB::select("
        SELECT A.auction_id, A.user_id, FORMAT(A.bid_amount,2) AS bid_amount, A.created_date, B.name, B.email 
        FROM auction_bids A 
        LEFT JOIN users B on A.user_id = B.id 
        WHERE A.id = '".$id."'
        ");
        $auction_id = $winningbid[0]->auction_id;
       
        $data_array['status']  = '1';
            DB::table('auction_bids')
            ->where('id', $id)
            ->where('auction_id', $auction_id)
            ->update($data_array);
            
        return redirect('admin/auctions/viewbids/'.$auction_id)->withMessage('Bid has been awarded successfully.');
    }

    public function winners()
    {
        $allwinners = DB::select("
        SELECT A.id, A.auction_id, A.user_id, FORMAT(A.bid_amount,2) AS bid_amount, A.created_date, A.status, A.order_status, B.name, C.product_id, D.name AS product_name   
        FROM auction_bids A 
        LEFT JOIN users B on A.user_id = B.id 
        LEFT JOIN auctions C on A.auction_id = C.id 
        LEFT JOIN products D on C.product_id = D.id 
        WHERE A.status = '1' 
        ORDER BY id DESC
        ");
        
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allwinners';
        $view_data['allRecords']    = $allwinners;
        return view('admin.allwinners', $view_data);
    }

    public function edit($id)
    {
        $user   = DB::table('users')->where('id', $id)->first();        
        if ($user != null){
            $view_data['hasForm']       = 'yes';
            $view_data['currPage']      = 'editUser';
            $view_data['record']        = $user;
            return view('admin.edituser', $view_data);
        }else{
            return redirect('admin/users'); 
        } 
    }
    public function update(Request $request)
    {   
        $usrid      = $request->input('currid');
        $required_array = array(            
            'name'      => 'required|max:50',
            'last_name' => 'required|max:50',
            'email'     => 'required|email|unique:users,email,'.$usrid,            
        );
        $validator = Validator::make($request->all(), $required_array);
        if ($validator->fails()) {
            return redirect('admin/users/edit/'.$usrid)
                        ->withErrors($validator)
                        ->withInput();
        }else{           
            $updated_at         = currentDBDate();
            $old_is_approved    = $request->input('old_is_approved');
            $is_approved        = $request->input('is_approved');
            $email              = $request->input('email');

            $data_array = array(
                'name'          => $request->input('name'),
                'last_name'     => $request->input('last_name'),
                'email'         => $request->input('email'),
                'verified'      => $request->input('verified'),
                'phone_number'  => $request->input('phone_number'),                
                'is_approved'   => $request->input('is_approved'),                
                'updated_at'    => $updated_at
            );
            DB::table('users')
                ->where('id', $usrid)
                ->update($data_array);

            if($old_is_approved==0 && $is_approved==1){
                $user = DB::table('users')->find($usrid);               
                $verifyUser = DB::table('verify_users')->where('user_id', $usrid)->first();
                $userdata['name']               = $user->name.' '.$user->last_name;
                $userdata['verifyUsertoken']    = $verifyUser->token;                    
                $subject        = 'Activate Your Account';
                $template       = 'emails.verifyUser';
                
                send_smtp_email($email,$subject,$userdata,$template);

                $welcomeSubject        = 'Welcome to Imedical Shop!';
                $welcomeTemplate       = 'emails.welcome';
                //send_smtp_email($email,$welcomeSubject,$userdata,$welcomeTemplate);
            }
            return redirect('admin/users/')->withMessage('User details has been updated successfully.');
        }
    }
   
    public function delete($id)
    {
        $users   = DB::table('users')->where('id', $id)->first();        
        if ($users != null){
            DB::table('users')->where('id', $id)->delete();
            return redirect('admin/users/')->withMessage('User has been deleted successfully.');
        }else{
            return redirect('admin/users'); 
        } 
    }
    public function getstate(){
        if(!empty($_POST)){
             $statelist = DB::table('states')->pluck( 'stateName','stateID');
             $html="";
             $html.='<select name="state" id="state" class="form-control fetch_cities" tabindex="11">
                   <option value="">Select State</option>';
                foreach($statelist as $key=>$value):
                    $html.='<option value="'.$key.'">"'.$value.'"</option>';
                endforeach;                  
                }
                $html.='</select>';
                echo $html;
                die;
    } 

    public function getproduct($id){
  if(!empty($id)){
             $statelist = DB::table('products')->where('category_id','like','%'.$id.'%')->pluck( 'name','id');
             $html="";
             $html.='<select name="product_id" id="product_id_auc" class="form-control fetch_product_auc" tabindex="11">
                   <option value="">Select Product</option>';
                foreach($statelist as $key=>$value):
                    $html.='<option value="'.$key.'">"'.$value.'"</option>';
                endforeach;                  
                }
                $html.='</select>';
                echo $html;
                die;

    }

    public function updatestatus(){
      if(!empty($_POST)){
         DB::table('auctions')->where('id', $_POST['auction_id'])->update(array('status'=>$_POST['status'])); 
         echo "1";
          die;
      }
      else{
        echo "0";
      }
    }

    public function updatebidorderstatus(){
        if(!empty($_POST)){
           DB::table('auction_bids')->where('id', $_POST['bidid'])->update(array('order_status'=>$_POST['order_status'])); 
        }
        return redirect('admin/auction/winners')->withMessage('Status has been updated successfully.');
      }

  }
