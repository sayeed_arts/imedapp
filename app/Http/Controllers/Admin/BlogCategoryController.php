<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;

class BlogCategoryController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allRecords = DB::table('blog_categories')->orderby('display_order', 'ASC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allblogcategories';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allblogcategories', $view_data);
    }

    public function add()
    {        
        $view_data['currPage']      = 'allblogcategories';
        $view_data['hasForm']       = 'yes';
        return view('admin.addblogcategory',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/blogcategories/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $data_array['slug']             = create_slug($request->input('name'),'','blog_categories','slug');
            $data_array['name']             = $request->input('name');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            
            DB::table('blog_categories')
            ->insert($data_array);

            return redirect('admin/blogcategories/')->withMessage('Blog Category has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('blog_categories')->where('id', $id)->first();        
        if ($record != null){
            $view_data['currPage']      = 'allblogcategories';
            $view_data['hasForm']       = 'yes';
            $view_data['record']         = $record;
            return view('admin.editblogcategory', $view_data);
        }else{
            return redirect('admin/blogcategories'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/blogcategories/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $data_array['slug']             = create_slug($request->input('name'),$currid,'blog_categories','slug');
            $data_array['name']             = $request->input('name');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('blog_categories')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/blogcategories/')->withMessage('Blog category has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('blog_categories')->where('id', $id)->first();        
        if ($record != null){
            DB::table('blog_categories')->where('id', $id)->delete();
            return redirect('admin/blogcategories/')->withMessage('Blog category has been deleted successfully.');
        }else{
            return redirect('admin/blogcategories'); 
        } 
    }    
}