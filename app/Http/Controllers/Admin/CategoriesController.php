<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Hash;
use Validator;
use App\Category;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');      
    }
    public function index()
    {       
        $allRecords = Category::where('parent_id',0)->orderby('display_order','ASC')->get();        
        $view_data['hasTable']          = 'yes';
        $view_data['currPage']          = 'allcategories';
        $view_data['allRecords']        = $allRecords;
        return view('admin.allcategories', $view_data);
    }
    public function add()
    {
        $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
        //$allRecords   = DB::select('select * from categories WHERE parent_id=0 ORDER BY display_order ASC');
        $allRecords     = Category::where('parent_id',0)->orderby('display_order','ASC')->get();   
        $view_data['hasForm']           = 'yes';
        $view_data['currPage']          = 'addCategory';
        $view_data['allRecords']        = $allRecords;
        $view_data['allLanguages']      = $allLanguages;
        return view('admin.addcategory',$view_data);
    }
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' 			=> 'required',
            'image'         => 'required|mimes:jpeg,png,jpg,gif|max:2048',
            //'display_order' => 'nullable|numeric|min:1',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/categories/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;
                }
            }            
            $data_array['slug']     = create_slug($request->input('name'),'','categories','slug');
            $data_array['name']          = $request->input('name');
            $data_array['parent_id']     = $request->input('parent_id');
            $data_array['content']       = $request->input('content');
            $data_array['meta_title']    = $request->input('meta_title');
            $data_array['meta_key']      = $request->input('meta_key');
            $data_array['meta_desc']     = $request->input('meta_desc');
            $data_array['display_order'] = $request->input('display_order');
            $data_array['display_status']= $request->input('display_status');
            $data_array['created_at']    = $created_at;
            $data_array['updated_at']    = $created_at;
            
            $currid = DB::table('categories')->insertGetId($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    if(!empty($name) || !empty($content)){
                       DB::table('categories_translations')
                                ->insert(['category_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'created_at'=>$created_at,'updated_at'=>$created_at]);
                    }
                }
            }

            return redirect('admin/categories/')->withMessage('Category details has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record     = DB::table('categories')->where('id', $id)->first();
        //$allRecords = DB::select('select * from categories WHERE parent_id=0 AND id !="'.$id.'" ORDER BY display_order ASC');
        $allRecords = Category::where('parent_id',0)->where('id','<>', $id)->orderby('display_order','ASC')->get();   
        if ($record != null){
            $allOtherSlide  = DB::table('categories_translations')->where('category_id', $id)->get();   
            $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
            $allOtherLang   = array();
            if(!empty($allOtherSlide)){
                foreach ($allOtherSlide as $singleLang) {
                    $allOtherLang[$singleLang->language]['name'] = $singleLang->name;
                    $allOtherLang[$singleLang->language]['content'] = $singleLang->content;
                }
            }
            $view_data['hasForm']       = 'yes';
            $view_data['currPage']      = 'editCategory';
            $view_data['record']        = $record;
            $view_data['allRecords']    = $allRecords;
            $view_data['allLanguages']  = $allLanguages;
            $view_data['allOtherLang']  = $allOtherLang;
            return view('admin.editcategory', $view_data);
        }else{
            return redirect('admin/categories'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name' 				=> 'required',
            //'display_order' 	=> 'nullable|numeric|min:1',
            'image'             => 'nullable|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/categories/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate(); 
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                    $old_path = $request->input('old_image');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    }                                        
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_image'));
                }
            }           
            $data_array['slug']             = create_slug($request->input('name'),$currid,'categories','slug');
            $data_array['name']             = $request->input('name');
            $data_array['content']        	= $request->input('content');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');
            $data_array['parent_id']        = $request->input('parent_id');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('categories')
            ->where('id', $currid)
            ->update($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    if(!empty($name) || !empty($content)){
                        $getLangData = DB::table('categories_translations')->where('category_id',$currid)->where('language',$lang)->first();
                        if(!empty($getLangData)){
                            DB::table('categories_translations')
                                ->where('category_id',$currid)->where('language',$lang)
                                ->update(['name'=>$name,'content'=>$content,'updated_at'=>$created_at]);
                        }else{
                            DB::table('categories_translations')
                                ->insert(['category_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'created_at'=>$created_at,'updated_at'=>$created_at]);
                        }
                    }else{
                        DB::table('categories_translations')->where('category_id',$currid)->where('language',$lang)->delete();
                    }
                }
            }
            return redirect('admin/categories/')->withMessage('Category details has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $category   = DB::table('categories')->where('id', $id)->first();
        if ($category != null){
            if($category->parent_id==0){
                $childCategory   = DB::table('categories')->where('parent_id', $category->id)->first();
                if(!empty($childCategory)){
                    return redirect('admin/categories/')->withErrors('This category have some child categories.');
                }
            }
            DB::table('categories')->where('id', $id)->delete();
            DB::table('categories_translations')->where('category_id', $id)->delete();
            $old_path = $category->image;
            if(!empty($old_path)){
                $old_path = str_replace(s3Path(), '', $old_path);
                Storage::disk('s3')->delete($old_path);
                @unlink($old_path);
            } 
            return redirect('admin/categories/')->withMessage('Category has been deleted successfully.');
        }else{
            return redirect('admin/categories'); 
        } 
    }

    public function categoryProduct($id)
    {   
        $record     = DB::table('categories')->where('id', $id)->first();
        if ($record != null){
            $allRecords = DB::table('products')
                ->whereRaw('FIND_IN_SET('.$id.',category_id)')
                ->orderby('display_order', 'ASC')
                ->get();
            $view_data['hasTable']      = 'yes';
            $view_data['currPage']      = 'allproduct';
            $view_data['allRecords']    = $allRecords;
            $view_data['record']        = $record;
            return view('admin.allcategoryproducts', $view_data);
        }else{
            return redirect('admin/categories'); 
        }
    }
}