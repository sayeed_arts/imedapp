<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;

class CouponsController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allRecords = DB::table('coupons')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allcoupons';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allcoupons', $view_data);
    }

    public function add()
    {        
        $view_data['currPage']      = 'allcoupons';
        $view_data['hasForm']       = 'yes';
        return view('admin.addcoupon',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'                      => 'required',
            'code'                      => 'required|unique:coupons,code',
            'value'                     => 'required|numeric',
            'start_date'                => 'required',
            'end_date'                  => 'required',
            'minimum_spend'             => 'required|numeric',
            'maximum_spend'             => 'required|numeric',
            'maximum_discount'          => 'required|numeric',
            'usage_limit_per_coupon'    => 'required|numeric|min:1',
            'usage_limit_per_customer'  => 'required|numeric|min:1',
            'status'                    => 'required',
        ]);        
        if ($validator->fails()) {
            return redirect('admin/coupons/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $data_array['name']                     = $request->input('name');
            $data_array['code']                     = $request->input('code');
            $data_array['type']                     = $request->input('type');
            $data_array['value']                    = $request->input('value');
            $data_array['start_date']               = $request->input('start_date');
            $data_array['end_date']                 = $request->input('end_date');
            $data_array['minimum_spend']            = $request->input('minimum_spend');
            $data_array['maximum_spend']            = $request->input('maximum_spend');
            $data_array['maximum_discount']         = $request->input('maximum_discount');
            $data_array['usage_limit_per_coupon']   = $request->input('usage_limit_per_coupon');
            $data_array['usage_limit_per_customer'] = $request->input('usage_limit_per_customer');
            $data_array['status']                   = $request->input('status');
            $data_array['created_at']               = $created_at;
            $data_array['updated_at']               = $created_at;
            
            DB::table('coupons')
            ->insert($data_array);

            return redirect('admin/coupons/')->withMessage('Coupon has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('coupons')->where('id', $id)->first();        
        if ($record != null){
            $view_data['currPage']      = 'allcoupons';
            $view_data['hasForm']       = 'yes';
            $view_data['record']         = $record;
            return view('admin.editcoupon', $view_data);
        }else{
            return redirect('admin/coupons'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'                      => 'required',
            'code'                      => 'required|unique:coupons,code,'.$currid,
            'value'                     => 'required|numeric',
            'start_date'                => 'required',
            'end_date'                  => 'required',
            'minimum_spend'             => 'required|numeric',
            'maximum_spend'             => 'required|numeric',
            'maximum_discount'          => 'required|numeric',
            'usage_limit_per_coupon'    => 'required|numeric|min:1',
            'usage_limit_per_customer'  => 'required|numeric|min:1',
            'status'                    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/coupons/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();            
            $data_array['name']                     = $request->input('name');
            $data_array['code']                     = $request->input('code');
            $data_array['type']                     = $request->input('type');
            $data_array['value']                    = $request->input('value');
            $data_array['start_date']               = $request->input('start_date');
            $data_array['end_date']                 = $request->input('end_date');
            $data_array['minimum_spend']            = $request->input('minimum_spend');
            $data_array['maximum_spend']            = $request->input('maximum_spend');
            $data_array['maximum_discount']         = $request->input('maximum_discount');
            $data_array['usage_limit_per_coupon']   = $request->input('usage_limit_per_coupon');
            $data_array['usage_limit_per_customer'] = $request->input('usage_limit_per_customer');
            $data_array['status']                   = $request->input('status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('coupons')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/coupons/')->withMessage('Coupon has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('coupons')->where('id', $id)->first();        
        if ($record != null){
            DB::table('coupons')->where('id', $id)->delete();
            return redirect('admin/coupons/')->withMessage('Coupon has been deleted successfully.');
        }else{
            return redirect('admin/coupons'); 
        } 
    }    
}