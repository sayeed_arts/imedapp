<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use File;
use Hash;
use Validator;
use App\Users;
class CurrencyController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }

    public function index()
    {        
        $allRecords = DB::table('currencies')->orderby('defaultC','DESC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allCurrency';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allcurrency', $view_data);
    }

    public function add()
    {   
        $view_data['currPage']      = 'allCurrency';
        $view_data['hasForm']       = 'yes';
        return view('admin.addcurrency',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'            => 'required',         
            'code'            => 'required|unique:currencies,code',            
            'exchange_rate'   => 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            if($request->input('defaultC')==1){
                DB::table('currencies')
                    ->update(['defaultC'=>0]);
            }
            $created_at                 = currentDBDate();                     
            $data_array['code']         = $request->input('code');
            $data_array['name']         = $request->input('name');
            $data_array['symbol']         = $request->input('symbol');
            $data_array['exchange_rate']  = $request->input('exchange_rate');
            $data_array['symbol_position']= $request->input('symbol_position');
            $data_array['defaultC']     = $request->input('defaultC');
            $data_array['status']       = $request->input('status');
            $data_array['created_at']   = $created_at;
            $data_array['updated_at']   = $created_at;
            
            DB::table('currencies')
            ->insert($data_array);

            return redirect('admin/currency/')->withMessage('Currency has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('currencies')->where('id', $id)->first();        
        if ($record != null){
             $view_data['currPage']     = 'allCurrency';
            $view_data['hasForm']       = 'yes';
            $view_data['record']        = $record;
            return view('admin.editcurrency', $view_data);
        }else{
            return redirect('admin/currency'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'code'              => 'required|unique:currencies,code,'.$currid,            
            'exchange_rate'   => 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at                 = currentDBDate();                                 
            $data_array['code']         = $request->input('code');
            $data_array['name']         = $request->input('name');
            $data_array['symbol']         = $request->input('symbol');
            $data_array['exchange_rate']         = $request->input('exchange_rate');
            $data_array['symbol_position']         = $request->input('symbol_position');
            $data_array['defaultC']     = $request->input('defaultC');
            $data_array['status']       = $request->input('status');
            $data_array['updated_at']   = $created_at;
            if($request->input('defaultC')==1){
                DB::table('currencies')
                    ->update(['defaultC'=>0]);
            }
            DB::table('currencies')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/currency/')->withMessage('Currency has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('currencies')->where('id', $id)->first();        
        if ($record != null){
            DB::table('currencies')->where('id', $id)->delete();
            return redirect('admin/currency/')->withMessage('Currency has been deleted successfully.');
        }else{
            return redirect('admin/currency'); 
        } 
    }
}