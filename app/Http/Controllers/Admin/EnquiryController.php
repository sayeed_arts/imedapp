<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;
use App\Users;
use App\VerifyUser;
use App\Category;

class EnquiryController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {
        $allenquiries = DB::select("select A.* from getaquote A ORDER BY A.id DESC");
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allenquiries';
        $view_data['allRecords']    = $allenquiries;
        return view('admin.allenquiries', $view_data);
    }

    public function detail($id){
         $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'enquirydetail';
       
        $view_data['vendordetail'] = DB::table('getaquote')->select('getaquote.*','states.stateName as stateName','countries.countryName as countryname')->where('id', $id)
        ->join('countries','getaquote.country','=','countries.countryID')
        ->join('states','getaquote.state','=','states.stateID')
        ->first();
     return view('admin.enquirydetail', $view_data);   
    }

}