<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;

class FaqController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allRecords = DB::table('faqs')->orderby('display_order', 'ASC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allfaqs';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allfaqs', $view_data);
    }

    public function add()
    {        
        $view_data['currPage']      = 'allfaqs';
        $view_data['hasForm']       = 'yes';
        return view('admin.addfaq',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'content'           => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/faq/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();            
            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            
            DB::table('faqs')
            ->insert($data_array);

            return redirect('admin/faq/')->withMessage('FAQ has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('faqs')->where('id', $id)->first();        
        if ($record != null){
            $view_data['currPage']      = 'allfaqs';
            $view_data['hasForm']       = 'yes';
            $view_data['record']         = $record;
            return view('admin.editfaq', $view_data);
        }else{
            return redirect('admin/faq'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'content'           => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/faq/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('faqs')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/faq/')->withMessage('FAQ has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('faqs')->where('id', $id)->first();        
        if ($record != null){
            DB::table('faqs')->where('id', $id)->delete();
            return redirect('admin/faq/')->withMessage('FAQ has been deleted successfully.');
        }else{
            return redirect('admin/faq'); 
        } 
    }    
}