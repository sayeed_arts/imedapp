<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;
use App\JobCategory;
use App\JobLocation;
use App\JobType;

class JobApplicationController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allRecords = DB::table('jobapplications')
                        ->leftJoin('jobs','jobapplications.job_id','=','jobs.id')
                        ->select('jobapplications.*','jobs.name')
                        ->orderby('id', 'DESC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'alljobapplications';
        $view_data['allRecords']    = $allRecords;
        return view('admin.alljobapplications', $view_data);
    }

    public function view($id)
    {
        $record   = DB::table('jobapplications')
                    ->leftJoin('jobs','jobapplications.job_id','=','jobs.id')
                    ->select('jobapplications.*','jobs.name')
                    ->where('jobapplications.id', $id)
                    ->first();  
        $allCategories = JobCategory::orderby('display_order','ASC')->get();  
        $allLocations = JobLocation::orderby('display_order','ASC')->get(); 
        $allTypes = JobType::orderby('display_order','ASC')->get();         
        if ($record != null){
            $view_data['currPage']      = 'alljobs';
            $view_data['hasForm']       = 'yes';
            $view_data['record']         = $record;
            $view_data['allCategories'] = $allCategories;
            $view_data['allLocations'] = $allLocations;
            $view_data['allTypes'] = $allTypes;
            return view('admin.viewjobapplication', $view_data);
        }else{
            return redirect('admin/jobapplications'); 
        } 
    }
    
    public function delete($id)
    {
        $record   = DB::table('jobapplications')->where('id', $id)->first();        
        if ($record != null){
            DB::table('jobapplications')->where('id', $id)->delete();
            return redirect('admin/jobapplications/')->withMessage('Job Application has been deleted successfully.');
        }else{
            return redirect('admin/jobapplications'); 
        } 
    }    
}