<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Hash;
use Validator;
use App\JobCategory;

class JobCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');      
    }
    public function index()
    {       
        $allRecords = JobCategory::orderby('display_order','ASC')->get();        
        $view_data['hasTable']          = 'yes';
        $view_data['currPage']          = 'alljobcategories';
        $view_data['allRecords']        = $allRecords;
        return view('admin.alljobcategories', $view_data);
    }
    public function add()
    {
        $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
        //$allRecords   = DB::select('select * from categories WHERE parent_id=0 ORDER BY display_order ASC');
        $allRecords     = JobCategory::orderby('display_order','ASC')->get();   
        $view_data['hasForm']           = 'yes';
        $view_data['currPage']          = 'addJobCategory';
        $view_data['allRecords']        = $allRecords;
        $view_data['allLanguages']      = $allLanguages;
        return view('admin.addjobcategory',$view_data);
    }
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' 			=> 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/jobcategories/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
                  
            $data_array['slug']     = create_slug($request->input('name'),'','jobcategories','slug');
            $data_array['name']          = $request->input('name');
            
            $data_array['content']       = $request->input('content');
            $data_array['meta_title']    = $request->input('meta_title');
            $data_array['meta_key']      = $request->input('meta_key');
            $data_array['meta_desc']     = $request->input('meta_desc');
            $data_array['display_order'] = $request->input('display_order');
            $data_array['display_status']= $request->input('display_status');
            $data_array['created_at']    = $created_at;
            $data_array['updated_at']    = $created_at;
            
            $currid = DB::table('jobcategories')->insertGetId($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    if(!empty($name) || !empty($content)){
                       DB::table('jobcategories_translations')
                                ->insert(['category_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'created_at'=>$created_at,'updated_at'=>$created_at]);
                    }
                }
            }

            return redirect('admin/jobcategories/')->withMessage('Category details has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record     = DB::table('jobcategories')->where('id', $id)->first();
        
        $allRecords = JobCategory::where('id','<>', $id)->orderby('display_order','ASC')->get();   
        if ($record != null){
            $allOtherSlide  = DB::table('jobcategories_translations')->where('category_id', $id)->get();   
            $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
            $allOtherLang   = array();
            if(!empty($allOtherSlide)){
                foreach ($allOtherSlide as $singleLang) {
                    $allOtherLang[$singleLang->language]['name'] = $singleLang->name;
                    $allOtherLang[$singleLang->language]['content'] = $singleLang->content;
                }
            }
            $view_data['hasForm']       = 'yes';
            $view_data['currPage']      = 'editJobCategory';
            $view_data['record']        = $record;
            $view_data['allRecords']    = $allRecords;
            $view_data['allLanguages']  = $allLanguages;
            $view_data['allOtherLang']  = $allOtherLang;
            return view('admin.editjobcategory', $view_data);
        }else{
            return redirect('admin/jobcategories'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name' 				=> 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/jobcategories/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate(); 
                      
            $data_array['slug']             = create_slug($request->input('name'),$currid,'jobcategories','slug');
            $data_array['name']             = $request->input('name');
            $data_array['content']        	= $request->input('content');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');

            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('jobcategories')
            ->where('id', $currid)
            ->update($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    if(!empty($name) || !empty($content)){
                        $getLangData = DB::table('jobcategories_translations')->where('category_id',$currid)->where('language',$lang)->first();
                        if(!empty($getLangData)){
                            DB::table('jobcategories_translations')
                                ->where('category_id',$currid)->where('language',$lang)
                                ->update(['name'=>$name,'content'=>$content,'updated_at'=>$created_at]);
                        }else{
                            DB::table('jobcategories_translations')
                                ->insert(['category_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'created_at'=>$created_at,'updated_at'=>$created_at]);
                        }
                    }else{
                        DB::table('jobcategories_translations')->where('category_id',$currid)->where('language',$lang)->delete();
                    }
                }
            }
            return redirect('admin/jobcategories/')->withMessage('Category details has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $category   = DB::table('jobcategories')->where('id', $id)->first();
        if ($category != null){
            DB::table('jobcategories')->where('id', $id)->delete();
            DB::table('jobcategories_translations')->where('category_id', $id)->delete();
            $jobs  = DB::table('jobs')->where('category_id', $id)->get();   
            foreach ($jobs as $singleJob) {
                DB::table('jobs')->where('id', $singleJob)->delete();
                DB::table('jobs_translations')->where('job_id', $singleJob)->delete();
            }
            return redirect('admin/jobcategories/')->withMessage('Category has been deleted successfully.');
        }else{
            return redirect('admin/jobcategories'); 
        } 
    }

    public function categoryJob($id)
    {   
        $record     = DB::table('jobcategories')->where('id', $id)->first();
        if ($record != null){
            $allRecords = DB::table('jobs')
                ->whereRaw('FIND_IN_SET('.$id.',category_id)')
                ->orderby('display_order', 'ASC')
                ->get();
            $view_data['hasTable']      = 'yes';
            $view_data['currPage']      = 'alljob';
            $view_data['allRecords']    = $allRecords;
            $view_data['record']        = $record;
            return view('admin.allcategoryjobs', $view_data);
        }else{
            return redirect('admin/jobcategories'); 
        }
    }
}
