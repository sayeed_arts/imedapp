<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;
use App\JobCategory;
use App\JobLocation;
use App\JobType;

class JobController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allRecords = DB::table('jobs')->orderby('display_order', 'ASC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'alljobs';
        $view_data['allRecords']    = $allRecords;
        return view('admin.alljobs', $view_data);
    }

    public function add()
    {        
        $allCategories = JobCategory::orderby('display_order','ASC')->get();  
        $allLocations = JobLocation::orderby('display_order','ASC')->get(); 
        $allTypes = JobType::orderby('display_order','ASC')->get();            
        $view_data['currPage']      = 'alljobs';
        $view_data['hasForm']       = 'yes';
        $view_data['allCategories'] = $allCategories;
        $view_data['allLocations'] = $allLocations;
        $view_data['allTypes'] = $allTypes;
        
        return view('admin.addjob',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'content'           => 'required',
            'email'             => 'required|email',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/jobs/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();            
            $data_array['name']             = $request->input('name');
            $data_array['slug']             = create_slug($request->input('name'),'','jobs','slug');
            $data_array['content']          = $request->input('content');
            $data_array['requirements']     = $request->input('requirements');
            $data_array['experience']       = $request->input('experience');
            $data_array['email']            = $request->input('email');
            $data_array['closing_date']     = date("Y-m-d", strtotime($request->input('closing_date')));
            $data_array['category_id']      = implode(",", $request->input('category_id'));
            $data_array['location_id']      = $request->input('location_id');
            $data_array['type_id']          = $request->input('type_id');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            
            DB::table('jobs')
            ->insert($data_array);

            return redirect('admin/jobs/')->withMessage('Job has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('jobs')->where('id', $id)->first();  
        $allCategories = JobCategory::orderby('display_order','ASC')->get();  
        $allLocations = JobLocation::orderby('display_order','ASC')->get(); 
        $allTypes = JobType::orderby('display_order','ASC')->get();         
        if ($record != null){
            $view_data['currPage']      = 'alljobs';
            $view_data['hasForm']       = 'yes';
            $view_data['record']         = $record;
            $view_data['allCategories'] = $allCategories;
            $view_data['allLocations'] = $allLocations;
            $view_data['allTypes'] = $allTypes;
            return view('admin.editjob', $view_data);
        }else{
            return redirect('admin/jobs'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'content'           => 'required',
            'email'             => 'required|email',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/jobs/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $data_array['name']             = $request->input('name');
            $data_array['slug']             = create_slug($request->input('name'),'','jobs','slug');
            $data_array['content']          = $request->input('content');
            $data_array['requirements']     = $request->input('requirements');
            $data_array['experience']       = $request->input('experience');
            $data_array['email']            = $request->input('email');
            $data_array['closing_date']     = date("Y-m-d", strtotime($request->input('closing_date')));
            $data_array['category_id']      = implode(",", $request->input('category_id'));
            $data_array['location_id']      = $request->input('location_id');
            $data_array['type_id']          = $request->input('type_id');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('jobs')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/jobs/')->withMessage('Job has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('jobs')->where('id', $id)->first();        
        if ($record != null){
            DB::table('jobs')->where('id', $id)->delete();
            return redirect('admin/jobs/')->withMessage('Job has been deleted successfully.');
        }else{
            return redirect('admin/jobs'); 
        } 
    }    
}