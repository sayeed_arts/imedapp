<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Hash;
use Validator;
use App\JobType;

class JobTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');      
    }
    public function index()
    {       
        $allRecords = JobType::orderby('display_order','ASC')->get();        
        $view_data['hasTable']          = 'yes';
        $view_data['currPage']          = 'alljobtypes';
        $view_data['allRecords']        = $allRecords;
        return view('admin.alljobtypes', $view_data);
    }
    public function add()
    {
        $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
        //$allRecords   = DB::select('select * from categories WHERE parent_id=0 ORDER BY display_order ASC');
        $allRecords     = JobType::orderby('display_order','ASC')->get();   
        $view_data['hasForm']           = 'yes';
        $view_data['currPage']          = 'addJobType';
        $view_data['allRecords']        = $allRecords;
        $view_data['allLanguages']      = $allLanguages;
        return view('admin.addjobtype',$view_data);
    }
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' 			=> 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/jobtypes/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
                  
            $data_array['slug']     = create_slug($request->input('name'),'','jobtypes','slug');
            $data_array['name']          = $request->input('name');
            
            $data_array['content']       = $request->input('content');
            $data_array['meta_title']    = $request->input('meta_title');
            $data_array['meta_key']      = $request->input('meta_key');
            $data_array['meta_desc']     = $request->input('meta_desc');
            $data_array['display_order'] = $request->input('display_order');
            $data_array['display_status']= $request->input('display_status');
            $data_array['created_at']    = $created_at;
            $data_array['updated_at']    = $created_at;
            
            $locid = DB::table('jobtypes')->insertGetId($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    if(!empty($name) || !empty($content)){
                       DB::table('jobtypes_translations')
                                ->insert(['type_id'=>$locid,'language'=>$lang,'name'=>$name,'content'=>$content,'created_at'=>$created_at,'updated_at'=>$created_at]);
                    }
                }
            }

            return redirect('admin/jobtypes/')->withMessage('Type details has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record     = DB::table('jobtypes')->where('id', $id)->first();
        
        $allRecords = JobType::where('id','<>', $id)->orderby('display_order','ASC')->get();   
        if ($record != null){
            $allOtherSlide  = DB::table('jobtypes_translations')->where('type_id', $id)->get();   
            $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
            $allOtherLang   = array();
            if(!empty($allOtherSlide)){
                foreach ($allOtherSlide as $singleLang) {
                    $allOtherLang[$singleLang->language]['name'] = $singleLang->name;
                    $allOtherLang[$singleLang->language]['content'] = $singleLang->content;
                }
            }
            $view_data['hasForm']       = 'yes';
            $view_data['currPage']      = 'editJobType';
            $view_data['record']        = $record;
            $view_data['allRecords']    = $allRecords;
            $view_data['allLanguages']  = $allLanguages;
            $view_data['allOtherLang']  = $allOtherLang;
            return view('admin.editjobtype', $view_data);
        }else{
            return redirect('admin/jobtypes'); 
        } 
    }
    public function update(Request $request)
    {
        $locid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name' 				=> 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/jobtypes/edit/'.$locid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate(); 
                      
            $data_array['slug']             = create_slug($request->input('name'),$locid,'jobtypes','slug');
            $data_array['name']             = $request->input('name');
            $data_array['content']        	= $request->input('content');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');

            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('jobtypes')
            ->where('id', $locid)
            ->update($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    if(!empty($name) || !empty($content)){
                        $getLangData = DB::table('jobtypes_translations')->where('type_id',$locid)->where('language',$lang)->first();
                        if(!empty($getLangData)){
                            DB::table('jobtypes_translations')
                                ->where('type_id',$locid)->where('language',$lang)
                                ->update(['name'=>$name,'content'=>$content,'updated_at'=>$created_at]);
                        }else{
                            DB::table('jobtypes_translations')
                                ->insert(['type_id'=>$locid,'language'=>$lang,'name'=>$name,'content'=>$content,'created_at'=>$created_at,'updated_at'=>$created_at]);
                        }
                    }else{
                        DB::table('jobtypes_translations')->where('type_id',$locid)->where('language',$lang)->delete();
                    }
                }
            }
            return redirect('admin/jobtypes/')->withMessage('Type details has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $category   = DB::table('jobtypes')->where('id', $id)->first();
        if ($category != null){
            DB::table('jobtypes')->where('id', $id)->delete();
            DB::table('jobtypes_translations')->where('type_id', $id)->delete();
            $jobs  = DB::table('jobs')->where('type_id', $id)->get();   
            foreach ($jobs as $singleJob) {
                DB::table('jobs')->where('id', $singleJob)->delete();
                DB::table('jobs_translations')->where('job_id', $singleJob)->delete();
            }
            return redirect('admin/jobtypes/')->withMessage('Type has been deleted successfully.');
        }else{
            return redirect('admin/jobtypes'); 
        } 
    }

    public function typeJob($id)
    {   
        $record     = DB::table('jobtypes')->where('id', $id)->first();
        if ($record != null){
            $allRecords = DB::table('jobs')
                ->whereRaw('FIND_IN_SET('.$id.',type_id)')
                ->orderby('display_order', 'ASC')
                ->get();
            $view_data['hasTable']      = 'yes';
            $view_data['currPage']      = 'alljob';
            $view_data['allRecords']    = $allRecords;
            $view_data['record']        = $record;
            return view('admin.alltypejobs', $view_data);
        }else{
            return redirect('admin/jobtypes'); 
        }
    }
}
