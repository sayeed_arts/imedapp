<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use File;
use Hash;
use Validator;
use App\Users;
class LanguageTranslationController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    /**
     * Remove the specified resource from storage.
     * @return Response
    */
    public function index()
    {
   	    $languages = DB::table('languages')->where('status',1)->get();

   	    $columns = [];
        $columnsCount = $languages->count();

	    if($languages->count() > 0){
	        foreach ($languages as $key => $language){
	            if ($key == 0) {
	                $columns[$key] = $this->openJSONFile($language->code);
	            }
	            $columns[++$key] = ['data'=>$this->openJSONFile($language->code), 'lang'=>$language->code];
	        }
	    }
   	    return view('admin.languages', compact('languages','columns','columnsCount'));
    }   
    /**
     * Remove the specified resource from storage.
     * @return Response
    */
    public function store(Request $request)
    {
   	    $request->validate([
		    'key' => 'required',
		    'value' => 'required',
		]);
		$data = $this->openJSONFile('en');
        $data[$request->key] = $request->value;
        $this->saveJSONFile('en', $data);
        return redirect()->route('languages');
    }


    /**
     * Remove the specified resource from storage.
     * @return Response
    */
    public function destroy($key)
    {
        $languages = DB::table('languages')->get();
        if($languages->count() > 0){
            foreach ($languages as $language){
                $data = $this->openJSONFile($language->code);
                unset($data[$key]);
                $this->saveJSONFile($language->code, $data);
            }
        }
        return response()->json(['success' => $key]);
    }


    /**
     * Open Translation File
     * @return Response
    */
    private function openJSONFile($code){
        $jsonString = [];
        if(File::exists(base_path('resources/lang/'.$code.'.json'))){
            $jsonString = file_get_contents(base_path('resources/lang/'.$code.'.json'));
            $jsonString = json_decode($jsonString, true);
        }
        return $jsonString;
    }


    /**
     * Save JSON File
     * @return Response
    */
    private function saveJSONFile($code, $data){
        ksort($data);
        $jsonData = json_encode($data, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
        file_put_contents(base_path('resources/lang/'.$code.'.json'), stripslashes($jsonData));
    }


    /**
     * Save JSON File
     * @return Response
    */
    public function transUpdate(Request $request){
        $data = $this->openJSONFile($request->code);
        $data[$request->pk] = $request->value;


        $this->saveJSONFile($request->code, $data);
        return response()->json(['success'=>'Done!']);
    }


    /**
     * Remove the specified resource from storage.
     * @return Response
    */
    public function transUpdateKey(Request $request){
        $languages = DB::table('languages')->get();

        if($languages->count() > 0){
            foreach ($languages as $language){
                $data = $this->openJSONFile($language->code);
                if (isset($data[$request->pk])){
                    $data[$request->value] = $data[$request->pk];
                    unset($data[$request->pk]);
                    $this->saveJSONFile($language->code, $data);
                }
            }
        }

        return response()->json(['success'=>'Done!']);
    }

    public function allLanguages()
    {        
        $allRecords = DB::table('languages')->where('id','<>',1)->orderby('defaultL','DESC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allLanguages';
        $view_data['allRecords']    = $allRecords;
        return view('admin.alllanguages', $view_data);
    }

    public function add()
    {   
        $view_data['currPage']      = 'allLanguages';
        $view_data['hasForm']       = 'yes';
        return view('admin.addlanguages',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'            => 'required',            
            'code'            => 'required|unique:languages,code'
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/languages/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            if($request->input('defaultL')==1){
                DB::table('languages')
                    ->update(['defaultL'=>0]);
            }
            $created_at                 = currentDBDate();                     
            $data_array['code']         = $request->input('code');
            $data_array['name']         = $request->input('name');
            $data_array['defaultL']     = $request->input('defaultL');
            $data_array['status']       = $request->input('status');
            $data_array['created_at']   = $created_at;
            $data_array['updated_at']   = $created_at;
            
            DB::table('languages')
            ->insert($data_array);

            return redirect('admin/alllanguages/')->withMessage('Language has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('languages')->where('id', $id)->first();        
        if ($record != null){
             $view_data['currPage']     = 'allLanguages';
            $view_data['hasForm']       = 'yes';
            $view_data['record']        = $record;
            return view('admin.editlanguages', $view_data);
        }else{
            return redirect('admin/alllanguages'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'name'              => 'required|unique:languages,code,'.$currid            
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at                 = currentDBDate();                                 
            $data_array['code']         = $request->input('code');
            $data_array['name']         = $request->input('name');
            $data_array['defaultL']     = $request->input('defaultL');
            $data_array['status']       = $request->input('status');
            $data_array['updated_at']   = $created_at;
            if($request->input('defaultL')==1){
                DB::table('languages')
                    ->update(['defaultL'=>0]);
            }
            DB::table('languages')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/alllanguages/')->withMessage('Language has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('languages')->where('id', $id)->first();        
        if ($record != null){
            DB::table('languages')->where('id', $id)->delete();
            return redirect('admin/alllanguages/')->withMessage('Language has been deleted successfully.');
        }else{
            return redirect('admin/alllanguages'); 
        } 
    }
}