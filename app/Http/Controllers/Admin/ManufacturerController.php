<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;
use Illuminate\Support\Facades\Storage;

class ManufacturerController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allRecords = DB::table('manufacturer')->orderby('display_order', 'ASC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allmanufacturer';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allmanufacturers', $view_data);
    }

    public function add()
    {
        $allLanguages = DB::table('languages')->where('id','<>', 1)->get();
        $view_data['currPage']      = 'allmanufacturer';
        $view_data['hasForm']       = 'yes';        
        $view_data['allLanguages']  = $allLanguages;
        return view('admin.addmanufacturer',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/manufacturers/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();            
           $logo          = $request->file('logo');
            if($logo!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->logo);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['logo']   = s3Path().$path;
                 
                    $old_path = $request->input('logo');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    }  
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['logo']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_logo_image'));
                }
            }


            $data_array['name']             = $request->input('name');
            $data_array['slug']             = create_slug($request->input('name'),'','manufacturer','slug');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['featured']   = $request->input('featured');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            
            $currid = DB::table('manufacturer')->insertGetId($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    if(!empty($name)){
                       DB::table('manufacturer_translations')
                                ->insert(['manufacturer_id'=>$currid,'language'=>$lang,'name'=>$name,'created_at'=>$created_at,'updated_at'=>$created_at]);
                    }
                }
            }

            return redirect('admin/manufacturers/')->withMessage('Manufacturer has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('manufacturer')->where('id', $id)->first();        
        if ($record != null){
            $allOtherSlide  = DB::table('manufacturer_translations')->where('manufacturer_id', $id)->get();   
            $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
            $allOtherLang   = array();
            if(!empty($allOtherSlide)){
                foreach ($allOtherSlide as $singleLang) {
                    $allOtherLang[$singleLang->language]['name'] = $singleLang->name;
                }
            }
            $view_data['currPage']      = 'allmanufacturer';
            $view_data['hasForm']       = 'yes';
            $view_data['record']        = $record;
            $view_data['allLanguages']  = $allLanguages;
            $view_data['allOtherLang']  = $allOtherLang;
            return view('admin.editmanufacturer', $view_data);
        }else{
            return redirect('admin/manufacturers'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/manufacturers/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $logo          = $request->file('logo');
            if($logo!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->logo);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['logo']   = s3Path().$path;
                 
                    $old_path = $request->input('logo');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    }  
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['logo']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_logo_image'));
                }
            }

            $data_array['name']             = $request->input('name');
            $data_array['slug']             = create_slug($request->input('name'),'','manufacturer','slug');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['featured']         = $request->input('featured');
            $data_array['updated_at']       = $created_at;
            
            DB::table('manufacturer')
            ->where('id', $currid)
            ->update($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    if(!empty($name)){
                        $getLangData = DB::table('manufacturer_translations')->where('manufacturer_id',$currid)->where('language',$lang)->first();
                        if(!empty($getLangData)){
                            DB::table('manufacturer_translations')
                                ->where('manufacturer_id',$currid)->where('language',$lang)
                                ->update(['name'=>$name,'updated_at'=>$created_at]);
                        }else{
                            DB::table('manufacturer_translations')
                                ->insert(['manufacturer_id'=>$currid,'language'=>$lang,'name'=>$name,'created_at'=>$created_at,'updated_at'=>$created_at]);
                        }
                    }else{
                        DB::table('manufacturer_translations')->where('manufacturer_id',$currid)->where('language',$lang)->delete();
                    }
                }
            }
            return redirect('admin/manufacturers/')->withMessage('manufacturer has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('manufacturer')->where('id', $id)->first();        
        if ($record != null){
            DB::table('manufacturer')->where('id', $id)->delete();
            DB::table('categories_translations')->where('manufacturer_id', $id)->delete();
            return redirect('admin/manufacturers/')->withMessage('manufacturer has been deleted successfully.');
        }else{
            return redirect('admin/manufacturers'); 
        } 
    }    
}