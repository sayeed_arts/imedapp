<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;

class NewsletterController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {
        $allRecords = DB::table('newsletters')->orderby('id', 'DESC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allnewsletters';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allnewsletters', $view_data);
    }
    public function edit($id)
    {
        $record   = DB::table('newsletters')->where('id', $id)->first();        
        if ($record != null){
            $view_data['currPage']      = 'allnewsletters';
            $view_data['hasForm']       = 'yes';
            $view_data['record']         = $record;
            return view('admin.editnewsletter', $view_data);
        }else{
            return redirect('admin/newsletter'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'email_address'     => 'required|email|unique:newsletters,email_address,'.$currid,
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/newsletter/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $data_array['email_address']    = $request->input('email_address');
            $data_array['status']           = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('newsletters')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/newsletter/')->withMessage('Subscriber has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('newsletters')->where('id', $id)->first();        
        if ($record != null){
            DB::table('newsletters')->where('id', $id)->delete();
            return redirect('admin/newsletter/')->withMessage('Subscriber has been deleted successfully.');
        }else{
            return redirect('admin/newsletter'); 
        } 
    }

    //Mailing List Functions

    public function mailingList()
    {
        $allRecords = DB::table('mailing_list')->orderby('id', 'DESC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allmailinglists';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allmailinglists', $view_data);
    }
    public function addMailingList()
    {
        $view_data['currPage']      = 'allmailinglists';
        $view_data['hasForm']       = 'yes';
        return view('admin.addmailinglist', $view_data);
    }
    public function saveMailingList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|unique:mailing_list,name',
            'status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/mailinglist/add/')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $data_array['name']         = $request->input('name');
            $data_array['status']       = $request->input('status');
            $data_array['created_at']   = $created_at;
            $data_array['updated_at']   = $created_at;
            
            DB::table('mailing_list')
            ->insert($data_array);

            return redirect('admin/mailinglist/')->withMessage('Mailing list has been added successfully.');
        }
    }
    public function editMailingList($id)
    {
        $record   = DB::table('mailing_list')->where('id', $id)->first();        
        if ($record != null){
            $view_data['currPage']      = 'allmailinglists';
            $view_data['hasForm']       = 'yes';
            $view_data['record']         = $record;
            return view('admin.editmailinglist', $view_data);
        }else{
            return redirect('admin/mailinglist'); 
        } 
    }
    public function updateMailingList(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'      => 'required|unique:mailing_list,name,'.$currid,
            'status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/mailinglist/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $data_array['name']    = $request->input('name');
            $data_array['status']           = $request->input('status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('mailing_list')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/mailinglist/')->withMessage('Mailing list has been updated successfully.');
        }
    }
    public function deleteMailingList($id)
    {
        $record   = DB::table('mailing_list')->where('id', $id)->first();        
        if ($record != null){
            DB::table('mailing_list')->where('id', $id)->delete();
            DB::table('mailing_list_emails')->where('list_id', $id)->delete();
            return redirect('admin/mailinglist/')->withMessage('Mailing list has been deleted successfully.');
        }else{
            return redirect('admin/mailinglist'); 
        } 
    } 

    //Mailing List Functions

    public function mailingListRecords($list_id)
    {
        $allRecords = DB::table('mailing_list_emails')->where('list_id',$list_id)->orderby('id', 'DESC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allmailinglistsrecords';
        $view_data['allRecords']    = $allRecords;
        $view_data['list_id']       = $list_id;
        return view('admin.allmailinglistsrecords', $view_data);
    }
    public function addMailingListRecords($list_id)
    {
        $view_data['currPage']      = 'allmailinglistsrecords';
        $view_data['hasForm']       = 'yes';
        $view_data['list_id']       = $list_id;
        return view('admin.addmailinglistrecords', $view_data);
    }
    public function saveMailingListRecords(Request $request)
    {
        $list_id = $request->input('list_id');
        if(empty($list_id) || !is_numeric($list_id)){
            return redirect('admin/mailinglist/');
        }
        $validator = Validator::make($request->all(), [
            'list_id'   => 'required',
            'name'      => 'required',
            'email_address'=> 'required|email|unique:mailing_list_emails,email_address',
            'status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/listrecords/add/'.$list_id)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at                 = currentDBDate();
            $data_array['list_id']      = $request->input('list_id');
            $data_array['name']         = $request->input('name');
            $data_array['status']       = $request->input('status');
            $data_array['email_address']        = $request->input('email_address');
            $data_array['created_at']   = $created_at;
            $data_array['updated_at']   = $created_at;
            
            DB::table('mailing_list_emails')
            ->insert($data_array);

            return redirect('admin/listrecords/'.$list_id)->withMessage('Mailing list record has been added successfully.');
        }
    }
    public function editMailingListRecords($id)
    {
        $record   = DB::table('mailing_list_emails')->where('id', $id)->first();        
        if ($record != null){
            $view_data['currPage']      = 'allmailinglistsrecords';
            $view_data['hasForm']       = 'yes';
            $view_data['record']         = $record;
            return view('admin.editmailinglistrecords', $view_data);
        }else{
            return redirect('admin/mailinglist'); 
        } 
    }
    public function updateMailingListRecords(Request $request)
    {
        $list_id = $request->input('list_id');
        if(empty($list_id) || !is_numeric($list_id)){
            return redirect('admin/mailinglist/');
        }

        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'email_address'      => 'required|email|unique:mailing_list_emails,email_address,'.$currid,
            'status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/mailinglist/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at                  = currentDBDate();
            $data_array['name']          = $request->input('name');
            $data_array['email_address'] = $request->input('email_address');
            $data_array['status']        = $request->input('status');
            $data_array['updated_at']    = $created_at;
            
            DB::table('mailing_list_emails')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/listrecords/'.$list_id)->withMessage('Mailing list record has been updated successfully.');
        }
    }
    public function deleteMailingListRecords($id)
    {
        $record   = DB::table('mailing_list_emails')->where('id', $id)->first();        
        if ($record != null){
            DB::table('mailing_list_emails')->where('id', $id)->delete();
            return redirect('admin/listrecords/'.$record->list_id)->withMessage('Mailing list record has been deleted successfully.');
        }else{
            return redirect('admin/mailinglist'); 
        } 
    }   
    public function importMailingListRecords(Request $request)
    {
        $list_id = $request->input('list_id');
        if(empty($list_id) || !is_numeric($list_id)){
            return redirect('admin/mailinglist/');
        }
        $validator = Validator::make($request->all(), [
            'list_id'           => 'required',
            'importlistrecords' => 'required|file',
        ]);
        $result = array($request->file('importlistrecords')->getClientOriginalExtension());
        $extensions = array("csv");
        if(in_array(strtolower($result[0]),$extensions)){
            if ($validator->fails()) {
                return redirect('admin/listrecords/'.$list_id)
                        ->withErrors($validator)
                        ->withInput();
            }else{
                $path       = $request->file('importlistrecords')->getRealPath();
                $csv_data   = array_map('str_getcsv', file($path));                        
                if(!empty($csv_data)){
                    foreach ($csv_data as $singleData) {
                        $email = (isset($singleData[1]))?trim($singleData[1]):'';
                        $name = (isset($singleData[0]))?trim($singleData[0]):'';
                        if(!empty($email)){
                            $record   = DB::table('mailing_list_emails')->where('email_address', $email)->first();
                            if ($record == null){
                                $created_at                 = currentDBDate();
                                $data_array = array();
                                $data_array['list_id']      = $request->input('list_id');
                                $data_array['name']         = $name;
                                $data_array['status']       = '1';
                                $data_array['email_address']= $email;
                                $data_array['created_at']   = $created_at;
                                $data_array['updated_at']   = $created_at;
                                DB::table('mailing_list_emails')->insert($data_array);
                            }
                        }
                    }
                }
                return redirect('admin/listrecords/'.$list_id)->withMessage('File has been imported successfully.');
            } 
        }else{
            return redirect('admin/listrecords/'.$list_id)->withError('Please upload csv file only.');
        }        
    }
    public function sendEmail()
    {
        $allRecords = DB::table('mailing_list')->orderby('id', 'DESC')->get();
        $view_data['currPage']      = 'sendEmail';
        $view_data['hasForm']       = 'yes';
        $view_data['allRecords']    = $allRecords;
        return view('admin.sendemail', $view_data);
    }
    public function sendEmailPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sendto'   => 'required',
            'subject'   => 'required',
            'name'      => 'required',
            'email'     => 'required|email',
            'content'   => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('admin/sendemail/')
                    ->withErrors($validator)
                    ->withInput();
        }else{ 
            $sendto = $request->input('sendto');

            if($sendto=='background'){
                $allRecords = DB::table('users')->select(DB::raw('CONCAT(name, " ", last_name) AS name'),'email as email_address')->where(array('acc_bg_result'=>NULL,'user_type'=>1,'is_admin'=>0,'is_approved'=>1))->orderby('id', 'DESC')->get();
            }else if($sendto=='verified_phone'){
                $allRecords = DB::table('users')->select(DB::raw('CONCAT(name, " ", last_name) AS name'),'email as email_address')->where(array('phone_verified'=>0,'is_admin'=>0,'is_approved'=>1))->orderby('id', 'DESC')->get();
            }else if($sendto=='caregivers'){
                $allRecords = DB::table('users')->select(DB::raw('CONCAT(name, " ", last_name) AS name'),'email as email_address')->where(array('user_type'=>1,'is_admin'=>0,'is_approved'=>1))->orderby('id', 'DESC')->get();
            }else if($sendto=='careseekers'){
                $allRecords = DB::table('users')->select(DB::raw('CONCAT(name, " ", last_name) AS name'),'email as email_address')->where(array('user_type'=>0,'is_admin'=>0,'is_approved'=>1))->orderby('id', 'DESC')->get();
            }else if($sendto=='news'){
                $allRecords = DB::table('newsletters')->where('status',1)->orderby('id', 'DESC')->get();
            }else{
                $allRecords = DB::table('mailing_list_emails')->where(array('list_id'=>$sendto,'status'=>1))->orderby('id', 'DESC')->get();
            }
            $emailSubject   = $request->input('subject');
            $fromName       = $request->input('name');
            $fromEmail      = $request->input('email');
            $content        = $request->input('content');
            if(!empty($allRecords)){
                foreach ($allRecords as $singleRec) {
                    $toemail    = $singleRec->email_address;
                    $tousername = (isset($singleRec->name))?$singleRec->name:'';
                    $content    = str_replace('[NAME]', $tousername, $content);
                    $content    = str_replace('[EMAIL]', $toemail, $content);
                    $messagebody['emailContent'] = $content;
                    $template       = 'emails.sendEmail';
                    $options = array(
                        'from_name'=>$fromName,
                        'from_email'=>$fromEmail,
                    );
                    send_smtp_email($toemail,$emailSubject,$messagebody,$template,array(),$options);
                }
                return redirect('admin/sendemail/')->withMessage('Email(s) has been sent successfully.');
            }else{
                return redirect('admin/sendemail/')->withError('No record available in this list.');
            }
        }
    }
}