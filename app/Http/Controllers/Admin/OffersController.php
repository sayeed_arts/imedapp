<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;

class OffersController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {   
    	$allRecords = DB::table('offers')->get();
        $view_data['hasTable']          = 'yes';
        $view_data['currPage']          = 'alloffers';
        $view_data['allRecords']        = $allRecords;
        return view('admin.alloffers', $view_data);
    }

    public function add()
    {   
        $allCats 	= DB::select('select * from categories WHERE parent_id=0 ORDER BY display_order ASC');
        $view_data['allCats']        	= $allCats;
        $view_data['hasForm']           = 'yes';
        $view_data['currPage']          = 'addoffer';
        return view('admin.addoffer',$view_data);
    }

    public function save(Request $request)
    {        
        $validator = Validator::make($request->all(), [
            'name' 			=> 'required',          
            'image'         => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/offers/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     		= currentDBDate();
            $image          		= $request->file('image');
            if($image!=''){
                $image_path         = randomFilename(15).'.'.$image->getClientOriginalExtension();
                $destinationPath    = public_path('/uploaded_images');
                $image->move($destinationPath, $image_path);
                $data_array['image']   = 'uploaded_images/'.$image_path;
            }
            if(!empty($request->input('category'))){
            	$data_array['category']         = implode(',', $request->input('category'));
            } 
            if(!empty($request->input('content'))){
                $bulletArray = array();
                foreach ($request->input('content') as $value) {
                    if(!empty($value)){
                        $bulletArray[]=$value;
                    }
                }
                $data_array['content']         = @implode('##~~##', $bulletArray);
            }             
            $data_array['name']             = $request->input('name');
            //$data_array['content']        	= $request->input('content');
            $data_array['link']       		= $request->input('link');
            $data_array['button_text']      = $request->input('button_text');
            $data_array['button_info']      = $request->input('button_info');
            //$data_array['rate_range']    	= $request->input('rate_range');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            
            DB::table('offers')
            ->insert($data_array);

            return redirect('admin/offers/')->withMessage('Offer details has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   	= DB::table('offers')->where('id', $id)->first();
        $allCats 	= DB::select('select * from categories WHERE parent_id=0 ORDER BY display_order ASC');
        $view_data['allCats']        	= $allCats;
        if ($record != null){
            $view_data['hasForm']       = 'yes';
            $view_data['currPage']      = 'editoffer';
            $view_data['record']        = $record;
            return view('admin.editoffer', $view_data);
        }else{
            return redirect('admin/offers'); 
        } 
    }
    public function update(Request $request)
    {   
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name' 				=> 'required',
            'image'             => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/offers/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          		= $request->file('image');
            if($image!=''){
                $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploaded_images');
                $image->move($destinationPath, $image_path);
                $data_array['image']   = 'uploaded_images/'.$image_path;                
                @unlink($request->input('old_image'));
            }            
            if(!empty($request->input('category'))){
            	$data_array['category']         = implode(',', $request->input('category'));
            }
            if(!empty($request->input('content'))){
                $bulletArray = array();
                foreach ($request->input('content') as $value) {
                    if(!empty($value)){
                        $bulletArray[]=$value;
                    }
                }
                $data_array['content']         = @implode('##~~##', $bulletArray);
            }  
            $data_array['name']             = $request->input('name');
            //$data_array['content']        	= $request->input('content');
            $data_array['link']       		= $request->input('link');
            $data_array['button_text']      = $request->input('button_text');
            $data_array['button_info']      = $request->input('button_info');
            //$data_array['rate_range']    	= $request->input('rate_range');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('offers')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/offers/')->withMessage('Offer details has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $offer   = DB::table('offers')->where('id', $id)->first();
        if ($offer != null){            
            DB::table('offers')->where('id', $id)->delete();
            return redirect('admin/offers/')->withMessage('Offer has been deleted successfully.');
        }else{
            return redirect('admin/offers'); 
        } 
    }
    public function assignedOffer($id)
    {   
        if(!empty($id)){
            $record     = DB::table('categories')->where('id', $id)->first();
            if(!empty($record)){
                $parentCategory = $record->parent_id;
                //category
                $availableOffers = DB::table('offers')
                ->whereRaw('FIND_IN_SET('.$parentCategory.',category)')
                ->orwhere('category','')
                ->get();
                $allRecords = DB::table('assigned_offers')->where('caregory_id',$id)->get();
                $view_data['hasTable']          = 'yes';
                $view_data['currPage']          = 'allassignedoffers';
                $view_data['availableOffers']   = $availableOffers;
                $view_data['allRecords']        = $allRecords;
                $view_data['catData']           = $record;
                return view('admin.allassignedoffers', $view_data);
            }else{
                return redirect('categories');
            }            
        }else{
            return redirect('categories');
        }
    }
    public function editAssignedOffer($catId,$id)
    {   
        if(!empty($catId) && !empty($id)){
            $record         = DB::table('categories')->where('id', $catId)->first();
            $assignedOffer  = DB::table('assigned_offers')->where('id', $id)->first();
            if(!empty($record) && !empty($assignedOffer)){
                $parentCategory = $record->parent_id;
                //category
                $availableOffers = DB::table('offers')
                ->whereRaw('FIND_IN_SET('.$parentCategory.',category)')
                ->orwhere('category','')
                ->get();
                $allRecords = DB::table('assigned_offers')->where('caregory_id',$catId)->get();
                $view_data['hasTable']          = 'yes';
                $view_data['currPage']          = 'allassignedoffers';
                $view_data['availableOffers']   = $availableOffers;
                $view_data['allRecords']        = $allRecords;
                $view_data['assignedOffer']     = $assignedOffer;
                $view_data['catData']           = $record;
                return view('admin.allassignedoffers', $view_data);
            }else{
                return redirect('categories');
            }            
        }else{
            return redirect('categories');
        }
    }
     
    public function updateAssignedOffer(Request $request)
    {
        $currid     = $request->input('currid');
        $editcurrid = $request->input('editcurrid');
        $validator  = Validator::make($request->all(), [
            'offer_id'              => 'required'
        ]);
        
        if ($validator->fails()) {
            if(!empty($editcurrid)){
                return redirect('admin/assigned-offers/'.$currid.'/'.$editcurrid)
                        ->withErrors($validator)
                        ->withInput();
            }else{
                return redirect('admin/assigned-offers/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
            }            
        }else{
            $created_at                     = currentDBDate();            
            $data_array['offer_id']         = $request->input('offer_id');                        
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            if(!empty($editcurrid)){
                $checkExists = DB::table('assigned_offers')
                ->where('caregory_id', $currid)
                ->where('offer_id', $data_array['offer_id'])
                ->where('id','<>',$editcurrid)
                ->first();
                if(empty($checkExists)){
                    DB::table('assigned_offers')
                    ->where('id',$editcurrid)
                    ->where('caregory_id',$currid)
                    ->update($data_array);
                }else{
                    return redirect('admin/assigned-offers/'.$currid.'/'.$editcurrid)->withErrors('This offer is already assign for this category.');
                }                
            }else{
                $data_array['created_at']       = $created_at;
                $data_array['caregory_id']      = $request->input('currid');
                $checkExists = DB::table('assigned_offers')
                ->where('caregory_id', $currid)
                ->where('offer_id', $data_array['offer_id'])
                ->first();
                if(empty($checkExists)){
                    DB::table('assigned_offers')->insert($data_array);
                }else{
                    return redirect('admin/assigned-offers/'.$currid)->withErrors('This offer is already assign for this category.');
                }                
            }

            return redirect('admin/assigned-offers/'.$currid)->withMessage('Offer has been assigned successfully.');
        }
    }
    public function deleteAssignOffer($catId,$id)
    {
        $record         = DB::table('categories')->where('id', $catId)->first();
        $assignedOffer  = DB::table('assigned_offers')->where('id', $id)->first();
        if ($record != null && $assignedOffer != null){            
            DB::table('assigned_offers')->where('id', $id)->delete();
            return redirect('admin/assigned-offers/'.$catId)->withMessage('Assigned offer has been removed successfully.');
        }else{
            return redirect('admin/categories'); 
        } 
    }
}