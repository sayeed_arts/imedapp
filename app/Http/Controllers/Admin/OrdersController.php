<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Hash;
use Validator;
use App\Users;
class OrdersController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allRecords = DB::table('orders')
        ->select('orders.*','users.name as customer_first_name','users.last_name as customer_last_name')
        ->join('users','orders.customer_id','=','users.id')
        ->where('orders.status','<>',0)
        ->orderby('ordered_on','DESC')
        ->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allOrders';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allOrders', $view_data);
    }
    public function view($order_id='')
    {   
        $orderData  = DB::table('orders')
            ->where('order_id',$order_id)
            ->first();
        if(!empty($orderData)){
            $id = $orderData->id;
            $shippingState      = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->shipping_state));
            $billingState       = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->state));
            $shippingCountry    = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->shipping_country));
            $billingCountry     = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->country));

            $orderProducts      = DB::table('order_items')
                ->where('order_id',$id)
                ->get();

            $view_data['hasForm']       = 'no';
            $view_data['currPage']      = 'allOrders';
            $view_data['orderData']     = $orderData;
            $view_data['orderProducts'] = $orderProducts;
            $view_data['billingState']  = $billingState;
            $view_data['shippingState'] = $shippingState;
            $view_data['shippingCountry']= $shippingCountry;
            $view_data['billingCountry'] = $billingCountry;
            return view('admin.orderDetails',$view_data);
        }else{
            return redirect(url('admin/orders'));
        }       
    }
     
    public function updatePO(Request $request){
        $validator = Validator::make($request->all(), [
            'po_order_number'=> 'required',
            'po_order_file'  => 'nullable|mimes:jpeg,png,jpg,pdf|max:2048',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $data_array = array(
                'po_order_qty'      => $request->input('po_order_qty'),
                'po_order_number'   => $request->input('po_order_number'),
                'shipping_status'   => 3
            );
            $updated_at         = currentDBDate();
            $currentorderid     = base64_decode($request->input('currentorderid'));
            $po_order_file      = $request->file('po_order_file');
            if($po_order_file!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->po_order_file);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['po_order_file']   = s3Path().$path;
                    $old_path = $request->input('old_po_order_file');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    }  
                }else{
                    $image_path = randomFilename(15).'.'.$po_order_file->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $po_order_file->move($destinationPath, $image_path);
                    $data_array['po_order_file']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_po_order_file'));
                }
            }
            
            DB::table('orders')
            ->where('order_id',$currentorderid)
            ->update($data_array);
            return redirect()
                ->back()->withMessage('Order PO has been updated successfully.');
        }

    } 
    public function getInvoice($order_id='')
    {   
        $orderData  = DB::table('orders')
            ->where('order_id',$order_id)
            ->first();
        if(!empty($orderData)){
            $id = $orderData->id;
            $shippingState  = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->shipping_state));
            $billingState   = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->state));
            $shippingCountry= fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->shipping_country));
            $billingCountry = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->country));
            $orderProducts  = DB::table('order_items')
                ->where('order_id',$id)
                ->get();
            $view_data['hasForm']       = 'no';
            $view_data['currPage']      = 'allOrders';
            $view_data['orderData']     = $orderData;
            $view_data['orderProducts'] = $orderProducts;
            $view_data['billingState']  = $billingState;
            $view_data['shippingState'] = $shippingState;
            $view_data['shippingCountry']= $shippingCountry;
            $view_data['billingCountry'] = $billingCountry;
            return view('emails.invoiceEmail',$view_data);

            /*$value      = view('emails.invoiceEmailPDF', $view_data);
            $file_name  = $orderData->order_number;
            $file_name  = str_replace(' ','-',$file_name).'.pdf';

            $result     = makedompdf($value,$file_name); 
            $file_name = $orderData->order_number;
            $file_name = str_replace(' ','-',$file_name);
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . strlen($result));
            header('Content-Disposition: attachment; filename=' . $file_name.'.pdf' );
            echo $result;
            die();*/
        }else{
            return redirect(url('admin/orders'));
        }       
    }
    public function packingSlip($order_id='')
    {   
        $orderData  = DB::table('orders')
            ->where('order_id',$order_id)
            ->first();
        if(!empty($orderData)){
            $id = $orderData->id;
            $shippingState  = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->shipping_state));
            $billingState   = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->state));
            $shippingCountry= fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->shipping_country));
            $billingCountry = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->country));
            $orderProducts  = DB::table('order_items')
                ->where('order_id',$id)
                ->get();
            $view_data['hasForm']       = 'no';
            $view_data['currPage']      = 'allOrders';
            $view_data['orderData']     = $orderData;
            $view_data['orderProducts'] = $orderProducts;
            $view_data['billingState']  = $billingState;
            $view_data['shippingState'] = $shippingState;
            $view_data['shippingCountry']= $shippingCountry;
            $view_data['billingCountry'] = $billingCountry;
            return view('emails.packingSlip',$view_data);

            /*$value      = view('emails.invoiceEmailPDF', $view_data);
            $file_name  = $orderData->order_number;
            $file_name  = str_replace(' ','-',$file_name).'.pdf';

            $result     = makedompdf($value,$file_name); 
            $file_name = $orderData->order_number;
            $file_name = str_replace(' ','-',$file_name);
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . strlen($result));
            header('Content-Disposition: attachment; filename=' . $file_name.'.pdf' );
            echo $result;
            die();*/
        }else{
            return redirect(url('admin/orders'));
        }       
    }
    public function shippingStatus(Request $request)
    { 
        $order_id           = $request->input('order_id');
        $shipping_status    = $request->input('shipping_status');
        if(!empty($order_id)){
           $orderData  = DB::table('orders')
            ->where('order_id',$order_id)
            ->first();
            if(!empty($orderData)){
                $created_at             = currentDBDate();
                $orderStatus = '';
                if($shipping_status==1){
                    $orderStatus = 'Shipped';
                    $update_array = array(
                        'shipping_status'=>$shipping_status,
                        'shipped_on'=>$created_at,
                    );
                }elseif($shipping_status==0){
                    $orderStatus = 'Received';
                    $update_array = array(
                        'shipping_status'=>$shipping_status,
                        'shipped_on'=>NULL,
                    );
                }else{
                    $update_array = array(
                        'shipping_status'=>$shipping_status
                    );
                }

                if($shipping_status ==2){
                    $orderStatus = 'Voided';
                }else if($shipping_status ==3){
                    $orderStatus = 'Processing';
                }else if($shipping_status ==4){
                    $orderStatus = 'Completed';
                }
                DB::table('orders')->where('order_id',$order_id)->update($update_array);

                $view_data['orderId']=$order_id;
                $view_data['orderStatus']=$orderStatus;
                $view_data['userName']=$orderData->first_name.' '.$orderData->last_name;

                $subject        = 'Order Status Update';
                $userTemplate   = 'emails.orderStatusChange';
                send_smtp_email($orderData->email_address,$subject,$view_data,$userTemplate);
                return redirect(url('admin/orders/'.$order_id))->withMessage('Order status has been updated successfully.');
            }else{
                return redirect(url('admin/orders'));
            }            
        }else{
            return redirect(url('admin/orders'));
        }
    }    
}