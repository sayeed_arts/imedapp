<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;

class PageController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allRecords = DB::table('pages')->orderby('id', 'ASC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allpages';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allpages', $view_data);
    }

    public function add()
    {   
        $allAvailableInMenu = array(
            '1'=>'Header Menu',
            '2'=>'Footer Menu 1',
            '3'=>'Footer Menu 2',
        );
        $allLanguages = DB::table('languages')->where('id','<>', 1)->get();
        $view_data['currPage']          = 'allpages';
        $view_data['hasForm']           = 'yes';
        $view_data['allLanguages']      = $allLanguages;
        $view_data['allAvailableInMenu']= $allAvailableInMenu;
        return view('admin.addpage',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/pages/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $data_array['slug']             = create_slug($request->input('name'),'','pages','slug');
            if(isset($_POST['in_menu'])) $data_array['in_menu'] = implode(',',$request->input('in_menu'));
            else
                $data_array['in_menu'] ='';

            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            
            $currid = DB::table('pages')->insertGetId($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    if(!empty($name) || !empty($content)){
                       DB::table('pages_translations')
                                ->insert(['page_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'created_at'=>$created_at,'updated_at'=>$created_at]);
                    }
                }
            }
            return redirect('admin/pages/')->withMessage('Page has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('pages')->where('id', $id)->first();
        $selectedInMenuArray = array();    
        if ($record != null){
            $allAvailableInMenu = array(
                '1'=>'Header Menu',
                '2'=>'Footer Menu 1',
                '3'=>'Footer Menu 2',
            );
            $selectedInMenu = $record->in_menu;
            if(!empty($selectedInMenu)){
                $selectedInMenuArray = explode(',', $selectedInMenu);
            }
            $allOtherSlide  = DB::table('pages_translations')->where('page_id', $id)->get();   
            $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
            $allOtherLang   = array();
            if(!empty($allOtherSlide)){
                foreach ($allOtherSlide as $singleLang) {
                    $allOtherLang[$singleLang->language]['name'] = $singleLang->name;
                    $allOtherLang[$singleLang->language]['content'] = $singleLang->content;
                }
            }

            $view_data['currPage']          = 'allpages';
            $view_data['hasForm']           = 'yes';
            $view_data['record']            = $record;
            $view_data['selectedInMenu']    = $selectedInMenuArray;
            $view_data['allAvailableInMenu']= $allAvailableInMenu;
            $view_data['allLanguages']      = $allLanguages;
            $view_data['allOtherLang']      = $allOtherLang;
            return view('admin.editpage', $view_data);
        }else{
            return redirect('admin/pages'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            //'content'           => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/pages/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $data_array['slug']             = create_slug($request->input('name'),$currid,'pages','slug');
            if(isset($_POST['in_menu'])) $data_array['in_menu'] = implode(',',$request->input('in_menu'));
            else
                $data_array['in_menu'] ='';
            
            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('pages')
            ->where('id', $currid)
            ->update($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    if(!empty($name) || !empty($content)){
                        $getLangData = DB::table('pages_translations')->where('page_id',$currid)->where('language',$lang)->first();
                        if(!empty($getLangData)){
                            DB::table('pages_translations')
                                ->where('page_id',$currid)->where('language',$lang)
                                ->update(['name'=>$name,'content'=>$content,'updated_at'=>$created_at]);
                        }else{
                            DB::table('pages_translations')
                                ->insert(['page_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'created_at'=>$created_at,'updated_at'=>$created_at]);
                        }
                    }else{
                        DB::table('pages_translations')->where('page_id',$currid)->where('language',$lang)->delete();
                    }
                }
            }
            return redirect('admin/pages/')->withMessage('Page has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('pages')->where('id', $id)->first();        
        if ($record != null){
            DB::table('pages')->where('id', $id)->delete();
            DB::table('pages_translations')->where('page_id', $id)->delete();
            return redirect('admin/pages/')->withMessage('Page has been deleted successfully.');
        }else{
            return redirect('admin/pages'); 
        } 
    }
}