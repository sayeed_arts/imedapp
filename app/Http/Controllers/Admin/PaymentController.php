<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;

class PaymentController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {   
        $allRecords = DB::table('payments')
        ->join('jobs', 'payments.job_id', '=', 'jobs.id')
        ->join('users', 'payments.seeker_id', '=', 'users.id')
        ->select('payments.*', 'jobs.job_title','users.name','users.last_name')
        ->orderby('id', 'DESC')
        ->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allpayments';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allpayments', $view_data);
    }
    public function edit($id)
    {
        $record   = DB::table('payments')->where('id', $id)->first();        
        if ($record != null){
            $view_data['currPage']      = 'allpayments';
            $view_data['hasForm']       = 'yes';
            $view_data['record']        = $record;
            return view('admin.editpayment', $view_data);
        }else{
            return redirect('admin/payments'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/payments/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $data_array['status']           = $request->input('status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('payments')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/payments/')->withMessage('Payment has been updated successfully.');
        }
    }   
}