<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Illuminate\Pagination\Paginator;

use Hash;
use Validator;
use App\Category;

class ProductController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index($id="")
    {   
        $is_admin = '';
        $display_status = '';
        if(isset($_REQUEST['is_admin'])){
            $is_admin = $_REQUEST['is_admin'];
        }
        if(isset($_REQUEST['display_status'])){
            $display_status = $_REQUEST['display_status'];
        }
        $searchTitle = isset($_REQUEST['search'])?$_REQUEST['search']:'';
        $perpage=20;
        $whereArray = array();
       
        if(!empty($searchTitle)){
            $whereArray[]=array( function ($query) use ($searchTitle) {
                $query->where('products.name', 'like', '%'.$searchTitle.'%');
            });
        }
        

        if(!empty($id)){

            $sqlAllRecords = DB::table('products');
            $sqlAllRecords->join('users', 'users.id', '=', 'products.user_id');
            if(!empty($searchTitle)){
                $sqlAllRecords->where('products.name', 'like', '%'.$searchTitle.'%');
            }
            $sqlAllRecords->where('products.user_id',$id);
            $sqlAllRecords->select('products.*', 'users.name as username', 'users.is_admin');
            $sqlAllRecords->orderby('display_order', 'DESC');
            $allRecords = $sqlAllRecords->paginate($perpage);
                
        }
        else{
            $sqlAllRecords = DB::table('products');
            $sqlAllRecords->join('users', 'users.id', '=', 'products.user_id');
            if(!empty($searchTitle)){
                $sqlAllRecords->where('products.name', 'like', '%'.$searchTitle.'%');
            }
            if($is_admin !== ''){
                $sqlAllRecords->where('users.is_admin', '=', $is_admin);
            }
            if($display_status !== ''){
                $sqlAllRecords->where('products.display_status', '=', $display_status);
            }
            $sqlAllRecords->select('products.*', 'users.name as username', 'users.is_admin');
            $sqlAllRecords->orderby('display_order', 'DESC');
            $allRecords = $sqlAllRecords->paginate($perpage);
                   
            
        }
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allproduct';
        $view_data['allRecords']    = $allRecords;
        $view_data['searchTitle']   = $searchTitle;
        $view_data['is_admin']   = $is_admin;
        $view_data['display_status']   = $display_status;
        return view('admin.allproducts', $view_data);
    }

    public function add()
    {   
        $allAvailableProducts= DB::table('products')->orderby('display_order', 'ASC')->get();
        $allManufacturer= DB::table('manufacturer')->orderby('display_order', 'ASC')->get();
        //$allCategory    = DB::table('categories')->where('parent_id',0)->orderby('display_order', 'ASC')->get();
        $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
        $allCategory    = Category::where('parent_id',0)->orderby('display_order','ASC')->get();  
        $allAttributes  = DB::table('attributes')
            ->orderby('display_order', 'ASC')
            ->get();
        $attrArray = array();
        if(!empty($allAttributes)){
            foreach ($allAttributes as $singleAttr) {
                $attrArray[$singleAttr->set_id][]=array(
                    'id'=>$singleAttr->id,
                    'name'=>$singleAttr->name,
                    'values'=>$singleAttr->values
                );
            }
        }

        $allAttributeValues  = DB::table('attribute_values')
            ->select('id','name','set_id','attribute_id')
            ->orderby('Id', 'ASC')
            ->get();
        $attrValuesArray = array();
        if(!empty($allAttributeValues)){
            foreach ($allAttributeValues as $singleAttrValue) {
                $attrValuesArray[$singleAttrValue->attribute_id][]=array(
                    'id'=>$singleAttrValue->id,
                    'set_id'=>$singleAttrValue->set_id,
                    'name'=>$singleAttrValue->name,
                    'attribute_id'=>$singleAttrValue->attribute_id,
                );
            }
        }
        $allAttributeSet = DB::table('attribute_sets')
            ->orderby('display_order', 'ASC')
            ->get();
        $view_data['currPage']          = 'allproduct';
        $view_data['hasForm']           = 'yes';
        $view_data['allManufacturer']   = $allManufacturer;
        $view_data['allCategory']       = $allCategory;
        $view_data['allAvailableProducts']   = $allAvailableProducts;
        $view_data['allLanguages']      = $allLanguages;
        $view_data['allAttributes']     = $allAttributes;
        $view_data['attrArray']         = $attrArray;
        $view_data['attrValuesArray']   = $attrValuesArray;
        $view_data['allAttributeSet']   = $allAttributeSet;  
        $view_data['vendorlist'] = DB::table('users')->where('users.is_admin','=','2')->where('users.is_approved','=','1')->pluck( 'name','id');
             
        return view('admin.addproduct',$view_data);
    }

    public function save(Request $request)
    {
        $requiredArray = array(
            'product_type'      => 'required',
            'name'              => 'required',
            'price'             => "required|numeric",
            'special_price'     => "nullable|numeric",
            'sku'               => 'required|unique:products,sku',
            'image'             => 'required|mimes:jpeg,png,jpg,gif|max:2048',
        );
        if($request->input('inventory_management')==1){
            $requiredArray['qty']='required|integer|min:1';
        }
        $validator = Validator::make($request->all(),$requiredArray);
        
        if ($validator->fails()) {
            return redirect('admin/products/add')->withErrors($validator)->withInput();
        }else{            
            $created_at     = currentDBDate(); 
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                }else{
                    $image_path         = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath    = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;
                }
            }
            $imagesArray = array();
            if($request->hasFile('additional_images'))
            {
                $allowedfileExtension=['jpg','jpeg','png','gif'];
                $allFiles           = $request->file('additional_images');
                $destinationPath1    = public_path('/uploaded_images');
                foreach($allFiles as $singleFile){
                    $path = Storage::disk('s3')->put('uploaded_images', $singleFile);
                    if(strpos($path, 'uploaded_images') !== false){
                       $imagesArray[]   = s3Path().$path;
                    }else{
                        $image_path1   = randomFilename(15).'.'.$singleFile->getClientOriginalExtension();
                        $extension     = $singleFile->getClientOriginalExtension();
                        $check=in_array($extension,$allowedfileExtension);
                        if($check)
                        {
                            $singleFile->move($destinationPath1, $image_path1);
                            $imagesArray[]   = 'uploaded_images/'.$image_path1;
                        }
                    }
                }
            }
            if(!empty($imagesArray)){
                $data_array['additional_images'] = implode("##~~##", $imagesArray);
            }
            $data_array['slug']         = create_slug($request->input('name'),'','products','slug');
            $data_array['product_type']         = $request->input('product_type');
           if(!empty($request->input('user_id'))){
            $data_array['user_id']              = $request->input('user_id');
           }
            $data_array['name']                 = $request->input('name');
            $data_array['short_description']    = $request->input('short_description');
            $data_array['content']              = $request->input('content');
            $data_array['more_info']            = $request->input('more_info');
            $data_array['related_products']     = (isset($_POST['related_products']))?@implode(',', $_POST['related_products']):'';
            $data_array['manufacturer_id']      = $request->input('manufacturer_id');
            $data_array['category_id']          = (isset($_POST['category_id']))?@implode(',', $_POST['category_id']):'';
            //$data_array['child_category_id']    = $request->input('child_category_id');
            $data_array['price']                = $request->input('price');
            $data_array['special_price']        = $request->input('special_price');
            $data_array['sku']                  = $request->input('sku');
            $data_array['inventory_management'] = $request->input('inventory_management');

            $data_array['qty']                  = $request->input('qty');
            $data_array['total_products']       = $request->input('qty');
            $data_array['stock_availability']   = $request->input('stock_availability');
            if($request->input('inventory_management')==0){
                $data_array['stock_availability']=1;
            }
            $data_array['meta_title']           = $request->input('meta_title');
            $data_array['meta_key']             = $request->input('meta_key');
            $data_array['meta_desc']            = $request->input('meta_desc');
            $data_array['display_order']        = $request->input('display_order');
            $data_array['display_status']       = $request->input('display_status');
            $data_array['is_featured']          = $request->input('is_featured');
            $data_array['new_date']             = $request->input('new_date');
            $data_array['created_at']           = $created_at;
            $data_array['updated_at']           = $created_at;            
            

            $currid = DB::table('products')->insertGetId($data_array);
            
            $variation_attributes = $request->input('variation_attributes');
            if(isset($_POST['selectedvarattrval'])){
                foreach ($_POST['selectedvarattrval'] as $key => $attribute_value) {
                    if(!empty($attribute_value)){
                        $keyval = array_search($attribute_value, $_POST['hiddenIds']);
                        $attribute_price                = (isset($_POST['selectedattrvalprice'][$keyval]))?$_POST['selectedattrvalprice'][$keyval]:'0';
                        
                        $attrArray                      = array();
                        $attrArray['product_id']        = $currid;
                        $attrArray['attribute_id']      = $variation_attributes;
                        $attrArray['attribute_value']   = $attribute_value;
                        $attrArray['attribute_price']   = $attribute_price;
                        $attrArray['is_variation']      = 1;
                        $attrArray['created_at']        = $created_at;
                        $attrArray['updated_at']        = $created_at; 
                        DB::table('products_attributes')->insertGetId($attrArray);
                    }                    
                }
            }
            if(isset($_POST['selectedattrval'])){
                foreach ($_POST['selectedattrval'] as $key => $attribute_value) {
                    if(!empty($attribute_value)){
                        $attrExp                        = explode('__', $attribute_value);
                        $attrArray                      = array();
                        $attrArray['product_id']        = $currid;
                        $attrArray['attribute_id']      = (isset($attrExp[0]))?$attrExp[0]:'';
                        $attrArray['attribute_value']   = (isset($attrExp[1]))?trim($attrExp[1]):'';
                        $attrArray['attribute_price']   = 0;
                        $attrArray['is_variation']      = 0;
                        $attrArray['created_at']        = $created_at;
                        $attrArray['updated_at']        = $created_at; 
                        DB::table('products_attributes')->insertGetId($attrArray);
                    }                    
                }
            }
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name               = $request->input('name_'.$lang);
                    $content            = $request->input('content_'.$lang);
                    $short_description  = $request->input('short_description_'.$lang);
                    $more_info          = $request->input('more_info_'.$lang);
                    if(!empty($name) || !empty($content) || !empty($short_description) || !empty($more_info)){
                       DB::table('products_translations')
                                ->insert(['product_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'short_description'=>$short_description,'more_info'=>$more_info,'created_at'=>$created_at,'updated_at'=>$created_at]);
                    }
                }
            }
            return redirect('admin/products/')->withMessage('Product has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('products')->where('id', $id)->first();        
        if ($record != null){
            $selectedFeaturedProducts = @explode(',', $record->related_products);
            $allAvailableProducts   = DB::table('products')->where('id','<>',$id)->orderby('display_order', 'ASC')->get();
            $allManufacturer        = DB::table('manufacturer')->orderby('display_order', 'ASC')->get();
            //$allCategory          = DB::table('categories')->where('parent_id',0)->orderby('display_order', 'ASC')->get();
            $allCategory            = Category::where('parent_id',0)->orderby('display_order','ASC')->get(); 

            //$allSubCategory     = DB::table('categories')->where('parent_id',$record->category_id)->orderby('display_order', 'ASC')->get();

            $allOtherSlide      = DB::table('products_translations')->where('product_id', $id)->get();   
            $allLanguages       = DB::table('languages')->where('id','<>', 1)->get();
            $allOtherLang       = array();
            if(!empty($allOtherSlide)){
                foreach ($allOtherSlide as $singleLang) {
                    $allOtherLang[$singleLang->language]['name'] = $singleLang->name;
                    $allOtherLang[$singleLang->language]['content'] = $singleLang->content;
                    $allOtherLang[$singleLang->language]['short_description'] = $singleLang->short_description;
                    $allOtherLang[$singleLang->language]['more_info'] = $singleLang->more_info;
                }
            }
            $allAttributes  = DB::table('attributes')
                ->orderby('display_order', 'ASC')
                ->get();
            $attrArray = array();
            if(!empty($allAttributes)){
                foreach ($allAttributes as $singleAttr) {
                    $attrArray[$singleAttr->set_id][]=array(
                        'id'=>$singleAttr->id,
                        'name'=>$singleAttr->name,
                        'values'=>$singleAttr->values
                    );
                }
            }

            $attrArrayEdit = array();
            if(!empty($allAttributes)){
                foreach ($allAttributes as $singleAttr) {
                    $attrArrayEdit[$singleAttr->id]=array(
                        'id'=>$singleAttr->id,
                        'name'=>$singleAttr->name,
                        'values'=>$singleAttr->values
                    );
                }
            }

            $allAttributeValues  = DB::table('attribute_values')
            ->select('id','name','set_id','attribute_id')
            ->orderby('Id', 'ASC')
            ->get();
            $attrValuesArray = array();
            if(!empty($allAttributeValues)){
                foreach ($allAttributeValues as $singleAttrValue) {
                    $attrValuesArray[$singleAttrValue->attribute_id][]=array(
                        'id'=>$singleAttrValue->id,
                        'set_id'=>$singleAttrValue->set_id,
                        'name'=>$singleAttrValue->name,
                        'attribute_id'=>$singleAttrValue->attribute_id,
                    );
                }
            }
            $attrValuesIdArray = array();
            if(!empty($allAttributeValues)){
                foreach ($allAttributeValues as $singleAttrValue) {
                    $attrValuesIdArray[$singleAttrValue->id]=$singleAttrValue->name;
                }
            }

            $allAttributeSet = DB::table('attribute_sets')
                ->orderby('display_order', 'ASC')
                ->get();

            $productVariAttr = DB::table('products_attributes')
                ->where('products_attributes.product_id', $record->id)
                ->where('products_attributes.is_variation', 1)
                ->orderby('products_attributes.id', 'ASC')
                ->get();
            $productAttr = DB::table('products_attributes')
                ->where('product_id', $record->id)
                ->where('is_variation', 0)
                ->orderby('id', 'ASC')
                ->get();
            $allVariAttr        = array();
            $allVariPriceAttr        = array();
            $variationAttrId    = '';
            if(!empty($productVariAttr)){
                foreach ($productVariAttr as $singleVariAttr) {
                    $allVariAttr[$singleVariAttr->attribute_id]=$singleVariAttr->attribute_id;
                    $variationAttrId = $singleVariAttr->attribute_id;
                }
            }
            if(!empty($productVariAttr)){
                foreach ($productVariAttr as $singleVariAttr) {
                    $allVariPriceAttr[$singleVariAttr->attribute_value]=$singleVariAttr->attribute_price;
                }
            }

            $variationAllOption = (isset($attrValuesArray[$variationAttrId]))?$attrValuesArray[$variationAttrId]:array();
            $allOtherAttr = array();
            if(!empty($productAttr)){
                foreach ($productAttr as $singleProAttr) {
                    $allOtherAttr[$singleProAttr->attribute_id]=$singleProAttr->attribute_id;
                }
            }
            $allOtherAttrSelected = array();
            if(!empty($productAttr)){
                foreach ($productAttr as $singleProAttr) {
                    $allOtherAttrSelected[$singleProAttr->attribute_id][]=$singleProAttr->attribute_value;
                }
            }
            $view_data['currPage']          = 'allproduct';
            $view_data['hasForm']           = 'yes';
            $view_data['record']            = $record;            
            $view_data['allManufacturer']   = $allManufacturer;
            $view_data['allCategory']       = $allCategory;
            //$view_data['allSubCategory']    = $allSubCategory;
            $view_data['allLanguages']      = $allLanguages;
            $view_data['allOtherLang']      = $allOtherLang;
            $view_data['allAvailableProducts']= $allAvailableProducts;
            $view_data['selectedFeaturedProducts']= $selectedFeaturedProducts;
            $view_data['allAttributes']     = $allAttributes;
            $view_data['attrArray']         = $attrArray;
            $view_data['allAttributeSet']   = $allAttributeSet;  
            $view_data['productVariAttr']   = $productVariAttr;  
            $view_data['productAttr']       = $productAttr;  
            $view_data['allVariAttr']       = $allVariAttr;  
            $view_data['allOtherAttr']      = $allOtherAttr;
            $view_data['allOtherAttrSelected']= $allOtherAttrSelected;
            $view_data['attrArrayEdit']     = $attrArrayEdit;
            $view_data['allAttributeValues']= $allAttributeValues;
            $view_data['attrValuesArray']   = $attrValuesArray;
            $view_data['attrValuesIdArray']   = $attrValuesIdArray;
            $view_data['allVariPriceAttr']   = $allVariPriceAttr;
            $view_data['variationAllOption']   = $variationAllOption;
            $view_data['vendorlist'] = DB::table('users')->where('users.is_admin','=','2')->where('users.is_approved','=','1')->pluck( 'name','id');
            
            return view('admin.editproduct', $view_data);
        }else{
            return redirect('admin/products'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $requiredArray = array(
            'product_type'      => 'required',
            'name'              => 'required',
            //'display_order'     => 'nullable|numeric|min:1',
            'price'             => "required|numeric",
            'special_price'     => "nullable|numeric",
            'sku'               => 'required|unique:products,sku,'.$currid,
            'image'             => 'nullable|mimes:jpeg,png,jpg,gif|max:2048',
        );
        if($request->input('inventory_management')==1){
            $requiredArray['qty']='required|integer|min:1';
        }
        $validator = Validator::make($request->all(),$requiredArray);
        
        if ($validator->fails()) {
            return redirect('admin/products/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $imagesArray = array();
            $created_at                     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                    $old_path = $request->input('old_image');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    }  
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_image'));
                }
            }
            
            if($request->hasFile('additional_images'))
            {
                $allowedfileExtension=['jpg','jpeg','png','gif'];
                $allFiles           = $request->file('additional_images');
                $destinationPath1    = public_path('/uploaded_images');
                foreach($allFiles as $singleFile){
                    $path = Storage::disk('s3')->put('uploaded_images', $singleFile);
                    if(strpos($path, 'uploaded_images') !== false){
                        $imagesArray[]   = s3Path().$path;
                    }else{
                        $image_path1   = randomFilename(15).'.'.$singleFile->getClientOriginalExtension();
                        $extension     = $singleFile->getClientOriginalExtension();
                        $check=in_array($extension,$allowedfileExtension);
                        if($check)
                        {
                            $singleFile->move($destinationPath1, $image_path1);
                            $imagesArray[]   = 'uploaded_images/'.$image_path1;
                        }
                    }
                }
            }
            $old_additional_images = $request->input('old_additional_images');
            if(!empty($old_additional_images)){
                $all_add_images = $request->input('all_add_images');
                $oldAddImages = explode('##~~##', $old_additional_images);
                foreach ($oldAddImages as $imageUrl) {
                    if(@in_array($imageUrl, $all_add_images)){
                        $imagesArray[]=$imageUrl;
                    }else{
                        $old_path = $imageUrl;
                        if(!empty($old_path)){
                            $old_path = str_replace(s3Path(), '', $old_path);
                            Storage::disk('s3')->delete($old_path);
                        }  
                        @unlink($imageUrl);
                    }
                }
            }
            if(!empty($imagesArray)){
                $data_array['additional_images'] = implode("##~~##", $imagesArray);
            }
            $old_qty        = $request->input('old_qty');
            $new_qty        = $request->input('qty');
            $total_products = $request->input('total_products');
            if($new_qty<$old_qty){
                $diff = $old_qty-$new_qty;
                $total_products = $total_products-$diff;
            }elseif($new_qty>$old_qty){
                $diff = $new_qty - $old_qty;
                $total_products = $total_products+$diff;
            }

            $data_array['total_products']       = $total_products;
            $data_array['slug']                 = create_slug($request->input('name'),$currid,'products','slug');
            $data_array['product_type']         = $request->input('product_type');
          if(!empty($request->input('user_id'))){
            $data_array['user_id']              = $request->input('user_id');
           }
            $data_array['name']                 = $request->input('name');
            $data_array['short_description']    = $request->input('short_description');
            $data_array['content']              = $request->input('content');
            $data_array['more_info']            = $request->input('more_info');
            $data_array['related_products']     = (isset($_POST['related_products']))?@implode(',', $_POST['related_products']):'';
            $data_array['manufacturer_id']      = $request->input('manufacturer_id');
            $data_array['category_id']          = (isset($_POST['category_id']))?@implode(',', $_POST['category_id']):'';
            //$data_array['child_category_id']    = $request->input('child_category_id');
            $data_array['price']                = $request->input('price');
            $data_array['special_price']        = $request->input('special_price');
            $data_array['sku']                  = $request->input('sku');
            $data_array['inventory_management'] = $request->input('inventory_management');
            $data_array['qty']                  = $request->input('qty');
            $data_array['stock_availability']   = $request->input('stock_availability');
            if($request->input('inventory_management')==0){
                $data_array['stock_availability']=1;
            }
            $data_array['meta_title']           = $request->input('meta_title');
            $data_array['meta_key']             = $request->input('meta_key');
            $data_array['meta_desc']            = $request->input('meta_desc');
            $data_array['display_order']        = $request->input('display_order');
            $data_array['display_status']       = $request->input('display_status');
            $data_array['is_featured']          = $request->input('is_featured');
            $data_array['new_date']             = $request->input('new_date');
            $data_array['updated_at']           = $created_at;  
            
            DB::table('products')
            ->where('id', $currid)
            ->update($data_array);

            DB::table('products_attributes')
            ->where('product_id', $currid)
            ->delete();

            $variation_attributes = $request->input('variation_attributes');
            if(isset($_POST['selectedvarattrval'])){
                foreach ($_POST['selectedvarattrval'] as $key => $attribute_value) {
                    if(!empty($attribute_value)){
                        $keyval = array_search($attribute_value, $_POST['hiddenIds']);
                        $attribute_price                = (isset($_POST['selectedattrvalprice'][$keyval]))?$_POST['selectedattrvalprice'][$keyval]:'0';
                        $attrArray                      = array();
                        $attrArray['product_id']        = $currid;
                        $attrArray['attribute_id']      = $variation_attributes;
                        $attrArray['attribute_value']   = $attribute_value;
                        $attrArray['attribute_price']   = $attribute_price;
                        $attrArray['is_variation']      = 1;
                        $attrArray['created_at']        = $created_at;
                        $attrArray['updated_at']        = $created_at; 
                       
                        DB::table('products_attributes')->insertGetId($attrArray);
                    }                    
                }
            }
            if(isset($_POST['selectedattrval'])){
                foreach ($_POST['selectedattrval'] as $key => $attribute_value) {
                    if(!empty($attribute_value)){
                        $attrExp                        = explode('__', $attribute_value);
                        $attrArray                      = array();
                        $attrArray['product_id']        = $currid;
                        $attrArray['attribute_id']      = (isset($attrExp[0]))?$attrExp[0]:'';
                        $attrArray['attribute_value']   = (isset($attrExp[1]))?trim($attrExp[1]):'';
                        $attrArray['attribute_price']   = 0;
                        $attrArray['is_variation']      = 0;
                        $attrArray['created_at']        = $created_at;
                        $attrArray['updated_at']        = $created_at; 
                        DB::table('products_attributes')->insertGetId($attrArray);
                    }                    
                }
            }

            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name               = $request->input('name_'.$lang);
                    $content            = $request->input('content_'.$lang);
                    $short_description  = $request->input('short_description_'.$lang);
                    $more_info          = $request->input('more_info_'.$lang);
                    if(!empty($name) || !empty($content) || !empty($short_description) || !empty($more_info)){
                        $getLangData = DB::table('products_translations')->where('product_id',$currid)->where('language',$lang)->first();
                        if(!empty($getLangData)){
                            DB::table('products_translations')
                                ->where('product_id',$currid)->where('language',$lang)
                                ->update(['name'=>$name,'content'=>$content,'short_description'=>$short_description,'more_info'=>$more_info,'updated_at'=>$created_at]);
                        }else{
                            DB::table('products_translations')
                                ->insert(['product_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'short_description'=>$short_description,'more_info'=>$more_info,'created_at'=>$created_at,'updated_at'=>$created_at]);
                        }
                    }else{
                        DB::table('products_translations')->where('product_id',$currid)->where('language',$lang)->delete();
                    }
                }
            }
            return redirect('admin/products/')->withMessage('Product has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('products')->where('id', $id)->first();        
        if ($record != null){
            DB::table('products')->where('id', $id)->delete();            
            DB::table('products_translations')->where('product_id', $id)->delete();
            $old_path = $record->image;
            if(!empty($old_path)){
                $old_path = str_replace(s3Path(), '', $old_path);
                Storage::disk('s3')->delete($old_path);
                @unlink($old_path);
            } 

            $old_additional_images = $record->additional_images;
            if(!empty($old_additional_images)){
                $oldAddImages = explode('##~~##', $old_additional_images);
                foreach ($oldAddImages as $imageUrl) {                    
                    $old_path = $imageUrl;
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    }  
                    @unlink($imageUrl);
                }
            }
            return redirect('admin/products/')->withMessage('Product has been deleted successfully.');
        }else{
            return redirect('admin/products'); 
        } 
    } 
    public function getSubCat($id=''){
        if(!empty($id)){
            $allCats = DB::table('categories')->select('id','name')->where(array('parent_id'=>$id))->orderby('id')->get();
            if(!empty($allCats)){
                $subCats = "<option value=\"\">Select</option>";
                foreach ($allCats as $singleCat) {
                    $subCats .= "<option value=\"".$singleCat->id."\">".$singleCat->name."</option>";
                }
            }else{
                $subCats = "<option value=\"\">Select</option>";
            }
        }else{
            $subCats = "<option value=\"\">Select</option>";
        }        
        echo $subCats;        
    }

    public function getProducts($id=''){
        $products = "";
        if(!empty($id)){
            $allProducts = DB::table('products')
            ->select('id','name')
            ->whereRaw('FIND_IN_SET('.$id.',category_id) AND display_status = 1 ')
            ->get();
            if(!empty($allProducts)){
                $products .= "<input type='checkbox' id='chkallboxs' name='chkallproducts'  value='1'>&nbsp; Select All<br>";
                foreach ($allProducts as $singleProd) {
                    $products .= "<input type='checkbox' class='chkboxs' name='products[]'  value=\"".$singleProd->id."\">&nbsp;".$singleProd->name."<br>";
                }
            }else{
                $products = "noproducts";
            }
        }else{
            $products = "noproducts";
        }        
        echo $products;        
    }

    public function copyProducts()
    {   
        
        $allCategory    = Category::where('parent_id',0)->where('display_status',1)->orderby('display_order','ASC')->get();  
        
        $view_data['currPage']          = 'copyproducts';
        $view_data['hasForm']           = 'yes';
        $view_data['allCategory']       = $allCategory;
        return view('admin.copyProducts',$view_data);
    }
    public function copySave(Request $request)
    {
        $requiredArray = array(
            'from_category'      => 'required',
            'to_category'        => 'required'
        );
        $validator = Validator::make($request->all(),$requiredArray);
        $from_category  = $request->input('from_category');
        $to_category    = $request->input('to_category');
        $act1    = $request->input('act1');
        $act2    = $request->input('act2');
        $products    = $request->input('products');
        
        $action = "";
        if($act1){
            $action = $act1;
        }
        if($act2){
            $action = $act2;
        }
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }elseif($to_category==$from_category){
            return redirect()->back()->withMessage('Products have been updated successfully.');
        }else{
            $created_at     = currentDBDate(); 

            if($products){
                $num = 0;
                foreach ($products as $singleProduct) {
                    $productDetails = DB::table('products')
                    ->select('category_id')
                    ->where('id',$singleProduct)
                    ->first();

                    $categoryIdsArray   = explode(',', $productDetails->category_id);
                    if($action == 'Copy'){
                        if(!in_array($to_category, $categoryIdsArray)){
                            array_push($categoryIdsArray, $to_category);
                            
                            $newCats = implode(',', $categoryIdsArray);
                            db::table('products')
                            ->where('id',$singleProduct)
                            ->update(['category_id'=>$newCats]);
                        }
                    }
                    if($action == 'Move'){
                        //var_dump($categoryIdsArray);
                        if (($key = array_search($from_category, $categoryIdsArray)) !== false) {
                            unset($categoryIdsArray[$key]);
                        }
                        //dd($categoryIdsArray);
                        if(!in_array($to_category, $categoryIdsArray)){
                            array_push($categoryIdsArray, $to_category);
                            $newCats = implode(',', $categoryIdsArray);
                            db::table('products')
                            ->where('id',$singleProduct)
                            ->update(['category_id'=>$newCats]);
                        }
                    }
                    $num++;
                }
                
            } else{
                return redirect()->back()->withError('Please choose products.');
            }
            return redirect()->back()->withMessage('Products have been updated successfully.');
        }
    }
    
}