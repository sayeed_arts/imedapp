<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;
use Hash;
use Validator;

class ProjectsController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allRecords = DB::table('projects')->orderby('display_order', 'ASC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allprojects';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allProjects', $view_data);
    }

    public function add()
    {
        $allLanguages               = DB::table('languages')->where('id','<>', 1)->get();
        $view_data['currPage']      = 'allprojects';
        $view_data['hasForm']       = 'yes';
        $view_data['allLanguages']  = $allLanguages;
        return view('admin.addProject',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'image'             => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'start_date'        => 'required',
            'end_date'          => 'required',
            'client_info'       => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/projects/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                }else{
                    $image_path         = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath    = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;
                }
            }
            $data_array['slug']             = create_slug($request->input('name'),'','projects','slug');
            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['type']             = $request->input('type');
            $data_array['start_date']       = $request->input('start_date');
            $data_array['end_date']         = $request->input('end_date');
            $data_array['client_info']      = $request->input('client_info');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            
            $currid = DB::table('projects')->insertGetId($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    $client_info    = $request->input('client_info_'.$lang);
                    if(!empty($name) || !empty($content) || !empty($client_info)){
                       DB::table('projects_translations')
                                ->insert(['project_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'client_info'=>$client_info,'created_at'=>$created_at,'updated_at'=>$created_at]);
                    }
                }
            }
            return redirect('admin/projects/')->withMessage('Project has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('projects')->where('id', $id)->first();        
        if ($record != null){
            $allOtherSlide  = DB::table('projects_translations')->where('project_id', $id)->get();   
            $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
            $allOtherLang   = array();
            if(!empty($allOtherSlide)){
                foreach ($allOtherSlide as $singleLang) {
                    $allOtherLang[$singleLang->language]['name'] = $singleLang->name;
                    $allOtherLang[$singleLang->language]['content'] = $singleLang->content;
                    $allOtherLang[$singleLang->language]['client_info'] = $singleLang->client_info;
                }
            }
            $view_data['currPage']      = 'allprojects';
            $view_data['hasForm']       = 'yes';
            $view_data['record']        = $record;
            $view_data['allLanguages']  = $allLanguages;
            $view_data['allOtherLang']  = $allOtherLang;
            return view('admin.editProject', $view_data);
        }else{
            return redirect('admin/projects'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'image'             => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'start_date'        => 'required',
            'end_date'          => 'required',
            'client_info'       => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/projects/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                    $old_path = $request->input('old_image');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    } 
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_image'));
                }
            }
            $data_array['slug']             = create_slug($request->input('name'),$currid,'projects','slug');
            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['type']             = $request->input('type');
            $data_array['start_date']       = $request->input('start_date');
            $data_array['end_date']         = $request->input('end_date');
            $data_array['client_info']      = $request->input('client_info');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('projects')
            ->where('id', $currid)
            ->update($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    $client_info    = $request->input('client_info_'.$lang);
                    if(!empty($name) || !empty($content) || !empty($client_info)){
                        $getLangData = DB::table('projects_translations')->where('project_id',$currid)->where('language',$lang)->first();
                        if(!empty($getLangData)){
                            DB::table('projects_translations')
                                ->where('project_id',$currid)->where('language',$lang)
                                ->update(['name'=>$name,'content'=>$content,'client_info'=>$client_info,'updated_at'=>$created_at]);
                        }else{
                            DB::table('projects_translations')
                                ->insert(['project_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'client_info'=>$client_info,'created_at'=>$created_at,'updated_at'=>$created_at]);
                        }
                    }else{
                        DB::table('projects_translations')->where('project_id',$currid)->where('language',$lang)->delete();
                    }
                }
            }
            return redirect('admin/projects/')->withMessage('Project has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('projects')->where('id', $id)->first();        
        if ($record != null){
            DB::table('projects')->where('id', $id)->delete();
            DB::table('projects_translations')->where('project_id', $id)->delete();
            $old_path = $record->image;
            if(!empty($old_path)){
                $old_path = str_replace(s3Path(), '', $old_path);
                Storage::disk('s3')->delete($old_path);
                @unlink($old_path);
            } 

            return redirect('admin/projects/')->withMessage('Project has been deleted successfully.');
        }else{
            return redirect('admin/projects'); 
        } 
    }    
}