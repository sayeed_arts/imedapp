<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Hash;
use Validator;

class ServiceAreasController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {
        $allRecords = DB::table('serviceareas')->orderby('display_order', 'ASC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allserviceareas';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allServiceAreas', $view_data);
    }
    public function add()
    {
        $allLanguages = DB::table('languages')->where('id','<>', 1)->get();
        $view_data['countrylist'] = DB::table('countries')->pluck( 'countryName','countryID');
        
        $view_data['currPage']      = 'allserviceareas';
        $view_data['hasForm']       = 'yes';
        $view_data['allLanguages']  = $allLanguages;
        return view('admin.addServiceArea',$view_data);
    }
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'country'           => 'required',
            'state'             => 'required',
            'cityname'          => 'required',
            'content'           => 'required',
            'image'             => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/serviceareas/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                }else{
                    $image_path         = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath    = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;
                }
            }
            $data_array['slug']             = create_slug($request->input('cityname'),'','serviceareas','slug');
            $data_array['cityname']         = $request->input('cityname');
            $data_array['content']          = $request->input('content');
            $data_array['country']          = $request->input('country');
            $data_array['state']            = $request->input('state');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            
            $currid = DB::table('serviceareas')->insertGetId($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $cityname       = $request->input('cityname_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    if(!empty($cityname) || !empty($content)){
                       DB::table('serviceareas_translations')
                                ->insert(['serviceareas_id'=>$currid,'language'=>$lang,'cityname'=>$cityname,'content'=>$content,'created_at'=>$created_at,'updated_at'=>$created_at]);
                    }
                }
            }
            return redirect('admin/serviceareas/')->withMessage('Service area has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('serviceareas')->where('id', $id)->first();        
        if ($record != null){
            $allOtherSlide  = DB::table('serviceareas_translations')->where('serviceareas_id', $id)->get();   
            $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
            $allOtherLang   = array();
            if(!empty($allOtherSlide)){
                foreach ($allOtherSlide as $singleLang) {
                    $allOtherLang[$singleLang->language]['cityname'] = $singleLang->name;
                    $allOtherLang[$singleLang->language]['content'] = $singleLang->content;
                }
            }
            
            $view_data['countrylist'] = DB::table('countries')->pluck( 'countryName','countryID');
            $view_data['allState'] = DB::table('states')->select('stateID','stateName','countryID')->where(array('status'=>'1','countryID'=>$record->country))->orderby('stateName')->get();
            $view_data['currPage']      = 'allserviceareas';
            $view_data['hasForm']       = 'yes';
            $view_data['record']        = $record;
            $view_data['allLanguages']  = $allLanguages;
            $view_data['allOtherLang']  = $allOtherLang;
            return view('admin.editServiceArea', $view_data);
        }else{
            return redirect('admin/serviceareas'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'country'           => 'required',
            'state'             => 'required',
            'cityname'          => 'required',
            'content'           => 'required',
            'image'             => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/serviceareas/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                    $old_path = $request->input('old_image');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    } 
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_image'));
                }
            }
            $data_array['slug']             = create_slug($request->input('cityname'),'','serviceareas','slug');
            $data_array['cityname']         = $request->input('cityname');
            $data_array['content']          = $request->input('content');
            $data_array['country']          = $request->input('country');
            $data_array['state']            = $request->input('state');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('serviceareas')
            ->where('id', $currid)
            ->update($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $cityname       = $request->input('cityname_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    if(!empty($name) || !empty($content)){
                        $getLangData = DB::table('serviceareas_translations')->where('serviceareas_id',$currid)->where('language',$lang)->first();
                        if(!empty($getLangData)){
                            DB::table('serviceareas_translations')
                                ->where('serviceareas_id',$currid)->where('language',$lang)
                                ->update(['cityname'=>$cityname,'content'=>$content,'updated_at'=>$created_at]);
                        }else{
                            DB::table('serviceareas_translations')
                                ->insert(['serviceareas_id'=>$currid,'language'=>$lang,'cityname'=>$cityname,'content'=>$content,'created_at'=>$created_at,'updated_at'=>$created_at]);
                        }
                    }else{
                        DB::table('serviceareas_translations')->where('serviceareas_id',$currid)->where('language',$lang)->delete();
                    }
                }
            }
            return redirect('admin/serviceareas/')->withMessage('Service area has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('serviceareas')->where('id', $id)->first();        
        if ($record != null){
            DB::table('serviceareas')->where('id', $id)->delete();            
            DB::table('serviceareas_translations')->where('serviceareas_id', $id)->delete();
            $old_path = $record->image;
            if(!empty($old_path)){
                $old_path = str_replace(s3Path(), '', $old_path);
                Storage::disk('s3')->delete($old_path);
                @unlink($old_path);
            } 
            return redirect('admin/serviceareas/')->withMessage('Service area has been deleted successfully.');
        }else{
            return redirect('admin/serviceareas'); 
        } 
    }
}