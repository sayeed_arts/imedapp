<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Hash;
use Validator;

class ServiceCategoryController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {
        $allRecords = DB::table('service_category')->orderby('display_order', 'ASC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allservicecategory';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allServiceCategory', $view_data);
    }
    public function add()
    {
        $allServices = DB::table('services')->orderby('display_order', 'ASC')->get();
        $allLanguages = DB::table('languages')->where('id','<>', 1)->get();
        $view_data['currPage']      = 'allservicecategory';
        $view_data['hasForm']       = 'yes';
        $view_data['allLanguages']  = $allLanguages;
        $view_data['allServices']   = $allServices;
        return view('admin.addServiceCategory',$view_data);
    }
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'image'             => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/service-category/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                }else{
                    $image_path         = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath    = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;
                }
            }
            $data_array['slug']             = create_slug($request->input('name'),'','service_category','slug');
            
            $data_array['services']         = '';//(isset($_POST['services']))?@implode(',', $_POST['services']):'';
            $data_array['name']             = $request->input('name');
            $data_array['offered']          = $request->input('offered');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;

            $currid = DB::table('service_category')->insertGetId($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $offered        = $request->input('offered_'.$lang);
                    if(!empty($name) || !empty($content) || !empty($offered)){
                       DB::table('service_category_translations')
                                ->insert(['services_id'=>$currid,'language'=>$lang,'name'=>$name,'offered'=>$offered,'created_at'=>$created_at,'updated_at'=>$created_at]);
                    }
                }
            }
            return redirect('admin/service-category/')->withMessage('Service category has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('service_category')->where('id', $id)->first();        
        if ($record != null){
            $allServices = DB::table('services')->orderby('display_order', 'ASC')->get();
            $allOtherSlide  = DB::table('service_category_translations')->where('services_id', $id)->get();   
            $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
            $allOtherLang   = array();
            if(!empty($allOtherSlide)){
                foreach ($allOtherSlide as $singleLang) {
                    $allOtherLang[$singleLang->language]['name'] = $singleLang->name;
                    $allOtherLang[$singleLang->language]['offered'] = $singleLang->offered;
                }
            }

            $view_data['currPage']      = 'allservicecategory';
            $view_data['hasForm']       = 'yes';
            $view_data['record']        = $record;
            $view_data['allLanguages']  = $allLanguages;
            $view_data['allOtherLang']  = $allOtherLang;
            $view_data['allServices']  = $allServices;
            $view_data['selectedServices']  = explode(',', $record->services);
            return view('admin.editServiceCategory', $view_data);
        }else{
            return redirect('admin/service-category'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator              = Validator::make($request->all(), [
            'name'              => 'required',
            'image'             => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/service-category/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                    $old_path = $request->input('old_image');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    } 
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_image'));
                }
            }
            $data_array['slug']             = create_slug($request->input('name'),$currid,'service_category','slug');
            $data_array['services']          = '';//(isset($_POST['services']))?@implode(',', $_POST['services']):'';
            $data_array['name']             = $request->input('name');
            $data_array['offered']          = $request->input('offered');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('service_category')
            ->where('id', $currid)
            ->update($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $offered        = $request->input('offered_'.$lang);
                    if(!empty($name) || !empty($offered)){
                        $getLangData = DB::table('service_category_translations')->where('services_id',$currid)->where('language',$lang)->first();
                        if(!empty($getLangData)){
                            DB::table('service_category_translations')
                                ->where('services_id',$currid)->where('language',$lang)
                                ->update(['name'=>$name,'offered'=>$offered,'updated_at'=>$created_at]);
                        }else{
                            DB::table('service_category_translations')
                                ->insert(['services_id'=>$currid,'language'=>$lang,'name'=>$name,'offered'=>$offered,'created_at'=>$created_at,'updated_at'=>$created_at]);
                        }
                    }else{
                        DB::table('service_category_translations')->where('services_id',$currid)->where('language',$lang)->delete();
                    }
                }
            }
            return redirect('admin/service-category/')->withMessage('Service category has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('service_category')->where('id', $id)->first();        
        if ($record != null){
            DB::table('service_category')->where('id', $id)->delete();            
            DB::table('service_category_translations')->where('services_id', $id)->delete();
            $old_path   = $record->image;
            if(!empty($old_path)){
                $old_path = str_replace(s3Path(), '', $old_path);
                Storage::disk('s3')->delete($old_path);
                @unlink($old_path);
            } 
            return redirect('admin/service-category/')->withMessage('Service category has been deleted successfully.');
        }else{
            return redirect('admin/service-category'); 
        } 
    }
}