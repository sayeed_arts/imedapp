<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Hash;
use Validator;

class ServicesController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {
        $allRecords = DB::table('services')->orderby('display_order', 'ASC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allservices';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allServices', $view_data);
    }
    public function add()
    {
        $allLanguages = DB::table('languages')->where('id','<>', 1)->get();
        $view_data['currPage']      = 'allservices';
        $view_data['hasForm']       = 'yes';
        $view_data['allLanguages']  = $allLanguages;
        return view('admin.addService',$view_data);
    }
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'image'             => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/services/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                }else{
                    $image_path         = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath    = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;
                }
            }
            $data_array['slug']             = create_slug($request->input('name'),'','services','slug');
            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['external_link']    = $request->input('external_link');
            $data_array['offered']          = '';//$request->input('offered');
            $data_array['display_form']     = $request->input('display_form');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;

            $currid = DB::table('services')->insertGetId($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    $offered        = '';//$request->input('offered_'.$lang);
                    if(!empty($name) || !empty($content) || !empty($offered)){
                       DB::table('services_translations')
                                ->insert(['services_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'offered'=>$offered,'created_at'=>$created_at,'updated_at'=>$created_at]);
                    }
                }
            }
            return redirect('admin/services/')->withMessage('Service has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('services')->where('id', $id)->first();        
        if ($record != null){
            $allOtherSlide  = DB::table('services_translations')->where('services_id', $id)->get();   
            $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
            $allOtherLang   = array();
            if(!empty($allOtherSlide)){
                foreach ($allOtherSlide as $singleLang) {
                    $allOtherLang[$singleLang->language]['name'] = $singleLang->name;
                    $allOtherLang[$singleLang->language]['content'] = $singleLang->content;
                    $allOtherLang[$singleLang->language]['offered'] = $singleLang->offered;
                }
            }
            $view_data['currPage']      = 'allservices';
            $view_data['hasForm']       = 'yes';
            $view_data['record']        = $record;
            $view_data['allLanguages']  = $allLanguages;
            $view_data['allOtherLang']  = $allOtherLang;
            return view('admin.editService', $view_data);
        }else{
            return redirect('admin/services'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'image'             => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/services/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                    $old_path = $request->input('old_image');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    } 
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_image'));
                }
            }
            $data_array['slug']             = create_slug($request->input('name'),$currid,'services','slug');
            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['offered']          = '';//$request->input('offered');
            $data_array['external_link']    = $request->input('external_link');
            $data_array['display_form']     = $request->input('display_form');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('services')
            ->where('id', $currid)
            ->update($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    $offered        = '';//$request->input('offered_'.$lang);
                    if(!empty($name) || !empty($content) || !empty($offered)){
                        $getLangData = DB::table('services_translations')->where('services_id',$currid)->where('language',$lang)->first();
                        if(!empty($getLangData)){
                            DB::table('services_translations')
                                ->where('services_id',$currid)->where('language',$lang)
                                ->update(['name'=>$name,'content'=>$content,'offered'=>$offered,'updated_at'=>$created_at]);
                        }else{
                            DB::table('services_translations')
                                ->insert(['services_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'offered'=>$offered,'created_at'=>$created_at,'updated_at'=>$created_at]);
                        }
                    }else{
                        DB::table('services_translations')->where('services_id',$currid)->where('language',$lang)->delete();
                    }
                }
            }
            return redirect('admin/services/')->withMessage('Service has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('services')->where('id', $id)->first();        
        if ($record != null){
            DB::table('services')->where('id', $id)->delete();            
            DB::table('services_translations')->where('services_id', $id)->delete();
            $old_path = $record->image;
            if(!empty($old_path)){
                $old_path = str_replace(s3Path(), '', $old_path);
                Storage::disk('s3')->delete($old_path);
                @unlink($old_path);
            } 
            return redirect('admin/services/')->withMessage('Service has been deleted successfully.');
        }else{
            return redirect('admin/services'); 
        } 
    }

    public function viewRequests()
    {
        $allRecords = DB::table('services_repair')->orderby('id', 'DESC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allServicesRequests';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allServicesRequests', $view_data);
    }

    public function editRequests($id)
    {
        $record   = DB::table('services_repair')->where('id', $id)->first();        
        if ($record != null){
            $equipment_type = $record->equipment_type;
            $serial_number  = $record->serial_number;
            $product_issue  = $record->product_issue;
            $amount         = $record->amount;
            $equipmentTypeArray = array();
            $serialNumberArray  = array();
            $productIssueArray  = array();
            $amountArray        = array();
            if(!empty($equipment_type)){
                $equipmentTypeArray = explode('########', $equipment_type);
                $serialNumberArray  = explode('########', $serial_number);
                $productIssueArray  = explode('########', $product_issue);
                $amountArray        = explode('########', $amount);
            }
            $view_data['currPage']          = 'allServicesRequests';
            $view_data['hasForm']           = 'yes';
            $view_data['record']            = $record;
            $view_data['equipmentTypeArray']= $equipmentTypeArray;
            $view_data['serialNumberArray'] = $serialNumberArray;
            $view_data['productIssueArray'] = $productIssueArray;
            $view_data['amountArray']       = $amountArray;
            return view('admin.editServiceRequests', $view_data);
        }else{
            return redirect('admin/services-request'); 
        } 
    }
    public function deleteRequests($id)
    {
        $record   = DB::table('services_repair')->where('id', $id)->first();        
        if ($record != null){
            DB::table('services_repair')->where('id', $id)->delete();            
           
            return redirect('admin/services-request/')->withMessage('Service request has been deleted successfully.');
        }else{
            return redirect('admin/services-request'); 
        } 
    }

}