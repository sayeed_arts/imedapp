<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Hash;
use Validator;

class SliderController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allslides = DB::table('sliders')->orderby('display_order', 'ASC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allslides';
        $view_data['allslides']     = $allslides;
        return view('admin.allslides', $view_data);
    }

    public function add()
    {   
        $allLanguages = DB::table('languages')->where('id','<>', 1)->get();
        $view_data['currPage']      = 'allslides';
        $view_data['hasForm']       = 'yes';
        $view_data['allLanguages']  = $allLanguages;
        return view('admin.addslide',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            /*'name'              => 'required',
            'content'           => 'required',*/
            'image'             => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/slider/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                }else{
                    $image_path         = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath    = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;
                }
            }
            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['button_link']      = $request->input('button_link');
            $data_array['button_text']      = $request->input('button_text');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            
            $currid = DB::table('sliders')->insertGetId($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    $button_text    = $request->input('button_text_'.$lang);                    
                    if(!empty($name) || !empty($content) || !empty($button_text)){
                       DB::table('sliders_translations')
                                ->insert(['slider_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'button_text'=>$button_text,'created_at'=>$created_at,'updated_at'=>$created_at]);
                    }
                }
            }
            return redirect('admin/slider/')->withMessage('Slide has been added successfully.');
        }
    }
    public function edit($id)
    {
        $slide   = DB::table('sliders')->where('id', $id)->first();        
        if ($slide != null){
            $allOtherSlide  = DB::table('sliders_translations')->where('slider_id', $id)->get();   
            $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
            $allOtherLang   = array();
            if(!empty($allOtherSlide)){
                foreach ($allOtherSlide as $singleLang) {
                    $allOtherLang[$singleLang->language]['name'] = $singleLang->name;
                    $allOtherLang[$singleLang->language]['content'] = $singleLang->content;
                    $allOtherLang[$singleLang->language]['button_text'] = $singleLang->button_text;
                }
            }
            $view_data['currPage']      = 'allslides';
            $view_data['hasForm']       = 'yes';
            $view_data['slide']         = $slide;
            $view_data['allLanguages']  = $allLanguages;
            $view_data['allOtherLang'] = $allOtherLang;
            return view('admin.editslide', $view_data);
        }else{
            return redirect('admin/slider'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            /*'name'              => 'required',*/
            'image'             => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/slider/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                    $old_path = $request->input('old_image');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    }  
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_image'));
                }
            }

            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['button_link']      = $request->input('button_link');
            $data_array['button_text']      = $request->input('button_text');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('sliders')
            ->where('id', $currid)
            ->update($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    $button_text    = $request->input('button_text_'.$lang);
                                      
                    if(!empty($name) || !empty($content) || !empty($button_text)){
                        $getLangData = DB::table('sliders_translations')->where('slider_id',$currid)->where('language',$lang)->first();
                        if(!empty($getLangData)){
                            DB::table('sliders_translations')
                                ->where('slider_id',$currid)->where('language',$lang)
                                ->update(['name'=>$name,'content'=>$content,'button_text'=>$button_text,'updated_at'=>$created_at]);
                        }else{
                            DB::table('sliders_translations')
                                ->insert(['slider_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'button_text'=>$button_text,'created_at'=>$created_at,'updated_at'=>$created_at]);
                        }
                    }else{
                        DB::table('sliders_translations')->where('slider_id',$currid)->where('language',$lang)->delete();
                    }
                }
            }
            return redirect('admin/slider/')->withMessage('Slide has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('sliders')->where('id', $id)->first();        
        if ($record != null){
            DB::table('sliders')->where('id', $id)->delete();
            DB::table('sliders_translations')->where('slider_id', $id)->delete();
            $old_path = $record->image;
            if(!empty($old_path)){
                $old_path = str_replace(s3Path(), '', $old_path);
                Storage::disk('s3')->delete($old_path);
                @unlink($old_path);
            }
            return redirect('admin/slider/')->withMessage('Slide has been deleted successfully.');
        }else{
            return redirect('admin/slider'); 
        } 
    }    
}