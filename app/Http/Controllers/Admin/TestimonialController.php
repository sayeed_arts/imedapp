<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;

class TestimonialController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allRecords = DB::table('testimonials')->orderby('display_order', 'ASC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'alltestimonials';
        $view_data['allRecords']    = $allRecords;
        return view('admin.alltestimonials', $view_data);
    }

    public function add()
    {        
        $view_data['currPage']      = 'alltestimonials';
        $view_data['hasForm']       = 'yes';
        return view('admin.addtestimonial',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'location'          => 'required',
            'image'             => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'content'           => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/testimonial/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();   
            $image          = $request->file('image');
            if($image!=''){
                $image_path         = randomFilename(15).'.'.$image->getClientOriginalExtension();
                $destinationPath    = public_path('/uploaded_images');
                $image->move($destinationPath, $image_path);
                $data_array['image']   = 'uploaded_images/'.$image_path;
            }         
            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['location']         = $request->input('location');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            
            DB::table('testimonials')
            ->insert($data_array);

            return redirect('admin/testimonial/')->withMessage('Testimonial has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('testimonials')->where('id', $id)->first();        
        if ($record != null){
            $view_data['currPage']      = 'alltestimonials';
            $view_data['hasForm']       = 'yes';
            $view_data['record']        = $record;
            return view('admin.edittestimonial', $view_data);
        }else{
            return redirect('admin/testimonial'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'location'          => 'required',
            'image'             => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'content'           => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/testimonial/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploaded_images');
                $image->move($destinationPath, $image_path);
                $data_array['image']   = 'uploaded_images/'.$image_path;                
                @unlink($request->input('old_image'));
            }
            $data_array['name']             = $request->input('name');
            $data_array['content']          = $request->input('content');
            $data_array['location']         = $request->input('location');
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('testimonials')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/testimonial/')->withMessage('Testimonial has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('testimonials')->where('id', $id)->first();        
        if ($record != null){
            @unlink($record->image);
            DB::table('testimonials')->where('id', $id)->delete();
            return redirect('admin/testimonial/')->withMessage('Testimonial has been deleted successfully.');
        }else{
            return redirect('admin/testimonial'); 
        } 
    }    
}