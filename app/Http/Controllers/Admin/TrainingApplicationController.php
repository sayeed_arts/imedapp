<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;
use App\TrainingCategory;

class TrainingApplicationController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allRecords = DB::table('trainingapplications')
                        ->leftJoin('trainings','trainingapplications.training_id','=','trainings.id')
                        ->select('trainingapplications.*','trainings.name')
                        ->orderby('id', 'DESC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'alltrainingapplications';
        $view_data['allRecords']    = $allRecords;
        return view('admin.alltrainingapplications', $view_data);
    }

    public function view($id)
    {
        $record   = DB::table('trainingapplications')
                    ->leftJoin('trainings','trainingapplications.training_id','=','trainings.id')
                    ->select('trainingapplications.*','trainings.name')
                    ->where('trainingapplications.id', $id)
                    ->first();  
        $allCategories = TrainingCategory::orderby('display_order','ASC')->get();         
        if ($record != null){
            $view_data['currPage']      = 'alljobs';
            $view_data['hasForm']       = 'yes';
            $view_data['record']         = $record;
            $view_data['allCategories'] = $allCategories;
            return view('admin.viewtrainingapplication', $view_data);
        }else{
            return redirect('admin/trainingapplications'); 
        } 
    }
    
    public function delete($id)
    {
        $record   = DB::table('trainingapplications')->where('id', $id)->first();        
        if ($record != null){
            DB::table('trainingapplications')->where('id', $id)->delete();
            return redirect('admin/trainingapplications/')->withMessage('Training Application has been deleted successfully.');
        }else{
            return redirect('admin/trainingapplications'); 
        } 
    }    
}