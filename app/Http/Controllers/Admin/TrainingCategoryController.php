<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Hash;
use Validator;
use App\TrainingCategory;

class TrainingCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');      
    }
    public function index()
    {       
        $allRecords = TrainingCategory::orderby('display_order','ASC')->get();        
        $view_data['hasTable']          = 'yes';
        $view_data['currPage']          = 'alltrainingcategories';
        $view_data['allRecords']        = $allRecords;
        return view('admin.alltrainingcategories', $view_data);
    }
    public function add()
    {
        $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
        //$allRecords   = DB::select('select * from categories WHERE parent_id=0 ORDER BY display_order ASC');
        $allRecords     = TrainingCategory::orderby('display_order','ASC')->get();   
        $view_data['hasForm']           = 'yes';
        $view_data['currPage']          = 'addTrainingCategory';
        $view_data['allRecords']        = $allRecords;
        $view_data['allLanguages']      = $allLanguages;
        return view('admin.addtrainingcategory',$view_data);
    }
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' 			=> 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/trainingcategories/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
                  
            $data_array['slug']     = create_slug($request->input('name'),'','trainingcategories','slug');
            $data_array['name']          = $request->input('name');
            
            $data_array['content']       = $request->input('content');
            $data_array['meta_title']    = $request->input('meta_title');
            $data_array['meta_key']      = $request->input('meta_key');
            $data_array['meta_desc']     = $request->input('meta_desc');
            $data_array['display_order'] = $request->input('display_order');
            $data_array['display_status']= $request->input('display_status');
            $data_array['created_at']    = $created_at;
            $data_array['updated_at']    = $created_at;
            
            $currid = DB::table('trainingcategories')->insertGetId($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    if(!empty($name) || !empty($content)){
                       DB::table('trainingcategories_translations')
                                ->insert(['category_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'created_at'=>$created_at,'updated_at'=>$created_at]);
                    }
                }
            }

            return redirect('admin/trainingcategories/')->withMessage('Category details has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record     = DB::table('trainingcategories')->where('id', $id)->first();
        
        $allRecords = TrainingCategory::where('id','<>', $id)->orderby('display_order','ASC')->get();   
        if ($record != null){
            $allOtherSlide  = DB::table('trainingcategories_translations')->where('category_id', $id)->get();   
            $allLanguages   = DB::table('languages')->where('id','<>', 1)->get();
            $allOtherLang   = array();
            if(!empty($allOtherSlide)){
                foreach ($allOtherSlide as $singleLang) {
                    $allOtherLang[$singleLang->language]['name'] = $singleLang->name;
                    $allOtherLang[$singleLang->language]['content'] = $singleLang->content;
                }
            }
            $view_data['hasForm']       = 'yes';
            $view_data['currPage']      = 'editTrainingCategory';
            $view_data['record']        = $record;
            $view_data['allRecords']    = $allRecords;
            $view_data['allLanguages']  = $allLanguages;
            $view_data['allOtherLang']  = $allOtherLang;
            return view('admin.edittrainingcategory', $view_data);
        }else{
            return redirect('admin/trainingcategories'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name' 				=> 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/trainingcategories/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate(); 
                      
            $data_array['slug']             = create_slug($request->input('name'),$currid,'trainingcategories','slug');
            $data_array['name']             = $request->input('name');
            $data_array['content']        	= $request->input('content');
            $data_array['meta_title']       = $request->input('meta_title');
            $data_array['meta_key']         = $request->input('meta_key');
            $data_array['meta_desc']        = $request->input('meta_desc');

            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('trainingcategories')
            ->where('id', $currid)
            ->update($data_array);
            if(isset($_POST['language'])){
                foreach ($_POST['language'] as $lang) {
                    $name           = $request->input('name_'.$lang);
                    $content        = $request->input('content_'.$lang);
                    if(!empty($name) || !empty($content)){
                        $getLangData = DB::table('trainingcategories_translations')->where('category_id',$currid)->where('language',$lang)->first();
                        if(!empty($getLangData)){
                            DB::table('trainingcategories_translations')
                                ->where('category_id',$currid)->where('language',$lang)
                                ->update(['name'=>$name,'content'=>$content,'updated_at'=>$created_at]);
                        }else{
                            DB::table('trainingcategories_translations')
                                ->insert(['category_id'=>$currid,'language'=>$lang,'name'=>$name,'content'=>$content,'created_at'=>$created_at,'updated_at'=>$created_at]);
                        }
                    }else{
                        DB::table('trainingcategories_translations')->where('category_id',$currid)->where('language',$lang)->delete();
                    }
                }
            }
            return redirect('admin/trainingcategories/')->withMessage('Category details has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $category   = DB::table('trainingcategories')->where('id', $id)->first();
        if ($category != null){
            DB::table('trainingcategories')->where('id', $id)->delete();
            DB::table('trainingcategories_translations')->where('category_id', $id)->delete();
            $trainings  = DB::table('trainings')->where('category_id', $id)->get();   
            foreach ($trainings as $singleTraining) {
                DB::table('trainings')->where('id', $singleTraining)->delete();
                DB::table('trainings_translations')->where('training_id', $singleTraining)->delete();
            }
            return redirect('admin/trainingcategories/')->withMessage('Category has been deleted successfully.');
        }else{
            return redirect('admin/trainingcategories'); 
        } 
    }

    public function categoryTraining ($id)
    {   
        $record     = DB::table('trainingcategories')->where('id', $id)->first();
        if ($record != null){
            $allRecords = DB::table('trainings')
                ->whereRaw('FIND_IN_SET('.$id.',category_id)')
                ->orderby('display_order', 'ASC')
                ->get();
            $view_data['hasTable']      = 'yes';
            $view_data['currPage']      = 'alltraining';
            $view_data['allRecords']    = $allRecords;
            $view_data['record']        = $record;
            return view('admin.allcategorytrainings', $view_data);
        }else{
            return redirect('admin/trainingcategories'); 
        }
    }
}
