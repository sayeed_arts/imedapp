<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;
use App\Users;
use App\VerifyUser;
class UsersController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allUsers = DB::select("select * from users where is_admin='0' ORDER BY id DESC");
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allusers';
        $view_data['allRecords']    = $allUsers;
        return view('admin.allusers', $view_data);
    }
    public function add()
    {        
        $view_data['hasForm']       = 'yes';
        $view_data['currPage']  = 'addUser';
        return view('admin.adduser',$view_data);
    }
    public function save(Request $request)
    {    
        $validator = Validator::make($request->all(), [
          'name'    => 'required|max:50',
          'email'   => 'required|email|unique:users,email',
          'password'=> 'required|between:5,20',
          'confirm_password'=> 'required|between:5,20|same:password'
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/users/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $hashpass       = Hash::make($request->input('password')); 
            $created_at     = currentDBDate();

            $insert_array = array(
                'name'=>$request->input('name'),
                'last_name'    => $request->input('last_name'),
                'email'=>$request->input('email'),
                'verified'=>$request->input('verified'),
                'phone_number' => $request->input('phone_number'),                
                'is_approved' => $request->input('is_approved'), 
                'password'=>$hashpass,
                'created_at'=>$created_at,
                'updated_at'=>$created_at
                );
            DB::table('users')->insert($insert_array);

            return redirect('admin/users/')->withMessage('Users details has been added successfully.');
        }
    }
    public function edit($id)
    {
        $user   = DB::table('users')->where('id', $id)->first();        
        if ($user != null){
            $view_data['hasForm']       = 'yes';
            $view_data['currPage']      = 'editUser';
            $view_data['record']        = $user;
            return view('admin.edituser', $view_data);
        }else{
            return redirect('admin/users'); 
        } 
    }
    public function update(Request $request)
    {   
        $usrid      = $request->input('currid');
        $required_array = array(            
            'name'      => 'required|max:50',
            'last_name' => 'required|max:50',
            'email'     => 'required|email|unique:users,email,'.$usrid,            
        );
        $validator = Validator::make($request->all(), $required_array);
        if ($validator->fails()) {
            return redirect('admin/users/edit/'.$usrid)
                        ->withErrors($validator)
                        ->withInput();
        }else{           
            $updated_at         = currentDBDate();
            $old_is_approved    = $request->input('old_is_approved');
            $is_approved        = $request->input('is_approved');
            $email              = $request->input('email');

            $data_array = array(
                'name'          => $request->input('name'),
                'last_name'     => $request->input('last_name'),
                'email'         => $request->input('email'),
                'verified'      => $request->input('verified'),
                'phone_number'  => $request->input('phone_number'),                
                'is_approved'   => $request->input('is_approved'),                
                'updated_at'    => $updated_at
            );
            DB::table('users')
                ->where('id', $usrid)
                ->update($data_array);

            if($old_is_approved==0 && $is_approved==1){
                $user = DB::table('users')->find($usrid);               
                $verifyUser = DB::table('verify_users')->where('user_id', $usrid)->first();
                $userdata['name']               = $user->name.' '.$user->last_name;
                $userdata['verifyUsertoken']    = $verifyUser->token;                    
                $subject        = 'Activate Your Account';
                $template       = 'emails.verifyUser';
                
                send_smtp_email($email,$subject,$userdata,$template);

                $welcomeSubject        = 'Welcome to Imedical Shop!';
                $welcomeTemplate       = 'emails.welcome';
                //send_smtp_email($email,$welcomeSubject,$userdata,$welcomeTemplate);
            }
            return redirect('admin/users/')->withMessage('User details has been updated successfully.');
        }
    }
   
    public function delete($id)
    {
        $users   = DB::table('users')->where('id', $id)->first();        
        if ($users != null){
            DB::table('users')->where('id', $id)->delete();
            return redirect('admin/users/')->withMessage('User has been deleted successfully.');
        }else{
            return redirect('admin/users'); 
        } 
    } 
}
