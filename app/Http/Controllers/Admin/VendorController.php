<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;
use App\Users;
use App\VerifyUser;
class VendorController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index(Request $request)
    {
        $requestdata = $request->all();
        $is_approved = '';
        if(isset($requestdata['is_approved'])){
            $is_approved = $requestdata['is_approved'];
        }
        if($is_approved !== ''){
            $allUsers = DB::select("select * from users where is_admin='2' AND is_approved = ".$is_approved." ORDER BY id DESC");
        }else{
            $allUsers = DB::select("select * from users where is_admin='2' ORDER BY id DESC");
        }
        
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allusers';
        $view_data['allRecords']    = $allUsers;
        return view('admin.allvendors', $view_data);
    }
    public function add($id="")
    {     

        $view_data['countrylist'] = DB::table('countries')->pluck( 'countryName','countryID');
        $view_data['hasForm']       = 'yes';
        $view_data['currPage']  = 'addvendor';
       if(!empty($id)){
        $view_data['vendordetail'] = DB::table('users')->where('id', $id)->where('is_admin', '2')->first();

        $view_data['allState'] = DB::table('states')->select('stateID','stateName','countryID')->where(array('status'=>'1','countryID'=>$view_data['vendordetail']->country))->orderby('stateName')->get();
       

       }
        return view('admin.addvendor',$view_data);
    }
    public function save(Request $request)
    {    
    
     if(!empty($request->input('vendor_id'))){
        $validator = Validator::make($request->all(), [
          'name'    => 'required|max:50',
          'last_name'    => 'required',
          'address1'    => 'required',
          'city'    => 'required',
          'title'    => 'required',
          'store'    => 'required',
          'state'    => 'required',
          'compweb'    => 'required',
          'ext'    => 'required',
          'comp_name'    => 'required',
          'phone_number'    => 'required',
          'email'   => 'required|email|unique:users,email,'.$request->input('vendor_id'),
        ]);
        }
        else{
         $validator = Validator::make($request->all(), [
          'name'    => 'required|max:50',
          'last_name'    => 'required',
          'address1'    => 'required',
          'city'    => 'required',
          'title'    => 'required',
          'store'    => 'required|unique:users,Store',
          'state'    => 'required',
          'compweb'    => 'required',
          'ext'    => 'required',
          'comp_name'    => 'required',
          'phone_number'    => 'required',
          'email'   => 'required|email|unique:users,email',
          'password'=> 'required|between:5,20',
          'confirm_password'=> 'required|between:5,20|same:password'
        ]);
            
        }
        if ($validator->fails()) {
                 if(empty($request->input('vendor_id'))){
                 
                        return redirect('admin/vendors/add')
                                    ->withErrors($validator)
                                    ->withInput();
                 }
                 else{
                        return redirect('admin/vendors/add/'.$request->input('vendor_id'))
                                    ->withErrors($validator)
                                    ->withInput();  
                 }
        }else{
            $hashpass       = Hash::make($request->input('password')); 
            $created_at     = currentDBDate();


            $insert_array = array(
                'name'=>$request->input('name'),
                'last_name'=> $request->input('last_name'),
                'email'=>$request->input('email'),
                'address1'=>$request->input('address1'),
                'city'=>$request->input('city'),
                'title'=>$request->input('title'),
                'country'=>$request->input('country'),
                'store_slug'=> $this->slugify($request->input('store')),
                'company'=>$request->input('comp_name'),
                'Store'=>$request->input('store'),
                'state'=>$request->input('state'),
                'website'=>$request->input('compweb'),
                'ext'=>$request->input('ext'),
                'phone_number'=> $request->input('phone_number'),
                'zipcode'=> $request->input('zipcode'),                
                'is_approved'=> '1', 
                'verified'=> '1', 
                'is_admin'=>'2',
                'password'=>$hashpass,
                'created_at'=>$created_at,
                'updated_at'=>$created_at
                );
           if(!empty($request->input('vendor_id'))){
               unset($insert_array['is_admin']);
               unset($insert_array['password']);
               unset($insert_array['created_at']);
               $insert_array['is_approved'] = $request->input('is_approved');
               DB::table('users')->where('id', $request->input('vendor_id'))
              ->update($insert_array);

            $template   = 'emails.vendorapprovedEmail';
            $view_data['name']     = $insert_array['name']." ".$insert_array['last_name'];
            send_smtp_email($insert_array['email'],'Vendor Account Approved',$view_data,$template,array(),array());


            return redirect('admin/vendors/')->withMessage('Vendor details has been updated successfully.');
           }
           else{
            $insert_array['readpass'] = $request->input('password');
            $lastid = DB::table('users')->insertGetId($insert_array);
            $data = array('user_id' => $lastid,
                'token' => str_random(40));

            $verifyUser = DB::table('verify_users')->insert($data);
            
            $view_data['name']     = $insert_array['name']." ".$insert_array['last_name'];
            $view_data['verifyUsertoken']     = $data['token'];
            $view_data['email']     = $insert_array['email'];
            $view_data['password']     = $insert_array['readpass'];
            
            $template   = 'emails.vendoraccountEmail';
            send_smtp_email($insert_array['email'],'Vendor Account Created',$view_data,$template,array(),array());


            $admintemplate = 'emails.vendoraccountadminEmail';
            $admin_view_data['name'] = $insert_array['name']." ".$insert_array['last_name'];
            $admin_view_data['email'] =$insert_array['email']; 
            send_smtp_email('negiyogesh145@gmail.com','Vendor Account Created',$admin_view_data,$admintemplate,array(),array());

            return redirect('admin/vendors/')->withMessage('Vendor details has been added successfully.');
            
           }

        }
    }


    public function slugify($text, string $divider = '-')
{
  // replace non letter or digits by divider
  $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, $divider);

  // remove duplicate divider
  $text = preg_replace('~-+~', $divider, $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}
    public function edit($id)
    {
        $user   = DB::table('users')->where('id', $id)->first();        
        if ($user != null){
            $view_data['hasForm']       = 'yes';
            $view_data['currPage']      = 'editUser';
            $view_data['record']        = $user;
            return view('admin.edituser', $view_data);
        }else{
            return redirect('admin/users'); 
        } 
    }
    public function update(Request $request)
    {   
        $usrid      = $request->input('currid');
        $required_array = array(            
            'name'      => 'required|max:50',
            'last_name' => 'required|max:50',
            'email'     => 'required|email|unique:users,email,'.$usrid,            
        );
        $validator = Validator::make($request->all(), $required_array);
        if ($validator->fails()) {
            return redirect('admin/users/edit/'.$usrid)
                        ->withErrors($validator)
                        ->withInput();
        }else{           
            $updated_at         = currentDBDate();
            $old_is_approved    = $request->input('old_is_approved');
            $is_approved        = $request->input('is_approved');
            $email              = $request->input('email');

            $data_array = array(
                'name'          => $request->input('name'),
                'last_name'     => $request->input('last_name'),
                'email'         => $request->input('email'),
                'verified'      => $request->input('verified'),
                'phone_number'  => $request->input('phone_number'),                
                'is_approved'   => $request->input('is_approved'),                
                'updated_at'    => $updated_at
            );
            DB::table('users')
                ->where('id', $usrid)
                ->update($data_array);

            if($old_is_approved==0 && $is_approved==1){
                $user = DB::table('users')->find($usrid);               
                $verifyUser = DB::table('verify_users')->where('user_id', $usrid)->first();
                $userdata['name']               = $user->name.' '.$user->last_name;
                $userdata['verifyUsertoken']    = $verifyUser->token;                    
                $subject        = 'Activate Your Account';
                $template       = 'emails.verifyUser';
                
                send_smtp_email($email,$subject,$userdata,$template);

                $welcomeSubject        = 'Welcome to Imedical Shop!';
                $welcomeTemplate       = 'emails.welcome';
                //send_smtp_email($email,$welcomeSubject,$userdata,$welcomeTemplate);
            }
            return redirect('admin/users/')->withMessage('User details has been updated successfully.');
        }
    }
   
    public function delete($id)
    {
        $users   = DB::table('users')->where('id', $id)->first();        
        if ($users != null){
            DB::table('users')->where('id', $id)->delete();
            return redirect('admin/users/')->withMessage('User has been deleted successfully.');
        }else{
            return redirect('admin/users'); 
        } 
    }
    public function getstate(){
        if(!empty($_POST)){
             $statelist = DB::table('states')->pluck( 'stateName','stateID');
             $html="";
             $html.='<select name="state" id="state" class="form-control fetch_cities" tabindex="11">
                   <option value="">Select State</option>';
                foreach($statelist as $key=>$value):
                    $html.='<option value="'.$key.'">"'.$value.'"</option>';
                endforeach;                  
                }
                $html.='</select>';
                echo $html;
                die;
    } 

    public function deletevendor(){
      if(!empty($_POST)){
          DB::table('users')->where('id', '=', $_POST['vendor_id'])->delete();
          echo "1";
          die;
      }
      else{
        echo "0";
      }
    }

    public function setcommision(){
        if(!empty($_POST)){

            DB::table('users')
            ->where('id', $_POST['vendorid'])
            ->update(array('commission'=>$_POST['commission']));

            return redirect('admin/vendors/')->withMessage('Commission has been updated successfully.');            
        }
    }


    public function vendorpayment(){
         $allUsers = DB::select("select * from users where is_admin='2' ORDER BY id DESC");
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allusers';
        $view_data['allRecords']    = $allUsers;
        return view('admin.allvendorpayments', $view_data);
       
    }


    public function payvendor(Request $request){
           if(!empty($request->input('vendorid'))){
            $created_at     = currentDBDate();

             $vendorcommission = DB::table('users')->select('users.commission')->where('users.id',Auth::user()->id);
              


             if(!empty($vendorcommission->commission)){
              $commission = $vendorcommission->commission;
             }
              if(empty($commission)){
                           $globalcommission = DB::table('settings')->select( 'settings.vendor_commission')->first();
                           $commission = $globalcommission->vendor_commission;
              }

              $commssion_amt = $request->input('amount') * $commission/100;
       
              $view_data['countrylist'] = DB::table('countries')->pluck( 'countryName','countryID');
       
            
            $insert_array = array(
                'vendor_id'=>$request->input('vendorid'),
                'amount'=> $request->input('amount'),
                'commission'=>$commssion_amt,
                'transaction_id'=>$request->input('transaction_id'),
                'transaction_type'=>$request->input('type'),
                'percent_commission'=>$commission,
                'added_at'=>$created_at
                );

            $lastid = DB::table('vendor_payments')->insertGetId($insert_array);
            return redirect('admin/vendors/vendorpayments/')->withMessage('Payment done successfully.');

            
           }

    }

    public function vendorpaymentlogs($id){
        $allUsers = DB::select("select * from vendor_payments where vendor_payments.vendor_id=".$id."  ORDER BY id DESC");

        $view_data['userdetails'] = DB::table('users')->where('id', $id)->first();

        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allusers';
        $view_data['allRecords']    = $allUsers;
        return view('admin.allvendorpaylogs', $view_data);
    }

}
