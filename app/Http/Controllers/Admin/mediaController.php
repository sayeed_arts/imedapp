<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Hash;
use Validator;

class mediaController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allRecords = DB::table('media_images')->orderby('id', 'DESC')->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allmediaimages';
        $view_data['allRecords']    = $allRecords;
        return view('admin.allmediaimages', $view_data);
    }

    public function add()
    {        
        $view_data['currPage']      = 'allmediaimages';
        $view_data['hasForm']       = 'yes';
        return view('admin.addmediaimage',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image'  => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/media/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;                    
                }else{
                    $image_path         = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath    = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;
                }
            }            
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            
            DB::table('media_images')
            ->insert($data_array);

            return redirect('admin/media/')->withMessage('Media image has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('media_images')->where('id', $id)->first();        
        if ($record != null){
            $view_data['currPage']      = 'allmediaimages';
            $view_data['hasForm']       = 'yes';
            $view_data['record']         = $record;
            return view('admin.editmediaimage', $view_data);
        }else{
            return redirect('admin/media'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/media/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $image          = $request->file('image');
            if($image!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->image);
                if(strpos($path, 'uploaded_images') !== false){
                    $data_array['image']   = s3Path().$path;
                    $old_path = $request->input('old_image');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    } 
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $data_array['image']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_image'));
                }
            }
            $data_array['updated_at']       = $created_at;
            
            DB::table('media_images')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('admin/media/')->withMessage('Media image has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('media_images')->where('id', $id)->first();        
        if ($record != null){
            DB::table('media_images')->where('id', $id)->delete();
            $old_path = $record->image;
            if(!empty($old_path)){
                $old_path = str_replace(s3Path(), '', $old_path);
                Storage::disk('s3')->delete($old_path);
                @unlink($old_path);
            }            
            return redirect('admin/media/')->withMessage('Media image has been deleted successfully.');
        }else{
            return redirect('admin/media'); 
        } 
    }    
}