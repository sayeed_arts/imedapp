<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\VerifyUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Cookie;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }
    public function authenticated(Request $request, $user)
    {

        if(!$user->is_approved){
            auth()->logout();
            return back()->with('warning', 'Your account is not approved yet from system admin. Please wait we will notify you via email once your account is approved.');
        }else if (!$user->verified) {
 
            $verifyUser = VerifyUser::where('user_id', $user->id)->update([
                'token' => str_random(40)
            ]);
            
            $userdata['name']   = $user->name.' '.$user->last_name;
            $userdata['verifyUsertoken']  = $user->verifyUser->token;                    
            $subject        = 'Verify Your Account';
            $template       = 'emails.verifyUser';
            
            send_smtp_email($user->email,$subject,$userdata,$template);

            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an verification email, please check your email.');
        }else if($user->is_admin!=1 && $user->is_admin!=2){
            $sessionId      = Cookie::get('imedcartcookie');
            $currentUserId = $user->id;
            DB::table('cart_products')->where('session_id',$sessionId)->update(['user_id'=>$currentUserId]);
        }
        return redirect()->intended($this->redirectPath());
    }
}
