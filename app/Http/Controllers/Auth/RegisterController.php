<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\VerifyUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;




class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'phone_number' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone_number' => $data['phone_number'],
        ]);
        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);
        
        $userdata['name']   = $user->name.' '.$user->last_name;
        $userdata['verifyUsertoken']  = $user->verifyUser->token;                    
        $subject        = 'Verify Your Account';
        $template       = 'emails.verifyUser';
        
        //send_smtp_email($user->email,$subject,$userdata,$template);

        return $user;
    }
    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->is_approved) {                
                $status = "Your account is not approved yet from system admin. Please wait we will notify you via email once your account is approved.";
                return redirect('/login')->with('status', $status);
            }else if(!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login.";
            }else{
                $status = "Your e-mail is already verified. You can now login.";
            }
            //return redirect('/login')->with('status', $status);
            Auth::login($user);
            return redirect('/user');
        }else{
            return redirect('/login')->with('warning', "Sorry your email can't be identified.");
        }
    }

    protected function registered(Request $request, $user)
    {
        $this->guard()->logout();
        return redirect('/login')->with('status', 'Thanks for creating your account at Imedical. Your account is under approval. We will notify you via email once your account is approved.');
    }

    public function vendor_register(){
        $view_data['currPage']              = 'Vendor Register';
        $view_data['countrylist'] = DB::table('countries')->pluck( 'countryName','countryID');
        // $view_data['allState'] = DB::table('states')->select('stateID','stateName','countryID')->where(array('status'=>'1','countryID'=>$view_data['vendordetail']->country))->orderby('stateName')->get();
      
        return view('auth/vendor_register',$view_data);
    }


    public function save(Request $request)
    {    
    
     if(!empty($request->input('vendor_id'))){
        $validator = Validator::make($request->all(), [
          'name'    => 'required|max:50',
          'last_name'    => 'required',
          'address1'    => 'required',
          'city'    => 'required',
          'title'    => 'required',
          'store'    => 'required',
          'state'    => 'required',
          'compweb'    => 'required',
          'ext'    => 'required',
          'comp_name'    => 'required',
          'phone_number'    => 'required',
          'email'   => 'required|email|unique:users,email,'.$request->input('vendor_id'),
        ]);
        }
        else{
         $validator = Validator::make($request->all(), [
          'name'    => 'required|max:50',
          'last_name'    => 'required',
          'address1'    => 'required',
          'city'    => 'required',
          'title'    => 'required',
          'store'    => 'required|unique:users,Store',
          'state'    => 'required',
          'compweb'    => 'required',
          'ext'    => 'required',
          'comp_name'    => 'required',
          'phone_number'    => 'required',
          'email'   => 'required|email|unique:users,email',
          'password'=> 'required|between:5,20',
          'confirm_password'=> 'required|between:5,20|same:password'
        ]);
            
        }
        if ($validator->fails()) {
                 if(empty($request->input('vendor_id'))){
                 
                        return redirect('admin/vendors/add')
                                    ->withErrors($validator)
                                    ->withInput();
                 }
                 else{
                        return redirect('admin/vendors/add/'.$request->input('vendor_id'))
                                    ->withErrors($validator)
                                    ->withInput();  
                 }
        }else{
            $hashpass       = Hash::make($request->input('password')); 
            $created_at     = currentDBDate();


            $insert_array = array(
                'name'=>$request->input('name'),
                'last_name'=> $request->input('last_name'),
                'email'=>$request->input('email'),
                'address1'=>$request->input('address1'),
                'city'=>$request->input('city'),
                'title'=>$request->input('title'),
                'country'=>$request->input('country'),
                'company'=>$request->input('comp_name'),
                'Store'=>$request->input('store'),
                'state'=>$request->input('state'),
                'website'=>$request->input('compweb'),
                'ext'=>$request->input('ext'),
                'phone_number'=> $request->input('phone_number'),
                'zipcode'=> $request->input('zipcode'),                
                'is_admin'=>'2',
                'password'=>$hashpass,
                'created_at'=>$created_at,
                'updated_at'=>$created_at
                );
           if(!empty($request->input('vendor_id'))){
               unset($insert_array['is_admin']);
               unset($insert_array['password']);
               unset($insert_array['created_at']);
               $insert_array['is_approved'] = $request->input('is_approved');
               DB::table('users')->where('id', $request->input('vendor_id'))
              ->update($insert_array);

            $template   = 'emails.vendorapprovedEmail';
            $view_data['name']     = $insert_array['name']." ".$insert_array['last_name'];
            //send_smtp_email($insert_array['email'],'Vendor Account Approved',$view_data,$template,array(),array());


            return redirect('/vendor-register')->withMessage('Vendor details has been updated successfully.');
           }
           else{
            $insert_array['readpass'] = $request->input('password');
            $lastid = DB::table('users')->insertGetId($insert_array);
            $data = array('user_id' => $lastid,
                'token' => str_random(40));

            $verifyUser = DB::table('verify_users')->insert($data);
            
            $view_data['name']     = $insert_array['name']." ".$insert_array['last_name'];
            $view_data['verifyUsertoken']     = $data['token'];
            $view_data['email']     = $insert_array['email'];
            $view_data['password']     = $insert_array['readpass'];
            
            $template   = 'emails.vendoraccountEmail';
            send_smtp_email($insert_array['email'],'Vendor Account Created',$view_data,$template,array(),array());


            $admintemplate = 'emails.vendoraccountadminEmail';
            $admin_view_data['name'] = $insert_array['name']." ".$insert_array['last_name'];
            $admin_view_data['email'] =$insert_array['email']; 
           // Nakli Admin
            $adminmail   = DB::table('users')->where('is_admin','1')->where('id','!=','4')->where('verified','=','1')->where('is_approved','=','1')->first();
       
            // Exact Admin
          //  $adminmail   = DB::table('users')->where('is_admin','1')->where('verified','=','1')->where('is_approved','=','1')->first();
       
            send_smtp_email($adminmail->email,'Vendor Account Created',$admin_view_data,$admintemplate,array(),array());

            return redirect('/vendor-register')->withMessage('Vendor details has been added successfully.');
            
           }

        }
    }

}
