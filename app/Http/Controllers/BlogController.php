<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Pagination\Paginator;

class BlogController extends Controller
{
    public function __construct()
    {        
        //$this->middleware('guest');
    }

    public function index(){ 
        $pageData                   = DB::table('pages')->where('id',10)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['currPage']      = 'blog';
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        $recent4 =  DB::table('blogs')
            ->where('display_status','1')
            ->orderby('display_order')
            ->limit(5)
            ->get();

        $allData   = DB::table('blogs')
            ->where('display_status','1')
            ->orderby('display_order')
            ->simplePaginate(10);
        $view_data['allData'] = $allData;   
        $view_data['recent4'] = $recent4;        
        return view('blog_listing',$view_data);
    }
    
    public function blogDetails($blog_slug=''){
        $pageData                   = DB::table('pages')->where('id',10)->get()->first();
        $blogDetails = DB::table('blogs')
            ->where('display_status','1')
            ->where('slug',$blog_slug)
            ->orderby('display_order')
            ->get()
            ->first();
        $recent4 =  DB::table('blogs')
            ->where('display_status','1')
            ->orderby('display_order')
            ->limit(5)
            ->get();
        if(!empty($blogDetails)){
            $view_data['pageData']      = $pageData;
            $view_data['blogDetails']   = $blogDetails;
            $view_data['recent4']       = $recent4;
            $view_data['currPage']      = 'blogdetails';
            $view_data['meta_title']    = $blogDetails->meta_title;
            $view_data['meta_key']      = $blogDetails->meta_key;
            $view_data['meta_desc']     = $blogDetails->meta_desc;
            $view_data['page_title']    = $blogDetails->name;
            return view('blog_details',$view_data); 
        }else{
            return redirect('blog');
        }        
    }

    public function pressMedia(){
        $pageData                   = DB::table('pages')->where('id',13)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['currPage']      = 'press-media';
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;

        $recent4 =  DB::table('press_media')
            ->where('display_status','1')
            ->orderby('display_order')
            ->limit(5)
            ->get();

        $allData   = DB::table('press_media')
            ->where('display_status','1')
            ->orderby('display_order')
            ->simplePaginate(10);
        $view_data['allData'] = $allData;   
        $view_data['recent4'] = $recent4;        
        return view('press_media_listing',$view_data);
    }
    public function pressMediaDetails($slug=''){
        $postDetails = DB::table('press_media')
            ->where('display_status','1')
            ->where('slug',$slug)
            ->orderby('display_order')
            ->get()
            ->first();
        $recent4 =  DB::table('press_media')
            ->where('display_status','1')
            ->orderby('display_order')
            ->limit(5)
            ->get();
        if(!empty($postDetails)){
            $pageData                   = DB::table('pages')->where('id',13)->get()->first();
            $view_data['pageData']      = $pageData;
            $view_data['postDetails']   = $postDetails;
            $view_data['recent4']       = $recent4;
            $view_data['currPage']      = 'postdetails';
            $view_data['meta_title']    = $postDetails->meta_title;
            $view_data['meta_key']      = $postDetails->meta_key;
            $view_data['meta_desc']     = $postDetails->meta_desc;
            $view_data['page_title']    = $postDetails->name;
            return view('press_media_details',$view_data);
        }else{
            return redirect('press-media');
        }
    }
}
