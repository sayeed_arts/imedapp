<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;
use Cookie;
use Session;

class CheckoutController extends Controller
{
	protected $currentUser;
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('is-front');
        $this->middleware(function ($request, $next) {
            $this->currentUser = Auth::user();            
            return $next($request);
        });
    }
    public function index(Request $request){
        $appliedCoupon      = $request->session()->get('appliedCoupon');
        $currentUser        = Auth::user();
        $current_user_id    = (isset($currentUser->id))?$currentUser->id:'0';
        $sessionId          = Cookie::get('imedcartcookie');
        
        $pageData       = DB::table('pages')->where('id',13)->get()->first();
        $address        = DB::table('users_address')->where('user_id',$current_user_id)->where('default',1)->first();
        $allCountries   = DB::table('countries')
            ->where('status',1)
            ->orderby('countryName','ASC')
            ->get();
        $allStates = array();
        $countryId  = (isset($address->country))?$address->country:'';
        if($countryId!=''){
            $allStates = DB::table('states')
                ->where('status',1)
                ->where('countryID',$countryId)
                ->orderby('stateName','ASC')
                ->get();
        }

        $whereArray = array(
            'products.display_status'=>1,
            'products.product_type'=>1
        );
        if($current_user_id!=0){
            $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                $query->where('cart_products.user_id','=',$current_user_id)
                    ->orWhere('cart_products.session_id', '=',$sessionId);
            });
        }else{
            $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                $query->Where('cart_products.session_id', '=',$sessionId);
            });
        }        

        $allCartItems = DB::table('cart_products')
        ->select('cart_products.*','products.name','products.image','products.slug','products.price','products.special_price','products.stock_availability','products.inventory_management','products.qty as availableQty')
        ->join('products','products.id','=','cart_products.product_id')
        ->where($whereArray)                
        ->orderby('cart_products.id','ASC')
        ->get();

        $subTotalAmount     = 0;
        $userAppliedCoupons = 0;
        $availedDiscount    = 0;
        $priceAfterDiscount = 0;
        if(!empty($appliedCoupon)){
            $currentDate    = date('Y-m-d');
            $getCouponData  = DB::table('coupons')
            ->where('code',$appliedCoupon)
            ->where('start_date','<=',$currentDate)
            ->where('end_date','>=',$currentDate)
            ->where('status',1)
            ->first();
            if(!empty($getCouponData)){
                if(!empty($allCartItems)){
                    foreach($allCartItems as $singleCartItem){
                        $qty            = $singleCartItem->qty;
                        $price          = $singleCartItem->price;
                        $special_price  = $singleCartItem->special_price;
                        $stock_availability     = $singleCartItem->stock_availability;
                        $inventory_management   = $singleCartItem->inventory_management;
                        $availableQty           = $singleCartItem->availableQty;
                        if(!empty($special_price) && $special_price>0){
                            $proPrice = $special_price;
                        }else{
                            $proPrice = $price;
                        }
                        if(!empty($singleCartItem->product_attr_id)){
                            $getProAttrCart = DB::table('products_attributes')
                                ->where('id',$singleCartItem->product_attr_id)
                                ->where('product_id',$singleCartItem->product_id)
                                ->where('is_variation',1)
                                ->first();                           
                            $proPrice = (isset($getProAttrCart->attribute_price) && $getProAttrCart->attribute_price>0)?$getProAttrCart->attribute_price:$proPrice;
                        }

                        if(($inventory_management==0 && $stock_availability==1) || ($inventory_management==1 && $stock_availability==1 && $availableQty>=$qty)){
                            $cartQty = $qty;
                        }else if($inventory_management==1 && $stock_availability==1 && $availableQty<$qty){
                            $cartQty = $availableQty;
                        }else{
                            $cartQty='0';
                        }
                        $totalPrice     = $cartQty*$proPrice;
                        $subTotalAmount = $subTotalAmount+$totalPrice;
                    }
                }            
                
                if($current_user_id!=0){
                    $userAppliedCoupons = DB::table('orders')
                    ->where('coupon_code',$appliedCoupon)
                    ->where('customer_id',$current_user_id)
                    ->where('status',2)
                    ->count();
                }
                $appliedCoupons = DB::table('orders')
                    ->where('coupon_code',$appliedCoupon)
                    ->where('status',2)
                    ->count();

                $type               = $getCouponData->type;
                $value              = $getCouponData->value;
                $minimum_spend      = $getCouponData->minimum_spend;
                $maximum_spend      = $getCouponData->maximum_spend;
                $maximum_discount   = $getCouponData->maximum_discount;
                $usage_limit_per_coupon     = $getCouponData->usage_limit_per_coupon;
                $usage_limit_per_customer   = $getCouponData->usage_limit_per_customer;

                if($type=='0'){
                    $discount = $subTotalAmount-$value;
                }else{
                    $discount = ($subTotalAmount*$value)/100;
                }
                if($discount<$maximum_discount){
                    $finalDiscount = $discount;
                }else{
                    $finalDiscount = $maximum_discount;
                }           

                if($subTotalAmount>=$minimum_spend && $subTotalAmount<=$maximum_spend){
                    if($appliedCoupons<$usage_limit_per_coupon && $current_user_id==0){
                        $availedDiscount    = number_format($finalDiscount,2);
                        $priceAfterDiscount = number_format($subTotalAmount-$availedDiscount,2);
                    }else if($appliedCoupons<$usage_limit_per_coupon && $userAppliedCoupons<$usage_limit_per_customer && $current_user_id!=0){                       
                        $availedDiscount    = number_format($finalDiscount,2);
                        $priceAfterDiscount = number_format($subTotalAmount-$availedDiscount,2);
                    }else{
                        $request->session()->put('appliedCoupon', '');
                    }
                }else{
                    $request->session()->put('appliedCoupon', '');
                }
            }else{
                $request->session()->put('appliedCoupon', '');
            }
        }

        $view_data['pageData']              = $pageData;
        $view_data['meta_title']            = $pageData->meta_title;
        $view_data['meta_key']              = $pageData->meta_key;
        $view_data['meta_desc']             = $pageData->meta_desc;
        $view_data['page_title']            = $pageData->name;
        $view_data['currPage']              = 'checkout';
        $view_data['address']               = $address;
        $view_data['currentUser']           = $currentUser;
        $view_data['allCountries']          = $allCountries;
        $view_data['allStates']             = $allStates;
        $view_data['allCartItems']          = $allCartItems;        
        $view_data['availedDiscount']       = $availedDiscount;
        $view_data['priceAfterDiscount']    = $priceAfterDiscount;
        $view_data['appliedCoupon']         = $appliedCoupon;
        $view_data['generalSettings']       = generalSettings();
        return view('checkout',$view_data);
    }    
}