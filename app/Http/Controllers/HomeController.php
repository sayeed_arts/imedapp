<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function lang($locale)
    {
        $checkLang = DB::table('languages')->where('code',$locale)->where('status',1)->first();
        if(!empty($checkLang)){
            App::setLocale($locale);
            session()->put('locale', $locale);
        }        
        return redirect()->back();
    }
    public function currency($code)
    {
        $checkCurrency = DB::table('currencies')->where('code',$code)->where('status',1)->first();
        if(!empty($checkCurrency)){
            setCurrency($code);
        }        
        return redirect()->back();
    }
    public function index(Request $request)
    {
        $userLang               = App::getLocale();
        $equipmentTypeSubCats   = array();
        $healthCareSubCats      = array();
        $sliderData             = DB::table('sliders')
            ->where('display_status',1)
            ->orderby('display_order','ASC')
            ->get();
        $featuredProducts = DB::table('products')
            ->select('id','image','name','slug','price','special_price','stock_availability')
            ->where(['product_type'=>1,'display_status'=>1,'is_featured'=>1])
            ->limit(10)->get();

        $advertisementData  = DB::table('advertisement')->where('display_status',1)->where('location',1)->orderby(DB::raw('RAND()'))->take(1)->get()->first();

        $settingData = DB::table('settings')->select('meta_title','meta_key','meta_desc','product_name')->where('id',1)->get()->first();

        $equipmentTypeCat   = fetchSingleRow('categories','*',array('id'=>2,'display_status'=>1));
        $healthCareCats     = fetchSingleRow('categories','*',array('id'=>1,'display_status'=>1));
        if(isset($equipmentTypeCat->id)){
            $equipmentTypeSubCats= DB::table('categories')
            ->select('id','image','name','slug')
            ->where(['parent_id'=>$equipmentTypeCat->id,'display_status'=>1])
            ->orderby('name')
            ->get();
        }
        
        if(isset($healthCareCats->id)){
            $healthCareSubCats= DB::table('categories')
            ->select('id','image','name','slug')
            ->where(['parent_id'=>$healthCareCats->id,'display_status'=>1])
            ->orderby('name')
            ->get();
        }

        // Featured Manufacturers
        $featuredManufacturers = DB::table('manufacturer')
            ->select('id','image','name','slug')
            ->where('featured', 1)
            ->orderby('name')
            ->get();

        // Latest News
        $latestNews = DB::table('blogs')
            ->select('id','image','name','slug', 'content', 'created_at')
            ->orderby('id', 'desc')
            ->limit(3)
            ->get();
        
        // Latest Testimonials
        $latestTestimonials = DB::table('testimonials')
            ->select('id','image','name','content', 'location')
            ->orderby('display_order', 'asc')
            ->limit(2)
            ->get();
        
        //Auctions
        $current_datetime = date("Y-m-d H:i:s");
        $allAuctions = DB::select("
        SELECT A.*, B.name, B.image, B.slug FROM auctions A 
        LEFT JOIN products B on A.product_id = B.id  
        WHERE a.status = '1' AND a.featured = '1' 
        AND CONCAT(end_date,' ', end_time) > '".$current_datetime."'
        ORDER BY end_date ASC
        ");
        

        $view_data['allAuctions']           = $allAuctions;
        $view_data['sliderData']            = $sliderData;
        $view_data['advertisementData']     = $advertisementData;
        $view_data['featuredProducts']      = $featuredProducts;
        $view_data['equipmentTypeSubCats']  = $equipmentTypeSubCats;
        $view_data['healthCareSubCats']     = $healthCareSubCats;
        $view_data['equipmentTypeCat']      = $equipmentTypeCat;
        $view_data['healthCareCats']        = $healthCareCats;
        $view_data['currPage']              = 'home';
        $view_data['meta_title']            = $settingData->meta_title;
        $view_data['meta_key']              = $settingData->meta_key;
        $view_data['meta_desc']             = $settingData->meta_key;
        $view_data['page_title']            = $settingData->product_name;
        $view_data['featuredManufacturers'] = $featuredManufacturers;
        $view_data['latestNews'] = $latestNews;
        $view_data['latestTestimonials'] = $latestTestimonials;

        return view('index',$view_data);
    }
    public function updateCurrency()
    {
        $updated_at = currentDBDate();
        $allData = DB::table('currencies')
            ->orderby('defaultC','DESC')
            ->get();
        if(!empty($allData)){
            $default_currency = '';
            $x=0;
            foreach ($allData as $singleData) {
                $x++;
                if($x==1){
                    $default_currency = $singleData->code;
                }else{
                    $exchangeResponse = exchangeRate(1,$default_currency,$singleData->code);
                    if(!empty($exchangeResponse)){
                        $resData = json_decode($exchangeResponse);
                        $toArray = $resData->to;
                        if(is_array($toArray)){
                            foreach ($toArray as $singleValue) {
                                $exchange_rate = $singleValue->mid;
                                $quotecurrency = $singleValue->quotecurrency;
                                if($exchange_rate>0){
                                    DB::table('currencies')
                                    ->where('id',$singleData->id)
                                    ->update(['exchange_rate'=>$exchange_rate,'updated_at'=>$updated_at]);
                                }
                            }
                        }                            
                    }
                }                
            }
        }
        echo "Exchange Rate has been updated successfully.";
    }
}