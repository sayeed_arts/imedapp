<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
//use Image;
use Hash;
use Validator;


class ImportController extends Controller
{
	protected $currentUser;
    public function __construct(){
        /*$this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->currentUser = Auth::user();               
            return $next($request);
        });*/
    }
    /*
        $allCategories = DB::table('categories')->get();
        foreach ($allCategories as $singleCat) {
            if(empty($singleCat->image)){
                $catName    = $singleCat->name;
                $catId      = $singleCat->id;
                $getCatData = DB::table('importdata_category')->where('name',$catName)->first();
                if(!empty($getCatData->images)){
                    $singleImage = $getCatData->images;
                    if(strpos($singleImage, 'https://') !== false){
                        $singleImageArr = explode('?', $singleImage);
                        $singleImage2 = (isset($singleImageArr[0]))?$singleImageArr[0]:'';
                        if(!empty($singleImage2)){                           
                            $content = @file_get_contents($singleImage2);
                            if(!empty($content)){
                                $imageNArray    = explode('/', $singleImage2);
                                $imageNArray    = array_reverse($imageNArray);
                                $imageName      = (isset($imageNArray[0]))?randomFilename(15).'--'.$imageNArray[0]:'';
                                $path           = Storage::disk('s3')->put('uploaded_images/'.$imageName, $content);
                                $singleImage    = 'https://imedicalshop.s3.amazonaws.com/uploaded_images/'.$imageName;
                            }
                        } 
                    }    

                    $updateArray = array();
                    //$updateArray['content'] = $getCatData->content;
                    $updateArray['image']   = $singleImage;


                    DB::table('categories')->where('id',$catId)->update($updateArray);
                }
            }            
        }
    */
    /*
        $allCategories = DB::table('categories')->get();
        foreach ($allCategories as $singleCat) {
            if(empty($singleCat->image)){
                $catName    = $singleCat->name;
                $catId      = $singleCat->id;
                $getCatData = DB::table('importdata_category')->where('name',$catName)->first();
                if(!empty($getCatData->images)){
                    $singleImage = $getCatData->images;
                    if(strpos($singleImage, 'https://') !== false){
                        $singleImageArr = explode('?', $singleImage);
                        $singleImage2 = (isset($singleImageArr[0]))?$singleImageArr[0]:'';
                        if(!empty($singleImage2)){                           
                            $content = @file_get_contents($singleImage2);
                            if(!empty($content)){
                                $imageNArray    = explode('/', $singleImage2);
                                $imageNArray    = array_reverse($imageNArray);
                                $imageName      = (isset($imageNArray[0]))?randomFilename(15).'--'.$imageNArray[0]:'';
                                $path           = Storage::disk('s3')->put('uploaded_images/'.$imageName, $content);
                                $singleImage    = 'https://imedicalshop.s3.amazonaws.com/uploaded_images/'.$imageName;
                            }
                        } 
                    }    

                    $updateArray = array();
                    //$updateArray['content'] = $getCatData->content;
                    $updateArray['image']   = $singleImage;


                    DB::table('categories')->where('id',$catId)->update($updateArray);
                }
            }            
        }
    */
    /*public function dataImporter(){
        $currentUser    = $this->currentUser;
        $view_data      = array();        
        $view_data['currPage']          = 'import-data';
        $view_data['meta_title']        = 'Import Data';
        $view_data['meta_key']          = 'Import Data';
        $view_data['meta_desc']         = 'Import Data';
        $view_data['page_title']        = 'Import Data';
        $view_data['show_datatable']    = 'no';
        $view_data['currentUser']       = $currentUser;
        return view('import/import-data', $view_data);
    }*/
    /*public function uploadData(Request $request){
        $updated_at     = currentDBDate();
        $importType     = $request->input('importType');
        $datafile       = $request->file('datafile');
        if($datafile!=''){
            $filename   = $datafile->getClientOriginalName();
            $extension  = $datafile->getClientOriginalExtension();
            $tempPath   = $datafile->getRealPath();
            $fileSize   = $datafile->getSize();
            $mimeType   = $datafile->getMimeType();
            // Valid File Extensions
            $valid_extension = array("csv");
            if(in_array(strtolower($extension),$valid_extension)){                    
                $datafile_file      = randomFilename(15).'.'.$datafile->getClientOriginalExtension();
                $destinationPath    = public_path('/importeddata');
                $datafile->move($destinationPath, $datafile_file);
                $datafile_file      = 'importeddata/'.$datafile_file;
                $filepath           = public_path($datafile_file);
                $datafile           = fopen($filepath,"r");
                $importData_arr     = array();
                $i = 0;    
                $y = 0;  

                if($importType==1){
                    //Import Product file
                    while (($filedata = fgetcsv($datafile)) !== FALSE) {                        
                        $num = count($filedata );
                        if($i>0){
                            $insert_array = array();                       
                            $insert_array['external_id']        = (isset($filedata[0]))?$filedata[0]:'';
                            $insert_array['SKU']                = (isset($filedata[1]))?$filedata[1]:'';
                            $insert_array['manufacturer_id']    = (isset($filedata[2]))?$filedata[2]:'';
                            $insert_array['condition_type']     = (isset($filedata[3]))?$filedata[3]:'';
                            $insert_array['name']               = (isset($filedata[4]))?$filedata[4]:'';
                            $insert_array['tag']                = (isset($filedata[5]))?$filedata[5]:'';
                            $insert_array['description']        = (isset($filedata[6]))?"$filedata[6]":'';
                            $insert_array['moreInfo']           = (isset($filedata[7]))?"$filedata[7]":'';                            
                            $insert_array['price']              = (isset($filedata[8]) && $filedata[8]>0)?$filedata[8]:0;
                            $insert_array['s_price']            = (isset($filedata[9]) && $filedata[9]>0)?$filedata[9]:0;                          
                            $insert_array['images']             = (isset($filedata[10]))?"$filedata[10]":'';
                            $insert_array['additional_images']  = (isset($filedata[11]))?"$filedata[11]":'';                            
                            $insert_array['category']           = (isset($filedata[17]))?$filedata[17]:'';
                            $insert_array['sub_category']       = (isset($filedata[18]))?"$filedata[18]":'';
                            $insert_array['sub_child_category'] = (isset($filedata[19]))?$filedata[19]:'';                            
                            $insert_array['visibility']         = 1;
                            $insert_array['published']          = 1;
                            $insert_array['status']             = 0;
                            $insert_array['created_at']         = $updated_at;
                            $insert_array['updated_at']         = $updated_at;
                            DB::table('importdata')->insert($insert_array);                        
                        }
                        $i++;
                    }
                }else if($importType==2){
                    //Import Product variant file
                    while (($filedata = fgetcsv($datafile)) !== FALSE) {
                        $num = count($filedata );
                        if($i>0){
                            $insert_array = array();                       
                            $insert_array['external_id']        = (isset($filedata[0]))?$filedata[0]:'';
                            $insert_array['name']               = (isset($filedata[5]))?$filedata[5]:'';
                            $insert_array['price']              = (isset($filedata[6]) && $filedata[6]>0)?$filedata[6]:0;
                            $insert_array['s_price']            = (isset($filedata[7]) && $filedata[7]>0)?$filedata[7]:0;
                            $insert_array['images']             = (isset($filedata[8]))?"$filedata[8]":'';
                            $insert_array['v_id']               = (isset($filedata[13]))?$filedata[13]:'';
                            $insert_array['v_sku']              = (isset($filedata[14]))?$filedata[14]:'';                            
                            $insert_array['v_featured_image']   = (isset($filedata[15]))?$filedata[15]:'';                            
                            $insert_array['status']             = 0;
                            $insert_array['created_at']         = $updated_at;
                            $insert_array['updated_at']         = $updated_at;
                            DB::table('importdata_variant')->insert($insert_array);                        
                        }
                        $i++;
                    }
                }else if($importType==3){
                    //Import Category file
                    while (($filedata = fgetcsv($datafile)) !== FALSE) {
                        $num = count($filedata );
                        if($i>0){
                            $insert_array = array();                       
                            $insert_array['external_id']        = (isset($filedata[0]))?$filedata[0]:'';
                            $insert_array['name']               = (isset($filedata[1]))?$filedata[1]:'';
                            $insert_array['content']            = (isset($filedata[3]))?$filedata[3]:'';
                            $insert_array['images']             = (isset($filedata[4]))?"$filedata[4]":'';                        
                            $insert_array['status']             = 0;
                            $insert_array['created_at']         = $updated_at;
                            $insert_array['updated_at']         = $updated_at;
                            DB::table('importdata_category')->insert($insert_array);                        
                        }
                        $i++;
                    }
                }
                

                fclose($datafile); 
                if($i>0)
                {
                    return redirect()
                    ->route('import-data')
                    ->withMessage($i.' records have been uploaded.');
                }else{
                    return redirect()
                    ->back()
                    ->withError('Please check your csv file. File format is wrong or you are trying to import invalid data.')
                    ->withInput();
                }                
            }else{
                return redirect()
                ->back()
                ->withError('Please upload only csv file.')
                ->withInput();
            }
        }else{
            return redirect()
                ->back()
                ->withError('Please upload csv file.')
                ->withInput();
        }
    }*/
    public function processData(){
        $limit              = 1000;
        $getData            = DB::table('importdata')->where('status',0)->limit($limit)->get();
        $destinationPath    = public_path('/uploaded_images');
        $created_at         = currentDBDate();
        $x=0;

        

        /*if(!empty($getData)){
            foreach ($getData as $singleData) {
                $x++;
                $productArray       = array();
                $external_id        = $singleData->external_id;                              
                $imagesArray        = array();               
                $imgArray           = array();
                $category           = $singleData->category;
                $sub_category       = $singleData->sub_category;
                $sub_child_category = $singleData->sub_child_category;
                
                $catArray = array();                
                if(!empty($category)){
                    $catArray = explode(',', $category);
                }
                $subCatArray = array();                
                if(!empty($sub_category)){
                    $subCatArray = explode(',', $sub_category);
                }
                $subChildCatArray = array();                
                if(!empty($sub_child_category)){
                    $subChildCatArray = explode(',', $sub_child_category);
                }
                $caregoryArray = array();
                if(!empty($catArray)){
                    foreach($catArray as $pCate){
                        if(!empty(trim($pCate))){
                            $pcatName = trim($pCate);
                            $getPCate = DB::table('categories')->where('name',$pcatName)->where('parent_id',0)->first();
                            if(!empty($getPCate)){
                                $caregoryArray[]=$getPCate->id;                                                             
                            }else{
                                $insertCategory = array();
                                $insertCategory['slug']          = create_slug($pcatName,'','categories','slug');
                                $insertCategory['name']          = $pcatName;
                                $insertCategory['parent_id']     = 0;
                                $insertCategory['image']         = '';
                                $insertCategory['content']       = '';
                                $insertCategory['meta_title']    = '';
                                $insertCategory['meta_key']      = '';
                                $insertCategory['meta_desc']     = '';
                                $insertCategory['display_order'] = 0;
                                $insertCategory['display_status']= 1;
                                $insertCategory['created_at']    = $created_at;
                                $insertCategory['updated_at']    = $created_at;                                
                                $caregoryArray[]=DB::table('categories')->insertGetId($insertCategory);
                            }
                        }                        
                    }
                }
                
                if(!empty($subCatArray)){
                    foreach($subCatArray as $subCats){
                        if(!empty(trim($subCats))){
                            $csuCatStr = explode('>', trim($subCats));
                            if(!empty($csuCatStr)){
                                $parentCat  = (isset($csuCatStr[0]))?trim($csuCatStr[0]):'';
                                $childCat   = (isset($csuCatStr[1]))?trim($csuCatStr[1]):'';
                                $getPaCate   = DB::table('categories')->where('name',$parentCat)->where('parent_id',0)->first();
                                if(!empty($getPaCate)){
                                    $parentCatId = $getPaCate->id;
                                    $childCatData = DB::table('categories')->where('name',$childCat)->where('parent_id',$parentCatId)->first();
                                    if(!empty($childCatData)){
                                        $caregoryArray[]=$childCatData->id;
                                    }else{
                                        $insertCategory = array();
                                        $insertCategory['slug']          = create_slug($childCat,'','categories','slug');
                                        $insertCategory['name']          = $childCat;
                                        $insertCategory['parent_id']     = $parentCatId;
                                        $insertCategory['image']         = '';
                                        $insertCategory['content']       = '';
                                        $insertCategory['meta_title']    = '';
                                        $insertCategory['meta_key']      = '';
                                        $insertCategory['meta_desc']     = '';
                                        $insertCategory['display_order'] = 0;
                                        $insertCategory['display_status']= 1;
                                        $insertCategory['created_at']    = $created_at;
                                        $insertCategory['updated_at']    = $created_at;
                                        $caregoryArray[] = DB::table('categories')->insertGetId($insertCategory);
                                    }
                                }
                            }
                        }                        
                    }
                }

                if(!empty($subChildCatArray)){
                    foreach($subChildCatArray as $subChildCats){
                        if(!empty(trim($subChildCats))){
                            $csuCatCatStr = explode('>', trim($subChildCats));
                            if(!empty($csuCatCatStr)){
                                $parentCat      = (isset($csuCatCatStr[0]))?trim($csuCatCatStr[0]):'';
                                $childCat       = (isset($csuCatCatStr[1]))?trim($csuCatCatStr[1]):'';
                                $subchildCat    = (isset($csuCatCatStr[2]))?trim($csuCatCatStr[2]):'';
                                $getPaCate      = DB::table('categories')->where('name',$parentCat)->where('parent_id',0)->first();
                                if(!empty($getPaCate)){
                                    $parentCatId = $getPaCate->id;
                                    $childCatData = DB::table('categories')->where('name',$childCat)->where('parent_id',$parentCatId)->first();
                                    if(!empty($childCatData)){
                                        $childCateId=$childCatData->id;
                                        $subChildCatData = DB::table('categories')->where('name',$subchildCat)->where('parent_id',$childCateId)->first();

                                        if(!empty($subChildCatData)){
                                            $caregoryArray[]=$subChildCatData->id;

                                        }else{
                                            $insertCategory = array();
                                            $insertCategory['slug']          = create_slug($subchildCat,'','categories','slug');
                                            $insertCategory['name']          = $subchildCat;
                                            $insertCategory['parent_id']     = $childCateId;
                                            $insertCategory['image']         = '';
                                            $insertCategory['content']       = '';
                                            $insertCategory['meta_title']    = '';
                                            $insertCategory['meta_key']      = '';
                                            $insertCategory['meta_desc']     = '';
                                            $insertCategory['display_order'] = 0;
                                            $insertCategory['display_status']= 1;
                                            $insertCategory['created_at']    = $created_at;
                                            $insertCategory['updated_at']    = $created_at;
                                            $caregoryArray[] = DB::table('categories')->insertGetId($insertCategory);
                                        }
                                    }
                                }
                            }
                        }                        
                    }
                }
        
                $productArray['category_id']            = @implode(',', $caregoryArray);

                DB::table('products')->where('external_id',$external_id)->update($productArray);
               
                DB::table('importdata')->where('id',$singleData->id)->update(['status'=>1]);
            }
        }*/

        /*$limit              = 100;
        $getData            = DB::table('products')->where('images_uploaded',0)->limit($limit)->get();
        $destinationPath    = public_path('/uploaded_images');
        $created_at         = currentDBDate();
        $x=0; 
        if(!empty($getData)){
            foreach ($getData as $singleData) {
                $x++;
                $singleImage       = $singleData->image;
                $additional_images = $singleData->additional_images;
                if(!empty($singleImage)){ 
                    if(strpos($singleImage, 'https://') !== false){
                        $singleImageArr = explode('?', $singleImage);
                        $singleImage2 = (isset($singleImageArr[0]))?$singleImageArr[0]:'';
                        if(!empty($singleImage2)){                           
                            $content = @file_get_contents($singleImage2);
                            if(!empty($content)){
                                $imageNArray    = explode('/', $singleImage2);
                                $imageNArray    = array_reverse($imageNArray);
                                $imageName      = (isset($imageNArray[0]))?randomFilename(15).'--'.$imageNArray[0]:'';
                                $path           = Storage::disk('s3')->put('uploaded_images/'.$imageName, $content);
                                $singleImage    = 'https://imedicalshop.s3.amazonaws.com/uploaded_images/'.$imageName;
                            }
                        } 
                    }                         
                }
                $imgArray = array();
                $imagesArray = array();
                if(!empty($additional_images)){
                    $imgArray           = explode('##~~##', $additional_images);    
                }                
                if(!empty($imgArray) && count($imgArray)>0){
                    foreach ($imgArray as $singleAddImage) {
                        if(!empty($singleAddImage)){
                            $singleImageArrAdd = explode('?', $singleAddImage);
                            $singleImage2Add = (isset($singleImageArrAdd[0]))?$singleImageArrAdd[0]:'';
                            if(!empty($singleImage2Add)){
                                $contentAdd = @file_get_contents($singleImage2Add);
                                if(!empty($contentAdd)){
                                    $imageNArrayAdd    = explode('/', $singleImage2Add);
                                    $imageNArrayAdd    = array_reverse($imageNArrayAdd);
                                    $imageNameAdd      = (isset($imageNArrayAdd[0]))?randomFilename(15).'--'.$imageNArrayAdd[0]:'';
                                    $pathAdd           = Storage::disk('s3')->put('uploaded_images/'.$imageNameAdd, $contentAdd);
                                    $singleImageAdd    = 'https://imedicalshop.s3.amazonaws.com/uploaded_images/'.$imageNameAdd;
                                    $imagesArray[]     = $singleImageAdd;
                                }  
                            }                            
                        }                                            
                    }
                }
                if(!empty($imagesArray)){
                    $additional_images = implode("##~~##", $imagesArray);
                }

                DB::table('products')
                ->where('id',$singleData->id)
                ->update(['image'=>$singleImage,'additional_images'=>$additional_images,'images_uploaded'=>1]);
            }
        }*/
        /*if(!empty($getData)){
            foreach ($getData as $singleData) {
                $x++;
                $productArray       = array();
                $external_id        = $singleData->external_id;
                $getVariantData     = DB::table('importdata_variant')->where('external_id',$external_id)->get();                
                $imagesArray        = array();               
                $imgArray           = array();               
                $first_image        = '';               
                $additionalImageData  = '';
                $additional_images = $singleData->additional_images;
                $additionalImagesArray = array();
                if(!empty($additional_images)){
                    $additionalImagesArray              = explode(',', $additional_images);    
                }  
                $imgArr = array();
                if(!empty($additionalImagesArray)){
                    foreach($additionalImagesArray as $imgPath){
                        if(!empty(trim($imgPath))){
                            $imgArr[] = trim($imgPath);
                        }                       
                    }
                }
                if(!empty($getVariantData)){
                    foreach($getVariantData as $singleVarPro){
                        $variantImages          = $singleVarPro->images;
                        $variantFeaturedImage   = $singleVarPro->v_featured_image;
                        if(!empty($variantFeaturedImage)){
                            $imgArr[] = $variantFeaturedImage;
                        }
                        if($singleData->images!=$variantImages){
                            $imgArr[] = $variantImages;
                        }
                    }
                }

                if(!empty($imgArr)){
                    $additionalImageData = implode("##~~##", $imgArr);
                }else{
                    $additionalImageData = $singleData->images;
                }

                $category           = $singleData->category;
                $sub_category       = $singleData->sub_category;
                $sub_child_category = $singleData->sub_child_category;
                
                $catArray = array();                
                if(!empty($category)){
                    $catArray = explode(',', $category);
                }

                $subCatArray = array();                
                if(!empty($sub_category)){
                    $subCatArray = explode(',', $sub_category);
                }
                $subChildCatArray = array();                
                if(!empty($sub_child_category)){
                    $subChildCatArray = explode(',', $sub_child_category);
                }
                $caregoryArray = array();
                if(!empty($catArray)){
                    foreach($catArray as $pCate){
                        if(!empty(trim($pCate))){
                            $pcatName = trim($pCate);
                            $getPCate = DB::table('categories')->where('name',$pcatName)->where('parent_id',0)->first();
                            if(!empty($getPCate)){
                                $caregoryArray[]=$getPCate->id;                                                             
                            }else{
                                $insertCategory = array();
                                $insertCategory['slug']          = create_slug($pcatName,'','categories','slug');
                                $insertCategory['name']          = $pcatName;
                                $insertCategory['parent_id']     = 0;
                                $insertCategory['image']         = '';
                                $insertCategory['content']       = '';
                                $insertCategory['meta_title']    = '';
                                $insertCategory['meta_key']      = '';
                                $insertCategory['meta_desc']     = '';
                                $insertCategory['display_order'] = 0;
                                $insertCategory['display_status']= 1;
                                $insertCategory['created_at']    = $created_at;
                                $insertCategory['updated_at']    = $created_at;                                
                                $caregoryArray[]=DB::table('categories')->insertGetId($insertCategory);
                            }
                        }                        
                    }
                }
                
                if(!empty($subCatArray)){
                    foreach($subCatArray as $subCats){
                        if(!empty(trim($subCats))){
                            $csuCatStr = explode('>', trim($subCats));
                            if(!empty($csuCatStr)){
                                $parentCat  = (isset($csuCatStr[0]))?trim($csuCatStr[0]):'';
                                $childCat   = (isset($csuCatStr[1]))?trim($csuCatStr[1]):'';
                                $getPaCate   = DB::table('categories')->where('name',$parentCat)->where('parent_id',0)->first();
                                if(!empty($getPaCate)){
                                    $parentCatId = $getPaCate->id;
                                    $childCatData = DB::table('categories')->where('name',$childCat)->where('parent_id',$parentCatId)->first();
                                    if(!empty($childCatData)){
                                        $caregoryArray[]=$childCatData->id;
                                    }else{
                                        $insertCategory = array();
                                        $insertCategory['slug']          = create_slug($childCat,'','categories','slug');
                                        $insertCategory['name']          = $childCat;
                                        $insertCategory['parent_id']     = $parentCatId;
                                        $insertCategory['image']         = '';
                                        $insertCategory['content']       = '';
                                        $insertCategory['meta_title']    = '';
                                        $insertCategory['meta_key']      = '';
                                        $insertCategory['meta_desc']     = '';
                                        $insertCategory['display_order'] = 0;
                                        $insertCategory['display_status']= 1;
                                        $insertCategory['created_at']    = $created_at;
                                        $insertCategory['updated_at']    = $created_at;
                                        $caregoryArray[] = DB::table('categories')->insertGetId($insertCategory);
                                    }
                                }
                            }
                        }                        
                    }
                }

                if(!empty($subChildCatArray)){
                    foreach($subChildCatArray as $subChildCats){
                        if(!empty(trim($subChildCats))){
                            $csuCatCatStr = explode('>', trim($subChildCats));
                            if(!empty($csuCatCatStr)){
                                $parentCat      = (isset($csuCatCatStr[0]))?trim($csuCatCatStr[0]):'';
                                $childCat       = (isset($csuCatCatStr[1]))?trim($csuCatCatStr[1]):'';
                                $subchildCat    = (isset($csuCatCatStr[2]))?trim($csuCatCatStr[2]):'';
                                $getPaCate      = DB::table('categories')->where('name',$parentCat)->where('parent_id',0)->first();
                                if(!empty($getPaCate)){
                                    $parentCatId = $getPaCate->id;
                                    $childCatData = DB::table('categories')->where('name',$childCat)->where('parent_id',$parentCatId)->first();
                                    if(!empty($childCatData)){
                                        $childCateId=$childCatData->id;
                                        $subChildCatData = DB::table('categories')->where('name',$subchildCat)->where('parent_id',$childCateId)->first();

                                        if(!empty($subChildCatData)){
                                            $caregoryArray[]=$subChildCatData->id;

                                        }else{
                                            $insertCategory = array();
                                            $insertCategory['slug']          = create_slug($subchildCat,'','categories','slug');
                                            $insertCategory['name']          = $subchildCat;
                                            $insertCategory['parent_id']     = $childCateId;
                                            $insertCategory['image']         = '';
                                            $insertCategory['content']       = '';
                                            $insertCategory['meta_title']    = '';
                                            $insertCategory['meta_key']      = '';
                                            $insertCategory['meta_desc']     = '';
                                            $insertCategory['display_order'] = 0;
                                            $insertCategory['display_status']= 1;
                                            $insertCategory['created_at']    = $created_at;
                                            $insertCategory['updated_at']    = $created_at;
                                            $caregoryArray[] = DB::table('categories')->insertGetId($insertCategory);
                                        }
                                    }
                                }
                            }
                        }                        
                    }
                }

                $manufacturerName = $singleData->manufacturer_id;
                $getmanufacturer = DB::table('manufacturer')->where('name',$manufacturerName)->first();

                if(!empty($getmanufacturer)){
                    $manufacturer_id    = $getmanufacturer->id;
                }else{
                    $manufacturer_array['name']             = $manufacturerName;
                    $manufacturer_array['display_order']    = 1;
                    $manufacturer_array['display_status']   = 1;
                    $manufacturer_array['created_at']       = $created_at;
                    $manufacturer_array['updated_at']       = $created_at;
                    
                    $manufacturer_id = DB::table('manufacturer')->insertGetId($manufacturer_array);
                }


                $category_id = '';            
                $productArray['category_id']            = @implode(',', $caregoryArray);
                $productArray['external_id']            = $external_id;
                $productArray['manufacturer_id']        = $manufacturer_id;
                $productArray['image']                  = $singleData->images;
                $productArray['additional_images']      = $additionalImageData;
                $productArray['price']                  = $singleData->price;
                $productArray['special_price']          = $singleData->s_price;
                $productArray['product_type']           = 1; //1: For Sale, 0: For Rent
                $productArray['inventory_management']   = 0;
                $productArray['stock_availability']     = 1;
                $productArray['qty']                    = 1;
                $productArray['total_products']         = 1;
                $productArray['slug']                   = create_slug($singleData->name,'','products','slug');
                $productArray['name']                   = $singleData->name;
                $productArray['content']                = $singleData->description;
                $productArray['sku']                    = $singleData->SKU;
                $productArray['display_status']         = 1;
                $productArray['tags']                   = $singleData->tag;
                $productArray['condition_type']         = $singleData->condition_type;
                $productArray['more_info']              = $singleData->moreInfo;
                $productArray['short_description']      = $singleData->specs;
                $productArray['created_at']             = $created_at;
                $productArray['updated_at']             = $created_at;
                
                $insertedId = DB::table('products')->insertGetId($productArray);
               
                if(!empty($getVariantData)){
                    foreach($getVariantData as $singleVarPro){
                        $variantName            = $singleVarPro->name;
                        $variantPrice           = $singleVarPro->price;
                        $variantSPrice          = $singleVarPro->s_price;

                        if($variantSPrice<$variantPrice && $variantSPrice>0){
                            $attribute_price = $variantSPrice;
                        }else{
                            $attribute_price = $variantPrice;
                        }
                        $getAttribute           = DB::table('attributes')->where('name',$singleData->name)->first();
                        if(!empty($getAttribute)){
                            $attributeId    = $getAttribute->id;
                            $set_id         = $getAttribute->set_id;
                        }else{
                            $data_array = array();
                            $set_id = 1;
                            $data_array['slug']             = create_slug($singleData->name,'','attributes','slug');           
                            $data_array['set_id']           = $set_id;
                            $data_array['name']             = $singleData->name;
                            $data_array['display_order']    = 1;
                            $data_array['display_status']   = 1;
                            $data_array['created_at']       = $created_at;
                            $data_array['updated_at']       = $created_at;
                            $data_array['values']           = '';
                            
                            $attributeId = DB::table('attributes')->insertGetId($data_array);
                        }
                        $data_value_array['set_id']           = $set_id;
                        $data_value_array['attribute_id']     = $attributeId;
                        $data_value_array['name']             = $variantName;
                        $data_value_array['display_order']    = 1;
                        $data_value_array['display_status']   = 1;
                        $data_value_array['created_at']       = $created_at;
                        $data_value_array['updated_at']       = $created_at;
                        $attrValId = DB::table('attribute_values')->insertGetId($data_value_array);
                        
                        
                        //Product Attribute Data
                        $attrArray                      = array();
                        $attrArray['product_id']        = $insertedId;
                        $attrArray['attribute_id']      = $attributeId;
                        $attrArray['attribute_value']   = $attrValId;
                        $attrArray['attribute_price']   = $attribute_price;
                        $attrArray['is_variation']      = 1;
                        $attrArray['created_at']        = $created_at;
                        $attrArray['updated_at']        = $created_at;                        

                        DB::table('products_attributes')->insertGetId($attrArray);
                    }
                }

                DB::table('importdata')->where('id',$singleData->id)->update(['status'=>1]);
            }
        } */
        echo $x.' records processed successfully.';       
    }
    /*public function processData(){
        $getData=DB::table('importdata')->where('status',0)->limit(1)->get();
        $destinationPath    = public_path('/uploaded_images');
        $created_at     = currentDBDate();
        $x=0; 
        if(!empty($getData)){
            foreach ($getData as $singleData) {
                $x++;
                $imagesArray        = array();               
                $imgArray           = array();               
                $first_image        = '';               
                $additional_images  = '';               
                $imagesString       = $singleData->images;
                if(!empty($imagesString)){
                    $imgArray           = explode(',', $imagesString);    
                }                
                if(!empty($imgArray) && count($imgArray)>0){
                    foreach ($imgArray as $singleImage) {
                        $imageNArray=explode('/', $singleImage);
                        $imageNArray = array_reverse($imageNArray);
                        $imageName= (isset($imageNArray[0]))?randomFilename(15).'--'.$imageNArray[0]:'';
                        $content = file_get_contents($singleImage);
                        file_put_contents($destinationPath. '/'.$imageName, $content);
                        $imagesArray[]='uploaded_images/'.$imageName;
                    }
                }
                if(!empty($imagesArray)){
                    $first_image = (isset($imagesArray[0]))?$imagesArray[0]:'';
                    $additional_images = implode("##~~##", $imagesArray);
                }
                
                $catName    = $singleData->category;
                $catData    = DB::table('categories')->where('name',$catName)->first();
                $productArray['image']              = $first_image;
                $productArray['additional_images']  = $additional_images;
                $productArray['price']              = $singleData->price;
                $productArray['special_price']      = $singleData->s_price;
                $productArray['product_type']       = 1; //1: For Sale, 0: For Rent
                $productArray['manufacturer_id']    = 0;
                $productArray['category_id']        = (isset($catData->parent_id))?$catData->parent_id:0;
                $productArray['child_category_id']  = (isset($catData->id))?$catData->id:0;
                $productArray['inventory_management'] = 0;                
                if($singleData->visibility=='visible'){
                    $productArray['stock_availability']=1;
                }else{
                    $productArray['stock_availability']=0;
                }
                $productArray['slug']           = create_slug($singleData->name,'','products','slug');
                $productArray['name']           = $singleData->name;
                $productArray['content']        = $singleData->description;
                $productArray['sku']            = $singleData->SKU;
                $productArray['display_status'] = $singleData->published;
                $productArray['tags']           = $singleData->tag;
                $productArray['more_info']      = $singleData->moreInfo;
                $productArray['short_description']  = $singleData->specs;
                $productArray['created_at']           = $created_at;
                $productArray['updated_at']           = $created_at;            

                DB::table('products')
                    ->insert($productArray);


                DB::table('importdata')->where('id',$singleData->id)->update(['status'=>1]);
            }
        } 
        echo $x.' records processed successfully.';       
    }*/
}