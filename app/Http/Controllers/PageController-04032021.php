<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Mail;
use Validator;

use Cookie;
use Session;
class PageController extends Controller
{	
    protected $currentUser;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->currentUser = Auth::user();            
            return $next($request);
        });
    }
    

    public function aboutUs(){
        $pageData                   = DB::table('pages')->where('id',1)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['currPage']      = 'aboutUs';
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('page',$view_data);
    }
    public function contact_us(){
        $view_data['details'] = DB::table('settings')->select('contact_address','contact_phone','contact_email')->where('id',1)->get()->first();
        $pageData                   = DB::table('pages')->where('id',5)->get()->first();
        $view_data['currPage']          = 'contactus';
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('contact_us',$view_data);
    }
    public function post_contact_us(Request $request){ 

        $validator = Validator::make($request->all(), [
          'name'        => 'required',
          'last_name'   => 'required',
          'email'       => 'required|email',
          'phone'       => 'required|numeric',
          'message'     => 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $generalSettings    = generalSettings();
            $contact['name']    = $request->input('name');
            $contact['last_name']   = $request->input('last_name');
            $contact['email']   = $request->input('email');
            $contact['phone']   = $request->input('phone');
            $contact['conmessage']= $request->input('message');
                        
            $subject        = 'Contact Us';
            $template       = 'emails.contactEmail';
            $to             = $generalSettings->admin_email;

            send_smtp_email($to,$subject,$contact,$template);
            
            return redirect()->back()->withMessage('Your request has been received successfully. we will contact you soon.');
        }
    }

    public function textualPage($slug=''){
        $pageData                   = DB::table('pages')->where('slug',$slug)->get()->first();
        if(!empty($pageData)){
            $view_data['pageData']      = $pageData;
            $view_data['meta_title']    = $pageData->meta_title;
            $view_data['meta_key']      = $pageData->meta_key;
            $view_data['meta_desc']     = $pageData->meta_desc;
            $view_data['page_title']    = $pageData->name;
            $view_data['currPage']      = $slug;
            return view('page',$view_data);
        }else{
            return redirect(url('/'));
        }        
    }

    /*public function termsOfSale(){
        $pageData                   = DB::table('pages')->where('id',3)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        $view_data['currPage']      = 'termsOfSale';
        return view('page',$view_data);
    }
    public function returnPolices(){
        $view_data['currPage']      = 'returnPolices';
        $pageData                   = DB::table('pages')->where('id',2)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('page',$view_data);
    }
    public function termsConditions(){
        $view_data['currPage']      = 'termsConditions';
        $pageData                   = DB::table('pages')->where('id',6)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('page',$view_data);
    }
    public function privacyPolicy(){
        $view_data['currPage']      = 'privacyPolicy';
        $pageData                   = DB::table('pages')->where('id',4)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('page',$view_data);
    }
    public function returnsRefunds(){
        $view_data['currPage']      = 'returnsRefunds';
        $pageData                   = DB::table('pages')->where('id',7)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('page',$view_data);
    }
    public function shippingDeliveryPolicy(){
        $view_data['currPage']      = 'shippingDeliveryPolicy';
        $pageData                   = DB::table('pages')->where('id',8)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('page',$view_data);
    }*/


    public function faq(){
        $faqs                       = DB::table('faqs')->where('display_status',1)->orderby('display_order')->get();
        $view_data['faqs']          = $faqs;
        $view_data['currPage']      = 'faq';        
        $pageData                   = DB::table('pages')->where('id',11)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('faq',$view_data);
    }
    public function projects(){
        $allCurrentRecord                  = DB::table('projects')->where('type',0)->where('display_status',1)->orderby('display_order')->get();
        $allFutureRecord                  = DB::table('projects')->where('type',2)->where('display_status',1)->orderby('display_order')->get();
        $allPastRecord                  = DB::table('projects')->where('type',1)->where('display_status',1)->orderby('display_order')->get();
        $view_data['allCurrentRecord']     = $allCurrentRecord;
        $view_data['allFutureRecord']       = $allFutureRecord;
        $view_data['allPastRecord']         = $allPastRecord;
        $view_data['currPage']      = 'projects';        
        $pageData                   = DB::table('pages')->where('id',10)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('projects',$view_data);
    }
    public function projectDetails($slug=''){
        $view_data['currPage']      = 'projects';        
        $pageData                   = DB::table('projects')->where('display_status',1)->where('slug',$slug)->first();
        if(!empty($pageData)){
            $view_data['pageData']      = $pageData;
            $view_data['meta_title']    = $pageData->meta_title;
            $view_data['meta_key']      = $pageData->meta_key;
            $view_data['meta_desc']     = $pageData->meta_desc;
            $view_data['page_title']    = $pageData->name;
            return view('projectDetails',$view_data); 
        }else{
            return redirect(url('projects'));
        }        
    }
    public function services(){
        $allRecord                  = DB::table('services')->where('display_status',1)->orderby('display_order')->get();
        $view_data['allRecord']     = $allRecord;
        $view_data['currPage']      = 'services';        
        $pageData                   = DB::table('pages')->where('id',9)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('services',$view_data);
    }
    public function serviceDetails($slug=''){
        $view_data['currPage']      = 'services';        
        $pageData                   = DB::table('services')->where('display_status',1)->where('slug',$slug)->first();
        if(!empty($pageData)){
            $equipment_type = array();
            $allSerCat      = DB::table('service_category')
            ->where('display_status',1)
            //->whereRaw('FIND_IN_SET('.$pageData->id.',services)')
            ->orderby('display_order','ASC')
            ->get();

            $serviceLangData = getLanguageReconds('services_translations',array('name','content','offered'),array('services_id'=>$pageData->id));
            if(!empty($serviceLangData->name)){
                $serviceName = $serviceLangData->name;
            }else{
                $serviceName = $pageData->name;
            }
            if(!empty($serviceLangData->content)){
                $serviceContent = $serviceLangData->content;
            }else{
                $serviceContent = $pageData->content;
            }

            /*if(!empty($serviceLangData->offered)){
                $equipStr = $serviceLangData->offered;
            }else{
                $equipStr = $pageData->offered;
            }
            if(!empty($equipStr)) $equipment_type = explode("\n", str_replace("\r", "", $equipStr));*/
            $equipment_type = array();

            $view_data['pageData']      = $pageData;
            $view_data['meta_title']    = $pageData->meta_title;
            $view_data['meta_key']      = $pageData->meta_key;
            $view_data['meta_desc']     = $pageData->meta_desc;
            $view_data['page_title']    = $serviceName;
            $view_data['serviceContent']= $serviceContent;
            $view_data['equipment_type']= $equipment_type;
            $view_data['allSerCat']     = $allSerCat;
            return view('serviceDetails',$view_data); 
        }else{
            return redirect(url('services'));
        }        
    }    
    public function postService(Request $request){
        $validator = Validator::make($request->all(), [
          'hospital_company'=> 'required',
          'name'            => 'required',
          'phone'           => 'required',
          'postal_code'     => 'required',
          'email'           => 'required|email',
          'service_category'=> 'required',
          'equipment_type'  => 'required',
          'serial_number'   => 'required',
          'amount'          => 'required',
          'product_issue'   => 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
        	$updated_at     			= currentDBDate();
            $generalSettings            = generalSettings();
            $contact['hospital_company']= $request->input('hospital_company');
            $contact['name']            = $request->input('name');
            $contact['phone']           = $request->input('phone');
            $contact['postal_code']     = $request->input('postal_code');
            $contact['extension']       = $request->input('extension');
            $contact['email']           = $request->input('email');
            $contact['PO']              = $request->input('PO');
            $contact['service_category']= $request->input('service_category');
            $contact['equipment_type']  = $request->input('equipment_type');
            $contact['serial_number']   = $request->input('serial_number');
            $contact['amount']          = $request->input('amount');
            $contact['product_issue']   = $request->input('product_issue');
            $contact['serviceName']     = $request->input('service_name');
            $contact['serviceslug']     = $request->input('service_slug');
            $subject        = 'Service Enquiry Form ';
            $template       = 'emails.serviceEmail';
            $to             = $generalSettings->admin_email;
            send_smtp_email($to,$subject,$contact,$template);  

            //To User

            $templateUser       = 'emails.userServiceEmail';
            $toUser             = (isset($contact['email']) && !empty($contact['email']))?$contact['email']:'';
            if(!empty($toUser)){
                send_smtp_email($toUser,$subject,$contact,$templateUser); 
            }

            if(isset($contact['equipment_type'])){
            	$contact['equipment_type'] = implode('########', $contact['equipment_type']);
            }
            if(isset($contact['serial_number'])){
            	$contact['serial_number'] = implode('########', $contact['serial_number']);
            }
            if(isset($contact['amount'])){
            	$contact['amount'] = implode('########', $contact['amount']);
            }
            if(isset($contact['product_issue'])){
            	$contact['product_issue'] = implode('########', $contact['product_issue']);
            }
            $contact['status'] = 1;
            $contact['created_at'] = $updated_at;
            $contact['updated_at'] = $updated_at;
            $added_id = DB::table('services_repair')->insertGetId($contact);
            return redirect()->back()->withMessage('Your request has been received successfully. we will contact you soon.');
        }
    }
    public function getaquote(Request $request){
        $email = $request->input('email');
        if(!empty($email)){
            $updated_at     = currentDBDate();
            $response_array = array('stat'=>'1');
            $generalSettings            = generalSettings();
            $insert_array['email']              = $request->input('email');
            $insert_array['organization']       = $request->input('organization');
            $insert_array['phone']              = $request->input('phone');
            $insert_array['comment']            = $request->input('comment');
            $insert_array['qty']                = $request->input('qty');
            $insert_array['firstname']          = $request->input('firstname');
            $insert_array['lastname']           = $request->input('lastname');
            $insert_array['street']             = $request->input('street');
            $insert_array['city']               = $request->input('city');
            $insert_array['state']              = $request->input('state');
            $insert_array['country']            = $request->input('country');
            $insert_array['zip']                = $request->input('zip');
            $insert_array['personalnotes']      = $request->input('personalnotes');
            $insert_array['quoteProductName']   = $request->input('quoteProductName');
            $insert_array['quoteProductUrl']    = $request->input('quoteProductUrl');
            $subject        = 'Get Quote ';
            $template       = 'emails.getaquote';
            $to             = $generalSettings->admin_email;
            send_smtp_email($to,$subject,$insert_array,$template);
            $insert_array['status'] = 1;
            $insert_array['created_at'] = $updated_at;
            $insert_array['updated_at'] = $updated_at;

            $added_id = DB::table('getaquote')->insertGetId($insert_array);
        }else{
            $response_array = array('stat'=>'0');
        }
        echo json_encode($response_array);
    }
    public function newsletter(Request $request){
        $email_address = $request->input('newsletter_email');
        if(!empty($email_address)){
            $updated_at     = currentDBDate();
            $response_array = array('stat'=>'1');
            $is_exists      = DB::table('newsletters')->select('id')->where('email_address',$email_address)->get();
            if(count($is_exists)==0){
                $insert_array = array(
                    'email_address'=>$email_address,
                    'status'=>1,
                    'created_at'=>$updated_at,
                    'updated_at'=>$updated_at
                );
                $added_id = DB::table('newsletters')->insertGetId($insert_array);
            }
        }else{
            $response_array = array('stat'=>'0');
        }
        echo json_encode($response_array);
    }
    public function getstates($countryId=''){
        $allState = DB::table('states')->select('stateID','stateName','countryID')->where(array('status'=>'1','countryID'=>$countryId))->orderby('stateName')->get();
        if(!empty($allState)){
            $state_str = "<option value=\"\">Select State</option>";
            foreach ($allState as $singleState) {
                $state_str .= "<option value=\"".$singleState->stateID."\">".$singleState->stateName."</option>";
            }
        }else{
            $state_str = "<option value=\"\">Select State</option>";
        }
        echo $state_str;
    }
    public function getcities($state_id=''){
        $all_cities = DB::table('cities')->select('id','city')->where(array('display_status'=>'1','state_id'=>$state_id))->orderby('city')->get();
        if(!empty($all_cities)){
            $city_str = "<option value=\"\">City</option>";
            foreach ($all_cities as $singlecity) {
                $city_str .= "<option value=\"".$singlecity->id."\">".$singlecity->city."</option>";
            }
        }else{
            $city_str = "<option value=\"\">City</option>";
        }
        echo $city_str;
    }
    public function getzipcode($city_id=''){
        $all_zipcodes = DB::table('zipcodes')->select('id','zipcode','latitude','longitude')->where(array('display_status'=>'1','city_id'=>$city_id))->orderby('id')->get();
        if(!empty($all_zipcodes)){
            $zip_str = "<option value=\"\">Zipcode</option>";
            foreach ($all_zipcodes as $singleZip) {
                $zip_str .= "<option value=\"".$singleZip->zipcode."\">".$singleZip->zipcode."</option>";
            }
        }else{
            $zip_str = "<option value=\"\">Zipcode</option>";
        }
        echo $zip_str;
    }
    public function getcitiesstates($zipcode=''){
        $where_array = array('zipcodes.display_status'=>1);
        $where_array = array('cities.display_status'=>1);
        $where_array = array('states.display_status'=>1);
        if(!empty($zipcode)){
            $where_array[]=array('zipcodes.zipcode','like','%'.$zipcode.'%');            
        }
        $all_zipcodes   = DB::table('zipcodes')
            ->join('cities', 'zipcodes.city_id', '=', 'cities.id')
            ->join('states', 'cities.state_id', '=', 'states.id')
            ->select('zipcodes.zipcode', 'cities.city', 'states.state')
            ->where($where_array)
            ->orderby('cities.city','ASC')
            ->get();        
        if(!empty($all_zipcodes)){
            $response_array = array();
            foreach ($all_zipcodes as $singleZip) {
                $response_array[] = $singleZip->city.', '.$singleZip->state.', '.$singleZip->zipcode;
            }
        }
        echo json_encode($response_array);
    }
    
    public function logout(Request $request) {
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        Session::flush();
        $request->session()->regenerate();
        Cookie::queue(Cookie::make('imedcartcookie', '', -1440));
        Session::flash('succ_message', 'Logged out Successfully');
        Auth::logout();
        return redirect('login');
    }
}