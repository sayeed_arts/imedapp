<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Mail;
use Validator;

use Cookie;
use Session;
use DateTime;
use App\JobCategory;
use App\JobLocation;
use App\JobType;
use App\TrainingCategory;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{	
    protected $currentUser;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->currentUser = Auth::user();            
            return $next($request);
        });
    }
    

    public function aboutUs(){
        $pageData                   = DB::table('pages')->where('id',1)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['currPage']      = 'aboutUs';
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('page',$view_data);
    }
    public function contact_us(){
        $view_data['details'] = DB::table('settings')->select('contact_address','contact_phone','contact_email')->where('id',1)->get()->first();
        $pageData                   = DB::table('pages')->where('id',5)->get()->first();
        $view_data['currPage']          = 'contactus';
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('contact_us',$view_data);
    }
    public function post_contact_us(Request $request){ 

        $validator = Validator::make($request->all(), [
          'name'        => 'required',
          'last_name'   => 'required',
          'email'       => 'required|email',
          'phone'       => 'required|numeric',
          'message'     => 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $generalSettings    = generalSettings();
            $contact['name']    = $request->input('name');
            $contact['last_name']   = $request->input('last_name');
            $contact['email']   = $request->input('email');
            $contact['phone']   = $request->input('phone');
            $contact['conmessage']= $request->input('message');
                        
            $subject        = 'Contact Us';
            $template       = 'emails.contactEmail';
            $to             = $generalSettings->admin_email;

            send_smtp_email($to,$subject,$contact,$template);
            
            return redirect()->back()->withMessage('Your request has been received successfully. we will contact you soon.');
        }
    }

    public function textualPage($slug=''){
        $pageData                   = DB::table('pages')->where('slug',$slug)->get()->first();
        if(!empty($pageData)){
            $view_data['pageData']      = $pageData;
            $view_data['meta_title']    = $pageData->meta_title;
            $view_data['meta_key']      = $pageData->meta_key;
            $view_data['meta_desc']     = $pageData->meta_desc;
            $view_data['page_title']    = $pageData->name;
            $view_data['currPage']      = $slug;
            return view('page',$view_data);
        }else{
            return redirect(url('/'));
        }        
    }

    /*public function termsOfSale(){
        $pageData                   = DB::table('pages')->where('id',3)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        $view_data['currPage']      = 'termsOfSale';
        return view('page',$view_data);
    }
    public function returnPolices(){
        $view_data['currPage']      = 'returnPolices';
        $pageData                   = DB::table('pages')->where('id',2)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('page',$view_data);
    }
    public function termsConditions(){
        $view_data['currPage']      = 'termsConditions';
        $pageData                   = DB::table('pages')->where('id',6)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('page',$view_data);
    }
    public function privacyPolicy(){
        $view_data['currPage']      = 'privacyPolicy';
        $pageData                   = DB::table('pages')->where('id',4)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('page',$view_data);
    }
    public function returnsRefunds(){
        $view_data['currPage']      = 'returnsRefunds';
        $pageData                   = DB::table('pages')->where('id',7)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('page',$view_data);
    }
    public function shippingDeliveryPolicy(){
        $view_data['currPage']      = 'shippingDeliveryPolicy';
        $pageData                   = DB::table('pages')->where('id',8)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('page',$view_data);
    }*/


    public function faq(){
        $faqs                       = DB::table('faqs')->where('display_status',1)->orderby('display_order')->get();
        $view_data['faqs']          = $faqs;
        $view_data['currPage']      = 'faq';        
        $pageData                   = DB::table('pages')->where('id',11)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('faq',$view_data);
    }
    public function projects(){
        $allCurrentRecord                  = DB::table('projects')->where('type',0)->where('display_status',1)->orderby('display_order')->get();
        $allFutureRecord                  = DB::table('projects')->where('type',2)->where('display_status',1)->orderby('display_order')->get();
        $allPastRecord                  = DB::table('projects')->where('type',1)->where('display_status',1)->orderby('display_order')->get();
        $view_data['allCurrentRecord']     = $allCurrentRecord;
        $view_data['allFutureRecord']       = $allFutureRecord;
        $view_data['allPastRecord']         = $allPastRecord;
        $view_data['currPage']      = 'projects';        
        $pageData                   = DB::table('pages')->where('id',10)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('projects',$view_data);
    }
    public function projectDetails($slug=''){
        $view_data['currPage']      = 'projects';        
        $pageData                   = DB::table('projects')->where('display_status',1)->where('slug',$slug)->first();
        if(!empty($pageData)){
            $view_data['pageData']      = $pageData;
            $view_data['meta_title']    = $pageData->meta_title;
            $view_data['meta_key']      = $pageData->meta_key;
            $view_data['meta_desc']     = $pageData->meta_desc;
            $view_data['page_title']    = $pageData->name;
            return view('projectDetails',$view_data); 
        }else{
            return redirect(url('projects'));
        }        
    }
    public function serviceareas (){
        $servicecountry = 'USA';
        $view_data['allStates'] = DB::select("SELECT DISTINCT serviceareas.state, states.stateName FROM serviceareas LEFT JOIN states ON serviceareas.state = states.stateID ORDER BY states.stateName ASC, serviceareas.cityname ASC");
        $view_data['currPage']      = 'serviceareas';        
        $view_data['meta_title']    = '';
        $view_data['meta_key']      = '';
        $view_data['meta_desc']     = '';
        $view_data['page_title']    = '';
        return view('serviceareas',$view_data);
    }

    public function serviceareascitydetails($slug=''){
        $view_data['currPage']      = 'serviceareas';        
        $pageData                   = DB::table('serviceareas')->where('display_status',1)->where('slug',$slug)->first();
        if(!empty($pageData)){
            $view_data['pageData']      = $pageData;
            $view_data['meta_title']    = $pageData->meta_title;
            $view_data['meta_key']      = $pageData->meta_key;
            $view_data['meta_desc']     = $pageData->meta_desc;
            $view_data['page_title']    = $pageData->cityname;
            $view_data['content']    = $pageData->content;
            $view_data['image']    = $pageData->image;
            return view('serviceAreaCityDetails',$view_data); 
        }else{
            return redirect(url('serviceareas'));
        }        
    }

    public function services(){
        $allRecord                  = DB::table('services')->where('display_status',1)->orderby('display_order')->get();
        $view_data['allRecord']     = $allRecord;
        $view_data['currPage']      = 'services';        
        $pageData                   = DB::table('pages')->where('id',9)->get()->first();
        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        return view('services',$view_data);
    }
    public function serviceDetails($slug=''){
        $view_data['currPage']      = 'services';        
        $pageData                   = DB::table('services')->where('display_status',1)->where('slug',$slug)->first();
        if(!empty($pageData)){
            $equipment_type = array();
            $allSerCat      = DB::table('service_category')
            ->where('display_status',1)
            //->whereRaw('FIND_IN_SET('.$pageData->id.',services)')
            ->orderby('display_order','ASC')
            ->get();

            $serviceLangData = getLanguageReconds('services_translations',array('name','content','offered'),array('services_id'=>$pageData->id));
            if(!empty($serviceLangData->name)){
                $serviceName = $serviceLangData->name;
            }else{
                $serviceName = $pageData->name;
            }
            if(!empty($serviceLangData->content)){
                $serviceContent = $serviceLangData->content;
            }else{
                $serviceContent = $pageData->content;
            }

            /*if(!empty($serviceLangData->offered)){
                $equipStr = $serviceLangData->offered;
            }else{
                $equipStr = $pageData->offered;
            }
            if(!empty($equipStr)) $equipment_type = explode("\n", str_replace("\r", "", $equipStr));*/
            $equipment_type = array();

            $view_data['pageData']      = $pageData;
            $view_data['meta_title']    = $pageData->meta_title;
            $view_data['meta_key']      = $pageData->meta_key;
            $view_data['meta_desc']     = $pageData->meta_desc;
            $view_data['page_title']    = $serviceName;
            $view_data['serviceContent']= $serviceContent;
            $view_data['equipment_type']= $equipment_type;
            $view_data['allSerCat']     = $allSerCat;
            return view('serviceDetails',$view_data); 
        }else{
            return redirect(url('services'));
        }        
    } 
    public function serviceDetailsForm ($slug='',$category=''){
        $view_data['currPage']      = 'services';        
        $pageData                   = DB::table('services')->where('display_status',1)->where('slug',$slug)->first();
        if(!empty($pageData)){
            $equipment_type = array();
            $singleServiceCat      = DB::table('service_category')
            ->where('display_status',1)
            ->where('slug',$category)
            ->orderby('display_order','ASC')
            ->first();
            if(!empty($pageData)){
                $serviceLangData = getLanguageReconds('services_translations',array('name','content','offered'),array('services_id'=>$pageData->id));
                if(!empty($serviceLangData->name)){
                    $serviceName = $serviceLangData->name;
                }else{
                    $serviceName = $pageData->name;
                }
                if(!empty($serviceLangData->content)){
                    $serviceContent = $serviceLangData->content;
                }else{
                    $serviceContent = $pageData->content;
                }

                $serviceCatLangData = getLanguageReconds('service_category_translations',array('name','offered'),array('services_id'=>$singleServiceCat->id));
                if(!empty($serviceCatLangData->name)){
                    $serviceCatName = $serviceCatLangData->name;
                }else{
                    $serviceCatName = $singleServiceCat->name;
                }
                if(!empty($serviceCatLangData->offered)){
                    $equipStr = $serviceCatLangData->offered;
                }else{
                    $equipStr = $singleServiceCat->offered;
                }
                if(!empty($equipStr)) $equipment_type = explode("\n", str_replace("\r", "", $equipStr));

                $view_data['pageData']      = $pageData;
                $view_data['meta_title']    = $pageData->meta_title;
                $view_data['meta_key']      = $pageData->meta_key;
                $view_data['meta_desc']     = $pageData->meta_desc;
                $view_data['page_title']    = $serviceName;
                $view_data['serviceCatName']= $serviceCatName;
                $view_data['serviceContent']= $serviceContent;
                $view_data['equipment_type']= $equipment_type;
                $view_data['singleServiceCat']= $singleServiceCat;
                return view('serviceDetailsForm',$view_data); 
            }else{
                return redirect(url('/'));
            }
        }else{
            return redirect(url('/'));
        }        
    }    
    public function postService(Request $request){
        $validator = Validator::make($request->all(), [
          'hospital_company'=> 'required',
          'name'            => 'required',
          'phone'           => 'required',
          'postal_code'     => 'required',
          'email'           => 'required|email',
          'service_category'=> 'required',
          'equipment_type'  => 'required',
          'serial_number'   => 'required',
          'amount'          => 'required',
          'product_issue'   => 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
        	$updated_at     			= currentDBDate();
            $generalSettings            = generalSettings();
            $contact['hospital_company']= $request->input('hospital_company');
            $contact['name']            = $request->input('name');
            $contact['phone']           = $request->input('phone');
            $contact['postal_code']     = $request->input('postal_code');
            $contact['extension']       = $request->input('extension');
            $contact['email']           = $request->input('email');
            $contact['PO']              = $request->input('PO');
            $contact['service_category']= $request->input('service_category');
            $contact['equipment_type']  = $request->input('equipment_type');
            $contact['serial_number']   = $request->input('serial_number');
            $contact['amount']          = $request->input('amount');
            $contact['product_issue']   = $request->input('product_issue');
            $contact['serviceName']     = $request->input('service_name');
            $contact['serviceslug']     = $request->input('service_slug');
            $subject        = 'Service Enquiry Form ';
            $template       = 'emails.serviceEmail';
            $to             = $generalSettings->admin_email;
            send_smtp_email($to,$subject,$contact,$template);  

            //To User

            $templateUser       = 'emails.userServiceEmail';
            $toUser             = (isset($contact['email']) && !empty($contact['email']))?$contact['email']:'';
            if(!empty($toUser)){
                send_smtp_email($toUser,$subject,$contact,$templateUser); 
            }

            if(isset($contact['equipment_type'])){
            	$contact['equipment_type'] = implode('########', $contact['equipment_type']);
            }
            if(isset($contact['serial_number'])){
            	$contact['serial_number'] = implode('########', $contact['serial_number']);
            }
            if(isset($contact['amount'])){
            	$contact['amount'] = implode('########', $contact['amount']);
            }
            if(isset($contact['product_issue'])){
            	$contact['product_issue'] = implode('########', $contact['product_issue']);
            }
            $contact['status'] = 1;
            $contact['created_at'] = $updated_at;
            $contact['updated_at'] = $updated_at;
            $added_id = DB::table('services_repair')->insertGetId($contact);
            return redirect()->back()->withMessage('Your request has been received successfully. we will contact you soon.');
        }
    }
    public function getaquote(Request $request){
        $email = $request->input('email');
        if(!empty($email)){
            $updated_at     = currentDBDate();
            $response_array = array('stat'=>'1');
            $generalSettings            = generalSettings();
            $insert_array['email']              = $request->input('email');
            $insert_array['organization']       = $request->input('organization');
            $insert_array['phone']              = $request->input('phone');
            $insert_array['comment']            = $request->input('comment');
            $insert_array['qty']                = $request->input('qty');
            $insert_array['firstname']          = $request->input('firstname');
            $insert_array['lastname']           = $request->input('lastname');
            $insert_array['street']             = $request->input('street');
            $insert_array['city']               = $request->input('city');
            $insert_array['state']              = $request->input('state');
            $insert_array['country']            = $request->input('country');
            $insert_array['zip']                = $request->input('zip');
            $insert_array['personalnotes']      = $request->input('personalnotes');
            $insert_array['quoteProductName']   = $request->input('quoteProductName');
            $insert_array['product_id']   = base64_decode($request->input('quoteProductID'));

            $insert_array['quoteProductUrl']    = $request->input('quoteProductUrl');
            $subject        = 'Get Quote ';
            $template       = 'emails.getaquote';
            $to             = $generalSettings->admin_email;
            send_smtp_email($to,$subject,$insert_array,$template);
            $insert_array['status'] = 1;
            $insert_array['created_at'] = $updated_at;
            $insert_array['updated_at'] = $updated_at;

            $added_id = DB::table('getaquote')->insertGetId($insert_array);
        }else{
            $response_array = array('stat'=>'0');
        }
        echo json_encode($response_array);
    }
    public function newsletter(Request $request){
        $email_address = $request->input('newsletter_email');
        if(!empty($email_address)){
            $updated_at     = currentDBDate();
            $response_array = array('stat'=>'1');
            $is_exists      = DB::table('newsletters')->select('id')->where('email_address',$email_address)->get();
            if(count($is_exists)==0){
                $insert_array = array(
                    'email_address'=>$email_address,
                    'status'=>1,
                    'created_at'=>$updated_at,
                    'updated_at'=>$updated_at
                );
                $added_id = DB::table('newsletters')->insertGetId($insert_array);
            }
        }else{
            $response_array = array('stat'=>'0');
        }
        echo json_encode($response_array);
    }
    public function getstates($countryId=''){
        $allState = DB::table('states')->select('stateID','stateName','countryID')->where(array('status'=>'1','countryID'=>$countryId))->orderby('stateName')->get();
        if(!empty($allState)){
            $state_str = "<option value=\"\">Select State</option>";
            foreach ($allState as $singleState) {
                $state_str .= "<option value=\"".$singleState->stateID."\">".$singleState->stateName."</option>";
            }
        }else{
            $state_str = "<option value=\"\">Select State</option>";
        }
        echo $state_str;
    }
    public function getcities($state_id=''){
        $all_cities = DB::table('cities')->select('id','city')->where(array('display_status'=>'1','state_id'=>$state_id))->orderby('city')->get();
        if(!empty($all_cities)){
            $city_str = "<option value=\"\">City</option>";
            foreach ($all_cities as $singlecity) {
                $city_str .= "<option value=\"".$singlecity->id."\">".$singlecity->city."</option>";
            }
        }else{
            $city_str = "<option value=\"\">City</option>";
        }
        echo $city_str;
    }
    public function getzipcode($city_id=''){
        $all_zipcodes = DB::table('zipcodes')->select('id','zipcode','latitude','longitude')->where(array('display_status'=>'1','city_id'=>$city_id))->orderby('id')->get();
        if(!empty($all_zipcodes)){
            $zip_str = "<option value=\"\">Zipcode</option>";
            foreach ($all_zipcodes as $singleZip) {
                $zip_str .= "<option value=\"".$singleZip->zipcode."\">".$singleZip->zipcode."</option>";
            }
        }else{
            $zip_str = "<option value=\"\">Zipcode</option>";
        }
        echo $zip_str;
    }
    public function getcitiesstates($zipcode=''){
        $where_array = array('zipcodes.display_status'=>1);
        $where_array = array('cities.display_status'=>1);
        $where_array = array('states.display_status'=>1);
        if(!empty($zipcode)){
            $where_array[]=array('zipcodes.zipcode','like','%'.$zipcode.'%');            
        }
        $all_zipcodes   = DB::table('zipcodes')
            ->join('cities', 'zipcodes.city_id', '=', 'cities.id')
            ->join('states', 'cities.state_id', '=', 'states.id')
            ->select('zipcodes.zipcode', 'cities.city', 'states.state')
            ->where($where_array)
            ->orderby('cities.city','ASC')
            ->get();        
        if(!empty($all_zipcodes)){
            $response_array = array();
            foreach ($all_zipcodes as $singleZip) {
                $response_array[] = $singleZip->city.', '.$singleZip->state.', '.$singleZip->zipcode;
            }
        }
        echo json_encode($response_array);
    }
    
    public function logout(Request $request) {
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        Session::flush();
        $request->session()->regenerate();
        Cookie::queue(Cookie::make('imedcartcookie', '', -1440));
        Session::flash('succ_message', 'Logged out Successfully');
        Auth::logout();
        return redirect('login');
    }

    public function listJobs(Request $request){
        $perpage = 10;

        $sqlRecords = DB::table('jobs');
        $sqlRecords->leftJoin('jobcategories','jobs.category_id','=','jobcategories.id');
        $sqlRecords->leftJoin('joblocations','jobs.location_id','=','joblocations.id');
        $sqlRecords->leftJoin('jobtypes','jobs.type_id','=','jobtypes.id');
        $sqlRecords ->selectRaw('jobs.*, jobcategories.name AS jobcategory, joblocations.name AS joblocation, jobtypes.name AS jobtype');
        $sqlRecords->orderby('jobs.display_order', 'ASC');
        if(isset($request['category_id'])){
            $view_data['category_id']    = $request['category_id'];
            if($request['category_id'] != '0'){
                $sqlRecords->whereRaw('FIND_IN_SET('.$request['category_id'].',jobs.category_id)');
            }
        } else{
            $view_data['category_id']    = 0;
        }
        if(isset($request['location_id'])){
            $view_data['location_id']    = $request['location_id'];
            if($request['location_id'] != '0'){
                $sqlRecords->whereRaw('FIND_IN_SET('.$request['location_id'].',jobs.location_id)');
            }
        } else{
            $view_data['location_id']    = 0;
        }
        if(isset($request['type_id'])){
            $view_data['type_id']    = $request['type_id'];
            if($request['type_id'] != '0'){
                $sqlRecords->whereRaw('FIND_IN_SET('.$request['type_id'].',jobs.type_id)');
            }
        } else{
            $view_data['type_id']    = 0;
        }
        $allRecords = $sqlRecords->paginate($perpage);
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'alljobs';
        $view_data['allRecords']    = $allRecords;

        $allCategories = JobCategory::orderby('display_order','ASC')->get();  
        $allLocations = JobLocation::orderby('display_order','ASC')->get(); 
        $allTypes = JobType::orderby('display_order','ASC')->get();    

        $view_data['allCategories']    = $allCategories;
        $view_data['allLocations']    = $allLocations;
        $view_data['allTypes']    = $allTypes;

        $view_data['currPage']      = 'jobs';
        $view_data['meta_title']    = "Jobs";
        $view_data['meta_key']      = "Jobs";
        $view_data['meta_desc']     = "Jobs";
        $view_data['page_title']    = "Jobs";
        //dd($view_data);
        return view('listjobs',$view_data);
    }

    public function viewJobDetails($slug=""){
        if($slug != ""){
            $sqlRecords = DB::table('jobs');
            $sqlRecords->leftJoin('jobcategories','jobs.category_id','=','jobcategories.id');
            $sqlRecords->leftJoin('joblocations','jobs.location_id','=','joblocations.id');
            $sqlRecords->leftJoin('jobtypes','jobs.type_id','=','jobtypes.id');
            $sqlRecords ->selectRaw('jobs.*, jobcategories.name AS jobcategory, joblocations.name AS joblocation, jobtypes.name AS jobtype');
            $sqlRecords->where('jobs.slug', $slug);
            
            $jobDetails = $sqlRecords->first();
            if(!$jobDetails){
                return redirect(url('/jobs'));
            }else{
                //$view_data['jobdetails']    = $jobDetails;
                $view_data['meta_title']    = "Jobs";
                $view_data['meta_key']      = "Jobs";
                $view_data['meta_desc']     = "Jobs";
                $view_data['page_title']    = "Jobs";
                $applylink = "/apply-job/".$jobDetails->slug;
                //dd($view_data);
                return view('viewjobdetails', compact('view_data', 'jobDetails', 'applylink'));
            }
        }else{
            return redirect(url('/jobs'));
        }
    }

    public function applyJob($slug=""){
        if($slug != ""){
            $sqlRecords = DB::table('jobs');
            $sqlRecords->leftJoin('jobcategories','jobs.category_id','=','jobcategories.id');
            $sqlRecords->leftJoin('joblocations','jobs.location_id','=','joblocations.id');
            $sqlRecords->leftJoin('jobtypes','jobs.type_id','=','jobtypes.id');
            $sqlRecords ->selectRaw('jobs.*, jobcategories.name AS jobcategory, joblocations.name AS joblocation, jobtypes.name AS jobtype');
            $sqlRecords->where('jobs.slug', $slug);
            
            $jobDetails = $sqlRecords->first();
            if(!$jobDetails){
                return redirect(url('/jobs'));
            }else{
                //$view_data['jobdetails']    = $jobDetails;
                $view_data['meta_title']    = "Jobs";
                $view_data['meta_key']      = "Jobs";
                $view_data['meta_desc']     = "Jobs";
                $view_data['page_title']    = "Jobs";
                //dd($view_data);
                return view('applyjob', compact('view_data', 'jobDetails'));
            }
        }else{
            return redirect(url('/jobs'));
        }
    }

    public function applyJobSubmit(Request $request){ 
        $input = $request->all();
        $validator = Validator::make($request->all(), [
          'job_id'        => 'required',
          'fullname'   => 'required',
          'email'       => 'required|email',
          'phone'       => 'required',
          'coverletter'     => 'required',
          'current_location'     => 'required',
          'preferred_location'     => 'required',
          'resume'     => 'required'
        ]);
        
        if ($validator->fails()) {
            $failedRules = $validator->failed();
            dd($failedRules);
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $path_to_resume = '';
            $resume = $request->file('resume');
            if($resume!=''){
                $path = Storage::disk('s3')->put('uploaded_resumes', $request->resume);
                if(strpos($path, 'uploaded_resumes') !== false){
                    $path_to_resume   = s3Path().$path;      
                }
            }
            $updated_at     			= currentDBDate();
            $insert_array = array(
                'job_id'=>$request['job_id'],
                'fullname'=>$request['fullname'],
                'email'=>$request['email'],
                'phone'=>$request['phone'],
                'coverletter'=>$request['coverletter'],
                'current_location'=>$request['current_location'],
                'preferred_location'=>$request['preferred_location'],
                'resume'=>$path_to_resume,
                'created_at'=>$updated_at,
                'updated_at'=>$updated_at
            );
            $added_id = DB::table('jobapplications')->insertGetId($insert_array);

            return redirect()->back()->withMessage('You have successfully applied for this job.');
        }
    }


    public function listTrainings(Request $request){
        $perpage = 10;

        $sqlRecords = DB::table('trainings');
        $sqlRecords->leftJoin('trainingcategories','trainings.category_id','=','trainingcategories.id');
        $sqlRecords ->selectRaw('trainings.*, trainingcategories.name AS trainingcategory, DATE_FORMAT(start_date, "%M %e %Y %T") AS start_date, DATE_FORMAT(closing_date, "%M %e %Y %T") AS closing_date');
        $sqlRecords->orderby('trainings.display_order', 'ASC');
        if(isset($request['category_id'])){
            $view_data['category_id']    = $request['category_id'];
            if($request['category_id'] != '0'){
                $sqlRecords->whereRaw('FIND_IN_SET('.$request['category_id'].',trainings.category_id)');
            }
        } else{
            $view_data['category_id']    = 0;
        }
        
        $allRecords = $sqlRecords->paginate($perpage);
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'alltrainings';
        $view_data['allRecords']    = $allRecords;

        $allCategories = TrainingCategory::orderby('display_order','ASC')->get();  
        $view_data['allCategories']    = $allCategories;

        $view_data['currPage']      = 'trainings';
        $view_data['meta_title']    = "Trainings";
        $view_data['meta_key']      = "Trainings";
        $view_data['meta_desc']     = "Trainings";
        $view_data['page_title']    = "Trainings";
        //dd($view_data);
        return view('listtrainings',$view_data);
    }


    // SAYEED CODE

    public function listTrainigsByCategory($slug){
        if($slug != ''){
            $category = DB::table('trainingcategories')->where('slug',$slug)->first(); 
            if($category){ 
                $perpage = 10;
                $sqlRecords = DB::table('trainings');
                $sqlRecords->leftJoin('trainingcategories','trainings.category_id','=','trainingcategories.id');
                $sqlRecords ->selectRaw('trainings.*, trainingcategories.name AS trainingscategory, trainingcategories.slug AS trainingslug');
                $sqlRecords->orderby('trainings.display_order', 'ASC');
                $sqlRecords->whereRaw('FIND_IN_SET('.$category->id.',trainings.category_id)');
                            
                $allRecords = $sqlRecords->paginate($perpage);
                $view_data['hasTable']      = 'yes';
                $view_data['currPage']      = 'alltrainings';
                $view_data['allRecords']    = $allRecords;

                $allCategories = DB::table('trainingcategories')->orderby('display_order','ASC')->get();  
                
                $view_data['allCategories']    = $allCategories;

                $view_data['currPage']      = 'trainings';
                $view_data['meta_title']    = "Trainings";
                $view_data['meta_key']      = "Trainings";
                $view_data['meta_desc']     = "Trainings";
                $view_data['page_title']    = "Trainings";
                $view_data['category_id']    = $category->id;
                //dd($allRecords);
                return view('listtrainings',$view_data);
            } else{
                return redirect(url('/trainings'));
            }
        }else{
            return redirect(url('/trainings'));
        }
    }


    public function viewTrainingDetails($slug=""){
        if($slug != ""){
            $sqlRecords = DB::table('trainings');
            $sqlRecords->leftJoin('trainingcategories','trainings.category_id','=','trainingcategories.id');
            $sqlRecords ->selectRaw('trainings.*, trainingcategories.name AS trainingcategory, DATE_FORMAT(start_date, "%M %e %Y %T") AS start_date, DATE_FORMAT(closing_date, "%M %e %Y %T") AS closing_date');
            $sqlRecords->where('trainings.slug', $slug);
            
            $jobDetails = $sqlRecords->first();
            if(!$jobDetails){
                return redirect(url('/trainings'));
            }else{
                $view_data['meta_title']    = "trainings";
                $view_data['meta_key']      = "Trainings";
                $view_data['meta_desc']     = "Trainings";
                $view_data['page_title']    = "Trainings";
                $applylink = "/apply-training/".$jobDetails->slug;
                //dd($view_data);
                return view('viewtrainingdetails', compact('view_data', 'jobDetails', 'applylink'));
            }
        }else{
            return redirect(url('/trainings'));
        }
    }

    public function applyTraining($slug=""){
        if($slug != ""){
            $sqlRecords = DB::table('trainings');
            $sqlRecords->leftJoin('trainingcategories','trainings.category_id','=','trainingcategories.id');
            $sqlRecords ->selectRaw('trainings.*, trainingcategories.name AS trainingcategory, DATE_FORMAT(start_date, "%M %e %Y %T") AS start_date, DATE_FORMAT(closing_date, "%M %e %Y %T") AS closing_date');
            $sqlRecords->where('trainings.slug', $slug);
            
            $jobDetails = $sqlRecords->first();
            if(!$jobDetails){
                return redirect(url('/trainings'));
            }else{
                $view_data['meta_title']    = "Trainings";
                $view_data['meta_key']      = "Trainings";
                $view_data['meta_desc']     = "Trainings";
                $view_data['page_title']    = "Trainings";
                //dd($view_data);
                return view('applytraining', compact('view_data', 'jobDetails'));
            }
        }else{
            return redirect(url('/trainings'));
        }
    }

    public function applyTrainingSubmit(Request $request){ 
        $input = $request->all();
        $validator = Validator::make($request->all(), [
          'training_id'        => 'required',
          'fullname'   => 'required',
          'email'       => 'required|email',
          'phone'       => 'required'
        ]);
        
        if ($validator->fails()) {
            $failedRules = $validator->failed();
            dd($failedRules);
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $updated_at     			= currentDBDate();
            $insert_array = array(
                'training_id'=>$request['training_id'],
                'fullname'=>$request['fullname'],
                'email'=>$request['email'],
                'phone'=>$request['phone'],
                'created_at'=>$updated_at,
                'updated_at'=>$updated_at
            );
            $added_id = DB::table('trainingapplications')->insertGetId($insert_array);

            return redirect()->back()->withMessage('You have successfully applied for this service training.');
        }
    }

    public function getNewsDetails($slug=""){
        if($slug != ""){
            $sqlRecords = DB::table('blogs');
            $sqlRecords->leftJoin('blog_categories','blogs.category_id','=','blog_categories.id');
            $sqlRecords ->selectRaw('blogs.*, blog_categories.name AS blogcategory');
            $sqlRecords->where('blogs.slug', $slug);
            
            $newsDetails = $sqlRecords->first();
            if(!$newsDetails){
                return redirect(url('/news'));
            }else{
                $allCategories = DB::table('blog_categories')->orderby('display_order','ASC')->get();  

                $view_data['meta_title']    = "News";
                $view_data['meta_key']      = "News";
                $view_data['meta_desc']     = "News";
                $view_data['page_title']    = "News";
                return view('viewnewsdetails', compact('view_data', 'newsDetails', 'allCategories'));
            }
        }else{
            return redirect(url('/news'));
        }
    }

    public function listNews(Request $request){
        $perpage = 10;

        $sqlRecords = DB::table('blogs');
        $sqlRecords->leftJoin('blog_categories','blogs.category_id','=','blog_categories.id');
        $sqlRecords ->selectRaw('blogs.*, blog_categories.name AS blogcategory, blog_categories.slug AS blogslug');
        $sqlRecords->orderby('blogs.display_order', 'ASC');
        if(isset($request['category_id'])){
            $view_data['category_id']    = $request['category_id'];
            if($request['category_id'] != '0'){
                $sqlRecords->whereRaw('FIND_IN_SET('.$request['category_id'].',blogs.category_id)');
            }
        } else{
            $view_data['category_id']    = 0;
        }
        
        $allRecords = $sqlRecords->paginate($perpage);
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allnews';
        $view_data['allRecords']    = $allRecords;

        $allCategories = DB::table('blog_categories')->orderby('display_order','ASC')->get();  
         
        $view_data['allCategories']    = $allCategories;

        $view_data['currPage']      = 'news';
        $view_data['meta_title']    = "News";
        $view_data['meta_key']      = "News";
        $view_data['meta_desc']     = "News";
        $view_data['page_title']    = "News";
        $view_data['category_id']    = "";
        //dd($allRecords);
        return view('listnews',$view_data);
    }

    public function listNewsByCategory($slug){
        if($slug != ''){
            $category = DB::table('blog_categories')->where('slug',$slug)->first(); 
            if($category){ 
                $perpage = 10;
                $sqlRecords = DB::table('blogs');
                $sqlRecords->leftJoin('blog_categories','blogs.category_id','=','blog_categories.id');
                $sqlRecords ->selectRaw('blogs.*, blog_categories.name AS blogcategory, blog_categories.slug AS blogslug');
                $sqlRecords->orderby('blogs.display_order', 'ASC');
                $sqlRecords->whereRaw('FIND_IN_SET('.$category->id.',blogs.category_id)');
                            
                $allRecords = $sqlRecords->paginate($perpage);
                $view_data['hasTable']      = 'yes';
                $view_data['currPage']      = 'allnews';
                $view_data['allRecords']    = $allRecords;

                $allCategories = DB::table('blog_categories')->orderby('display_order','ASC')->get();  
                
                $view_data['allCategories']    = $allCategories;

                $view_data['currPage']      = 'news';
                $view_data['meta_title']    = "News";
                $view_data['meta_key']      = "News";
                $view_data['meta_desc']     = "News";
                $view_data['page_title']    = "News";
                $view_data['category_id']    = $category->id;
                //dd($allRecords);
                return view('listnews',$view_data);
            } else{
                return redirect(url('/news'));
            }
        }else{
            return redirect(url('/news'));
        }
    }

    public function auctions(){
        $current_datetime = date("Y-m-d H:i:s");
        $allauctions = DB::select("
        SELECT A.*, B.name, B.image, B.slug FROM auctions A 
        LEFT JOIN products B on A.product_id = B.id  
        WHERE a.status = '1' 
        AND CONCAT(end_date,' ', end_time) > '".$current_datetime."'
        ORDER BY end_date ASC
        ");
        $view_data['allRecords']     = $allauctions;
        $view_data['currPage']      = 'auctions';        
        $view_data['meta_title']    = "Auctions";
        $view_data['meta_key']      = "Auctions";
        $view_data['meta_desc']     = "Auctions";
        $view_data['page_title']    = "Auctions";
        return view('auctions',$view_data);
    }

    public function auctiondetail($encodedid){
        if($encodedid != ''){
            $id = base64_decode($encodedid);

            $current_datetime = date("Y-m-d H:i:s");
            /*$auctiondetails = DB::select("
            SELECT A.*, B.slug FROM auctions A 
            LEFT JOIN products B on A.product_id = B.id  
            WHERE 
            a.id = '".$id."' 
            AND a.status = '1' 
            AND CONCAT(end_date,' ', end_time) > '".$current_datetime."'
            ");*/
            $auctiondetails = DB::select("
            SELECT A.*, B.slug FROM auctions A 
            LEFT JOIN products B on A.product_id = B.id  
            WHERE 
            a.id = '".$id."' 
            ");
            $product_slug = $auctiondetails[0]->slug;
            $product_id = $auctiondetails[0]->product_id;
            $productdetails = DB::table('products')
                            ->where('id',$product_id)
                            ->get()
                            ->first();
            $view_data['averagerating'] = DB::table('product_ratings')
                                            ->select('*')
                                            ->where(['product_id'=>$product_id,'status'=>'1'])
                                            ->avg('product_ratings.rating');
            $view_data['totalcount'] = DB::table('product_ratings')
                                            ->select('*')
                                            ->where(['product_id'=>$product_id,'status'=>'1'])
                                            ->count('product_ratings.rating');

            $faq = DB::table('auction_faq')->where('auction_id',$id)->orderby('id')->get();;

            $showsavedforbidlater = true;
            if(Auth::user()){
                $currentuserid = Auth::user()->id;
                $savedforbidlater = DB::table('user_saved_bid_later')
                ->where('user_id',$currentuserid)
                ->where('auction_id',$id)
                ->get()
                ->first();
                if($savedforbidlater){
                    $showsavedforbidlater = false;
                }
            }
            $view_data['faq']  = $faq;
            $view_data['showsavedforbidlater']  = $showsavedforbidlater;
            $view_data['productdetails']     = $productdetails;
            $view_data['auctiondetails']     = $auctiondetails[0];
            $view_data['currPage']      = 'auctiondetails';        
            $view_data['meta_title']    = "Auctions";
            $view_data['meta_key']      = "Auctions";
            $view_data['meta_desc']     = "Auctions";
            $view_data['page_title']    = "Auctions";
            return view('auctiondetail',$view_data);
        }else {
            return redirect(url('/auctions'));
        }
    }

    public function auctiontimer($id){
        if($id != ''){
            $current_datetime = date("Y-m-d H:i:s");
            $auctiondetails = DB::select("SELECT CONCAT(end_date,' ', end_time) AS closing_datetime FROM auctions WHERE id = '".$id."' ");
            $closing_datetime = $auctiondetails[0]->closing_datetime;
            $start_date = new DateTime($current_datetime);
            $since_start = $start_date->diff(new DateTime($closing_datetime));

            $return_text = '';
            if($since_start->y >0){
                $return_text .= $since_start->y.' years ';
            }
            if($since_start->m >0){
                $return_text .= $since_start->m.' months ';
            }
            if($since_start->d >0){
                $return_text .= $since_start->d.' d ';
            }
            if($since_start->h >0){
                $return_text .= $since_start->h.' hrs ';
            }
            if($since_start->i >0){
                $return_text .= $since_start->i.' mins ';
            }
            if($since_start->s >0){
                $return_text .= $since_start->s.' secs ';
            }
            $retarr = [];
            $retarr['id'] = $id;
            $retarr['time_remain'] = $return_text;
            return $retarr;
        }else {
            return '';
        }
    }

    public function bidnow($id){
        if($id != ''){
            $bid = DB::select("SELECT FORMAT(bid_amount,2) AS bid_amount FROM auction_bids WHERE auction_id = '".$id."' ORDER BY id DESC LIMIT 0, 1");
            if($bid){
                $last_bid_amount = $bid[0]->bid_amount;
            } else{
                $auctiondetails = DB::select("SELECT FORMAT(min_bid_price,2) AS min_bid_price FROM auctions WHERE id = '".$id."' ");
                $last_bid_amount = $auctiondetails[0]->min_bid_price;
            }
            $retarr = [];
            $retarr['id'] = $id;
            $retarr['last_bid_amount'] = '$'.$last_bid_amount;
            return $retarr;
        }else {
            return '';
        }
    }

    public function submitbid(Request $request){
        $retarr = [];
        if(Auth::user()){
            $currentuserid = Auth::user()->id;
            $validator = Validator::make($request->all(), [
                'bid_amount'        => 'required|numeric'
            ]);
            if ($validator->fails()) {
                $retarr['type'] = 'error';
                $retarr['message'] = 'Please enter valid bid amount';
            } else{
                $bid_auction_id = $request->input('bid_auction_id');
                $bid_amount = $request->input('bid_amount');
                $bid = DB::select("SELECT bid_amount FROM auction_bids WHERE auction_id = '".$bid_auction_id."' ORDER BY id DESC LIMIT 0, 1");
                if($bid){
                    $last_bid_amount = $bid[0]->bid_amount;
                } else{
                    $auctiondetails = DB::select("SELECT min_bid_price FROM auctions WHERE id = '".$bid_auction_id."' ");
                    $last_bid_amount = $auctiondetails[0]->min_bid_price;
                }
                if($bid_amount <= $last_bid_amount){
                    $retarr['type'] = 'error';
                    $retarr['message'] = 'Please enter bid amount greater than '.$last_bid_amount;
                } else{
                    $insert_array['auction_id'] = $bid_auction_id;
                    $insert_array['user_id'] = $currentuserid;
                    $insert_array['bid_amount'] = $bid_amount;
                    $insert_array['status'] = '0';
                    $insert_array['created_date'] = date("Y-m-d H:i:s");
                    $added_bid = DB::table('auction_bids')->insertGetId($insert_array);
                    $retarr['type'] = 'success';
                    $retarr['message'] = 'You bid has been submitted.';
                }
            }
        } else{
            $retarr['type'] = 'error';
            $retarr['message'] = 'Please login to bid.';
        }
        return $retarr;
    }

    public function saveforbidlater($id){
        $retarr = [];
        if($id != ''){
            if(Auth::user()){
                $currentuserid = Auth::user()->id;
                $insert_array['auction_id'] = $id;
                $insert_array['user_id'] = $currentuserid;
                $insert_array['created_date'] = date("Y-m-d H:i:s");
                $added = DB::table('user_saved_bid_later')->insertGetId($insert_array);
                $retarr['type'] = 'success';
                $retarr['message'] = 'Auction has been saved for you to bid later.';
            } else{
                $retarr['type'] = 'error';
                $retarr['message'] = 'Invalid Request.';
            }
        }else {
            $retarr['type'] = 'error';
            $retarr['message'] = 'Invalid Request.';
        }
        return $retarr;
    }
}