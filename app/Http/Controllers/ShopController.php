<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;

use DB;
use Hash;
use Validator;
use Cookie;
use Session;

class ShopController extends Controller
{	
    protected $currentUser;
    public function __construct()
    {   
        $this->middleware(function ($request, $next) {
            $this->currentUser = Auth::user();            
            return $next($request);
        });
    }
    public function getCategory($slug='',$child_slug='',$sub_child_slug='',Request $request){
        $formSlug = 'shop';
        $isbrandpage = false;
        $whereArray = array(
            'products.display_status'=>1,
            'products.product_type'=>1
        );

        if(isset($_REQUEST['sortby'])){
            $sortby = trim($_REQUEST['sortby']);
            $request->session()->put('sortby', $sortby);
        }else{
            $sortby = $request->session()->get('sortby');
        }
        if(isset($_REQUEST['search'])){
            $search = trim($_REQUEST['search']);
            $search->session()->put('search', $search);
        }else{
            $search = $request->session()->get('search');
        }
        if(isset($_REQUEST['fromPrice'])){
            $fromPrice = trim($_REQUEST['fromPrice']);
            $request->session()->put('fromPrice', $fromPrice);
        }else{
            $fromPrice = $request->session()->get('fromPrice');
        }
        if(isset($_REQUEST['toPrice'])){
            $toPrice = trim($_REQUEST['toPrice']);
            $request->session()->put('toPrice', $toPrice);
        }else{
            $toPrice = $request->session()->get('toPrice');
        }
        if(isset($_REQUEST['selectedmanufacturer'])){
            $selectedArray =array();
            $yesCleared = 0;
            foreach ($_REQUEST['selectedmanufacturer'] as $value) {
                $selectedArray[]=$value; 
                if($value=='-1'){
                   $yesCleared = 1;
                }
            }
            if($yesCleared==1){
                $selectedmanufacturer ='';
            }else{
                $selectedmanufacturer = implode(',',$selectedArray);
            }            
            $request->session()->put('selectedmanufacturer', $selectedmanufacturer);
        }else{
            $selectedmanufacturer = $request->session()->get('selectedmanufacturer');
        }
        
        if(isset($_REQUEST['perpage'])){
            $perpage = trim($_REQUEST['perpage']);
            $request->session()->put('perpage', $perpage);
        }else{
            $perpage = $request->session()->get('perpage');
        }
        if($perpage<1){
            $perpage =16;
        }
        if(empty($fromPrice) || $fromPrice<0 || !is_numeric($fromPrice)){
            $fromPrice = 0;
        }
        if(empty($toPrice) || !is_numeric($toPrice)){
            $toPrice = 100000000;
        }
        $minimumAmount=0;
        $maximumAmount=100000;
        $sorting_column = array(
            ''=>'products.id',
            'title-asc'=>'products.name',
            'title-desc'=>'products.name',
            'date-asc'=>'products.updated_at',
            'date-desc'=>'products.updated_at',
            'price-asc'=>'products.price',
            'price-desc'=>'products.price',
        );
        $sorting_order = array(
            ''=>'desc',
            'title-asc'=>'asc',
            'title-desc'=>'desc',
            'date-asc'=>'asc',
            'date-desc'=>'desc',
            'price-asc'=>'asc',
            'price-desc'=>'desc',
        );        
        $ordercolumn = (isset($sorting_column[$sortby]))?trim($sorting_column[$sortby]):'products.id';
        $orderdir    = (isset($sorting_order[$sortby]))?trim($sorting_order[$sortby]):'DESC';
        $page        = (isset($_REQUEST['page']))?trim($_REQUEST['page']):'1';

        if(!empty($selectedmanufacturer)){
            $explodeMenuf=explode(',',$selectedmanufacturer);
            $whereArray[]=array( function ($query) use ($explodeMenuf) {
                $query->whereIn('products.manufacturer_id',$explodeMenuf);
            });
        }
        if(!empty($fromPrice) && !empty($toPrice)){
            $minPrice = (isset($fromPrice) && $fromPrice>0 && is_numeric($fromPrice))?$fromPrice:'0';
            $maxPrice = (isset($toPrice) && $toPrice>0 && is_numeric($toPrice))?$toPrice:'10000000';
            
            $minPrice = showFilterPrice($minPrice);
            $maxPrice = showFilterPrice($maxPrice);
            $whereArray[]=array( function ($query) use ($minPrice,$maxPrice) {
                $query->where('products.price','>=',$minPrice)
                    ->Where('products.price', '<=',$maxPrice);
            });
        }
        if(empty($fromPrice) && !empty($toPrice)){
            $maxPrice = (isset($toPrice) && $toPrice>0 && is_numeric($toPrice))?$toPrice:'10000000';
            $maxPrice = showFilterPrice($maxPrice);
            $whereArray[]=array( function ($query) use ($maxPrice) {
                $query->Where('products.price', '<=',$maxPrice);
            });
        }
        if(!empty($fromPrice) && empty($toPrice)){
            $minPrice = (isset($fromPrice) && $fromPrice>0 && is_numeric($fromPrice))?$fromPrice:'0';
            $minPrice = showFilterPrice($minPrice);
            $whereArray[]=array( function ($query) use ($minPrice) {
                $query->where('products.price','>=',$minPrice);
            });
        }        
        if($sub_child_slug!=''){
            $categoryData = DB::table('categories')->where('display_status',1)->where('slug',$sub_child_slug)->get()->first();
            if(!empty($categoryData)){                
                $formSlug = 'shop/'.$slug.'/'.$child_slug.'/'.$sub_child_slug;
                $allProducts = DB::table('products')                
                ->select('products.*')
                //->join('categories as childCts','products.child_category_id','=','childCts.id')
                //->where('childCts.display_status',1)
                //->where('products.child_category_id',$categoryData->id)
                ->whereRaw('FIND_IN_SET('.$categoryData->id.',products.category_id)')
                ->where($whereArray)                
                ->orderby($ordercolumn,$orderdir)
                ->paginate($perpage);

                $productCount = DB::table('products')
                //->join('categories as childCts','products.child_category_id','=','childCts.id')
                //->where('childCts.display_status',1)
                //->where('products.child_category_id',$categoryData->id)
                ->whereRaw('FIND_IN_SET('.$categoryData->id.',products.category_id)')
                ->where($whereArray)                
                ->orderby($ordercolumn,$orderdir)
                ->count();
            }else{
                return redirect(route('shop'));
            }
        }else if($child_slug!=''){
            $categoryData = DB::table('categories')->where('display_status',1)->where('slug',$child_slug)->get()->first();
            if(!empty($categoryData)){                
                $formSlug = 'shop/'.$slug.'/'.$child_slug;
                $allProducts = DB::table('products')                
                ->select('products.*')
                //->join('categories as childCts','products.child_category_id','=','childCts.id')
                //->where('childCts.display_status',1)
                //->where('products.child_category_id',$categoryData->id)
                ->whereRaw('FIND_IN_SET('.$categoryData->id.',products.category_id)')
                ->where($whereArray)                
                ->orderby($ordercolumn,$orderdir)
                ->paginate($perpage);

                $productCount = DB::table('products')
                //->join('categories as childCts','products.child_category_id','=','childCts.id')
                //->where('childCts.display_status',1)
                //->where('products.child_category_id',$categoryData->id)
                ->whereRaw('FIND_IN_SET('.$categoryData->id.',products.category_id)')
                ->where($whereArray)                
                ->orderby($ordercolumn,$orderdir)
                ->count();
            }else{
                return redirect(route('shop'));
            }
        }else if($slug!=''){
            if (strpos($slug, 'brand-') !== false) {
                $brandslug = str_replace("brand-","",$slug);
                $categoryData = DB::table('manufacturer')->where('display_status',1)->where('slug',$brandslug)->get()->first();
                if(!empty($categoryData)){
                    $colname = 'products.manufacturer_id';
                    $formSlug = 'shop/'.$slug;
                    $allProducts = DB::table('products')                
                    ->select('products.*')
                    //->join('categories as cts','products.category_id','=','cts.id')
                    //->where('cts.display_status',1)
                    //->where('products.category_id',$categoryData->id)
                    ->whereRaw('FIND_IN_SET('.$categoryData->id.',products.manufacturer_id)')
                    ->where($whereArray)                
                    ->orderby($ordercolumn,$orderdir)
                    ->paginate($perpage);
                    

                    $productCount = DB::table('products')
                    //->join('categories as cts','products.category_id','=','cts.id')
                    //->where('cts.display_status',1)
                    //->where('products.category_id',$categoryData->id)
                    ->whereRaw('FIND_IN_SET('.$categoryData->id.',products.manufacturer_id)')
                    ->where($whereArray)                
                    ->orderby($ordercolumn,$orderdir)
                    ->count();

                    $categoryData->slug='shop';
                    $categoryData->meta_title ='Shop';
                    $categoryData->meta_desc ='Shop';
                    $categoryData->meta_key ='Shop';
                    $isbrandpage = true;
                }else{
                    return redirect(route('shop'));
                }
            } else{
                $categoryData = DB::table('categories')->where('display_status',1)->where('slug',$slug)->get()->first();
                if(!empty($categoryData)){
                    $colname = 'products.category_id';
                    $formSlug = 'shop/'.$slug;
                    $allProducts = DB::table('products')                
                    ->select('products.*')
                    //->join('categories as cts','products.category_id','=','cts.id')
                    //->where('cts.display_status',1)
                    //->where('products.category_id',$categoryData->id)
                    ->whereRaw('FIND_IN_SET('.$categoryData->id.',products.category_id)')
                    ->where($whereArray)                
                    ->orderby($ordercolumn,$orderdir)
                    ->paginate($perpage);

                    $productCount = DB::table('products')
                    //->join('categories as cts','products.category_id','=','cts.id')
                    //->where('cts.display_status',1)
                    //->where('products.category_id',$categoryData->id)
                    ->whereRaw('FIND_IN_SET('.$categoryData->id.',products.category_id)')
                    ->where($whereArray)                
                    ->orderby($ordercolumn,$orderdir)
                    ->count();
                }else{
                    return redirect(route('shop'));
                }
            }
        }else{            
            //$whereArray['cts.display_status'] = 1;
            //$whereArray['childCts.display_status'] = 1;
            if(!empty($search)){
                $whereArray[]=array( function ($query) use ($search) {
                    $query->where('products.name', 'like', '%'.$search.'%');
                          //->orWhere('cts.name', 'like', '%'.$search.'%');
                          //->orWhere('childCts.name', 'like', '%'.$search.'%');
                });
            }
            //DB::enableQueryLog();
            $allProducts = DB::table('products')
                ->select('products.*')
                //->join('categories as cts','products.category_id','=','cts.id')
                //->join('categories as childCts','products.child_category_id','=','childCts.id')
                ->where($whereArray)                
                ->orderby($ordercolumn,$orderdir)
                ->paginate($perpage);

                /*$query = DB::getQueryLog();
                print_r($query);
                die();*/
            $productCount =DB::table('products')
                //->join('categories as cts','products.category_id','=','cts.id')
                //->join('categories as childCts','products.child_category_id','=','childCts.id')
                ->where($whereArray)                
                ->orderby($ordercolumn,$orderdir)
                ->count();    
            $categoryData['slug']='shop';
            $categoryData['meta_title']='Shop';
            $categoryData['meta_desc']='Shop';
            $categoryData['meta_key']='Shop';
            $categoryData['name']='Shop';
            $categoryData = (object)$categoryData;
        }
        $allManufacturer        = DB::table('manufacturer')->where('display_status',1)->orderby('display_order','ASC')->get();
        $advertisementData      = DB::table('advertisement')->where('display_status',1)->where('location',2)->orderby(DB::raw('RAND()'))->take(1)->get()->first();
        if(isset($categoryData->id)){
            $catLangData = getLanguageReconds('categories_translations',array('name'),array('category_id'=>$categoryData->id));
            if(!empty($catLangData->name)){
                $categotyName = $catLangData->name;
            }else{
                $categotyName = $categoryData->name;
            }
        }else{
            $categotyName = $categoryData->name;
        }
        $view_data['pageData']      = $categoryData;
        $view_data['currPage']      = $categoryData->slug;
        $view_data['meta_title']    = $categoryData->meta_title;
        $view_data['meta_key']      = $categoryData->meta_key;
        $view_data['meta_desc']     = $categoryData->meta_desc;
        $view_data['page_title']    = $categotyName;
        $view_data['fromPrice']     = $fromPrice;
        $view_data['toPrice']       = $toPrice;
        $view_data['minimumAmount'] = $minimumAmount;
        $view_data['maximumAmount'] = $maximumAmount;
        $view_data['perpage']       = $perpage;
        $view_data['page']          = $page;
        $view_data['sortby']        = $sortby;
        $view_data['headerSearch']  = $search;
        $view_data['selectedmanufacturer']  = $selectedmanufacturer;
        $view_data['allProducts']   = $allProducts;
        $view_data['productCount']  = $productCount;
        $view_data['allManufacturer']= $allManufacturer;
        $view_data['advertisementData']= $advertisementData;
        $view_data['formSlug']      = $formSlug;
        $view_data['isbrandpage']      = $isbrandpage;
        return view('category',$view_data);
    }
    public function search(Request $request){
        if(isset($_REQUEST['search'])){
            $search = trim($_REQUEST['search']);
            $request->session()->put('search', $search);
        }
        $request->session()->put('sortby', '');
        $request->session()->put('fromPrice', '');
        $request->session()->put('toPrice', '');
        $request->session()->put('selectedmanufacturer', '');
        return redirect(route('shop'));
    }
    public function product($slug=''){
        $currentUser = $this->currentUser;
        $productData = DB::table('products')
        ->where('display_status',1)
        ->where('product_type',1)
        ->where('slug',$slug)
        ->get()
        ->first();


        if(!empty($productData)){
        
        if(!empty($productData->user_id)){
            $view_data['storedetail'] = DB::table('users')->select('users.*')->where('users.id',$productData->user_id)->first();
        }

        $view_data['ratinglist'] = DB::table('product_ratings')
            ->select('*')
            ->leftJoin('users','product_ratings.user_id','=','users.id')
            ->where(['product_id'=>$productData->id,'status'=>'1'])
            ->get();


        $view_data['averagerating'] = DB::table('product_ratings')
            ->select('*')
            ->where(['product_id'=>$productData->id,'status'=>'1'])
            ->avg('product_ratings.rating');


        $view_data['totalcount'] = DB::table('product_ratings')
            ->select('*')
            ->where(['product_id'=>$productData->id,'status'=>'1'])
            ->count('product_ratings.rating');


            $productVariAttributes = DB::table('products_attributes')
                ->select('products_attributes.*','attribute_values.name as attrValue','attributes.name as attrName','attribute_sets.name as attrSetName')
                ->join('attribute_values','products_attributes.attribute_value','=','attribute_values.id')
                ->join('attributes','products_attributes.attribute_id','=','attributes.id')
                ->join('attribute_sets','attribute_values.set_id','=','attribute_sets.id')
                ->where('products_attributes.product_id',$productData->id)
                ->where('products_attributes.is_variation',1)
                ->get();

            $productOtherAttributes = DB::table('products_attributes')
                ->select('products_attributes.*','attribute_values.name as attrValue','attributes.name as attrName')
                ->join('attribute_values','products_attributes.attribute_value','=','attribute_values.id')
                ->join('attributes','products_attributes.attribute_id','=','attributes.id')
                ->where('products_attributes.product_id',$productData->id)
                ->where('products_attributes.is_variation',0)
                ->get();

            $wishlistExists = 0;
            if(isset($currentUser->id) && $currentUser->id!=''){
                $wishlistExists = DB::table('wishlist')->where('user_id',$currentUser->id)->where('product_id',$productData->id)->count();
            }
            $related_products = $productData->related_products;
            if(!empty($related_products)){
                $relatedArray       = explode(',',$related_products);
                $relatedProducts    = DB::table('products')
                    ->where('display_status',1)
                    ->where('product_type',1)
                    ->whereIn('id', $relatedArray)
                    ->get();
            }else{
                $relatedProducts = array();
            }

            $productLangData = getLanguageReconds('products_translations',array('name','short_description','content','more_info'),array('product_id'=>$productData->id));
            if(!empty($productLangData->name)){
                $productName = $productLangData->name;
            }else{
                $productName = $productData->name;
            }
            if(!empty($productLangData->name)){
                $productShortDescription = $productLangData->short_description;
            }else{
                $productShortDescription = $productData->short_description;
            }
            if(!empty($productLangData->content)){
                $productContent = $productLangData->content;
            }else{
                $productContent = $productData->content;
            }
            if(!empty($productLangData->more_info)){
                $productMoreInfo = $productLangData->more_info;
            }else{
                $productMoreInfo = $productData->more_info;
            }

            $view_data['pageData']      = $productData;
            $view_data['lastsegment']      = $slug;
            $view_data['isProductPage'] = 'Yes';
            $view_data['currPage']      = $productData->slug;
            $view_data['meta_title']    = $productData->meta_title;
            $view_data['meta_key']      = $productData->meta_key;
            $view_data['meta_desc']     = $productData->meta_desc;
            $view_data['page_title']    = $productName;       
            $view_data['productShortDescription']    = $productShortDescription;       
            $view_data['productContent']        = $productContent;       
            $view_data['productMoreInfo']       = $productMoreInfo;       
            $view_data['relatedProducts']       = $relatedProducts;        
            $view_data['wishlistExists']        = $wishlistExists;        
            $view_data['currentUser']           = $currentUser;        
            $view_data['productVariAttributes'] = $productVariAttributes;        
            $view_data['productOtherAttributes']= $productOtherAttributes;       
            
            if(Auth::user()){
                $currentuserid = Auth::user()->id;
                $product_owner = $productData->user_id; 
                if($currentuserid == $product_owner){
                    $view_data['showratings'] = false;
                } else{
                    $view_data['showratings'] = true;
                }
            } else{
                $view_data['showratings'] = false;
            }
            
            return view('product',$view_data);
        }else{
            return redirect(route('shop'));
        }        
    }
    public function cart(Request $request){
        $currentUser    = Auth::user();
        $sessionId      = Cookie::get('imedcartcookie');
        $current_user_id = (isset($currentUser->id))?$currentUser->id:'0';
        $whereArray = array(
            'products.display_status'=>1,
            'products.product_type'=>1
        );
        if($current_user_id!=0){
            $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                $query->where('cart_products.user_id','=',$current_user_id)
                    ->orWhere('cart_products.session_id', '=',$sessionId);
            });
        }else{
            $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                $query->Where('cart_products.session_id', '=',$sessionId);
            });
        }
        
        $allCartItems = DB::table('cart_products')
        ->select('cart_products.*','products.name','products.image','products.slug','products.price','products.special_price','products.stock_availability','products.inventory_management','products.qty as availableQty')
        ->join('products','products.id','=','cart_products.product_id')
        ->where($whereArray)                
        ->orderby('cart_products.id','ASC')
        ->get();
        $subTotalAmount     = 0;
        $userAppliedCoupons = 0;
        $availedDiscount    = 0;
        $priceAfterDiscount = 0;
        $appliedCoupon      = $request->session()->get('appliedCoupon');
        if(!empty($appliedCoupon)){
            $currentDate    = date('Y-m-d');
            $getCouponData  = DB::table('coupons')
            ->where('code',$appliedCoupon)
            ->where('start_date','<=',$currentDate)
            ->where('end_date','>=',$currentDate)
            ->where('status',1)
            ->first();
            if(!empty($getCouponData)){
                if(!empty($allCartItems)){
                    foreach($allCartItems as $singleCartItem){
                        $qty            = $singleCartItem->qty;
                        $price          = $singleCartItem->price;
                        $special_price  = $singleCartItem->special_price;
                        $stock_availability     = $singleCartItem->stock_availability;
                        $inventory_management   = $singleCartItem->inventory_management;
                        $availableQty           = $singleCartItem->availableQty;
                        if(!empty($special_price) && $special_price>0){
                            $proPrice = $special_price;
                        }else{
                            $proPrice = $price;
                        }
                        if(!empty($singleCartItem->product_attr_id)){
                            $getProAttrCart = DB::table('products_attributes')
                                ->where('id',$singleCartItem->product_attr_id)
                                ->where('product_id',$singleCartItem->product_id)
                                ->where('is_variation',1)
                                ->first();                           
                            $proPrice = (isset($getProAttrCart->attribute_price) && $getProAttrCart->attribute_price>0)?$getProAttrCart->attribute_price:$proPrice;
                        }

                        if(($inventory_management==0 && $stock_availability==1) || ($inventory_management==1 && $stock_availability==1 && $availableQty>=$qty)){
                            $cartQty = $qty;
                        }else if($inventory_management==1 && $stock_availability==1 && $availableQty<$qty){
                            $cartQty = $availableQty;
                        }else{
                            $cartQty='0';
                        }
                        $totalPrice     = $cartQty*$proPrice;
                        $subTotalAmount = $subTotalAmount+$totalPrice;
                    }
                }            
                
                if($current_user_id!=0){
                    $userAppliedCoupons = DB::table('orders')
                    ->where('coupon_code',$appliedCoupon)
                    ->where('customer_id',$current_user_id)
                    ->where('status',2)
                    ->count();
                }
                $appliedCoupons = DB::table('orders')
                    ->where('coupon_code',$appliedCoupon)
                    ->where('status',2)
                    ->count();
                $type               = $getCouponData->type;
                $value              = $getCouponData->value;
                $minimum_spend      = $getCouponData->minimum_spend;
                $maximum_spend      = $getCouponData->maximum_spend;
                $maximum_discount   = $getCouponData->maximum_discount;
                $usage_limit_per_coupon     = $getCouponData->usage_limit_per_coupon;
                $usage_limit_per_customer   = $getCouponData->usage_limit_per_customer;

                if($type=='0'){
                    $discount = $subTotalAmount-$value;
                }else{
                    $discount = ($subTotalAmount*$value)/100;
                }
                if($discount<$maximum_discount){
                    $finalDiscount = $discount;
                }else{
                    $finalDiscount = $maximum_discount;
                }           

                if($subTotalAmount>=$minimum_spend && $subTotalAmount<=$maximum_spend){
                    if($appliedCoupons<$usage_limit_per_coupon && $current_user_id==0){
                        $availedDiscount    = $finalDiscount;
                        $priceAfterDiscount = $subTotalAmount-$availedDiscount;
                    }else if($appliedCoupons<$usage_limit_per_coupon && $userAppliedCoupons<$usage_limit_per_customer && $current_user_id!=0){                       
                        $availedDiscount    = $finalDiscount;
                        $priceAfterDiscount = $subTotalAmount-$availedDiscount;
                    }else{
                        $request->session()->put('appliedCoupon', '');
                    }
                }else{
                    $request->session()->put('appliedCoupon', '');
                }
            }else{
                $request->session()->put('appliedCoupon', '');
            }
        }
        $pageData                           = DB::table('pages')->where('id',12)->get()->first();
        $view_data['pageData']              = $pageData;
        $view_data['meta_title']            = $pageData->meta_title;
        $view_data['meta_key']              = $pageData->meta_key;
        $view_data['meta_desc']             = $pageData->meta_desc;
        $view_data['page_title']            = $pageData->name;
        $view_data['allCartItems']          = $allCartItems;
        $view_data['availedDiscount']       = $availedDiscount;
        $view_data['priceAfterDiscount']    = $priceAfterDiscount;
        $view_data['appliedCoupon']         = $appliedCoupon;
       
        $view_data['currPage']              = 'cart';
        return view('cart',$view_data);
    }
    /*public function checkout(Request $request){
        $appliedCoupon = $request->session()->get('appliedCoupon');
        $currentUser        = Auth::user();
        $current_user_id    = (isset($currentUser->id))?$currentUser->id:'0';
        $sessionId          = Cookie::get('imedcartcookie');
        if($current_user_id==0){
            return redirect(url('login'))->withWarning('Please login or create an account to place your order.');
        }

        $pageData       = DB::table('pages')->where('id',13)->get()->first();
        $address        = DB::table('users_address')->where('user_id',$current_user_id)->where('default',1)->first();
        $allCountries   = DB::table('countries')
            ->where('status',1)
            ->orderby('countryName','ASC')
            ->get();
        $allStates = array();
        $countryId  = (isset($address->country))?$address->country:'';
        if($countryId!=''){
            $allStates = DB::table('states')
                ->where('status',1)
                ->where('countryID',$countryId)
                ->orderby('stateName','ASC')
                ->get();
        }

        $whereArray = array(
            'products.display_status'=>1,
            'products.product_type'=>1
        );
        if($current_user_id!=0){
            $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                $query->where('cart_products.user_id','=',$current_user_id)
                    ->orWhere('cart_products.session_id', '=',$sessionId);
            });
        }else{
            $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                $query->Where('cart_products.session_id', '=',$sessionId);
            });
        }
        

        $allCartItems = DB::table('cart_products')
        ->select('cart_products.*','products.name','products.image','products.slug','products.price','products.special_price','products.stock_availability','products.inventory_management','products.qty as availableQty')
        ->join('products','products.id','=','cart_products.product_id')
        ->where($whereArray)                
        ->orderby('cart_products.id','ASC')
        ->get();

        $subTotalAmount     = 0;
        $userAppliedCoupons = 0;
        $availedDiscount    = 0;
        $priceAfterDiscount = 0;
        if(!empty($appliedCoupon)){
            $currentDate    = date('Y-m-d');
            $getCouponData  = DB::table('coupons')
            ->where('code',$appliedCoupon)
            ->where('start_date','<=',$currentDate)
            ->where('end_date','>=',$currentDate)
            ->where('status',1)
            ->first();
            if(!empty($getCouponData)){
                if(!empty($allCartItems)){
                    foreach($allCartItems as $singleCartItem){
                        $qty            = $singleCartItem->qty;
                        $price          = $singleCartItem->price;
                        $special_price  = $singleCartItem->special_price;
                        $stock_availability     = $singleCartItem->stock_availability;
                        $inventory_management   = $singleCartItem->inventory_management;
                        $availableQty           = $singleCartItem->availableQty;
                        if(!empty($special_price)){
                            $proPrice = $special_price;
                        }else{
                            $proPrice = $price;
                        }
                        if(($inventory_management==0 && $stock_availability==1) || ($inventory_management==1 && $stock_availability==1 && $availableQty>=$qty)){
                            $cartQty = $qty;
                        }else if($inventory_management==1 && $stock_availability==1 && $availableQty<$qty){
                            $cartQty = $availableQty;
                        }else{
                            $cartQty='0';
                        }
                        $totalPrice     = $cartQty*$proPrice;
                        $subTotalAmount = $subTotalAmount+$totalPrice;
                    }
                }            
                
                if($current_user_id!=0){
                    $userAppliedCoupons = DB::table('orders')
                    ->where('coupon_code',$appliedCoupon)
                    ->where('customer_id',$current_user_id)
                    ->where('status',2)
                    ->count();
                }
                $appliedCoupons = DB::table('orders')
                    ->where('coupon_code',$appliedCoupon)
                    ->where('status',2)
                    ->count();

                $type               = $getCouponData->type;
                $value              = $getCouponData->value;
                $minimum_spend      = $getCouponData->minimum_spend;
                $maximum_spend      = $getCouponData->maximum_spend;
                $maximum_discount   = $getCouponData->maximum_discount;
                $usage_limit_per_coupon     = $getCouponData->usage_limit_per_coupon;
                $usage_limit_per_customer   = $getCouponData->usage_limit_per_customer;

                if($type=='0'){
                    $discount = $subTotalAmount-$value;
                }else{
                    $discount = ($subTotalAmount*$value)/100;
                }
                if($discount<$maximum_discount){
                    $finalDiscount = $discount;
                }else{
                    $finalDiscount = $maximum_discount;
                }           

                if($subTotalAmount>=$minimum_spend && $subTotalAmount<=$maximum_spend){
                    if($appliedCoupons<$usage_limit_per_coupon && $current_user_id==0){
                        $availedDiscount    = number_format($finalDiscount,2);
                        $priceAfterDiscount = number_format($subTotalAmount-$availedDiscount,2);
                    }else if($appliedCoupons<$usage_limit_per_coupon && $userAppliedCoupons<$usage_limit_per_customer && $current_user_id!=0){                       
                        $availedDiscount    = number_format($finalDiscount,2);
                        $priceAfterDiscount = number_format($subTotalAmount-$availedDiscount,2);
                    }else{
                        $request->session()->put('appliedCoupon', '');
                    }
                }else{
                    $request->session()->put('appliedCoupon', '');
                }
            }else{
                $request->session()->put('appliedCoupon', '');
            }
        }

        $view_data['pageData']      = $pageData;
        $view_data['meta_title']    = $pageData->meta_title;
        $view_data['meta_key']      = $pageData->meta_key;
        $view_data['meta_desc']     = $pageData->meta_desc;
        $view_data['page_title']    = $pageData->name;
        $view_data['currPage']      = 'checkout';
        $view_data['address']       = $address;
        $view_data['currentUser']   = $currentUser;
        $view_data['allCountries']  = $allCountries;
        $view_data['allStates']     = $allStates;
        $view_data['allCartItems']  = $allCartItems;        
        $view_data['availedDiscount']       = $availedDiscount;
        $view_data['priceAfterDiscount']    = $priceAfterDiscount;
        $view_data['appliedCoupon']         = $appliedCoupon;
        $view_data['generalSettings']  = generalSettings();
        return view('checkout',$view_data);
    }*/
    public function applyCoupon(Request $request){
        $subTotalAmount     = 0;
        $userAppliedCoupons = 0;
        $availedDiscount    = 0;
        $priceAfterDiscount = 0;
        $currentDate    = date('Y-m-d');
        $applycoupon    = $request->input('applycoupon');
        if(!empty($applycoupon)){
            $getCouponData  = DB::table('coupons')
            ->where('code',$applycoupon)
            ->where('start_date','<=',$currentDate)
            ->where('end_date','>=',$currentDate)
            ->where('status',1)
            ->first();
            if(!empty($getCouponData)){
                $currentUser        = Auth::user();
                $sessionId          = Cookie::get('imedcartcookie');
                $current_user_id    = (isset($currentUser->id))?$currentUser->id:'0';
                $whereArray = array(
                    'products.display_status'=>1,
                    'products.product_type'=>1
                );
                if($current_user_id!=0){
                    $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                        $query->where('cart_products.user_id','=',$current_user_id)
                            ->orWhere('cart_products.session_id', '=',$sessionId);
                    });
                }else{
                    $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                        $query->Where('cart_products.session_id', '=',$sessionId);
                    });
                }
                $allCartItems = DB::table('cart_products')
                ->select('cart_products.*','products.name','products.image','products.slug','products.price','products.special_price','products.stock_availability','products.inventory_management','products.qty as availableQty')
                ->join('products','products.id','=','cart_products.product_id')
                ->where($whereArray)                
                ->orderby('cart_products.id','ASC')
                ->get();
                
                if(!empty($allCartItems)){
                    foreach($allCartItems as $singleCartItem){
                        $qty            = $singleCartItem->qty;
                        $price          = $singleCartItem->price;
                        $special_price  = $singleCartItem->special_price;
                        $stock_availability     = $singleCartItem->stock_availability;
                        $inventory_management   = $singleCartItem->inventory_management;
                        $availableQty           = $singleCartItem->availableQty;
                        if(!empty($special_price) && $special_price>0){
                            $proPrice = $special_price;
                        }else{
                            $proPrice = $price;
                        }
                        if(($inventory_management==0 && $stock_availability==1) || ($inventory_management==1 && $stock_availability==1 && $availableQty>=$qty)){
                            $cartQty = $qty;
                        }else if($inventory_management==1 && $stock_availability==1 && $availableQty<$qty){
                            $cartQty = $availableQty;
                        }else{
                            $cartQty='0';
                        }
                        $totalPrice     = $cartQty*$proPrice;
                        $subTotalAmount = $subTotalAmount+$totalPrice;
                    }
                }            
                
                if($current_user_id!=0){
                    $userAppliedCoupons = DB::table('orders')
                    ->where('coupon_code',$applycoupon)
                    ->where('customer_id',$current_user_id)
                    ->where('status',2)
                    ->count();
                }
                $appliedCoupons = DB::table('orders')
                    ->where('coupon_code',$applycoupon)
                    ->where('status',2)
                    ->count();

                $type               = $getCouponData->type;
                $value              = $getCouponData->value;
                $minimum_spend      = $getCouponData->minimum_spend;
                $maximum_spend      = $getCouponData->maximum_spend;
                $maximum_discount   = $getCouponData->maximum_discount;
                $usage_limit_per_coupon     = $getCouponData->usage_limit_per_coupon;
                $usage_limit_per_customer   = $getCouponData->usage_limit_per_customer;

                if($type=='0'){
                    $discount = $subTotalAmount-$value;
                }else{
                    $discount = ($subTotalAmount*$value)/100;
                }
                if($discount<$maximum_discount){
                    $finalDiscount = $discount;
                }else{
                    $finalDiscount = $maximum_discount;
                }           

                if($subTotalAmount>=$minimum_spend && $subTotalAmount<=$maximum_spend){
                    if($appliedCoupons<$usage_limit_per_coupon && $current_user_id==0){
                        $request->session()->put('appliedCoupon', $applycoupon);
                        $availedDiscount    = $finalDiscount;
                        $priceAfterDiscount = $subTotalAmount-$availedDiscount;
                        $stat = 1;
                        $msg = __('Discount coupon has been applied successfully.');
                    }else if($appliedCoupons<$usage_limit_per_coupon && $userAppliedCoupons<$usage_limit_per_customer && $current_user_id!=0){
                        $request->session()->put('appliedCoupon', $applycoupon);
                        $availedDiscount    = $finalDiscount;
                        $priceAfterDiscount = $subTotalAmount-$availedDiscount;
                        $stat = 1;
                        $msg = __('Discount coupon has been applied successfully.');
                    }else{
                        $stat = 0;
                        $msg = __("Invalid code has been exceeded it's limit.") ;
                    }
                }else{
                    $stat = 0;
                    $msg = __('Total cart amount should be in between').' '.$minimum_spend.' '.__('to').' '.$maximum_spend.' '.__('to apply this coupon.');
                }            
            }else{
                $stat = 0;
                $msg = __('Invalid or expired coupon code.');
            }
        }else{
            $request->session()->put('appliedCoupon', $applycoupon);
            $stat = 1;
            $msg = '';
        }        
        $response_array = array('stat'=>$stat,'msg'=>$msg,'availedDiscount'=>showPrice($availedDiscount),'priceAfterDiscount'=>showPrice($priceAfterDiscount));
        echo json_encode($response_array);   
    }
    public function addProducts(Request $request){
        $product_id     = $request->input('product_id');
        $qty            = $request->input('qty');
        $variationId    = $request->input('variationId');
        $currentUser    = Auth::user();
        $sessionId      = Cookie::get('imedcartcookie');
        $current_user_id = (isset($currentUser->id))?$currentUser->id:'0';
        if(isset($qty) && $qty>0 && is_numeric($qty)){
            $quantity=$qty;
        }else{
            $quantity=1;
        }

        $stat = 1;
        $msg = '';
        $responseArray  = array();
        $created_at     = currentDBDate();
        if(!empty($product_id)){
            $product_id = base64_decode($product_id);
        }
        if(!empty($variationId)){
            $variationArray = explode('##~~##',$variationId);
            $variationId = (isset($variationArray[0]))?$variationArray[0]:'';
            $variationId = @base64_decode($variationId);
        }
        $getProAttr = array();
        $product_attr_id = '0';
        if(!empty($product_id) && !empty($variationId)){
            $getProAttr = DB::table('products_attributes')
            ->where('id',$variationId)
            ->where('product_id',$product_id)
            ->where('is_variation',1)
            ->first();
            $product_attr_id = (isset($getProAttr->id))?$getProAttr->id:'0';
        }
        if(!empty($variationId) && empty($getProAttr)){
            $stat = 0;
            $msg = __('Product is out of stock.');
        }else if($product_id!=''){
            $getProduct = DB::table('products')
            ->where('display_status',1)
            ->where('product_type',1)
            ->where('id',$product_id)
            ->get()->first();
            if(isset($getProduct->id)){
                $availableQty           = $getProduct->qty;
                $stock_availability     = $getProduct->stock_availability;
                $inventory_management   = $getProduct->inventory_management;
                if(($inventory_management==0 && $stock_availability==1) || ($inventory_management==1 && $stock_availability==1 && $availableQty>=$quantity)){
                    $whereCartArray = array(
                        'product_id'=>$product_id
                    );
                    if($current_user_id!=0){
                        $whereCartArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                            $query->where('user_id','=',$current_user_id)
                                ->orWhere('session_id', '=',$sessionId);
                        });
                    }else{
                        $whereCartArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                            $query->Where('session_id', '=',$sessionId);
                        });
                    }  
                    
                    $cartProduct   = DB::table('cart_products')
                    ->where($whereCartArray)
                    ->get()->first();
                    if(isset($cartProduct->id)){
                        $update_array=array(
                            'user_id'=>$current_user_id,
                            'session_id'=>$sessionId,
                            'product_id'=>$product_id,
                            'product_attr_id'=>$product_attr_id,
                            'qty'=>$quantity,                            
                            'updated_at'=>$created_at
                        );
                        DB::table('cart_products')->where('id',$cartProduct->id)->update($update_array);
                    }else{
                    }
                        $insert_array=array(
                            'user_id'=>$current_user_id,
                            'session_id'=>$sessionId,
                            'product_id'=>$product_id,
                            'product_attr_id'=>$product_attr_id,
                            'qty'=>$quantity,
                            'created_at'=>$created_at,
                            'updated_at'=>$created_at
                        );
                        DB::table('cart_products')->insert($insert_array);
                    $stat = 1;
                    $msg = __('Product added successfully.');
                }else if(($inventory_management==0 && $stock_availability==1) || ($inventory_management==1 && $stock_availability==1 && $availableQty<$quantity && $availableQty>0)){
                    $quantity = $availableQty;
                    $whereCartArray = array(
                        'product_id'=>$product_id
                    );
                    if($current_user_id!=0){
                        $whereCartArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                            $query->where('user_id','=',$current_user_id)
                                ->orWhere('session_id', '=',$sessionId);
                        });
                    }else{
                        $whereCartArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                            $query->Where('session_id', '=',$sessionId);
                        });
                    }
                    
                    $cartProduct   = DB::table('cart_products')
                    ->where($whereCartArray)
                    ->get()->first();
                    if(isset($cartProduct->id)){
                        $update_array=array(
                            'user_id'=>$current_user_id,
                            'session_id'=>$sessionId,
                            'product_id'=>$product_id,
                            'product_attr_id'=>$product_attr_id,
                            'qty'=>$quantity,                            
                            'updated_at'=>$created_at
                        );
                        DB::table('cart_products')->where('id',$cartProduct->id)->update($update_array);
                    }else{
                        $insert_array=array(
                            'user_id'=>$current_user_id,
                            'session_id'=>$sessionId,
                            'product_id'=>$product_id,
                            'product_attr_id'=>$product_attr_id,
                            'qty'=>$quantity,
                            'created_at'=>$created_at,
                            'updated_at'=>$created_at
                        );
                        DB::table('cart_products')->insert($insert_array);
                    }
                    $stat = 1;
                    $msg = __('Product added successfully.');
                }else {
                    $stat = 0;
                    $msg = __('Product is out of stock.');
                }
            }            
        }
       
        $whereArray = array(
            'products.display_status'=>1,
            'products.product_type'=>1
        );
        if($current_user_id!=0){
            $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                $query->where('cart_products.user_id','=',$current_user_id)
                    ->orWhere('cart_products.session_id', '=',$sessionId);
            }); 
        }else{
            $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                $query->Where('cart_products.session_id', '=',$sessionId);
            });  
        }
        
        $allCartItems   = DB::table('cart_products')
        ->select('cart_products.*','products.name','products.image','products.slug','products.price','products.special_price','products.stock_availability','products.inventory_management','products.qty as availableQty')
        ->join('products','products.id','=','cart_products.product_id')
        ->where($whereArray)                
        ->orderby('cart_products.id','ASC')
        ->get();
        $cartHtml       = '';
        $subTotalAmount = 0;
        if(!empty($allCartItems) && count($allCartItems)>0){
            foreach($allCartItems as $singleCartItem){
                $cartProLangData = getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleCartItem->product_id));
                if(!empty($cartProLangData->name)){
                    $cartProductName = $cartProLangData->name;
                }else{
                    $cartProductName = $singleCartItem->name;
                }
                $qty = $singleCartItem->qty;
                $price = $singleCartItem->price;
                $special_price = $singleCartItem->special_price;
                $stock_availability     = $singleCartItem->stock_availability;
                $inventory_management   = $singleCartItem->inventory_management;
                $availableQty           = $singleCartItem->availableQty;
                if(!empty($special_price) && $special_price>0){
                    $proPrice = $special_price;
                }else{
                    $proPrice = $price;
                }
                $attrProNAme = '';
                if(!empty($singleCartItem->product_attr_id)){
                    $getProAttrCart = DB::table('products_attributes')
                        ->where('id',$singleCartItem->product_attr_id)
                        ->where('product_id',$singleCartItem->product_id)
                        ->where('is_variation',1)
                        ->first();
                    $proAttribute_value =(isset($getProAttrCart->attribute_value))?$getProAttrCart->attribute_value:'';
                    $getAttrCartData = array();

                    if(!empty($proAttribute_value)){
                        $getAttrCartData = DB::table('attribute_values')
                            ->where('id',$proAttribute_value)
                            ->first();
                    }
                    $attrProNAme = (isset($getAttrCartData->name))?$getAttrCartData->name:'';
                    $proPrice = (isset($getProAttrCart->attribute_price) && $getProAttrCart->attribute_price>0)?$getProAttrCart->attribute_price:$proPrice;
                }


                if(($inventory_management==0 && $stock_availability==1) || ($inventory_management==1 && $stock_availability==1 && $availableQty>=$qty)){
                    $cartQty = $qty;
                }else if($inventory_management==1 && $stock_availability==1 && $availableQty<$qty){
                    $cartQty = $availableQty;
                }else{
                    $cartQty='0';
                }
                $totalPrice         = $cartQty*$proPrice;
                $subTotalAmount     = $subTotalAmount+$totalPrice;
                $cartHtml .=' <ul class="mini-cart-list"><li>
                    <div class="media">
                    <a href="'.url('product/'.$singleCartItem->slug).'"><img alt="'.$cartProductName.'" class="mr-3" src="'. asset($singleCartItem->image).'"></a>
                        <div class="media-body">
                            <a href="'.url('product/'.$singleCartItem->slug).'">
                                <h4>'.$cartProductName.'</h4>
                            </a>';
                        if(!empty($attrProNAme)){
                            $cartHtml .='<h4><span>'.__($attrProNAme).'</span></h4>';
                        }                      

                        $cartHtml .='<h4><span>'.$cartQty.' x '.showPrice($proPrice).'</span></h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="javascript:void(0)" class="delete-from-cart" data-id="'.base64_encode($singleCartItem->product_id).'" data-loc="header"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="none">
                        <path d="M6.2253 4.81096C5.83477 4.42044 5.20161 4.42044 4.81108 4.81096C4.42056 5.20148 4.42056 5.83465 4.81108 6.22517L10.5858 11.9999L4.81114 17.7746C4.42062 18.1651 4.42062 18.7983 4.81114 19.1888C5.20167 19.5793 5.83483 19.5793 6.22535 19.1888L12 13.4141L17.7747 19.1888C18.1652 19.5793 18.7984 19.5793 19.1889 19.1888C19.5794 18.7983 19.5794 18.1651 19.1889 17.7746L13.4142 11.9999L19.189 6.22517C19.5795 5.83465 19.5795 5.20148 19.189 4.81096C18.7985 4.42044 18.1653 4.42044 17.7748 4.81096L12 10.5857L6.2253 4.81096Z" fill="black"/>
                        </svg>
                        </a>
                    </div>
                </li></ul>';
            }
            $cartHtml .='<div class="bottom">
            <ul><li>
                <div class="total">
                    <h5>'.__('Subtotal').' : <span>'.showPrice($subTotalAmount).'</span></h5>
                </div>
            </li>';
            $cartHtml .='<li>
                <div class="buttons"><a href="'.route('cart').'" class="view-cart">'.__('View Cart').'</a> <a href="'.route('checkout').'" class="checkout">'.__('Checkout').'</a></div>
            </li></ul>
            </div>';
        }else{
            $cartHtml .='<div class="empty">
            <div class="icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24" fill="none">
                    <path d="M12 0.75C10.8066 0.75 9.66196 1.22411 8.81805 2.06802C7.97414 2.91193 7.50003 4.05653 7.50003 5.25V6H6.87753C6.24612 5.99934 5.63878 6.24222 5.18194 6.6781C4.72511 7.11398 4.45399 7.70925 4.42503 8.34L3.83253 20.6775C3.81734 21.0093 3.86949 21.3407 3.98582 21.6517C4.10216 21.9628 4.28028 22.2471 4.50944 22.4875C4.7386 22.7278 5.01407 22.9193 5.31923 23.0504C5.62439 23.1814 5.95292 23.2493 6.28503 23.25H17.715C18.0471 23.2493 18.3757 23.1814 18.6808 23.0504C18.986 22.9193 19.2615 22.7278 19.4906 22.4875C19.7198 22.2471 19.8979 21.9628 20.0142 21.6517C20.1306 21.3407 20.1827 21.0093 20.1675 20.6775L19.575 8.34C19.5461 7.70925 19.2749 7.11398 18.8181 6.6781C18.3613 6.24222 17.7539 5.99934 17.1225 6H16.5V5.25C16.5 4.05653 16.0259 2.91193 15.182 2.06802C14.3381 1.22411 13.1935 0.75 12 0.75V0.75ZM9.00003 5.25C9.00003 4.45435 9.3161 3.69129 9.87871 3.12868C10.4413 2.56607 11.2044 2.25 12 2.25C12.7957 2.25 13.5587 2.56607 14.1213 3.12868C14.684 3.69129 15 4.45435 15 5.25V6H9.00003V5.25ZM18.075 8.4075L18.6675 20.7525C18.6722 20.8815 18.6515 21.0102 18.6064 21.1311C18.5614 21.2521 18.493 21.363 18.405 21.4575C18.3153 21.5497 18.208 21.623 18.0895 21.6733C17.971 21.7235 17.8437 21.7496 17.715 21.75H6.28503C6.15635 21.7496 6.02904 21.7235 5.91056 21.6733C5.79208 21.623 5.68481 21.5497 5.59503 21.4575C5.50711 21.363 5.43867 21.2521 5.39363 21.1311C5.34859 21.0102 5.32783 20.8815 5.33253 20.7525L5.92503 8.4075C5.93661 8.16276 6.04204 7.9319 6.21943 7.76289C6.39682 7.59388 6.63252 7.49973 6.87753 7.5H17.1225C17.3675 7.49973 17.6032 7.59388 17.7806 7.76289C17.958 7.9319 18.0635 8.16276 18.075 8.4075V8.4075Z" fill="#395589"/>
                    <path d="M8.52002 10.8925C8.93423 10.8925 9.27002 10.5567 9.27002 10.1425C9.27002 9.72824 8.93423 9.39246 8.52002 9.39246C8.10581 9.39246 7.77002 9.72824 7.77002 10.1425C7.77002 10.5567 8.10581 10.8925 8.52002 10.8925Z" fill="#395589"/>
                    <path d="M15.4801 10.8925C15.8943 10.8925 16.2301 10.5567 16.2301 10.1425C16.2301 9.72824 15.8943 9.39246 15.4801 9.39246C15.0659 9.39246 14.7301 9.72824 14.7301 10.1425C14.7301 10.5567 15.0659 10.8925 15.4801 10.8925Z" fill="#395589"/>
                </svg>
            </div>
          
            <h5>'.__('Your Cart is Empty').'</h5>
            
        </div>';
        }
        
        $response_array = array('stat'=>$stat,'msg'=>$msg,'cartHtml'=>$cartHtml,'cartCount'=>count($allCartItems));
        echo json_encode($response_array);
    }
    public function deleteProducts(Request $request){
        $product_id     = $request->input('product_id');
        $loc            = $request->input('loc');
        $product_id     = base64_decode($product_id);
        $currentUser    = Auth::user();
        $sessionId      = Cookie::get('imedcartcookie');
        $current_user_id = (isset($currentUser->id))?$currentUser->id:'0';
        if($loc!='header'){
            Session::flash('message', __('Product has been removed from cart.'));
        }        

        $stat   = 1;
        $msg    = __('Product has been removed from cart.');
        $whereDeleteArray = array(
            'product_id'=>$product_id
        ); 
        if($current_user_id!=0){
            $whereDeleteArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                $query->where('cart_products.user_id','=',$current_user_id)
                    ->orWhere('cart_products.session_id', '=',$sessionId);
            }); 
        }else{
            $whereDeleteArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                $query->Where('cart_products.session_id', '=',$sessionId);
            });  
        }

        DB::table('cart_products')->where($whereDeleteArray)->delete();


        $whereArray = array(
            'products.display_status'=>1,
            'products.product_type'=>1
        ); 

        if($current_user_id!=0){
            $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                $query->where('cart_products.user_id','=',$current_user_id)
                    ->orWhere('cart_products.session_id', '=',$sessionId);
            }); 
        }else{
            $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                $query->Where('cart_products.session_id', '=',$sessionId);
            });  
        }

        $allCartItems   = DB::table('cart_products')
        ->select('cart_products.*','products.name','products.image','products.slug','products.price','products.special_price','products.stock_availability','products.inventory_management','products.qty as availableQty')
        ->join('products','products.id','=','cart_products.product_id')
        ->where($whereArray)                
        ->orderby('cart_products.id','ASC')
        ->get();
        $cartHtml='';
        $subTotalAmount = 0;
        if(!empty($allCartItems) && count($allCartItems)>0){
            foreach($allCartItems as $singleCartItem){
                $cartProLangData = getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleCartItem->product_id));
                if(!empty($cartProLangData->name)){
                    $cartProductName = $cartProLangData->name;
                }else{
                    $cartProductName = $singleCartItem->name;
                }

                $qty    = $singleCartItem->qty;
                $price  = $singleCartItem->price;
                $special_price = $singleCartItem->special_price;
                $stock_availability     = $singleCartItem->stock_availability;
                $inventory_management   = $singleCartItem->inventory_management;
                $availableQty           = $singleCartItem->availableQty;
                if(!empty($special_price) && $special_price>0){
                    $proPrice = $special_price;
                }else{
                    $proPrice = $price;
                }

                $attrProNAme = '';
                if(!empty($singleCartItem->product_attr_id)){
                    $getProAttrCart = DB::table('products_attributes')
                        ->where('id',$singleCartItem->product_attr_id)
                        ->where('product_id',$singleCartItem->product_id)
                        ->where('is_variation',1)
                        ->first();
                    $proAttribute_value =(isset($getProAttrCart->attribute_value))?$getProAttrCart->attribute_value:'';
                    $getAttrCartData = array();

                    if(!empty($proAttribute_value)){
                        $getAttrCartData = DB::table('attribute_values')
                            ->where('id',$proAttribute_value)
                            ->first();
                    }
                    $attrProNAme = (isset($getAttrCartData->name))?$getAttrCartData->name:'';
                    $proPrice = (isset($getProAttrCart->attribute_price) && $getProAttrCart->attribute_price>0)?$getProAttrCart->attribute_price:$proPrice;
                }

                if(($inventory_management==0 && $stock_availability==1) || ($inventory_management==1 && $stock_availability==1 && $availableQty>=$qty)){
                    $cartQty = $qty;
                }else if($inventory_management==1 && $stock_availability==1 && $availableQty<$qty){
                    $cartQty = $availableQty;
                }else{
                    $cartQty='0';
                }
                $totalPrice = $cartQty*$proPrice;
                $subTotalAmount = $subTotalAmount+$totalPrice;
                $cartHtml .='<ul class="mini-cart-list"><li>
                    <div class="media">
                    <a href="'.url('product/'.$singleCartItem->slug).'"><img alt="'.$cartProductName.'" class="mr-3" src="'. asset($singleCartItem->image).'"></a>
                        <div class="media-body">
                            <a href="'.url('product/'.$singleCartItem->slug).'">
                                <h4>'.$cartProductName.'</h4>
                            </a>';
                        if(!empty($attrProNAme)){
                            $cartHtml .='<h4><span>'.__($attrProNAme).'</span></h4>';
                        }                      

                        $cartHtml .='<h4><span>'.$cartQty.' x '.showPrice($proPrice).'</span></h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="javascript:void(0)" class="delete-from-cart" data-id="'.base64_encode($singleCartItem->product_id).'" data-loc="header"> <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="none">
                        <path d="M6.2253 4.81096C5.83477 4.42044 5.20161 4.42044 4.81108 4.81096C4.42056 5.20148 4.42056 5.83465 4.81108 6.22517L10.5858 11.9999L4.81114 17.7746C4.42062 18.1651 4.42062 18.7983 4.81114 19.1888C5.20167 19.5793 5.83483 19.5793 6.22535 19.1888L12 13.4141L17.7747 19.1888C18.1652 19.5793 18.7984 19.5793 19.1889 19.1888C19.5794 18.7983 19.5794 18.1651 19.1889 17.7746L13.4142 11.9999L19.189 6.22517C19.5795 5.83465 19.5795 5.20148 19.189 4.81096C18.7985 4.42044 18.1653 4.42044 17.7748 4.81096L12 10.5857L6.2253 4.81096Z" fill="black"/>
                        </svg>
                        </a>
                    </div>
                </li></ul>';
            }
            $cartHtml .='<div class="bottom">
            <ul><li>
                <div class="total">
                    <h5>'.__('Subtotal').' : <span>'.showPrice($subTotalAmount).'</span></h5>
                </div>
            </li>';
            $cartHtml .='<li>
                <div class="buttons"><a href="'.route('cart').'" class="view-cart">'.__('View Cart').'</a> <a href="'.route('checkout').'" class="checkout">'.__('Checkout').'</a></div>
            </li></ul></div>';
        }else{
            $cartHtml .='<div class="empty">
            <div class="icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24" fill="none">
                    <path d="M12 0.75C10.8066 0.75 9.66196 1.22411 8.81805 2.06802C7.97414 2.91193 7.50003 4.05653 7.50003 5.25V6H6.87753C6.24612 5.99934 5.63878 6.24222 5.18194 6.6781C4.72511 7.11398 4.45399 7.70925 4.42503 8.34L3.83253 20.6775C3.81734 21.0093 3.86949 21.3407 3.98582 21.6517C4.10216 21.9628 4.28028 22.2471 4.50944 22.4875C4.7386 22.7278 5.01407 22.9193 5.31923 23.0504C5.62439 23.1814 5.95292 23.2493 6.28503 23.25H17.715C18.0471 23.2493 18.3757 23.1814 18.6808 23.0504C18.986 22.9193 19.2615 22.7278 19.4906 22.4875C19.7198 22.2471 19.8979 21.9628 20.0142 21.6517C20.1306 21.3407 20.1827 21.0093 20.1675 20.6775L19.575 8.34C19.5461 7.70925 19.2749 7.11398 18.8181 6.6781C18.3613 6.24222 17.7539 5.99934 17.1225 6H16.5V5.25C16.5 4.05653 16.0259 2.91193 15.182 2.06802C14.3381 1.22411 13.1935 0.75 12 0.75V0.75ZM9.00003 5.25C9.00003 4.45435 9.3161 3.69129 9.87871 3.12868C10.4413 2.56607 11.2044 2.25 12 2.25C12.7957 2.25 13.5587 2.56607 14.1213 3.12868C14.684 3.69129 15 4.45435 15 5.25V6H9.00003V5.25ZM18.075 8.4075L18.6675 20.7525C18.6722 20.8815 18.6515 21.0102 18.6064 21.1311C18.5614 21.2521 18.493 21.363 18.405 21.4575C18.3153 21.5497 18.208 21.623 18.0895 21.6733C17.971 21.7235 17.8437 21.7496 17.715 21.75H6.28503C6.15635 21.7496 6.02904 21.7235 5.91056 21.6733C5.79208 21.623 5.68481 21.5497 5.59503 21.4575C5.50711 21.363 5.43867 21.2521 5.39363 21.1311C5.34859 21.0102 5.32783 20.8815 5.33253 20.7525L5.92503 8.4075C5.93661 8.16276 6.04204 7.9319 6.21943 7.76289C6.39682 7.59388 6.63252 7.49973 6.87753 7.5H17.1225C17.3675 7.49973 17.6032 7.59388 17.7806 7.76289C17.958 7.9319 18.0635 8.16276 18.075 8.4075V8.4075Z" fill="#395589"/>
                    <path d="M8.52002 10.8925C8.93423 10.8925 9.27002 10.5567 9.27002 10.1425C9.27002 9.72824 8.93423 9.39246 8.52002 9.39246C8.10581 9.39246 7.77002 9.72824 7.77002 10.1425C7.77002 10.5567 8.10581 10.8925 8.52002 10.8925Z" fill="#395589"/>
                    <path d="M15.4801 10.8925C15.8943 10.8925 16.2301 10.5567 16.2301 10.1425C16.2301 9.72824 15.8943 9.39246 15.4801 9.39246C15.0659 9.39246 14.7301 9.72824 14.7301 10.1425C14.7301 10.5567 15.0659 10.8925 15.4801 10.8925Z" fill="#395589"/>
                </svg>
            </div>
          
            <h5>'.__('Your Cart is Empty').'</h5>
            
        </div>';
        }
        
        $response_array = array('stat'=>$stat,'msg'=>$msg,'cartHtml'=>$cartHtml,'cartCount'=>count($allCartItems));
        echo json_encode(mb_convert_encoding($response_array, 'UTF-8', 'UTF-8'));
    }
    public function updateProducts(Request $request){
        $product_id     = $request->input('product_id');
        $product_id     = base64_decode($product_id);
        $qty            = $request->input('qty');
        $currentUser    = Auth::user();
        $sessionId      = Cookie::get('imedcartcookie');
        $current_user_id = (isset($currentUser->id))?$currentUser->id:'0';
        if(isset($qty) && $qty>0 && is_numeric($qty)){
            $quantity=$qty;
        }else{
            $quantity=0;
        }

        $stat = 0;
        $msg = 'Invalid Request.';
        $responseArray  = array();

        $created_at     = currentDBDate();
        if($product_id!=''){
            $getProduct = DB::table('products')
            ->where('display_status',1)
            ->where('product_type',1)
            ->where('id',$product_id)
            ->get()->first();
            if(isset($getProduct->id)){
                $availableQty           = $getProduct->qty;
                $stock_availability     = $getProduct->stock_availability;
                $inventory_management   = $getProduct->inventory_management;
                if($quantity==0){
                    $whereCartArray = array(
                        'product_id'=>$product_id
                    );
                    if($current_user_id!=0){
                        $whereCartArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                            $query->where('cart_products.user_id','=',$current_user_id)
                                ->orWhere('cart_products.session_id', '=',$sessionId);
                        }); 
                    }else{
                        $whereCartArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                            $query->Where('cart_products.session_id', '=',$sessionId);
                        });  
                    }

                    $cartProduct   = DB::table('cart_products')
                    ->where($whereCartArray)
                    ->delete();
                    $stat = 1;
                    $msg = __('Product has been deleted successfully.');
                    Session::flash('message', __('Product has been updated successfully.'));
                }
                else if(($inventory_management==0 && $stock_availability==1) || ($inventory_management==1 && $stock_availability==1 && $availableQty>=$quantity) && $quantity>0){
                    $whereCartArray = array(
                        'product_id'=>$product_id
                    );  

                    if($current_user_id!=0){
                        $whereCartArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                            $query->where('cart_products.user_id','=',$current_user_id)
                                ->orWhere('cart_products.session_id', '=',$sessionId);
                        }); 
                    }else{
                        $whereCartArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                            $query->Where('cart_products.session_id', '=',$sessionId);
                        });  
                    }

                    $cartProduct   = DB::table('cart_products')
                    ->where($whereCartArray)
                    ->get()->first();
                    if(isset($cartProduct->id)){
                        $update_array=array(
                            'user_id'=>$current_user_id,
                            'session_id'=>$sessionId,
                            'product_id'=>$product_id,
                            'qty'=>$quantity,                            
                            'updated_at'=>$created_at
                        );
                        DB::table('cart_products')->where('id',$cartProduct->id)->update($update_array);
                    }
                    $stat = 1;
                    $msg = __('Product has been updated successfully.');
                    Session::flash('message', __('Product has been updated successfully.'));
                }else if(($inventory_management==0 && $stock_availability==1) || ($inventory_management==1 && $stock_availability==1 && $availableQty<$quantity) && $quantity>0 && $availableQty>0){
                    $quantity = $availableQty;
                    $whereCartArray = array(
                        'product_id'=>$product_id
                    );       
                    if($current_user_id!=0){
                        $whereCartArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                            $query->where('cart_products.user_id','=',$current_user_id)
                                ->orWhere('cart_products.session_id', '=',$sessionId);
                        }); 
                    }else{
                        $whereCartArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                            $query->Where('cart_products.session_id', '=',$sessionId);
                        });  
                    }
                    $cartProduct   = DB::table('cart_products')
                    ->where($whereCartArray)
                    ->get()->first();
                    if(isset($cartProduct->id)){
                        $update_array=array(
                            'user_id'=>$current_user_id,
                            'session_id'=>$sessionId,
                            'product_id'=>$product_id,
                            'qty'=>$quantity,                            
                            'updated_at'=>$created_at
                        );
                        DB::table('cart_products')->where('id',$cartProduct->id)->update($update_array);
                    }
                    $stat = 1;
                    $msg = __('Product has been updated successfully.');

                    Session::flash('message', __('Product has been updated successfully.'));
                    //$request->session()->put('appliedCoupon', $applycoupon);
                }else if($stock_availability!=1){
                    $stat = 0;
                    $msg = __('Product is out of stock.');
                }else {
                    $stat = 0;
                    $msg = __('Only '.$availableQty.' product(s) available.');
                }
            }            
        }
        $response_array = array('stat'=>$stat,'msg'=>$msg);
        echo json_encode($response_array);
    }
    public function placeOrder(Request $request){
        $currentUser        = Auth::user();
        $current_user_id    = (isset($currentUser->id))?$currentUser->id:'0';
        $sessionId          = Cookie::get('imedcartcookie');
        if($current_user_id==0){
            return redirect(url('login'))->withWarning('Please login or create an account to place your order.');
        }
        $currentCurrency = currentCurrency();
        $getCurrencyData = DB::table('currencies')->where('status',1)->where('code',$currentCurrency)->first();
        $getDefaultCurrencyData    = getDefaultCurrency(true);
        if(!empty($getCurrencyData)){
            $currency_code      = $getCurrencyData->code;
            $currency_symbol    = $getCurrencyData->symbol;
            $exchange_rate      = $getCurrencyData->exchange_rate;
        }else{            
            $currency_code      = $getDefaultCurrencyData->code;
            $currency_symbol    = $getDefaultCurrencyData->symbol;
            $exchange_rate      = $getDefaultCurrencyData->exchange_rate;
        }
        $requiredArray = array(
            'first_name'=> 'required',
            'last_name'=> 'required',
            'phone'=> 'required',
            'street_address'=> 'required',
            'country'=> 'required',
            'state'=> 'required',
            'town'=> 'required',
            'postcode'=> 'required'
        );

        if($request->input('ship_to_different_address')==1){
            $requiredArray['shipping_first_name']='required';
            $requiredArray['shipping_last_name']='required';
            $requiredArray['shipping_phone']='required';
            $requiredArray['shipping_street_address']='required';
            $requiredArray['shipping_country']='required';
            $requiredArray['shipping_state']='required';
            $requiredArray['shipping_town']='required';
            $requiredArray['shipping_postcode']='required';
        }
        $validator = Validator::make($request->all(), $requiredArray);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $updated_at         = currentDBDate();  
            $order_number       = randomString('16');
            $generalSettings    = generalSettings();
            $insert_Array = array(
                'order_number'=>$order_number,
                'order_type'=>0,
                'customer_id'=>$current_user_id,
                'agent_id'=>0,
                'first_name'=>$request->input('first_name'),
                'last_name'=>$request->input('last_name'),
                'company'=>$request->input('company'),
                'street_address'=>$request->input('street_address'),
                'apartment'=>$request->input('apartment'),
                'country'=>$request->input('country'),
                'state'=>$request->input('state'),
                'town'=>$request->input('town'),
                'postcode'=>$request->input('postcode'),
                'phone'=>$request->input('phone'),
                'payment_option'=>$request->input('payment_option'),
                'shipping_status'=>0,
                'email_address'=>$currentUser->email,
            );
            if($request->input('payment_option')==1){
                $insert_Array['status']=2;
            }else{
                $insert_Array['status']=0;
            }
            if($request->input('ship_to_different_address')==1){
                $insert_Array['shipping_first_name']    =$request->input('shipping_first_name');
                $insert_Array['shipping_last_name']     =$request->input('shipping_last_name');
                $insert_Array['shipping_company']       =$request->input('shipping_company');
                $insert_Array['shipping_phone']         =$request->input('shipping_phone');
                $insert_Array['shipping_street_address']=$request->input('shipping_street_address');
                $insert_Array['shipping_apartment']     =$request->input('shipping_apartment');
                $insert_Array['shipping_country']       =$request->input('shipping_country');
                $insert_Array['shipping_state']         =$request->input('shipping_state');
                $insert_Array['shipping_town']          =$request->input('shipping_town');
                $insert_Array['shipping_postcode']      =$request->input('shipping_postcode');
                $insert_Array['ship_to_different_address']=$request->input('ship_to_different_address');
            }else{
                $insert_Array['shipping_first_name']=$request->input('first_name');
                $insert_Array['shipping_last_name']=$request->input('last_name');
                $insert_Array['shipping_company']=$request->input('company');
                $insert_Array['shipping_phone']=$request->input('phone');
                $insert_Array['shipping_street_address']=$request->input('street_address');
                $insert_Array['shipping_apartment']=$request->input('apartment');
                $insert_Array['shipping_country']=$request->input('country');
                $insert_Array['shipping_state']=$request->input('state');
                $insert_Array['shipping_town']=$request->input('town');
                $insert_Array['shipping_postcode']=$request->input('postcode');
                $insert_Array['ship_to_different_address']=0;
            }

            $whereArray = array(
                'products.display_status'=>1,
                'products.product_type'=>1
            );
            if($current_user_id!=0){
                $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                    $query->where('cart_products.user_id','=',$current_user_id)
                        ->orWhere('cart_products.session_id', '=',$sessionId);
                });
            }else{
                $whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
                    $query->Where('cart_products.session_id', '=',$sessionId);
                });
            }           

            $allCartItems = DB::table('cart_products')
            ->select('cart_products.*','products.id as productId','products.name','products.image','products.slug','products.content','products.short_description','products.sku','products.price','products.special_price','products.stock_availability','products.inventory_management','products.qty as availableQty')
            ->join('products','products.id','=','cart_products.product_id')
            ->where($whereArray)                
            ->orderby('cart_products.id','ASC')
            ->get();
            $subTotalAmount = 0;
            if(!empty($allCartItems) && count($allCartItems)>0)
            {
                foreach($allCartItems as $singleCartItem){
                    $proPrice       = 0;
                    $qty            = $singleCartItem->qty;
                    $price          = $singleCartItem->price;
                    $special_price  = $singleCartItem->special_price;
                    $stock_availability     = $singleCartItem->stock_availability;
                    $inventory_management   = $singleCartItem->inventory_management;
                    $availableQty           = $singleCartItem->availableQty;
                    if(!empty($special_price) && $special_price>0){
                        $proPrice = $special_price;
                    }else{
                        $proPrice = $price;
                    }

                    if(!empty($singleCartItem->product_attr_id)){
                        $getProAttrCart = DB::table('products_attributes')
                            ->where('id',$singleCartItem->product_attr_id)
                            ->where('product_id',$singleCartItem->product_id)
                            ->where('is_variation',1)
                            ->first();
                        $proPrice = (isset($getProAttrCart->attribute_price) && $getProAttrCart->attribute_price>0)?$getProAttrCart->attribute_price:$proPrice;
                    }
                    if(($inventory_management==0 && $stock_availability==1) || ($inventory_management==1 && $stock_availability==1 && $availableQty>=$qty)){
                        $cartQty = $qty;
                    }else if($inventory_management==1 && $stock_availability==1 && $availableQty<$qty){
                        $cartQty = $availableQty;
                    }else{
                        $cartQty='0';
                    }
                    $totalPrice = $cartQty*$proPrice;
                    $subTotalAmount = $subTotalAmount+$totalPrice;
                }

                $coupon_code        = '';
                $userAppliedCoupons = 0;
                $availedDiscount    = 0;
                $appliedCoupon      = $request->session()->get('appliedCoupon');
                if(!empty($appliedCoupon)){
                    $currentDate    = date('Y-m-d');
                    $getCouponData  = DB::table('coupons')
                    ->where('code',$appliedCoupon)
                    ->where('start_date','<=',$currentDate)
                    ->where('end_date','>=',$currentDate)
                    ->where('status',1)
                    ->first();
                    if(!empty($getCouponData)){
                        if($current_user_id!=0){
                            $userAppliedCoupons = DB::table('orders')
                            ->where('coupon_code',$appliedCoupon)
                            ->where('customer_id',$current_user_id)
                            ->where('status',2)
                            ->count();
                        }
                        $appliedCoupons = DB::table('orders')
                            ->where('coupon_code',$appliedCoupon)
                            ->where('status',2)
                            ->count();

                        $type               = $getCouponData->type;
                        $value              = $getCouponData->value;
                        $minimum_spend      = $getCouponData->minimum_spend;
                        $maximum_spend      = $getCouponData->maximum_spend;
                        $maximum_discount   = $getCouponData->maximum_discount;
                        $usage_limit_per_coupon     = $getCouponData->usage_limit_per_coupon;
                        $usage_limit_per_customer   = $getCouponData->usage_limit_per_customer;

                        if($type=='0'){
                            $discount = $subTotalAmount-$value;
                        }else{
                            $discount = ($subTotalAmount*$value)/100;
                        }
                        if($discount<$maximum_discount){
                            $finalDiscount = $discount;
                        }else{
                            $finalDiscount = $maximum_discount;
                        }           

                        if($subTotalAmount>=$minimum_spend && $subTotalAmount<=$maximum_spend){
                            if($appliedCoupons<$usage_limit_per_coupon && $current_user_id==0){
                                $availedDiscount    = number_format($finalDiscount,2);
                                $coupon_code        = $appliedCoupon;
                            }else if($appliedCoupons<$usage_limit_per_coupon && $userAppliedCoupons<$usage_limit_per_customer && $current_user_id!=0){                      
                                $availedDiscount    = number_format($finalDiscount,2);
                                $coupon_code        = $appliedCoupon;
                            }else{
                                $request->session()->put('appliedCoupon', '');
                            }
                        }else{
                            $request->session()->put('appliedCoupon', '');
                        }
                    }else{
                        $request->session()->put('appliedCoupon', '');
                    }
                }

                $shipping_type  = $generalSettings->shipping_type;
                $taxPercent     = $generalSettings->tax;
                $handling_tax   = $generalSettings->handling_tax;
                if($shipping_type==1){
                    $shippingCharges = $generalSettings->flat_rate_fee;
                }else{
                    $shippingCharges = 0.00;
                }

                $taxAmount          = ($subTotalAmount*$taxPercent)/100;
                
                $couponDiscount     = $availedDiscount;
                if($request->input('payment_option')==1){
                    $handling_fee   = 0.00;
                    $grandTotal     = $subTotalAmount+$taxAmount+$shippingCharges-$availedDiscount;
                }else{
                    $handling_fee   = ($subTotalAmount*$handling_tax)/100;
                    $grandTotal     = $subTotalAmount+$taxAmount+$shippingCharges+$handling_fee-$availedDiscount;
                }

                $insert_Array['shipping']           = $shippingCharges;
                $insert_Array['tax']                = $taxAmount;
                $insert_Array['handling_fee']       = $handling_fee;
                $insert_Array['subtotal']           = $subTotalAmount;
                $insert_Array['coupon_discount']    = $couponDiscount;
                $insert_Array['coupon_code']        = $coupon_code;
                $insert_Array['total']              = $grandTotal;
                $insert_Array['ordered_on']         = $updated_at;
                $insert_Array['currency_code']      = $currency_code;
                $insert_Array['currency_symbol']    = $currency_symbol;
                $insert_Array['exchange_rate']      = $exchange_rate;
                $insert_Array['customer_ip_address'] = $_SERVER['REMOTE_ADDR'];
                $insertedId = DB::table('orders')->insertGetId($insert_Array);
                if($insertedId>0){
                    $request->session()->put('appliedCoupon', '');
                }
                foreach($allCartItems as $singleCartItem){
                    $qty            = $singleCartItem->qty;
                    $price          = $singleCartItem->price;
                    $special_price  = $singleCartItem->special_price;
                    $stock_availability     = $singleCartItem->stock_availability;
                    $inventory_management   = $singleCartItem->inventory_management;
                    $availableQty           = $singleCartItem->availableQty;
                    if(!empty($special_price) && $special_price>0){
                        $proPrice = $special_price;
                    }else{
                        $proPrice = $price;
                    }
                    $attrProNAme = '';
                    $product_attr_price = '0';
                    if(!empty($singleCartItem->product_attr_id)){
                        $getProAttrCart = DB::table('products_attributes')
                            ->where('id',$singleCartItem->product_attr_id)
                            ->where('product_id',$singleCartItem->product_id)
                            ->where('is_variation',1)
                            ->first();
                        $proAttribute_value =(isset($getProAttrCart->attribute_value))?$getProAttrCart->attribute_value:'';
                        $getAttrCartData = array();

                        if(!empty($proAttribute_value)){
                            $getAttrCartData = DB::table('attribute_values')
                                ->where('id',$proAttribute_value)
                                ->first();
                        }
                        $attrProNAme = (isset($getAttrCartData->name))?$getAttrCartData->name:'';
                        $proPrice = (isset($getProAttrCart->attribute_price) && $getProAttrCart->attribute_price>0)?$getProAttrCart->attribute_price:$proPrice;
                        $product_attr_price = (isset($getProAttrCart->attribute_price) && $getProAttrCart->attribute_price>0)?$getProAttrCart->attribute_price:0;
                    }

                    if(($inventory_management==0 && $stock_availability==1) || ($inventory_management==1 && $stock_availability==1 && $availableQty>=$qty)){
                        $cartQty = $qty;
                    }else if($inventory_management==1 && $stock_availability==1 && $availableQty<$qty){
                        $cartQty = $availableQty;
                    }else{
                        $cartQty='0';
                    }
                    $totalPrice = $cartQty*$proPrice;

                    $product_array = array(
                        'order_id'=>$insertedId,
                        'product_id'=>$singleCartItem->productId,
                        'is_giftcard'=>0,
                        'hash'=>NULL,
                        'quantity'=>$cartQty,
                        'sku'=>$singleCartItem->sku,
                        'name'=>$singleCartItem->name,
                        'slug'=>$singleCartItem->slug,
                        'images'=>$singleCartItem->image,
                        'description'=>$singleCartItem->content,
                        'excerpt'=>$singleCartItem->short_description,
                        'price'=>$singleCartItem->price,
                        'saleprice'=>$singleCartItem->special_price,
                        'product_attr_id'=>$singleCartItem->product_attr_id,
                        'product_attr_name'=>$attrProNAme,
                        'product_attr_price'=>$product_attr_price,
                        'total_price'=>$totalPrice,
                    );
                    DB::table('order_items')->insert($product_array);
                }
                $order_id = fetchSingleColumn('orders','order_id',array('id'=>$insertedId));                
                DB::table('cart_products')
                    ->where('user_id',$current_user_id)
                    ->orWhere('session_id',$sessionId)
                    ->delete();
                if($request->input('payment_option')==1){
                    $orderData          = DB::table('orders')
                        ->where('id',$insertedId)
                        ->first();
                    $orderProducts  = DB::table('order_items')
                        ->where('order_id',$insertedId)
                        ->get();
                    $shippingState      = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->shipping_state));
                    $billingState       = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->state));
                    $shippingCountry    = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->shipping_country));
                    $billingCountry     = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->country));
                    
                    $view_data['shippingState']     = $shippingState;
                    $view_data['billingState']      = $billingState;
                    $view_data['shippingCountry']   = $shippingCountry;
                    $view_data['billingCountry']    = $billingCountry;
                    $view_data['orderData']=$orderData;
                    $view_data['orderProducts']=$orderProducts;
                    $subject        = 'Order Successfull';
                    $userTemplate   = 'emails.userOrderConfirmation';
                    $adminTemplate  = 'emails.adminOrderConfirmation';
                    send_smtp_email($orderData->email_address,$subject,$view_data,$userTemplate);
                    send_smtp_email($generalSettings->admin_email,$subject,$view_data,$adminTemplate);
                    if(!empty($orderProducts)){
                        foreach($orderProducts as $singPro){
                            $product_id = $singPro->product_id;
                            $quantity   = $singPro->quantity;
                            $getItemData = DB::table('products')->select('qty','stock_availability')->where('id',$product_id)->where('inventory_management',1)->first();
                            if(!empty($getItemData)){
                                $remainingQty       = $getItemData->qty-$quantity;
                                $updateArray        = array();
                                $updateArray['qty'] = $remainingQty;
                                if($remainingQty<1){
                                    $updateArray['stock_availability']=0;
                                }
                                DB::table('products')->where('id',$product_id)->update($updateArray);
                            }
                        }
                    }
                    return redirect(url('order-confirmation/'.$order_id));
                }else{
                    return redirect(url('pay-now/'.$order_id));
                }
            }else{
                return redirect(url('user'));
            }
        }
    }
    public function orderConfirmation($order_id=''){
        $user       = $this->currentUser;
        $orderData  = DB::table('orders')
            ->where('order_id',$order_id)
            ->where('customer_id',$user->id)
            ->first();
        if(!empty($orderData)){
            $id = $orderData->id;            
            $shippingState      = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->shipping_state));
            $billingState       = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->state));
            $shippingCountry    = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->shipping_country));
            $billingCountry     = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->country));
            $orderProducts      = DB::table('order_items')
                ->where('order_id',$id)
                ->get();
            $view_data['currPage']          = 'myorders';
            $view_data['meta_title']        = 'Order Confirmation';
            $view_data['meta_key']          = 'Order Confirmation';
            $view_data['meta_desc']         = 'Order Confirmation';
            $view_data['page_title']        = 'Order Confirmation';
            $view_data['user']              = $user;
            $view_data['orderData']         = $orderData;
            $view_data['orderProducts']     = $orderProducts;
            $view_data['shippingState']     = $shippingState;
            $view_data['billingState']      = $billingState;
            $view_data['shippingCountry']   = $shippingCountry;
            $view_data['billingCountry']    = $billingCountry;
            return view('order-confirmation', $view_data);
        }else{
            return redirect(url('user/my-orders'));
        }        
    }
    public function payNow($order_id=''){
        $user       = $this->currentUser;
        $orderData  = DB::table('orders')
            ->where('order_id',$order_id)
            ->first();
        if(!empty($orderData)){
            $id = $orderData->id;
            $shippingState      = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->shipping_state));
            $billingState       = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->state));            
            $shippingCountry    = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->shipping_country));
            $billingCountry     = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->country));
            $orderProducts      = DB::table('order_items')
                ->where('order_id',$id)
                ->get();
            $generalSettings     = generalSettings();
            if($generalSettings->paypal_mode==0){
                $paypalFormAction = 'https://sandbox.paypal.com/cgi-bin/webscr';
            }else{
                $paypalFormAction = 'https://www.paypal.com/cgi-bin/webscr';
            }
            $view_data['currPage']          = 'myorders';
            $view_data['meta_title']        = 'Pay Now';
            $view_data['meta_key']          = 'Pay Now';
            $view_data['meta_desc']         = 'Pay Now';
            $view_data['page_title']        = 'Pay Now';
            $view_data['user']              = $user;
            $view_data['orderData']         = $orderData;
            $view_data['orderProducts']     = $orderProducts;
            $view_data['paypalFormAction']  = $paypalFormAction;
            $view_data['shippingState']     = $shippingState;
            $view_data['billingState']      = $billingState;
            $view_data['shippingCountry']   = $shippingCountry;
            $view_data['billingCountry']    = $billingCountry;
            $view_data['paypalEmail']       = $generalSettings->paypal_email;
            return view('pay-now', $view_data);
        }else{
            return redirect(url('user/my-orders'));
        }        
    }
    public function notifyUrl(Request $request){
        @mail('sumitchattha05@gmail.com','notifyResponsepost',json_encode($_POST));
        if(isset($_POST['payer_email'])){
            $payer_id       = (isset($_POST['payer_id']))?$_POST['payer_id']:'';
            $txn_id         = (isset($_POST['txn_id']))?$_POST['txn_id']:'';
            $order_id       = (isset($_POST['custom']))?$_POST['custom']:'';
            $payment_status = (isset($_POST['payment_status']))?$_POST['payment_status']:'';
            $paymentResponse = serialize($_POST);
            $orderData  = DB::table('orders')
                ->where('order_id',$order_id)
                ->first();
            if(!empty($orderData) && $orderData->status==0){
                if(strtolower($payment_status)=='completed'){
                    $generalSettings    = generalSettings();
                    $insertedId         = $orderData->id;                    
                    $shippingState      = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->shipping_state));
                    $billingState       = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->state));
                    $shippingCountry    = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->shipping_country));
                    $billingCountry     = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->country));
                    $orderProducts      = DB::table('order_items')
                        ->where('order_id',$insertedId)
                        ->get();

                    $view_data['shippingState']     = $shippingState;
                    $view_data['billingState']      = $billingState;
                    $view_data['shippingCountry']   = $shippingCountry;
                    $view_data['billingCountry']    = $billingCountry;
                    $view_data['orderData']         = $orderData;
                    $view_data['orderProducts']     = $orderProducts;
                    $subject        = 'Order Successfull';
                    $userTemplate   = 'emails.userOrderConfirmation';
                    $adminTemplate  = 'emails.adminOrderConfirmation';
                    send_smtp_email($orderData->email_address,$subject,$view_data,$userTemplate);
                    send_smtp_email($generalSettings->admin_email,$subject,$view_data,$adminTemplate);
                    if(!empty($orderProducts)){
                        foreach($orderProducts as $singPro){
                            $product_id = $singPro->product_id;
                            $quantity   = $singPro->quantity;

                            $getItemData = DB::table('products')->select('qty','stock_availability')->where('id',$product_id)->where('inventory_management',1)->first();
                            if(!empty($getItemData)){
                                $remainingQty       = $getItemData->qty-$quantity;
                                $updateArray        = array();
                                $updateArray['qty'] = $remainingQty;
                                if($remainingQty<1){
                                    $updateArray['stock_availability']=0;
                                }
                                DB::table('products')->where('id',$product_id)->update($updateArray);
                            }
                        }
                    }
                    $updateArray = array('transaction_id'=>$txn_id,'status'=>'2','payment_response'=>$paymentResponse);
                }else{
                    $updateArray = array('transaction_id'=>$txn_id,'status'=>'1','payment_response'=>$paymentResponse);
                }
                DB::table('orders')
                    ->where('order_id',$order_id)
                    ->update($updateArray);                
            }
        }
    }
    public function cancelReturn(Request $request){
        @mail('sumitchattha05@gmail.com','notifyResponsepost Request',json_encode($_REQUEST));
        $user       = $this->currentUser;
        $view_data['currPage']          = 'myorders';
        $view_data['meta_title']        = 'Cancel';
        $view_data['meta_key']          = 'Cancel';
        $view_data['meta_desc']         = 'Cancel';
        $view_data['page_title']        = 'Cancel';
        $view_data['user']              = $user;
        return view('cancel', $view_data);
    }
    public function return(Request $request){
        @mail('sumitchattha05@gmail.com','returnResponsepost',json_encode($_POST));
        if(isset($_POST['payer_email'])){
            $payer_id       = (isset($_POST['payer_id']))?$_POST['payer_id']:'';
            $txn_id         = (isset($_POST['txn_id']))?$_POST['txn_id']:'';
            $order_id       = (isset($_POST['custom']))?$_POST['custom']:'';
            $payment_status = (isset($_POST['payment_status']))?$_POST['payment_status']:'';
            $paymentResponse = serialize($_POST);
            $orderData  = DB::table('orders')
                ->where('order_id',$order_id)
                ->first();
            if(!empty($orderData) && $orderData->status==0){
                if(strtolower($payment_status)=='completed'){
                    $updateArray = array('transaction_id'=>$txn_id,'status'=>'2','payment_response'=>$paymentResponse);
                }else{
                    $updateArray = array('transaction_id'=>$txn_id,'status'=>'1','payment_response'=>$paymentResponse);
                }
                DB::table('orders')
                ->where('order_id',$order_id)
                ->update($updateArray);
                return redirect(url('order-confirmation/'.$order_id));
            }else if(!empty($orderData) && $orderData->status!=0){
               return redirect(url('order-confirmation/'.$order_id));
            }else{
                return redirect(url('user'));
            }
        }else{
            return redirect(url('user'));
        }
    }

    public function storepage($id){
        $user       = $this->currentUser;
        $view_data['currPage']          = 'Store Details';
        $view_data['meta_title']        = 'Store Details';
        $view_data['meta_key']          = 'Store Details';
        $view_data['meta_desc']         = 'Store Details';
        $view_data['page_title']        = 'Store Details';
        $view_data['storedetail'] = DB::table('users')->select('users.*')->where('users.store_slug',$id)->first();
        $view_data['lastsegment'] = $id;
        $view_data['products'] = DB::table('products')
            ->select('id','image','name','slug','price','special_price','stock_availability')
            ->where(['display_status'=>1,'user_id'=>$view_data['storedetail']->id])
            ->limit(3)->get();

        $view_data['manufacturer'] = DB::table('manufacturer')
            ->select('*')
            ->where(['display_status'=>1,'user_id'=>$view_data['storedetail']->id])
            ->get();


        $view_data['ratinglist'] = DB::table('vendor_ratings')
            ->select('*')
            ->leftJoin('users','vendor_ratings.user_id','=','users.id')
            ->where(['vendor_id'=>$view_data['storedetail']->id,'status'=>'1'])
            ->get();


        $view_data['averagerating'] = DB::table('vendor_ratings')
            ->select('*')
            ->where(['vendor_id'=>$view_data['storedetail']->id,'status'=>'1'])
            ->avg('vendor_ratings.rating');

        
        $view_data['totalcount'] = DB::table('vendor_ratings')
            ->select('*')
            ->where(['vendor_id'=>$view_data['storedetail']->id,'status'=>'1'])
            ->count('vendor_ratings.rating');
        
        
        if(Auth::user()){
            $currentuserid = Auth::user()->id;
            $store_user = $view_data['storedetail']->id; 
            if($currentuserid == $store_user){
                $view_data['showratings'] = false;
            } else{
                $view_data['showratings'] = true;
            }
        } else{
            $view_data['showratings'] = false;
        }
        return view('storepage', $view_data);
    }

    public function submitvendorrating(){
        if(!empty($_POST)){
            $created_at     = currentDBDate();
            $view_data['storedetail'] = DB::table('users')->select('users.*')->where('users.store_slug',$_POST['slug'])->first();
            $currentuserid = Auth::user()->id;

            if($currentuserid != $view_data['storedetail']->id){
                $insert_array=array(
                                    'user_id'=>Auth::user()->id,
                                    'vendor_id'=>$view_data['storedetail']->id,
                                    'rating'=>$_POST['rating'],
                                    'description'=>$_POST['description'],
                                    'added_at'=>$created_at
                                    );
                                DB::table('vendor_ratings')->insert($insert_array);
                            $stat = 1;
            } else{
                $store_slug = Auth::user()->store_slug;
                return redirect(url('storepage/'.$store_slug));
            }
        }
        
    }


    public function submitproductrating(){
        if(!empty($_POST)){
            $created_at     = currentDBDate();
             $productData = DB::table('products')
                            ->where('display_status',1)
                            ->where('product_type',1)
                            ->where('slug',$_POST['slug'])
                            ->get()
                            ->first();
            $currentuserid = Auth::user()->id;
            $productowner = $productData->user_id;
            if($currentuserid != $productowner){
                $insert_array=array(
                                    'user_id'=>Auth::user()->id,
                                    'product_id'=>$productData->id,
                                    'rating'=>$_POST['rating'],
                                    'description'=>$_POST['description'],
                                    'added_at'=>$created_at
                                    );
                                DB::table('product_ratings')->insert($insert_array);
                            $stat = 1;
            }else{
                return redirect(url('product/'.$_POST['slug']));
            }
        }
        
    }


}