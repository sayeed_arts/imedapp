<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Storage;

use Hash;
use Validator;
use Cookie;
use Session;

class UserController extends Controller
{
	protected $currentUser;
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('is-front');
        $this->middleware(function ($request, $next) {
            $this->currentUser = Auth::user();            
            return $next($request);
        });
    }
    public function index(){
        $user = $this->currentUser;
        $address  = DB::table('users_address')
            ->select('users_address.*','countries.countryName','states.stateName')
            ->join('countries','users_address.country','=','countries.countryID')
            ->join('states','users_address.state','=','states.stateID')
            ->where('users_address.user_id',$user->id)
            ->where('users_address.default',1)
            ->first();

        $view_data['currPage']       = 'dashboard';
        $view_data['meta_title']     = 'My Dashboard';
        $view_data['meta_key']       = 'My Dashboard';
        $view_data['meta_desc']      = 'My Dashboard';
        $view_data['page_title']     = 'My Dashboard';
        $view_data['address']        = $address;
        $view_data['user']           = $user;        
        return view('dashboard', $view_data);
    }   
    public function changePassword(){
        $user   = $this->currentUser;        
        $view_data['currPage']          = 'changepassword';
        $view_data['meta_title']        = 'Change Password';
        $view_data['meta_key']          = 'Change Password';
        $view_data['meta_desc']         = 'Change Password';
        $view_data['page_title']        = 'Change Password';
        $view_data['user']              = $user;
        return view('change_password', $view_data);
    }
    public function updateChangePassword(Request $request){
        $userID = $this->currentUser->id;
        $user   = Auth::user();
        $validator = Validator::make($request->all(), [
          'password'=> 'required|between:5,20',
          'new_password'=> 'required|between:5,20',
          'confirm_password'=> 'required|between:5,20|same:new_password'
        ]);
        if ($validator->fails()) {
            return redirect('user/change-password')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            if (Hash::check($request->password, $user->password)) { 
                $updated_at = currentDBDate();
                $hashpass   = Hash::make($request->input('new_password'));
                $data_array = array(
                    'password'      => $hashpass,
                    'updated_at'    => $updated_at
                    );
                DB::table('users')
                    ->where('id', $userID)
                    ->update($data_array);

                return redirect('user/change-password')->withMessage('Password has been updated successfully.');
            } else {
                $request->session()->flash('error', "Your current password is incorrect.");
                return redirect('user/change-password');
            }
        }
    }
    public function editProfile(){
        $user   = $this->currentUser;        
        $view_data['currPage']          = 'editprofile';
        $view_data['meta_title']        = 'My Account';
        $view_data['meta_key']          = 'My Account';
        $view_data['meta_desc']         = 'My Account';
        $view_data['page_title']        = 'My Account';
        $view_data['user']              = $user;
        return view('edit_profile', $view_data);
    }    
    public function updateprofile(Request $request){
        $userID = $this->currentUser->id;
        $required_array = array(
            'name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email' => 'required|email|unique:users,email,'.$userID,
            'phone_number' => 'required|regex:/[0-9]{10}/'            
        );        
        $validator = Validator::make($request->all(), $required_array);
        
        if ($validator->fails()) {
            return redirect('user/edit-profile')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $updated_at         = currentDBDate();
            $old_email          = $request->input('old_email');
            $phone_number       = $request->input('phone_number');
            $email              = $request->input('email');

            $data_array = array(
                'name'         => $request->input('name'),
                'last_name'    => $request->input('last_name'),
                'email'        => $request->input('email'),
                'phone_number' => $request->input('phone_number'),
                'updated_at'   => $updated_at
            );
            
            if($old_email!=$email){
                //$data_array['verified']='0';
            }     
            DB::table('users')
                    ->where('id', $userID)
                    ->update($data_array);            
            return redirect('user/edit-profile')->withMessage('Profile has been updated successfully.');
        }
    }
    public function myOrders(){
        $user   = $this->currentUser;
        $perpage = 15;
        $myOrders = DB::table('orders')
            ->select('id','order_id','order_number','status','shipping_status','ordered_on','total','currency_code','exchange_rate','payment_option')
            ->where('customer_id',$user->id)
            ->orderby('id','desc')
            ->paginate($perpage);
        $view_data['currPage']          = 'myorders';
        $view_data['meta_title']        = 'My Orders';
        $view_data['meta_key']          = 'My Orders';
        $view_data['meta_desc']         = 'My Orders';
        $view_data['page_title']        = 'My Orders';
        $view_data['user']              = $user;
        $view_data['myOrders']          = $myOrders;
        return view('my_orders', $view_data);
    }   
    
    public function myBids(){
        $user   = $this->currentUser;
        $perpage = 15;
        $myOrders = DB::table('auction_bids')
            ->select('auction_bids.id','auction_bids.auction_id','auction_bids.bid_amount','auction_bids.status','auction_bids.order_status','auction_bids.created_date', 'products.name')
            ->join('auctions','auction_bids.auction_id','=','auctions.id')
            ->join('products','auctions.product_id','=','products.id')
            ->where('auction_bids.user_id',$user->id)
            ->orderby('auction_bids.id','desc')
            ->paginate($perpage);
        //dd($myOrders);
        $currentCurrency = currentCurrency();
        $getCurrencyData = DB::table('currencies')->where('status',1)->where('code',$currentCurrency)->first();
        $getDefaultCurrencyData    = getDefaultCurrency(true);
        $currency_code      = $getCurrencyData->code;
        $currency_symbol    = $getCurrencyData->symbol;
        $exchange_rate      = $getCurrencyData->exchange_rate;

        $view_data['currPage']          = 'mybids';
        $view_data['meta_title']        = 'My Bids';
        $view_data['meta_key']          = 'My Bids';
        $view_data['meta_desc']         = 'My Bids';
        $view_data['page_title']        = 'My Bids';
        $view_data['user']              = $user;
        $view_data['myOrders']          = $myOrders;
        $view_data['currency_code']     = $currency_code;
        $view_data['currency_symbol']   = $currency_symbol;
        $view_data['exchange_rate']     = $exchange_rate;
        return view('my_bids', $view_data);
    } 

    public function ordersDetails($order_id=''){
        $user       = $this->currentUser;
        $orderData  = DB::table('orders')
            ->where('order_id',$order_id)
            ->where('customer_id',$user->id)
            ->first();
        if(!empty($orderData)){
            $id = $orderData->id;
            $shippingState          = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->shipping_state));
            
            $billingState           = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->state));
            $shippingCountry        = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->shipping_country));
            $billingCountry         = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->country));
            $orderProducts  = DB::table('order_items')
                ->where('order_id',$id)
                ->get();

            $view_data['currPage']          = 'myorders';
            $view_data['meta_title']        = 'Order Details';
            $view_data['meta_key']          = 'Order Details';
            $view_data['meta_desc']         = 'Order Details';
            $view_data['page_title']        = 'Order Details';
            $view_data['user']              = $user;
            $view_data['orderData']         = $orderData;
            $view_data['orderProducts']     = $orderProducts;
            $view_data['shippingState']     = $shippingState;
            $view_data['billingState']      = $billingState;
            $view_data['shippingCountry']     = $shippingCountry;
            $view_data['billingCountry']      = $billingCountry;
            return view('order-detail', $view_data);
        }else{
            return redirect(url('user/my-orders'));
        }        
    }
     
    public function submitPO($order_id=''){
        $user       = $this->currentUser;
        $orderData  = DB::table('orders')
            ->where('order_id',$order_id)
            ->where('customer_id',$user->id)
            ->where('payment_option',1)
            ->first();
        if(!empty($orderData)){
            $id = $orderData->id;
            $shippingState          = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->shipping_state));            
            $billingState           = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->state));
            $shippingCountry        = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->shipping_country));
            $billingCountry         = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->country));
            $orderProducts  = DB::table('order_items')
                ->where('order_id',$id)
                ->get();

            $view_data['currPage']          = 'submitpo';
            $view_data['meta_title']        = 'Submit PO';
            $view_data['meta_key']          = 'Submit PO';
            $view_data['meta_desc']         = 'Submit PO';
            $view_data['page_title']        = 'Submit PO';
            $view_data['user']              = $user;
            $view_data['orderData']         = $orderData;
            $view_data['orderProducts']     = $orderProducts;
            $view_data['shippingState']     = $shippingState;
            $view_data['billingState']      = $billingState;
            $view_data['shippingCountry']   = $shippingCountry;
            $view_data['billingCountry']    = $billingCountry;
            return view('submitPO', $view_data);
        }else{
            return redirect(url('user/my-orders'));
        }        
    }
    public function updateSubmitPO(Request $request){
        $user       = $this->currentUser;
        $userID     = $this->currentUser->id;
        $validator = Validator::make($request->all(), [
            'po_order_number'=> 'required',
            'po_order_file'  => 'nullable|mimes:jpeg,png,jpg,pdf|max:2048',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $data_array = array(
                'po_order_qty'      => $request->input('po_order_qty'),
                'po_order_number'   => $request->input('po_order_number'),
                'shipping_status'   => 3
            );
            $updated_at         = currentDBDate();
            $currentorderid     = base64_decode($request->input('currentorderid'));
            $orderData  = DB::table('orders')
                ->where('order_id',$currentorderid)
                ->where('customer_id',$user->id)
                ->where('payment_option',1)
                ->first();
            if(!empty($orderData)){
                $po_order_file      = $request->file('po_order_file');
                if($po_order_file!=''){
                    $path = Storage::disk('s3')->put('uploaded_images', $request->po_order_file);
                    if(strpos($path, 'uploaded_images') !== false){
                        $data_array['po_order_file']   = s3Path().$path;
                        $old_path = $request->input('old_po_order_file');
                        if(!empty($old_path)){
                            $old_path = str_replace(s3Path(), '', $old_path);
                            Storage::disk('s3')->delete($old_path);
                        }  
                    }else{
                        $image_path = randomFilename(15).'.'.$po_order_file->getClientOriginalExtension();
                        $destinationPath = public_path('/uploaded_images');
                        $po_order_file->move($destinationPath, $image_path);
                        $data_array['po_order_file']   = 'uploaded_images/'.$image_path;                
                        @unlink($request->input('old_po_order_file'));
                    }
                }
                
                DB::table('orders')
                ->where('customer_id',$userID)
                ->where('order_id',$currentorderid)
                ->update($data_array);

                $orderData  = DB::table('orders')
                ->where('order_id',$currentorderid)
                ->where('customer_id',$user->id)
                ->where('payment_option',1)
                ->first();
                $generalSettings    = generalSettings();            
                $orderProducts  = DB::table('order_items')
                    ->where('order_id',$orderData->id)
                    ->get();
                $shippingState      = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->shipping_state));
                $billingState       = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->state));
                $shippingCountry    = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->shipping_country));
                $billingCountry     = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->country));
                
                $view_data['shippingState']     = $shippingState;
                $view_data['billingState']      = $billingState;
                $view_data['shippingCountry']   = $shippingCountry;
                $view_data['billingCountry']    = $billingCountry;
                $view_data['orderData']=$orderData;
                $view_data['orderProducts']=$orderProducts;                    
                $subject                    = 'PO Order submitted Successfully';
                $adminTemplate              = 'emails.adminPOOrderConfirmation';
                send_smtp_email($generalSettings->admin_email,$subject,$view_data,$adminTemplate);

                return redirect('user/submit-po/'.$currentorderid)->withMessage('Your PO has been updated successfully.');

            }else{
                return redirect(url('user/my-orders'));
            }
        }
    }    


    public function invoiceDetails($order_id=''){
        $user       = $this->currentUser;
        $orderData  = DB::table('orders')
            ->where('order_id',$order_id)
            ->where('customer_id',$user->id)
            ->first();
        if(!empty($orderData)){
            $id = $orderData->id;
            $shippingState          = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->shipping_state));
            $billingState           = fetchSingleColumn('states','stateName',array('stateID'=>$orderData->state));
            $shippingCountry        = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->shipping_country));
            $billingCountry         = fetchSingleColumn('countries','countryName',array('countryID'=>$orderData->country));
            $orderProducts  = DB::table('order_items')
                ->where('order_id',$id)
                ->get();
            $view_data['currPage']          = 'myorders';
            $view_data['meta_title']        = 'Order Details';
            $view_data['meta_key']          = 'Order Details';
            $view_data['meta_desc']         = 'Order Details';
            $view_data['page_title']        = 'Order Details';
            $view_data['user']              = $user;
            $view_data['orderData']         = $orderData;
            $view_data['orderProducts']     = $orderProducts;            
            $view_data['shippingState']     = $shippingState;
            $view_data['billingState']      = $billingState;
            $view_data['shippingCountry']     = $shippingCountry;
            $view_data['billingCountry']      = $billingCountry;
            return view('emails.invoiceEmail', $view_data);

            /*$value      = view('emails.invoiceEmailPDF', $view_data);
            $file_name  = $orderData->order_number;
            $file_name  = str_replace(' ','-',$file_name).'.pdf';

            $result     = makedompdf($value,$file_name); 
            $file_name = $orderData->order_number;
            $file_name = str_replace(' ','-',$file_name);
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . strlen($result));
            header('Content-Disposition: attachment; filename=' . $file_name.'.pdf' );
            echo $result;
            die();*/
        }else{
            return redirect(url('user/my-orders'));
        }        
    }
    public function addressBook($id=''){
        $user   = $this->currentUser;
        $editAddress = array();
        $allStates = array();
        $allCities = array();
        $allAddress  = DB::table('users_address')
            ->select('users_address.*','countries.countryName','states.stateName')
            ->join('countries','users_address.country','=','countries.countryID')
            ->join('states','users_address.state','=','states.stateID')
            ->where('users_address.user_id',$user->id)
            ->orderby('users_address.default','desc')
            ->orderby('users_address.updated_at','desc')
            ->get();
        $allCountries = DB::table('countries')
            ->where('status',1)
            ->orderby('countryName','ASC')
            ->get();
        if($id!=''){
            $addressId = base64_decode($id);
            //$addressId = $id;
            $editAddress  = DB::table('users_address')
                ->where('id',$addressId)
                ->where('user_id',$user->id)
                ->orderby('default','desc')
                ->orderby('updated_at','desc')
                ->first();
            $countryId  = $editAddress->country;
            $stateId    = $editAddress->state;

            $allStates = DB::table('states')
                ->where('status',1)
                ->where('countryID',$countryId)
                ->orderby('stateName','ASC')
                ->get();
            /*$allCities = DB::table('cities')
                ->where('status',1)
                ->where('stateID',$stateId)
                ->orderby('cityName','ASC')
                ->get();*/

        }    
        $view_data['currPage']          = 'addressbook';
        $view_data['meta_title']        = 'Address Book';
        $view_data['meta_key']          = 'Address Book';
        $view_data['meta_desc']         = 'Address Book';
        $view_data['page_title']        = 'Address Book';
        $view_data['user']              = $user;
        $view_data['editAddress']       = $editAddress;
        $view_data['allAddress']        = $allAddress;
        $view_data['allCountries']      = $allCountries;
        $view_data['allStates']         = $allStates;
        $view_data['allCities']         = $allCities;
        return view('address_book', $view_data);
    }
    public function updateAddress(Request $request){
        $user       = $this->currentUser;
        $userID     = $this->currentUser->id;
        $validator = Validator::make($request->all(), [
          'first_name'=> 'required',
          'last_name'=> 'required',
          'address1'=> 'required',
          'country'=> 'required',
          'state'=> 'required',
          'city'=> 'required',
          'zip_code'=> 'required',
          'phone'=> 'required',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $updated_at         = currentDBDate();
            $action             = $request->input('action');
            $recid              = $request->input('recid');            
            $defaultaddress     = $request->input('defaultaddress');            
            $data_array = array(
                'first_name'    => $request->input('first_name'),
                'last_name'     => $request->input('last_name'),
                'company'       => $request->input('company'),
                'address1'      => $request->input('address1'),
                'address2'      => $request->input('address2'),
                'country'       => $request->input('country'),
                'state'         => $request->input('state'),
                'city'          => $request->input('city'),
                'phone'         => $request->input('phone'),
                'zip_code'      => $request->input('zip_code'),
                'updated_at'    => $updated_at
            );
            if(isset($defaultaddress) && $defaultaddress){
                $data_array['default']=1;
                DB::table('users_address')->where('user_id',$userID)->update(['default'=>0]);
            }
            if($action=='edit' && $recid!=''){
                DB::table('users_address')
                ->where('user_id',$userID)
                ->where('id',$recid)
                ->update($data_array);
                return redirect('user/address-book')->withMessage('Address has been updated successfully.');
            }else{
                $data_array['user_id']=$userID;              
                $data_array['created_at']=$updated_at;              
                DB::table('users_address')
                ->insert($data_array);
                return redirect('user/address-book')->withMessage('Address has been inserted successfully.');
            }
        }
    }
    public function removeAddress(Request $request){
        $user       = $this->currentUser;
        $userID     = $this->currentUser->id;
        $address_id = $request->input('address_id');
        if(!empty($address_id)){
            DB::table('users_address')
                ->where('id',$address_id)
                ->where('user_id',$userID)
                ->delete();
            $defaultAddress = DB::table('users_address')->where('user_id',$userID)->where('default',1)->first();
            if(empty($defaultAddress)){
                $getLatestAddress = DB::table('users_address')->where('user_id',$userID)->first();
                if(!empty($getLatestAddress)){
                    DB::table('users_address')->where('id',$getLatestAddress->id)->update(['default'=>1]);
                }
            }
            return redirect('user/address-book')->withMessage('Address has been removed successfully.');
        }else{
            return redirect('user/address-book');
        }
    }
    public function myWishlist(){
        $user       = $this->currentUser;
        $myWishlist = DB::table('wishlist')
            ->select('products.*')
            ->join('products','wishlist.product_id','=','products.id')
            ->where('products.display_status',1)
            ->where('wishlist.user_id',$user->id)
            ->orderby('wishlist.id','desc')
            ->get();
        $view_data['currPage']          = 'mywishlist';
        $view_data['meta_title']        = 'My Wishlist';
        $view_data['meta_key']          = 'My Wishlist';
        $view_data['meta_desc']         = 'My Wishlist';
        $view_data['page_title']        = 'My Wishlist';
        $view_data['user']              = $user;
        $view_data['myWishlist']        = $myWishlist;
        return view('my_wishlist ', $view_data);
    }
    public function removeWishlist(Request $request){
        $user       = $this->currentUser;
        $userID     = $this->currentUser->id;
        $product_id = $request->input('product_id');
      
        if(!empty($product_id)){
            DB::table('wishlist')
                ->where('product_id',$product_id)
                ->where('user_id',$userID)
                ->delete();            
            return redirect('user/my-wishlist')->withMessage('Product has been removed successfully.');
        }else{
            return redirect('user/my-wishlist');
        }
    }
    public function addWishlist(Request $request){
        $user       = $this->currentUser;
        $product_id = $request->input('product_id'); 
        $product_id = base64_decode($product_id);       
        $productExists = DB::table('wishlist')
            ->where('user_id',$user->id)
            ->where('product_id',$product_id)
            ->get();
        if(count($productExists)==0){
            $created_at     = currentDBDate();
            $insertArray = array(
                'user_id'=>$user->id,
                'product_id'=>$product_id,
                'created_at'=>$created_at,
                'updated_at'=>$created_at
            );
            DB::table('wishlist')->insert($insertArray);
            $stat = 1;
            $msg = 'Product has been added successfully.';
        }else{
             DB::table('wishlist')
            ->where('user_id',$user->id)
            ->where('product_id',$product_id)
            ->delete();
            $stat = 0;
            $msg = 'Product has been removed successfully.';
        }
        $response_array = array('stat'=>$stat,'msg'=>$msg);
        echo json_encode($response_array);
    }
    public function deleteWishlist(Request $request){
        $user       = $this->currentUser;
        $product_id = $request->input('product_id');
        $stat = 0;
        $msg = 'Error occured. Please try again.';
        $productExists = DB::table('wishlist')
            ->where('user_id',$user->id)
            ->where('product_id',$product_id)
            ->get();
        if(count($productExists)==0){           
            DB::table('wishlist')
            ->where('user_id',$user->id)
            ->where('product_id',$product_id)
            ->delete();
            $stat = 1;
            $msg = 'Product has been deleted successfully.';
        }else{
            $stat = 0;
            $msg = 'Product does not exists in wishlist.';
        }
        $response_array = array('stat'=>$stat,'msg'=>$msg);
        echo json_encode($response_array);
    }
}