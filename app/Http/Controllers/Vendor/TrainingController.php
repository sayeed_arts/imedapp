<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Hash;
use Validator;
use App\TrainingCategory;

class TrainingController extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth');      
    }
    public function index()
    {        
        $allRecords = DB::table('trainings')
        ->orderby('display_order', 'ASC')
        ->where('trainings.vendor_id',Auth::user()->id)
        ->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'alltrainings';
        $view_data['allRecords']    = $allRecords;
        return view('vendor.alltrainings', $view_data);
    }

    public function add()
    {        
        $allCategories = TrainingCategory::orderby('display_order','ASC')->get();          
        $view_data['currPage']      = 'alltrainings';
        $view_data['hasForm']       = 'yes';
        $view_data['allCategories'] = $allCategories; 
        $view_data['vendorlist'] = DB::table('users')->where('users.is_admin','=','2')->where('users.is_approved','=','1')->pluck( 'name','id');
        return view('vendor.addtraining',$view_data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'content'           => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('vendor/trainings/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();            
            $data_array['name']             = $request->input('name');
            $data_array['slug']             = create_slug($request->input('name'),'','trainings','slug');
            $data_array['content']          = $request->input('content');
            $data_array['start_date']       = date("Y-m-d", strtotime($request->input('start_date')));
            $data_array['closing_date']     = date("Y-m-d", strtotime($request->input('closing_date')));
            $data_array['category_id']      = implode(",", $request->input('category_id'));
            $data_array['location']         = $request->input('location');
            $data_array['vendor_id']        = Auth::user()->id;
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['created_at']       = $created_at;
            $data_array['updated_at']       = $created_at;
            
            DB::table('trainings')
            ->insert($data_array);

            return redirect('vendor/trainings/')->withMessage('Training has been added successfully.');
        }
    }
    public function edit($id)
    {
        $record   = DB::table('trainings')->where('id', $id)->first();  
        $allCategories = TrainingCategory::orderby('display_order','ASC')->get();        
        if ($record != null){
            $view_data['currPage']      = 'alltrainings';
            $view_data['hasForm']       = 'yes';
            $view_data['record']         = $record;
            $view_data['allCategories'] = $allCategories;
            $view_data['vendorlist'] = DB::table('users')->where('users.is_admin','=','2')->where('users.is_approved','=','1')->pluck( 'name','id');
            return view('vendor.edittraining', $view_data);
        }else{
            return redirect('vendor/trainings'); 
        } 
    }
    public function update(Request $request)
    {
        $currid = $request->input('currid');
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'content'           => 'required',
            'display_order'     => 'required|numeric|min:1',
            'display_status'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('vendor/trainings/edit/'.$currid)
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $created_at     = currentDBDate();
            $data_array['name']             = $request->input('name');
            $data_array['slug']             = create_slug($request->input('name'),'','trainings','slug');
            $data_array['content']          = $request->input('content');
            $data_array['start_date']       = date("Y-m-d", strtotime($request->input('start_date')));
            $data_array['closing_date']     = date("Y-m-d", strtotime($request->input('closing_date')));
            $data_array['category_id']      = implode(",", $request->input('category_id'));
            $data_array['location']         = $request->input('location');
            $data_array['vendor_id']        = Auth::user()->id;
            $data_array['display_order']    = $request->input('display_order');
            $data_array['display_status']   = $request->input('display_status');
            $data_array['updated_at']       = $created_at;
            
            DB::table('trainings')
            ->where('id', $currid)
            ->update($data_array);

            return redirect('vendor/trainings/')->withMessage('Training has been updated successfully.');
        }
    }
    public function delete($id)
    {
        $record   = DB::table('trainings')->where('id', $id)->first();        
        if ($record != null){
            DB::table('trainings')->where('id', $id)->delete();
            return redirect('vendor/trainings/')->withMessage('Training has been deleted successfully.');
        }else{
            return redirect('vendor/trainings'); 
        } 
    }    
}