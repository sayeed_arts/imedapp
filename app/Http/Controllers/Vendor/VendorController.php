<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;
use Hash;
use Validator;
use App\Users;
use Carbon\Carbon; 

class VendorController extends Controller
{
//    protected $currentUser;
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->currentUser = Auth::user();
            return $next($request);
        });

    }
    public function index()
    {
        $view_data['current_user']      = Auth::user();
        $view_data['hasTable']          = 'yes';
        $view_data['chartTitle']        = 'Orders';
        $view_data['chartTitle']        = 'Orders';
        $view_data['currPage']          = 'dashboard';

        // $view_data['receivedOrder']     = DB::table('orders')->where('status','<>',0)->where('shipping_status',0)->count();
        // $view_data['processingOrder']   = DB::table('orders')->where('status','<>',0)->where('shipping_status',3)->count();
        // $view_data['voidedOrder']       = DB::table('orders')->where('status','<>',0)->where('shipping_status',2)->count();
        // $view_data['shippedOrder']      = DB::table('orders')->where('status','<>',0)->where('shipping_status',1)->count();
        // $view_data['completedOrder']    = DB::table('orders')->where('status','<>',0)->where('shipping_status',4)->count();


        // $view_data['total_sales']       = DB::table('orders')->where('status',2)->sum('total');
        // $view_data['total_orders']      = DB::table('orders')->where('status','<>',0)->count();
        // $view_data['total_products']    = DB::table('products')->count();
        // $view_data['total_users']       = DB::table('users')->where('is_admin',0)->count();
       
// Code by Yogesh Negi

        
        $view_data['total_vendor_products']       = DB::table('products')
        ->select('products.*')
        ->leftJoin('users','products.user_id','=','users.id')
        ->where('users.is_admin',2)
        ->where('products.user_id',Auth::user()->id)
        ->count();

        $total_vendor_orders       = DB::table('orders')
        ->select('orders.*','users.name as customer_first_name','users.last_name as customer_last_name')
        ->join('users','orders.customer_id','=','users.id')
        ->join('order_items','orders.id','=','order_items.order_id')
        ->join('products','order_items.product_id','=','products.id')
        ->where('orders.status','<>',0)
        ->where('products.user_id',Auth::user()->id)
        ->groupBy('orders.id','order_items.order_id')
        ->orderby('ordered_on','DESC')
        ->get();

        $view_data['total_vendor_orders'] = count($total_vendor_orders);



        $view_data['total_vendor_enquiries']       = DB::table('getaquote')
        ->select('getaquote.*')
        ->leftJoin('products','getaquote.product_id', '=', 'products.id')
        ->where('products.user_id',Auth::user()->id)
        ->count();
       
       $view_data['total_vendor_pay_count']      = DB::table('order_items')
        ->leftJoin('orders','order_items.order_id','=','orders.id')
        ->leftJoin('products','order_items.product_id','=','products.id')
        ->leftJoin('users','products.user_id','=','users.id')
        ->where('products.user_id',Auth::user()->id)
        ->sum('order_items.total_price');



        $view_data['total_vendor_pending_products']       = DB::table('products')
        ->select('products.*')
        ->leftJoin('users','products.user_id','=','users.id')
        ->where('users.is_admin',2)
        ->where('products.user_id',Auth::user()->id)
        ->where('products.display_status',0)
        ->count();

// Code by Yogesh Negi       

        
        // $view_data['latest_users']      = DB::select("select * from users where is_admin='0' ORDER BY id DESC LIMIT 10");
         $view_data['latest_orders']     = DB::table('orders')
        ->select('orders.*','users.name as customer_first_name','users.last_name as customer_last_name')
        ->join('users','orders.customer_id','=','users.id')
        ->join('order_items','orders.id','=','order_items.order_id')
        ->join('products','order_items.product_id','=','products.id')
        ->where('orders.status','<>',0)
        ->where('products.user_id',Auth::user()->id)
        ->groupBy('orders.id','order_items.order_id')
        ->orderby('ordered_on','DESC')
        ->take(10)
        ->get();

        return view('vendor.dashboard',$view_data);  
    }

       public function add()
    {     

        $id = Auth::user()->id;
        $view_data['countrylist'] = DB::table('countries')->pluck( 'countryName','countryID');
        $view_data['hasForm']       = 'yes';
        $view_data['currPage']  = 'addvendor';
       if(!empty($id)){
        $view_data['vendordetail'] = DB::table('users')->where('id', $id)->where('is_admin', '2')->first();

        $view_data['allState'] = DB::table('states')->select('stateID','stateName','countryID')->where(array('status'=>'1','countryID'=>$view_data['vendordetail']->country))->orderby('stateName')->get();
       

       }
        return view('vendor.addvendor',$view_data);
    }
    public function save(Request $request)
    {    

     if(!empty($request->input('vendor_id'))){
        $validator = Validator::make($request->all(), [
          'name'    => 'required|max:50',
          'last_name'    => 'required',
          'address1'    => 'required',
          'city'    => 'required',
          'title'    => 'required',
          'store'    => 'required',
          'state'    => 'required',
          'compweb'    => 'required',
          'ext'    => 'required',
          'comp_name'    => 'required',
          'phone_number'    => 'required',
          'email'   => 'required|email|unique:users,email,'.$request->input('vendor_id'),
        ]);
        }
        else{
         $validator = Validator::make($request->all(), [
          'name'    => 'required|max:50',
          'last_name'    => 'required',
          'address1'    => 'required',
          'city'    => 'required',
          'title'    => 'required',
          'store'    => 'required|unique:users,Store',
          'state'    => 'required',
          'compweb'    => 'required',
          'ext'    => 'required',
          'comp_name'    => 'required',
          'phone_number'    => 'required',
          'email'   => 'required|email|unique:users,email',
          'password'=> 'required|between:5,20',
          'confirm_password'=> 'required|between:5,20|same:password'
        ]);
            
        }
        if ($validator->fails()) {

                 if(empty($request->input('vendor_id'))){
                 
                        return redirect('admin/editprofile')
                                    ->withErrors($validator)
                                    ->withInput();
                 }
                 else{
                        return redirect('vendor/editprofile')
                                    ->withErrors($validator)
                                    ->withInput();  
                 }
        }else{
    
            $hashpass       = Hash::make($request->input('password')); 
            $created_at     = currentDBDate();


            $insert_array = array(
                'name'=>$request->input('name'),
                'last_name'=> $request->input('last_name'),
                'email'=>$request->input('email'),
                'address1'=>$request->input('address1'),
                'city'=>$request->input('city'),
                'title'=>$request->input('title'),
                'country'=>$request->input('country'),
                'store_slug'=> $this->slugify($request->input('store')),
                'company'=>$request->input('comp_name'),
                'Store'=>$request->input('store'),
                'state'=>$request->input('state'),
                'website'=>$request->input('compweb'),
                'ext'=>$request->input('ext'),
                'phone_number'=> $request->input('phone_number'),
                'zipcode'=> $request->input('zipcode'),                
                'is_approved'=> '1', 
                'verified'=> '1', 
                'is_admin'=>'2',
                'password'=>$hashpass,
                'created_at'=>$created_at,
                'updated_at'=>$created_at
                );


           if(!empty($request->input('vendor_id'))){
               unset($insert_array['is_admin']);
               unset($insert_array['password']);
               unset($insert_array['created_at']);
               $insert_array['is_approved'] = $request->input('is_approved');
               DB::table('users')->where('id', $request->input('vendor_id'))
              ->update($insert_array);

            $template   = 'emails.vendorapprovedEmail';
            $view_data['name']     = $insert_array['name']." ".$insert_array['last_name'];
            //send_smtp_email($insert_array['email'],'Vendor Account Approved',$view_data,$template,array(),array());

            return redirect('vendor/editprofile/')->withMessage('Profile details has been updated successfully.');
           }
           else{
            $insert_array['readpass'] = $request->input('password');
            $lastid = DB::table('users')->insertGetId($insert_array);
            $data = array('user_id' => $lastid,
                'token' => str_random(40));

            $verifyUser = DB::table('verify_users')->insert($data);
            
            $view_data['name']     = $insert_array['name']." ".$insert_array['last_name'];
            $view_data['verifyUsertoken']     = $data['token'];
            $view_data['email']     = $insert_array['email'];
            $view_data['password']     = $insert_array['readpass'];
            
            $template   = 'emails.vendoraccountEmail';
            send_smtp_email($insert_array['email'],'Vendor Account Created',$view_data,$template,array(),array());


            $admintemplate = 'emails.vendoraccountadminEmail';
            $admin_view_data['name'] = $insert_array['name']." ".$insert_array['last_name'];
            $admin_view_data['email'] =$insert_array['email']; 
            send_smtp_email('negiyogesh145@gmail.com','Vendor Account Created',$admin_view_data,$admintemplate,array(),array());

            return redirect('admin/vendors/')->withMessage('Vendor details has been added successfully.');
            
           }

        }
    }

    public function slugify($text, string $divider = '-')
{
  // replace non letter or digits by divider
  $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, $divider);

  // remove duplicate divider
  $text = preg_replace('~-+~', $divider, $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

    public function storepage(){

        $view_data['current_user']      = Auth::user();
        $view_data['hasTable']          = 'yes';
        $view_data['chartTitle']        = 'Orders';
        $view_data['chartTitle']        = 'Orders';
        $view_data['currPage']          = 'dashboard';
        $view_data['storedata']          = DB::table('users')->select('*')->where('id',Auth::user()->id)->first();
        
                return view('vendor.storepage',$view_data);   
            }
        
    public function updatestorepage(Request $request){

           
$update_array = array();

         $logo          = $request->file('logo');
            if($logo!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->logo);
                if(strpos($path, 'uploaded_images') !== false){
                    $update_array['logo']   = s3Path().$path;
                 
                    $old_path = $request->input('logo');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    }  
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $update_array['logo']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_logo_image'));
                }
            }
         $banner          = $request->file('banner');
            if($banner!=''){
                $path = Storage::disk('s3')->put('uploaded_images', $request->banner);
                if(strpos($path, 'uploaded_images') !== false){
                    $update_array['banner']   = s3Path().$path;
                    $old_path = $request->input('old_image');
                    if(!empty($old_path)){
                        $old_path = str_replace(s3Path(), '', $old_path);
                        Storage::disk('s3')->delete($old_path);
                    }  
                }else{
                    $image_path = randomFilename(15).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploaded_images');
                    $image->move($destinationPath, $image_path);
                    $update_array['banner']   = 'uploaded_images/'.$image_path;                
                    @unlink($request->input('old_banner_image'));
                }
            }
           
             $update_array['fb_link'] = $request->input('fb_link');
             $update_array['insta_link'] = $request->input('insta_link');
             $update_array['pinterest_link'] = $request->input('pinterest_link');
             $update_array['twitter_link'] = $request->input('twitter_link');

// echo "<pre>";
// print_r($update_array);
// die;
             DB::table('users')->where('id', Auth::user()->id)
              ->update($update_array);

            return redirect('vendor/storepage/')->withMessage('Store Details has been updated successfully.');
    

    }

      public function vendorpaymentlogs(){
        $allUsers = DB::select("select * from vendor_payments where vendor_payments.vendor_id=".Auth::user()->id."  ORDER BY id DESC");

        $view_data['userdetails'] = DB::table('users')->where('id', Auth::user()->id)->first();

        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allusers';
        $view_data['allRecords']    = $allUsers;
        return view('vendor.allvendorpaylogs', $view_data);
    }


  public function reports(Request $request)
    {        
        $view_data['currPage']      = 'reports';
        $start_date     = $request->input('start_date');
        $end_date       = $request->input('end_date');
        $export_type    = $request->input('export_type');
        $export         = $request->input('export');
        $export_order_status                = $request->input('export_order_status');
        $view_data['start_date']            = $start_date;
        $view_data['end_date']              = $end_date;
        $view_data['export_type']           = $export_type;
        $view_data['export_order_status']   = $export_order_status;
       
        if(!empty($start_date) && !empty($end_date)){
            /*$from_date_array= explode('-', $start_date);
            $to_date_array  = explode('-', $end_date);
            $from_date      = $from_date_array[2].'-'.$from_date_array[1].'-'.$from_date_array[0];
            $to_date        = $to_date_array[2].'-'.$to_date_array[1].'-'.$to_date_array[0];*/
            $headers = array(
                "Content-type" => "text/csv",
                "Content-Disposition" => "attachment; filename=file.csv",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
            );
            if($export_type=='1'){
                //Export All Users
                $reviews = DB::table('users')
                ->where(array('is_admin'=>0))
                ->where('created_at','>=',$start_date)
                ->where('created_at','<=',$end_date)
                ->orderby('id','DESC')
                ->get(); 
                if($export=='Search'){
                    $view_data['hasTable']      = 'yes';
                    $view_data['allRecords']    = $reviews;
                    return view('admin.reportallusers', $view_data);
                }else{
                    $columns = array('Name', 'Email', 'Phone', 'Email Verified', 'Status', 'Created');

                    $filename = "AllUsers-".date('Y-m-d').".csv";
                    $file = fopen($filename, 'w+');
                    fputcsv($file, $columns);

                    foreach($reviews as $review) {
                        if($review->verified==1){
                            $verified="Yes";
                        }else{
                            $verified="No";
                        }
                        if($review->is_approved==1){
                            $is_approved="Active";
                        }else{
                            $is_approved="InActive";
                        }
                        fputcsv($file, array($review->name.' '.$review->last_name, $review->email, $review->phone_number,$verified, $is_approved, $review->created_at));
                    }
                    fclose($file); 
                }                  
            }else if($export_type==2){
                //Export All PO Orders
                $whereArray = array('orders.payment_option'=>1);
                if($export_order_status!=''){
                    if($export_order_status==-1){
                        $export_order_status = 0;
                    }
                    $whereArray['orders.shipping_status'] = $export_order_status;
                }
                $reviews = DB::table('orders')
                ->select('orders.*','users.name as customer_first_name','users.last_name as customer_last_name')
                ->join('users','orders.customer_id','=','users.id')
                ->leftjoin('order_items','orders.id','=','order_items.order_id')
                ->leftjoin('products','order_items.product_id','=','products.id')
                ->where('products.user_id',Auth::user()->id)
                ->where('orders.status','<>',0)
                ->where('orders.ordered_on','>=',$start_date)
                ->where('orders.ordered_on','<=',$end_date)
                ->where($whereArray)
                ->groupBy('orders.id','order_items.order_id')
                ->orderby('orders.id','DESC')
                ->get(); 


           

                if($export=='Search'){
                    $view_data['hasTable']      = 'yes';
                    $view_data['allRecords']    = $reviews;
                    return view('admin.reportallPOOrders', $view_data);
                }else{
                    $columns = array('Order No', 'Customer Purchase Order Number', 'Wire Bank information', 'Name', 'Email', 'Phone', 'Status', 'Sub Total', 'Tax', 'Discount', 'Shipping', 'Total', 'Date');

                    $filename = "AllPOOrders-".date('Y-m-d').".csv";
                    $file = fopen($filename, 'w+');
                    fputcsv($file, $columns);
                    foreach($reviews as $review) {                        
                        if($review->shipping_status==0){
                            $is_approved="Received";
                        }elseif($review->shipping_status==1){
                            $is_approved="Shipped";
                        }elseif($review->shipping_status==2){
                            $is_approved="Voided";
                        }elseif($review->shipping_status==3){
                            $is_approved="Processing";
                        }elseif($review->shipping_status==4){
                            $is_approved="Completed";
                        }
                        fputcsv($file, array('PRO-IMED-'.$review->id,$review->po_order_number,$review->po_order_qty,$review->customer_first_name.' '.$review->customer_last_name, $review->email_address, $review->phone,$is_approved, showAdminPrice($review->subtotal),showAdminPrice($review->tax), showAdminPrice($review->coupon_discount),showAdminPrice($review->shipping),showAdminPrice($review->total), $review->ordered_on));
                    }
                    fclose($file); 
                }          
            }else if($export_type==3){
                //Export All Sales Orders
                $whereArray = array('orders.payment_option'=>2);
                if($export_order_status!=''){
                    if($export_order_status==-1){
                        $export_order_status = 0;
                    }
                    $whereArray['orders.shipping_status'] = $export_order_status;
                }
                $reviews = DB::table('orders')
                ->select('orders.*','users.name as customer_first_name','users.last_name as customer_last_name')
                ->join('users','orders.customer_id','=','users.id')
                ->leftjoin('order_items','orders.id','=','order_items.order_id')
                ->leftjoin('products','order_items.product_id','=','products.id')
                ->where('products.user_id',Auth::user()->id)
                ->where('orders.status','<>',0)
                ->where('orders.ordered_on','>=',$start_date)
                ->where('orders.ordered_on','<=',$end_date)
                ->where($whereArray)
                ->groupBy('orders.id','order_items.order_id')
                ->orderby('orders.id','DESC')
                ->get(); 
                if($export=='Search'){
                    $view_data['hasTable']      = 'yes';
                    $view_data['allRecords']    = $reviews;
                    return view('admin.reportallSalesOrders', $view_data);
                }else{
                    $columns = array('Order No','Transaction Id', 'Name', 'Email', 'Phone', 'Status', 'Sub Total', 'Tax', 'Discount', 'Shipping', 'Total', 'Date');

                    $filename = "AllSalesOrders-".date('Y-m-d').".csv";
                    $file = fopen($filename, 'w+');
                    fputcsv($file, $columns);
                    foreach($reviews as $review) {                        
                        if($review->shipping_status==0){
                            $is_approved="Received";
                        }elseif($review->shipping_status==1){
                            $is_approved="Shipped";
                        }elseif($review->shipping_status==2){
                            $is_approved="Voided";
                        }elseif($review->shipping_status==3){
                            $is_approved="Processing";
                        }elseif($review->shipping_status==4){
                            $is_approved="Completed";
                        }
                        fputcsv($file, array('PRO-IMED-'.$review->id,$review->transaction_id,$review->customer_first_name.' '.$review->customer_last_name, $review->email_address, $review->phone,$is_approved, showAdminPrice($review->subtotal),showAdminPrice($review->tax), showAdminPrice($review->coupon_discount),showAdminPrice($review->shipping),showAdminPrice($review->total), $review->ordered_on));
                    }
                    fclose($file); 
                }            
            }
            return Response::download($filename, $filename, $headers);
        }
        return view('vendor.reports', $view_data);
    }


   public function vendorreview(){
      $allRecords = DB::table('vendor_ratings')
        ->leftjoin('users', 'vendor_ratings.user_id', '=', 'users.id')
        ->leftjoin('users as vendor_detail', 'vendor_ratings.vendor_id', '=', 'vendor_detail.id')
        ->select('vendor_ratings.*', 'users.name','users.last_name','vendor_detail.Store')
        ->where('vendor_ratings.vendor_id',Auth::user()->id)
        ->groupBy('vendor_ratings.id')
        ->orderby('vendor_ratings.id', 'DESC')

        ->get();
        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allvendorreviews';
        $view_data['allRecords']    = $allRecords;
        return view('vendor.allvendorreviews', $view_data);
   }
   
   public function productreview(){
       $allRecords = DB::table('product_ratings')
        ->join('users', 'product_ratings.user_id', '=', 'users.id')
        ->join('products', 'product_ratings.product_id', '=', 'products.id')
        ->select('product_ratings.*', 'users.name','users.last_name','products.name as product_name')
        ->where('products.user_id',Auth::user()->id)
        ->orderby('product_ratings.id', 'DESC')
        ->get();

        $view_data['hasTable']      = 'yes';
        $view_data['currPage']      = 'allproductreviews';
        $view_data['allRecords']    = $allRecords;
        return view('vendor.allproductreviews', $view_data);
   }

  public function updateprodreviewstatus(){
        if(!empty($_POST)){
                 DB::table('product_ratings')->where('id', $_POST['auction_id'])->update(array('status'=>$_POST['status'])); 
                 echo "1";
                  die;
              }
              else{
                echo "0";
              }
  }
  
  public function updatevenreviewstatus(){
       if(!empty($_POST)){
             DB::table('vendor_ratings')->where('id', $_POST['auction_id'])->update(array('status'=>$_POST['status'])); 
             echo "1";
              die;
          }
          else{
            echo "0";
          } 
  }
}