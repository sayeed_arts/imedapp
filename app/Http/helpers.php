<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Category;
require_once  __DIR__ .'/../../dompdf/autoload.inc.php';
use Dompdf\Dompdf;
function makedompdf($html,$filename='Invoice.pdf'){	
    $dompdf = new Dompdf();
	$dompdf->set_option('isRemoteEnabled', TRUE);
	$dompdf->loadHtml($html);
	$dompdf->set_option('isRemoteEnabled', TRUE);
	$dompdf->set_option('DOMPDF_ENABLE_CSS_FLOAT', TRUE);
	$dompdf->setPaper('A4', 'landscape');
	$dompdf->render();
	//$dompdf->stream($filename,array("Attachment"=>0));
    return $dompdf->output();
}
function s3Path(){
	return 'https://imedicalshop.s3.amazonaws.com/';
}
function exchangeRate($amount, $from_currency, $to_currency) {
	$account_id = 'imedicalequipment&service257510477';
	$api_key 	= '5vlsrbcf56fou1slf56s9h2b4i';
	$url    	= "https://xecdapi.xe.com/v1/convert_from.json/?from=$from_currency&to=$to_currency&amount=$amount";
	$ch     	= @curl_init();
	$timeout 	= 0;	 
	curl_setopt ($ch, CURLOPT_URL, $url);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	curl_setopt($ch, CURLOPT_USERPWD, $account_id . ":".$api_key);	 
	$rawdata = curl_exec($ch);
	curl_close($ch);
	return $rawdata;
}

if (!function_exists('showAdminPrice')) {
    function showAdminPrice($price,$isFiltered=0){
    	$defaultCurrency= getDefaultCurrency(true);
    	if($isFiltered==1){
			return $price;
		}else if($defaultCurrency->symbol_position==0){
			return $defaultCurrency->symbol.''.number_format($price,2);
		}else{
			return number_format($price,2).$defaultCurrency->symbol;
		}
    }
}

if (!function_exists('vendorpayment')) {
	function vendorpayment($id){
		$totalsum = DB::table('order_items')
        ->leftJoin('orders','order_items.order_id','=','orders.id')
        ->leftJoin('products','order_items.product_id','=','products.id')
        ->leftJoin('users','products.user_id','=','users.id')
        ->where('products.user_id',$id)
        ->sum('order_items.total_price');
		return $totalsum;
	}
}

if (!function_exists('vendorpaymentsdone')) {
	function vendorpaymentsdone($id){
		$totalPayementDone = DB::select("select SUM(commission) AS paidcommission from vendor_payments where vendor_payments.vendor_id=".$id."");
		return $totalPayementDone[0]->paidcommission?$totalPayementDone[0]->paidcommission:'0';
	}
}

if (!function_exists('vendorcommission')) {
	function vendorcommission($id){
		$vendorcommission = DB::table('users')->select('users.commission')->where('users.id',$id);
		if(!empty($vendorcommission->commission)){
			$commission = $vendorcommission->commission;
		}
		if(empty($commission)){
			$globalcommission = DB::table('settings')->select( 'settings.vendor_commission')->first();
			$commission = $globalcommission->vendor_commission;
		}
		$vendorpayment = vendorpayment($id);
		$commssion_amt = $vendorpayment * $commission/100;
		return $commssion_amt;
	}
}

if (!function_exists('showOrderPrice')) {
    function showOrderPrice($price,$appcurrency,$exchangeRate,$isFiltered=0){
    	$defaultCurrency= getDefaultCurrency(true);
    	if($defaultCurrency->code==$appcurrency){
    		if($isFiltered==1){
    			return $price;
    		}else if($defaultCurrency->symbol_position==0){
    			return $defaultCurrency->symbol.''.number_format($price,2);
    		}else{
    			return number_format($price,2).$defaultCurrency->symbol;
    		}
    	}else{
    		$selectedCurrData = DB::table('currencies')
    			->where('status',1)
    			->where('code',$appcurrency)
    			->first();

    		$price 			= $price*$exchangeRate;    		
    		if($isFiltered==1){
    			return $price;
    		}else if($selectedCurrData->symbol_position==0){
    			return $selectedCurrData->symbol.''.number_format($price,2);
    		}else{
    			return number_format($price,2).$selectedCurrData->symbol;
    		}
    	}
    }
}


if (!function_exists('countrylist')) {
	function countrylist(){
		$countrylist = DB::table('countries')->pluck( 'countryName','countryID');
        return $countrylist;
	}
}

if (!function_exists('showPrice')) {
    function showPrice($price,$isFiltered=0){
    	$defaultCurrency= getDefaultCurrency(true);
    	$appcurrency 	= Session::get('appcurrency'); 
    	if($defaultCurrency->code==$appcurrency){
    		if($isFiltered==1){
    			return $price;
    		}else if($defaultCurrency->symbol_position==0){
    			return $defaultCurrency->symbol.''.number_format($price,2);
    		}else{
    			return number_format($price,2).$defaultCurrency->symbol;
    		}
    	}else{
    		$selectedCurrData = DB::table('currencies')
    			->where('status',1)
    			->where('code',$appcurrency)
    			->first();

    		$exchange_rate 	= $selectedCurrData->exchange_rate;
    		$price 			= $price*$exchange_rate;
    		
    		if($isFiltered==1){
    			return $price;
    		}else if($selectedCurrData->symbol_position==0){
    			return $selectedCurrData->symbol.number_format($price,2);
    		}else{
    			return number_format($price,2).$selectedCurrData->symbol;
    		}
    	}
    }
}
if (!function_exists('showFilterPrice')) {
    function showFilterPrice($price){
    	$defaultCurrency= getDefaultCurrency(true);
    	$appcurrency 	= Session::get('appcurrency'); 
    	if($defaultCurrency->code==$appcurrency){
    		return $price;
    	}else{
    		$selectedCurrData = DB::table('currencies')
    			->where('status',1)
    			->where('code',$appcurrency)
    			->first();

    		$exchange_rate 	= $selectedCurrData->exchange_rate;
    		$price 			= $price/$exchange_rate;
    		
    		return $price;   		
    	}
    }
}
if (!function_exists('currentCurrency')) {
    function currentCurrency(){
    	$allCurrencies = getCurrencies();
        if (Session::has('appcurrency') AND array_key_exists(Session::get('appcurrency'), $allCurrencies) && !empty($allCurrencies)) {
            return Session::get('appcurrency');
        }
        return '';
    }
}
if (!function_exists('setCurrency')) {
    function setCurrency($currency){
    	$allCurrencies = getCurrencies();
        if (array_key_exists($currency, $allCurrencies)) {
            Session::put('appcurrency', $currency);
        } else {
            throw new \Exception('not a valid currency');
        }
    }
}

function attributeExists($product_id=''){
	$allRecord = DB::table('products_attributes')
	->where('product_id',$product_id)
	->where('is_variation',1)
	->count();
	return $allRecord;
}
function isProduction(){
	if($_SERVER['HTTP_HOST']!='localhost'){
		return true;
	}else{
		return false;
	}
}
function createCartCookie(){
	$imedcartcookie = Cookie::get('imedcartcookie');
	if(empty($imedcartcookie)){
		$sessionId  = session()->getId();
		Cookie::queue(Cookie::make('imedcartcookie', $sessionId, 43200));
	}
}
function getCurrencies(){
	$currencyArray 	= array();
	$allRecord 		= DB::table('currencies')->where('status',1)->orderby('defaultC','DESC')->get();
	if(!empty($allRecord) && count($allRecord)>0){
		foreach ($allRecord as $singleCurrency) {
			$currencyArray[$singleCurrency->code]=array(
													'defaultC'=>$singleCurrency->defaultC,
													'code'=>$singleCurrency->code,
													'name'=>$singleCurrency->name,
													'symbol'=>$singleCurrency->symbol,
													'symbol_position'=>$singleCurrency->symbol_position,
												);
		}
	}
	return $currencyArray;
}
function getDefaultCurrency($boolean=false){
	$allRecord = DB::table('currencies')->where('status',1)->where('defaultC',1)->first();
	if(empty($allRecord)){
		$allRecord = DB::table('currencies')->where('status',1)->orderby('id','ASC')->first();
	}
	if($boolean)
		return $allRecord;
	else
		return $allRecord->code;
}
function getLanguages(){
	$allRecord = DB::table('languages')->where('status',1)->orderby('defaultL','DESC')->get();
	return $allRecord;
}
function getDefaultLanguages(){
	$allRecord = DB::table('languages')->where('status',1)->where('defaultL',1)->first();
	if(empty($allRecord)){
		$allRecord = DB::table('languages')->where('status',1)->orderby('id','ASC')->first();
	}
	return $allRecord->code;
}
function getMenuPages($location){
	$allRecord = DB::table('pages')
	->where('display_status',1)
	->where('in_menu','like','%' . $location . '%')
	->orderby('display_order')
	->get();
	return $allRecord;
}
function getHeaderLangServices(){
	$userLang   = App::getLocale();
	$returnData = array();
	if($userLang!='en'){
		$allRecords = DB::table('services_translations')
					->where('language',$userLang)
					->get();		
		if(!empty($allRecords)){
			foreach ($allRecords as $singleRecord) {
				$returnData[$singleRecord->services_id]=$singleRecord->name;
			}
		}		
	}
	return $returnData;
}
function getHeaderServices(){
	$allRecord = DB::table('services')->where('display_status',1)->orderby('display_order')->get();
	return $allRecord;
}
function getCartIterms(){
	$currentUser    	= Auth::user();
    $sessionId      	= Cookie::get('imedcartcookie');
    $current_user_id 	= (isset($currentUser->id))?$currentUser->id:'0';
	$whereArray = array(
        'products.display_status'=>1,
        'products.product_type'=>1
    ); 
    if($current_user_id!=0){
    	$whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
	        $query->where('cart_products.user_id','=',$current_user_id)
	            ->orWhere('cart_products.session_id', '=',$sessionId);
	    });
    }else{
    	$whereArray[]=array( function ($query) use ($sessionId,$current_user_id) {
	        $query->where('cart_products.session_id','=',$sessionId);
	    });
    }    

    $allCartItems = DB::table('cart_products')
    ->select('cart_products.*','products.name','products.image','products.slug','products.price','products.special_price','products.stock_availability','products.inventory_management','products.qty as availableQty')
    ->join('products','products.id','=','cart_products.product_id')
    ->where($whereArray)                
    ->orderby('cart_products.id','ASC')
    ->get();
    return $allCartItems;
}
function successMesaage($session=''){
	$errordiv = '';
	if($session!=''){
		$errordiv .= '<div class="msg-component">';
		$errordiv .= '<div class="alert alert-success alert-dismissable">';
		$errordiv .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
		$errordiv .= '<strong>'.$session.'</strong>';
		$errordiv .= '</div>';
		$errordiv .= '</div>';
	}   
   	echo $errordiv;
}
function errorMesaage($session=''){	
	$errordiv = '';
	if($session!=''){
		$errordiv .= '<div class="msg-component">';
		$errordiv .= '<div class="alert alert-danger alert-dismissable">';
		$errordiv .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
		$errordiv .= '<strong>'.$session.'</strong>';
		$errordiv .= '</div>';
		$errordiv .= '</div>';
	}   
   	echo $errordiv;
}
function validationError($errors){
	$errordiv = '';
	if($errors->any()){
		$errordiv .= '<div class="msg-component">';
		$errordiv .= '<div class="alert alert-danger alert-dismissable">';
		$errordiv .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
		$errordiv .= '<ul>';
		$all_error = $errors->all();
		foreach ($all_error as $key => $value) {
			$errordiv .= '<li>'.$value.'</li>';
		}
		$errordiv .= '</ul>';
		$errordiv .= '</div>';		
		$errordiv .= '</div>';		
	}
	echo $errordiv;
}
function displayTimeMD($date){
	$current_user_data = Auth::user();
	$timezoneval = (isset($current_user_data->timezoneval))?$current_user_data->timezoneval:'';
	return date('h:i A',(strtotime('+'.$timezoneval.' minutes',strtotime($date))));
}
function displayDateMD($date){
	/*$current_user_data = Auth::user();
	$timezoneval = (isset($current_user_data->timezoneval))?$current_user_data->timezoneval:'';
	return date('M d, Y',(strtotime('+'.$timezoneval.' minutes',strtotime($date))));*/
	return date('M d, Y',strtotime($date));
}
function displayDatedMY($date){
	return date('d M Y',strtotime($date));
}
function displayDatemdy($date){
	return date('m/d/Y',strtotime($date));
}
function displayDate($date){
	return date('Y-m-d',strtotime($date));
}
function displayDateWithTimeMD($date){
	return date('M d, Y h:i A',strtotime($date));
}
function displayDateWithTime($date){
	return date('Y-m-d H:i',strtotime($date));
}
function currentDBDate(){
	return date('Y-m-d H:i:s');
}
function currentYMDDate(){
	return date('Y-m-d');
}
function viewFileURL($url){
	if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
       	return url('/').'/'.$url;
    }else{
    	return $url;
    }
}
function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}
function randomFilename($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return time().'-'.$randomString;
}
function randomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function randomOTP($length = 6) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function userUnReanNotification($userId,$catId){
	return 0;
}
function fetchStates(){
	$all_states = DB::table('states')->select('id','state')->where('display_status','1')->orderby('state')->get();
	return $all_states;
}
function fetchCities($state_id=''){
	$all_cities = array();
	if(!empty($state_id)){
		$all_cities = DB::table('cities')->select('id','city')->where(array('display_status'=>'1','state_id'=>$state_id))->orderby('city')->get();
	}
	return $all_cities;
}
function generalSettings(){
	$settings = DB::table('settings')->get()->first();
	return $settings;
}
if( ! function_exists('send_smtp_email') ){
	function send_smtp_email($to='',$subject='',$messagebody='',$template='',$header=array(),$options=array()){			
        $settings 						= DB::table('settings')->get()->first();
        $messagebody['domain'] 			= env('APP_URL');
        $messagebody['wl_logo'] 		= env('APP_LOGO');
        $messagebody['productName'] 	= $settings->product_name;
        $messagebody['mailSignatureName'] = $settings->mail_signature_name;
        
        $from_name 	= (isset($options['from_name']) && $options['from_name']!='')?$options['from_name']:$settings->mail_sender_name;
		$from_email 	= (isset($options['from_email']) && $options['from_email']!='')?$options['from_email']:$settings->from_email;
		
		if($settings->current_mail_provider==1){
			$host 		= $settings->amazon_host;		
			$username 	= $settings->amazon_username;
			$password 	= $settings->amazon_password;
			$encryption = 'ssl';
			$port 		= '465';
			$driver 	= 'smtp';
		}else{
			$host 		= '';		
			$username 	= '';
			$password 	= '';
		}
		
		$ccemail 	= (isset($header['ccemail']) && $header['ccemail']!='')?$header['ccemail']:'';
		$bccemail 	= (isset($header['bccemail']) && $header['bccemail']!='')?$header['bccemail']:'';
		if(!empty($host)){
			Config::set('mail.driver', $driver);
	        Config::set('mail.host', $host); 
	        Config::set('mail.port', $port); 
	        Config::set('mail.encryption', $encryption); 
	        Config::set('mail.username', $username); 
	        Config::set('mail.password', $password); 
		}
		if($_SERVER['HTTP_HOST']!='localhost'){
			return @Mail::send($template, $messagebody, function ($message) use ($to, $subject, $from_email,$from_name,$ccemail,$bccemail) 
	        {	
	            $message->from($from_email, $from_name);
	            $message->subject($subject);
	            $message->to($to);           

	            if($ccemail!='')
	            	$message->cc($ccemail);

	            if($bccemail!='')
					$message->bcc($bccemail);

				$message->replyTo($from_email, $from_name);
	        });
		}       
	}
}
if( ! function_exists('encryptsalt') ){
	function encryptsalt($string='') {
		$key=env('ENCRYPTSALTKEY','sumitChattha');
		$result = '';
		if($string!=''){
			for($i=0; $i<strlen($string); $i++) {
				$char 		= substr($string, $i, 1);
				$keychar 	= substr($key, ($i % strlen($key))-1, 1);
				$char 		= chr(ord($char)+ord($keychar));
				$result 	.= $char;
			}
			return base64_encode($result);
		}else{
			return $result;
		}	
	}
}
if( ! function_exists('decryptsalt') ){
	function decryptsalt($string='') {
		$key=env('ENCRYPTSALTKEY','sumitChattha');
		$result 	= '';
		if($string!=''){
			$string 	= base64_decode($string);
			for($i=0; $i<strlen($string); $i++) {
				$char 		= substr($string, $i, 1);
				$keychar 	= substr($key, ($i % strlen($key))-1, 1);
				$char 		= chr(ord($char)-ord($keychar));
				$result 	.= $char;
			}
		}
		return $result;	
	}
}
if (! function_exists('wordsLimit')) {
    /**
     * Limit the number of words in a string.
     *
     * @param  string  $value
     * @param  int     $words
     * @param  string  $end
     * @return string
     */
    function wordsLimit($value, $words = 100, $end = '...')
    {
        return \Illuminate\Support\Str::words($value, $words, $end);
    }
}
function fetchSingleColumn($table,$select,$where){
	$settings = DB::table($table)->select($select)->where($where)->get()->first();
	return $settings->$select;
}
function fetchSingleRow($table,$select='*',$where){
	$settings = DB::table($table)->select($select)->where($where)->get()->first();
	return $settings;
}
function displayUserPic($imagepath='',$name='', $type=1, $class='' ){
	if(!empty($imagepath)){
		return '<img src="'.url($imagepath).'" alt="'.$name.'" class="'.$class.'"/>';
	}else if($type==1){
		return '<img src="'.url('/frontend/images/blank-profile-picture.png').'" alt="'.$name.'" class="'.$class.'"/>';
	}else if($type==0){
		return '<img src="'.url('/frontend/images/blank-profile-picture.png').'" alt="'.$name.'" class="'.$class.'"/>';
	}	
}
function getLanguageReconds($table,$select='*',$where){
	$userLang   = App::getLocale();
	if($userLang!='en'){
		$record 	= DB::table($table)
					->select($select)
					->where($where)
					->where('language',$userLang)
					->first();
		return $record;
	}else{
		return null;
	}	
}
function getReconds($table,$select='*',$where,$orderby,$order,$limit=4,$offset=0){
	$recentjobs = DB::table($table)->select($select)->where($where)->orderBy($orderby,$order)->skip($offset)->take($limit)->get();
	return $recentjobs;
}
function getYears($date){
	$currentDate = date('Y-m-d');
	$d1 = new DateTime($currentDate);
	$d2 = new DateTime($date);
	$diff = $d2->diff($d1);
	return $diff->y;
}
function allAllowedSections(){
	$allow = array(
		'1'=>'Dashboard Data',
		'2'=>'General Settings',
		//3'=>'How It Work',
		'4'=>'View Sliders',
		'5'=>'Add New Slider',
		'6'=>'Edit Slider',
		'7'=>'Delete Slider',

		'53'=>'View Products',
		'54'=>'Add New Product',
		'55'=>'Edit Product',
		//'56'=>'Delete Product',
		'25'=>'View Categories',
		'26'=>'Add New Category',
		'27'=>'Edit Category',
		'28'=>'Delete Category',

		'57'=>'View Manufacturers',
		'58'=>'Add New Manufacturer',
		'59'=>'Edit Manufacturer',
		'60'=>'Delete Manufacturer',

		'61'=>'View Attribute Sets',
		'62'=>'Add New Attribute Set',
		'63'=>'Edit Attribute Set',
		'64'=>'Delete Attribute Set',

		'65'=>'View Attributes',
		'66'=>'Add New Attribute',
		'67'=>'Edit Attribute',
		'68'=>'Delete Attribute',

		'69'=>'View Coupons',
		'70'=>'Add New Coupon',
		'71'=>'Edit Coupon',
		'72'=>'Delete Coupon',

		'92'=>'Manage Orders',
		'93'=>'View Order Details',
		'38'=>'Manage Reports',

		'12'=>'View Advertisements',
		'13'=>'Add New Advertisements',
		'14'=>'Edit Advertisements',
		'15'=>'Delete Advertisements',

		'20'=>'View Newsletter',
		'21'=>'Edit Newsletter',
		'22'=>'Delete Newsletter',	

		'23'=>'View Pages',
		'51'=>'Add Pages',
		'24'=>'Edit Pages',
		'52'=>'Delete Pages',

		'29'=>'View Users',
		'30'=>'Edit Users',

		'73'=>'View Services',
		'74'=>'Add New Service',
		'75'=>'Edit Service',
		'76'=>'Delete Service',

		'77'=>'View Projects',
		'78'=>'Add New Project',
		'79'=>'Edit Project',
		'80'=>'Delete Project',		 

		'34'=>'View Admin Users',
		'35'=>'Add Admin User',
		'36'=>'Edit Admin User',
		'37'=>'Delete Admin Users',

		'48'=>'Manage Media Images',
		'49'=>'Add Media Image',
		'50'=>'Delete Media Image',

		'81'=>'View Languages',
		'82'=>'Add New Language',
		'83'=>'Edit Language',
		'84'=>'Delete Language',

		'85'=>'View Languages Content',
		'86'=>'Add New Language Content',
		'87'=>'Delete Language Content',

		'88'=>'View Currencies',
		'89'=>'Add New Currency',
		'90'=>'Edit Currency',
		'91'=>'Delete Currency',

		'94'=>'View Services Request',
		//'95'=>'Add New Services Request',
		'96'=>'View Single Services Request',
		'97'=>'Delete Services Request',
		'98'=>'Copy Products',

		'99'=>'View Service Category',
		'100'=>'Add New Service Category',
		'101'=>'Edit Service Category',
		'102'=>'Delete Service Category',



		//'8'=>'View Blogs',
		//'9'=>'Add New Blogs',
		//'10'=>'Edit Blogs',
		//'11'=>'Delete Blogs',
		//'16'=>'View FAQ',
		//'17'=>'Add New FAQ',
		//'18'=>'Edit FAQ',
		//'19'=>'Delete FAQ',
		//'31'=>'View Jobs',
		//'32'=>'Edit Jobs',
		//'33'=>'View Payments',
		//'39'=>'View Mailing Lists',
		//'40'=>'Add Mailing Lists',
		//'41'=>'Edit Mailing Lists',
		//'42'=>'Delete Mailing Lists',		
		//'43'=>'View Mailing Lists Records',
		//'44'=>'Add Mailing Lists Records',
		//'45'=>'Edit Mailing Lists Records',
		//'46'=>'Delete Mailing Lists Records',
		//'47'=>'Send Bulk Emails',


	);
	return $allow;
}
function isAllowed($user_id,$allowed_sections='',$current_section){
	$isAllowed = false;
	if(!empty($allowed_sections)){
		$allowed_array = explode(',', $allowed_sections);
		if(in_array($current_section, $allowed_array)){
			$isAllowed = true;
		}
	}
	if($user_id==1){
		$isAllowed = true;
	}
	return $isAllowed;
}
function updateNestedObject($stripeObj, $actionType, $value) {
  $arrayOfNestObjectKeys = explode('.', $actionType);
  return updateChild($stripeObj, $arrayOfNestObjectKeys, $value);
}
function updateChild ($root, $listOfKeys, $value) {
  if (count($listOfKeys) == 1) {
    $root[$listOfKeys[0]] = $value;
    return $root;
  } else {
    $child = $root[$listOfKeys[0]];
    $remainingKeys = array_splice($listOfKeys, 1);
    $root[$listOfKeys[0]] = updateChild ($child, $remainingKeys, $value);
    return $root;
  }
}
function makeLinks($string){
	$reg_exUrl = "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/";
	// Check if there is a url in the text
	if(preg_match($reg_exUrl, $string, $url)) {		
		if(strpos( $url[0], ":" ) === false){
			$link = 'http://'.$url[0];
		}else{
			$link = $url[0];
		}			
	   	// make the urls hyper links
	   	$string = preg_replace($reg_exUrl, '<a href="'.$link.'" title="'.$url[0].'" target="_blank">'.$url[0].'</a>', $string);	
	}	
	return $string;
}
function pre($array){
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}
function getLangCategories(){
	$userLang   = App::getLocale();
	$returnData = array();
	if($userLang!='en'){
		$allCategory 	= DB::table('categories_translations')
					->where('language',$userLang)
					->get();		
		if(!empty($allCategory)){
			foreach ($allCategory as $singleCategory) {
				$returnData[$singleCategory->category_id]=$singleCategory->name;
			}
		}		
	}
	return $returnData;
}
function getCategories(){
	return  $allRecords = Category::where(['parent_id'=>0,'display_status'=>1])
        ->orderby('name','ASC')
        ->get();
}
function getPageBySlug($table,$column,$slug){
	return DB::table($table)->select('*')->where([$column=>$slug])->first();
}
function checkSlug($page_slug,$id='',$table,$column){
	$where_array = array($column=>$page_slug);
	if($id!=''){
		$where_array[]=array('id','<>',$id);
    }
	return DB::table($table)->select('*')->where($where_array)->first();
}
if (!function_exists('create_slug')){
	function create_slug($string,$id,$table,$column){
		$string 	= trim($string);
        $string 	= str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $get_title 	= preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		$res 		= strtolower($get_title);
        $data 		= checkSlug($res,$id,$table,$column);
		if(!empty($data)){
			$have='1';
		}else{
			$have='0';
		}
		$x=1;
		while($have>0){
			$res2=$res.$x;
			$data = checkSlug($res2,$id,$table,$column);
			if(!empty($data)){
				$have='1';
			}else{
				$have='0';
				$res=$res2;
			}
			$x++;
		}
		return $res;
    }
}

if (!function_exists('serviceCitiesByState')) {
	function serviceCitiesByState($state_id){
		$citylist = DB::select('SELECT id, cityname, slug FROM serviceareas WHERE state = '.$state_id.' AND display_status = 1 ORDER BY cityname ASC');
        return $citylist;
	}
}