<?php

namespace App\Http\Middleware;

use Closure;
use App;
class Currency
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!currentCurrency()) {
            setCurrency(getDefaultCurrency());
        }
        return $next($request);
    }
}
