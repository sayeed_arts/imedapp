<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{   
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   
    protected $fillable = [
        'id', 'slug', 'name', 'content', 'requirements', 'experience', 'closing_date', 'category_id', 'location_id', 'type_id', 'display_status','display_order', 'meta_title', 'meta_key', 'meta_desc', 'created_at','updated_at'
    ];
    protected $primaryKey = 'id';

    protected $keyType = 'string';

}
