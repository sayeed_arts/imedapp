<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingCategory extends Model
{   
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'trainingcategories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   
    protected $fillable = [
        'id', 'slug', 'name', 'content', 'display_status','display_order', 'meta_title', 'meta_key', 'meta_desc', 'created_at','updated_at'
    ];
    protected $primaryKey = 'id';

    protected $keyType = 'string';

}
