
ALTER TABLE `auctions` CHANGE `start_bid_price` `start_bid_price` FLOAT(10,2) NOT NULL, CHANGE `min_bid_price` `min_bid_price` FLOAT(10,2) NOT NULL; 

ALTER TABLE `auctions` ADD `featured` INT NOT NULL AFTER `terms_and_conditions`; 

-- --------------------------------------------------------

--
-- Table structure for table `auction_bids`
--

DROP TABLE IF EXISTS `auction_bids`;
CREATE TABLE IF NOT EXISTS `auction_bids` (
  `id` int NOT NULL AUTO_INCREMENT,
  `auction_id` int NOT NULL,
  `user_id` int NOT NULL,
  `bid_amount` float(10,2) NOT NULL,
  `status` int NOT NULL COMMENT '1: winning bid',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_saved_bid_later`
--

DROP TABLE IF EXISTS `user_saved_bid_later`;
CREATE TABLE IF NOT EXISTS `user_saved_bid_later` (
  `id` int NOT NULL AUTO_INCREMENT,
  `auction_id` int NOT NULL,
  `user_id` int NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
COMMIT;
