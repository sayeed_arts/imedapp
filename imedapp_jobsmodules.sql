--
-- Table structure for table `jobapplications`
--

DROP TABLE IF EXISTS `jobapplications`;
CREATE TABLE IF NOT EXISTS `jobapplications` (
  `id` int NOT NULL AUTO_INCREMENT,
  `job_id` int DEFAULT NULL,
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `coverletter` text NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `preferred_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `resume` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jobapplications`
--

INSERT INTO `jobapplications` (`id`, `job_id`, `fullname`, `email`, `phone`, `coverletter`, `current_location`, `preferred_location`, `resume`, `created_at`, `updated_at`) VALUES
(2, 1, 'Adam James1', 'adamjames@test.com', '9876543210', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Noida', 'New Delhi', 'https://imedicalshop.s3.amazonaws.com/uploaded_images/1609534932-LVyDTxud67iUIHK--ritter-222-barrier-free-power-examination-table-15_zpsgds2e5vz_0afccb77-477c-47bf-b559-458704ccb43e.jpeg', '2021-07-11 11:57:59', '2021-07-11 11:57:59');

-- --------------------------------------------------------

--
-- Table structure for table `jobcategories`
--

DROP TABLE IF EXISTS `jobcategories`;
CREATE TABLE IF NOT EXISTS `jobcategories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `display_status` int DEFAULT '1',
  `display_order` float(10,2) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_key` text,
  `meta_desc` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jobcategories`
--

INSERT INTO `jobcategories` (`id`, `slug`, `name`, `content`, `display_status`, `display_order`, `meta_title`, `meta_key`, `meta_desc`, `created_at`, `updated_at`) VALUES
(1, 'job-category-1-a', 'Job Category 1 A', '<p>Desc 1A</p>', 1, 1.00, 'Job Category 1 Meta TitleA', 'Job Category 1 Meta KeywordsA', 'Job Category 1 Meta DescriptionA', '2021-07-10 09:35:40', '2021-07-10 09:42:38'),
(3, 'sdfsdfsd', 'sdfsdfsd', '<p>sdfsdf</p>', 1, 2.00, 'sdfsd', 'fsdf', 'sdf', '2021-07-10 13:38:36', '2021-07-10 13:38:36');

-- --------------------------------------------------------

--
-- Table structure for table `jobcategories_translations`
--

DROP TABLE IF EXISTS `jobcategories_translations`;
CREATE TABLE IF NOT EXISTS `jobcategories_translations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_id` int DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jobcategories_translations`
--

INSERT INTO `jobcategories_translations` (`id`, `category_id`, `language`, `name`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 'fr', 'thtghyt', '<p>utyu</p>', '2021-07-10 09:42:38', '2021-07-10 09:42:38');

-- --------------------------------------------------------

--
-- Table structure for table `joblocations`
--

DROP TABLE IF EXISTS `joblocations`;
CREATE TABLE IF NOT EXISTS `joblocations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `display_status` int DEFAULT '1',
  `display_order` float(10,2) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_key` text,
  `meta_desc` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `joblocations`
--

INSERT INTO `joblocations` (`id`, `slug`, `name`, `content`, `display_status`, `display_order`, `meta_title`, `meta_key`, `meta_desc`, `created_at`, `updated_at`) VALUES
(1, 'location-1-a', 'Location 1 A', '<p>Location 1 A</p>', 0, 2.00, 'Location 1 A', 'Location 1 A', 'Location 1 A', '2021-07-10 12:54:51', '2021-07-10 12:56:08'),
(3, 'location-2-a', 'Location 2 A', '<p>Location 2 A</p>', 0, 2.00, 'Location 1 A', 'Location 1 A', 'Location 1 A', '2021-07-10 12:54:51', '2021-07-10 12:56:08');

-- --------------------------------------------------------

--
-- Table structure for table `joblocations_translations`
--

DROP TABLE IF EXISTS `joblocations_translations`;
CREATE TABLE IF NOT EXISTS `joblocations_translations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `location_id` int DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `requirements` text NOT NULL,
  `experience` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `closing_date` timestamp NOT NULL,
  `category_id` text NOT NULL,
  `location_id` text NOT NULL,
  `type_id` text NOT NULL,
  `display_status` int DEFAULT '1',
  `display_order` float(10,2) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_key` text,
  `meta_desc` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `slug`, `name`, `content`, `requirements`, `experience`, `email`, `closing_date`, `category_id`, `location_id`, `type_id`, `display_status`, `display_order`, `meta_title`, `meta_key`, `meta_desc`, `created_at`, `updated_at`) VALUES
(1, 'sales-manager', 'Sales Manager', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', '6-8 Years', 'sdfsdf@sdfdsfs.com', '2021-07-30 18:30:00', '1', '2', '1', 1, 1.00, NULL, NULL, NULL, '2021-07-11 06:35:44', '2021-07-11 08:11:14'),
(5, 'area-manager', 'Area Manager', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', '6-8 Years', 'sdfsdf@sdfdsfs.com', '2021-07-30 18:30:00', '1,3', '1', '2', 1, 1.00, NULL, NULL, NULL, '2021-07-11 06:35:44', '2021-07-11 08:11:26');

-- --------------------------------------------------------

--
-- Table structure for table `jobs_translations`
--

DROP TABLE IF EXISTS `jobs_translations`;
CREATE TABLE IF NOT EXISTS `jobs_translations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `job_id` int DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jobtypes`
--

DROP TABLE IF EXISTS `jobtypes`;
CREATE TABLE IF NOT EXISTS `jobtypes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `display_status` int DEFAULT '1',
  `display_order` float(10,2) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_key` text,
  `meta_desc` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jobtypes`
--

INSERT INTO `jobtypes` (`id`, `slug`, `name`, `content`, `display_status`, `display_order`, `meta_title`, `meta_key`, `meta_desc`, `created_at`, `updated_at`) VALUES
(1, 'job-type-1a', 'Job Type 1A', '<p>Job Type 1A</p>', 0, 2.00, 'Job Type 1A', 'Job Type 1A', 'Job Type 1A', '2021-07-10 13:08:32', '2021-07-10 13:09:41'),
(2, 'job-type-2a', 'Job Type 2A', '<p>Job Type 2A</p>', 0, 2.00, 'Job Type 1A', 'Job Type 1A', 'Job Type 1A', '2021-07-10 13:08:32', '2021-07-10 13:09:41');

-- --------------------------------------------------------

--
-- Table structure for table `jobtypes_translations`
--

DROP TABLE IF EXISTS `jobtypes_translations`;
CREATE TABLE IF NOT EXISTS `jobtypes_translations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type_id` int DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jobtypes_translations`
--

INSERT INTO `jobtypes_translations` (`id`, `type_id`, `language`, `name`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 'fr', 'gddfgdf', '<p>fgdfgdg</p>', '2021-07-10 13:09:41', '2021-07-10 13:09:41');
COMMIT;
