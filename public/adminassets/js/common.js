$(".delete-form").on('submit',function(e) {
	if(!confirm('Are you sure, you want to delete this record?')){
		return false;
	}
})
$(document).on('click','.delete-product-image',function(){
	$(this).parent().remove();
});
$(document).on('change','#order_shipping_status',function(){
	if(confirm('Are you sure, you want to update the shipping status?')){
		$('#shippingform').submit();
	}else{
		return false;
	}
});
$(document).on('click','.add-more-attribute', function(){
	var display = $(this).attr('data-displayed');
	if($('#display_attr__'+display).length>0){
		$('#display_attr__'+display).show();
		var displayNew = parseInt(display)+1;
		if($('#display_attr__'+displayNew).length>0){
			$(this).attr('data-displayed',displayNew);
		}else{
			$('.add-more-attribute').hide();
		}		
	}
});


$(document).on('change','#export_type', function(){
	var currval = $(this).val();
	if(currval==2 || currval==3){
		$('#orderstatusdiv').show();
	}else{
		$('#orderstatusdiv').hide();
	}
});

$(document).on('click','.variation_attr', function(){
	var dataId = $(this).attr('data-id');
	if($(this).is(":checked")){
		$('#attrvalforprice_'+dataId).show();
		$('#attrvalforprice_'+dataId).prop('required',true);

	}else{
		$('#attrvalforprice_'+dataId).prop('required',false);
		$('#attrvalforprice_'+dataId).val('');
		$('#attrvalforprice_'+dataId).hide();
	}
});
$(document).on('change','#variation_attributes', function(){
	var currentValu=$(this).val();
	var displayHtmlC='';
	if(currentValu!=''){
		var attrvalsv = $('#singleattributeval__'+currentValu).html();
		var res3 	= attrvalsv.split("##~~##");		
		$.each(res3, function(k){
			if(res3[k]!=''){
				var value 	= $.trim(res3[k]);
				var res4 	= value.split("==");
				var idval 	= $.trim(res4[0]);
				var valval 	= $.trim(res4[1]);
				displayHtmlC = displayHtmlC+'<input type="hidden" name="hiddenIds[]" value="'+idval+'"><input type="checkbox" name="selectedvarattrval[]" id="attrvarval_'+currentValu+'_'+idval+'" data-id="'+currentValu+'_'+idval+'" value="'+idval+'" class="variation_attr"> <label for="attrvarval_'+currentValu+'_'+idval+'">'+valval+'</label> <input type="number" steps="0.01" name="selectedattrvalprice[]" class="form-control" id="attrvalforprice_'+currentValu+'_'+idval+'" style="display:none"> <br>';
			}      	
	   	});
	}	
	$('.variation_attribute_value').html(displayHtmlC);
});

$(document).on('change','.selectattributes', function(){
	var currentVal=$(this).val();
	var displayHtml='';
	if(currentVal!=''){
		var attrvals = $('#singleattributeval__'+currentVal).html();
		var res = attrvals.split("##~~##");
		
		$.each(res, function(k){
			if($.trim(res[k])!=''){
				var value 	= $.trim(res[k]);
				var res4 	= value.split("==");
				var idval 	= $.trim(res4[0]);
				var valval 	= $.trim(res4[1]);
				displayHtml = displayHtml+'<input type="checkbox" name="selectedattrval[]" id="attrval_'+currentVal+'_'+idval+'" value="'+currentVal+'__'+idval+'" class=""> <label for="attrval_'+currentVal+'_'+idval+'">'+valval+'</label><br>';
			}      	
	   	});
	}
	
	$(this).closest('div.row').find('.attribute_value').html(displayHtml);
});
/*$(document).on('change','.selectattributes', function(){
	var currSec 	= $(this).attr('data-attribute-id');
	var currentVal 	= $(this).val();
	var attrvals 	= $('#attributeval__'+currentVal).html();
	var res 		= attrvals.split("##~~##");
	var displayHtml = '';
	$.each(res, function(k){
		if($.trim(res[k])!=''){
			displayHtml = displayHtml+'<option value="'+res[k]+'">'+res[k]+'</option>';
		}
   	});
   	console.log(displayHtml);
	$('#attributesValues_'+currSec).html(displayHtml);
});*/

$(document).on('click','.add_more_bullets_button', function(){
	//if($('.bullet_points').length<10){
    	$('.add_more_bullets').append('<input type="text" name="values[]" class="form-control bullet_points" placeholder="Value" value="">');
    //}
    /*if($('.bullet_points').length==10){
    	$('.add_more_bullets_button').hide();
    }*/
});
$(document).on('click','.add_more_bullets_button_lang', function(){
	var lang = $(this).attr('data-lang');
	$('.add_more_bullets_lang_'+lang).append('<input type="text" name="values_'+lang+'[]" class="form-control bullet_points" placeholder="Value" value="">');
});
$(document).on('change','#shipping_type',function(){
	var shipping_type = $(this).val();
	if(shipping_type=='1'){		
		$('#flat_rate_fee').prop('required',true);
	}else{
		$('#flat_rate_fee').prop('required',false);
	}	
});
$(document).on('change','#inventory_management',function(){
	var inventory_management = $(this).val();
	if(inventory_management=='1'){
		$('#trackinventory').show();
		$('#qty').prop('required',true);
		$('#stock_availability').prop('required',true);
	}else{
		$('#trackinventory').hide();		
		$('#qty').prop('required',false);
		$('#stock_availability').prop('required',false);
	}
});
$(document).on('change','#category_id',function(){
	var categoryId = $(this).val();
	$.ajax({ 
		type: 'GET',
		url: base_url+'/admin/products/getsubcat/'+categoryId,
		data: {}, 						
		success: function(data){
			if(data!=''){
				$('#child_category_id').html(data);
			}
		} 
	});
});

$(document).on('change','#from_category',function(){
	var categoryId = $(this).val();
	$.ajax({ 
		type: 'GET',
		url: base_url+'/admin/products/getproducts/'+categoryId,
		data: {}, 						
		success: function(data){
			if(data!=''){
				$('#div_products').css("display", "block");
				$('#ajax_products').html(data);
			}
		} 
	});
});

$(document).on('change','#to_category',function(){
	var categoryId = $(this).val();
	if(categoryId){
		let checked = $("input[type=checkbox]:checked").length;
		if(checked) {
			$('#act1').prop('disabled', false);
			$('#act2').prop('disabled', false);
		} else{
			$('#act1').prop('disabled', true);
			$('#act2').prop('disabled', true);
		}
	}
});

$(document).on('change','#chkallboxs',function(){
	var chkallboxs = $('#chkallboxs').is(":checked");
	if(chkallboxs){
		$('.chkboxs').prop('checked', true);
	} else{
		$('.chkboxs').prop('checked', false);
	}
});

$(document).on('click','.chkboxs',function(){
	let checked = $("input[type=checkbox]:checked").length;
	if(checked) {
		$('#act1').prop('disabled', false);
		$('#act2').prop('disabled', false);
	} else{
		$('#act1').prop('disabled', true);
		$('#act2').prop('disabled', true);
	}
});

$(document).on('change','#country_yo',function(){
	var countryid = $(this).val();
	$.ajax({ 
		type: 'GET',
		url: base_url+'/getstates/'+countryid,
		data: {}, 						
		success: function(data){
			if(data!=''){
				$("#state_yo").empty().append(data);
			}
		} 
	});
});


$(document).on('change','.fetch_cities',function(){
	var state = $(this).val();
	$.ajax({ 
		type: 'GET',
		url: base_url+'/getcities/'+state,
		data: {}, 						
		success: function(data){
			if(data!=''){
				$('.fetch_zipcode').html(data);
				$('.zipcodesec').html('<option value="">Zipcode</option>');
			}
		} 
	});
});
$(document).on('change','.fetch_zipcode',function(){
	var city = $(this).val();
	$.ajax({ 
		type: 'GET',
		url: base_url+'/getzipcode/'+city,
		data: {}, 						
		success: function(data){
			if(data!=''){
				$('.zipcodesec').html(data);
			}else{
				$('.zipcodesec').html('<option value="">Zipcode</option>');
			}
		} 
	});
});
function addditepicker(){
	$('.selecteddate').datepicker({
        format: 'mm/dd/yyyy',
        //startDate: '+1d'
    });
}
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$('.translate').editable({
    params: function(params) {
        params.code = $(this).editable().data('code');
        return params;
    }
});


$('.translate-key').editable({
    validate: function(value) {
        if($.trim(value) == '') {
            return 'Key is required';
        }
    }
});


$('body').on('click', '.remove-key', function(){
    var cObj = $(this);
    if (confirm("Are you sure want to remove this item?")) {
        $.ajax({
            url: cObj.data('action'),
            method: 'DELETE',
            success: function(data) {
                cObj.parents("tr").remove();
                alert("Your imaginary file has been deleted.");
            }
        });
    }
});

$('.deletevendor').on('click',function(){
    var vendor_id = $(this).attr('vendor_id');

    if (confirm("Are you sure want to remove this vendor?")) {
        $.ajax({
            url: base_url+'/admin/vendors/deletevendor',
            data: {vendor_id:vendor_id},
            method: 'POST',
            success: function(data) {
                alert("Vendor deleted successfully.");
                location.reload();
            }
        });
    }
})


$(".addmore_auction").on('click',function(e){

count = 0;
       //  $('.addrw').each(function(){
       //      if($(this).val() == '' ){
       //          $(this).css('border-color','red');
       //          count++;
       //      }
       //      else{
       //          $(this).css('color','');
       //      }
       //  })

       // var startval = parseInt($('.discountdiv').find('.limittoo').last().val()) + 1;

       

if(count == '0'){
        var html = ' <div><div class="form-group"><label class="col-md-3 control-label" for="name">Question</label><div class="col-md-8"><input class="form-control" name="faq_ques[]" id="faq_ques" required></div></div><div class="form-group"><label class="col-md-3 control-label" for="name">Answer</label><div class="row"><div class="col-md-7"><textarea name="faq_ans[]" id="faq_ans" class="form-control" rows="3" required></textarea></div><div class="col-md-1"><button style="margin-top:40px;" class="btn remove_lm btn-danger">Remove</button></div></div></div></div>';
		$('.faq_list').append(html);

    
}
        return false;

        
    });


$(document.body).delegate('.remove_lm','click',function(){
    $(this).parent().parent().parent().parent().remove();
});

$(document).on('change','#category_id_auc',function(){
	var state = $(this).val();
	$.ajax({ 
		type: 'GET',
		url: base_url+'/admin/auctions/getproduct/'+state,
		data: {}, 						
		success: function(data){
			console.log(data);
			if(data!=''){
				$('#product_id_auc').html(data);
			}
		} 
	});
});

$('.deleteauction').on('click',function(){
    var vendor_id = $(this).attr('vendor_id');

    if (confirm("Are you sure want to remove this auction?")) {
        $.ajax({
            url: base_url+'/admin/auctions/deleteauctions',
            data: {vendor_id:vendor_id},
            method: 'POST',
            success: function(data) {
                alert("Auction deleted successfully.");
                location.reload();
            }
        });
    }
})
$(function () {
                $('.datetimepicker5').datetimepicker({
                    format: 'HH:mm'
                });
            });



$('.chnagestatus').on('click',function(){
    var status = $(this).attr('attr_value');
    var auction_id = $(this).attr('attr_auction_id');

    if (confirm("Are you sure want to change status of this auction?")) {
        $.ajax({
            url: base_url+'/admin/auctions/updatestatus',
            data: {status:status,auction_id:auction_id},
            method: 'POST',
            success: function(data) {
                alert("Auction status updated successfully.");
                location.reload();
            }
        });
    }
});


$('.chnagestatusprodvendor').on('click',function(){
    var status = $(this).attr('attr_value');
    var auction_id = $(this).attr('attr_auction_id');

    if (confirm("Are you sure want to change status of this rating?")) {
        $.ajax({
            url: base_url+'/vendor/updateprodreviewstatus',
            data: {status:status,auction_id:auction_id},
            method: 'POST',
            success: function(data) {
                alert("Rating status updated successfully.");
                location.reload();
            }
        });
    }
});

$('.chnagestatusvenvendor').on('click',function(){
    var status = $(this).attr('attr_value');
    var auction_id = $(this).attr('attr_auction_id');

    if (confirm("Are you sure want to change status of this rating?")) {
        $.ajax({
            url: base_url+'/vendor/updatevenreviewstatus',
            data: {status:status,auction_id:auction_id},
            method: 'POST',
            success: function(data) {
                alert("Rating status updated successfully.");
                location.reload();
            }
        });
    }
});



$('.chnagestatusprod').on('click',function(){
    var status = $(this).attr('attr_value');
    var auction_id = $(this).attr('attr_auction_id');

    if (confirm("Are you sure want to change status of this rating?")) {
        $.ajax({
            url: base_url+'/admin/updateprodreviewstatus',
            data: {status:status,auction_id:auction_id},
            method: 'POST',
            success: function(data) {
                alert("Rating status updated successfully.");
                location.reload();
            }
        });
    }
});

$('.chnagestatusven').on('click',function(){
    var status = $(this).attr('attr_value');
    var auction_id = $(this).attr('attr_auction_id');

    if (confirm("Are you sure want to change status of this rating?")) {
        $.ajax({
            url: base_url+'/admin/updatevenreviewstatus',
            data: {status:status,auction_id:auction_id},
            method: 'POST',
            success: function(data) {
                alert("Rating status updated successfully.");
                location.reload();
            }
        });
    }
});


$(document).ready(function(){
	if (window.location.href.indexOf("viewvendor") > -1) {
		$("#addvendoryo :input").prop("disabled", true);
		$('.btn').remove();
	}
});


$(document).ready(function(){
	if (window.location.href.indexOf("viewauction") > -1) {
		$("#acut_form :input").prop("disabled", true);
		$('.btn').remove();
		$('.faq_list').remove();
	var datevalue = $("#js_time_date").val();
var countDownDate = new Date(datevalue).getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("countdown").innerHTML = 'Time Remaing for auction: '+days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("countdown").innerHTML = "EXPIRED";
  }
}, 1000);
	}



	if (window.location.href.indexOf("auctions/add") > -1) {
	var datevalue = $("#js_time_date").val();
	var countDownDate = new Date(datevalue).getTime();

	// Update the count down every 1 second
	var x = setInterval(function() {

	  // Get today's date and time
	  var now = new Date().getTime();
    
	  // Find the distance between now and the count down date
	  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
 if(datevalue.trim() != ""){
 // Output the result in an element with id="demo"
  document.getElementById("countdown").innerHTML = 'Time Remaing for auction: '+days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("countdown").innerHTML = "EXPIRED";
  }	
 }
  
}, 1000);
	}

})

$(".setcommission").on('click',function(){
	var vendorid = $(this).attr('vendor_id');
	var commission = $(this).attr('vendorcom');
//alert(vendorid);
	$("#commission").val(commission);
	$("#vendorid").val(vendorid);

})


$(document).ready(function() {
    setInterval(function(){ 
        var objs = document.getElementsByClassName('countdowntimer');
        for(var i=0; i<objs.length; i++){
            var auc_id = objs[i].id;
            auc_id = auc_id.replace('auc_','');
            $.ajax({
            type: 'GET',
            url: base_url + '/auctiontimer/' + auc_id,
            data: {},
            success: function (data) {
                if (data != '') {
                    document.getElementById('auc_'+data.id).innerHTML = data.time_remain;
                }
            }
        });
        } 
    }, 1000);
});