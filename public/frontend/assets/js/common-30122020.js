function openSearch() {
    document.getElementById("search-overlay").style.display = "block";
}
function closeSearch() {
    document.getElementById("search-overlay").style.display = "none";
}
$(document).on('change','.headerchangeLang',function(){
	window.location.href = base_url + "/lang/"+ $(this).val();
});
$(document).on('change','.headerchangeCurrency',function(){
	window.location.href = base_url + "/currency/"+ $(this).val();
});
$(document).on('keyup','#applycoupon', function(){
	$('#applycoupon').removeClass('alert alert-danger');
	$('#errorMessage').html('');
	/*$('#discountValue').html('');
	$('#grandTotalValue').html('');
	$('#discountLabel').hide();
	$('#grandTotal').hide();*/
});
$(document).on('click','#applyCouponButton', function(){
	$('#applycoupon').removeClass('alert alert-danger');
	$('#errorMessage').html('');
	var applycoupon = $('#applycoupon').val();
	/*if($.trim(applycoupon)==''){
		$('#applycoupon').addClass('alert alert-danger');
		$('#errorMessage').html('Please enter your coupon code.');
	}else{*/
		$.ajax({ 
			type: 'POST',
			url: base_url+'/apply-coupon',
			data: {applycoupon:applycoupon}, 						
			success: function(data){
				var ndata = $.parseJSON(data);
				if(ndata.stat=='1' && ndata.msg!=''){
					$('#discountValue').html(ndata.availedDiscount);
					$('#grandTotalValue').html(ndata.priceAfterDiscount);
				}else if(ndata.stat=='1' && ndata.msg==''){
					$('#discountValue').html(ndata.availedDiscount);
					$('#grandTotalValue').html($('#totalValue').html());					
				}else if(ndata.stat=='0'){
					$('#applycoupon').addClass('alert alert-danger');
					$('#errorMessage').html(ndata.msg);
				}
			} 
		});
	//}
});
$(document).on('click','.add-more-service-button', function(){
	var display = $(this).attr('dataval');
	if($('#display_pro_sec__'+display).length>0){
		$('#display_pro_sec__'+display).show();
		var displayNew = parseInt(display)+1;
		if($('#display_pro_sec__'+displayNew).length>0){
			$(this).attr('dataval',displayNew);
		}else{
			$('.add-more-service-button').hide();
		}		
	}
});
$("#footer_newsletter").on('submit',function(e) {
	e.preventDefault();
	$('#ajax-uploading').html('<span style="color:red">Please wait...</span>');
	$.ajax({
		url: base_url+'/newsletter',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(data){
			var ndata = $.parseJSON(data);
			if(ndata.stat=='1'){
				$('#ajax-uploading').html('<span style="color:green">Your e-mail address has been subscribed.</span>');
				$('#newsletter_email').val('');
			}else{
				$('#ajax-uploading').html('<span style="color:red">Error occurred. Please try again.</span>');
			}
		},
		error: function(){
			$('#ajax-uploading').html("");
		} 	        
	});
});
$(document).on('change','.sorting-filter-change',function(){
	$('#product_search_form').submit();
});
$(document).on('change','.price-filter-change',function(){
	$('#product_search_form').submit();
});
$(document).on('change','.manufacturer-filter-change',function(){
	$('#product_search_form').submit();
});
function toggleshippingdetails(){
	$('.shipping-details-section').toggle();
	if($('#checkout-shipping-option').prop('checked')){
		$('.shipping-content').each(function(){
			$(this).prop('required',true);
		});
	}else{
		$('.shipping-content').each(function(){
			$(this).prop('required',false);
		});
	}	
}
$(document).on('click','.add-to-wishlist',function(){
	var currObj = $(this);
	var product_id = currObj.attr('data-id');
	var userId = currObj.attr('data-user');
	if($.trim(userId)==''){
		alert('Please login to add product into your wishlist.');
		return false;
	}else{
		$.ajax({ 
			type: 'POST',
			url: base_url+'/user/add-wishlist',
			data: {product_id:product_id}, 						
			success: function(data){
				var ndata = $.parseJSON(data);
				if(ndata.stat=='1'){
					currObj.addClass('active');
					currObj.html('<i class="fa fa-heart active"></i><span class="title-font">Remove From WishList</span>');
				}else{
					currObj.html('<i class="fa fa-heart active"></i><span class="title-font">Add To WishList</span>');					
					currObj.removeClass('active');
				}
			} 
		});
	}	
});
$(document).on('click','.add-to-cart',function(){
	var currObj = $(this);
	currObj.html('Please wait...');
	var product_id = currObj.attr('data-id');
	$.ajax({ 
		type: 'POST',
		url: base_url+'/add-product',
		data: {product_id:product_id}, 						
		success: function(data){
			var ndata = $.parseJSON(data);
			if(ndata.stat=='1'){
				$('.showheadercartsection').html(ndata.cartHtml);
				$('#header_cart_counter').html(ndata.cartCount);
				currObj.html('SHOP NOW');
			}else{
				alert(ndata.msg);
				currObj.html('SHOP NOW');
			}
		} 
	});
});
$(document).on('click','.details-add-to-cart',function(){
	var currObj = $(this);
	currObj.html('Please wait...');
	var product_id = currObj.attr('data-id');
	var qty = $('#details-qty').val();
	$.ajax({ 
		type: 'POST',
		url: base_url+'/add-product',
		data: {product_id:product_id,qty:qty}, 						
		success: function(data){
			var ndata = $.parseJSON(data);
			if(ndata.stat=='1'){
				$('.showheadercartsection').html(ndata.cartHtml);
				$('#header_cart_counter').html(ndata.cartCount);
				currObj.html('add to cart');
			}else{
				alert(ndata.msg);
				currObj.html('add to cart');
			}
		} 
	});
});
$(document).on('click','.delete-from-cart',function(){
	var currObj = $(this);
	var product_id = currObj.attr('data-id');
	var loc = currObj.attr('data-loc');
	$.ajax({ 
		type: 'POST',
		url: base_url+'/delete-product',
		data: {product_id:product_id}, 						
		success: function(data){
			var ndata = $.parseJSON(data);
			if(loc!='header'){
				alert(ndata.msg);
				location.reload(true);
			}else if(ndata.stat=='1'){
				$('.showheadercartsection').html(ndata.cartHtml);
				$('#header_cart_counter').html(ndata.cartCount);
			}else{
				alert(ndata.msg);
			}
		} 
	});
});
$(document).on('change','.cart-change-qty',function(){
	var currObj 	= $(this);
	var product_id 	= currObj.attr('data-id');
	var qty 		= currObj.val();
	$.ajax({ 
		type: 'POST',
		url: base_url+'/update-product',
		data: {product_id:product_id,qty:qty}, 						
		success: function(data){
			var ndata = $.parseJSON(data);
			if(ndata.stat=='1'){
				alert(ndata.msg);
				location.reload(true);
			}else{
				alert(ndata.msg);
			}			
		} 
	});
});
$(document).on('change','#address_country',function(){
	var country = $(this).val();
	$.ajax({ 
		type: 'GET',
		url: base_url+'/getstates/'+country,
		data: {}, 						
		success: function(data){
			if(data!=''){
				$('#address_state').html(data);
			}
		} 
	});
});
$(document).on('change','#address_shipping_country',function(){
	var country = $(this).val();
	$.ajax({ 
		type: 'GET',
		url: base_url+'/getstates/'+country,
		data: {}, 						
		success: function(data){
			if(data!=''){
				$('#address_shipping_state').html(data);
			}
		} 
	});
});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});