function openSearch() {
    document.getElementById("search-overlay").style.display = "block";
}

function closeSearch() {
    document.getElementById("search-overlay").style.display = "none";
}
$(document).on('change', '.headerchangeLang', function () {
    window.location.href = base_url + "/lang/" + $(this).val();
});
$(document).on('change', '.headerchangeCurrency', function () {
    window.location.href = base_url + "/currency/" + $(this).val();
});
$(document).on('change', '#variAttribute', function () {
    var pric = $(this).val();
    if ($.trim(pric) != '') {
        var res3 = pric.split("##~~##");
        if (res3[1] != '') {
            $('#attr-pro-price').html(res3[1]);
        }
    }
});
$(document).on('click', '.selected-service-cat', function () {
    var dataequip = $(this).attr('dataequip');
    var options = '<option value="">Select</option>';
    var data = jQuery.parseJSON(dataequip);
    $.each(data, function (index, value) {
        options += '<option value="' + value + '">' + value + '</option>';
    });
    $('.allcatequiptype').each(function () {
        $(this).html(options);
    });
    $('#category_section').show();
});

$(document).on('click', '.payment-method', function () {
    var currval = $(this).val();
    if (currval == 1) {
        $('#with_handling_fee').hide();
        $('#handling_fee').hide();
        $('#without_handling_fee').show();
    } else {
        $('#handling_fee').show();
        $('#with_handling_fee').show();
        $('#without_handling_fee').hide();
    }
});
$(document).on('keyup', '#applycoupon', function () {
    $('#applycoupon').removeClass('alert alert-danger');
    $('#errorMessage').html('');
    /*$('#discountValue').html('');
    $('#grandTotalValue').html('');
    $('#discountLabel').hide();
    $('#grandTotal').hide();*/
});
$(document).on('click', '#applyCouponButton', function () {
    $('#applycoupon').removeClass('alert alert-danger');
    $('#errorMessage').html('');
    var applycoupon = $('#applycoupon').val();
    if ($.trim(applycoupon) == '') {
        $('#applycoupon').addClass('alert alert-danger');
        $('#errorMessage').html('Please enter the coupon code.');
    } else {
        $.ajax({
            type: 'POST',
            url: base_url + '/apply-coupon',
            data: {
                applycoupon: applycoupon
            },
            success: function (data) {
                var ndata = $.parseJSON(data);
                if (ndata.stat == '1' && ndata.msg != '') {
                    $('#applycoupon').removeClass('alert alert-danger');
                    $('#errorMessage').html(ndata.msg);
                    $('#discountValue').html(ndata.availedDiscount);
                    $('#grandTotalValue').html(ndata.priceAfterDiscount);
                } else if (ndata.stat == '1' && ndata.msg == '') {
                    $('#discountValue').html(ndata.availedDiscount);
                    $('#grandTotalValue').html($('#totalValue').html());
                } else if (ndata.stat == '0') {
                    $('#applycoupon').addClass('alert alert-danger');
                    $('#errorMessage').html(ndata.msg);
                }
            }
        });
    }
});
$(document).on('click', '.add-more-service-button', function () {
    var display = $(this).attr('dataval');
    if ($('#display_pro_sec__' + display).length > 0) {
        $('#display_pro_sec__' + display).show();
        var displayNew = parseInt(display) + 1;
        if ($('#display_pro_sec__' + displayNew).length > 0) {
            $(this).attr('dataval', displayNew);
        } else {
            $('.add-more-service-button').hide();
        }
    }
});
$(document).on('click', '.get-quote', function () {
    var productName = $(this).attr('data-name');
    var productUrl = $(this).attr('data-url');
    var productid = $(this).attr('data-id');
    $('#quoteProductName').val(productName);
    $('#quoteProductID').val(productid);

    $('#quoteProductUrl').val(productUrl);
    $('#getTheQuote').modal('show');
});
$("#getaquoteform").on('submit', function (e) {
    e.preventDefault();
    $('#ajax-uploading-get-quote').html('<span style="color:red">Please wait...</span>');
    $.ajax({
        url: base_url + '/getaquote',
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            var ndata = $.parseJSON(data);
            if (ndata.stat == '1') {
                $('#ajax-uploading-get-quote').html('<span style="color:green">Your request has been received successfully. we will contact you soon.</span>');
                $('#getaquoteform').trigger('reset');
                // $('#getaquoteform .form-group').hide();
                // $('#getaquoteform').addClass('submitted');
            } else {
                $('#ajax-uploading-get-quote').html('<span style="color:red">Error occurred. Please try again.</span>');
            }
        },
        error: function () {
            $('#ajax-uploading-get-quote').html("");
        }
    });
});
$("#footer_newsletter").on('submit', function (e) {
    e.preventDefault();
    $('#ajax-uploading').html('<span style="color:red">Please wait...</span>');
    $.ajax({
        url: base_url + '/newsletter',
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            var ndata = $.parseJSON(data);
            if (ndata.stat == '1') {
                $('#ajax-uploading').html('<span style="color:green">Your e-mail address has been subscribed.</span>');
                $('#newsletter_email').val('');
            } else {
                $('#ajax-uploading').html('<span style="color:red">Error occurred. Please try again.</span>');
            }
        },
        error: function () {
            $('#ajax-uploading').html("");
        }
    });
});
$(document).on('change', '.sorting-filter-change', function () {
    $('#product_search_form').submit();
});
$(document).on('change', '.price-filter-change', function () {
    $('#product_search_form').submit();
});
$(document).on('change', '.manufacturer-filter-change', function () {
    $('#product_search_form').submit();
});

function toggleshippingdetails() {
    $('.shipping-details-section').toggle();
    if ($('#checkout-shipping-option').prop('checked')) {
        $('.shipping-content').each(function () {
            $(this).prop('required', true);
        });
    } else {
        $('.shipping-content').each(function () {
            $(this).prop('required', false);
        });
    }
}
$(document).on('click', '.add-to-wishlist', function () {
    var currObj = $(this);
    var product_id = currObj.attr('data-id');
    var userId = currObj.attr('data-user');
    if ($.trim(userId) == '') {
        $('#headershowajaxmessage').html('Please login to add product into your wishlist.');
        $('#headershowajaxmessage').addClass('alert-danger');
        $('#headershowajaxmessage').show();
        //alert('Please login to add product into your wishlist.');
        return false;
    } else {
        $.ajax({
            type: 'POST',
            url: base_url + '/user/add-wishlist',
            data: {
                product_id: product_id
            },
            success: function (data) {
                var ndata = $.parseJSON(data);
                if (ndata.stat == '1') {
                    currObj.addClass('active');
                    currObj.html('<i class="fa fa-heart active"></i><span class="title-font">Remove From WishList</span>');
                } else {
                    currObj.html('<i class="fa fa-heart active"></i><span class="title-font">Add To WishList</span>');
                    currObj.removeClass('active');
                }
            }
        });
    }
});
$(document).on('click', '.add-to-cart', function () {
    var currObj = $(this);
    currObj.html("<img id='theImg' src='https://www.imedicalshop.com/imedres/public/frontend/assets/images/loading.gif'/>");
    var product_id = currObj.attr('data-id');
    $.ajax({
        type: 'POST',
        url: base_url + '/add-product',
        data: {
            product_id: product_id
        },
        success: function (data) {
            var ndata = $.parseJSON(data);
            if (ndata.stat == '1') {
                $('.showheadercartsection').html(ndata.cartHtml);
                $('.header_cart_counter').html(ndata.cartCount);
                $('#headershowajaxmessage').html(ndata.msg);
                //$('#headershowajaxmessage').addClass('alert-success');
                //$('#headershowajaxmessage').show();
                currObj.html("<img id='theImg' src='https://www.imedicalshop.com/imedres/public/frontend/assets/images/cart.jpg'/>");
                Swal.fire({
                    position: 'center-center',
                    icon: 'success',
                    title: 'Product added to your cart Successfully',
                    showConfirmButton: false,
                    timer: 1100
                })

            } else {
                //alert(ndata.msg);
                $('#headershowajaxmessage').html(ndata.msg);
                // $('#headershowajaxmessage').addClass('alert-danger');
                // $('#headershowajaxmessage').show();                
                currObj.html("<img id='theImg' src='https://www.imedicalshop.com/imedres/public/frontend/assets/images/cart.jpg'/>");
                Swal.fire({
                    position: 'center-center',
                    icon: 'error',
                    title: 'Product is out of stock.',
                    showConfirmButton: false,
                    timer: 1100
                })
            }
        }
    });
});
$(document).on('click', '.details-add-to-cart', function () {
    $('#error-attribute-div').html('');
    if ($('#variAttribute').length > 0 && $('#variAttribute').val() == '') {
        $('#error-attribute-div').html('Please select the value');
        $('#variAttribute').focus();
        return false;
    } else {
        var currObj = $(this);
        currObj.html("<img id='theImg' src='https://www.imedicalshop.com/imedres/public/frontend/assets/images/loading.gif'/>");
        var product_id = currObj.attr('data-id');
        var qty = $('#details-qty').val();
        if ($('#variAttribute').length > 0) {
            var variationId = $('#variAttribute').val();
        } else {
            var variationId = '';
        }
        $.ajax({
            type: 'POST',
            url: base_url + '/add-product',
            data: {
                product_id: product_id,
                qty: qty,
                variationId: variationId
            },
            success: function (data) {
                var ndata = $.parseJSON(data);
                if (ndata.stat == '1') {
                    $('.showheadercartsection').html(ndata.cartHtml);
                    $('.header_cart_counter').html(ndata.cartCount);
                    $('#headershowajaxmessage').html(ndata.msg);
                    // $('#headershowajaxmessage').addClass('alert-success');
                    // $('#headershowajaxmessage').show();
                    currObj.html("Add to Cart");

                    Swal.fire({
                        position: 'center-center',
                        icon: 'success',
                        title: 'Product added to your cart Successfully',
                        showConfirmButton: false,
                        timer: 1100
                    })


                } else {
                    //alert(ndata.msg);
                    $('#headershowajaxmessage').html(ndata.msg);
                    // $('#headershowajaxmessage').addClass('alert-danger');
                    // $('#headershowajaxmessage').show();
                    currObj.html("Add to Cart");

                    Swal.fire({
                        position: 'center-center',
                        icon: 'error',
                        title: 'Product is out of stock.',
                        showConfirmButton: false,
                        timer: 1100
                    })
                }
            }
        });
    }
});
$(document).on('click', '.delete-from-cart', function () {
    var currObj = $(this);
    var product_id = currObj.attr('data-id');
    var loc = currObj.attr('data-loc');
    $.ajax({
        type: 'POST',
        url: base_url + '/delete-product',
        data: {
            product_id: product_id,
            loc: loc
        },
        success: function (data) {
            var ndata = $.parseJSON(data);
            if (loc != 'header') {
                //alert(ndata.msg);
                Swal.fire({
                    position: 'center-center',
                    icon: 'success',
                    title: 'Product has been removed from cart.',
                    showConfirmButton: false,
                    timer: 1100
                })
                location.reload(true);
            } else if (ndata.stat == '1') {                
                $('.showheadercartsection').html(ndata.cartHtml);
                $('.header_cart_counter').html(ndata.cartCount);
                //$('#headershowajaxmessage').html(ndata.msg);
                // $('#headershowajaxmessage').addClass('alert-success');
                // $('#headershowajaxmessage').show();
                Swal.fire({
                    position: 'center-center',
                    icon: 'success',
                    title: 'Product has been removed from cart.',
                    showConfirmButton: false,
                    timer: 1100
                })
            } else {
                //alert(ndata.msg);
                //$('#headershowajaxmessage').html(ndata.msg);
                //$('#headershowajaxmessage').addClass('alert-danger');
                //$('#headershowajaxmessage').show();

                Swal.fire({
                    position: 'center-center',
                    icon: 'error',
                    title: 'ERROR',
                    showConfirmButton: false,
                    timer: 1100
                })
            }
        }
    });
});
$(document).on('change', '.cart-change-qty', function () {
    var currObj = $(this);
    var product_id = currObj.attr('data-id');
    var qty = currObj.val();
    $.ajax({
        type: 'POST',
        url: base_url + '/update-product',
        data: {
            product_id: product_id,
            qty: qty
        },
        success: function (data) {
            var ndata = $.parseJSON(data);
            if (ndata.stat == '1') {
                //alert(ndata.msg);
                Swal.fire({
                    position: 'center-center',
                    icon: 'success',
                    title: 'Your Cart has been Updated',
                    showConfirmButton: false,
                    timer: 1100
                })
                location.reload(true);
            } else {
                //alert(ndata.msg);
                $('#headershowajaxmessage').html(ndata.msg);
                $('#headershowajaxmessage').addClass('alert-danger');
                $('#headershowajaxmessage').show();
            }
        }
    });
});
$(document).on('change', '#address_country', function () {
    var country = $(this).val();
    $.ajax({
        type: 'GET',
        url: base_url + '/getstates/' + country,
        data: {},
        success: function (data) {
            if (data != '') {
                $('#address_state').html(data);
            }
        }
    });
});
$(document).on('change', '#address_shipping_country', function () {
    var country = $(this).val();
    $.ajax({
        type: 'GET',
        url: base_url + '/getstates/' + country,
        data: {},
        success: function (data) {
            if (data != '') {
                $('#address_shipping_state').html(data);
            }
        }
    });
});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$(document).on('change', '#country_yo', function () {
    var countryid = $(this).val();
    $.ajax({
        type: 'GET',
        url: base_url + '/getstates/' + countryid,
        data: {},
        success: function (data) {
            if (data != '') {
                $("#state_yo").empty().append(data);
            }
        }
    });
});

$('.submitratingvendor').on('click', function () {

    var rating = $('input[name="radiorating"]:checked').val();
    var description = $("#comment").val();
    var slug = $("#slug").val();
    var flag = true;
    if (rating == false) {
        $("#rating_err").html('Please choose rating');
        flag = false;
    }

    if (description.trim() == "") {
        $("#comment").css("border-color", "red");
        $("#description_err").html('Please enter description');
        flag = false;
    }

    if (flag == true) {
        $.ajax({
            type: 'POST',
            url: base_url + '/submitvendorrating',
            data: {
                rating: rating,
                description: description,
                slug: slug
            },
            success: function (data) {

                Swal.fire({
                    title: "Thank You!",
                    text: "For check abusive content your rating visible after verify abusive content!",
                    icon: "success",
                });
                setTimeout(function () {
                    location.reload();
                }, 2000);
                
            }
        });

        $("#comment").val('')
    }
});

$('.submitratingproduct').on('click', function () {

    var rating = $('input[name="radiorating"]:checked').val();
    var description = $("#comment").val();
    var slug = $("#slug").val();
    var flag = true;
    if (rating == false) {
        $("#rating_err").html('Please choose rating');
        flag = false;
    }

    if (description.trim() == "") {
        $("#comment").css("border-color", "red");
        $("#description_err").html('Please enter description');
        flag = false;
    }

    if (flag == true) {
        $.ajax({
            type: 'POST',
            url: base_url + '/submitproductrating',
            data: {
                rating: rating,
                description: description,
                slug: slug
            },
            success: function (data) {
                Swal.fire({
                    title: "Thank You!",
                    text: "For check abusive content your rating visible after verify abusive content!",
                    icon: "success",
                });
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
        });
        $("#comment").val('')
    }
});

$(document).ready(function() {
    setInterval(function(){ 
        var objs = document.getElementsByClassName('countdowntimer');
        for(var i=0; i<objs.length; i++){
            var auc_id = objs[i].id;
            auc_id = auc_id.replace('auc_','');
            $.ajax({
            type: 'GET',
            url: base_url + '/auctiontimer/' + auc_id,
            data: {},
            success: function (data) {
                if (data != '') {
                    document.getElementById('auc_'+data.id).innerHTML = data.time_remain;
                }
            }
        });
        } 
    }, 1000);
});

function bidNow(auc_id){
    document.getElementById('bidresponsemessage').style.display = "none";
    document.getElementById('bid_auction_id').value = auc_id;
    document.getElementById('bid_amount').value = "";
    document.getElementById('bidformcontent').style.display = "none";
    document.getElementById('bidformloading').style.display = "block";
    document.getElementById('last_bid_amount').innerHTML = "";
    $.ajax({
        type: 'GET',
        url: base_url + '/bidnow/' + auc_id,
        data: {},
        success: function (data) {
            if (data) {
                document.getElementById('bidformcontent').style.display = "block";
                document.getElementById('bidformloading').style.display = "none";
                document.getElementById('last_bid_amount').innerHTML = data.last_bid_amount;
                document.getElementById('bid_auction_id').value = data.id;
            }
        }
    });
}

function submitBid(){
    var bid_auction_id = document.getElementById('bid_auction_id').value;
    var bid_amount = document.getElementById('bid_amount').value;
    document.getElementById('btn_submit_bid').innerHTML = "Please wait...";
    $.ajax({
        type: 'POST',
        url: base_url + '/submitbid',
        data: {
            bid_auction_id: bid_auction_id,
            bid_amount: bid_amount
        },
        success: function (data) {
            document.getElementById('bidresponsemessage').style.display = "block";
            document.getElementById('btn_submit_bid').innerHTML = "Place Your bid";
            $("#bidresponsemessage").removeClass( "alert alert-success alert-danger" );
            if(data.type == 'error'){
                $("#bidresponsemessage").addClass( "alert alert-danger" );
            } else{
                $("#bidresponsemessage").addClass( "alert alert-success" );
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
            document.getElementById('bidresponsemessage').innerHTML = data.message;
        }
    });
}

function saveForBidLater(auc_id){
    document.getElementById('linksaveforbidlater').innerHTML = "Please wait...";
    $.ajax({
        type: 'GET',
        url: base_url + '/saveforbidlater/' + auc_id,
        data: {},
        success: function (data) {
            document.getElementById('linksaveforbidlatermessage').style.display = "block";
            $("#linksaveforbidlatermessage").removeClass( "alert alert-success alert-danger" );
            if (data) {
                if(data.type == 'error'){
                    $("#linksaveforbidlatermessage").addClass( "alert alert-danger" );
                } else{
                    $("#linksaveforbidlatermessage").addClass( "alert alert-success" );
                    document.getElementById('linksaveforbidlater').innerHTML = "Saved!";
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
                document.getElementById('linksaveforbidlatermessage').innerHTML = data.message;
            }
        }
    });
}