@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>{{$page_title}}</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$page_title}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb End -->
    <!-- section start -->
    <section class="wishlist-section section-b-space">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="account-sidebar"><a class="popup-btn">{{__('My Account')}}</a></div>
                    <div class="dashboard-left">
                        <div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> {{__('Back')}}</span></div>
                        <div class="block-content">
                            <ul>
                                <li @if(isset($currPage) && $currPage=='dashboard') class="active" @endif ><a href="{{route('user')}}"> <i><img src="{{ asset('frontend/assets/images/dashboard.png') }}" alt="" srcset=""></i> <span>{{__('Dashboard')}}</span> </a></li>
                                <li @if(isset($currPage) && $currPage=='myorders') class="active" @endif ><a href="{{route('my-orders')}}"><i><img src="{{ asset('frontend/assets/images/bag.png') }}" alt="" srcset=""></i> {{__('My Orders')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='mybids') class="active" @endif ><a href="{{route('my-bids')}}"><i><img src="{{ asset('frontend/assets/images/bag.png') }}" alt="" srcset=""></i> {{__('My Bids')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='mywishlist') class="active" @endif ><a href="{{route('my-wishlist')}}"><i><img src="{{ asset('frontend/assets/images/my-wishlist.png') }}" alt="" srcset=""></i> {{__('My Wishlist')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='editprofile') class="active" @endif ><a href="{{route('edit-profile')}}"> <i><img src="{{ asset('frontend/assets/images/my-account.png') }}" alt="" srcset=""></i>{{__('My Account')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='addressbook') class="active" @endif ><a href="{{route('address-book')}}"><i><img src="{{ asset('frontend/assets/images/address.png') }}" alt="" srcset=""></i> {{__('Address Book')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='changepassword') class="active" @endif ><a href="{{route('change-password')}}"> <i><img src="{{ asset('frontend/assets/images/password.png') }}" alt="" srcset=""></i>{{__('Change Password')}}</a></li>
                                <li class="last">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                       <i><img src="{{ asset('frontend/assets/images/logout.png') }}" alt="" srcset=""></i> <span>{{ __('Log Out') }}</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="page-title">
                        <h2>{{$page_title}}</h2>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            @if (Session::has('message'))
                                {!! successMesaage(Session::get('message')) !!}   
                            @endif 
                            @if (Session::has('error'))
                                {!! errorMesaage(Session::get('error')) !!}   
                            @endif
                            {!! validationError($errors) !!}
                            <table class="table cart-table table-responsive-xs address-book">
                                <thead>
                                    <tr class="table-head">
                                        <th scope="col" class="align-left">{{__('Default Address')}}</th>
                                        <th scope="col">{{__('Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($allAddress) && count($allAddress)>0)
                                        @foreach($allAddress as $singleAddress)
                                            <tr>
                                                <td>
                                                    <div class="radio-option">
                                                        <input type="radio" name="markDefaultaddress" id="markDefaultaddress-{{$singleAddress->id}}" value="{{$singleAddress->id}}" @if($singleAddress->default==1) checked @endif>
                                                        <label for="payment-2">{{$singleAddress->first_name.' '.$singleAddress->last_name.', '.$singleAddress->phone.', '.$singleAddress->company.', '.$singleAddress->address1.' '.$singleAddress->address2.', '.$singleAddress->city.' '.$singleAddress->stateName.', '.$singleAddress->countryName.' '.$singleAddress->zip_code}}</label>
                                                    </div>
                                                </td>
                                                <td>                                            
                                                    <a href="{{url('user/address-book/'.base64_encode($singleAddress->id))}}" class="edit-user-address" data-id="{{$singleAddress->id}}"> <i class="ti-pencil"></i> {{__('Edit')}}</a>
                                                    <a href="{{ url('user/remove-address') }}"
                                                       onclick="event.preventDefault(); document.getElementById('remove-address-{{$singleAddress->id}}').submit();"> <i class="ti-trash"></i> {{__('Remove')}}</a>
                                                    <form id="remove-address-{{$singleAddress->id}}" action="{{ url('user/remove-address') }}" method="POST" style="display: none;">
                                                        <input type="hidden" name="address_id" value="{{$singleAddress->id}}">
                                                        @csrf
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>                       
                            </table>
                        </div>
                    </div>
                    <div class="add-address">
                        <form action="{{route('update-address')}}" method="POST" name="general-form" class="jqueryValidate">
                            {{csrf_field()}}
                            <input type="hidden" name="action" value="{{((isset($editAddress->id) && !empty($editAddress->id))?'edit':'add') }}">
                            <input type="hidden" name="recid" value="{{((isset($editAddress->id) && !empty($editAddress->id))?$editAddress->id:'') }}">
                            <h4>{{__('Personal Information')}}</h4>                            
                            <div class="form-row">
                                <div class="from-group col-md-6 mb-4">
                                    <div class="from-group">
                                        <input type="checkbox" name="defaultaddress" class="form-control" id="defaultaddress" {{(((isset($editAddress->first_name) && !empty($editAddress->first_name)) || (empty($allAddress) || count($allAddress)==0) )?'checked':'')}} value="1">
                                        <span for="defaultaddress">{{__('Default Address')}}</span>
                                    </div>
                                </div>                                
                            </div>
                            <div class="form-row">
                                <div class="from-group col-md-6 mb-4">
                                    <div class="input-group ">                             
                                        <input type="text" name="first_name" class="form-control" id="inlineFormInputGroup0" placeholder="{{__('First Name')}}" value="{{((isset($editAddress->first_name) && !empty($editAddress->first_name))?$editAddress->first_name:'') }}" required >
                                    </div>
                                </div>
                                <div class="from-group col-md-6 mb-4">
                                    <div class="input-group ">                              
                                        <input type="text" name="last_name" class="form-control" id="inlineFormInputGroup1" placeholder="{{__('Last Name')}}" value="{{((isset($editAddress->last_name) && !empty($editAddress->last_name))?$editAddress->last_name:'') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="from-group col-md-6 mb-4">
                                    <div class="input-group ">
                                        <input type="text" name="company" class="form-control" id="inlineFormInputGroup2" placeholder="{{__('Company Name')}}" value="{{((isset($editAddress->company) && !empty($editAddress->company))?$editAddress->company:'') }}">
                                    </div>
                                </div>
                                <div class="from-group col-md-6 mb-4">
                                    <div class="input-group">
                                        <input type="text" name="phone" class="form-control" id="inlineFormInputGroup8" placeholder="{{__('Phone')}}" value="{{((isset($editAddress->phone) && !empty($editAddress->phone))?$editAddress->phone:'') }}" required> 
                                    </div>
                                </div>                             
                            </div>
                            <div class="form-row">
                                <div class="from-group col-md-6 mb-4">
                                    <div class="input-group ">
                                        <input type="text" name="address1" class="form-control" id="inlineFormInputGroup3" placeholder="{{__('Address')}}" value="{{((isset($editAddress->address1) && !empty($editAddress->address1))?$editAddress->address1:'') }}" required>
                                    </div>
                                </div>
                                <div class="from-group col-md-6 mb-4">
                                    <div class="input-group ">
                                        <input type="text" name="address2" class="form-control" id="inlineFormInputGroup3" placeholder="{{__('Address 2')}}" value="{{((isset($editAddress->address2) && !empty($editAddress->address2))?$editAddress->address2:'') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="from-group col-md-6 mb-4">
                                    <div class="input-group select-control">
                                        <select class="form-control" name="country" id="address_country" required>
                                            <option value="">{{__('Select Country')}}</option>
                                            @if(!empty($allCountries))
                                                @foreach($allCountries as $singleCountry)
                                                    <option value="{{$singleCountry->countryID}}" {{((isset($editAddress->country) && $editAddress->country==$singleCountry->countryID)?'Selected':'') }}>{{$singleCountry->countryName}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="from-group col-md-6 mb-4">
                                    <div class="input-group select-control">
                                        <select class="form-control" name="state" id="address_state" required>
                                            <option selected="">{{__('Select State')}}</option>
                                            @if(!empty($allStates))
                                                @foreach($allStates as $singleState)
                                                    <option value="{{$singleState->stateID}}" {{((isset($editAddress->state) && $editAddress->state==$singleState->stateID)?'Selected':'') }}>{{$singleState->stateName}}</option>
                                                @endforeach
                                            @endif
                                        </select> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="from-group col-md-6 mb-4">
                                    <div class="input-group">              
                                        <input type="text" name="city" class="form-control" id="inlineFormInputGroup7" placeholder="{{__('City')}}" value="{{((isset($editAddress->city) && !empty($editAddress->city))?$editAddress->city:'') }}" required> 
                                    </div>
                                </div>
                                <div class="from-group col-md-6 mb-4">
                                    <div class="input-group">           
                                        <input type="text" name="zip_code" class="form-control" id="inlineFormInputGroup7" placeholder="{{__('Postal Code')}}" value="{{((isset($editAddress->zip_code) && !empty($editAddress->zip_code))?$editAddress->zip_code:'') }}" required> 
                                    </div>
                                </div>                              
                            </div>                                        
                            <input type="submit" class="btn btn-secondary swipe-to-top" value="{{__('Save Address')}}">
                            @if(isset($editAddress->zip_code) && !empty($editAddress->zip_code))
                                <a href="{{url('user/address-book')}}" class="btn">{{__('Cancel')}}</a>
                            @else
                                <button type="reset" class="btn btn-secondary swipe-to-top">{{__('Reset')}}</button>
                            @endif
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section end -->
@endsection