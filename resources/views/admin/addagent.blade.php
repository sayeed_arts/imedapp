@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Add User
                    <span><a href="{{url('admin/agents')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/agents')}}"><i class="fa fa-table"></i> Users</a></li>
            <li>Add User</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/agents/save')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">First Name</label>
                            <div class="col-md-9">
                                <input type="text" id="name" name="name" class="form-control" required placeholder="First Name" value="{{old('name')}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="last_name">Last Name</label>
                            <div class="col-md-9">
                                <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name" value="{{old('last_name')}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">Email</label>
                            <div class="col-md-9">
                                <input type="email" id="email" name="email" class="form-control" required placeholder="Email" value="{{old('email')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Email Verified</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="verified_radio1">
                                    <input type="radio" id="verified_radio1" name="verified" value="1"checked> Yes
                                </label>
                                <label class="radio-inline" for="verified_radio2">
                                    <input type="radio" id="verified_radio2" name="verified" value="0"> No
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">User Status</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="display_status_radio1">
                                    <input type="radio" id="display_status_radio1" name="is_approved" value="1" checked> Active
                                </label>
                                <label class="radio-inline" for="display_status_radio2">
                                    <input type="radio" id="display_status_radio2" name="is_approved" value="0" > Inactive
                                </label>
                            </div>
                        </div>    
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="password">Password</label>
                            <div class="col-md-9">
                                <input type="password" id="password" name="password" class="form-control" required placeholder="Password" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="confirm_password">Confirm Password</label>
                            <div class="col-md-9">
                                <input type="password" id="confirm_password" name="confirm_password" class="form-control" required placeholder="Confirm Password" value="">
                            </div>
                        </div>
                        @php
                            $allAllowed = allAllowedSections();
                            $allowed_sections_array = old('allowed_sections');
                        @endphp
                        <div class="form-group">
                            <label class="col-md-3 control-label">All Sections</label>
                            <div class="col-md-9">
                                <a href="javascript:void(0)" onclick="selectall(1)">Select All</a> &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="javascript:void(0)" onclick="selectall(2)">Unselect All</a>
                                <br>
                                @foreach($allAllowed as $key=>$value)
                                    <label style="width: 30%;" class="radio-inline" for="allowed_sections{{$key}}">
                                        <input type="checkbox" id="allowed_sections{{$key}}" name="allowed_sections[]" value="{{$key}}" 
                                        @if(is_array($allowed_sections_array))
                                        @if(in_array($key,$allowed_sections_array)) checked @endif @endif> {{$value}}
                                    </label>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
    <script type="text/javascript">
        function selectall(val){
            if(val==1){
                var ele=document.getElementsByName('allowed_sections[]');  
                for(var i=0; i<ele.length; i++){  
                    if(ele[i].type=='checkbox')  
                        ele[i].checked=true;  
                }  
            }else{
                var ele=document.getElementsByName('allowed_sections[]');  
                for(var i=0; i<ele.length; i++){  
                    if(ele[i].type=='checkbox')  
                        ele[i].checked=false;  
                }  
            }
        }
    </script>
@endsection