
@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    <?php if(!isset($auctiondetail)){ echo 'Add'; } ?> Auction
                    <span><a href="{{url('admin/auctions')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/auctions')}}"><i class="fa fa-table"></i> Auctions</a></li>
            <li><?php if(!isset($auctiondetail)){ echo 'Add'; } ?> Auction</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/auctions/save')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" id="acut_form">
                        {{ csrf_field() }}
                    <div class="text-center">
<span id="countdown" style="font:weight:100px;"></span>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Category</label>
                            <div class="col-md-8">
                            <?php 
                            $selected_array = array();
                                if(!empty($auctiondetail->category_id)){
                                  $selected_array =  array($auctiondetail->category_id);
                                }

                            ?>

<?php if(!empty($auctiondetail->category_id)):


$newdate = date('M d, Y',strtotime($auctiondetail->end_date));

?>
 <input type="hidden" id="js_time_date" value="<?=isset($newdate)?$newdate.' '.$auctiondetail->end_time:''?>">
 <input type="hidden" name="auctionid" value="<?=isset($auctiondetail->id)?$auctiondetail->id:''?>">
<?php  
 endif;
 ?>
                           <select name="category_id" id="category_id_auc" class="form-control" required>
                                                            <option value="">Select</option>
                                                            @if(!empty($allCategory))
                                                                @foreach($allCategory as $category)
                                                                    <option value="{{$category->id}}"
 @if(isset($auctiondetail->category_id) && ($category->id == $auctiondetail->category_id) )
                                            selected
                                            @endif >{{$category->name}}</option>
                                                                    @if(count($category->subcategory))
                                                                        @include('admin/subCategoriesProOptions',['subcategories' => $category->subcategory,'parent'=>$category->name,'selectedCat'=>$selected_array])
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        </select>
                            </div>
                        </div>
                    </div>


<div class="text-center">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Product</label>
                            <div class="col-md-8">
                                <select name="product_id" id="product_id_auc" class="form-control fetch_product_auc" required><option value="">Select Product</option>
                                     @if(!empty($allProduct))       
                                    @foreach($allProduct as $key=>$val)
                                    
                                    <option     @if(isset($auctiondetail->product_id) && ($key == $auctiondetail->product_id) )
                                            selected
                                            @endif value="<?=$key?>"><?=$val?></option>
                                    @endforeach
                                    @endif
                                </select>
                                
                                </div>
                        </div>
                    </div>


<div class="text-center">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Short Description</label>
                            <div class="col-md-8">
                                <textarea name="short_desc" id="short_desc" class="form-control" rows="3" required maxlength="254"><?=isset($auctiondetail->short_desc)?$auctiondetail->short_desc:''?></textarea>
                            </div>
                        </div>
                    </div>



<div class="text-center">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">End Date</label>
                            <div class="col-md-8">
                                <input type="text" id="end_date" name="end_date" class="form-control input-datepicker" required placeholder="Enter End Date" value="<?=isset($auctiondetail->end_date)?date('m/d/Y',strtotime($auctiondetail->end_date)):''?>" tabindex="1" readonly="readonly">
                            </div>
                        </div>
                    </div>


<div class="text-center">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">End Time</label>
                            <div class="col-md-8">
                                <input type="text" id="end_time" name="end_time" class="form-control datetimepicker5" required placeholder="Enter End Time" value="<?=isset($auctiondetail->end_time)?$auctiondetail->end_time:''?>" >
                            </div>
                        </div>
                    </div>


<div class="text-center">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Starting Bid Price</label>
                            <div class="col-md-8">
                                <input type="text" id="start_bid_price" name="start_bid_price" class="form-control" required placeholder="Enter Start Bid Price" value="<?=isset($auctiondetail->start_bid_price)?$auctiondetail->start_bid_price:''?>" tabindex="1">
                            </div>
                        </div>
                    </div>


<div class="text-center">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Minimum Bid Price</label>
                            <div class="col-md-8">
                                <input type="text" id="min_bid_price" name="min_bid_price" class="form-control" required placeholder="Enter Minimum Bid Price" value="<?=isset($auctiondetail->min_bid_price)?$auctiondetail->min_bid_price:''?>" tabindex="1">
                            </div>
                        </div>
                    </div>



<div class="text-center">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="description">Description</label>
                            <div class="col-md-8">
                                <textarea name="description" id="description" class="form-control" rows="3" required><?=isset($auctiondetail->description)?$auctiondetail->description:''?></textarea>
                            </div>
                        </div>
                    </div>


<div class="text-center">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Conditions</label>
                            <div class="col-md-8">
                                <textarea name="conditions" id="conditions" class="form-control" rows="3" required><?=isset($auctiondetail->conditions)?$auctiondetail->conditions:''?></textarea>
                            </div>
                        </div>
                    </div>


<div class="text-center">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Terms and Conditions</label>
                            <div class="col-md-8">
                                <textarea name="terms_and_conditions" id="terms_and_conditions" class="form-control" rows="3" required><?=isset($auctiondetail->terms_and_conditions)?$auctiondetail->terms_and_conditions:''?></textarea>
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Featured</label>
                            <div class="col-md-9" style="text-align: left;">
                                <label class="radio-inline" for="featured_radio1">
                                    <input type="radio" id="featured_radio1" name="featured" value="1" <?php if(isset($auctiondetail->featured) && $auctiondetail->featured=='1') { echo 'checked'; } ?> /> Yes
                                </label>
                                <label class="radio-inline" for="featured_radio2">
                                    <input type="radio" id="featured_radio2" name="featured" value="0"  <?php if((isset($auctiondetail->featured) && $auctiondetail->featured=='0') || !isset($auctiondetail)) { echo 'checked'; } ?>/> No
                                </label>
                            </div>
                        </div>
                    </div>
                    <hr>

<div class="text-center"><h3><b>FAQ LIST</b></h3></div>
@if(!empty($faqlist)):
<div>
         @foreach($faqlist as $val):

               <div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Question</label>
                            <div class="col-md-8">
                                <input class="form-control" name="faq_ques[]" id="faq_ques" value="<?=isset($val->faq_ques)?$val->faq_ques:''?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Answer</label>
             <div class="row">
                            <div class="col-md-7">
                                <textarea name="faq_ans[]" id="terms_and_cond" class="form-control" rows="3" required><?=isset($val->faq_ans)?$val->faq_ans:''?></textarea>
                            </div>
                            <div class="col-md-1">
                                <button style="margin-top:40px;" class="btn remove_lm btn-danger">Remove</button>
                            </div>
                        </div>

                </div>
                </div>

                    
@endforeach
                    </div>
                       



@endif

<button type="button" class="addmore_auction btn btn-primary" style="float:right;">Add More</button>


<div class="faq_list">
               <div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Question</label>
                            <div class="col-md-8">
                                <input class="form-control" name="faq_ques[]" id="faq_ques" required>
                            </div>
                        </div>
             
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Answer</label>
                            <div class="col-md-8">
                                <textarea name="faq_ans[]" id="terms_and_cond" class="form-control" rows="3" required></textarea>
                            </div>
                        </div>
                </div>

                    </div>
                    

                       </div>

                        <div class="form-group form-actions">
                            <div class="col-md-8 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection


