@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Add Coupon
                    <span><a href="{{url('admin/coupons')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/coupons')}}"><i class="fa fa-table"></i> Coupons</a></li>
            <li>Add Coupon</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">                    
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <form action="{{url('admin/coupons/save')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        @csrf
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Name</label>
                            <div class="col-md-9">
                                <input type="text" id="name" name="name" class="form-control" required placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="code">Code</label>
                            <div class="col-md-9">
                                <input type="text" id="code" name="code" class="form-control" required placeholder="Code">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="type">Type</label>
                            <div class="col-md-9">
                                <select id="type" name="type" class="form-control">
                                    <option value="0">Fixed</option>
                                    <option value="1">Percent</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="value">Value</label>
                            <div class="col-md-9">
                                <input type="number" step="0.01" id="value" name="value" class="form-control" required placeholder="Value">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="start_date">Start Date</label>
                            <div class="col-md-9">
                                <input type="date" id="start_date" name="start_date" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="end_date">End Date</label>
                            <div class="col-md-9">
                                <input type="date" id="end_date" name="end_date" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="minimum_spend">Minimum Spend</label>
                            <div class="col-md-9">
                                <input type="number" step="0.01" id="minimum_spend" name="minimum_spend" class="form-control" required placeholder="Minimum Spend">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="maximum_spend">Maximum Spend</label>
                            <div class="col-md-9">
                                <input type="number" step="0.01" id="maximum_spend" name="maximum_spend" class="form-control" required placeholder="Maximum Spend">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="maximum_discount">Maximum Discount</label>
                            <div class="col-md-9">
                                <input type="number" step="0.01" id="maximum_discount" name="maximum_discount" class="form-control" required placeholder="Maximum Discount">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="usage_limit_per_coupon">Usage Limit Per Coupon</label>
                            <div class="col-md-9">
                                <input type="number" id="usage_limit_per_coupon" name="usage_limit_per_coupon" class="form-control" required placeholder="Usage Limit Per Coupon">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="usage_limit_per_customer">Usage Limit Per Customer</label>
                            <div class="col-md-9">
                                <input type="number" id="usage_limit_per_customer" name="usage_limit_per_customer" class="form-control" required placeholder="Usage Limit Per Customer">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Status</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="display_status_radio1">
                                    <input type="radio" id="display_status_radio1" name="status" value="1" checked> Active
                                </label>
                                <label class="radio-inline" for="display_status_radio2">
                                    <input type="radio" id="display_status_radio2" name="status" value="0"> Inactive
                                </label>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection