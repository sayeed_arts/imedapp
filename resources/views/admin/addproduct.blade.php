@extends('layouts.adminapp')

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Forms General Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                Add Product
                <span><a href="{{url('admin/products')}}" class="btn btn-default">Cancel</a></span>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('admin/products')}}"><i class="fa fa-table"></i> Products</a></li>
        <li>Add Product</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Content -->
                @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}
                @endif
                {!! validationError($errors) !!}
                <form action="{{url('admin/products/save')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                    {{ csrf_field() }}
                    <div class="nav-tab-section">
                        <h2>Product Data</h2>
                        <div class="row" style="min-height:300px;">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <ul class="nav nav-tabs tabs-left">
                                            <li class="active"><a href="#home" data-toggle="tab">General</a></li>
                                            <li><a href="#price" data-toggle="tab">Price</a></li>
                                            <li><a href="#inventory" data-toggle="tab">Inventory</a></li>
                                            <li><a href="#images" data-toggle="tab">Images</a></li>
                                            <li><a href="#attributes" data-toggle="tab">Attributes</a></li>
                                            <li><a href="#related" data-toggle="tab">Related Products</a></li>
                                            <li><a href="#seo" data-toggle="tab">SEO</a></li>
                                            @if(!empty($allLanguages) && count($allLanguages)>0)
                                                @foreach($allLanguages as $singleLanguage)
                                                    <li><a href="#language_{{$singleLanguage->code}}" data-toggle="tab">{{$singleLanguage->name}}</a></li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="tab-content">
                                            <!-- GENERAL TAB START -->
                                            <div class="tab-pane active" id="home">
                                                <h2>English</h2>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="product_type">Product
                                                        Available</label>
                                                    <div class="col-md-9">
                                                        <select name="product_type" id="product_type" class="form-control">
                                                            <option value="1" @if(old('product_type') == '1') selected @endif>For Sale</option>
                                                            <option value="0" @if(old('product_type') == '0') selected @endif>For Rent</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="name">Title</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="name" name="name" class="form-control" required placeholder="Title" value="{{old('name')}}">
                                                    </div>
                                                </div>


                                                

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="manufacturer_id">Vendor</label>
                                                    <div class="col-md-9">
                                                        <select name="user_id" id="user_id"
                                                            class="form-control">
                                                            <option value="">Select</option>
                                                            @if(!empty($vendorlist))
                                                                @foreach($vendorlist as $key=>$val)
                                                                    <option value="{{$key}}">{{$val}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="sku">SKU</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="sku" name="sku" class="form-control" required placeholder="SKU" value="{{old('sku')}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="short_description">Short Description</label>
                                                    <div class="col-md-9">
                                                        <textarea id="short_description" name="short_description" class="form-control" placeholder="Short Description">{{old('short_description')}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="content">Description</label>
                                                    <div class="col-md-9">
                                                        <textarea id="content" name="content" class="form-control ckeditor"placeholder="Description">{{old('content')}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="more_info">More Info</label>
                                                    <div class="col-md-9">
                                                        <textarea id="more_info" name="more_info" class="form-control ckeditor" placeholder="More Info">{{old('more_info')}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="new_date">New Till Date</label>
                                                    <div class="col-md-9">
                                                        <input type="date" id="new_date" name="new_date" class="form-control" placeholder="" value="{{old('new_date')}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Featured Product</label>
                                                    <div class="col-md-9">
                                                        <label class="radio-inline" for="featured_product_radio2">
                                                            <input type="radio" id="featured_product_radio2" name="is_featured" value="0" checked> No
                                                        </label>
                                                        <label class="radio-inline" for="featured_product_radio1">
                                                            <input type="radio" id="featured_product_radio1" name="is_featured" value="1" @if(old('is_featured') == '1') checked @endif> Yes
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="display_order">Display Order</label>
                                                    <div class="col-md-9">
                                                        <input type="number" id="display_order" name="display_order" class="form-control" placeholder="1.00" value="{{old('display_order')}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Display Status</label>
                                                    <div class="col-md-9">
                                                        <label class="radio-inline" for="display_status_radio1">
                                                            <input type="radio" id="display_status_radio1" name="display_status" value="1" checked> Active
                                                        </label>
                                                        <label class="radio-inline" for="display_status_radio2">
                                                            <input type="radio" id="display_status_radio2" name="display_status" value="0" @if(old('is_featured') == '0') checked @endif> Inactive
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="manufacturer_id">Manufacturer</label>
                                                    <div class="col-md-9">
                                                        <select name="manufacturer_id" id="manufacturer_id"
                                                            class="form-control">
                                                            <option value="">Select</option>
                                                            @if(!empty($allManufacturer))
                                                                @foreach($allManufacturer as $manufacturer)
                                                                    <option value="{{$manufacturer->id}}">{{$manufacturer->name}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="category_id">Category</label>
                                                    <div class="col-md-9">
                                                        <select name="category_id[]" id="category_id11" class="form-control selectpicker" multiple  data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
                                                            <!-- <option value="">Select</option> -->
                                                            @if(!empty($allCategory))
                                                                @foreach($allCategory as $category)
                                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                                    @if(count($category->subcategory))
                                                                        @include('admin/subCategoriesProOptions',['subcategories' => $category->subcategory,'parent'=>$category->name,'selectedCat'=>array()])
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <!-- <div class="form-group">
                                                    <label class="col-md-3 control-label" for="child_category_id">Child Category</label>
                                                    <div class="col-md-9">
                                                        <select name="child_category_id" id="child_category_id" class="form-control">
                                                            <option value="">Select</option>
                                                        </select>
                                                    </div>
                                                </div> -->
                                            </div>
                                            <!-- GENERAL TAB END -->

                                            <!-- PRICE TAB START -->
                                            <div class="tab-pane" id="price">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="price">Price</label>
                                                    <div class="col-md-9">
                                                        <input type="number" id="price" name="price" step="0.01" class="form-control" required placeholder="Price" value="{{old('price')}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="special_price">Special Price</label>
                                                    <div class="col-md-9">
                                                        <input type="number" step="0.01" id="special_price" name="special_price" class="form-control" placeholder="Special Price" value="{{old('special_price')}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- PRICE TAB END -->

                                            <!-- INVENTORY TAB START -->
                                            <div class="tab-pane" id="inventory">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="inventory_management">Inventory Management</label>
                                                    <div class="col-md-9">
                                                        <select name="inventory_management" id="inventory_management" class="form-control">
                                                            <option value="0">Don't Track Inventory</option>
                                                            <option value="1">Track Inventory</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="trackinventory" style="display:none;">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="qty">Qty</label>
                                                        <div class="col-md-9">
                                                            <input type="number" id="qty" name="qty" class="form-control" placeholder="Qty" value="{{old('qty')}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="stock_availability">Stock Availability</label>
                                                        <div class="col-md-9">
                                                            <select name="stock_availability" id="stock_availability" class="form-control">
                                                                <option value="1">In Stock</option>
                                                                <option value="0">Out of Stock</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- INVENTORY TAB END -->

                                            <!-- IMAGES TAB START -->
                                            <div class="tab-pane" id="images">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="image-input">Base Image</label>
                                                    <div class="col-md-9">
                                                        <input type="file" id="image-input" name="image" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="additional_images">Additional Images</label>
                                                    <div class="col-md-9">
                                                        <input type="file" id="additional_images" name="additional_images[]" class="form-control" multiple>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- IMAGES TAB START -->
                                            <!-- ATTRIBUTES TAB START -->
                                            <div class="tab-pane" id="attributes">
                                                <h3 class="tab-content-title">Attributes</h3>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="variation_attributes">Variation Attributes</label>
                                                    <div class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-4 attrsec">
                                                                <select name="variation_attributes" id="variation_attributes" class="form-control">
                                                                    <option value="">Select Attribute</option>
                                                                    @if(!empty($allAttributeSet))
                                                                        @foreach($allAttributeSet as $singleAttrSet)
                                                                            @php
                                                                            $allAttrs = (isset($attrArray[$singleAttrSet->id]))?$attrArray[$singleAttrSet->id]:array();
                                                                            @endphp
                                                                            <optgroup label="{{$singleAttrSet->name}}">
                                                                                @if(is_array($allAttrs) && !empty($allAttrs))
                                                                                    @foreach($allAttrs as $singleAttr)
                                                                                        <option value="{{$singleAttr['id']}}">{{$singleAttr['name']}}</option>
                                                                                    @endforeach
                                                                                @endif
                                                                            </optgroup>
                                                                        @endforeach
                                                                    @endif                                
                                                                </select>
                                                            </div>
                                                            <div class="col-md-8 variation_attribute_value">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- ATTRIBUTES TAB END -->
                                            <!-- RELATED TAB START -->
                                            <div class="tab-pane" id="related">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="related_products">Related Products</label>
                                                    <div class="col-md-9">
                                                        <select id="related_products" name="related_products[]" class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
                                                            @if(!empty($allAvailableProducts))
                                                                @foreach($allAvailableProducts as $singleAvailItem)
                                                                    <option value="{{$singleAvailItem->id}}">{{$singleAvailItem->name}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- RELATED TAB END -->
                                            <!-- RELATED TAB START -->
                                            <div class="tab-pane" id="seo">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="meta_title">Meta
                                                        Title</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="meta_title" name="meta_title" class="form-control" placeholder="Meta Title"  value="{{old('meta_title')}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="meta_key">Meta
                                                        Keywords</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="meta_key" name="meta_key" class="form-control" placeholder="Meta Keywords" value="{{old('meta_key')}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="meta_desc">Meta Description</label>
                                                    <div class="col-md-9">
                                                        <textarea id="meta_desc" name="meta_desc" rows="4" class="form-control" placeholder="Meta Description">{{old('meta_desc')}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- RELATED TAB END -->
                                            @if(!empty($allLanguages) && count($allLanguages)>0)
                                                @foreach($allLanguages as $singleLanguage)
                                                <input type="hidden" name="language[]" value="{{$singleLanguage->code}}">
                                                <div class="tab-pane" id="language_{{$singleLanguage->code}}">
                                                    <h2>{{$singleLanguage->name}}</h2>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="name_{{$singleLanguage->code}}">Title</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="name_{{$singleLanguage->code}}" name="name_{{$singleLanguage->code}}" class="form-control" placeholder="Title">
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="short_description_{{$singleLanguage->code}}">Short Description</label>
                                                        <div class="col-md-9">
                                                            <textarea id="short_description_{{$singleLanguage->code}}" name="short_description_{{$singleLanguage->code}}" rows="2" class="form-control" placeholder="Short Description"></textarea>
                                                        </div>
                                                    </div>  
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="content_{{$singleLanguage->code}}">Description</label>
                                                        <div class="col-md-9">
                                                            <textarea id="content_{{$singleLanguage->code}}" name="content_{{$singleLanguage->code}}" rows="2" class="form-control ckeditor" placeholder="Description"></textarea>
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="more_info_{{$singleLanguage->code}}">More Info</label>
                                                        <div class="col-md-9">
                                                            <textarea id="more_info_{{$singleLanguage->code}}" name="more_info_{{$singleLanguage->code}}" rows="2" class="form-control ckeditor" placeholder="More Info"></textarea>
                                                        </div>
                                                    </div>              
                                                </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i>Submit</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i>Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
    <!-- END Form Example with Blocks in the Grid -->
</div>
<!-- END Page Content -->
<div style="display: none;">
    @if(!empty($allAttributes))
        @foreach($allAttributes as $sigAttr)
        <span id="attributeval__{{$sigAttr->id}}">
            {{$sigAttr->values}}
        </span>
        @endforeach
    @endif

    @if(!empty($attrValuesArray))
        @foreach($attrValuesArray as $key=> $sigAttrVal)
        <span id="singleattributeval__{{$key}}">
            @php
            $valStr = '';
            $xx=0;
            @endphp
            @if(!empty($sigAttrVal))
                @foreach($sigAttrVal as $val)                    
                    @php
                    if($xx==0){
                        $valStr .= $val['id'].'=='.$val['name'];
                    }else{
                        $valStr .= '##~~##'.$val['id'].'=='.$val['name'];
                    }
                    $xx++;                        
                    @endphp
                @endforeach
            @endif
            {{$valStr}}
        </span>
        @endforeach
    @endif
</div>
@endsection