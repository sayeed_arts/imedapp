@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Add Slide
                    <span><a href="{{url('admin/slider')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/slider')}}"><i class="fa fa-table"></i> Slider</a></li>
            <li>Add Slide</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/slider/save')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        {{ csrf_field() }}
                        <div class="nav-tab-section">
                            <div class="row" style="min-height:300px;">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <ul class="nav nav-tabs tabs-left">
                                                <li class="active"><a href="#home" data-toggle="tab">General</a></li>
                                                @if(!empty($allLanguages) && count($allLanguages)>0)
                                                    @foreach($allLanguages as $singleLanguage)
                                                        <li><a href="#language_{{$singleLanguage->code}}" data-toggle="tab">{{$singleLanguage->name}}</a></li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="tab-content">
                                                <!-- GENERAL TAB START -->
                                                <div class="tab-pane active" id="home">
                                                    <h2>English</h2>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="name">Caption</label>
                                                        <div class="col-md-9">
                                                            <textarea id="name" name="name" rows="2" class="form-control" placeholder="Caption"></textarea>
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="content">Caption 2</label>
                                                        <div class="col-md-9">
                                                            <textarea id="content" name="content" rows="2" class="form-control" placeholder="Caption 2"></textarea>
                                                        </div>
                                                    </div>                        
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="image-input">Slide</label>
                                                        <div class="col-md-9">
                                                            <input type="file" id="image-input" name="image" class="form-control" required>
                                                        </div>
                                                    </div>  
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="button_text">Button Text</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="button_text" name="button_text" class="form-control" placeholder="Shop Now" value="">
                                                        </div>
                                                    </div>                    
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="button_link">Button Link</label>
                                                        <div class="col-md-9">
                                                            <input type="url" id="button_link" name="button_link" class="form-control" placeholder="https://www.google.com" value="">
                                                        </div>
                                                    </div>                            
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="display_order">Display Order</label>
                                                        <div class="col-md-9">
                                                            <input type="number" id="display_order" name="display_order" class="form-control" placeholder="1.00">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Display Status</label>
                                                        <div class="col-md-9">
                                                            <label class="radio-inline" for="display_status_radio1">
                                                                <input type="radio" id="display_status_radio1" name="display_status" value="1" checked> Active
                                                            </label>
                                                            <label class="radio-inline" for="display_status_radio2">
                                                                <input type="radio" id="display_status_radio2" name="display_status" value="0"> Inactive
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if(!empty($allLanguages) && count($allLanguages)>0)
                                                    @foreach($allLanguages as $singleLanguage)
                                                    <input type="hidden" name="language[]" value="{{$singleLanguage->code}}">
                                                    <div class="tab-pane" id="language_{{$singleLanguage->code}}">
                                                        <h2>{{$singleLanguage->name}}</h2>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label" for="name_{{$singleLanguage->code}}">Caption</label>
                                                            <div class="col-md-9">
                                                                <textarea id="name_{{$singleLanguage->code}}" name="name_{{$singleLanguage->code}}" rows="2" class="form-control" placeholder="Caption"></textarea>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label" for="content_{{$singleLanguage->code}}">Caption 2</label>
                                                            <div class="col-md-9">
                                                                <textarea id="content_{{$singleLanguage->code}}" name="content_{{$singleLanguage->code}}" rows="2" class="form-control" placeholder="Caption 2"></textarea>
                                                            </div>
                                                        </div>  
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label" for="button_text_{{$singleLanguage->code}}">Button Text</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="button_text_{{$singleLanguage->code}}" name="button_text_{{$singleLanguage->code}}" class="form-control" placeholder="Shop Now" value="">
                                                            </div>
                                                        </div>  
                                                    </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection