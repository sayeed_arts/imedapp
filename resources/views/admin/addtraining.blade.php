@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Add New
                    <span><a href="{{url('admin/trainings')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/trainings')}}"><i class="fa fa-table"></i> Service Trainings</a></li>
            <li>Add New</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Content -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <form action="{{url('admin/trainings/save')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="vendor_id">Vendor</label>
                            <div class="col-md-9">
                                <select name="vendor_id" id="vendor_id"
                                    class="form-control">
                                    <option value="">Select</option>
                                    @if(!empty($vendorlist))
                                        @foreach($vendorlist as $key=>$val)
                                            <option value="{{$key}}">{{$val}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Title</label>
                            <div class="col-md-9">
                                <input type="text" id="name" name="name" class="form-control" required placeholder="Title" value="{{old('name')}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="content">Description</label>
                            <div class="col-md-9">
                                <textarea id="content" name="content" rows="4" class="form-control ckeditor" placeholder="Description" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="display_order">Display Order</label>
                            <div class="col-md-9">
                                <input type="number" id="display_order" name="display_order" class="form-control" placeholder="1.00" required value="{{old('display_order')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Display Status</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="display_status_radio1">
                                    <input type="radio" id="display_status_radio1" name="display_status" value="1" checked> Active
                                </label>
                                <label class="radio-inline" for="display_status_radio2">
                                    <input type="radio" id="display_status_radio2" name="display_status" value="0"> Inactive
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Start Date</label>
                            <div class="col-md-8">
                                <input type="text" id="start_date" name="start_date" class="form-control input-datepicker" required placeholder="Enter Start Date" value="{{old('start_date')}}" tabindex="1" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">End Date</label>
                            <div class="col-md-8">
                                <input type="text" id="closing_date" name="closing_date" class="form-control input-datepicker" required placeholder="Enter End Date" value="{{old('closing_date')}}" tabindex="1" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="category_id11">Category</label>
                            <div class="col-md-9">
                                <select name="category_id[]" id="category_id11" class="form-control selectpicker" multiple  data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">    
                                    <option value="0">Category</option>
                                    @if(!empty($allCategories))
                                        @foreach($allCategories as $singleCategory)
                                            <option value="{{$singleCategory->id}}">{{$singleCategory->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="location">Location</label>
                            <div class="col-md-9">
                                <input type="text" id="location" name="location" class="form-control" required placeholder="Location" value="{{old('location')}}">
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection