@extends('layouts.adminapp')

@section('content')
    @php
    $current_user = Auth::user();
    @endphp
    <!-- Page content -->
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Services Requests
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Services Requests</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Hospital/Company</th>
                            <th class="text-center">Full Name</th>
                            <th class="text-center">Phone</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">PO</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>    
                                    <td class="text-center">{{ $singleData->hospital_company }}</td>
                                    <td class="text-center">{{ $singleData->name }}</td>
                                    <td class="text-center">{{ $singleData->phone }}</td>
                                    <td class="text-center">{{ $singleData->email }}</td>
                                    <td class="text-center">{{ $singleData->PO }}</td>                                    
                                    <td class="text-center">
                                        <div class="btn-group">                                            
                                            @if(isAllowed($current_user->id,$current_user->allowed_sections,'96'))
                                                <a href="{{url('admin/services-request/edit/'.$singleData->id)}}" data-toggle="tooltip" title="View" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                            @endif
                                            @if(isAllowed($current_user->id,$current_user->allowed_sections,'97'))
                                                <form action="{{ url('/admin/services-request/delete/'.$singleData->id)}}" method="POST" class="delete-form">
                                                {{ csrf_field() }}
                                                    <input type="submit" name="delete" value="X" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete">
                                                </form>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="6" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->
@endsection