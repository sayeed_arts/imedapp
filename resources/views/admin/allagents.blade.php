@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Admin Users 
                    @if(isAllowed($current_user->id,$current_user->allowed_sections,'35'))
                        <span><a href="{{url('admin/agents/add')}}" class="btn btn-default">Add New</a></span>
                    @endif
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Admin Users</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Name</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>
                                    <td class="text-center">{{ $singleData->name }} {{ $singleData->last_name }}</td>                         
                                    <td class="text-center">{{ $singleData->email }}</td>
                                    <td class="text-center">
                                        @if($singleData->verified=='1')
                                            <span class="label label-success">Verified</span>
                                        @elseif($singleData->verified=='0')
                                            <span class="label label-info">Unverified</span>
                                        @endif                                        
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            @if(isAllowed($current_user->id,$current_user->allowed_sections,'36'))
                                            <a href="{{url('admin/agents/edit/'.$singleData->id)}}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                            @endif
                                            @if(isAllowed($current_user->id,$current_user->allowed_sections,'37'))
                                            <form action="{{ url('/admin/agents/delete/'.$singleData->id)}}" method="POST" class="delete-form">
                                                {{ csrf_field() }}
                                                <input type="submit" name="delete" value="X" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete">
                                            </form>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="4" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->
@endsection