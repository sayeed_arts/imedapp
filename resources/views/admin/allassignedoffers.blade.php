@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>Assigned Offers [{{$catData->name}}]</h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/categories')}}"><i class="fa fa-table"></i> {{$catData->name}}</a></li>
            <li>Assigned Offers</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}

            <form action="{{url('admin/assigned-offers/update')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                {{ csrf_field() }}
                <input type="hidden" name="currid" value="{{$catData->id}}">
                <input type="hidden" name="editcurrid" value="{{(isset($assignedOffer->id))?$assignedOffer->id:''}}">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="offer_id">Offer</label>
                    <div class="col-md-9">
                        <select id="offer_id" name="offer_id" class="form-control" required >
                            <option value="">Select Offer</option>
                            @if(!empty($availableOffers))
                                @foreach($availableOffers as $availOffer)
                                    <option value="{{$availOffer->id}}" @if(isset($assignedOffer->offer_id) && $assignedOffer->offer_id==$availOffer->id) selected @endif>{{$availOffer->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div> 
                                        
                <div class="form-group">
                    <label class="col-md-3 control-label" for="display_order">Display Order</label>
                    <div class="col-md-9">
                        <input type="number" id="display_order" name="display_order" class="form-control" placeholder="1.00" required value="{{(isset($assignedOffer->display_order))?$assignedOffer->display_order:''}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Display Status</label>
                    <div class="col-md-9">
                        <label class="radio-inline" for="display_status_radio1">
                            <input type="radio" id="display_status_radio1" name="display_status" value="1" checked> Active
                        </label>
                        <label class="radio-inline" for="display_status_radio2">
                            <input type="radio" id="display_status_radio2" name="display_status" value="0" @if(isset($assignedOffer->display_status) && $assignedOffer->display_status==0) checked @endif> Inactive
                        </label>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Offer Name</th>
                            <th class="text-center">Display Order</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>
                                    <td class="text-center">{{ fetchSingleColumn('offers','name',array('id'=>$singleData->offer_id)) }}</td>
                                    <td class="text-center">{{ $singleData->display_order }}</td>
                                    <td class="text-center">
                                        @if($singleData->display_status=='1')
                                            <span class="label label-success">Active</span>
                                        @elseif($singleData->display_status=='0')
                                            <span class="label label-info">InActive</span>
                                        @endif                                        
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{url('admin/assigned-offers/'.$singleData->caregory_id.'/'.$singleData->id)}}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>                                            
                                            <form action="{{ url('/admin/assigned-offers/delete/'.$singleData->caregory_id.'/'.$singleData->id)}}" method="POST" class="delete-form" onsubmit="return confirm('Do you really want to delete this record?');">
                                                {{ csrf_field() }}
                                                <input type="submit" name="delete" value="X" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete">
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="3" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->
@endsection