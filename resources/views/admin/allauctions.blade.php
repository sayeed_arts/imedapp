@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header row">
            <div class="col-md-4 header-section">
                <h1>
                    Auctions
                </h1>
            </div>
            <div class="col-md-8  header-section">
            <a style="float:right;" href="{{url('admin/auctions/add')}}" class="btn btn-primary">Add Auction</a>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Auctions</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Auction Product</th>
                            <th class="text-center">User</th>
                            <th class="text-center">Date Created</th>
                            <th class="text-center">Time Remaining</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>                                     
                                    <td class="text-center">{{ $singleData->name }} {{ $singleData->name }}</td>
                                    <td class="text-center">
                                        <?php
                                            if($singleData->is_admin == '1') echo 'Admin';
                                            else  if($singleData->is_admin == '2') echo 'Vendor';
                                        ?>
                                    </td>
                                    <td class="text-center">{{ $singleData->created_at }}</td>
                                    <td class="text-center">
                                        @if($singleData->auction_awarded != '1')
                                        <p class="countdowntimer" id="auc_<?php echo $singleData->id; ?>"></p>
                                        @endif 
                                    </td>
                                    @if($singleData->auction_awarded == '1')
                                        <td class="text-center active_auc"><a href="javascript:void(0)" class="btn btn-info">Awarded</a></td>
                                    @else
                                        @if($singleData->status == '1')
                                            <td class="text-center active_auc"><a href="javascript:void(0)" attr_auction_id="<?=$singleData->id?>" attr_value="0" class="btn btn-success chnagestatus">Active</a></td>
                                        @else
                                            <td class="text-center"><a href="javascript:void(0)" class="btn btn-danger chnagestatus" attr_value="1">Inactive</a></td>
                                        @endif        
                                    @endif           
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{url('admin/auctions/viewauction/'.$singleData->id)}}" data-toggle="tooltip" title="View Auction"  class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                            @if(isAllowed($current_user->id,$current_user->allowed_sections,'30'))
                                            <a href="{{url('admin/auctions/add/'.$singleData->id)}}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                            @endif
                                            <a href="javascript:void(0)" data-toggle="tooltip" title="Delete Auction" vendor_id="{{$singleData->id}}"  class="deleteauction btn btn-xs btn-default"><i class="fa fa-trash"></i></a>
                                            <a href="{{url('admin/auctions/viewbids/'.$singleData->id)}}" data-toggle="tooltip" title="View Bids" vendor_id="{{$singleData->id}}"  class=" btn btn-xs btn-default">View Bids</a>
                                            <!-- <form action="{{ url('/admin/users/delete/'.$singleData->id)}}" method="POST" class="delete-form" onsubmit="return confirm('Do you really want to delete this record?');">
                                                {{ csrf_field() }}
                                                <input type="submit" name="delete" value="X" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete">
                                            </form> -->
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="3" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->
@endsection