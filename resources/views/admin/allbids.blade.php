@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header row">
            <div class="col-md-4 header-section">
                <h1>
                    Bids
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/auctions')}}"><i class="fa fa-dashboard"></i> Auctions</a></li>
            <li>Bids</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">User</th>
                            <th class="text-center">Bidding Date</th>
                            <th class="text-center">Bid Amount</th>
                            <th class="text-center">Actions</th>
                            <!-- <th class="text-center">Order Status</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>                                     
                                    <td class="text-center">{{ $singleData->name }} {{ $singleData->name }}</td>
                                    <td class="text-center">{{ $singleData->created_date }}</td>
                                    <td class="text-center">{{ $singleData->bid_amount }}</td>              
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <?php 
                                            if($singleData->status == '1'){ 
                                                echo 'Awarded'; 
                                            }
                                            ?>
                                            <?php if(!$auction_awarded){?>
                                                <a href="{{url('admin/auction/award/'.$singleData->id)}}" data-toggle="tooltip" title="View Bids" vendor_id="{{$singleData->id}}"  class=" btn btn-xs btn-default">Award Auction</a>
                                            <?php } ?>
                                        </div>
                                    </td>
                                    <!-- <td class="text-center">
                                        <?php 
                                        if($singleData->status == '1'){
                                        ?>
                                        <form action="{{url('admin/bidorder/update')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                                            <input type="hidden" name="bidid" value="{{$singleData->id}}">
                                            <input type="hidden" name="auction_id" value="{{$singleData->auction_id}}">
                                            {{ csrf_field() }}
                                            <select name="order_status" id="order_status" class="form-control" style="margin-top: 15px;" onchange="javascript:this.form.submit();">
                                                <option value="0" <?php if($singleData->order_status == '0'){ echo "selected"; }?> >Received</option>
                                                <option value="3" <?php if($singleData->order_status == '3'){ echo "selected"; }?> >Processing</option>
                                                <option value="2" <?php if($singleData->order_status == '2'){ echo "selected"; }?> >Voided </option>
                                                <option value="1" <?php if($singleData->order_status == '1'){ echo "selected"; }?> >Shipped</option>
                                                <option value="4" <?php if($singleData->order_status == '4'){ echo "selected"; }?> >Completed</option>
                                            </select>
                                        </form>
                                        <?php } ?>      
                                    </td> -->
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="4" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->
@endsection