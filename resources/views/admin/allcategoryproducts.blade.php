@extends('layouts.adminapp')

@section('content')
@php
$current_user = Auth::user();
@endphp
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                Products [{{$record->name}}]
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('admin/categories')}}"><i class="fa fa-cubes"></i> Categories</a></li>
        <li>Product</li>
    </ul>
    <!-- END Datatables Header -->
    <!-- Datatables Content -->
    <div class="block full">
        <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
        @if (Session::has('message'))
        {!! successMesaage(Session::get('message')) !!}
        @endif
        {!! validationError($errors) !!}
        <div class="table-responsive">            
            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">Title</th>
                        <th class="text-center">Type</th>
                        <th class="text-center">Is Featured</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if($allRecords)
                    @foreach ($allRecords as $singleData)
                    <tr>
                        <td class="text-center">{{ $singleData->name }}</td>
                        <td class="text-center">
                            @if($singleData->product_type=='1')
                            For Sale
                            @elseif($singleData->product_type=='0')
                            For Rent
                            @endif
                        </td>
                        <td class="text-center">
                            @if($singleData->is_featured=='1')
                            Yes
                            @elseif($singleData->is_featured!='1')
                            No
                            @endif
                        </td>
                        <td class="text-center">
                            @if($singleData->display_status=='1')
                            <span class="label label-success">Active</span>
                            @elseif($singleData->display_status=='0')
                            <span class="label label-info">InActive</span>
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'55'))
                                <a href="{{url('admin/products/edit/'.$singleData->id)}}" data-toggle="tooltip"
                                    title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'56'))
                                <!-- <form action="{{ url('/admin/products/delete/'.$singleData->id)}}" method="POST" class="delete-form"> {{ csrf_field() }}
                                                <input type="submit" name="delete" value="X" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete">
                                            </form> -->
                                @endif

                                <a href="{{url('product/'.$singleData->slug)}}" data-toggle="tooltip" title="View" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-eye"></i></a>
                                <!-- <a href="{{url('product/'.$singleData->slug)}}" target="_blank" data-toggle="tooltip"
                                    title="View" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a> -->
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="3" class="text-center">No Record Available to display.</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>        
    </div>
    <!-- END Datatables Content -->
</div>
<!-- END Page Content -->
@endsection