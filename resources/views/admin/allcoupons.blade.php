@extends('layouts.adminapp')

@section('content')
    @php
    $current_user = Auth::user();
    @endphp
    <!-- Page content -->
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Coupons 
                    @if(isAllowed($current_user->id,$current_user->allowed_sections,'70'))
                    <span><a href="{{url('admin/coupons/add')}}" class="btn btn-default">Add New</a></span>
                    @endif
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Coupons</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Name</th>
                            <th class="text-center">Code</th>
                            <th class="text-center">Type</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>    
                                    <td class="text-center">{{ $singleData->name }}</td>
                                    <td class="text-center">{{ $singleData->code }}</td>
                                    <td class="text-center">
                                        @if($singleData->type=='0')
                                            Fixed
                                        @elseif($singleData->type=='1')
                                            Percent
                                        @endif                                        
                                    </td>
                                    <td class="text-center">
                                        @if($singleData->status=='1')
                                            <span class="label label-success">Active</span>
                                        @elseif($singleData->status=='0')
                                            <span class="label label-info">InActive</span>
                                        @endif                                        
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            
                                            @if(isAllowed($current_user->id,$current_user->allowed_sections,'71'))
                                            <a href="{{url('admin/coupons/edit/'.$singleData->id)}}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                            @endif
                                            @if(isAllowed($current_user->id,$current_user->allowed_sections,'72'))
                                            <form action="{{ url('/admin/coupons/delete/'.$singleData->id)}}" method="POST" class="delete-form">
                                            {{ csrf_field() }}
                                                <input type="submit" name="delete" value="X" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete">
                                            </form>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="5" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->
@endsection