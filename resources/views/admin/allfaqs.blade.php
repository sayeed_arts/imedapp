@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    FAQ 
                    @if(isAllowed($current_user->id,$current_user->allowed_sections,'17'))
                        <span><a href="{{url('admin/faq/add')}}" class="btn btn-default">Add New</a></span>
                    @endif
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>FAQ</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            <!-- <div class="block-title">
                <h2><strong>All FAQ's</strong></h2>
            </div> -->
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Question</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>
                                    <td class="text-center">{{ $singleData->name }}</td>
                                    <td class="text-center">
                                        @if($singleData->display_status=='1')
                                            <span class="label label-success">Active</span>
                                        @elseif($singleData->display_status=='0')
                                            <span class="label label-info">InActive</span>
                                        @endif                                        
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            @if(isAllowed($current_user->id,$current_user->allowed_sections,'18'))
                                            <a href="{{url('admin/faq/edit/'.$singleData->id)}}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                            @endif
                                            @if(isAllowed($current_user->id,$current_user->allowed_sections,'19'))
                                            <form action="{{ url('/admin/faq/delete/'.$singleData->id)}}" method="POST" class="delete-form">
                                                {{ csrf_field() }}
                                                <input type="submit" name="delete" value="X" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete">
                                            </form>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="3" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->
@endsection