@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Mailing Lists Records
                    @if(isAllowed($current_user->id,$current_user->allowed_sections,'44'))
                        <span><a href="{{url('admin/listrecords/add/'.$list_id)}}" class="btn btn-default">Add New</a></span>
                    @endif
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/mailinglist')}}"><i class="fa fa-table"></i> Mailing Lists</a></li>
            <li>Mailing Lists Records</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">            
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="row1">
                <div classs="col-md-12">
                    <form action="{{url('admin/listrecords/import')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        {{ csrf_field() }}
                        <input type="hidden" name="list_id" value="{{$list_id}}">
                        <div class="form-group form-actions">
                            <div class="col-md-5 col-md-offset-1">
                                <input type="file" name="importlistrecords" class="form-control">
                                <span><a href="{{url('importlist.csv')}}">Download Format</a></span>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Import</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Name</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>
                                    <td class="text-center">{{ $singleData->name }}</td>
                                    <td class="text-center">{{ $singleData->email_address }}</td>
                                    <td class="text-center">
                                        @if($singleData->status=='1')
                                            <span class="label label-success">Active</span>
                                        @elseif($singleData->status=='0')
                                            <span class="label label-info">InActive</span>
                                        @endif                                        
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            @if(isAllowed($current_user->id,$current_user->allowed_sections,'45'))
                                                <a href="{{url('admin/listrecords/edit/'.$singleData->id)}}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                            @endif
                                            @if(isAllowed($current_user->id,$current_user->allowed_sections,'46'))
                                                <form action="{{ url('/admin/listrecords/delete/'.$singleData->id)}}" method="POST" class="delete-form" onsubmit="return confirm('Do you really want to delete this record?');">
                                                    {{ csrf_field() }}
                                                    <input type="submit" name="delete" value="X" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete">
                                                </form>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="4" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->
@endsection