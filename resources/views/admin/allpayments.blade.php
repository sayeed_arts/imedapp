@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Payments
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Payments</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            <!-- <div class="block-title">
                <h2><strong>All Newsletters</strong></h2>
            </div> -->
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Job</th>
                            <th class="text-center">Care Seeker</th>
                            <th class="text-center">Care Giver</th>
                            <th class="text-center">Amount</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Status</th>
                            <!-- <th class="text-center">Actions</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                @php
                                $giver_data = fetchSingleRow('users',array('name','last_name'),array('id'=>$singleData->giver_id));
                                @endphp
                                <tr>
                                    <td class="text-center">{{ $singleData->job_title }}</td>
                                    <td class="text-center">{{ $singleData->name.' '.$singleData->last_name }}</td>
                                    <td class="text-center">{{ $giver_data->name.' '.$giver_data->last_name }}</td>
                                    <td class="text-center">{{ $singleData->amount }}</td>
                                    <td class="text-center">{{ $singleData->updated_at }}</td>
                                    <td class="text-center">
                                        @if($singleData->status=='1')
                                            <span class="label label-default">Escrowed</span>
                                        @elseif($singleData->status=='0')
                                            <span class="label label-info">Pending</span>
                                        @elseif($singleData->status=='2')
                                            <span class="label label-info">Added Into Wallet</span>
                                        @elseif($singleData->status=='3')
                                            <span class="label label-info">Refunded</span>
                                        @elseif($singleData->status=='4')
                                            <span class="label label-success">Paid</span>
                                        @endif                                        
                                    </td>
                                    <!-- <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{url('admin/payments/edit/'.$singleData->id)}}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                        </div>
                                    </td> -->
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="3" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->
@endsection