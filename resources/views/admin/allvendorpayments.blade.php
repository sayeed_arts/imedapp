@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header row">
            <div class="col-md-4 header-section">
                <h1>
                    Vendors
                </h1>
            </div>
           </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Vendors</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Title</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Total Order Amount</th>
                            <th class="text-center">Total Payable Amount</th>
                            <th class="text-center">Total Amount Paid</th>
                            <th class="text-center">Pending Payment</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>                                    
                                    <td class="text-center">{{ $singleData->name }} {{ $singleData->last_name }}</td>  
                                    <td class="text-center">
                                        @if($singleData->is_approved=='1')
                                            <span class="label label-success">Active</span>
                                        @elseif($singleData->is_approved=='0')
                                            <span class="label label-info">InActive</span>
                                        @endif                                        
                                    </td>
                                    <td>{!!  vendorpayment($singleData->id) !!}</td>
                                    <td>{!!  vendorcommission($singleData->id) !!}</td>
                                    <td>{!!  vendorpaymentsdone($singleData->id) !!}</td>
                                    <td>{!! vendorcommission($singleData->id) - vendorpaymentsdone($singleData->id)!!}</td>
                                    <td class="text-center">
                                        <div class="btn-group">

                                            <a href="{{url('admin/vendors/vendorpaymentlogs/'.$singleData->id)}}" data-toggle="tooltip" title="View Vendor Payments" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>



                                            <?php if(vendorpaymentsdone($singleData->id)<vendorcommission($singleData->id)){?>
                                            <a href="javascript:void(0)" vendor_id="<?=$singleData->id?>" vendorcom="<?=$singleData->commission?>" data-toggle="modal" data-target="#exampleModal" class="setcommission btn btn-sm btn-default"><i data-toggle="tooltip" title="Pay to Vendor" class="fa fa-dollar" aria-hidden="true"></i></a>
                                            <?php } ?>        





                                            <!-- <form action="{{ url('/admin/users/delete/'.$singleData->id)}}" method="POST" class="delete-form" onsubmit="return confirm('Do you really want to delete this record?');">
                                                {{ csrf_field() }}
                                                <input type="submit" name="delete" value="X" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete">
                                            </form> -->
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="3" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->


    <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pay Vendor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       <form action="{{url('admin/vendors/payvendor')}}" method="post">
      @csrf
      <div class="modal-body">
            <div class="form-group">
                <label>Transaction ID</label>
               <input name="transaction_id" class="form-control" id="transaction_id" placeholder="Enter Transaction ID" value="" required="required">
               <input type="hidden" id="vendorid" name="vendorid" value="">
            </div>
          

            <div class="form-group">
                <label>Type</label>
               <select name="type" class="form-control" id="type" required="required">
                   <option value="">Select Type</option>
                   <option value="1">Wired Bank Transfer</option>
                   <option value="2">Check</option>
                   <option value="3">Cash</option>

               </select>
            </div>


            <div class="form-group">
                <label>Amount</label>
               <input name="amount" class="form-control" id="amount" placeholder="Enter Amount" value="" required="required">
            </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
       </form>
    </div>
  </div>
</div>
@endsection