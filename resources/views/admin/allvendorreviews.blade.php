@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header row">
            <div class="col-md-4 header-section">
                <h1>
                    Vendor Ratings
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Vendors</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Store</th>
                            <th class="text-center">User</th>
                            <th class="text-center">Rating</th>
                            <th class="text-center">Review</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Created Date</th>
                           </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>                                    
                                    
                                    <td class="text-center">{{ $singleData->Store }}</td>
                                    

                                    <td class="text-center">{{ $singleData->name }} {{ $singleData->last_name }}</td>
                            
                                
                                    <td class="text-center">{{ $singleData->rating }}</td>

                                    <td class="text-center">{{ $singleData->description }}</td>
                                
                                @if($singleData->status == '1')
                                    <td class="text-center active_auc"><a href="javascript:void(0)" attr_auction_id="<?=$singleData->id?>" attr_value="0" class="btn btn-success chnagestatusven">Active</a></td>
                                @else
                                    <td class="text-center"><a href="javascript:void(0)" class="btn btn-danger chnagestatusven" attr_value="1" attr_auction_id="<?=$singleData->id?>">Inactive</a></td>

                                @endif
                                    <td class="text-center">{{ date('m/d/Y H:i:s',strtotime($singleData->added_at)) }}</td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="3" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->
@endsection