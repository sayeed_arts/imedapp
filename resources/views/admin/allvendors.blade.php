@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <div class="col-md-12">
            <div class="col-md-4">
                <form id="statusfilterform" action="{{url('admin/vendors')}}" method="get">
                <select  name="is_approved" id="is_approved" class="form-control" onchange="filterByStatus();">
                    <option value="">Filter By Status</option>
                    <option value="">All</option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
                <form>
            </div>
        </div>
        <!-- Datatables Header -->
        <div class="content-header row">
            <div class="col-md-4 header-section">
                <h1>
                    Vendors
                </h1>
            </div>
            <div class="col-md-8  header-section">
            <a style="float:right;" href="{{url('admin/vendors/add')}}" class="btn btn-primary">Add Vendor</a>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Vendors</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Title</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>                                    
                                    
                                    <td class="text-center">{{ $singleData->name }} {{ $singleData->last_name }}</td>
                                    
                                                            
                                    <td class="text-center">
                                        @if($singleData->is_approved=='1')
                                            <span class="label label-success">Active</span>
                                        @elseif($singleData->is_approved=='0')
                                            <span class="label label-info">InActive</span>
                                        @endif                                        
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">

                                            <!-- <form action="{{ url('/admin/users/delete/'.$singleData->id)}}" method="POST" class="delete-form" onsubmit="return confirm('Do you really want to delete this record?');">
                                                {{ csrf_field() }}
                                                <input type="submit" name="delete" value="X" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete">
                                            </form> -->


                                            <a href="{{url('admin/vendorproducts/'.$singleData->id)}}" data-toggle="tooltip" title="View Vendor Products" class="btn btn-sm btn-default"><i class="fa fa-cubes"></i></a>



                                            <a href="{{url('admin/vendors/viewvendor/'.$singleData->id)}}" data-toggle="tooltip" title="View Vendor" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>                                    

                                            @if(isAllowed($current_user->id,$current_user->allowed_sections,'30'))
                                            <a href="{{url('admin/vendors/add/'.$singleData->id)}}" data-toggle="tooltip" title="Edit" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></a>
                                            @endif


                                            <a href="javascript:void(0)" vendor_id="<?=$singleData->id?>"   vendorcom="<?=$singleData->commission?>" data-toggle="modal" data-target="#exampleModal" class="setcommission btn btn-sm btn-default"><i data-toggle="tooltip" title="Set Vendor Commision" class="fa fa-percent" aria-hidden="true"></i></a>


                                            <a class="btn btn-sm btn-default deletevendor" href="javascript:void(0)" data-toggle="tooltip" title="Delete Vendor" vendor_id="{{$singleData->id}}"><i class="fa fa-trash" ></i></a>




                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="3" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->


    <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Set Commission</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       <form action="{{url('admin/vendors/setcommision')}}" method="post">
      @csrf
      <div class="modal-body">
           <label>Commission</label>
           <input name="commission" class="form-control" id="commission" placeholder="Enter Commission" value="" required="required">
           <input type="hidden" id="vendorid" name="vendorid" value="">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
       </form>
    </div>
  </div>
</div>
<script type="text/javascript">
function filterByStatus(){
    document.getElementById('statusfilterform').submit();
}
</script>
@endsection