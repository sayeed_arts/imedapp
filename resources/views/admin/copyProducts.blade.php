@extends('layouts.adminapp')

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Forms General Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                Copy Product
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Copy Product</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Content -->
                @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}
                @endif
                @if (Session::has('errors'))
                { !!session('errors')->first('message1') !!}
                @endif
                {!! validationError($errors) !!}
                <form action="{{url('admin/products/copy-save')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                    {{ csrf_field() }}
                    <div class="nav-tab-section1">
                        <div class="row">
                            <div class="col-sm-12">                                
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="from_category">From Category</label>
                                    <div class="col-md-9">
                                        <select name="from_category" id="from_category" class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" required>
                                            <option value="">Select From Category</option>
                                            @if(!empty($allCategory))
                                                @foreach($allCategory as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @if(count($category->subcategoryactive))
                                                        @include('admin/subCategoriesProOptionsActive',['subcategories' => $category->subcategoryactive,'parent'=>$category->name,'selectedCat'=>array()])
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div> 
                                <div class="form-group" id="div_products" style="display:none;">
                                    <label class="col-md-3 control-label" for="products">Products</label>
                                    <div class="col-md-9" id="ajax_products">
                                        
                                    </div>
                                </div>                             
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="to_category">To Category</label>
                                    <div class="col-md-9">
                                        <select name="to_category" id="to_category" class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" required>
                                            <option value="">Select To Category</option>
                                            @if(!empty($allCategory))
                                                @foreach($allCategory as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @if(count($category->subcategory))
                                                        @include('admin/subCategoriesProOptions',['subcategories' => $category->subcategory,'parent'=>$category->name,'selectedCat'=>array()])
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <input type="submit" class="btn btn-sm btn-primary" name="act1" value="Copy" id="act1" disabled="true" />
                            <input type="submit" class="btn btn-sm btn-primary" name="act2" value="Move" id="act2" disabled="true" />
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i>Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
    <!-- END Form Example with Blocks in the Grid -->
</div>
<!-- END Page Content -->
@endsection