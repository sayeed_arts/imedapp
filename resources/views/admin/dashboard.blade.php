@extends('layouts.adminapp')

@section('content')
<style type="text/css">
.canvasjs-chart-credit {
    display: none;
}
</style>
<div id="page-content">
    @if(isAllowed($current_user->id,$current_user->allowed_sections,'1'))
    <div class="row">
        <div class="col-sm-12">
            <div class="block full">
                <div class="block-title">
                    <h3><strong>Admin dashboard </strong></h3>
                </div>
                <div class="row text-center">
                    <div class="col-sm-6 col-lg-3">
                        <a href="{{url('admin/orders')}}" class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <div class="widget-icon pull-left themed-background-modern animation-fadeIn">
                                    <i class="fa fa-money "></i>
                                </div>
                                <h3 class="widget-content text-left animation-pullDown">
                                    <strong>{{showAdminPrice($total_sales)}}</strong><br>
                                    <small>Total Sales</small>
                                </h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <a href="{{url('admin/orders')}}" class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <div class="widget-icon pull-left themed-background-modern animation-fadeIn">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <h3 class="widget-content text-left animation-pullDown">
                                    <strong>{{$total_orders}}</strong><br>
                                    <small>Total Orders</small>
                                </h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <a href="{{url('admin/products')}}" class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <div class="widget-icon pull-left themed-background-modern animation-fadeIn">
                                    <i class="fa fa-cubes"></i>
                                </div>
                                <h3 class="widget-content text-left animation-pullDown">
                                    <strong>{{$total_products}}</strong><br>
                                    <small>Total Products</small>
                                </h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <a href="{{url('admin/users')}}" class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <div class="widget-icon pull-left themed-background-modern animation-fadeIn">
                                    <i class="fa fa-users"></i>
                                </div>
                                <h3 class="widget-content text-left animation-pullDown">
                                    <strong>{{$total_users}}</strong>
                                    <small>Total Customers</small>
                                </h3>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <div  class="row">
            <div class="col-sm-12">
                <div class="block full">
                    <div class="block-title">
                    <h3> <strong> VENDOR DASHBOARD</strong></h3>
                    </div>
                    
                    
                    <div class="row text-center">
                        <div class="col-sm-6 col-lg-3">
                            <a href="{{url('admin/vendors')}}" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-default animation-fadeIn">
                                        <i class="fa fa-users "></i>
                                    </div>
                                    <h3 class="widget-content text-left animation-pullDown">
                                        <strong>{{$total_vendors}}</strong><br>
                                        <small>Vendors</small>
                                    </h3>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <a href="{{url('admin/products')}}" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-default animation-fadeIn">
                                        <i class="fa fa-cubes"></i>
                                    </div>
                                    <h3 class="widget-content text-left animation-pullDown">
                                        <strong>{{$total_vendor_products}}</strong><br>
                                        <small>Vendor Products</small>
                                    </h3>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <a href="{{url('admin/orders')}}" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-default animation-fadeIn">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                    <h3 class="widget-content text-left animation-pullDown">
                                        <strong>{{$total_vendor_orders}}</strong><br>
                                        <small>Vendor Orders</small>
                                    </h3>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <a href="{{url('admin/enquiries')}}" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-default animation-fadeIn">
                                        <i class="fa fa-envelope"></i>
                                    </div>
                                    <h3 class="widget-content text-left animation-pullDown">
                                        <strong>{{$total_vendor_enquiries}}</strong>
                                        <small>Vendors Enquiries</small>
                                    </h3>
                                </div>
                            </a>
                        </div>
                    </div>



                    <div class="row text-center">
                        <div class="col-sm-6 col-lg-3">
                            <a href="{{url('admin/vendors')}}" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-default animation-fadeIn">
                                        <i class="fa fa-users "></i>
                                    </div>
                                    <h3 class="widget-content text-left animation-pullDown">
                                        <strong>{{$total_vendor_pending}}</strong><br>
                                        <small>Vendors Approval  </small>
                                    </h3>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <a href="{{url('admin/vendors/vendorpayments')}}" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-default animation-fadeIn">
                                        <i class="fa fa-dollar"></i>
                                    </div>
                                    <h3 class="widget-content text-left animation-pullDown">
                                        <strong>{{$total_vendor_pay_count}}</strong><br>
                                        <small>Vendors Payments</small>
                                    </h3>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <a href="{{url('admin/products')}}" class="widget widget-hover-effect1">
                                <div class="widget-simple">
                                    <div class="widget-icon pull-left themed-background-default animation-fadeIn">
                                        <i class="fa fa-cubes"></i>
                                    </div>
                                    <h3 class="widget-content text-left animation-pullDown">
                                        <strong>{{$total_vendor_pending_products}}</strong>
                                        <small> Products Approval </small>
                                    </h3>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="block full">
                <div class="block-title">
                    <h2><strong>Recent</strong> Orders</h2>
                </div>

                <div class="row text-center">
                    <div class="col-sm-6 col-lg-2">
                        <div class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <h3 class="widget-content text-center animation-pullDown">
                                    <strong>Orders Breakup :</strong>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-2">
                        <div class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <h3 class="widget-content text-center animation-pullDown">
                                    <small>Received : </small>
                                    <strong>{{$receivedOrder}}</strong>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2">
                        <div class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <h3 class="widget-content text-center animation-pullDown">
                                    <small>Processing : </small>
                                    <strong>{{$processingOrder}}</strong>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2">
                        <div class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <h3 class="widget-content text-center animation-pullDown">
                                    <small>Voided : </small>
                                    <strong>{{$voidedOrder}}</strong>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2">
                        <div class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <h3 class="widget-content text-center animation-pullDown">
                                    <small>Shipped : </small>
                                    <strong>{{$shippedOrder}}</strong>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2">
                        <div class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <h3 class="widget-content text-center animation-pullDown">
                                    <small>Completed : </small>
                                    <strong>{{$completedOrder}}</strong>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable1" class="table table-vcenter table-condensed table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">Customer</th>
                                <th class="text-center">Order No.</th>
                                <th class="text-center">Order Date</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Current Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($latest_orders)
                            @foreach($latest_orders as $singleData)
                            <tr>
                                <td class="text-center">
                                    {{ $singleData->customer_first_name.' '.$singleData->customer_last_name }}</td>
                                <td class="text-center">{{ $singleData->order_number }}</td>
                                <td class="text-center">{{ displayDateWithTimeMD($singleData->ordered_on) }}</td>
                                <td class="text-center">
                                    @if($singleData->status=='3')
                                    <span class="label label-warning">Canceled</span>
                                    @elseif($singleData->status=='2')
                                    <span class="label label-success">Successfull</span>
                                    @elseif($singleData->status=='1')
                                    <span class="label label-danger">Failed</span>
                                    @elseif($singleData->status=='0')
                                    <span class="label label-info">Pending</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($singleData->status=='2')
                                        @if($singleData->shipping_status=='0')
                                        <span class="label label-info">Received</span>
                                        @elseif($singleData->shipping_status=='1')
                                        <span class="label label-warning">Shipped</span>
                                        @elseif($singleData->shipping_status=='2')
                                        <span class="label label-danger">Voided</span>
                                        @elseif($singleData->shipping_status=='3')
                                        <span class="label label-danger">Processing</span>
                                        @elseif($singleData->shipping_status=='4')
                                        <span class="label label-success">Completed</span>
                                        @endif
                                    @endif
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{url('admin/orders/'.$singleData->order_id)}}" data-toggle="tooltip"
                                            title="Edit" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6" class="text-center">No Record Available to display.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="block full">
                <div class="block-title">
                    <h2><strong>Recent</strong> Users</h2>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable1" class="table table-vcenter table-condensed table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">Title</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($latest_users)
                            @foreach ($latest_users as $singleUser)
                            <tr>
                                <td class="text-center">{{ $singleUser->name }} {{ $singleUser->last_name }}</td>
                                <td class="text-center">
                                    @if($singleUser->is_approved=='1')
                                    <span class="label label-success">Active</span>
                                    @elseif($singleUser->is_approved=='0')
                                    <span class="label label-info">InActive</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        @if(isAllowed($current_user->id,$current_user->allowed_sections,'30'))
                                        <a href="{{url('admin/users/edit/'.$singleUser->id)}}" data-toggle="tooltip"
                                            title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="3" class="text-center">No Record Available to display.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection