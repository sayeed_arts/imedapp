@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Update Profile
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Update Profile</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/updateprofile')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        <input type="hidden" name="old_profile_picture" value="{{$user->profile_picture}}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">First Name</label>
                            <div class="col-md-9">
                                <input type="text" required  id="name" name="name" class="form-control" placeholder="First Name"  value="{{$user->name}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="last_name">Last Name</label>
                            <div class="col-md-9">
                                <input type="text" required  id="last_name" name="last_name" class="form-control" placeholder="Last Name"  value="{{$user->last_name}}">
                            </div>
                        </div>                       
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="image-input">Profile Picture</label>
                            <div class="col-md-9">
                                <input type="file" id="image-input" name="profile_picture" class="form-control">
                                @if(!empty($user->profile_picture))
                                <img src="{{ asset($user->profile_picture)}}" alt="image" height="150" width="150">
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">Email</label>
                            <div class="col-md-9">
                                <input type="email" required  id="email" name="email" class="form-control" placeholder="Email"  value="{{$user->email}}">
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="password">Password</label>
                            <div class="col-md-9">
                                <input type="password" id="password" name="password" class="form-control" placeholder="password"  value="">
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="confirm_password">Confirm Password</label>
                            <div class="col-md-9">
                                <input type="password" id="confirm_password" name="confirm_password" class="form-control" placeholder="Confirm Password"  value="">
                            </div>
                        </div> 
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection