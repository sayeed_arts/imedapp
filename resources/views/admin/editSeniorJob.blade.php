@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Edit Job
                    <span><a href="{{url('admin/jobs')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/jobs')}}"><i class="fa fa-table"></i> Jobs</a></li>
            <li>Edit Job</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/jobs/update')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        <input type="hidden" name="currid" value="{{$record->id}}">
                        <input type="hidden" name="job_type" value="{{$record->job_type}}">
                        {{ csrf_field() }}
                        @php
                        $selecteddate       = $record->selecteddate;
                        $selecteddate_array = array();
                        if(!empty($selecteddate)){
                            $selecteddate_array = explode(',',$selecteddate);
                        }
                        $selectedtime       = $record->selectedtime;
                        $selectedtime_array = array();
                        if(!empty($selectedtime)){
                            $selectedtime_array = explode(',',$selectedtime);
                        }
                        $selectedfre        = $record->selectedfre;
                        $selectedfre_array  = array();
                        if(!empty($selectedfre)){
                            $selectedfre_array = explode(',',$selectedfre);
                        }                        
                        @endphp
                        <div class="form-group">
                            <label class="col-md-12">Schedule</label>
                            <div class="col-md-12">
                                <ul class="schedule">
                                    <li>
                                        <input type="text" name="selecteddate[]" class="form-control date selecteddate" readonly value="{{ (isset($selecteddate_array[0])?$selecteddate_array[0]:'') }}"/>
                                    </li>
                                    <li>
                                        <div class="select-style">
                                            <select name="selectedtime[]" class="form-control selectedtime">
                                                <option value="">Time Frame</option>
                                                <option value="Between 6am to 9am" @if(isset($selectedtime_array[0]) && $selectedtime_array[0]=='Between 6am to 9am') selected @endif>Between 6am to 9am</option>
                                                <option value="Between 9am to 12pm" @if(isset($selectedtime_array[0]) && $selectedtime_array[0]=='Between 9am to 12pm') selected @endif>Between 9am to 12pm</option>
                                                <option value="Between 12pm to 3pm" @if(isset($selectedtime_array[0]) && $selectedtime_array[0]=='Between 12pm to 3pm') selected @endif>Between 12pm to 3pm</option>
                                                <option value="Between 3pm to 6pm" @if(isset($selectedtime_array[0]) && $selectedtime_array[0]=='Between 3pm to 6pm') selected @endif>Between 3pm to 6pm</option>
                                                <option value="Between 6pm to 9pm" @if(isset($selectedtime_array[0]) && $selectedtime_array[0]=='Between 6pm to 9pm') selected @endif>Between 6pm to 9pm</option>
                                                <option value="Between 6am-9am" @if(isset($selectedtime_array[0]) && $selectedtime_array[0]=='Between 6am-9am') selected @endif>Between 6am-9am</option>
                                                <option value="Between 12am to 6am" @if(isset($selectedtime_array[0]) && $selectedtime_array[0]=='Between 12am to 6am') selected @endif>Between 12am to 6am</option>
                                            </select>
                                        </div>
                                    </li>                                        
                                    <li class="day">
                                        <div class="select-style">
                                            <select name="selectedfre[]" class="form-control selectedfre">
                                                <option value="">Frequency</option>     
                                                <option value="One time" @if(isset($selectedfre_array[0]) && $selectedfre_array[0]=='One time') selected @endif>One time</option>
                                                <option value="Weekly" @if(isset($selectedfre_array[0]) && $selectedfre_array[0]=='Weekly') selected @endif>Weekly</option>
                                                <option value="Every two weeks" @if(isset($selectedfre_array[0]) && $selectedfre_array[0]=='Every two weeks') selected @endif>Every two weeks</option>
                                                <option value="Twice a month" @if(isset($selectedfre_array[0]) && $selectedfre_array[0]=='Twice a month') selected @endif>Twice a month</option>
                                                <option value="Monthly" @if(isset($selectedfre_array[0]) && $selectedfre_array[0]=='Monthly') selected @endif>Monthly</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="add-more-li">
                                        <a href="javascript:void(0)" class="add-more-scheduler"><i class="fa fa-plus"></i></a>
                                    </li>
                                </ul>
                                <div class="more-scheduler">
                                    @php
                                        $total_records = count($selecteddate_array);
                                        for($x=1;$x<$total_records;$x++){
                                        @endphp
                                        <ul class="schedule more-added-scheduler">
                                            <li>
                                                <input type="text" name="selecteddate[]" class="form-control date selecteddate" readonly value="{{ (isset($selecteddate_array[$x])?$selecteddate_array[$x]:'') }}"/>
                                            </li>
                                            <li>
                                                <div class="select-style">
                                                    <select name="selectedtime[]" class="selectedtime form-control">
                                                        <option value="">Time Frame</option>
                                                        <option value="Between 6am to 9am" @if(isset($selectedtime_array[$x]) && $selectedtime_array[$x]=='Between 6am to 9am') selected @endif>Between 6am to 9am</option>
                                                        <option value="Between 9am to 12pm" @if(isset($selectedtime_array[$x]) && $selectedtime_array[$x]=='Between 9am to 12pm') selected @endif>Between 9am to 12pm</option>
                                                        <option value="Between 12pm to 3pm" @if(isset($selectedtime_array[$x]) && $selectedtime_array[$x]=='Between 12pm to 3pm') selected @endif>Between 12pm to 3pm</option>
                                                        <option value="Between 3pm to 6pm" @if(isset($selectedtime_array[$x]) && $selectedtime_array[$x]=='Between 3pm to 6pm') selected @endif>Between 3pm to 6pm</option>
                                                        <option value="Between 6pm to 9pm" @if(isset($selectedtime_array[$x]) && $selectedtime_array[$x]=='Between 6pm to 9pm') selected @endif>Between 6pm to 9pm</option>
                                                        <option value="Between 6am-9am" @if(isset($selectedtime_array[$x]) && $selectedtime_array[$x]=='Between 6am-9am') selected @endif>Between 6am-9am</option>
                                                        <option value="Between 12am to 6am" @if(isset($selectedtime_array[$x]) && $selectedtime_array[$x]=='Between 12am to 6am') selected @endif>Between 12am to 6am</option>
                                                    </select>
                                                </div>
                                            </li>
                                            <li class="day">
                                                <div class="select-style">
                                                    <select name="selectedfre[]" class="selectedfre form-control">
                                                        <option value="">Frequency</option>     
                                                        <option value="One time" @if(isset($selectedfre_array[$x]) && $selectedfre_array[$x]=='One time') selected @endif>One time</option>
                                                        <option value="Weekly" @if(isset($selectedfre_array[$x]) && $selectedfre_array[$x]=='Weekly') selected @endif>Weekly</option>
                                                        <option value="Every two weeks" @if(isset($selectedfre_array[$x]) && $selectedfre_array[$x]=='Every two weeks') selected @endif>Every two weeks</option>
                                                        <option value="Twice a month" @if(isset($selectedfre_array[$x]) && $selectedfre_array[$x]=='Twice a month') selected @endif>Twice a month</option>
                                                        <option value="Monthly" @if(isset($selectedfre_array[$x]) && $selectedfre_array[$x]=='Monthly') selected @endif>Monthly</option>
                                                    </select>
                                                </div>
                                            </li>
                                            <li class="add-more-li">
                                                <a href="javascript:void(0)" class="remove-last-scheduler"><i class="fa fa-minus"></i></a>
                                            </li>
                                        </ul>
                                        @php
                                        }
                                    @endphp
                                </div>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="care_location2">Care Location</label>
                            <div class="col-md-9">
                                <input type="hidden" name="care_location" id="care_location" value="">
                                <input type="text" id="care_location2" name="care_location2" class="form-control" required placeholder="Location Zip Code" value="{{$record->care_location}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="job_title">JOB DETAILS
                                <br>Type of caregiver needed</label>
                            <div class="col-md-9">
                                @php
                                $child_care = getCategories(1,0);
                                @endphp
                                @if(!empty($child_care))
                                    @foreach($child_care as $childCare)
                                        <label class="radio-inline" for="radio{{$childCare->id}}">
                                            <input type="radio" class="type-of-caregiver" value="{{$childCare->id}}" @if($record->categories==$childCare->id) checked @endif id="radio{{$childCare->id}}" name="categories">{{$childCare->name}}
                                        </label>                                    
                                    @endforeach
                                @endif  
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="job_title">Job Title</label>
                            <div class="col-md-9">
                                <input type="text" id="job_title" name="job_title" class="form-control" required placeholder="Job Title" value="{{$record->job_title}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="additional_details">Job Description</label>
                            <div class="col-md-9">
                                <textarea id="additional_details" name="additional_details" class="form-control" required placeholder="Job Description">{{$record->additional_details}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="relationship">Who need care</label>
                            <div class="col-md-9">
                                <select name="relationship" id="relationship" class="form-control">
                                    <option value="">Relationship</option>
                                    <option value="Mother" @if($record->relationship=='Mother') selected @endif>Mother</option>
                                    <option value="Father" @if($record->relationship=='Father') selected @endif>Father</option>
                                    <option value="Husband" @if($record->relationship=='Husband') selected @endif>Husband</option>
                                    <option value="Wife" @if($record->relationship=='Wife') selected @endif>Wife</option>
                                    <option value="Self" @if($record->relationship=='Self') selected @endif>Self</option>
                                    <option value="Grandmother" @if($record->relationship=='Grandmother') selected @endif>Grandmother</option>
                                    <option value="Friend/Relative" @if($record->relationship=='Friend/Relative') selected @endif>Friend/Relative</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Requirements: (Mandatory)</label>
                            <div class="col-md-9">
                                <section class="check additional ">
                                    @php
                                    $requirements        = $record->requirements;
                                    $requirements_array  = array();
                                    if(!empty($requirements)){
                                        $requirements_array = explode(',',$requirements);
                                    }
                                    @endphp
                                    <ul>
                                        <li>
                                            <input @if(in_array('Light Housekeeping',$requirements_array)) checked @endif type="checkbox" id="light_housekeeping" value="Light Housekeeping" name="requirements[]" class="requirements_post" />
                                            <label for="light_housekeeping"><span></span>Light Housekeeping</label>
                                        </li>
                                        <li>
                                            <input @if(in_array('Meal Preparation',$requirements_array)) checked @endif type="checkbox" id="meal_preparation" value="Meal Preparation" name="requirements[]" class="requirements_post" />
                                            <label for="meal_preparation"><span></span>Meal Preparation</label>
                                        </li>
                                        <li>
                                            <input @if(in_array('Laundry',$requirements_array)) checked @endif type="checkbox" id="laundry" value="Laundry" name="requirements[]" class="requirements_post" />
                                            <label for="laundry"><span></span>Laundry</label>
                                        </li>                                        
                                        <li>
                                            <input @if(in_array('Errands / Groceries',$requirements_array)) checked @endif type="checkbox" id="errands_groceries" value="Errands / Groceries" name="requirements[]" class="requirements_post" />
                                            <label for="errands_groceries"><span></span>Errands / Groceries</label>
                                        </li>                                        
                                        <li>
                                            <input @if(in_array('Companionship',$requirements_array)) checked @endif type="checkbox" id="companionship" value="Companionship" name="requirements[]" class="requirements_post" />
                                            <label for="companionship"><span></span>Companionship</label>
                                        </li>                                        
                                        <li>
                                            <input @if(in_array('Has Transportation',$requirements_array)) checked @endif type="checkbox" id="has_transportation" value="Has Transportation" name="requirements[]" class="requirements_post" />
                                            <label for="has_transportation"><span></span>Has Transportation</label>
                                        </li>
                                        <li>
                                            <input @if(in_array('Comfortable with Pets',$requirements_array)) checked @endif type="checkbox" id="comfortable_with_pets" name="requirements[]" class="requirements_post" value="Comfortable with Pets"/>
                                            <label for="comfortable_with_pets"><span></span> Comfortable with Pets</label>
                                        </li>
                                        <li>
                                            <input @if(in_array('others',$requirements_array)) checked @endif type="checkbox" id="req_others" name="requirements[]" class="requirements_post" value="others"/>
                                            <label for="req_others"><span></span>Others</label>
                                        </li>
                                    </ul>
                                    <div class="other-requirements-sec" @if(in_array('others',$requirements_array)) style="display:block;" @endif>
                                        <textarea name="other_requirements" id="other_requirements" class="form-control" placeholder="Please enter comma saperated">{{$record->other_requirements}}</textarea>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="special_needs">Special Needs</label>
                            <div class="col-md-9">
                                <textarea name="special_needs" id="special_needs" class="form-control" placeholder="Please enter comma saperated">{{$record->special_needs}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="education_level">Highest Education Level</label>
                            <div class="col-md-9">
                                <select name="education_level" id="education_level" class="form-control">
                                    <option value="">Education Level</option>
                                    <option value="Some high school" @if($record->education_level=='Some high school') selected @endif>Some high school</option>
                                    <option value="High school degree" @if($record->education_level=='High school degree') selected @endif>High school degree</option>
                                    <option value="Some college" @if($record->education_level=='Some college') selected @endif>Some college</option>
                                    <option value="College degree" @if($record->education_level=='College degree') selected @endif>College degree</option>
                                    <option value="Some graduate school" @if($record->education_level=='Some graduate school') selected @endif>Some graduate school</option>
                                    <option value="Graduate degree" @if($record->education_level=='Graduate degree') selected @endif>Graduate degree</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="ex2">Pay Rate</label>
                            <div class="col-md-9">                           
                                <div class="range">
                                    <input id="ex2" name="price_range" type="text" class="span2" value="{{$record->price_range}}" data-slider-min="10" data-slider-max="50" data-slider-step="1" data-slider-value="[{{$record->price_range}}]"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Status</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="status_radio1">
                                    <input type="radio" id="status_radio1" name="status" value="1" @if($record->status==1) checked @endif > Active
                                </label>
                                <label class="radio-inline" for="status_radio2">
                                    <input type="radio" id="status_radio2" name="status" value="0" @if($record->status==0) checked @endif > Inactive
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Approved Status</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="approved1">
                                    <input type="radio" id="approved1" name="approved" value="1" @if($record->approved==1) checked @endif > Approved
                                </label>
                                <label class="radio-inline" for="approved2">
                                    <input type="radio" id="approved2" name="approved" value="0" @if($record->approved==0) checked @endif > Unapproved
                                </label>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection