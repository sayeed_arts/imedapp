@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Edit Category
                    <span><a href="{{url('admin/service-category')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/service-category')}}"><i class="fa fa-table"></i> Service Category</a></li>
            <li>Edit Category</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">                    
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/service-category/update')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        <input type="hidden" name="currid" value="{{$record->id}}">
                        <input type="hidden" name="old_image" value="{{$record->image}}">
                        {{ csrf_field() }}
                        <div class="nav-tab-section">
                            <div class="row" style="min-height:300px;">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <ul class="nav nav-tabs tabs-left">
                                                <li class="active"><a href="#home" data-toggle="tab">General</a></li>
                                                @if(!empty($allLanguages) && count($allLanguages)>0)
                                                    @foreach($allLanguages as $singleLanguage)
                                                        <li><a href="#language_{{$singleLanguage->code}}" data-toggle="tab">{{$singleLanguage->name}}</a></li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="tab-content">
                                                <!-- GENERAL TAB START -->
                                                <div class="tab-pane active" id="home">
                                                    <h2>English</h2>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="name">Title</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="name" name="name" class="form-control" required placeholder="Title" value="{{$record->name}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="image-input">Image</label>
                                                        <div class="col-md-9">
                                                            <input type="file" id="image-input" name="image" class="form-control">
                                                            @if(!empty($record->image))
                                                            <img src="{{ asset($record->image)}}" alt="Image" height="150" width="150">
                                                            @endif
                                                        </div>
                                                    </div>                                         
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="offered">Equipment Type</label>
                                                        <div class="col-md-9">
                                                            <textarea id="offered" name="offered" rows="5" class="form-control" placeholder="Equipment Type">{{$record->offered}}</textarea>
                                                        </div>
                                                    </div>
                                                    <?php /*
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label"
                                                            for="services">Services</label>
                                                        <div class="col-md-9">
                                                            <select name="services[]" id="services" class="form-control selectpicker" multiple  data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
                                                               <!--  <option value="">Select</option> -->
                                                                @if(!empty($allServices))
                                                                    @foreach($allServices as $category)
                                                                        <option value="{{$category->id}}" @if(in_array($category->id,$selectedServices)) selected @endif>{{$category->name}}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>         */?>          
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="display_order">Display Order</label>
                                                        <div class="col-md-9">
                                                            <input type="number" id="display_order" name="display_order" class="form-control" placeholder="1.00" required value="{{$record->display_order}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Display Status</label>
                                                        <div class="col-md-9">
                                                            <label class="radio-inline" for="display_status_radio1">
                                                                <input type="radio" id="display_status_radio1" name="display_status" value="1" @if($record->display_status==1) checked @endif > Active
                                                            </label>
                                                            <label class="radio-inline" for="display_status_radio2">
                                                                <input type="radio" id="display_status_radio2" name="display_status" value="0" @if($record->display_status==0) checked @endif > Inactive
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if(!empty($allLanguages) && count($allLanguages)>0)
                                                    @foreach($allLanguages as $singleLanguage)
                                                    @php
                                                        $languageData = (isset($allOtherLang[$singleLanguage->code]))?$allOtherLang[$singleLanguage->code]:array();
                                                    @endphp
                                                    <input type="hidden" name="language[]" value="{{$singleLanguage->code}}">
                                                    <div class="tab-pane" id="language_{{$singleLanguage->code}}">
                                                        <h2>{{$singleLanguage->name}}</h2>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label" for="name_{{$singleLanguage->code}}">Title</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="name_{{$singleLanguage->code}}" name="name_{{$singleLanguage->code}}"  class="form-control" placeholder="Title" value="{{(isset($languageData['name'])?$languageData['name']:'')}}"></textarea>
                                                            </div>
                                                        </div>       
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label" for="offered_{{$singleLanguage->code}}">Equipment Type</label>
                                                            <div class="col-md-9">
                                                                <textarea id="offered_{{$singleLanguage->code}}" name="offered_{{$singleLanguage->code}}" rows="5" class="form-control" placeholder="Equipment Type">{{(isset($languageData['offered'])?$languageData['offered']:'')}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection