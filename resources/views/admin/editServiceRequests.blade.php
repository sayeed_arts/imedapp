@extends('layouts.adminapp')

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Forms General Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                View Service Request
                <span><a href="{{url('admin/services-request')}}" class="btn btn-default">Cancel</a></span>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('admin/services-request')}}"><i class="fa fa-table"></i> Service Requests</a></li>
        <li>View Service Request</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}
                @endif
                {!! validationError($errors) !!}
                <!-- Basic Form Elements Content -->
                {{ csrf_field() }}
                <div class="service-request-details">
                    <div class="row" style="min-height:300px;">
                        <div class="col-sm-12 ">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                style="border-collapse:collapse; border-spacing:0px; margin: 0;">
                                <tr>
                                    <td width="33%">Hospital / Company</td>
                                    <td width="3%">:</td>
                                    <td> {!!$record->hospital_company!!}</td>
                                </tr>
                                <tr>
                                    <td width="">Full Name</td>
                                    <td width="">:</td>
                                    <td width=""> {!!$record->name!!}</td>
                                </tr>
                                <tr>
                                    <td width="">Phone</td>
                                    <td width="">:</td>
                                    <td width=""> {!!$record->phone!!}</td>
                                </tr>
                                <tr>
                                    <td width="">Postal Code</td>
                                    <td width="">:</td>
                                    <td width=""> {!!$record->postal_code!!}</td>
                                </tr>
                                <tr>
                                    <td width="">Extension</td>
                                    <td width="">:</td>
                                    <td width=""> {!!$record->extension!!}</td>
                                </tr>
                                <tr>
                                    <td width="">Email</td>
                                    <td width="">:</td>
                                    <td width=""> {!!$record->email!!}</td>
                                </tr>
                                <tr>
                                    <td width="">PO #</td>
                                    <td width="">:</td>
                                    <td width=""> {!!$record->PO!!}</td>
                                </tr>
                                <tr>
                                    <td width="">Category</td>
                                    <td width="">:</td>
                                    <td width=""> {!!$record->service_category!!}</td>
                                </tr>

                            </table>
                            @if(!empty($equipmentTypeArray))
                            <div class="equipments">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                    style="border-collapse:collapse; border-spacing:0px; margin: 0;">
                                    @foreach($equipmentTypeArray as $key => $equipmentType)

                                    @if(!empty($equipmentType))
                                    <tr>
                                        <td><b>Equipment Type :</b> {{$equipmentType}}</td>
                                        <td><b>Serial Number :</b>
                                            {{((isset($serialNumberArray[$key]))?$serialNumberArray[$key]:'')}}</td>
                                        <td><b>Quantity : </b> {{((isset($amountArray[$key]))?$amountArray[$key]:'')}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Product Issue :</b>
                                            {!!((isset($productIssueArray[$key]))?nl2br($productIssueArray[$key]):'')!!}
                                        </td>
                                    </tr>

                                    @endif

                                    @endforeach
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                        style="border-collapse:collapse; border-spacing:0px; margin: 0;">
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
    <!-- END Form Example with Blocks in the Grid -->
</div>
<!-- END Page Content -->
@endsection