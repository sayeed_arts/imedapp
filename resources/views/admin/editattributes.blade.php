@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Edit Attribute
                    <span><a href="{{url('admin/attributes')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/attributes')}}"><i class="fa fa-table"></i> Attributes</a></li>
            <li>Edit Attribute</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/attributes/update')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        <input type="hidden" name="currid" value="{{$record->id}}">
                        {{ csrf_field() }}
                        <!-- <div class="nav-tab-section">
                            <div class="row" style="min-height:300px;">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <ul class="nav nav-tabs tabs-left">
                                                <li class="active"><a href="#home" data-toggle="tab">General</a></li>
                                                @if(!empty($allLanguages) && count($allLanguages)>0)
                                                    @foreach($allLanguages as $singleLanguage)
                                                        <li><a href="#language_{{$singleLanguage->code}}" data-toggle="tab">{{$singleLanguage->name}}</a></li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="home">
                                                    <h2>English</h2> -->
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="set_id">Attribute Sets</label>
                                                        <div class="col-md-9">
                                                            <select id="set_id" name="set_id" class="form-control" required>
                                                                <option value="">Attribute Sets</option>
                                                                @if(!empty($allRecords))
                                                                    @foreach($allRecords as $singleRecord)
                                                                        <option value="{{$singleRecord->id}}" @if($record->set_id==$singleRecord->id) selected @endif>{{$singleRecord->name}}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="name">Title</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="name" name="name" class="form-control" required placeholder="Title" value="{{$record->name}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="values_1">Value</label>
                                                        <div class="col-md-9 add_more_bullets">                    
                                                            @if(!empty($allRecordValues) && count($allRecordValues)>0)
                                                                @foreach($allRecordValues as $singleBullet)
                                                                    <input type="text" name="values[]" class="form-control bullet_points" placeholder="Value" value="{{$singleBullet->name}}">
                                                                @endforeach
                                                            @else
                                                                <input type="text" name="values[]" class="form-control bullet_points" placeholder="value" value="">
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">&nbsp;</label>
                                                        <div class="col-md-9">
                                                            <input type="button" name="add_more" value="Add More" class="btn btn-default add_more_bullets_button">
                                                        </div>
                                                    </div>      
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="display_order">Display Order</label>
                                                        <div class="col-md-9">
                                                            <input type="number" id="display_order" name="display_order" class="form-control" placeholder="1.00" required value="{{$record->display_order}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Display Status</label>
                                                        <div class="col-md-9">
                                                            <label class="radio-inline" for="display_status_radio1">
                                                                <input type="radio" id="display_status_radio1" name="display_status" value="1" @if($record->display_status==1) checked @endif > Active
                                                            </label>
                                                            <label class="radio-inline" for="display_status_radio2">
                                                                <input type="radio" id="display_status_radio2" name="display_status" value="0" @if($record->display_status==0) checked @endif > Inactive
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div><!-- 
                                                @if(!empty($allLanguages) && count($allLanguages)>0)
                                                    @foreach($allLanguages as $singleLanguage)
                                                    @php
                                                        $languageData = (isset($allOtherLang[$singleLanguage->code]))?$allOtherLang[$singleLanguage->code]:array();
                                                    @endphp
                                                    <input type="hidden" name="language[]" value="{{$singleLanguage->code}}">
                                                    <div class="tab-pane" id="language_{{$singleLanguage->code}}">
                                                        <h2>{{$singleLanguage->name}}</h2>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label" for="name_{{$singleLanguage->code}}">Title</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="name_{{$singleLanguage->code}}" name="name_{{$singleLanguage->code}}"  class="form-control" placeholder="Title" value="{{(isset($languageData['name'])?$languageData['name']:'')}}"></textarea>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Value</label>
                                                            <div class="col-md-9 add_more_bullets_lang_{{$singleLanguage->code}}">
                                                                @php
                                                                $offerContent = (isset($languageData['values']))?$languageData['values']:'';
                                                                $offerContentArray = explode('##~~##',$offerContent);
                                                                @endphp
                                                                @if(!empty($offerContentArray))
                                                                    @foreach($offerContentArray as $singleBullet)
                                                                        <input type="text" name="values_{{$singleLanguage->code}}[]" class="form-control bullet_points" placeholder="Value" value="{{$singleBullet}}">
                                                                    @endforeach
                                                                @else
                                                                    <input type="text" name="values_{{$singleLanguage->code}}[]" class="form-control bullet_points" placeholder="value" value="">
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">&nbsp;</label>
                                                            <div class="col-md-9">
                                                                <input type="button" name="add_more" value="Add More" class="btn btn-default add_more_bullets_button_lang" data-lang="{{$singleLanguage->code}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection