@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Edit Currency
                    <span><a href="{{url('admin/currency')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/currency')}}"><i class="fa fa-table"></i> Currencies</a></li>
            <li>Edit Currency</li>
        </ul>
        <!-- END Forms General Header -->
        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/currency/update')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        <input type="hidden" name="currid" value="{{$record->id}}">
                        {{ csrf_field() }}
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Title</label>
                            <div class="col-md-9">
                                <input type="text" id="name" name="name" class="form-control" required placeholder="Title" value="{{$record->name}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="code">Code</label>
                            <div class="col-md-9">
                                <input type="text" id="code" name="code" class="form-control" required placeholder="Code" value="{{$record->code}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="symbol">Symbol</label>
                            <div class="col-md-9">
                                <input type="text" id="symbol" name="symbol" class="form-control" required value="{{$record->symbol}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="exchange_rate">Exchange Rate</label>
                            <div class="col-md-9">
                                <input type="text" id="exchange_rate" name="exchange_rate" class="form-control" required value="{{$record->exchange_rate}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Symbol Position</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="symbol_position1">
                                    <input type="radio" id="symbol_position1" name="symbol_position" value="1" @if($record->symbol_position==1) checked @endif > Postfix
                                </label>
                                <label class="radio-inline" for="defaultL_radio2">
                                    <input type="radio" id="defaultL_radio2" name="symbol_position" value="0" @if($record->symbol_position==0) checked @endif > Prefix
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Default</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="defaultL_radio1">
                                    <input type="radio" id="defaultL_radio1" name="defaultC" value="1" @if($record->defaultC==1) checked @endif > Yes
                                </label>
                                <label class="radio-inline" for="defaultL_radio2">
                                    <input type="radio" id="defaultL_radio2" name="defaultC" value="0" @if($record->defaultC==0) checked @endif > No
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Status</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="display_status_radio1">
                                    <input type="radio" id="display_status_radio1" name="status" value="1" @if($record->status==1) checked @endif > Active
                                </label>
                                <label class="radio-inline" for="display_status_radio2">
                                    <input type="radio" id="display_status_radio2" name="status" value="0" @if($record->status==0) checked @endif > Inactive
                                </label>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection