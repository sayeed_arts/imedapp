@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Edit FAQ
                    <span><a href="{{url('admin/faq')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/faq')}}"><i class="fa fa-table"></i> FAQ</a></li>
            <li>Edit FAQ</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/faq/update')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        <input type="hidden" name="currid" value="{{$record->id}}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Question</label>
                            <div class="col-md-9">
                                <input type="text" id="name" name="name" class="form-control" required placeholder="Question" value="{{$record->name}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Answer</label>
                            <div class="col-md-9">
                                <textarea id="content" name="content" rows="4" class="form-control ckeditor" placeholder="Answer" required>{{$record->content}}</textarea>
                            </div>
                        </div>        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="display_order">Display Order</label>
                            <div class="col-md-9">
                                <input type="number" id="display_order" name="display_order" class="form-control" placeholder="1.00" required value="{{$record->display_order}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Display Status</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="display_status_radio1">
                                    <input type="radio" id="display_status_radio1" name="display_status" value="1" @if($record->display_status==1) checked @endif > Active
                                </label>
                                <label class="radio-inline" for="display_status_radio2">
                                    <input type="radio" id="display_status_radio2" name="display_status" value="0" @if($record->display_status==0) checked @endif > Inactive
                                </label>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection