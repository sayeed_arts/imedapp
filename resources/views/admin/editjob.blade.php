@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Edit Job
                    <span><a href="{{url('admin/job')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/job')}}"><i class="fa fa-table"></i> Job</a></li>
            <li>Edit Job</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/jobs/update')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        <input type="hidden" name="currid" value="{{$record->id}}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Title</label>
                            <div class="col-md-9">
                                <input type="text" id="name" name="name" class="form-control" required placeholder="Title" value="{{$record->name}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Description</label>
                            <div class="col-md-9">
                                <textarea id="content" name="content" rows="4" class="form-control ckeditor" placeholder="Description" required>{{$record->content}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="requirements">Requirements</label>
                            <div class="col-md-9">
                                <textarea id="requirements" name="requirements" rows="4" class="form-control ckeditor" placeholder="Requirements" required>{{$record->requirements}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">Email</label>
                            <div class="col-md-9">
                                <input type="email" id="email" name="email" class="form-control" required placeholder="Email" value="{{$record->email}}">
                            </div>
                        </div>       
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="display_order">Display Order</label>
                            <div class="col-md-9">
                                <input type="number" id="display_order" name="display_order" class="form-control" placeholder="1.00" required value="{{$record->display_order}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Display Status</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="display_status_radio1">
                                    <input type="radio" id="display_status_radio1" name="display_status" value="1" @if($record->display_status==1) checked @endif > Active
                                </label>
                                <label class="radio-inline" for="display_status_radio2">
                                    <input type="radio" id="display_status_radio2" name="display_status" value="0" @if($record->display_status==0) checked @endif > Inactive
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">Experience</label>
                            <div class="col-md-9">
                                <input type="text" id="experience" name="experience" class="form-control" required placeholder="Experience Years (6-8 Years)" value="{{$record->experience}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">End Date</label>
                            <div class="col-md-8">
                                <input type="text" id="closing_date" name="closing_date" class="form-control input-datepicker" required placeholder="Enter End Date" value="{{date('m/d/Y', strtotime($record->closing_date))}}" tabindex="1" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="category_id11">Category <?php $catarray = explode(",", $record->category_id); ?></label>
                            <div class="col-md-9">
                                <select name="category_id[]" id="category_id11" class="form-control selectpicker" multiple  data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">    
                                    <option value="0">Category</option>
                                    @if(!empty($allCategories))
                                        @foreach($allCategories as $singleCategory)
                                            <option value="{{$singleCategory->id}}" <?php if (in_array($singleCategory->id, $catarray)) { echo 'selected'; }?> >{{$singleCategory->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="location_id">Location</label>
                            <div class="col-md-9">
                                <select name="location_id" id="location_id" class="form-control">
                                    <option value="0">Location</option>
                                    @if(!empty($allLocations))
                                        @foreach($allLocations as $singleLocation)
                                            <option value="{{$singleLocation->id}}" <?php if($singleLocation->id==$record->location_id){
                                                                        echo "selected";
                                                                        } ?>>{{$singleLocation->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="type_id">Type</label>
                            <div class="col-md-9">
                                <select name="type_id" id="type_id" class="form-control">
                                    <option value="0">Type</option>
                                    @if(!empty($allTypes))
                                        @foreach($allTypes as $singleType)
                                            <option value="{{$singleType->id}}" <?php if($singleType->id==$record->type_id){
                                                                        echo "selected";
                                                                        } ?>>{{$singleType->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection