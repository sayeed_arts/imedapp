@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Edit Job Location
                    <span><a href="{{url('admin/joblocations')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/joblocations')}}"><i class="fa fa-table"></i> Job Locations</a></li>
            <li>Edit Location</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                   
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/joblocations/update')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        <input type="hidden" name="currid" value="{{$record->id}}">
                        {{ csrf_field() }}
                        <div class="nav-tab-section">
                            <div class="row" style="min-height:300px;">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <ul class="nav nav-tabs tabs-left">
                                                <li class="active"><a href="#home" data-toggle="tab">General</a></li>
                                                @if(!empty($allLanguages) && count($allLanguages)>0)
                                                    @foreach($allLanguages as $singleLanguage)
                                                        <li><a href="#language_{{$singleLanguage->code}}" data-toggle="tab">{{$singleLanguage->name}}</a></li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="tab-content">
                                                <!-- GENERAL TAB START -->
                                                <div class="tab-pane active" id="home">
                                                    <h2>English</h2>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="name">Title</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="name" name="name" class="form-control" required placeholder="Title" value="{{$record->name}}">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="Content">Content</label>
                                                        <div class="col-md-9">
                                                            <textarea id="content" name="content" class="form-control ckeditor" placeholder="Content">{{$record->content}}</textarea>
                                                        </div>
                                                    </div>  

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="meta_title">Meta Title</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="meta_title" name="meta_title" class="form-control" placeholder="Meta Title" value="{{$record->meta_title}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="meta_key">Meta Keywords</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="meta_key" name="meta_key" class="form-control" placeholder="Meta Keywords" value="{{$record->meta_key}}">
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="meta_desc">Meta Description</label>
                                                        <div class="col-md-9">
                                                            <textarea id="meta_desc" name="meta_desc" rows="4" class="form-control" placeholder="Meta Description" >{{$record->meta_desc}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="display_order">Display Order</label>
                                                        <div class="col-md-9">
                                                            <input type="number" id="display_order" name="display_order" class="form-control" placeholder="1.00" value="{{$record->display_order}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Display Status</label>
                                                        <div class="col-md-9">
                                                            <label class="radio-inline" for="display_status_radio1">
                                                                <input type="radio" id="display_status_radio1" name="display_status" value="1" @if($record->display_status==1) checked @endif > Active
                                                            </label>
                                                            <label class="radio-inline" for="display_status_radio2">
                                                                <input type="radio" id="display_status_radio2" name="display_status" value="0" @if($record->display_status==0) checked @endif > Inactive
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if(!empty($allLanguages) && count($allLanguages)>0)
                                                    @foreach($allLanguages as $singleLanguage)
                                                    @php
                                                        $languageData = (isset($allOtherLang[$singleLanguage->code]))?$allOtherLang[$singleLanguage->code]:array();
                                                    @endphp
                                                    <input type="hidden" name="language[]" value="{{$singleLanguage->code}}">
                                                    <div class="tab-pane" id="language_{{$singleLanguage->code}}">
                                                        <h2>{{$singleLanguage->name}}</h2>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label" for="name_{{$singleLanguage->code}}">Title</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="name_{{$singleLanguage->code}}" name="name_{{$singleLanguage->code}}"  class="form-control" placeholder="Title" value="{{(isset($languageData['name'])?$languageData['name']:'')}}"></textarea>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label" for="content_{{$singleLanguage->code}}">Content</label>
                                                            <div class="col-md-9">
                                                                <textarea id="content_{{$singleLanguage->code}}" name="content_{{$singleLanguage->code}}" rows="2" class="form-control ckeditor" placeholder="Content">{{(isset($languageData['content'])?$languageData['content']:'')}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection