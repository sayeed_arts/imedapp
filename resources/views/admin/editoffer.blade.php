@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Edit Offer
                    <span><a href="{{url('admin/offers')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/offers')}}"><i class="fa fa-table"></i> Offers</a></li>
            <li>Edit Offer</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">                   
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/offers/update')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        <input type="hidden" name="currid" value="{{$record->id}}">                       
                        <input type="hidden" name="old_image" value="{{$record->image}}">
                        {{ csrf_field() }}
                        @php
                        $selectedCatArray = array();
                        if(!empty($record->category)){
                            $selectedCatArray = explode(',',$record->category);
                        }
                        @endphp                    
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="category">Parent Category</label>
                            <div class="col-md-9">
                                <select name="category[]" id="category" class="form-control" multiple>
                                    <option value="">Select All</option>
                                    @if(!empty($allCats))
                                        @foreach($allCats as $singleRecord)
                                            <option value="{{$singleRecord->id}}" @if(in_array($singleRecord->id,$selectedCatArray)) selected="selected" @endif>{{$singleRecord->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Title</label>
                            <div class="col-md-9">
                                <input type="text" id="name" name="name" class="form-control" required placeholder="Title" value="{{$record->name}}">
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="image-input">Image</label>
                            <div class="col-md-9">
                                <input type="file" id="image-input" name="image" class="form-control">
                                @if(!empty($record->image))
                                    <img src="{{ asset($record->image)}}" alt="Image" height="150" width="150">
                                @endif
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="content_1">Bullet Points</label>
                            <div class="col-md-9 add_more_bullets">
                                @php
                                $offerContent = $record->content;
                                $offerContentArray = explode('##~~##',$offerContent);
                                @endphp
                                @if(!empty($offerContentArray))
                                    @foreach($offerContentArray as $singleBullet)
                                        <input type="text" name="content[]" class="form-control bullet_points" placeholder="Bullet Point" value="{{$singleBullet}}">
                                    @endforeach
                                @else
                                    <input type="text" name="content[]" class="form-control bullet_points" placeholder="Bullet Point" value="">
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-9">
                                <input type="button" name="add_more" value="Add More" class="btn btn-default add_more_bullets_button">
                            </div>
                        </div>                      
                        <!-- <div class="form-group">
                            <label class="col-md-3 control-label" for="Content">Content</label>
                            <div class="col-md-9">
                                <textarea id="content" name="content" class="form-control ckeditor" placeholder="Content">{{$record->content}}</textarea>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="rate_range">Rate Range</label>
                            <div class="col-md-9">
                                <input type="text" id="rate_range" name="rate_range" class="form-control" placeholder="Rate Range" value="{{$record->rate_range}}">
                            </div>
                        </div> --> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="button_text">Button Text</label>
                            <div class="col-md-9">
                                <input type="text" id="button_text" name="button_text" class="form-control" placeholder="Button Text" value="{{$record->button_text}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="link">Button Link</label>
                            <div class="col-md-9">
                                <input type="url" id="link" name="link" class="form-control" placeholder="Button Link" value="{{$record->link}}">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="button_info">Button Info</label>
                            <div class="col-md-9">
                                <textarea id="button_info" name="button_info" rows="4" class="form-control" placeholder="Button Info" >{{$record->button_info}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Display Status</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="display_status_radio1">
                                    <input type="radio" id="display_status_radio1" name="display_status" value="1" @if($record->display_status==1) checked @endif > Active
                                </label>
                                <label class="radio-inline" for="display_status_radio2">
                                    <input type="radio" id="display_status_radio2" name="display_status" value="0" @if($record->display_status==0) checked @endif > Inactive
                                </label>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection