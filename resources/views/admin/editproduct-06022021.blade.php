@extends('layouts.adminapp')

@section('content')
<style type="text/css">
.gallary_images {
    float: left;
    padding: 5px;
}
</style>
<!-- Page content -->
<div id="page-content">
    <!-- Forms General Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                Edit Product
                <span><a href="{{url('admin/products')}}" class="btn btn-default">Cancel</a></span>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('admin/products')}}"><i class="fa fa-table"></i> Products</a></li>
        <li>Edit Product</li>
    </ul>
    <!-- END Forms General Header -->
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <!-- <div class="block-title">
                        Add New
                    </div> -->
                <!-- END Form Elements Title -->
                @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}
                @endif
                {!! validationError($errors) !!}
                <!-- Basic Form Elements Content -->
                <form action="{{url('admin/products/update')}}" method="post" enctype="multipart/form-data"
                    class="form-horizontal form-bordered">
                    <input type="hidden" name="currid" value="{{$record->id}}">
                    <input type="hidden" name="old_qty" value="{{$record->qty}}">
                    <input type="hidden" name="old_image" value="{{$record->image}}">
                    <input type="hidden" name="total_products" value="{{$record->total_products}}">
                    <input type="hidden" name="old_additional_images" value="{{$record->additional_images}}">
                    {{ csrf_field() }}
                    <div class="nav-tab-section">
                        <h2>Product Data</h2>
                        <div class="row" style="min-height:300px;">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <ul class="nav nav-tabs tabs-left">
                                            <li class="active"><a href="#home" data-toggle="tab">General</a></li>
                                            <li><a href="#price" data-toggle="tab">Price</a></li>
                                            <li><a href="#inventory" data-toggle="tab">Inventory</a></li>
                                            <li><a href="#images" data-toggle="tab">Images</a></li>
                                            <li><a href="#attributes" data-toggle="tab">Attributes</a></li>
                                            <li><a href="#related" data-toggle="tab">Related Products</a></li>
                                            <li><a href="#seo" data-toggle="tab">SEO</a></li>
                                            @if(!empty($allLanguages) && count($allLanguages)>0)
                                                @foreach($allLanguages as $singleLanguage)
                                                    <li><a href="#language_{{$singleLanguage->code}}" data-toggle="tab">{{$singleLanguage->name}}</a></li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="tab-content">
                                            <!-- GENERAL TAB START -->
                                            <div class="tab-pane active" id="home">
                                                <h2>English</h2>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="product_type">Product
                                                        Available</label>
                                                    <div class="col-md-9">
                                                        <select name="product_type" id="product_type"
                                                            class="form-control">
                                                            <option value="1" @if($record->product_type==1) selected
                                                                @endif>For Sale</option>
                                                            <option value="0" @if($record->product_type==0) selected
                                                                @endif>For Rent</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="name">Title</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="name" name="name" class="form-control"
                                                            required placeholder="Title" value="{{$record->name}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="sku">SKU</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="sku" name="sku" class="form-control"
                                                            required placeholder="SKU" value="{{$record->sku}}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="new_date">New Till
                                                        Date</label>
                                                    <div class="col-md-9">
                                                        <input type="date" id="new_date" name="new_date"
                                                            class="form-control" placeholder=""
                                                            value="{{$record->new_date}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Featured Product</label>
                                                    <div class="col-md-9">
                                                        <label class="radio-inline" for="featured_product_radio2">
                                                            <input type="radio" id="featured_product_radio2"
                                                                name="is_featured" value="0"
                                                                @if($record->is_featured==0) checked @endif > No
                                                        </label>
                                                        <label class="radio-inline" for="featured_product_radio1">
                                                            <input type="radio" id="featured_product_radio1"
                                                                name="is_featured" value="1"
                                                                @if($record->is_featured==1) checked @endif > Yes
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="display_order">Display
                                                        Order</label>
                                                    <div class="col-md-9">
                                                        <input type="number" id="display_order" name="display_order"
                                                            class="form-control" placeholder="1.00"
                                                            value="{{$record->display_order}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Display Status</label>
                                                    <div class="col-md-9">
                                                        <label class="radio-inline" for="display_status_radio1">
                                                            <input type="radio" id="display_status_radio1"
                                                                name="display_status" value="1"
                                                                @if($record->display_status==1) checked @endif > Active
                                                        </label>
                                                        <label class="radio-inline" for="display_status_radio2">
                                                            <input type="radio" id="display_status_radio2"
                                                                name="display_status" value="0"
                                                                @if($record->display_status==0) checked @endif >
                                                            Inactive
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="short_description">Short
                                                        Description</label>
                                                    <div class="col-md-9">
                                                        <textarea id="short_description" name="short_description"
                                                            class="form-control"
                                                            placeholder="Short Description">{{$record->short_description}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label"
                                                        for="content">Description</label>
                                                    <div class="col-md-9">
                                                        <textarea id="content" name="content"
                                                            class="form-control ckeditor"
                                                            placeholder="Description">{{$record->content}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="more_info">More
                                                        Info</label>
                                                    <div class="col-md-9">
                                                        <textarea id="more_info" name="more_info"
                                                            class="form-control ckeditor"
                                                            placeholder="More Info">{{$record->more_info}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label"
                                                        for="manufacturer_id">Manufacturer</label>
                                                    <div class="col-md-9">
                                                        <select name="manufacturer_id" id="manufacturer_id"
                                                            class="form-control">
                                                            <option value="">Select</option>
                                                            @if(!empty($allManufacturer))
                                                                @foreach($allManufacturer as $manufacturer)
                                                                    <option value="{{$manufacturer->id}}" @if($record->manufacturer_id==$manufacturer->id) selected @endif>{{$manufacturer->name}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label"
                                                        for="category_id">Category</label>
                                                    <div class="col-md-9">
                                                        <select name="category_id[]" id="category_id" class="form-control selectpicker" multiple  data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
                                                           <!--  <option value="">Select</option> -->
                                                            @if(!empty($allCategory))
                                                                @foreach($allCategory as $category)
                                                                    <option value="{{$category->id}}" @if($record->category_id==$category->id) selected @endif>{{$category->name}}</option>
                                                                    @if(count($category->subcategory))
                                                                        @include('admin/subCategoriesProOptions',['subcategories' => $category->subcategory,'parent'=>$category->name,'selectedCat'=>@explode(',',$record->category_id)])
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <!-- <div class="form-group">
                                                    <label class="col-md-3 control-label" for="child_category_id">Child Category</label>
                                                    <div class="col-md-9">
                                                        <select name="child_category_id" id="child_category_id" class="form-control">
                                                            <option value="">Select</option>
                                                            @if(!empty($allSubCategory))
                                                            @foreach($allSubCategory as $subCategory)
                                                            <option value="{{$subCategory->id}}" @if($record->
                                                                child_category_id==$subCategory->id)
                                                                selected @endif>{{$subCategory->name}}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div> -->
                                            </div>
                                            <!-- GENERAL TAB END -->
                                            <!-- PRICE TAB START -->
                                            <div class="tab-pane" id="price">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="price">Price</label>
                                                    <div class="col-md-9">
                                                        <input type="number" id="price" name="price" class="form-control" step="0.01" required placeholder="Price" value="{{$record->price}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="special_price">Special
                                                        Price</label>
                                                    <div class="col-md-9">
                                                        <input type="number" step="0.01" id="special_price" name="special_price" class="form-control" placeholder="Special Price" value="{{$record->special_price}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- PRICE TAB END -->
                                            <!-- INVENTORY TAB START -->
                                            <div class="tab-pane" id="inventory">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label"
                                                        for="inventory_management">Inventory Management</label>
                                                    <div class="col-md-9">
                                                        <select name="inventory_management" id="inventory_management"
                                                            class="form-control">
                                                            <option value="0" @if($record->inventory_management==0)
                                                                selected @endif>Don't Track
                                                                Inventory</option>
                                                            <option value="1" @if($record->inventory_management==1)
                                                                selected @endif>Track Inventory
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="trackinventory" @if($record->inventory_management==0)
                                                    style="display:none;" @endif >
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="qty">Qty</label>
                                                        <div class="col-md-9">
                                                            <input type="number" id="qty" name="qty" class="form-control" placeholder="Qty" value="{{$record->qty}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label"
                                                            for="stock_availability">Stock Availability</label>
                                                        <div class="col-md-9">
                                                            <select name="stock_availability" id="stock_availability"
                                                                class="form-control">
                                                                <option value="1" @if($record->stock_availability==1)
                                                                    selected @endif>In Stock
                                                                </option>
                                                                <option value="0" @if($record->stock_availability==0)
                                                                    selected @endif>Out of Stock
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- INVENTORY TAB END -->
                                            <!-- IMAGES TAB START -->
                                            <div class="tab-pane" id="images">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="image-input">Base
                                                        Image</label>
                                                    <div class="col-md-9">
                                                        <input type="file" id="image-input" name="image"
                                                            class="form-control">
                                                        @if(!empty($record->image))
                                                        <br>
                                                        <img src="{{ asset($record->image)}}" alt="Image" height="150"
                                                            width="150">
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label"
                                                        for="additional_images">Additional Images</label>
                                                    <div class="col-md-9">
                                                        <input type="file" id="additional_images"
                                                            name="additional_images[]" class="form-control" multiple>
                                                        @if(!empty($record->additional_images))
                                                            @php
                                                            $addImgArray =explode('##~~##',$record->additional_images)
                                                            @endphp
                                                            <br>
                                                            @foreach($addImgArray as $img)
                                                            <div class="gallary_images">
                                                                <input type="hidden" name="all_add_images[]" value="{{$img}}">
                                                                <img src="{{ asset($img)}}" alt="Image" height="150" width="150">
                                                                <span class="delete-product-image"><i class="fa fa-trash"></i></span>
                                                            </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- IMAGES TAB START -->
                                            <!-- ATTRIBUTES TAB START -->
                                            <div class="tab-pane" id="attributes">
                                                <h3 class="tab-content-title">Attributes</h3>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="variation_attributes">Variation Attributes</label>
                                                    <div class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-4 attrsec">
                                                                <select name="variation_attributes" id="variation_attributes" class="form-control">
                                                                    <option value="">Select Attribute</option>
                                                                    @if(!empty($allAttributeSet))
                                                                        @foreach($allAttributeSet as $singleAttrSet)
                                                                            @php
                                                                            $allAttrs = (isset($attrArray[$singleAttrSet->id]))?$attrArray[$singleAttrSet->id]:array();
                                                                            @endphp
                                                                            <optgroup label="{{$singleAttrSet->name}}">
                                                                                @if(is_array($allAttrs) && !empty($allAttrs))
                                                                                    @foreach($allAttrs as $singleAttr)
                                                                                        <option value="{{$singleAttr['id']}}" @if(in_array($singleAttr['id'],$allVariAttr)) selected="selected" @endif>{{$singleAttr['name']}}</option>
                                                                                    @endforeach
                                                                                @endif
                                                                            </optgroup>
                                                                        @endforeach
                                                                    @endif                                
                                                                </select>
                                                            </div>
                                                            <div class="col-md-8 variation_attribute_value">
                                                                <!-- 
                                                                @if(!empty($productVariAttr))
                                                                    @foreach($productVariAttr as $singleVariAttr)
                                                                        @php
                                                                        $attrValLabel = (isset($attrValuesIdArray[$singleVariAttr->attribute_value]))?$attrValuesIdArray[$singleVariAttr->attribute_value]:'';
                                                                        @endphp
                                                                        <input type="checkbox" name="selectedvarattrval[]" id="attrvarval_{{$singleVariAttr->attribute_id}}_{{$singleVariAttr->attribute_value}}" data-id="{{$singleVariAttr->attribute_id}}_{{$singleVariAttr->attribute_value}}" value="{{$singleVariAttr->attribute_value}}" class="variation_attr" checked> <label for="attrvarval_{{$singleVariAttr->attribute_id}}_{{$singleVariAttr->attribute_value}}">{{$attrValLabel}}</label> <input type="number" steps="0.01" name="selectedattrvalprice[]" class="form-control" id="attrvalforprice_{{$singleVariAttr->attribute_id}}_{{$singleVariAttr->attribute_value}}" value="{{$singleVariAttr->attribute_price}}"> <br>
                                                                    @endforeach
                                                                @endif -->
                                                                @if(!empty($variationAllOption))
                                                                    @foreach($variationAllOption as $singleVarPoption)
                                                                        @php
                                                                            $priceval = (isset($allVariPriceAttr[$singleVarPoption['id']]))?$allVariPriceAttr[$singleVarPoption['id']]:'';
                                                                            if(isset($allVariPriceAttr[$singleVarPoption['id']])){
                                                                                $checked = 'checked';
                                                                            }else{
                                                                                $checked = '';
                                                                            }
                                                                        @endphp
                                                                        <input type="hidden" name="hiddenIds[]" value="{{$singleVarPoption['id']}}">
                                                                        <input type="checkbox" name="selectedvarattrval[]" id="attrvarval_{{$singleVarPoption['attribute_id']}}_{{$singleVarPoption['id']}}" data-id="{{$singleVarPoption['attribute_id']}}_{{$singleVarPoption['id']}}" value="{{$singleVarPoption['id']}}" class="variation_attr" {{$checked}}> <label for="attrvarval_{{$singleVarPoption['attribute_id']}}_{{$singleVarPoption['id']}}">{{$singleVarPoption['name']}}</label> <input type="number" steps="0.01" name="selectedattrvalprice[]" class="form-control" id="attrvalforprice_{{$singleVarPoption['attribute_id']}}_{{$singleVarPoption['id']}}" value="{{$priceval}}" @if($checked!='checked') style="display:none; "@endif> <br>
                                                                    @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="display:none;">
                                                    <label class="col-md-3 control-label" for="Attributes">Other Attributes</label>
                                                    <div  class="col-md-9">
                                                        @php
                                                            $j=0;
                                                            $allAvailAttr = count($allAttributes);
                                                            if($allAvailAttr>10){
                                                                $allAvailAttr =10;
                                                            }
                                                        @endphp
                                                        @if(!empty($allOtherAttr))
                                                            @foreach($allOtherAttr as $selectedAtr)
                                                                <div class="row" id="display_attr__{{$j}}">
                                                                    <div class="col-md-4 attrsec">
                                                                        <select name="attributes[]" class="form-control selectattributes">
                                                                            <option value="">Select Attributes</option>
                                                                            @if(!empty($allAttributeSet))
                                                                                @foreach($allAttributeSet as $singleAttrSet)
                                                                                    @php
                                                                                    $allAttrs = (isset($attrArray[$singleAttrSet->id]))?$attrArray[$singleAttrSet->id]:array();
                                                                                    @endphp
                                                                                    <optgroup label="{{$singleAttrSet->name}}">
                                                                                        @if(is_array($allAttrs) && !empty($allAttrs))
                                                                                            @foreach($allAttrs as $singleAttr)
                                                                                                <option value="{{$singleAttr['id']}}" @if($selectedAtr==$singleAttr['id']) selected="selected" @endif>{{$singleAttr['name']}}</option>
                                                                                            @endforeach
                                                                                        @endif
                                                                                    </optgroup>
                                                                                @endforeach
                                                                            @endif                                  
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-6 attribute_value">
                                                                        @php
                                                                        $allAvailableOptionArr = (isset($attrValuesArray[$selectedAtr]))?$attrValuesArray[$selectedAtr]:array();
                                                                        $allOtherAttrSelectedArr = (isset($allOtherAttrSelected[$selectedAtr]))?$allOtherAttrSelected[$selectedAtr]:array();
                                                                        @endphp
                                                                        @if(!empty($allAvailableOptionArr))
                                                                            @foreach($allAvailableOptionArr as $singleOtherAttr)         
                                                                                <input type="checkbox" name="selectedattrval[]" id="attrval_{{$selectedAtr}}_{{$singleOtherAttr['id']}}" value="{{$selectedAtr}}__{{$singleOtherAttr['id']}}" class="" @if(in_array($singleOtherAttr['id'],$allOtherAttrSelectedArr)) checked @endif> <label for="attrval_{{$selectedAtr}}_{{$singleOtherAttr['id']}}">{{$singleOtherAttr['name']}}</label><br>                
                                                                            @endforeach
                                                                        @endif 
                                                                    </div>
                                                                    <br>
                                                                    <br>
                                                                </div>
                                                                @php
                                                                    $j++;
                                                                @endphp
                                                            @endforeach
                                                        @endif

                                                        @for($i=$j;$i<$allAvailAttr; $i++)
                                                            <div class="row" id="display_attr__{{$i}}" style="display:none">
                                                                <div class="col-md-4 attrsec">
                                                                    <select name="attributes[]" class="form-control selectattributes">
                                                                        <option value="">Select Attributes</option>
                                                                        @if(!empty($allAttributeSet))
                                                                            @foreach($allAttributeSet as $singleAttrSet)
                                                                                @php
                                                                                $allAttrs = (isset($attrArray[$singleAttrSet->id]))?$attrArray[$singleAttrSet->id]:array();
                                                                                @endphp
                                                                                <optgroup label="{{$singleAttrSet->name}}">
                                                                                    @if(is_array($allAttrs) && !empty($allAttrs))
                                                                                        @foreach($allAttrs as $singleAttr)
                                                                                            <option value="{{$singleAttr['id']}}">{{$singleAttr['name']}}</option>
                                                                                        @endforeach
                                                                                    @endif
                                                                                </optgroup>
                                                                            @endforeach
                                                                        @endif                                  
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6 attribute_value">
        
                                                                </div>
                                                                <br>
                                                                <br>
                                                            </div>
                                                        @endfor
                                                        <br>
                                                        <a href="javascript:void(0)" class="add-more-attribute" data-displayed="{{count($allOtherAttr)}}">Add More</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- ATTRIBUTES TAB END -->
                                            <!-- RELATED TAB START -->
                                            <div class="tab-pane" id="related">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="related_products">Related Products</label>
                                                    <div class="col-md-9">
                                                        <select id="related_products" name="related_products[]" class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
                                                            @if(!empty($allAvailableProducts))
                                                                @foreach($allAvailableProducts as $singleAvailItem)
                                                                    <option value="{{$singleAvailItem->id}}" @if(in_array($singleAvailItem->
                                                                    id,$selectedFeaturedProducts)) selected @endif >{{$singleAvailItem->name}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- RELATED TAB END -->
                                            <!-- SEO TAB START -->
                                            <div class="tab-pane" id="seo">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="meta_title">Meta Title</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="meta_title" name="meta_title" class="form-control" placeholder="Meta Title" value="{{$record->meta_title}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="meta_key">Meta Keywords</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="meta_key" name="meta_key" class="form-control" placeholder="Meta Keywords" value="{{$record->meta_key}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="meta_desc">Meta Description</label>
                                                    <div class="col-md-9">
                                                        <textarea id="meta_desc" name="meta_desc" rows="4" class="form-control" placeholder="Meta Description">{{$record->meta_desc}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- SEO TAB END -->
                                            @if(!empty($allLanguages) && count($allLanguages)>0)
                                                @foreach($allLanguages as $singleLanguage)
                                                @php
                                                    $languageData = (isset($allOtherLang[$singleLanguage->code]))?$allOtherLang[$singleLanguage->code]:array();
                                                @endphp
                                                <input type="hidden" name="language[]" value="{{$singleLanguage->code}}">
                                                <div class="tab-pane" id="language_{{$singleLanguage->code}}">
                                                    <h2>{{$singleLanguage->name}}</h2>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="name_{{$singleLanguage->code}}">Title</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="name_{{$singleLanguage->code}}" name="name_{{$singleLanguage->code}}"  class="form-control" placeholder="Title" value="{{(isset($languageData['name'])?$languageData['name']:'')}}"></textarea>
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="short_description_{{$singleLanguage->code}}">Short Description</label>
                                                        <div class="col-md-9">
                                                            <textarea id="short_description_{{$singleLanguage->code}}" name="short_description_{{$singleLanguage->code}}" rows="2" class="form-control" placeholder="Short Description">{{(isset($languageData['short_description'])?$languageData['short_description']:'')}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="content_{{$singleLanguage->code}}">Description</label>
                                                        <div class="col-md-9">
                                                            <textarea id="content_{{$singleLanguage->code}}" name="content_{{$singleLanguage->code}}" rows="2" class="form-control ckeditor" placeholder="Description">{{(isset($languageData['content'])?$languageData['content']:'')}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="more_info_{{$singleLanguage->code}}">More Info</label>
                                                        <div class="col-md-9">
                                                            <textarea id="more_info_{{$singleLanguage->code}}" name="more_info_{{$singleLanguage->code}}" rows="2" class="form-control ckeditor" placeholder="More Info">{{(isset($languageData['more_info'])?$languageData['more_info']:'')}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i>Submit</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i>Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
    <!-- END Form Example with Blocks in the Grid -->
</div>
<!-- END Page Content -->
<div style="display: none;">
    @if(!empty($allAttributes))
        @foreach($allAttributes as $sigAttr)
        <span id="attributeval__{{$sigAttr->id}}">
            {{$sigAttr->values}}
        </span>
        @endforeach
    @endif
    @if(!empty($attrValuesArray))
        @foreach($attrValuesArray as $key=> $sigAttrVal)
        <span id="singleattributeval__{{$key}}">
            @php
            $valStr = '';
            $xx=0;
            @endphp
            @if(!empty($sigAttrVal))
                @foreach($sigAttrVal as $val)                    
                    @php
                    if($xx==0){
                        $valStr .= $val['id'].'=='.$val['name'];
                    }else{
                        $valStr .= '##~~##'.$val['id'].'=='.$val['name'];
                    }
                    $xx++;                        
                    @endphp
                @endforeach
            @endif
            {{$valStr}}
        </span>
        @endforeach
    @endif
</div>
@endsection