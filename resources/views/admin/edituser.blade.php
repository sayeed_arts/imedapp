@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Edit User
                    <span><a href="{{url('admin/users')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/users')}}"><i class="fa fa-table"></i> Users</a></li>
            <li>Edit User</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/users/update')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        <input type="hidden" name="currid" value="{{$record->id}}">
                        <input type="hidden" name="old_is_approved" value="{{$record->is_approved}}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">First Name</label>
                            <div class="col-md-9">
                                <input type="text" id="name" name="name" class="form-control" required placeholder="First Name" value="{{$record->name}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="last_name">Last Name</label>
                            <div class="col-md-9">
                                <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name" value="{{$record->last_name}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">Email</label>
                            <div class="col-md-9">
                                <input type="email" id="email" name="email" class="form-control" required placeholder="Email" value="{{$record->email}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Email Verified</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="verified_radio1">
                                    <input type="radio" id="verified_radio1" name="verified" value="1" @if($record->verified==1) checked @endif > Yes
                                </label>
                                <label class="radio-inline" for="verified_radio2">
                                    <input type="radio" id="verified_radio2" name="verified" value="0" @if($record->verified==0) checked @endif > No
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="phone_number">Phone Number</label>
                            <div class="col-md-9">
                                <input type="text" id="phone_number" name="phone_number" class="form-control" placeholder="Phone Number" value="{{$record->phone_number}}">
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-md-3 control-label">Phone Verified</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="phone_verified_radio1">
                                    <input type="radio" id="phone_verified_radio1" name="phone_verified" value="1" @if($record->phone_verified==1) checked @endif > Yes
                                </label>
                                <label class="radio-inline" for="phone_verified_radio2">
                                    <input type="radio" id="phone_verified_radio2" name="phone_verified" value="0" @if($record->phone_verified==0) checked @endif > No
                                </label>
                            </div>
                        </div> -->
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">User Status</label>
                            <div class="col-md-9">
                                <label class="radio-inline" for="display_status_radio1">
                                    <input type="radio" id="display_status_radio1" name="is_approved" value="1" @if($record->is_approved==1) checked @endif > Active
                                </label>
                                <label class="radio-inline" for="display_status_radio2">
                                    <input type="radio" id="display_status_radio2" name="is_approved" value="0" @if($record->is_approved==0) checked @endif > Inactive
                                </label>
                            </div>
                        </div>
                        
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection