@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Enquiry Detail
                    </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/enquiries')}}"><i class="fa fa-table"></i> Enquiries</a></li>
            <li>Enquiry Details</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/vendors/save')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        {{ csrf_field() }}
                 
                   <div class="form-group">
                                <label  for="email"><span>Email</span></label>
                                <input name="email" placeholder="User Email" id="email" value="<?=isset($vendordetail->email)?$vendordetail->email:''?>" type="email"
                                    class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label  for="organization"><span>Organization</span></label>
                                <input name="organization" placeholder="Organization" id="organization" value="<?=isset($vendordetail->organization)?$vendordetail->organization:''?>"
                                    type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="phone"><span>Phone</span></label>
                                <input name="phone" id="phone" placeholder="Phone Number" value="<?=isset($vendordetail->phone)?$vendordetail->phone:''?>" type="text"
                                    class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="comment"><span>Additional Details</span></label>
                                <textarea id="comment"
                                    placeholder="List specific &amp; unique requirements here, such as: Configuration, software rev, options, accessories, etc."
                                    name="comment" class="form-control"><?=isset($vendordetail->comment)?$vendordetail->comment:''?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="qty"><span>Quantity</span></label>
                                <input name="qty" placeholder="Quantity" id="qty" value="<?=isset($vendordetail->qty)?$vendordetail->qty:''?>" type="number"
                                    class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="name"><span>First Name</span></label>
                                <input name="firstname" placeholder="First Name" id="firstname" value="<?=isset($vendordetail->firstname)?$vendordetail->firstname:''?>"
                                    class="form-control" type="text" required>
                            </div>
                            <div class="form-group">
                                <label for="lastname"><span>Last Name</span></label>
                                <input name="lastname" placeholder="Last Name" id="lastname" value="<?=isset($vendordetail->lastname)?$vendordetail->lastname:''?>"
                                    class="form-control" type="text" required>
                            </div>
                            <div class="form-group">
                                <label for="street"><span>Address</span></label>
                                <input name="street" id="street" placeholder="Address" value="<?=isset($vendordetail->street)?$vendordetail->street:''?>" type="text"
                                    class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="city"><span>City</span></label>
                                <input name="city" id="city" placeholder="City" value="<?=isset($vendordetail->city)?$vendordetail->city:''?>" type="text"
                                    class="form-control" required>
                            </div>
                            
                            <div class="form-group">
                                <label for="country"><span>Country</span></label>
                                <inout name="country" id="country_yo" placeholder="Country" value="<?=isset($vendordetail->countryname)?$vendordetail->countryname:''?>" class="form-control" required>
                                                               </div>
                            <div class="form-group">
                                <label for="state"><span>State</span></label>
                                <input name="state" id="state_yo"
                                    class="form-control" value="<?=isset($vendordetail->stateName)?$vendordetail->stateName:''?>">
                            </div>
                            <div class="form-group">
                                <label for="zipcode"><span>Zip</span></label>
                                <input name="zip" id="zip" placeholder="Zip Code" value="<?=isset($vendordetail->zip)?$vendordetail->zip:''?>" type="text"
                                    class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="personalnotes"><span>Personal Notes</span></label>
                                <textarea id="personalnotes" placeholder="Personal Notes" class="form-control"
                                    name="personalnotes"><?=isset($vendordetail->personalnotes)?$vendordetail->personalnotes:''?></textarea>
                            </div>

                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection

