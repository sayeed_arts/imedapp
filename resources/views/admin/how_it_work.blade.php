@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Home Page Sections
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Home Page Sections</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/updatehowitworks')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                    	<input type="hidden" name="old_banner_image" value="{{$record->banner_image}}">
                    	<input type="hidden" name="old_section1_image" value="{{$record->section1_image}}">
                    	<input type="hidden" name="old_section2_image" value="{{$record->section2_image}}">
                    	<input type="hidden" name="old_section3_image" value="{{$record->section3_image}}">
                    	<input type="hidden" name="old_section5_image" value="{{$record->section5_image}}">
                    	<input type="hidden" name="old_section6_image" value="{{$record->section6_image}}">
                    	<input type="hidden" name="old_section7_image" value="{{$record->section7_image}}">
                        {{ csrf_field() }}
                        <!-- <div class="form-group">
                            <label class="col-md-3 control-label" for="banner_image">Image</label>
                            <div class="col-md-9">
                                <input type="file" id="banner_image" name="banner_image" class="form-control">
                                @if(!empty($record->banner_image))
                                <img src="{{ asset($record->banner_image)}}" alt="Image" height="150" width="150">
                                @endif
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-md-12"><h2>How It Works</h2></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="banner_title">Title</label>
                            <div class="col-md-9">
                                <input type="text" id="banner_title" required name="banner_title" class="form-control" placeholder="Title"  value="{{$record->banner_title}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="banner_caption">Caption</label>
                            <div class="col-md-9">
                                <textarea id="banner_caption" name="banner_caption" class="form-control" placeholder="Caption">{{$record->banner_caption}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section1_image">Section 1 Image</label>
                            <div class="col-md-9">
                                <input type="file" id="section1_image" name="section1_image" class="form-control">
                                @if(!empty($record->section1_image))
                                <img src="{{ asset($record->section1_image)}}" alt="Image" height="150" width="150">
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section1_title">Section 1 Title</label>
                            <div class="col-md-9">
                                <input type="text" id="section1_title" name="section1_title" class="form-control" placeholder="Section 1 Title"  value="{{$record->section1_title}}">
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section1_content">Section 1 Content</label>
                            <div class="col-md-9">
                                <textarea id="section1_content" name="section1_content" class="form-control" placeholder="Section 1 Content">{{$record->section1_content}}</textarea>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section2_image">Section 2 Image</label>
                            <div class="col-md-9">
                                <input type="file" id="section2_image" name="section2_image" class="form-control">
                                @if(!empty($record->section2_image))
                                <img src="{{ asset($record->section2_image)}}" alt="Image" height="150" width="150">
                                @endif
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section2_title">Section 2 Title</label>
                            <div class="col-md-9">
                                <input type="text" id="section2_title" name="section2_title" class="form-control" placeholder="Section 2 Title"  value="{{$record->section2_title}}">
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section2_content">Section 2 Content</label>
                            <div class="col-md-9">
                                <textarea id="section2_content" name="section2_content" class="form-control" placeholder="Section 2 Content">{{$record->section2_content}}</textarea>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section3_image">Section 3 Image</label>
                            <div class="col-md-9">
                                <input type="file" id="section3_image" name="section3_image" class="form-control">
                                @if(!empty($record->section3_image))
                                <img src="{{ asset($record->section3_image)}}" alt="Image" height="150" width="150">
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section3_title">Section 3 Title</label>
                            <div class="col-md-9">
                                <input type="text" id="section3_title" name="section3_title" class="form-control" placeholder="Section 3 Title"  value="{{$record->section3_title}}">
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section3_content">Section 3 Content</label>
                            <div class="col-md-9">
                                <textarea id="section3_content" name="section3_content" class="form-control" placeholder="Section 3 Content">{{$record->section3_content}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12"><h2>The RateGuard Advantage</h2></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section4_title">Title</label>
                            <div class="col-md-9">
                                <input type="text" id="section4_title" name="section4_title" class="form-control" placeholder="Title"  value="{{$record->section4_title}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section4_content_left">Content 1</label>
                            <div class="col-md-9">
                                <textarea id="section4_content_left" name="section4_content_left" class="form-control" placeholder="Content 1">{{$record->section4_content_left}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section4_content_right">Content 2</label>
                            <div class="col-md-9">
                                <textarea id="section4_content_right" name="section4_content_right" class="form-control" placeholder="Content 2">{{$record->section4_content_right}}</textarea>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-12"><h2>Trusted Lenders, Low Rates & A Free Service Built Just For You</h2></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section8_content_right">Title</label>
                            <div class="col-md-9">
                                <input type="text" id="section8_content_right" name="section8_content_right" class="form-control" placeholder="Title"  value="{{$record->section8_content_right}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section5_image">Section 1 Image</label>
                            <div class="col-md-9">
                                <input type="file" id="section5_image" name="section5_image" class="form-control">
                                @if(!empty($record->section5_image))
                                <img src="{{ asset($record->section5_image)}}" alt="Image" height="150" width="150">
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section5_title">Section 1 Title</label>
                            <div class="col-md-9">
                                <input type="text" id="section5_title" name="section5_title" class="form-control" placeholder="Section 1 Title"  value="{{$record->section5_title}}">
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section5_content">Section 1 Content</label>
                            <div class="col-md-9">
                                <textarea id="section5_content" name="section5_content" class="form-control" placeholder="Section 1 Content">{{$record->section5_content}}</textarea>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section6_image">Section 2 Image</label>
                            <div class="col-md-9">
                                <input type="file" id="section6_image" name="section6_image" class="form-control">
                                @if(!empty($record->section6_image))
                                <img src="{{ asset($record->section6_image)}}" alt="Image" height="150" width="150">
                                @endif
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section6_title">Section 2 Title</label>
                            <div class="col-md-9">
                                <input type="text" id="section6_title" name="section6_title" class="form-control" placeholder="Section 2 Title"  value="{{$record->section6_title}}">
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section6_content">Section 2 Content</label>
                            <div class="col-md-9">
                                <textarea id="section6_content" name="section6_content" class="form-control" placeholder="Section 2 Content">{{$record->section6_content}}</textarea>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section7_image">Section 3 Image</label>
                            <div class="col-md-9">
                                <input type="file" id="section7_image" name="section7_image" class="form-control">
                                @if(!empty($record->section7_image))
                                <img src="{{ asset($record->section7_image)}}" alt="Image" height="150" width="150">
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section7_title">Section 3 Title</label>
                            <div class="col-md-9">
                                <input type="text" id="section7_title" name="section7_title" class="form-control" placeholder="Section 3 Title"  value="{{$record->section7_title}}">
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section7_content">Section 3 Content</label>
                            <div class="col-md-9">
                                <textarea id="section7_content" name="section7_content" class="form-control" placeholder="Section 3 Content">{{$record->section7_content}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12"><h2>About Us</h2></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section8_title">Title</label>
                            <div class="col-md-9">
                                <input type="text" id="section8_title" name="section8_title" class="form-control" placeholder="Title"  value="{{$record->section8_title}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="section8_content_left">Content</label>
                            <div class="col-md-9">
                                <textarea id="section8_content_left" name="section8_content_left" class="form-control" placeholder="Content">{{$record->section8_content_left}}</textarea>
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection