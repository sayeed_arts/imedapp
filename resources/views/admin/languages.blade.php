@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Languages
                    
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Languages</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            
            @if(isAllowed($current_user->id,$current_user->allowed_sections,'86'))
                <form method="POST" action="{{ route('translations.create') }}">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <label>Key:</label>
                            <input type="text" name="key" class="form-control" placeholder="Enter Key...">
                        </div>
                        <div class="col-md-4">
                            <label>Value:</label>
                            <input type="text" name="value" class="form-control" placeholder="Enter Value...">
                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-success">Add</button>
                        </div>
                    </div>
                </form>
            @endif

            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th>Key</th>
                    @if($languages->count() > 0)
                        @foreach($languages as $language)
                            <th>{{ $language->name }}({{ $language->code }})</th>
                        @endforeach
                    @endif
                    <th width="80px;">Action</th>
                </tr>
                </thead>
                <tbody>
                    @if($columnsCount > 0)
                        @foreach($columns[0] as $columnKey => $columnValue)
                            <tr>
                                <td><a href="#" class="translate-key" data-title="Enter Key" data-type="text" data-pk="{{ $columnKey }}" data-url="{{ route('translation.update.json.key') }}">{{ $columnKey }}</a></td>
                                @for($i=1; $i<=$columnsCount; ++$i)
                                <td><a href="#" data-title="Enter Translate" class="translate" data-code="{{ $columns[$i]['lang'] }}" data-type="textarea" data-pk="{{ $columnKey }}" data-url="{{ route('translation.update.json') }}">{{ isset($columns[$i]['data'][$columnKey]) ? $columns[$i]['data'][$columnKey] : '' }}</a></td>
                                @endfor
                                <td>
                                    @if(isAllowed($current_user->id,$current_user->allowed_sections,'87'))
                                    <button data-action="{{ route('translations.destroy', $columnKey) }}" class="btn btn-danger btn-xs remove-key">Delete</button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->    
@endsection