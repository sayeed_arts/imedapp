@extends('layouts.adminapp')

@section('content')
@php
$current_user = Auth::user();
@endphp
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                Order Details
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('admin/orders')}}"><i class="fa fa-table"></i> Orders</a></li>
        <li>Order Details</li>
    </ul>
    <!-- END Datatables Header -->
    <!-- Datatables Content -->
    <div class="block full">
        <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
        @if (Session::has('message'))
        {!! successMesaage(Session::get('message')) !!}
        @endif
        {!! validationError($errors) !!}


        <div class="row">
            <div class="col-md-8">
                <b>Order#</b> SO-IMED-{{$orderData->id}}<br>
                <b>Ordered on</b> {{displayDateWithTimeMD($orderData->ordered_on)}}<br>
                <!-- <b>Order Status</b>
                @if($orderData->status=='3')
                Canceled
                @elseif($orderData->status=='2')
                Successfull
                @elseif($orderData->status=='1')
                Failed
                @elseif($orderData->status=='0')
                Pending
                @endif -->
                @if(!empty($orderData->shipped_on))
                <b>Shipped on</b> {{displayDateWithTimeMD($orderData->shipped_on)}}
                @endif
            </div>
            <div class="col-md-4">
                @if($orderData->status==2)
                <form action="{{route('order-shipping-status')}}" method="post" name="shippingform" id="shippingform">
                    @csrf
                    <input type="hidden" name="order_id" value="{{$orderData->order_id}}">
                    <label for="order_shipping_status">Order Status</label>
                    <select name="shipping_status" id="order_shipping_status" class="form-control">
                        <option value="0" @if($orderData->shipping_status==0) selected @endif>Received</option>
                        <option value="3" @if($orderData->shipping_status==3) selected @endif>Processing</option>
                        <option value="2" @if($orderData->shipping_status==2) selected @endif>Voided </option>
                        <option value="1" @if($orderData->shipping_status==1) selected @endif>Shipped</option>
                        <option value="4" @if($orderData->shipping_status==4) selected @endif>Completed</option>
                    </select>
                </form><br>
                @endif

                @if($orderData->shipping_status!=0 && $orderData->shipping_status!=2 && $orderData->shipping_status!=3 )
                <p>
                    <a href="{{url('admin/get-invoice/'.$orderData->order_id)}}" target="_blank"> <i class="fa fa-print"
                            aria-hidden="true"></i>
                        @if($orderData->payment_option==1)
                        View Sales Order
                        @else
                        View Sales Order
                        @endif
                    </a>
                </p>
                <p>
                    <a href="{{url('admin/packing-slip/'.$orderData->order_id)}}" target="_blank"> <i
                            class="fa fa-print" aria-hidden="true"></i>
                        @if($orderData->payment_option==1)
                        View Packing Slip
                        @else
                        View Packing Slip
                        @endif
                    </a>
                </p>

                @endif
                <!-- @if($orderData->shipping_status!=0 && $orderData->shipping_status!=2 && $orderData->shipping_status!=3 )
                <p><a href="{{url('admin/get-invoice/'.$orderData->order_id)}}" target="_blank"> <i class="fa fa-print"
                            aria-hidden="true"></i>{{__('View Purchase Order')}} </a></p>
                @else if($orderData->shipping_status!=0 && $orderData->shipping_status!=2 &&
                $orderData->shipping_status!=3 )
                <p><a href="{{url('admin/get-invoice/'.$orderData->order_id)}}" target="_blank"> <i class="fa fa-print"
                            aria-hidden="true"></i>View Invoice </a></p>
                @endif -->

            </div>
            <div class="col-md-12">
                <div class="po-order-details">
                    @if($orderData->payment_option==1)
                    <a class="btn mb-3 btn-primary" data-toggle="collapse" href="#collapseCustomerOrder" role="button"
                        aria-expanded="false" aria-controls="collapseExample">
                        Customer Purchase Oder
                    </a>
                    <div class="collapse" id="collapseCustomerOrder">
                        <div class="row" id="">
                            <div class="col-md-12">
                                @if(empty($orderData->po_order_number))
                                <form method="POST" class="row text-center jqueryValidate"
                                    action="{{ route('admin-update-po') }}" enctype="multipart/form-data">
                                    <input type="hidden" name="currentorderid"
                                        value="{{base64_encode($orderData->order_id)}}">
                                    <input type="hidden" name="old_po_order_file" value="{{$orderData->po_order_file}}">
                                    @csrf
                                    <div class="col-sm-6">
                                        <div class="form-group text-left">
                                            <label>{{__('Enter Customer Purchase Order Number')}}</label>
                                            <input type="text" name="po_order_number" id="po_order_number"
                                                class="form-control {{ $errors->has('po_order_number') ? ' is-invalid' : '' }}"
                                                required value="{{$orderData->po_order_number }}">
                                            @if ($errors->has('po_order_number'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('po_order_number') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group text-left">
                                            <label>{{__('Upload Customer Purchase Order File')}}</label>
                                            <input type="file" name="po_order_file" id="po_order_file"
                                                class="form-control {{ $errors->has('po_order_file') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('po_order_file'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('po_order_file') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group text-left">
                                            <label>{{__('Wire Bank information “if applicable”')}}</label>
                                            <textarea name="po_order_qty" id="po_order_qty"
                                                class="form-control {{ $errors->has('po_order_qty') ? ' is-invalid' : '' }}">{{ $orderData->po_order_qty }}</textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-4 offset-md-4 text-center">
                                        <button type="submit"
                                            class="btn btn-success getStarted btn-block">{{__('Submit')}}</button>
                                    </div>
                                </form>
                                <p>&nbsp;</p>
                                @else
                                <div class="row" style="background: #f5f5f5; padding: 20px 20px 10px 20px;">

                                    <div class="col-sm-4">
                                        <div class="form-group text-left">
                                            <label><b>{{__('Customer Purchase Order Number')}}</b></label>
                                            <p>{{$orderData->po_order_number }}</p>
                                            <!-- <input type="text" name="po_order_number" id="po_order_number" class="form-control"
                                    value="{{$orderData->po_order_number }}" readonly> -->
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group text-left">
                                            <label><b> {{__('Customer Purchase Order File')}}</b> </label><br>
                                            @if(!empty($orderData->po_order_file))
                                            <a class="btn btn-primary" href="{{url($orderData->po_order_file)}}"
                                                target="_blank">View</a>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group text-left">
                                            <label><b>{{__('Wire Bank information “if applicable”')}}</b></label>
                                            <p>{{ $orderData->po_order_qty }}</p>
                                            <!-- <textarea name="po_order_qty" id="po_order_qty" class="form-control"
                                    readonly>{{ $orderData->po_order_qty }}</textarea> -->
                                        </div>
                                    </div>


                                </div>
                                <p>&nbsp;</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        @if($orderData->ship_to_different_address==1)
                        <th class="text-center" style="width:50%">Billing Details</th>
                        @endif
                        <th class="text-center" style="width:50%">Shipping Details</th>
                        <th class="text-center" style="width:50%">Payment Method</th>
                    </tr>
                </thead>
                <tbody>
                    @if($orderData->ship_to_different_address==1)
                    <td>
                        {{$orderData->first_name.' '.$orderData->last_name}}<br>
                        @if(!empty($orderData->company))
                        {{$orderData->company}}<br>
                        @endif
                        {{$orderData->street_address.', '.$orderData->apartment}}<br>
                        {{$orderData->town.', '.$billingState}}<br>
                        {{$billingCountry.', '.$orderData->postcode}}<br>
                        Email {{$orderData->email_address}}<br>
                        Contact No. {{$orderData->phone}}
                    </td>
                    @endif
                    <td>
                        {{$orderData->shipping_first_name.' '.$orderData->shipping_last_name}}<br>
                        @if(!empty($orderData->shipping_company))
                        {{$orderData->shipping_company}}<br>
                        @endif
                        {{$orderData->shipping_street_address.', '.$orderData->shipping_apartment}}<br>
                        {{$orderData->shipping_town.', '.$shippingState}}<br>
                        {{$shippingCountry.', '.$orderData->shipping_postcode}}<br><br>
                        Contact No. {{$orderData->shipping_phone}}
                    </td>
                    <td class="text-center">
                        @if($orderData->payment_option==1)

                        @if(empty($orderData->po_order_number))
                        Customer Purchase Order : <b> No Updated</b>
                        @else
                        Customer Purchase Order #:<b> {{$orderData->po_order_number }}</b>
                        @endif

                        @else
                        PayPal
                        @endif
                        <!-- <p>@if($orderData->payment_option==1)
                            {{__('Purhase Order Number')}}
                            @else
                            {{__('Invoice Number')}}
                            @endif
                            #:{{$orderData->id}}
                        </p> -->

                    </td>
                </tbody>
            </table>
            <table class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">Product</th>
                        <th class="text-center">Quantity</th>
                        <th class="text-center">Price</th>
                    </tr>
                </thead>
                <tbody>
                    @if($orderProducts)
                    @foreach ($orderProducts as $singleData)
                    <tr>
                        <td class="text-center">
                            <a href="{{url('product/'.$singleData->slug)}}" target="_blank">{{ $singleData->name }}</a>
                            @if(!empty($singleData->product_attr_name))
                            <h5>{{$singleData->product_attr_name}}</h5>
                            @endif
                        </td>
                        <td class="text-center">{{ $singleData->quantity }}</td>
                        <td class="text-center">{{ showAdminPrice($singleData->total_price) }}</td>
                    </tr>
                    @endforeach
                    @endif
                    <tr>
                        <td colspan="2" class="text-right"><b>Sub Total</b></td>
                        <td class="text-center">{{showAdminPrice($orderData->subtotal)}}</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-right"><b>Tax</b></td>
                        <td class="text-center">{{showAdminPrice($orderData->tax)}}</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-right"><b>Discount</b></td>
                        <td class="text-center">{{showAdminPrice($orderData->coupon_discount)}}</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-right"><b>Shipment</b></td>
                        <td class="text-center">{{showAdminPrice($orderData->shipping)}}</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-right"><b>Grand Total</b></td>
                        <td class="text-center"><b>{{showAdminPrice($orderData->total)}}</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
<!-- END Page Content -->
@endsection