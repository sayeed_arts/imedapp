@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    All Users
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>All Users</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive1">
                <form action="{{url('admin/reports')}}" method="post">
                    @csrf
                    <table class="table table-vcenter">
                        <tbody>
                             <tr>
                                <td>From Date</td>
                                <td>
                                    <input type="date" id="start_date" name="start_date" required class="form-control" value="{{$start_date}}" placeholder="From Date">
                                </td>
                                <td>To Date</td>
                                <td> 
                                    <input type="date" id="end_date" name="end_date" required class="form-control" value="{{$end_date}}" placeholder="To Date">
                                </td>
                                <td>
                                    <select name="export_type" id="export_type" class="form-control" required>
                                        <option value="">Report Type</option>
                                        <option value="1" @if($export_type==1) selected @endif>All Users</option>
                                        <option value="2" @if($export_type==2) selected @endif>PO Orders</option>
                                        <option value="3" @if($export_type==3) selected @endif>Sales Orders</option>
                                    </select>
                                </td>
                                <td id="orderstatusdiv" @if($export_type==1) style="display: none" @endif >
                                    <select name="export_order_status" id="export_order_status" class="form-control">
                                        <option value="">Order Status</option>
                                        <option value="-1" @if($export_order_status==-1) selected @endif>Received</option>
                                        <option value="1" @if($export_order_status==1) selected @endif>Shipped</option>
                                        <option value="2" @if($export_order_status==2) selected @endif>Voided</option>
                                        <option value="3" @if($export_order_status==3) selected @endif>Processing</option>
                                        <option value="4" @if($export_order_status==4) selected @endif>Completed</option>
                                    </select>
                                </td>
                                <td><input type="submit" name="export" value="Search" class="btn btn-success"></td>
                                <td><input type="submit" name="export" value="Export" class="btn btn-success"></td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>                            
                            <th class="text-center">Order No</th>
                            <th class="text-center">Transaction Id</th>
                            <th class="text-center">Name</th> 
                            <th class="text-center">Email</th>
                            <th class="text-center">Phone</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Sub Total</th>
                            <th class="text-center">Tax</th>
                            <th class="text-center">Discount</th>
                            <th class="text-center">Shipping</th>
                            <th class="text-center">Total</th>
                            <th class="text-center">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>
                                    <td class="text-center">PRO-IMED-{{ $singleData->id }}</td>
                                    <td class="text-center">{{ $singleData->transaction_id }}</td>
                                    <td class="text-center">{{ $singleData->customer_first_name }} {{ $singleData->customer_last_name }}</td>
                                    <td class="text-center">{{ $singleData->email_address }}</td>                                    
                                    <td class="text-center">{{ $singleData->phone }}</td>
                                    <td class="text-center">
                                        @if($singleData->shipping_status==0)
                                        Received
                                        @elseif($singleData->shipping_status==1)
                                        Shipped
                                        @elseif($singleData->shipping_status==2)
                                        Voided
                                        @elseif($singleData->shipping_status==3)
                                        Processing
                                        @elseif($singleData->shipping_status==4)
                                        Completed
                                        @endif
                                    </td> 
                                    <td class="text-center">{{ showAdminPrice($singleData->subtotal) }}</td> 
                                    <td class="text-center">{{ showAdminPrice($singleData->tax) }}</td> 
                                    <td class="text-center">{{ showAdminPrice($singleData->coupon_discount) }}</td> 
                                    <td class="text-center">{{ showAdminPrice($singleData->shipping) }}</td> 
                                    <td class="text-center">{{ showAdminPrice($singleData->total) }}</td> 
                                    <td class="text-center">{{ $singleData->ordered_on }}</td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="10" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->
@endsection