@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    All Care Seekers
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>All Care Seekers</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive1">
                <form action="{{url('admin/reports')}}" method="post">
                    @csrf
                    <table class="table table-vcenter">
                        <tbody>
                            <tr>
                                <td>
                                    <input type="text" id="start_date" name="start_date" required class="form-control" value="{{$start_date}}" >
                                </td>
                                <td>
                                    <select name="export_type" id="export_type" class="form-control" required>
                                        <option value="">Select</option>
                                        <option value="1" @if($export_type==1) selected @endif>All Users</option>
                                        <option value="2" @if($export_type==2) selected @endif>All Care Givers</option>
                                        <option value="3" @if($export_type==3) selected @endif>All Care Seekers</option>
                                        <!-- <option value="4" @if($export_type==4) selected @endif>All Jobs</option>
                                        <option value="5" @if($export_type==5) selected @endif>All Senior Jobs</option>
                                        <option value="6" @if($export_type==6) selected @endif>All Child Jobs</option> -->
                                        <option value="7" @if($export_type==7) selected @endif>All Payments</option>
                                        <option value="8" @if($export_type==8) selected @endif>Background Check Payments</option>
                                    </select>
                                </td>
                                <td><input type="submit" name="export" value="Search" class="btn btn-success"></td>
                                <td><input type="submit" name="export" value="Export" class="btn btn-success"></td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Name</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Phone</th>
                            <th class="text-center">DOB</th>
                            <th class="text-center">Created</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>
                                    <td class="text-center">{{ $singleData->name }} {{ $singleData->last_name }}</td>
                                    <td class="text-center">{{ $singleData->email }}</td>
                                    <td class="text-center">{{ $singleData->phone_number }}</td>
                                    <td class="text-center">{{ $singleData->dob }}</td>
                                    <td class="text-center">{{ $singleData->created_at }}</td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="6" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->
@endsection