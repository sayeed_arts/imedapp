@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>Send Emial</h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Send Email</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/sendemail')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="sendto">Send To</label>
                            <div class="col-md-9">
                                <select name="sendto" id="sendto" required="" class="form-control">
                                    <!-- <option value="background">Users without background check</option>
                                    <option value="verified_phone">Users without verified phone numbers</option>
                                    <option value="caregivers">Only Care Givers </option>
                                    <option value="careseekers">Only Care Seekers </option> -->
                                    <option value="news">Newsletter Subscribers</option>
                                    
                                    @if(!empty($allRecords))
                                        @foreach($allRecords as $single)
                                            <option value="{{$single->id}}">Mailing List: {{$single->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="subject">Subject</label>
                            <div class="col-md-9">
                                <input type="text" id="subject" name="subject" class="form-control" required placeholder="Subject" value="{{old('subject')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Name</label>
                            <div class="col-md-9">
                                <input type="text" id="name" name="name" class="form-control" required placeholder="From Name" value="{{old('name')}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">From Email</label>
                            <div class="col-md-9">
                                <input type="email" id="email" name="email" class="form-control" required placeholder="Email Address" value="{{old('email')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="content">Content</label>
                            <div class="col-md-9">
                                <textarea id="content" name="content" rows="4" class="form-control ckeditor" placeholder="Content" required>{{old('content')}}</textarea>
                                <br>
                                [NAME] [EMAIL]
                            </div>
                        </div> 

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection