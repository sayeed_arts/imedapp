@extends('layouts.adminapp')

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Forms General Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                General Settings
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Update Settings</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}
                @endif
                {!! validationError($errors) !!}
                <!-- Basic Form Elements Content -->
                <form action="{{url('admin/updatesettings')}}" method="post" enctype="multipart/form-data"
                    class="form-horizontal form-bordered">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="admin_email">Admin Email</label>
                        <div class="col-md-9">
                            <input type="email" required id="admin_email" name="admin_email" class="form-control"
                                placeholder="Admin Email" value="{{$record->admin_email}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="meta_title">Meta Title</label>
                        <div class="col-md-9">
                            <input type="text" required id="meta_title" name="meta_title" class="form-control"
                                placeholder="Meta Title" value="{{$record->meta_title}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="meta_key">Meta Keywords</label>
                        <div class="col-md-9">
                            <input type="text" id="meta_key" name="meta_key" class="form-control"
                                placeholder="Meta Keywords" value="{{$record->meta_key}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="meta_desc">Meta Description</label>
                        <div class="col-md-9">
                            <input type="text" id="meta_desc" name="meta_desc" class="form-control"
                                placeholder="Meta Description" value="{{$record->meta_desc}}">
                        </div>
                    </div>
                    <!-- <div class="form-group">
                            <label class="col-md-3 control-label" for="footer_desc">Footer Description</label>
                            <div class="col-md-9">
                                <input type="text" required id="footer_desc" name="footer_desc" class="form-control" placeholder="Footer Description"  value="{{$record->footer_desc}}">
                            </div>
                        </div> -->
                    <div class="form-group">
                        <label class="col-md-3 control-label">Facebook Status</label>
                        <div class="col-md-9">
                            <label class="radio-inline" for="fb_status_radio1">
                                <input type="radio" id="fb_status_radio1" name="fb_status" value="1"
                                    @if($record->fb_status==1) checked @endif > Active
                            </label>
                            <label class="radio-inline" for="fb_status_radio2">
                                <input type="radio" id="fb_status_radio2" name="fb_status" value="0"
                                    @if($record->fb_status==0) checked @endif > Inactive
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="fb_link">Facebook URL</label>
                        <div class="col-md-9">
                            <input type="url" id="fb_link" name="fb_link" class="form-control"
                                placeholder="https://www.facebook.com" value="{{$record->fb_link}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Google Plus Status</label>
                        <div class="col-md-9">
                            <label class="radio-inline" for="gp_status_radio1">
                                <input type="radio" id="gp_status_radio1" name="gp_status" value="1"
                                    @if($record->gp_status==1) checked @endif > Active
                            </label>
                            <label class="radio-inline" for="gp_status_radio2">
                                <input type="radio" id="gp_status_radio2" name="gp_status" value="0"
                                    @if($record->gp_status==0) checked @endif > Inactive
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="gp_link">Google Plus URL</label>
                        <div class="col-md-9">
                            <input type="url" id="gp_link" name="gp_link" class="form-control"
                                placeholder="https://plus.google.com" value="{{$record->gp_link}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Linkedin Status</label>
                        <div class="col-md-9">
                            <label class="radio-inline" for="li_status_radio1">
                                <input type="radio" id="li_status_radio1" name="li_status" value="1"
                                    @if($record->li_status==1) checked @endif > Active
                            </label>
                            <label class="radio-inline" for="li_status_radio2">
                                <input type="radio" id="li_status_radio2" name="li_status" value="0"
                                    @if($record->li_status==0) checked @endif > Inactive
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="li_link">Linkedin URL</label>
                        <div class="col-md-9">
                            <input type="url" id="tw_link" name="li_link" class="form-control"
                                placeholder="https://www.linkedin.com" value="{{$record->li_link}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Twitter Status</label>
                        <div class="col-md-9">
                            <label class="radio-inline" for="tw_status_radio1">
                                <input type="radio" id="tw_status_radio1" name="tw_status" value="1"
                                    @if($record->tw_status==1) checked @endif > Active
                            </label>
                            <label class="radio-inline" for="tw_status_radio2">
                                <input type="radio" id="tw_status_radio2" name="tw_status" value="0"
                                    @if($record->tw_status==0) checked @endif > Inactive
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="tw_link">Twitter URL</label>
                        <div class="col-md-9">
                            <input type="url" id="tw_link" name="tw_link" class="form-control"
                                placeholder="https://www.twitter.com" value="{{$record->tw_link}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Instagram Status</label>
                        <div class="col-md-9">
                            <label class="radio-inline" for="gp_status_radio1">
                                <input type="radio" id="gp_status_radio1" name="in_status" value="1"
                                    @if($record->in_status==1) checked @endif > Active
                            </label>
                            <label class="radio-inline" for="gp_status_radio2">
                                <input type="radio" id="gp_status_radio2" name="in_status" value="0"
                                    @if($record->in_status==0) checked @endif > Inactive
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="in_link">Instagram URL</label>
                        <div class="col-md-9">
                            <input type="url" id="in_link" name="in_link" class="form-control"
                                placeholder="https://www.instagram.com" value="{{$record->in_link}}">
                        </div>
                    </div>
                    <!-- <div class="form-group">
                            <label class="col-md-3 control-label" for="service_fee">Service Fee (in % )</label>
                            <div class="col-md-9">
                                <input type="number" id="service_fee" name="service_fee" class="form-control" placeholder="Service Fee"  value="{{$record->service_fee}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="stripe_secret_key">Stripe Secret Key</label>
                            <div class="col-md-9">
                                <input type="text" id="stripe_secret_key" name="stripe_secret_key" class="form-control" placeholder="Stripe Secret Key"  value="{{$record->stripe_secret_key}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="stripe_publishable_key">Stripe Publishable Key</label>
                            <div class="col-md-9">
                                <input type="text" id="stripe_publishable_key" name="stripe_publishable_key" class="form-control" placeholder="Stripe Publishable Key"  value="{{$record->stripe_publishable_key}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="sms_api_secret">AMS API Secret</label>
                            <div class="col-md-9">
                                <input type="text" id="sms_api_secret" name="sms_api_secret" class="form-control" placeholder="AMS API Secret"  value="{{$record->sms_api_secret}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="background_check">Background Check Key</label>
                            <div class="col-md-9">
                                <input type="text" id="background_check" name="background_check" class="form-control" placeholder="Background Check Key"  value="{{$record->background_check}}">
                            </div>
                        </div> -->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="contact_address">Contact Address</label>
                        <div class="col-md-9">
                            <textarea id="contact_address" name="contact_address" class="form-control ckeditor"
                                placeholder="Contact Address">{{$record->contact_address}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="contact_phone">Contact Phone Number</label>
                        <div class="col-md-9">
                            <input type="text" id="contact_phone" name="contact_phone" class="form-control"
                                placeholder="Contact Phone Number" value="{{$record->contact_phone}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="contact_email">Contact Email Address</label>
                        <div class="col-md-9">
                            <input type="email" id="contact_email" name="contact_email" class="form-control"
                                placeholder="Contact Email Address" value="{{$record->contact_email}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="amazon_host">SMTP Host</label>
                        <div class="col-md-9">
                            <input type="text" id="amazon_host" name="amazon_host" class="form-control"
                                placeholder="SMTP Host" value="{{$record->amazon_host}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="amazon_username">SMTP Username</label>
                        <div class="col-md-9">
                            <input type="text" id="amazon_username" name="amazon_username" class="form-control"
                                placeholder="SMTP Username" value="{{$record->amazon_username}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="amazon_password">SMTP Password</label>
                        <div class="col-md-9">
                            <input type="text" id="amazon_password" name="amazon_password" class="form-control"
                                placeholder="SMTP Password" value="{{$record->amazon_password}}">
                        </div>
                    </div>
                    <!-- <div class="form-group">
                            <label class="col-md-3 control-label" for="mailgun_host">Mailgun Host</label>
                            <div class="col-md-9">
                                <input type="text" id="mailgun_host" name="mailgun_host" class="form-control" placeholder="Mailgun Host"  value="{{$record->mailgun_host}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="mailgun_username">Mailgun Username</label>
                            <div class="col-md-9">
                                <input type="text" id="mailgun_username" name="mailgun_username" class="form-control" placeholder="Mailgun Username"  value="{{$record->mailgun_username}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="mailgun_password">Mailgun Password</label>
                            <div class="col-md-9">
                                <input type="text" id="mailgun_password" name="mailgun_password" class="form-control" placeholder="Mailgun Password"  value="{{$record->mailgun_password}}">
                            </div>
                        </div> -->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="current_mail_provider">Current Email Provider</label>
                        <div class="col-md-9">
                            <select name="current_mail_provider" id="current_mail_provider" class="form-control">
                                <option value="1" @if($record->current_mail_provider==1) selected @endif>SMTP</option>
                                <!-- <option value="2" @if($record->current_mail_provider==2) selected @endif>Mailgun</option> -->
                                <option value="3" @if($record->current_mail_provider==3) selected @endif>Default
                                </option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                            <label class="col-md-3 control-label" for="first_acc_check_fee">Criminal History Fee</label>
                            <div class="col-md-9">
                                <input type="text" id="first_acc_check_fee" name="first_acc_check_fee" class="form-control" placeholder="Basic Record Check Fee"  value="{{$record->first_acc_check_fee}}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="mvr_acc_check_fee">Moter Vehicle Report Fee</label>
                            <div class="col-md-9">
                                <input type="text" id="mvr_acc_check_fee" name="mvr_acc_check_fee" class="form-control" placeholder="Moter Vehicle Report Fee"  value="{{$record->mvr_acc_check_fee}}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="second_acc_check_fee">Service Fee</label>
                            <div class="col-md-9">
                                <input type="text" id="second_acc_check_fee" name="second_acc_check_fee" class="form-control" placeholder="Creminal Record Check Fee"  value="{{$record->second_acc_check_fee}}" required>
                            </div>
                        </div> -->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="from_email">From Email Address</label>
                        <div class="col-md-9">
                            <input type="email" id="from_email" name="from_email" class="form-control"
                                placeholder="From Email Address" value="{{$record->from_email}}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="mail_sender_name">From Name</label>
                        <div class="col-md-9">
                            <input type="text" id="mail_sender_name" name="mail_sender_name" class="form-control"
                                placeholder="From Email Name" value="{{$record->mail_sender_name}}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="product_name">Project Name</label>
                        <div class="col-md-9">
                            <input type="text" id="product_name" name="product_name" class="form-control"
                                placeholder="Project Name" value="{{$record->product_name}}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="mail_signature_name">Mail Signature Name</label>
                        <div class="col-md-9">
                            <input type="text" id="mail_signature_name" name="mail_signature_name" class="form-control"
                                placeholder="Mail Signature Name" value="{{$record->mail_signature_name}}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="tax">Tax (in %)</label>
                        <div class="col-md-9">
                            <input type="number" step="0.01" id="tax" name="tax" class="form-control"
                                placeholder="Tax (in %)" value="{{$record->tax}}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="shipping_type">Shipping</label>
                        <div class="col-md-9">
                            <select id="shipping_type" name="shipping_type" class="form-control">
                                <option value="0" @if($record->shipping_type==0) selected @endif>Free</option>
                                <option value="1" @if($record->shipping_type==1) selected @endif>Flat Rate</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="flat_rate_fee">Flat Rate Fee</label>
                        <div class="col-md-9">
                            <input type="nimber" step="0.01" id="flat_rate_fee" name="flat_rate_fee"
                                class="form-control" placeholder="Flat Rate Fee" value="{{$record->flat_rate_fee}}"
                                @if($record->shipping_type==1) required @endif>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="flat_rate_fee">Vendor Commission (%)</label>
                        <div class="col-md-9">
                            <input type="nimber" step="0.01" id="vendor_commission" name="vendor_commission"
                                class="form-control" placeholder="Vendor Commission" value="{{$record->vendor_commission}}"
                                @if($record->shipping_type==1) required @endif>
                        </div>
                    </div>

                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i>
                                Submit</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i>
                                Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
    <!-- END Form Example with Blocks in the Grid -->
</div>
<!-- END Page Content -->
@endsection