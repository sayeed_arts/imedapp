@if($subcategories)
    @foreach ($subcategories as $singleSubData)
        <tr>
            @if(isAllowed($current_user->id,$current_user->allowed_sections,'53'))
                <td class="text-center"><a href="{{url('admin/category-product/'.$singleSubData->id)}}" target="_blank">{{ $singleSubData->name }}</a></td>
            @else
                <td class="text-center">{{ $singleSubData->name }}</td>
            @endif

            <!-- <td class="text-center">{{ $singleSubData->name }}</td> -->
            <td class="text-center">{{(isset($parent))?$parent:''}}</td>            
            <td class="text-center">
                @if($singleSubData->display_status=='1')
                    <span class="label label-success">Active</span>
                @elseif($singleSubData->display_status=='0')
                    <span class="label label-info">InActive</span>
                @endif                                        
            </td>
            <td class="text-center">
                <div class="btn-group">
                    @if(isAllowed($current_user->id,$current_user->allowed_sections,'27'))
                    <a href="{{url('admin/categories/edit/'.$singleSubData->id)}}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                    @endif
                    @if(isAllowed($current_user->id,$current_user->allowed_sections,'28'))
                    <form action="{{ url('/admin/categories/delete/'.$singleSubData->id)}}" method="POST" class="delete-form">
                        {{ csrf_field() }}
                        <input type="submit" name="delete" value="X" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete">
                    </form>
                    @endif
                </div>
            </td>
        </tr>
        @if(count($singleSubData->subcategory))
            @include('admin/subCategories',['subcategories' => $singleSubData->subcategory,'parent'=>$parent.' |-- '.$singleSubData->name])
        @endif
    @endforeach                          
@endif