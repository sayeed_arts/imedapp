@if($subcategories)
    @foreach ($subcategories as $singleSubData)
    	@if(isset($currId) && $currId!=$singleSubData->id)
        	<option value="{{$singleSubData->id}}" @if(isset($selectedCat) && $selectedCat==$singleSubData->id) selected="selected" @endif>{{$parent}} |--{{$singleSubData->name}}</option>
        @endif
        @if(count($singleSubData->subcategory))
            @include('admin/subCategoriesOptions',['subcategories' => $singleSubData->subcategory,'parent'=>$parent.' |-- '.$singleSubData->name,'selectedCat'=>$selectedCat,'currId'=>$currId])
        @endif
    @endforeach                          
@endif