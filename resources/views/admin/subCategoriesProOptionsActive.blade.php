@if($subcategories)
    @foreach ($subcategories as $singleSubData)
    	<option value="{{$singleSubData->id}}" @if(isset($selectedCat) && in_array($singleSubData->id,$selectedCat)) selected="selected" @endif>{{$parent}} |--{{$singleSubData->name}}</option>
        @if(count($singleSubData->subcategoryactive))
            @include('admin/subCategoriesProOptionsActive',['subcategories' => $singleSubData->subcategoryactive,'parent'=>$parent.' |-- '.$singleSubData->name,'selectedCat'=>$selectedCat])
        @endif
    @endforeach                          
@endif