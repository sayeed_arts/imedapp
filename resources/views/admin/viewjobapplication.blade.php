@extends('layouts.adminapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    View Job Application
                    <span><a href="{{url('admin/jobapplications')}}" class="btn btn-default">Cancel</a></span>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/jobapplications')}}"><i class="fa fa-table"></i> Job Applications</a></li>
            <li>View Job</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('admin/jobs/update')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        <input type="hidden" name="currid" value="{{$record->id}}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-3 control-label">Job Title</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" readonly value="{{$record->name}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label">Full Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" readonly value="{{$record->fullname}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" >Email</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" readonly value="{{$record->email}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Phone</label>
                            <div class="col-md-9">
                                <input type="text"class="form-control" readonly value="{{$record->phone}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" >Current Location</label>
                            <div class="col-md-9">
                                <input type="text"class="form-control" readonly value="{{$record->current_location}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label">Preferred Location</label>
                            <div class="col-md-9">
                                <input type="text"class="form-control" readonly value="{{$record->preferred_location}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Cover Letter</label>
                            <div class="col-md-9">
                                <textarea rows="4" class="form-control ckeditor">{{$record->coverletter}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Resume</label>
                            <div class="col-md-9">
                                <a href="{{$record->resume}}" target="_blank">Download Resume <i class="fa fa-download"></i></a>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection