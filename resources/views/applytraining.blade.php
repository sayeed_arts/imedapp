@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>{!!$jobDetails->name!!}</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('/trainings')}}">Service Trainings</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- About Start -->
    <div class="job-list mt-5 mb-5">
        <div class="container">        
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 offset-md-3">
                            <div class="card mb-3">
                               <div>
                                    <div class="card-body">
                                        <div class="job-form-inner">
                                            <h2> Apply for this service training	</h2>                               
                                            <p>Use the form below to submit your job application</p>
                                            @if (Session::has('message'))
                                            {!! successMesaage(Session::get('message')) !!}
                                            @endif
                                            <form method="post" enctype="multipart/form-data" method="POST" class="jqueryValidate" action="{{ route('post-apply-training') }}">
                                                <input type="hidden" name="training_id" value="{{$jobDetails->id}}">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="fullname">Full Name </label>
                                                    <input type="text" name="fullname" id="fullname" class="form-control " required value="{{ old('fullname') }}" />
                                                    @if ($errors->has('fullname'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('fullname') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">Email </label>
                                                    <input type="email" name="email" id="email" class="form-control " required value="{{ old('email') }}" />
                                                    @if ($errors->has('email'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="phone">Phone </label>
                                                    <input type="text" name="phone" id="phone" class="form-control " required value="{{ old('phone') }}" />
                                                    @if ($errors->has('phone'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('phone') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                
                                                <div class="form-group">                                        
                                                    <input type="submit" name="form_sub" id="application-submit-btn" class="btn btn-primary" value="Submit" data-response-text="Submitting..">
                                                </div>
                                            </form>
                                        </div>
                                        <a href="{{url('trainings')}} " class="btn">Back to listings</a>
                                    </div>
                                </div>
                            </div>

                            

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection