@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a> <i
                                class=" fa fa-angle-right"></i></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$page_title}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb End -->
<!-- section start -->
<section class=" pt-30">
    <div class="collection-wrapper auction-details ">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 prd-gallery">
                    @if(!empty($productdetails->additional_images))
                    <div class="product-big-gallery product-slick">
                        @php
                        $allImagesArray = explode('##~~##',$productdetails->additional_images);
                        @endphp
                        @foreach($allImagesArray as $singleImage)
                        <div class="gallery-item">
                            <img src="{{ asset($singleImage)}}" alt=""
                                class="img-fluid blur-up lazyload image_zoom_cls-0">
                        </div>
                        @endforeach
                    </div>
                    <div class="row product-thumb-gallery">
                        <div class="col-12 p-0">
                            <div class="slider-nav">
                                @foreach($allImagesArray as $singleImage)
                                <div>
                                    <img src="{{ asset($singleImage)}}" alt="" class="img-fluid blur-up lazyload">
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="col-lg-7 rtl-text">
                    <div class="product-right">
                        <h2 class="">{{$productdetails->name}}</h2>
                        <div class="head-star">
                            <div class="rating">
                                Seller Rating : <img src="{{ asset('frontend/assets/images/') }}/{{round($averagerating,1)}}star.png " alt="">
                            </div>
                            <span>({{$totalcount}})</span>
                            <span>&nbsp; | &nbsp; Auction Id: #{{$auctiondetails->id}}</span>
                        </div>
                        <div class="product">
                            <p>{!! $auctiondetails->description !!}</p>
                        </div>
                        <div class="border-product">
                            <p>Starting Bid <strong>${!! $auctiondetails->start_bid_price !!}</strong></p>
                        </div>

                        <div class="timer">
                            <span>Time left</span>
                            <p class="countdowntimer" id="auc_<?php echo $auctiondetails->id; ?>"></p>
                        </div>
                        @guest
                        
                        @else
                            <?php
                            $current_user = Auth::user();
                            if($current_user->is_admin == '0'){
                            ?>
                            <button onclick="bidNow(<?php echo $auctiondetails->id; ?>);" class="btn bid-auction-btn" data-toggle="modal" data-target="#biddingModal"> Bid on Auction</button>
                            <?php if($showsavedforbidlater){?>
                                <a href="javascript:void(0);" class="bid" onclick="saveForBidLater(<?php echo $auctiondetails->id; ?>);" id="linksaveforbidlater"><img src="{{ asset('frontend/assets/images/heart.png') }} " alt=""> Save for Bid Later</a>
                                <div role="alert" id="linksaveforbidlatermessage" style="display:none; margin-top:20px;"></div>
                            <?php } ?>
                            <?php
                            }
                            ?>
                                <!-- <div class="read-terms">
                             Read <a href="{{url('page/terms--conditions')}}" class="bid" target="_blank"> Terms and Conditions</a>
                            </div> -->
                        @endguest

                        @if(!empty($storedetail->company))
                            <div class="vendor-info mt-3">
                                <h4>Sold By</h4>
                                <a href="{{url('storepage/'.$storedetail->store_slug)}}">
                                    <img src="{{ $storedetail->logo }} " width="80px" alt="">
                                    <p>{{$storedetail->company}}</p>
                                </a>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade bidding-modal" id="biddingModal" tabindex="-1" role="dialog" aria-labelledby="biddingModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Place Your Bid <span>Note : Your bid should be greater than last bid</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div role="alert" id="bidresponsemessage" style="display:none;"></div>
                <div id="bidformcontent" style="display:none;">
                    <form name="bidform">
                        <input type="hidden" name="bid_auction_id" id="bid_auction_id" value="" />
                        <ul>
                            <li><input type="text" name="bid_amount" id="bid_amount" class="form-control" value="" /></li>
                            <li class="text-right"><button id="btn_submit_bid" type="button" class="btn btn-primary" onclick="submitBid();">Place Your bid</button></li>
                        </ul>
                    </form>
                    <div class="last-bid">
                        LAST BID VALUE <span id="last_bid_amount"></span>
                    </div>
                </div>
                <div id="bidformloading">
                    <p style="text-align:center;">Loading...</p>
                </div>
            </div>
        </div>
    </div>
</div>


<section class="tab-product mt-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">

                <ul class="nav nav-tabs nav-material" id="top-tab" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="auction-description-tab" data-toggle="tab" href="#auction-description"
                            role="tab" aria-selected="true">{{__('Description')}}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link " id="auction-conditions-tab" data-toggle="tab" href="#auction-conditions"
                            role="tab" aria-selected="true">{{__('Conditions')}}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link " id="auction-terms-tab" data-toggle="tab" href="#auction-terms"
                            role="tab" aria-selected="true">{{__('Terms & Conditions')}}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link " id="auction-faq-tab" data-toggle="tab" href="#auction-faq"
                            role="tab" aria-selected="true">{{__('FAQ')}}</a>
                        </li>
                </ul>

                <div class="tab-content nav-material" id="top-tabContent">
                    <div class="tab-pane fade show active p-4" id="auction-description" role="tabpanel"
                        aria-labelledby="top-home-tab">
                        
                        <h2>Description</h2>                       
                        <p><?php echo $auctiondetails->description; ?></p>
                        
                    </div>


                    <div class="tab-pane fade p-4" id="auction-conditions" role="tabpanel"
                        aria-labelledby="top-home-tab">
                        
                        <h2>Conditions</h2>                       
                        <p><?php echo $auctiondetails->conditions; ?></p>
                        
                    </div>


                    <div class="tab-pane fade p-4" id="auction-terms" role="tabpanel"
                        aria-labelledby="top-home-tab">
                        
                        <h2>Terms & Conditions</h2>                       
                        <p><?php echo $auctiondetails->terms_and_conditions; ?></p>
                        
                    </div>


                    <div class="tab-pane fade p-4" id="auction-faq" role="tabpanel"
                        aria-labelledby="top-home-tab">
                                
                        <?php foreach($faq as $thefaq){?>
                            <h2><?php echo $thefaq->faq_ques?></h2> 
                            <p><?php echo $thefaq->faq_ans?></p>     
                        <?php } ?>    
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- product-tab ends -->
@endsection