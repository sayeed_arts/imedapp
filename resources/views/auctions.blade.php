@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="page-title">
                {{((isset($page_title))?$page_title:'')}}
                </h2>
            </div>
            <div class="col-md-6 text-right">
                <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a> <i class=" fa fa-angle-right"></i> </li>
                        <li class="breadcrumb-item active" aria-current="page">{{((isset($page_title))?$page_title:'')}}
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb end -->
<!-- section start -->
<section class="section-n-space pt-30 ratio_asos">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="collection-content col">
                    <div class="page-main-content">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="collection-filter-block">
                                    <div class="collection-mobile-back">
                                        <span class="filter-back">
                                            <i class="fa fa-angle-left" aria-hidden="true"></i> Back
                                        </span>
                                    </div>
                                
                                    <div class="collection-collapse-block open">
                                        <h3 class="collapse-block-title">Filter by Categories</h3>

                                        <div class="custom-check clear">
                                            <input type="checkbox" class="manufacturer-filter-change" id="any" value="-1" name="selectedmanufacturer[]">
                                            <label class="" for="any">Clear All</label>
                                        </div>

                                        <div class="collection-collapse-block-content">
                                            <div class="collection-brand-filter">
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Energizer169" value="169" name="selectedmanufacturer[]">
                                                    <label class="" for="Energizer169">Energizer</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Diversey170" value="170" name="selectedmanufacturer[]">
                                                    <label class="" for="Diversey170">Diversey</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Level 1171" value="171" name="selectedmanufacturer[]">
                                                    <label class="" for="Level 1171">Level 1</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Criticare172" value="172" name="selectedmanufacturer[]">
                                                    <label class="" for="Criticare172">Criticare</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Ceracarta173" value="173" name="selectedmanufacturer[]">
                                                    <label class="" for="Ceracarta173">Ceracarta</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Eckel174" value="174" name="selectedmanufacturer[]">
                                                    <label class="" for="Eckel174">Eckel</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Mintron175" value="175" name="selectedmanufacturer[]">
                                                    <label class="" for="Mintron175">Mintron</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Suntech Medical176" value="176" name="selectedmanufacturer[]">
                                                    <label class="" for="Suntech Medical176">Suntech Medical</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Erbe177" value="177" name="selectedmanufacturer[]">
                                                    <label class="" for="Erbe177">Erbe</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="IEM178" value="178" name="selectedmanufacturer[]">
                                                    <label class="" for="IEM178">IEM</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Future Health Concepts179" value="179" name="selectedmanufacturer[]">
                                                    <label class="" for="Future Health Concepts179">Future Health Concepts</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Medex180" value="180" name="selectedmanufacturer[]">
                                                    <label class="" for="Medex180">Medex</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Netac181" value="181" name="selectedmanufacturer[]">
                                                    <label class="" for="Netac181">Netac</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="GCX182" value="182" name="selectedmanufacturer[]">
                                                    <label class="" for="GCX182">GCX</label>
                                                </div>
                                                                                                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="TP-Link183" value="183" name="selectedmanufacturer[]">
                                                    <label class="" for="TP-Link183">TP-Link</label>
                                                </div>
                                                 <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Multigon184" value="184" name="selectedmanufacturer[]">
                                                    <label class="" for="Multigon184">Multigon</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="MedGraphics185" value="185" name="selectedmanufacturer[]">
                                                    <label class="" for="MedGraphics185">MedGraphics</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="manufacturer-filter-change" id="Lutech Industries, Inc.186" value="186" name="selectedmanufacturer[]">
                                                    <label class="" for="Lutech Industries, Inc.186">Lutech Industries, Inc.</label>
                                                </div>
                                                                                        
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="collection-filter-block">
                                    <div class="collection-mobile-back">
                                        <span class="filter-back">
                                            <i class="fa fa-angle-left" aria-hidden="true"></i> Back
                                        </span>
                                    </div>
                                
                                    <div class="collection-collapse-block open">
                                        <h3 class="collapse-block-title">Auction by Status</h3>

                                      

                                        <div class="collection-collapse-block-content">
                                            <div class="collection-brand-filter">
                                                <div class="custom-check">
                                                    <input type="checkbox" class="auction-status-filter-change">
                                                    <label class="" for="Energizer169">Active Auction</label>
                                                </div>
                                                <div class="custom-check">
                                                    <input type="checkbox" class="auction-status-filter-change">
                                                    <label class="" for="Diversey170">InActive Auction</label>
                                                </div>                                                                                        
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-9">
                                <div class="collection-product-wrapper">
                                    <div class="auctions-list">
                                        @if(!empty($allRecords) && count($allRecords)>0)
                                            @foreach($allRecords as $data) 
                                                <div class="auction-box">
                                                    <div class="thumb">
                                                        <img src="{{asset($data->image)}}" alt="{!! $data->name !!}" srcset="" />
                                                    </div>
                                                    <a href="{{url('product/'.$data->slug)}}" class="title">
                                                        <h3>{!! $data->name !!}</h3>
                                                    </a>
                                                    <div class="timer">
                                                        <span>Time left</span>
                                                        <p class="countdowntimer" id="auc_<?php echo $data->id; ?>"></p>
                                                    </div>
                                                    <div class="buttons">
                                                        <a href="{{url('auction/'.base64_encode($data->id))}}" class="more">Read More</a>
                                                        @guest
                                                        <a href="{{url('login')}}" class="bid">Login to Bid</a>
                                                        @else
                                                        <?php
                                                        $current_user = Auth::user();
                                                        if($current_user->is_admin == '0'){
                                                        ?>
                                                        <a href="javascript:void(0);" onclick="bidNow(<?php echo $data->id; ?>);" class="bid" data-toggle="modal" data-target="#biddingModal">Bid Now</a>
                                                        <?php } ?>
                                                        @endguest
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <p>No auctions found </p>    
                                        @endif
                                    </div>

                                    
                                    <!-- Modal -->
                                    <div class="modal fade bidding-modal" id="biddingModal" tabindex="-1" role="dialog" aria-labelledby="biddingModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Place Your Bid <span>Note : Your bid should be greater than last bid</span></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div role="alert" id="bidresponsemessage" style="display:none;"></div>
                                                    <div id="bidformcontent" style="display:none;">
                                                        <form name="bidform">
                                                            <input type="hidden" name="bid_auction_id" id="bid_auction_id" value="" />
                                                            <ul>
                                                                <li><input type="text" name="bid_amount" id="bid_amount" class="form-control" value="" /></li>
                                                                <li class="text-right"><button id="btn_submit_bid" type="button" class="btn btn-primary" onclick="submitBid();">Place Your bid</button></li>
                                                            </ul>
                                                        </form>
                                                        <div class="last-bid">
                                                            LAST BID VALUE <span id="last_bid_amount"></span>
                                                        </div>
                                                    </div>
                                                    <div id="bidformloading">
                                                        <p style="text-align:center;">Loading...</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection