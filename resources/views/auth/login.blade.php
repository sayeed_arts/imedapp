@extends('layouts.app')

@section('content')
<section class="cart-section section-b-space auth-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="login-form">
                    <div class="login-form-inner">
                        <h2>Welcome Back</h2>
                        <h3>Login with your email & password</h3>
                        <form method="POST" action="{{ route('login') }}" class="jqueryValidate">
                            @csrf                            
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            @if (session('warning'))
                                <div class="alert alert-warning">
                                    {{ session('warning') }}
                                </div>
                            @endif     
                            <div class="form-group text-left">                
                                <input id="email" placeholder="Email Address" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group text-left">                
                                
                                <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"> 
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>  
                            <div class="form-group text-left"> 
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-green btn-lg btn-block login"> {{ __('Sign In') }} </button>
                            </div>                                      
                        </form>
                        <div class="row">
                            <div class="col-sm-12 text-center pb-3 register">
                                @if (Route::has('register'))
                                    New to Imedical? <a href="{{ route('register') }}">{{ __('Register Now') }}</a>
                                @endif
                            </div>
                        </div>
                        <div class="forgot text-center">
                            @if (Route::has('password.request'))
                                {{ __(' Forgot your password?') }}                            
                                <a href="{{ route('password.request') }}">
                                    {{ __('Reset It') }}
                                </a>
                            @endif  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
