@extends('layouts.app')

@section('content')
<section class="cart-section section-b-space auth-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 ">
                <div class="login-form">
                    <h2>Password Reset</h2>
                    <h3>We will send you an email to reset your password.</h3>
                    <div class="login-form-inner">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ route('password.email') }}" class="jqueryValidate">
                            @csrf     
                            <div class="form-group text-left">                
                               
                                <input id="email" placeholder="Email Address" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                
                            </div>            
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-green btn-lg btn-block login"> {{ __('Send Password Reset Link') }}</button>
                            </div>    
                        </form>

                        <div class="forgot text-center">
                            {{ __('Remembered your password?') }} <a href="{{ route('login') }}">{{ __('Back to Login') }}</a>
                        </div>
                     
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
