@extends('layouts.app')

@section('content')
<section class="cart-section section-b-space auth-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 ">
                <div class="login-form">
                    <div class="login-form-inner">
                        <form method="POST" action="{{ route('password.update') }}" class="jqueryValidate">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group text-left">                
                                <label>Email</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group text-left">                
                                <label>{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group text-left">                
                                <label>{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">                
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-green btn-lg btn-block login"> {{ __('Reset Password') }}</button>
                            </div>                    
                        </form>
                        <hr>
                        <p class="text-center"> <a href="{{ route('login') }}">{{ __('Back to login') }}</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection