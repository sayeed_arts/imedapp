@extends('layouts.app')

@section('content')
<section class="cart-section section-b-space auth-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="register-form">
                    <h2>Sign Up</h2>
                    <h3>By signing up, you agree to Imedical</h3>
                <form method="POST" id="msform" action="{{ route('register') }}" class="jqueryValidate">
                    @csrf
                    <fieldset>                    
                        <div class="row">
                            <div class="col-sm-6">
                            <div class="form-group text-left">
                               
                                <input placeholder="First Name" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>
                            <div class="col-sm-6 text-left">
                            <div class="form-group">
                                
                                <input placeholder="Last Name" id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}"  autocomplete="last_name" autofocus required>

                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>
                            <div class="col-sm-6 text-left">
                            <div class="form-group">
                                                       
                                <input placeholder="Email Adress" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" required>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <small>{{ __('This will serve as your login') }}</small>
                            </div>
                            </div>
                            <div class="col-sm-6 text-left">
                            <div class="form-group">
                                <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password" required>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                <small>{{ __('Please create a strong password') }}</small>
                            </div>
                            </div>
                            <div class="col-sm-12 text-left">
                            <div class="form-group">
                               
                                <input placeholder="Phone Number" type="tel" id="phone_number" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number"  placeholder="" required value="{{ old('phone_number') }}"/>
                                <!-- pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" -->
                                @error('phone_number')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <small>{{ __('Please provide your mobile number. This serves as your backup verification method in case you forget your password . Don\'t worry this remains private just like all of your information.') }}</small>
                            </div>
                            </div>
                        </div>                    
                        <button type="submit" name="next" class="btn btn-success signin btn-block">{{ __('Register') }}</button>
                      
                    </fieldset>
                </form>  
                <div class="forgot text-center mt-3">
                    <a class="ClickHere" href="{{ route('login') }}">{{ __('Back to Login') }}</a>
                </div>
                </div>          
            </div>
        </div>
    </div>     
</section>
@endsection