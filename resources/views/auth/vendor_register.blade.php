@extends('layouts.app')

@section('content')
<section class="cart-section section-b-space auth-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="register-form">
                    <h2>Welcome to Our Marketplace</h2>
                    <h3>By signing up, you agree to Imedical</h3>
               @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    
                <form method="POST" id="msform" action="{{ url('save_vendor') }}" class="jqueryValidate">
                    @csrf
                    <fieldset>                    
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Company Information</h4>
                            </div>    
                            <div class="col-sm-6">
                                <div class="form-group text-left">                               
                                    <input placeholder="First Name" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                    <div class="form-group text-left">

                                <input type="text" id="last_name" name="last_name" class="form-control @error('last_name') is-invalid @enderror" required placeholder="Last Name" value="" tabindex="1">
                                </div>
                            </div>




                            <div class="col-sm-6">
                                    <div class="form-group text-left">

                                <input type="text" id="title" name="title" class="form-control" required placeholder="Title" value="<?=isset($vendordetail->title)?$vendordetail->title:''?>" tabindex="2"></div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group text-left">                               
                                    <input placeholder="Company Name" id="comp_name" type="text" class="form-control @error('name') is-invalid @enderror" name="comp_name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group text-left">                               
                                    <input placeholder="Email ID" id="email" type="text" class="form-control @error('name') is-invalid @enderror" name="email" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group text-left">                               
                                    <input placeholder="Phone" id="phone_number" type="text" class="form-control @error('name') is-invalid @enderror" name="phone_number" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group text-left">                               
                                    <input placeholder="Extension" id="ext" type="text" class="form-control @error('name') is-invalid @enderror" name="ext" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group text-left">
                            <input type="text" id="store" name="store" class="form-control" required placeholder="Store Name" value="<?=isset($vendordetail->Store)?$vendordetail->Store:''?>" tabindex="4">
                            </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group text-left">                               
                                    <input placeholder="Company Website URL" id="compweb" type="text" class="form-control @error('name') is-invalid @enderror" name="compweb" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>  


                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Where You Located</h4>
                            </div>    
                            
                              <div class="col-sm-6">
                                <div class="form-group text-left">
                                         <select name="country" id="country_yo" class="form-control" tabindex="10">
                                    <option value="">Select Country</option>
                                    @foreach($countrylist as $key=>$val)
                                    
                                    <option     @if(isset($vendordetail->country) && ($key == $vendordetail->country) )
                                            selected
                                            @endif value="<?=$key?>"><?=$val?></option>
                                    @endforeach
                                </select>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                              <div class="col-sm-6">
                                <div class="form-group text-left">
                                      <select name="state" id="state_yo" class="form-control fetch_cities" tabindex="11">
                                    <option value="">Select State</option>
                             @if(!empty($allState))       
                                    @foreach($allState as $key=>$val)
                                    
                                    <option     @if(isset($vendordetail->state) && ($val->stateID == $vendordetail->state) )
                                            selected
                                            @endif value="<?=$val->stateID?>"><?=$val->stateName?></option>
                                    @endforeach
                                    @endif
                                    </select>
                              @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                            
                            <div class="col-sm-6">
                                <div class="form-group text-left">                               
                                    <input placeholder="City" id="city" type="text" class="form-control @error('name') is-invalid @enderror" name="city" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group text-left">                               
                                    <input placeholder="Zip Code" id="zipcode" type="text" class="form-control @error('name') is-invalid @enderror" name="zipcode" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group text-left">  
                                    <textarea class="form-control" placeholder="Address" name="address1" id="address1" cols="10" rows="10"></textarea>                             
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                        </div>  


                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Credentials</h4>
                            </div>    
                            
                            <div class="col-sm-6">
                                <div class="form-group text-left">                               
                                    <input placeholder="Password" id="password" type="password" class="form-control @error('name') is-invalid @enderror" name="password" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group text-left">                               
                                    <input placeholder="Confirm Password" id="confirm_password" type="password" class="form-control @error('name') is-invalid @enderror" name="confirm_password" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                        </div>  


                        <button type="submit" name="next" class="btn btn-success signin btn-block">{{ __('Register') }}</button>
                      
                    </fieldset>
                </form>  
                <div class="forgot text-center mt-3">
                    <a class="ClickHere" href="{{ route('login') }}">{{ __('Back to Login') }}</a>
                </div>
                </div>          
            </div>
        </div>
    </div>     
</section>
@endsection