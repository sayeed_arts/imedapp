@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>{{$page_title}}</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{$page_title}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb End -->
<!--section start-->


<!-- thank-you section start -->
<section class="section-b-space light-layout">
    <div class="container">
        <div class="row">
            <div class="col-md-12">


                @if (Session::has('message'))
                <div class="success-text">
                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                    {!! successMesaage(Session::get('message')) !!}
                </div>
                @endif
                <div class="success-text order-fail">
                    @if (Session::has('error'))
                    <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                    {!! errorMesaage(Session::get('error')) !!}
                    <a href="{{ url('/')}}" class="btn"> Continue Shopping</a>
                </div>
                @endif
                <div class="success-text order-fail">
                    {!! validationError($errors) !!}
                    <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                    <p>{{__('You canceled your order.')}}</p>
                    <a href="{{ url('/')}}" class="btn"> Continue Shopping</a>
                </div>
        </div>
    </div>
</section>
<!-- Section ends -->



<!-- 
<section class="cart-section section-b-space">
    <div class="container">
        @if (Session::has('message'))
        {!! successMesaage(Session::get('message')) !!}
        @endif
        @if (Session::has('error'))
        {!! errorMesaage(Session::get('error')) !!}
        @endif
        {!! validationError($errors) !!}
        <p>{{__('You canceled your order.')}}</p>
    </div>
</section> -->
@endsection