@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>{{$page_title}}</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a> <i
                                class=" fa fa-angle-right"></i></li>
                        <li class="breadcrumb-item active">{{$page_title}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb End -->
<!--section start-->
<section class="cart-section section-b-space">
    <div class="container">
        @if (Session::has('message'))
        {!! successMesaage(Session::get('message')) !!}
        @endif
        @if (Session::has('error'))
        {!! errorMesaage(Session::get('error')) !!}
        @endif
        {!! validationError($errors) !!}
        @php
        $subTotalAmount = 0;
        @endphp
        @if(!empty($allCartItems) && count($allCartItems)>0)
        <div class="row">
            <div class="col-sm-8">
                <div class="cart-row">
                        @foreach($allCartItems as $singleCartItem)
                        @php
                        $cartProLangData =
                        getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleCartItem->product_id));
                        if(!empty($cartProLangData->name)){
                        $productName = $cartProLangData->name;
                        }else{
                        $productName = $singleCartItem->name;
                        }

                        $qty = $singleCartItem->qty;
                        $price = $singleCartItem->price;
                        $special_price = $singleCartItem->special_price;
                        $stock_availability = $singleCartItem->stock_availability;
                        $inventory_management = $singleCartItem->inventory_management;
                        $availableQty = $singleCartItem->availableQty;
                        if(!empty($special_price) && $special_price>0){
                        $proPrice = $special_price;
                        }else{
                        $proPrice = $price;
                        }
                        $attrProNAme = '';
                        if(!empty($singleCartItem->product_attr_id)){
                        $getProAttrCart = DB::table('products_attributes')
                        ->where('id',$singleCartItem->product_attr_id)
                        ->where('product_id',$singleCartItem->product_id)
                        ->where('is_variation',1)
                        ->first();
                        $proAttribute_value
                        =(isset($getProAttrCart->attribute_value))?$getProAttrCart->attribute_value:'';
                        $getAttrCartData = array();

                        if(!empty($proAttribute_value)){
                        $getAttrCartData = DB::table('attribute_values')
                        ->where('id',$proAttribute_value)
                        ->first();
                        }
                        $attrProNAme = (isset($getAttrCartData->name))?$getAttrCartData->name:'';
                        $proPrice = (isset($getProAttrCart->attribute_price) &&
                        $getProAttrCart->attribute_price>0)?$getProAttrCart->attribute_price:$proPrice;
                        }
                        if(($inventory_management==0 && $stock_availability==1) || ($inventory_management==1 &&
                        $stock_availability==1 && $availableQty>=$qty)){
                        $cartQty = $qty;
                        }else if($inventory_management==1 && $stock_availability==1 && $availableQty<$qty){
                            $cartQty=$availableQty; }else{ $cartQty='0' ; } $totalPrice=$cartQty*$proPrice;
                            $subTotalAmount=$subTotalAmount+$totalPrice; @endphp 

                            <div class="cart-item">
                                <div class="thumb">
                                    <a href="{{url('product/'.$singleCartItem->slug)}}">
                                    <img alt="{{$productName}}"class="mr-3" src="{{ asset($singleCartItem->image) }}"></a>
                                </div>

                                <div class="details">
                                    <div class="name">
                                        <a href="{{url('product/'.$singleCartItem->slug)}}">
                                            <h2>{{$productName}}</h2>
                                        </a>
                                        @if(!empty($attrProNAme))
                                        <h2>{{__($attrProNAme)}}</h2>
                                        @endif
                                    </div>

                                    <div class="bottom-detail-row">
                                        <div class="price">
                                            <h5>{{showPrice($proPrice)}}</h5>
                                        </div>

                                        <div class="qty">
                                            <div class="qty-box">
                                                <div class="input-group">
                                                    <input type="number" name="quantity"
                                                        class="form-control input-number cart-change-qty"
                                                        value="{{$cartQty}}"
                                                        data-id="{{base64_encode($singleCartItem->product_id)}}"
                                                        data-loc="cart">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="total-price">
                                            <h5 class="td-color">{{showPrice($totalPrice)}} </h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="remove">
                                    <a href="javascript:void(0)" class="icon delete-from-cart" data-id="{{base64_encode($singleCartItem->product_id)}}" data-loc="cart"><i class="ti-close"></i></a>
                                </div>
                            </div>
                        @endforeach
                </div>
            </div>
     
            <div class="col-md-4">

            <div class="row">
                <div class="col-md-12">

                     <div class="cart-total-side">
                
                        <table class="table cart-table table-responsive-md">
                            <tfoot>
                                <tr>
                                    <td>{{__('Total')}} :</td>
                                    <td>
                                        <h2 id="totalValue">{{showPrice($subTotalAmount)}}</h2>
                                    </td>
                                </tr>
                                <tr id="discountLabel">
                                    <td>{{__('Discount')}} :</td>
                                    <td>
                                        <h2 id="discountValue">{{showPrice($availedDiscount)}}</h2>
                                    </td>
                                </tr>
                                <tr id="grandTotal">
                                    <td>{{__('Grand Total')}} :</td>
                                    <td>
                                        <h2 id="grandTotalValue">
                                            @if($priceAfterDiscount==0)
                                            {{showPrice($subTotalAmount)}}
                                            @else
                                            {{showPrice($priceAfterDiscount)}}
                                            @endif
                                        </h2>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                        <div class="coupon-code">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <h3>{{__('Apply Discount Code')}}</h3>
                                        <input type="text" name="applycoupon" id="applycoupon" class="form-control"
                                            value={{$appliedCoupon}}>
                                        <span id="errorMessage"></span>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <input type="button" name="applybutton" value="{{__('Apply Discount')}}"
                                        class="btn btn-outline btn-sm apply-coupon-button" id="applyCouponButton">
                                </div>
                            </div>
                        </div>

                        <div class="row cart-buttons">
                            <div class="col-12">
                                <a href="{{url('shop')}}" class="btn btn-solid btn-sm">{{__('Continue Shopping')}}</a>
                                <a href="{{url('checkout')}}" class="btn btn-solid btn-sm">{{__('Checkout')}}</a>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
                
            </div>
            
        </div>


        @else
       
        
        <div class="row">
            <div class="col-md-12 text-center">
            <img src="{{ asset('frontend/assets/images/empty-cart.png') }}" class="img-fluid mb-0" alt="">
            
            <h3 class="mb-4">{{__('Your Shopping Bag is empty.')}}</h3>
            </div>
            <div class="col-12 text-center"><a href="{{url('/')}}" class="btn btn-outline mr-2">{{__('Go to Homepage')}}</a> <a href="{{url('shop')}}" class="btn btn-solid">{{__('Continue Shopping')}}</a></div>
        </div>
        @endif
    </div>
</section>
<!--section end-->
@endsection