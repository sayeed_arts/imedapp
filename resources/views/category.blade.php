@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="page-title">
                {{((isset($page_title))?$page_title:'')}}
                </h2>
            </div>
            <div class="col-md-6 text-right">
                <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a> <i class=" fa fa-angle-right"></i> </li>
                        @if(isset($page_title) && $page_title!='Shop')
                        <li class="breadcrumb-item"><a href="{{url('/shop')}}">{{__('Shop')}}</a> <i class=" fa fa-angle-right"></i> </li>
                        @endif
                        <li class="breadcrumb-item active" aria-current="page">{{((isset($page_title))?$page_title:'')}}
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb end -->
<!-- section start -->
<section class="section-n-space pt-30 ratio_asos">
    <div class="collection-wrapper">
        <div class="container">
            <input type="hidden" name="minimumAmount" id="minimumAmount" value="{{$minimumAmount}}">
            <input type="hidden" name="maximumAmount" id="maximumAmount" value="{{$maximumAmount}}">
            <form name="product_search_form" id="product_search_form" action="{{url($formSlug)}}" method="get">
                <div class="row">
                    <div class="col-sm-3 collection-filter">
                        <div class="collection-filter-block">
                            <div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left"
                                        aria-hidden="true"></i> {{__('Back')}}</span></div>
                            <div class="collection-collapse-block border-0 open">
                                <h3 class="collapse-block-title">{{__('Filter by Price')}}</h3>
                                <div class="collection-collapse-block-content">
                                    <div class="wrapper mt-3">
                                        
                                        <div id="filter-slider-range"></div>                                        
                                        <input type="text" class="price-filter-change" id="filter_price" readonly>
                                        
                                        <input type="hidden" name="fromPrice" id="fromPriceAmount"
                                            value="{{$fromPrice}}" class="price-filter-change">
                                        <input type="hidden" name="toPrice" id="toPriceAmount" value="{{$toPrice}}"
                                            class="price-filter-change">
                                    </div>



                                </div>
                            </div>
                          
                        </div>
                        

                        <div class="collection-filter-block" <?php if($isbrandpage) { echo 'style="display:none;"'; } ?> >
                            <div class="collection-mobile-back">
                                <span class="filter-back">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i> {{__('Back')}}
                                </span>
                            </div>
                            
                            @if(!empty($allManufacturer) && count($allManufacturer)>0)
                            <div class="collection-collapse-block open">
                                <h3 class="collapse-block-title">{{__('Filter by Brands')}}</h3>

                                <div class="custom-check clear">
                                    <input type="checkbox"
                                        class="manufacturer-filter-change" id="any"
                                        value="-1" @if(!empty($selManufArray) && in_array('-1',$selManufArray))
                                        checked @endif name="selectedmanufacturer[]">
                                    <label class="" for="any">{{__('Clear All')}}</label>
                                </div>

                                <div class="collection-collapse-block-content">
                                    <div class="collection-brand-filter">
                                        @php
                                        $selManufArray = array();
                                        if(!empty($selectedmanufacturer)){
                                        $selManufArray = explode(',',$selectedmanufacturer);
                                        }
                                        @endphp
                                        @foreach($allManufacturer as $singleMenu)
                                        @php
                                        $menufactLangData =
                                        getLanguageReconds('manufacturer_translations',array('name'),array('manufacturer_id'=>$singleMenu->id));

                                        if(!empty($menufactLangData->name)){
                                        $manufacturerName = $menufactLangData->name;
                                        }else{
                                        $manufacturerName = $singleMenu->name;
                                        }
                                        @endphp
                                        <div class="custom-check">
                                            <input type="checkbox"
                                                class="manufacturer-filter-change"
                                                id="{{$singleMenu->name.$singleMenu->id}}" value="{{$singleMenu->id}}"
                                                @if(!empty($selManufArray) && in_array($singleMenu->id,$selManufArray))
                                            checked @endif name="selectedmanufacturer[]">
                                            <label class=""
                                                for="{{$singleMenu->name.$singleMenu->id}}">{{$manufacturerName}}</label>
                                        </div>
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>

                        @if(isset($advertisementData->image) && $advertisementData->image=='')
                        <div class="collection-sidebar-banner">
                            @if($advertisementData->content!='')
                            <a href="{{$advertisementData->content}}" target="_blank">
                                <img src="{{ asset($advertisementData->image) }}" alt="{{$advertisementData->name}}"
                                    class="img-fluid blur-up lazyload">
                            </a>
                            @else
                            <img src="{{ asset($advertisementData->image) }}" alt="{{$advertisementData->name}}"
                                class="img-fluid blur-up lazyload">
                            @endif
                        </div>
                        @endif
                    </div>
                    <div class="collection-content col">
                        <div class="page-main-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- <div class="top-banner-wrapper">
                                        <div class="top-banner-content">
                                            <h4>{{((isset($page_title))?$page_title:'')}}</h4>
                                            {!!((isset($pageData->content))?$pageData->content:'')!!}
                                        </div>
                                    </div> -->
                                    <div class="collection-product-wrapper">
                                        @if(!empty($allProducts) && count($allProducts)>0)
                                        <div class="product-top-filter">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="product-filter-content">
                                                        <div class="search-count">
                                                            <?php 
                                                                $start  = (($page*$perpage)-$perpage)+1;
                                                                $end    = ($start+$allProducts->count())-1;
                                                                ?>
                                                            <h5>{{__('Showing Products')}} {{ $start}}-{{$end}}
                                                                {{__('of')}} {{$productCount}} {{__('Result')}}</h5>
                                                        </div>
                                                        <div class="custom-select-drop">
                                                            <div class="select">
                                                                <select name="sortby" id="sortby"
                                                                    class="sorting-filter-change">
                                                                    <option value="">{{__('Sort')}}</option>
                                                                    <option value="price-asc" @if($sortby=='price-asc' )
                                                                        selected @endif>{{__('Price Low to High')}}</option>
                                                                    <option value="price-desc" @if($sortby=='price-desc' )
                                                                        selected @endif>{{__('Price High to Low')}}</option>
                                                                    <option value="title-asc" @if($sortby=='title-asc' )
                                                                        selected @endif>{{__('Name A-Z')}}</option>
                                                                    <option value="title-desc" @if($sortby=='title-desc' )
                                                                        selected @endif>{{__('Name Z-A')}}</option>
                                                                    <option value="date-asc" @if($sortby=='date-asc' )
                                                                        selected @endif>{{__('Oldest')}}</option>
                                                                    <option value="date-desc" @if($sortby=='date-desc' )
                                                                        selected @endif>{{__('Most Recent')}}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="product-wrapper-grid">
                                            <div class="row margin-res">
                                                @if(!empty($allProducts))
                                                @foreach($allProducts as $singleProduct)
                                                @php
                                                $catProLangData =
                                                getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleProduct->id));
                                                if(!empty($catProLangData->name)){
                                                $productName = $catProLangData->name;
                                                }else{
                                                $productName = $singleProduct->name;
                                                }
                                                @endphp
                                                <div class="col-xl-4 col-6 col-grid-box">
                                                    <div class="product-box">
                                                        <div class="img-wrapper">
                                                            <div class="front">
                                                                <a href="{{url('product/'.$singleProduct->slug)}}">
                                                                    <img src="{{asset($singleProduct->image)}}"
                                                                        class="img-fluid blur-up lazyload "
                                                                        alt="{{$productName}}">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="product-detail">
                                                            <a class="prd-title" href="{{url('product/'.$singleProduct->slug)}}">
                                                                <h6>{{$productName}}</h6>
                                                            </a>
                                                            <!-- <p>Electrosurgical Units</p> -->
                                                            <div class="price-detail">
                                                                <div class="price">
                                                                    <!-- <p>Electrosurgical Units</p> -->
                                                                    @if(!empty($singleProduct->special_price) && $singleProduct->special_price>0)
                                                                        <h4>{{showPrice($singleProduct->special_price)}} 
                                                                            <span><del>{{showPrice($singleProduct->price)}}</del></span>
                                                                        </h4>
                                                                    @elseif($singleProduct->price>0 && $singleProduct->stock_availability==1)
                                                                        <h4>{{showPrice($singleProduct->price)}}</h4>
                                                                    @endif
                                                                </div>

                                                                <div class="buttons">
                                                                    @if($singleProduct->stock_availability==0 || $singleProduct->price<1)
                                                                        <button type="button" class="btn get-quote" data-name="{{$singleProduct->name}}" data-url="{{url('product/'.$singleProduct->slug)}}" data-page ='listing' data-id="{{base64_encode($singleProduct->id)}}">{{__('Get Quote')}}</button>
                                                                    @elseif(attributeExists($singleProduct->id))
                                                                        <a href="{{url('product/'.$singleProduct->slug)}}">
                                                                            <button type="button" class="btn view-details" data-id="{{base64_encode($singleProduct->id)}}"> <img src="{{ asset('frontend/assets/images/info.png') }}" alt="" srcset=""></button>
                                                                        </a>
                                                                    @else
                                                                        <button type="button" class="btn add-to-cart" data-id="{{base64_encode($singleProduct->id)}}"> <img src="{{ asset('frontend/assets/images/cart.jpg') }}" alt="" srcset=""></button>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <div class="product-pagination">
                                            <div class="theme-paggination-block">
                                                <div class="row">
                                                    <div class="col-xl-12 col-md-12 col-sm-12">
                                                        <nav aria-label="Page navigation">
                                                            {{ $allProducts->links() }}
                                                        </nav>
                                                    </div>
                                                    <!-- <div class="col-xl-4 col-md-4 col-sm-12">
                                                        <div class="product-search-count-bottom">
                                                            <?php 
                                                                //$start  = (($page*$perpage)-$perpage)+1;
                                                                //$end    = ($start+$allProducts->count())-1;
                                                                ?>
                                                            <h5>{{__('Showing Products')}} {{ $start}}-{{$end}}
                                                                {{__('of')}} {{$productCount}} {{__('Result')}}</h5>
                                                        </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                        @else
                                        <p>{{__('No record available to display')}}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection