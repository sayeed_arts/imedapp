@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{((isset($page_title))?$page_title:'')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb end -->
    <!-- section start -->
    <section class="section-n-space pt-30 ratio_asos">
        <div class="collection-wrapper">
            <div class="container">
                <form name="product_search_form" id="product_search_form" action="{{url($formSlug)}}" method="get">
                    <div class="row">
                        <div class="col-sm-3 collection-filter">
                            <div class="collection-filter-block">
                                <div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> back</span></div>
                                <div class="collection-collapse-block border-0 open">
                                    <h3 class="collapse-block-title">price</h3>
                                    <div class="collection-collapse-block-content">
                                        <div class="wrapper mt-3">
                                            <div class="range-slider">
                                                <input type="text" class="js-range-slider price-filter-change" value="{{$filter_price}}" name="filter_price"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(!empty($allManufacturer) && count($allManufacturer)>0)
                                <div class="collection-collapse-block open">
                                    <h3 class="collapse-block-title">Manufacturer</h3>
                                    <div class="collection-collapse-block-content">
                                        <div class="collection-brand-filter">
                                            @php
                                            $selManufArray = array();
                                            if(!empty($selectedmanufacturer)){
                                                $selManufArray = explode(',',$selectedmanufacturer);
                                            }                                            
                                            @endphp
                                            <div class="custom-control custom-checkbox collection-filter-checkbox">
                                                <input type="checkbox" class="custom-control-input manufacturer-filter-change" id="any" value="-1" @if(!empty($selManufArray) && in_array('-1',$selManufArray)) checked @endif name="selectedmanufacturer[]">
                                                <label class="custom-control-label" for="any">Clear All</label>
                                            </div>
                                            @foreach($allManufacturer as $singleMenu)
                                                <div class="custom-control custom-checkbox collection-filter-checkbox">
                                                    <input type="checkbox" class="custom-control-input manufacturer-filter-change" id="{{$singleMenu->name.$singleMenu->id}}" value="{{$singleMenu->id}}" @if(!empty($selManufArray) && in_array($singleMenu->id,$selManufArray)) checked @endif name="selectedmanufacturer[]">
                                                    <label class="custom-control-label" for="{{$singleMenu->name.$singleMenu->id}}">{{$singleMenu->name}}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            @if(isset($advertisementData->image) && $advertisementData->image!='')
                                <div class="collection-sidebar-banner">
                                    @if($advertisementData->content!='')
                                        <a href="{{$advertisementData->content}}" target="_blank">
                                            <img src="{{ asset($advertisementData->image) }}" alt="{{$advertisementData->name}}" class="img-fluid blur-up lazyload" >
                                        </a>                
                                    @else
                                        <img src="{{ asset($advertisementData->image) }}" alt="{{$advertisementData->name}}" class="img-fluid blur-up lazyload" >
                                    @endif
                                </div>
                            @endif
                        </div>
                        <div class="collection-content col">
                            <div class="page-main-content">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="top-banner-wrapper">
                                            <div class="top-banner-content small-section">
                                                <h4>{{((isset($page_title))?$page_title:'')}}</h4>
                                                {!!((isset($pageData->content))?$pageData->content:'')!!}
                                            </div>
                                        </div>
                                        <div class="collection-product-wrapper">
                                            @if(!empty($allProducts) && count($allProducts)>0)
                                                <div class="product-top-filter">
                                                    <div class="row">
                                                        <div class="col-xl-12">
                                                            <div class="filter-main-btn"><span class="filter-btn btn btn-theme"><i class="fa fa-filter" aria-hidden="true"></i> Filter</span></div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="product-filter-content">
                                                                <div class="search-count">
                                                                    <?php 
                                                                    $start  = (($page*$perpage)-$perpage)+1;
                                                                    $end    = ($start+$allProducts->count())-1;
                                                                    ?>
                                                                    <h5>Showing Products {{ $start}}-{{$end}} of {{$productCount}} Result</h5>
                                                                </div>
                                                                <div class="product-page-filter">
                                                                    <select name="sortby" id="sortby" class="sorting-filter-change">
                                                                        <option value="">Sort</option>
                                                                        <option value="price-asc" @if($sortby=='price-asc') selected @endif >Price Low to High</option>
                                                                        <option value="price-desc" @if($sortby=='price-desc') selected @endif >Price High to Low</option>
                                                                        <option value="title-asc" @if($sortby=='title-asc') selected @endif >Name A-Z</option>
                                                                        <option value="title-desc" @if($sortby=='title-desc') selected @endif >Name Z-A</option>
                                                                        <option value="date-asc" @if($sortby=='date-asc') selected @endif >Oldest</option>
                                                                        <option value="date-desc" @if($sortby=='date-desc') selected @endif >Most Recent</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-wrapper-grid">
                                                    <div class="row margin-res">
                                                        @if(!empty($allProducts))
                                                            @foreach($allProducts as $singleProduct)
                                                                <div class="col-xl-3 col-6 col-grid-box">
                                                                    <div class="product-box">
                                                                        <div class="img-wrapper">
                                                                            <div class="front">
                                                                                <a href="{{url('product/'.$singleProduct->slug)}}"><img src="{{asset($singleProduct->image)}}" class="img-fluid blur-up lazyload " alt="{{$singleProduct->name}}"></a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="product-detail">
                                                                            <a href="{{url('product/'.$singleProduct->slug)}}">
                                                                                <h6>{{$singleProduct->name}}</h6>
                                                                            </a>
                                                                            <!-- <p>Electrosurgical Units</p> -->
                                                                            @if(!empty($singleProduct->special_price))
                                                                                <h4>{{number_format($singleProduct->special_price,2)}}</h4>
                                                                                <h4>
                                                                                    <del>${{number_format($singleProduct->price,2)}}</del>
                                                                                    <!-- <span>55% off</span> -->
                                                                                </h4>
                                                                            @else
                                                                                <h4>{{number_format($singleProduct->price,2)}}</h4>
                                                                            @endif
                                                                            <button type="button" class="btn add-to-cart" data-id="{{base64_encode($singleProduct->id)}}">SHOP NOW</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="product-pagination">
                                                    <div class="theme-paggination-block">
                                                        <div class="row">
                                                            <div class="col-xl-6 col-md-6 col-sm-12">
                                                                <nav aria-label="Page navigation">
                                                                    {{ $allProducts->links() }}
                                                                </nav>
                                                            </div>
                                                            <div class="col-xl-6 col-md-6 col-sm-12">
                                                                <div class="product-search-count-bottom">
                                                                    <?php 
                                                                    $start  = (($page*$perpage)-$perpage)+1;
                                                                    $end    = ($start+$allProducts->count())-1;
                                                                    ?>
                                                                    <h5>Showing Products {{ $start}}-{{$end}} of {{$productCount}} Result</h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <p>No record available to display</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection