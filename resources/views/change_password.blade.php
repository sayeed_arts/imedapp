@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>{{$page_title}}</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$page_title}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb End -->
    <!-- section start -->
    <section class="section-b-space pt-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="account-sidebar"><a class="popup-btn">{{__('My Account')}}</a></div>
                    <div class="dashboard-left">
                        <div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> {{__('Back')}}</span></div>
                        <div class="block-content">
                            <ul>
                                <li @if(isset($currPage) && $currPage=='dashboard') class="active" @endif ><a href="{{route('user')}}"> <i><img src="{{ asset('frontend/assets/images/dashboard.png') }}" alt="" srcset=""></i> <span>{{__('Dashboard')}}</span> </a></li>
                                <li @if(isset($currPage) && $currPage=='myorders') class="active" @endif ><a href="{{route('my-orders')}}"><i><img src="{{ asset('frontend/assets/images/bag.png') }}" alt="" srcset=""></i> {{__('My Orders')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='mybids') class="active" @endif ><a href="{{route('my-bids')}}"><i><img src="{{ asset('frontend/assets/images/bag.png') }}" alt="" srcset=""></i> {{__('My Bids')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='mywishlist') class="active" @endif ><a href="{{route('my-wishlist')}}"><i><img src="{{ asset('frontend/assets/images/my-wishlist.png') }}" alt="" srcset=""></i> {{__('My Wishlist')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='editprofile') class="active" @endif ><a href="{{route('edit-profile')}}"> <i><img src="{{ asset('frontend/assets/images/my-account.png') }}" alt="" srcset=""></i>{{__('My Account')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='addressbook') class="active" @endif ><a href="{{route('address-book')}}"><i><img src="{{ asset('frontend/assets/images/address.png') }}" alt="" srcset=""></i> {{__('Address Book')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='changepassword') class="active" @endif ><a href="{{route('change-password')}}"> <i><img src="{{ asset('frontend/assets/images/password.png') }}" alt="" srcset=""></i>{{__('Change Password')}}</a></li>
                                <li class="last">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                       <i><img src="{{ asset('frontend/assets/images/logout.png') }}" alt="" srcset=""></i> <span>{{ __('Log Out') }}</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="dashboard-right">

                         <div class="page-title">
                            <h2>{{$page_title}}</h2>
                        </div>


                        <div class="dashboard">
                            <div class="page-title">
                                <!-- <h2>My Dashboard</h2> -->
                            </div>
                            <div class="box-account box-info">                              
                                <div class="row">
                                    @if (Session::has('message'))
                                        {!! successMesaage(Session::get('message')) !!}   
                                    @endif 
                                    @if (Session::has('error'))
                                        {!! errorMesaage(Session::get('error')) !!}   
                                    @endif
                                    <form method="POST" class="row text-center jqueryValidate" action="{{ route('update-change-password') }}">
                                        @csrf
                                        <div class="col-sm-6">
                                            <div class="form-group text-left">
                                                <label>{{ __('Current Password')}}</label>
                                                <input type="password" name="password" id="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" value="{{ old('password') }}">
                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6"></div>
                                        <div class="col-sm-6">
                                            <div class="form-group text-left">
                                                <label>{{ __('New Password')}}</label>
                                                <input type="password" name="new_password" id="new_password" class="form-control {{ $errors->has('new_password') ? ' is-invalid' : '' }}" value="{{ old('new_password') }}">
                                                @if ($errors->has('new_password'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('new_password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group text-left">
                                                <label>{{ __('Confirm Password')}}</label>
                                                <input type="password" name="confirm_password" id="confirm_password" class="form-control {{ $errors->has('confirm_password') ? ' is-invalid' : '' }}" value="{{ old('confirm_password') }}">
                                                @if ($errors->has('confirm_password'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('confirm_password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-4 offset-md-4 text-center">
                                            <button type="submit" class="btn btn-success getStarted btn-block">{{ __('Update')}}</button>
                                        </div>          
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section end -->
@endsection