@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>{{$page_title}}</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a> <i
                                class=" fa fa-angle-right"></i></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$page_title}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb End -->
<!-- section start -->
<section class="section-b-space pt-30">
    <div class="container">
        <div class="checkout-page">
            <div class="checkout-form">
                @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}
                @endif
                @if (Session::has('error'))
                {!! errorMesaage(Session::get('error')) !!}
                @endif
                {!! validationError($errors) !!}
                <form name="checkoutform" action="{{ route('place-order')}}" method="POST" class="jqueryValidate">
                    @csrf
                    <div class="row">
                        <div class="col-lg-7 col-sm-12 col-xs-12">
                            <div class="checkout-title">
                                <h3>{{__('Billing Details')}}</h3>
                            </div>
                            @if(!empty($address))
                            <div class="row check-out">
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="field-label">{{__('First Name')}}</div>
                                    <input type="text" name="first_name" value="{{$address->first_name}}" required>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="field-label">{{__('Last Name')}}</div>
                                    <input type="text" name="last_name" value="{{$address->last_name}}" required>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="field-label">{{__('Company Name')}}</div>
                                    <input type="text" name="company" value="{{$address->company}}">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="field-label">{{__('Phone')}}</div>
                                    <input type="text" name="phone" value="{{$address->phone}}" required>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="field-label">{{__('Address')}}</div>
                                    <input type="text" name="street_address" value="{{$address->address1}}" required>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="field-label">{{__('Address 2')}}</div>
                                    <input type="text" name="apartment" value="{{$address->address2}}">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-6">
                                    <div class="field-label">{{__('Select Country')}}</div>
                                    <select class="form-control" name="country" id="address_country" required>
                                        <option value="">{{__('Select Country')}}</option>
                                        @if(!empty($allCountries))
                                        @foreach($allCountries as $singleCountry)
                                        <option value="{{$singleCountry->countryID}}"
                                            {{((isset($address->country) && $address->country==$singleCountry->countryID)?'Selected':'') }}>
                                            {{$singleCountry->countryName}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-6">
                                    <div class="field-label">{{__('Select State')}}</div>
                                    <select class="form-control" name="state" id="address_state" required>
                                        <option value="">{{__('Select State')}}</option>
                                        @if(!empty($allStates))
                                        @foreach($allStates as $singleState)
                                        <option value="{{$singleState->stateID}}"
                                            {{((isset($address->state) && $address->state==$singleState->stateID)?'Selected':'') }}>
                                            {{$singleState->stateName}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <div class="field-label">{{__('City')}}</div>
                                    <input type="text" name="town" value="{{$address->city}}" required>
                                </div>
                                <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                    <div class="field-label">{{__('Postal Code')}}</div>
                                    <input type="text" name="postcode" value="{{$address->zip_code}}" required>
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="checkbox" name="ship_to_different_address"
                                        id="checkout-shipping-option" value="1" onclick="toggleshippingdetails()">
                                    &ensp;
                                    <label for="checkout-shipping-option">{{__('Ship To Different Address')}}</label>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </div>
                            <div class="shipping-details-section" style="display:none;">
                                <div class="checkout-title">
                                    <h3>{{__('Shipping Details')}}</h3>
                                </div>
                                <div class="row check-out">
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">{{__('First Name')}}</div>
                                        <input type="text" class="shipping-content" name="shipping_first_name"
                                            value="{{$address->first_name}}">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">{{__('Last Name')}}</div>
                                        <input type="text" class="shipping-content" name="shipping_last_name"
                                            value="{{$address->last_name}}">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">{{__('Company Name')}}</div>
                                        <input type="text" name="shipping_company" value="{{$address->company}}">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">{{__('Phone')}}</div>
                                        <input type="text" class="shipping-content" name="shipping_phone"
                                            value="{{$address->phone}}">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">{{__('Address')}}</div>
                                        <input type="text" class="shipping-content" name="shipping_street_address"
                                            value="{{$address->address1}}">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">{{__('Address 2')}}</div>
                                        <input type="text" name="shipping_apartment" value="{{$address->address2}}">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                                        <div class="field-label">{{__('Select Country')}}</div>
                                        <select class="shipping-content form-control" name="shipping_country"
                                            id="address_shipping_country">
                                            <option value="">{{__('Select Country')}}</option>
                                            @if(!empty($allCountries))
                                            @foreach($allCountries as $singleCountry)
                                            <option value="{{$singleCountry->countryID}}"
                                                {{((isset($address->country) && $address->country==$singleCountry->countryID)?'Selected':'') }}>
                                                {{$singleCountry->countryName}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                                        <div class="field-label">{{__('Select State')}}</div>
                                        <select class="shipping-content form-control" name="shipping_state"
                                            id="address_shipping_state">
                                            <option value="">{{__('Select State')}}</option>
                                            @if(!empty($allStates))
                                            @foreach($allStates as $singleState)
                                            <option value="{{$singleState->stateID}}"
                                                {{((isset($address->state) && $address->state==$singleState->stateID)?'Selected':'') }}>
                                                {{$singleState->stateName}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <div class="field-label">{{__('City')}}</div>
                                        <input type="text" class="shipping-content" name="shipping_town"
                                            value="{{$address->city}}">
                                    </div>
                                    <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                        <div class="field-label">{{__('Postal Code')}}</div>
                                        <input type="text" class="shipping-content" name="shipping_postcode"
                                            value="{{$address->zip_code}}">
                                    </div>
                                </div>
                            </div>
                            @else
                            {{__('Please')}} <a href="{{url('user/address-book')}}">{{__('click here')}}</a>
                            {{__('to update your default address')}}.
                            @endif
                        </div>
                        <div class="col-lg-5 col-sm-12 col-xs-12">
                            <div class="checkout-details">
                                @php
                                $subTotalAmount = 0;
                                @endphp
                                @if(!empty($allCartItems) && count($allCartItems)>0)
                                <div class="order-box">
                                    <div class="title-box">
                                        <div>{{__('Product Name')}} <span>{{__('Total')}}</span></div>
                                    </div>
                                    <ul class="qty">
                                        @foreach($allCartItems as $singleCartItem)
                                        @php
                                        $cartProLangData =
                                        getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleCartItem->product_id));
                                        if(!empty($cartProLangData->name)){
                                        $productName = $cartProLangData->name;
                                        }else{
                                        $productName = $singleCartItem->name;
                                        }
                                        $qty = $singleCartItem->qty;
                                        $price = $singleCartItem->price;
                                        $special_price = $singleCartItem->special_price;
                                        $stock_availability = $singleCartItem->stock_availability;
                                        $inventory_management = $singleCartItem->inventory_management;
                                        $availableQty = $singleCartItem->availableQty;
                                        if(!empty($special_price) && $special_price>0){
                                        $proPrice = $special_price;
                                        }else{
                                        $proPrice = $price;
                                        }

                                        $attrProNAme = '';
                                        if(!empty($singleCartItem->product_attr_id)){
                                        $getProAttrCart = DB::table('products_attributes')
                                        ->where('id',$singleCartItem->product_attr_id)
                                        ->where('product_id',$singleCartItem->product_id)
                                        ->where('is_variation',1)
                                        ->first();
                                        $proAttribute_value
                                        =(isset($getProAttrCart->attribute_value))?$getProAttrCart->attribute_value:'';
                                        $getAttrCartData = array();

                                        if(!empty($proAttribute_value)){
                                        $getAttrCartData = DB::table('attribute_values')
                                        ->where('id',$proAttribute_value)
                                        ->first();
                                        }
                                        $attrProNAme = (isset($getAttrCartData->name))?$getAttrCartData->name:'';
                                        $proPrice = (isset($getProAttrCart->attribute_price) &&
                                        $getProAttrCart->attribute_price>0)?$getProAttrCart->attribute_price:$proPrice;
                                        }
                                        if(($inventory_management==0 && $stock_availability==1) ||
                                        ($inventory_management==1 && $stock_availability==1 && $availableQty>=$qty)){
                                        $cartQty = $qty;
                                        }else if($inventory_management==1 && $stock_availability==1 && $availableQty <
                                            $qty){ $cartQty=$availableQty; }else{ $cartQty='0' ; }
                                            $totalPrice=$cartQty*$proPrice; $subTotalAmount=$subTotalAmount+$totalPrice;
                                            @endphp <li>
                                            <span class="prd-name">
                                                {{wordsLimit($productName,15)}}
                                                @if(!empty($attrProNAme))
                                                <br>
                                                {{__($attrProNAme)}}
                                                @endif
                                            </span> × {{$cartQty}}
                                            <span class="cost">{{showPrice($totalPrice)}}</span>
                                            </li>
                                            @endforeach
                                    </ul>
                                    <ul class="sub-total">
                                        @php
                                        $shipping_type  = $generalSettings->shipping_type;
                                        $taxPercent     = $generalSettings->tax;
                                        $handling_tax   = $generalSettings->handling_tax;
                                        if($shipping_type==1){
                                        $shippingCharges = $generalSettings->flat_rate_fee;
                                        }else{
                                        $shippingCharges = 0.00;
                                        }
                                        $handling_fee   = ($subTotalAmount*$handling_tax)/100;
                                        $taxAmount      = ($subTotalAmount*$taxPercent)/100;
                                        $grandTotal     = $subTotalAmount+$taxAmount+$shippingCharges+$handling_fee-$availedDiscount;
                                        $without_handling_fee_grandTotal = $subTotalAmount+$taxAmount+$shippingCharges-$availedDiscount;
                                        @endphp
                                        <li>{{__('Subtotal')}} <span class="count">{{showPrice($subTotalAmount)}}</span></li>
                                        <li>{{__('Tax')}} <span class="count">{{showPrice($taxAmount)}} </span></li>
                                        <li>{{__('Discount')}} <span class="count">{{showPrice($availedDiscount)}}</span></li>
                                        <li>{{__('Shipping')}} <span class="count">{{showPrice($shippingCharges,2)}}</span></li>
                                        <li id="handling_fee">{{__('Handling Fee')}} <span class="count">{{showPrice($handling_fee)}} </span></li>
                                    </ul>
                                    <ul class="total">
                                        <li id="with_handling_fee">{{__('Total')}} <span class="count">{{showPrice($grandTotal)}}</span></li>
                                        <li id="without_handling_fee" style="display:none;">{{__('Total')}} <span class="count">{{showPrice($without_handling_fee_grandTotal)}}</span></li>
                                    </ul>
                                </div>
                                <div class="payment-box">
                                    <div class="upper-box">
                                        <div class="payment-options">
                                            <ul>
                                                <li>
                                                    <div class="radio-option">
                                                        <input type="radio" name="payment_option" id="payment-2"
                                                            value="1" class="payment-method">
                                                        <label for="payment-2">{{__('Request a Quote')}}
                                                            <span class="small-text">{{__('Please send a check to Store Name, Store Street, Store Town, Store State / County, Store Postcode.')}}</span>
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radio-option paypal">
                                                        <input type="radio" name="payment_option" id="payment-3" checked
                                                            value="2" class="payment-method">
                                                        <label  for="payment-3">{{__('MasterCard/ Credit Card/ Debit Card.')}}<span
                                                                class="image"><img
                                                                    src="{{ asset('frontend/assets/images/paypal.png')}}"
                                                                    alt=""></span></label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <input type="submit" class="btn-solid btn" name="placeorder"
                                            value="{{__('Place Order')}}">
                                    </div>
                                </div>
                                @else
                                <p>{{__('Your Cart is Empty')}}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- section end -->
@endsection