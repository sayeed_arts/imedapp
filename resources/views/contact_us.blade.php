@extends('layouts.app')
@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>{!!$page_title!!}</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active">{!!$page_title!!}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<section class="content-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-left">
                {!!$pageData->content!!}
            </div>
        </div>
        <div class="row mt-1">
            <div class="col-sm-12">
                <h2 class="text-center text-green mb-5">{{__('For Inquiry')}}</h2>
                @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}
                @endif
            </div>
        </div>
        <form method="POST" class="row text-center jqueryValidate" action="{{ route('post-contact-us') }}">
            @csrf
            <div class="col-sm-6">
                <div class="form-group text-left">
                    <label>{{__('First Name')}}</label>
                    <input type="text" name="name" id="name"
                        class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" required
                        value="{{ old('name') }}">
                    @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group text-left">
                    <label>{{__('Last Name')}}</label>
                    <input type="text" name="last_name" id="last_name"
                        class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" required
                        value="{{ old('last_name') }}">
                    @if ($errors->has('last_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group text-left">
                    <label>{{__('Phone')}}</label>
                    <input type="text" name="phone"
                        class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" required
                        value="{{ old('phone') }}" minlength="10">
                    @if ($errors->has('phone'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group text-left">
                    <label>{{__('Email')}}</label>
                    <input type="email" name="email"
                        class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" required
                        value="{{ old('email') }}">
                    @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group text-left">
                    <label>{{__('Comments')}}</label>
                    <textarea name="message" class="form-control {{ $errors->has('message') ? ' is-invalid' : '' }}"
                        required id="exampleFormControlTextarea1" rows="3">{{ old('message') }}</textarea>
                    @if ($errors->has('message'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('message') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="col-sm-4 offset-md-4 text-center">
                <button type="submit" class="btn btn-success getStarted btn-block">{{__('Submit')}}</button>
            </div>
        </form>
    </div>
</section>
@endsection