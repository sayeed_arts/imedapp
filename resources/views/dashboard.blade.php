@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>{{$page_title}}</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a> <i
                                class=" fa fa-angle-right"></i></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$page_title}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb End -->
    <!-- section start -->
    <section class="section-b-space pt-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="account-sidebar"><a class="popup-btn">{{__('My Account')}}</a></div>
                    <div class="dashboard-left">
                        <div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> {{__('Back')}}</span></div>
                        <div class="block-content">
                            <ul>
                                <li @if(isset($currPage) && $currPage=='dashboard') class="active" @endif ><a href="{{route('user')}}"> <i><img src="{{ asset('frontend/assets/images/dashboard.png') }}" alt="" srcset=""></i> <span>{{__('Dashboard')}}</span> </a></li>
                                <li @if(isset($currPage) && $currPage=='myorders') class="active" @endif ><a href="{{route('my-orders')}}"><i><img src="{{ asset('frontend/assets/images/bag.png') }}" alt="" srcset=""></i> {{__('My Orders')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='mybids') class="active" @endif ><a href="{{route('my-bids')}}"><i><img src="{{ asset('frontend/assets/images/bag.png') }}" alt="" srcset=""></i> {{__('My Bids')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='mywishlist') class="active" @endif ><a href="{{route('my-wishlist')}}"><i><img src="{{ asset('frontend/assets/images/my-wishlist.png') }}" alt="" srcset=""></i> {{__('My Wishlist')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='editprofile') class="active" @endif ><a href="{{route('edit-profile')}}"> <i><img src="{{ asset('frontend/assets/images/my-account.png') }}" alt="" srcset=""></i>{{__('My Account')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='addressbook') class="active" @endif ><a href="{{route('address-book')}}"><i><img src="{{ asset('frontend/assets/images/address.png') }}" alt="" srcset=""></i> {{__('Address Book')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='changepassword') class="active" @endif ><a href="{{route('change-password')}}"> <i><img src="{{ asset('frontend/assets/images/password.png') }}" alt="" srcset=""></i>{{__('Change Password')}}</a></li>
                                <li class="last">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                       <i><img src="{{ asset('frontend/assets/images/logout.png') }}" alt="" srcset=""></i> <span>{{ __('Log Out') }}</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="dashboard-right">
                        <div class="dashboard">
                            <div class="page-title">
                                <!-- <h2>{{$page_title}}</h2> -->
                                <h2>My Profile</h2>
                            </div>

                            <div class="row dash-top-row">
                                <div class="col-md-6">
                                    <div class="dash-block first-block">
                                        <i class="profile-pic">
                                            @if(!empty($user->profile_picture))
                                            <img src="{{ asset($user->profile_picture)}}" alt="image" height="150" width="150">
                                            @else
                                            <img src="{{ asset('frontend/assets/images/avatar.png') }}" alt="" srcset="">
                                            @endif

                                           
                                        </i> 
                                        <div class="info">
                                            <h6>{{$user->name.' '.$user->last_name}}</h6>
                                            <p> <a href="mailto:{{$user->email}}"> {{$user->email}}</a></p>
                                            <a class="btn" href="{{url('user/edit-profile')}}">{{__('Update Profile')}}</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="dash-block">
                                     <i class="icon"><img src="{{ asset('frontend/assets/images/bag-1.png') }}" alt="" srcset=""></i> 
                                     <h3>5</h3>
                                     <p>Total Orders</p>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="dash-block">
                                        <i class="icon"><img src="{{ asset('frontend/assets/images/wishlist.png') }}" alt="" srcset=""></i> 
                                        <h3>5</h3>
                                        <p>Wishlist</p>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="dash-block">
                                    <i class="icon"><img src="{{ asset('frontend/assets/images/delivery.png') }}" alt="" srcset=""></i> 
                                    <h3>5</h3>
                                    <p>Awaiting Delivery</p>
                                    </div>
                                </div>



                            </div>
                            <!-- <div class="welcome-msg">
                                <p>{{__('Hello')}}, {{$user->name.' '.$user->last_name}} !</p>
                                <p>{{__('From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.')}}</p>
                            </div> -->


                            <div class="box-account box-info">
                                <!-- <div class="box-head">
                                    <h2>{{__('Account Information')}}</h2>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="box">
                                            <div class="box-title">
                                                <h3>{{__('Contact Information')}}</h3><a href="{{url('user/edit-profile')}}">{{__('Edit')}}</a>
                                            </div>
                                            <div class="box-content">
                                                <h6>{{$user->name.' '.$user->last_name}}</h6>
                                                <h6>{{$user->email}}</h6>
                                                <h6><a href="{{url('user/change-password')}}">{{__('Change Password')}}</a></h6>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div>
                                    <div class="box">

                                        <div class="page-title" style="display:flex; justify-content:space-between; align-items:center">
                                            <h2>{{__('My Address Book')}}</h2>
                                            <a href="{{url('user/address-book')}}">{{__('Manage Addresses')}}</a>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h6>{{__('Default Address')}}</h6>
                                                <address>
                                                    @if(!empty($address))
                                                        <span>{!!$address->first_name.' '.$address->last_name.' '.$address->company.'</span><span class="address-1"> '.$address->address1.' '.$address->city.', '.$address->stateName.' '.$address->countryName.' '.$address->zip_code!!}</span><span>
                                                        {{__('Contact No')}}: {{$address->phone}} </span>
                                                    @else
                                                        {{__('You have not set a default billing address')}}.
                                                        <br>
                                                        <a href="{{url('user/address-book')}}">{{__('Edit Address')}}</a>
                                                    @endif
                                                </address>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section end -->
@endsection