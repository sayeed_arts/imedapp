@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>{{$page_title}}</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$page_title}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb End -->
    <!-- section start -->
    <section class="section-b-space pt-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="account-sidebar"><a class="popup-btn">{{__('My Account')}}</a></div>
                    <div class="dashboard-left">
                        <div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> {{__('Back')}}</span></div>
                        <div class="block-content">
                        <ul>
                                <li @if(isset($currPage) && $currPage=='dashboard') class="active" @endif ><a href="{{route('user')}}"> <i><img src="{{ asset('frontend/assets/images/dashboard.png') }}" alt="" srcset=""></i> <span>{{__('Dashboard')}}</span> </a></li>
                                <li @if(isset($currPage) && $currPage=='myorders') class="active" @endif ><a href="{{route('my-orders')}}"><i><img src="{{ asset('frontend/assets/images/bag.png') }}" alt="" srcset=""></i> {{__('My Orders')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='mybids') class="active" @endif ><a href="{{route('my-bids')}}"><i><img src="{{ asset('frontend/assets/images/bag.png') }}" alt="" srcset=""></i> {{__('My Bids')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='mywishlist') class="active" @endif ><a href="{{route('my-wishlist')}}"><i><img src="{{ asset('frontend/assets/images/my-wishlist.png') }}" alt="" srcset=""></i> {{__('My Wishlist')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='editprofile') class="active" @endif ><a href="{{route('edit-profile')}}"> <i><img src="{{ asset('frontend/assets/images/my-account.png') }}" alt="" srcset=""></i>{{__('My Account')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='addressbook') class="active" @endif ><a href="{{route('address-book')}}"><i><img src="{{ asset('frontend/assets/images/address.png') }}" alt="" srcset=""></i> {{__('Address Book')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='changepassword') class="active" @endif ><a href="{{route('change-password')}}"> <i><img src="{{ asset('frontend/assets/images/password.png') }}" alt="" srcset=""></i>{{__('Change Password')}}</a></li>
                                <li class="last">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                       <i><img src="{{ asset('frontend/assets/images/logout.png') }}" alt="" srcset=""></i> <span>{{ __('Log Out') }}</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="dashboard-right">
                        <div class="dashboard">
                            <div class="page-title">
                                <h2>{{$page_title}}</h2>
                            </div>
                            <div class="box-account box-info">
                                <div class="row">
                                    @if (Session::has('message'))
                                        {!! successMesaage(Session::get('message')) !!}   
                                    @endif 
                                    @if (Session::has('error'))
                                        {!! errorMesaage(Session::get('error')) !!}   
                                    @endif
                                    <form method="POST" class="row text-center jqueryValidate" action="{{ route('update-profile') }}">
                                        @csrf
                                        <input type="hidden" name="old_email" value="{{ (isset($user->email))?$user->email:'' }}">
                                        <div class="col-sm-6">
                                            <div class="form-group text-left">
                                                <label>{{__('First Name')}}</label>
                                                <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" required  value="{{ (isset($user->name))?$user->name:'' }}">
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group text-left">
                                                <label>{{__('Last Name')}}</label>
                                                <input type="text" name="last_name" id="last_name" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" required  value="{{ (isset($user->last_name))?$user->last_name:'' }}">
                                                @if ($errors->has('last_name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="col-sm-6">
                                            <div class="form-group text-left">
                                                <label>{{__('Profile Picture')}}</label>
                                                <input type="file" id="image-input" name="profile_picture" class="form-control">
                                                @if(!empty($user->profile_picture))
                                                <img src="{{ asset($user->profile_picture)}}" alt="image" height="150" width="150">
                                                @endif

                                            </div>
                                        </div>



                                       


                                        <div class="col-sm-6">
                                            <div class="form-group text-left">
                                                <label>{{__('Email')}}</label>
                                                <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" required  value="{{ (isset($user->email))?$user->email:'' }}">
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group text-left">
                                                <label>{{__('Phone')}}</label>
                                                <input type="text" name="phone_number" class="form-control {{ $errors->has('phone_number') ? ' is-invalid' : '' }}" required  value="{{ (isset($user->phone_number))?$user->phone_number:'' }}" minlength="10">
                                                @if ($errors->has('phone_number'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-4 offset-md-4 text-center">
                                            <button type="submit" class="btn btn-success getStarted btn-block">{{__('Update')}}</button>
                                        </div>          
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section end -->
@endsection