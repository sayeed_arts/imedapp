<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Invoice</title>    
    <style>
	    .invoice-box {
	        max-width: 800px;
	        margin: auto;
	        padding: 30px;
	        border: 1px solid #eee;
	        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
	        font-size: 16px;
	        line-height: 24px;
	        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
	        color: #555;
	    }    
	    .invoice-box table {
	        width: 100%;
	        line-height: inherit;
	        text-align: left;
	    }    
	    .invoice-box table td {
	        padding: 5px;
	        vertical-align: top;
	    }    
	    .invoice-box table.totals tr td:nth-child(2),.invoice-box table tr.information td:nth-child(2){
	        text-align: right;
	    }
	    
	    .invoice-box table tr.top table td {
	        padding-bottom: 20px;
	    }
	    
	    .invoice-box table tr.top table td.title {
	        font-size: 45px;
	        line-height: 45px;
	        color: #333;
	    }
	    
	    .invoice-box table tr.information table td {
	        padding-bottom: 40px;
	    }	    
	    .invoice-box table tr.heading td {
	        background: #eee;
	        border-bottom: 1px solid #ddd;
	        font-weight: bold;
			padding:10px 20px;
	    }		
		.invoice-box table tr.heading td.last {
			text-align:right;
	    }
	    .invoice-box table tr.details td {
	        padding-bottom: 20px;
	    }	    
	    .invoice-box table tr.item td{
	        border-bottom: 1px solid #eee;
			padding:10px 20px;
	    }	    
	    .invoice-box table tr.item td.last {
			text-align:right
	    }		
		.invoice-box table tr.item.last td {
	        border-bottom: none;
	    }	    
	    .invoice-box table.totals {
	        border-top: 0px solid #eee;
			background:#f5f5f5;
			padding:20px 0px;
	    }		
		.invoice-box table.totals td{
	        padding:5px 20px;
	    }		
		.invoice-box table.totals .ttl{
			background:#f5f5f5;
			padding:0px 0px;
		}		
		.invoice-box table.totals .ttl td{
			padding:10px 20px;
		}		
		.invoice-box table.totals .ttl h3{
			margin:0px;	
		}		
		.center{
			text-align:center;
		}	    
	    @media only screen and (max-width: 600px) {
	        .invoice-box table tr.top table td {
	            width: 100%;
	            display: block;
	            text-align: center;
	        }	        
	        .invoice-box table tr.information table td {
	            width: 100%;
	            display: block;
	            text-align: center;
	        }
	    }	    
	    /** RTL **/
	    .rtl {
	        direction: rtl;
	        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
	    }	    
	    .rtl table {
	        text-align: right;
	    }	    
	    .rtl table tr td:nth-child(2) {
	        text-align: left;
	    }
    </style>
</head>
<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="{{asset('frontend/assets/images/icon/logo.png')}}" style="width:100%; max-width:300px;">
                            </td>                            
                            <td style="text-align:right;">
                                <b>
                                @if($orderData->payment_option==1)
	                                {{__('PO')}}
	                            @else
	                                {{__('Invoice')}}
	                            @endif
	                        	#:</b> {{$orderData->id}}<br>
                                <b>{{__('Date')}}:</b> {{displayDateMD($orderData->ordered_on)}}<br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
		</table>
	 	<table cellpadding="0" cellspacing="0">
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
							<b>{{__('Shipping Address')}}</b><br>
                                {{$orderData->shipping_first_name.' '.$orderData->shipping_last_name}}<br>
                                @if(!empty($orderData->shipping_company))
                                    {{$orderData->shipping_company}}<br>
                                @endif
                                {{$orderData->shipping_street_address.', '.$orderData->shipping_apartment}}<br>
                                {{$orderData->shipping_town.', '.$shippingState}}<br>
                                {{$shippingCountry.', '.$orderData->shipping_postcode}}<br>
                                {{__('Contact No')}}. {{$orderData->shipping_phone}}
                            </td>                            
                            <td>
							<b>{{__('Billing Address')}}</b><br>
                                {{$orderData->first_name.' '.$orderData->last_name}}<br>
                                @if(!empty($orderData->company))
                                    {{$orderData->company}}<br>
                                @endif
                                {{$orderData->street_address.', '.$orderData->apartment}}<br>
                                {{$orderData->town.', '.$billingState}}<br>
                                {{$billingCountry.', '.$orderData->postcode}}<br>
                                {{__('Contact No')}}. {{$orderData->phone}}
                            </td>
                        </tr>
                    </table>
                </td>	
            </tr>            
      	</table>  
       	<table cellpadding="0" cellspacing="0">  
            <tr class="heading">
                <td>{{__('Product Name')}}</td>                
                <td class="center">{{__('Quantity')}}</td>
				<td>{{__('Price')}}</td>
				<td class="last">{{__('Subtotal')}}</td>				
            </tr>
            @if(!empty($orderProducts))
                @foreach($orderProducts as $singleProducts)
                	@php
                		$orderProLangData = getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleProducts->product_id));
                        if(!empty($orderProLangData->name)){
                            $productName = $orderProLangData->name;
                        }else{
                            $productName = $singleProducts->name;
                        }
                		$proPrice = 0;
                		if(!empty($singleProducts->saleprice) && $singleProducts->saleprice>0){
                            $proPrice = $singleProducts->saleprice;
                		}
                        else{
                            $proPrice = $singleProducts->price;
                        }
                		$subPrice = $proPrice*$singleProducts->quantity;
                	@endphp    
		            <tr class="item">
		                <td>{{$productName}}</td>
						<td class="center">{{$singleProducts->quantity}}</td>
						<td>
		                    @if(!empty($singleProducts->saleprice) && $singleProducts->saleprice>0)
                                {{showOrderPrice($singleProducts->saleprice,$orderData->currency_code,$orderData->exchange_rate)}}
                            @else
                                {{showOrderPrice($singleProducts->price,$orderData->currency_code,$orderData->exchange_rate)}}
                            @endif
		                </td>                
		                <td  class="last">
		                    {{showOrderPrice($subPrice,$orderData->currency_code,$orderData->exchange_rate)}}
		                </td>
		            </tr>            
                @endforeach
            @endif
		</table>
		<table cellpadding="0" cellspacing="0" class="totals">
            <tr>
                <td><b>{{__('Subtotal')}}</b></td>                
                <td>
                   {{showOrderPrice($orderData->subtotal,$orderData->currency_code,$orderData->exchange_rate)}}
                </td>
            </tr>	
			<tr>
                <td><b>{{__('Tax(GST)')}}</b></td>                
                <td>
                   {{showOrderPrice($orderData->tax,$orderData->currency_code,$orderData->exchange_rate)}}
                </td>
            </tr>	
            <tr>
                <td><b>{{__('Discount')}}</b></td>                
                <td>
                   {{showOrderPrice($orderData->coupon_discount,$orderData->currency_code,$orderData->exchange_rate)}}
                </td>
            </tr>
			<tr>
                <td><b>{{__('Shipping')}}</b></td>                
                <td>
                   {{showOrderPrice($orderData->shipping,$orderData->currency_code,$orderData->exchange_rate)}}
                </td>
            </tr>		
			<tr class="ttl">
                <td><h3>{{__('Total')}}</h3></td>                
                <td>
                   <h3>{{showOrderPrice($orderData->total,$orderData->currency_code,$orderData->exchange_rate)}}</h3>
                </td>
            </tr>
        </table>		
		<p style="text-align:center; font-size:13px; "><b>{{__('NOTE')}} :</b> {{__('This is computer generated receipt and does not require physical signature.')}}</p>
    </div>
</body>
</html>