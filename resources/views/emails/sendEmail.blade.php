<div style="width:800px; margin:0 auto; font-family:Open Sans,sans-serif;">
	<table width="800" border="0" cellpadding="0" cellspacing="0" style="width:800px; margin:0 auto; font-family:Lato,sans-serif;">
		<tr>
			<td align="left" width="50%" style=" padding-left:10px; vertical-align:bottom; width:50%; font-size:14px; margin:0px; text-align:left; line-height:32px; color:#000;">
				<img src="{{url('/') }}/{{ $wl_logo }}" style="width:120px;" alt="{{ $productName }}">
			</td>
			<td align="right" width="50%" style=" padding-left:10px; width:50%; font-size:14px; margin:0px; text-align:right; line-height:32px; color:#000;">Date : <?php echo (date('M d, Y'));?></td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td colspan="2"  style=" border-bottom:1px #ccc solid;  border-top:1px #ccc solid;  padding:10px; font-size:14px; margin:0px; text-align:left;  color:#000;"><p></p>
				{!!$emailContent!!}
			</td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td align="center" colspan="2" width="100%" style=" padding-left:10px; width:100%; font-size:17px; margin:0px; text-align:center; line-height:22px; color:#0089cb;"><b style="text-align:center;"><br />Thanks,<br />{{ $mailSignatureName }}</b></td>
		</tr>
	</table>
</div>