<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Email Template </title>
    <style type="text/css">
    body {
        text-align: center;
        margin: 0 auto;
        width: 650px;
        font-family: 'Arial', sans-serif;
        background-color: #e2e2e2;
        display: block;
    }

    ul {
        margin: 0;
        padding: 0;
    }

    li {
        display: inline-block;
        text-decoration: unset;
    }

    a {
        text-decoration: none;
    }

    p {
        margin: 15px 0;
    }

    h5 {
        color: #444;
        text-align: left;
        font-weight: 400;
    }

    .text-center {
        text-align: center
    }

    .main-bg-light {
        background-color: #fafafa;
    }

    .title {
        color: #444444;
        font-size: 22px;
        font-weight: bold;
        margin-top: 10px;
        margin-bottom: 10px;
        padding-bottom: 0;
        text-transform: uppercase;
        display: inline-block;
        line-height: 1;
    }

    table {
        margin-top: 30px
    }

    table.top-0 {
        margin-top: 0;
    }

    table.order-detail {
        border: 1px solid #ddd;
    }

    table.order-detail h5 {
        font-size: 16px;
    }

    table.order-detail tr:nth-child(even) {
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
    }

    table.order-detail tr:nth-child(odd) {
        border-bottom: 1px solid #ddd;
    }

    .pad-left-right-space {
        border: unset !important;
    }

    .pad-left-right-space td {
        padding: 5px 15px;
    }

    .pad-left-right-space td p {
        margin: 0;
    }

    .pad-left-right-space td b {
        font-size: 15px;
        font-family: 'Roboto', sans-serif;
    }

    .order-detail th {
        font-size: 16px;
        padding: 15px;
        background: #fafafa;
    }

    .order-detail tr.pad-left-right-space td {
        text-align: left;

    }

    .footer-social-icon tr td img {
        margin-left: 5px;
        margin-right: 5px;
    }
    </style>
</head>

<body style="margin: 20px auto;">


    @if($orderData->payment_option==1)
    <!-- REQUEST QUOTE MAIL -->
    <table align="center" border="0" cellpadding="0" cellspacing="0"
        style="padding: 0 30px;background-color: #fff; -webkit-box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);width: 100%;">
        <tbody>
            <tr>
                <td>
                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="text-align: left;"
                        width="100%">
                        <tr>
                            <td style="text-align: center;">
                                <img src="{{asset('frontend/assets/images/icon/logo.png')}}" width="250" alt=""
                                    style=";margin-bottom: 30px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p style="font-size: 14px;"><b>{{__('Hi')}} {{$orderData->first_name}}
                                        {{$orderData->last_name}},</b></p>
                                <p style="font-size: 14px;">
                                    {{__('Your requested quote has been successfully generated. Please submit your purchase order.')}}
                                </p>
                                <p>Please <a href="{{url('user/submit-po/'.$orderData->order_id)}}"
                                        class="btn btn-success">
                                        {{__('Click Here')}} </a> to Submit Your Purchase Order.
                                </p>
                                @if(!empty($orderData->transaction_id))
                                <p style="font-size: 14px;">{{__('Transaction ID')}} : {{$orderData->transaction_id}}
                                </p>
                                @endif
                                <p>Your Order Number is : <b>SO-IMED-{{ $orderData->id }}</b> </p>
                            </td>
                        </tr>
                    </table>


                    <table cellpadding="0" cellspacing="0" border="0" align="left"
                        style="width: 100%;margin-top: 10px; margin-bottom: 10px;">
                        <tbody>
                            <tr>
                                <td
                                    style="background-color: #fafafa;border: 1px solid #ddd;padding: 15px;letter-spacing: 0.3px;width: 50%;">
                                    <h5
                                        style="font-size: 16px; font-weight: 600;color: #000; line-height: 16px; padding-bottom: 13px; border-bottom: 1px solid #e6e8eb; letter-spacing: -0.65px; margin-top:0; margin-bottom: 13px;">
                                        {{__('Shipping Address')}}</h5>
                                    <p
                                        style="text-align: left;font-weight: normal; font-size: 14px; color: #000000;line-height: 21px;    margin-top: 0;">
                                        {{$orderData->shipping_first_name.' '.$orderData->shipping_last_name}}<br>
                                        @if(!empty($orderData->shipping_company))
                                        {{$orderData->shipping_company}}<br>
                                        @endif
                                        {{$orderData->shipping_street_address.', '.$orderData->shipping_apartment}}<br>
                                        {{$orderData->shipping_town.', '.$shippingState}},
                                        {{$shippingCountry.', '.$orderData->shipping_postcode}}<br>
                                        {{__('Contact No')}}. {{$orderData->shipping_phone}}
                                    </p>
                                </td>
                                <td><img src="{{asset('frontend/assets/images/email-temp/space.jpg')}}" alt=" "
                                        height="25" width="30">
                                </td>
                                <td
                                    style="background-color: #fafafa;border: 1px solid #ddd;padding: 15px;letter-spacing: 0.3px;width: 50%;">
                                    <h5
                                        style="font-size: 16px;font-weight: 600;color: #000; line-height: 16px; padding-bottom: 13px; border-bottom: 1px solid #e6e8eb; letter-spacing: -0.65px; margin-top:0; margin-bottom: 13px;">
                                        {{__('Billing Address')}}:</h5>
                                    <p
                                        style="text-align: left;font-weight: normal; font-size: 14px; color: #000000;line-height: 21px;    margin-top: 0;">
                                        {{$orderData->first_name.' '.$orderData->last_name}}<br>
                                        @if(!empty($orderData->company))
                                        {{$orderData->company}}<br>
                                        @endif
                                        {{$orderData->street_address.', '.$orderData->apartment}}<br>
                                        {{$orderData->town.', '.$billingState}}<br>
                                        {{$billingCountry.', '.$orderData->postcode}}<br>
                                        {{__('Contact No')}}. {{$orderData->phone}}
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="order-detail" border="0" cellpadding="0" cellspacing="0" align="left"
                        style="width: 100%; margin-bottom: 50px;">
                        <tr align="left">
                            <th>{{__('Product Name')}}</th>
                            <th>{{__('Quantity')}}</th>
                            <th>{{__('Price')}} </th>
                        </tr>
                        @if(!empty($orderProducts))
                        @foreach($orderProducts as $singleProducts)
                        @php
                        $orderProLangData =
                        getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleProducts->product_id));
                        if(!empty($orderProLangData->name)){
                        $productName = $orderProLangData->name;
                        }else{
                        $productName = $singleProducts->name;
                        }
                        $proPrice = 0;
                        if(!empty($singleProducts->product_attr_price) && $singleProducts->product_attr_price>0){
                        $proPrice = $singleProducts->product_attr_price;
                        }
                        else if(!empty($singleProducts->saleprice) && $singleProducts->saleprice>0){
                        $proPrice = $singleProducts->saleprice;
                        }
                        else{
                        $proPrice = $singleProducts->price;
                        }
                        $subPrice = $proPrice*$singleProducts->quantity;
                        @endphp
                        <tr>
                            <td valign="top" style="padding-left: 15px;">
                                <h5 style="margin-top: 15px;">
                                    {{$productName}}
                                </h5>
                                @if(!empty($singleProducts->product_attr_name))
                                <h4>
                                    {{__($singleProducts->product_attr_name)}}
                                </h4>
                                @endif
                            </td>
                            <td valign="top" style="padding-left: 15px;">
                                <h5 style="font-size: 14px; color:#444;margin-top: 10px;">
                                    <span>{{$singleProducts->quantity}}</span>
                                </h5>
                            </td>
                            <td valign="top" style="padding-left: 15px;">
                                <h5 style="font-size: 14px; color:#444;margin-top:15px">
                                    <b>{{showOrderPrice($subPrice,$orderData->currency_code,$orderData->exchange_rate)}}</b>
                                </h5>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        <tr class="pad-left-right-space ">
                            <td class="m-t-5" colspan="2" align="left">
                                <p style="font-size: 14px;">{{__('Subtotal')}} : </p>
                            </td>
                            <td class="m-t-5" colspan="2" align="right">
                                <b
                                    style>{{showOrderPrice($orderData->subtotal,$orderData->currency_code,$orderData->exchange_rate)}}</b>
                            </td>
                        </tr>
                        <tr class="pad-left-right-space">
                            <td colspan="2" align="left">
                                <p style="font-size: 14px;">{{__('Tax(%)')}} :</p>
                            </td>
                            <td colspan="2" align="right">
                                <b>{{showOrderPrice($orderData->tax,$orderData->currency_code,$orderData->exchange_rate)}}</b>
                            </td>
                        </tr>
                        <tr class="pad-left-right-space">
                            <td colspan="2" align="left">
                                <p style="font-size: 14px;">{{__('Discount')}} :</p>
                            </td>
                            <td colspan="2" align="right">
                                <b>{{showOrderPrice($orderData->coupon_discount,$orderData->currency_code,$orderData->exchange_rate)}}</b>
                            </td>
                        </tr>
                        <tr class="pad-left-right-space">
                            <td colspan="2" align="left">
                                <p style="font-size: 14px;">{{__('Shipping')}} :</p>
                            </td>
                            <td colspan="2" align="right">
                                <b>{{showOrderPrice($orderData->shipping,$orderData->currency_code,$orderData->exchange_rate)}}</b>
                            </td>
                        </tr>
                        <tr class="pad-left-right-space ">
                            <td class="m-b-5" colspan="2" align="left">
                                <p style="font-size: 14px;">{{__('Total')}} :</p>
                            </td>
                            <td class="m-b-5" colspan="2" align="right">
                                <b>{{showOrderPrice($orderData->total,$orderData->currency_code,$orderData->exchange_rate)}}</b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="main-bg-light text-center top-0" align="center" border="0" cellpadding="0" cellspacing="0"
        width="100%">
        <tr>
            <td style="padding: 30px;">
                <div>
                    <h4 class="title" style="margin:0;text-align: center;">{{__('Follow us')}}</h4>
                </div>
                <table border="0" cellpadding="0" cellspacing="0" class="footer-social-icon" align="center"
                    class="text-center" style="margin-top:20px;">
                    @php
                    $generalSettings = generalSettings();
                    @endphp
                    <tr>
                        @if(isset($generalSettings->fb_status) && $generalSettings->fb_status==1 &&
                        isset($generalSettings->fb_link) && !empty($generalSettings->fb_link) )
                        <td>
                            <a href="{{$generalSettings->fb_link}}"><img
                                    src="{{asset('frontend/assets/images/email-temp/facebook.png')}}" alt=""></a>
                        </td>
                        @endif
                        @if(isset($generalSettings->gp_status) && $generalSettings->gp_status==1 &&
                        isset($generalSettings->gp_link) && !empty($generalSettings->gp_link) )
                        <td>
                            <a href="{{$generalSettings->gp_link}}"><img
                                    src="{{asset('frontend/assets/images/email-temp/gplus.png')}}" alt=""></a>
                        </td>
                        @endif
                        @if(isset($generalSettings->tw_status) && $generalSettings->tw_status==1 &&
                        isset($generalSettings->tw_link) && !empty($generalSettings->tw_link) )
                        <td>
                            <a href="{{$generalSettings->tw_link}}"><img
                                    src="{{asset('frontend/assets/images/email-temp/twitter.png')}}" alt=""></a>
                        </td>
                        @endif
                        @if(isset($generalSettings->li_status) && $generalSettings->li_status==1 &&
                        isset($generalSettings->li_status) && !empty($generalSettings->li_status) )
                        <td>
                            <a href="{{$generalSettings->li_link}}"><img
                                    src="{{asset('frontend/assets/images/email-temp/linkedin.png')}}" alt=""></a>
                        </td>
                        @endif
                        @if(isset($generalSettings->in_status) && $generalSettings->in_status==1 &&
                        isset($generalSettings->in_status) && !empty($generalSettings->in_status) )
                        <td>
                            <a href="{{$generalSettings->in_link}}"><img
                                    src="{{asset('frontend/assets/images/email-temp/instagram.png')}}" alt=""></a>
                        </td>
                        @endif
                    </tr>
                </table>
                <div style="border-top: 1px solid #ddd; margin: 20px auto 0;"></div>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 20px auto 0;">
                    <tr>
                        <td>
                            <p style="font-size:13px; margin:0;">© {{date('Y')}}
                                {{ __('ImedicalShop. All Rights Reserved.') }}</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    @else
    <!-- PAYMENT MAIL -->

    <table align="center" border="0" cellpadding="0" cellspacing="0"
        style="padding: 0 30px;background-color: #fff; -webkit-box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);width: 100%;">
        <tbody>
            <tr>
                <td>
                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="text-align: left;"
                        width="100%">
                        <tr>
                            <td style="text-align: center;">
                                <img src="{{asset('frontend/assets/images/icon/logo.png')}}" width="250" alt=""
                                    style=";margin-bottom: 30px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p style="font-size: 14px;"><b>{{__('Hi')}} {{$orderData->first_name}}
                                        {{$orderData->last_name}},</b></p>
                                <p style="font-size: 14px;">
                                    {{__('Order Is Successfully Processsed And Your Order Is On The Way.')}}</p>
                                @if(!empty($orderData->transaction_id))
                                <p style="font-size: 14px;">{{__('Transaction ID')}} : {{$orderData->transaction_id}},
                                </p>
                                @endif
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0" border="0" align="left"
                        style="width: 100%;margin-top: 10px; margin-bottom: 10px;">
                        <tbody>
                            <tr>
                                <td
                                    style="background-color: #fafafa;border: 1px solid #ddd;padding: 15px;letter-spacing: 0.3px;width: 50%;">
                                    <h5
                                        style="font-size: 16px; font-weight: 600;color: #000; line-height: 16px; padding-bottom: 13px; border-bottom: 1px solid #e6e8eb; letter-spacing: -0.65px; margin-top:0; margin-bottom: 13px;">
                                        {{__('Shipping Address')}}</h5>
                                    <p
                                        style="text-align: left;font-weight: normal; font-size: 14px; color: #000000;line-height: 21px;    margin-top: 0;">
                                        {{$orderData->shipping_first_name.' '.$orderData->shipping_last_name}}<br>
                                        @if(!empty($orderData->shipping_company))
                                        {{$orderData->shipping_company}}<br>
                                        @endif
                                        {{$orderData->shipping_street_address.', '.$orderData->shipping_apartment}}<br>
                                        {{$orderData->shipping_town.', '.$orderData->shipping_state}}<br>
                                        {{$orderData->shipping_country.', '.$orderData->shipping_postcode}}<br>
                                        {{__('Contact No')}}. {{$orderData->shipping_phone}}
                                    </p>
                                </td>
                                <td><img src="{{asset('frontend/assets/images/email-temp/space.jpg')}}" alt=" "
                                        height="25" width="30">
                                </td>
                                <td
                                    style="background-color: #fafafa;border: 1px solid #ddd;padding: 15px;letter-spacing: 0.3px;width: 50%;">
                                    <h5
                                        style="font-size: 16px;font-weight: 600;color: #000; line-height: 16px; padding-bottom: 13px; border-bottom: 1px solid #e6e8eb; letter-spacing: -0.65px; margin-top:0; margin-bottom: 13px;">
                                        {{__('Billing Address')}}:</h5>
                                    <p
                                        style="text-align: left;font-weight: normal; font-size: 14px; color: #000000;line-height: 21px;    margin-top: 0;">
                                        {{$orderData->first_name.' '.$orderData->last_name}}<br>
                                        @if(!empty($orderData->company))
                                        {{$orderData->company}}<br>
                                        @endif
                                        {{$orderData->street_address.', '.$orderData->apartment}}<br>
                                        {{$orderData->town.', '.$orderData->state}}<br>
                                        {{$orderData->country.', '.$orderData->postcode}}<br>
                                        {{__('Contact No')}}. {{$orderData->phone}}
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="order-detail" border="0" cellpadding="0" cellspacing="0" align="left"
                        style="width: 100%; margin-bottom: 50px;">
                        <tr align="left">
                            <th>{{__('Product Name')}}</th>
                            <th>{{__('Quantity')}}</th>
                            <th>{{__('Price')}} </th>
                        </tr>
                        @if(!empty($orderProducts))
                        @foreach($orderProducts as $singleProducts)
                        @php
                        $orderProLangData =
                        getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleProducts->product_id));
                        if(!empty($orderProLangData->name)){
                        $productName = $orderProLangData->name;
                        }else{
                        $productName = $singleProducts->name;
                        }
                        $proPrice = 0;
                        if(!empty($singleProducts->product_attr_price) && $singleProducts->product_attr_price>0){
                        $proPrice = $singleProducts->product_attr_price;
                        }
                        else if(!empty($singleProducts->saleprice) && $singleProducts->saleprice>0){
                        $proPrice = $singleProducts->saleprice;
                        }
                        else{
                        $proPrice = $singleProducts->price;
                        }
                        $subPrice = $proPrice*$singleProducts->quantity;
                        @endphp
                        <tr>
                            <td valign="top" style="padding-left: 15px;">
                                <h5 style="margin-top: 15px;">
                                    {{$productName}}
                                </h5>
                                @if(!empty($singleProducts->product_attr_name))
                                <h4>
                                    {{__($singleProducts->product_attr_name)}}
                                </h4>
                                @endif
                            </td>
                            <td valign="top" style="padding-left: 15px;">
                                <h5 style="font-size: 14px; color:#444;margin-top: 10px;">
                                    <span>{{$singleProducts->quantity}}</span>
                                </h5>
                            </td>
                            <td valign="top" style="padding-left: 15px;">
                                <h5 style="font-size: 14px; color:#444;margin-top:15px">
                                    <b>{{showOrderPrice($subPrice,$orderData->currency_code,$orderData->exchange_rate)}}</b>
                                </h5>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        <tr class="pad-left-right-space ">
                            <td class="m-t-5" colspan="2" align="left">
                                <p style="font-size: 14px;">{{__('Subtotal')}} : </p>
                            </td>
                            <td class="m-t-5" colspan="2" align="right">
                                <b
                                    style>{{showOrderPrice($orderData->subtotal,$orderData->currency_code,$orderData->exchange_rate)}}</b>
                            </td>
                        </tr>
                        <tr class="pad-left-right-space">
                            <td colspan="2" align="left">
                                <p style="font-size: 14px;">{{__('Discount')}} :</p>
                            </td>
                            <td colspan="2" align="right">
                                <b>{{showOrderPrice($orderData->coupon_discount,$orderData->currency_code,$orderData->exchange_rate)}}</b>
                            </td>
                        </tr>
                        <tr class="pad-left-right-space">
                            <td colspan="2" align="left">
                                <p style="font-size: 14px;">{{__('Shipping')}} :</p>
                            </td>
                            <td colspan="2" align="right">
                                <b>{{showOrderPrice($orderData->shipping,$orderData->currency_code,$orderData->exchange_rate)}}</b>
                            </td>
                        </tr>
                        <tr class="pad-left-right-space">
                            <td colspan="2" align="left">
                                <p style="font-size: 14px;">{{__('Tax(%)')}} :</p>
                            </td>
                            <td colspan="2" align="right">
                                <b>{{showOrderPrice($orderData->tax,$orderData->currency_code,$orderData->exchange_rate)}}</b>
                            </td>
                        </tr>
                        <tr class="pad-left-right-space ">
                            <td class="m-b-5" colspan="2" align="left">
                                <p style="font-size: 14px;">{{__('Total')}} :</p>
                            </td>
                            <td class="m-b-5" colspan="2" align="right">
                                <b>{{showOrderPrice($orderData->total,$orderData->currency_code,$orderData->exchange_rate)}}</b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="main-bg-light text-center top-0" align="center" border="0" cellpadding="0" cellspacing="0"
        width="100%">
        <tr>
            <td style="padding: 30px;">
                <div>
                    <h4 class="title" style="margin:0;text-align: center;">{{__('Follow us')}}</h4>
                </div>
                <table border="0" cellpadding="0" cellspacing="0" class="footer-social-icon" align="center"
                    class="text-center" style="margin-top:20px;">
                    @php
                    $generalSettings = generalSettings();
                    @endphp
                    <tr>
                        @if(isset($generalSettings->fb_status) && $generalSettings->fb_status==1 &&
                        isset($generalSettings->fb_link) && !empty($generalSettings->fb_link) )
                        <td>
                            <a href="{{$generalSettings->fb_link}}"><img
                                    src="{{asset('frontend/assets/images/email-temp/facebook.png')}}" alt=""></a>
                        </td>
                        @endif
                        @if(isset($generalSettings->gp_status) && $generalSettings->gp_status==1 &&
                        isset($generalSettings->gp_link) && !empty($generalSettings->gp_link) )
                        <td>
                            <a href="{{$generalSettings->gp_link}}"><img
                                    src="{{asset('frontend/assets/images/email-temp/gplus.png')}}" alt=""></a>
                        </td>
                        @endif
                        @if(isset($generalSettings->tw_status) && $generalSettings->tw_status==1 &&
                        isset($generalSettings->tw_link) && !empty($generalSettings->tw_link) )
                        <td>
                            <a href="{{$generalSettings->tw_link}}"><img
                                    src="{{asset('frontend/assets/images/email-temp/twitter.png')}}" alt=""></a>
                        </td>
                        @endif
                        @if(isset($generalSettings->li_status) && $generalSettings->li_status==1 &&
                        isset($generalSettings->li_status) && !empty($generalSettings->li_status) )
                        <td>
                            <a href="{{$generalSettings->li_link}}"><img
                                    src="{{asset('frontend/assets/images/email-temp/linkedin.png')}}" alt=""></a>
                        </td>
                        @endif
                        @if(isset($generalSettings->in_status) && $generalSettings->in_status==1 &&
                        isset($generalSettings->in_status) && !empty($generalSettings->in_status) )
                        <td>
                            <a href="{{$generalSettings->in_link}}"><img
                                    src="{{asset('frontend/assets/images/email-temp/instagram.png')}}" alt=""></a>
                        </td>
                        @endif
                    </tr>
                </table>
                <div style="border-top: 1px solid #ddd; margin: 20px auto 0;"></div>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 20px auto 0;">
                    <tr>
                        <td>
                            <p style="font-size:13px; margin:0;">© {{date('Y')}}
                                {{ __('ImedicalShop. All Rights Reserved.') }}</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    @endif
</body>