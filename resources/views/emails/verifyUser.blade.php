<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activate Your Account</title>
    <style type="text/css">
        body {
            text-align: center;
            margin: 0 auto;
            width: 650px;
            font-family: 'Arial', sans-serif;
            background-color: #e2e2e2;
            display: block;
        }
        ul {
            margin: 0;
            padding: 0;
        }
        li {
            display: inline-block;
            text-decoration: unset;
        }
        a {
            text-decoration: none;
        }
        p {
            margin: 15px 0;
        }
        h5 {
            color: #444;
            text-align: left;
            font-weight: 400;
        }

        .text-center {
            text-align: center
        }

        .main-bg-light {
            background-color: #fafafa;
        }

        .title {
            color: #444444;
            font-size: 22px;
            font-weight: bold;
            margin-top: 10px;
            margin-bottom: 10px;
            padding-bottom: 0;
            text-transform: uppercase;
            display: inline-block;
            line-height: 1;
        }

        table {
            margin-top: 30px
        }

        table.top-0 {
            margin-top: 0;
        }

        table.order-detail {
            border: 1px solid #ddd;
            border-collapse: collapse;
        }

        table.order-detail tr:nth-child(even) {
            border-top: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
        }

        table.order-detail tr:nth-child(odd) {
            border-bottom: 1px solid #ddd;
        }

        .pad-left-right-space {
            border: unset !important;
        }

        .pad-left-right-space td {
            padding: 5px 15px;
        }

        .pad-left-right-space td p {
            margin: 0;
        }

        .pad-left-right-space td b {
            font-size: 15px;
            font-family: 'Roboto', sans-serif;
        }
        .order-detail th {
            font-size: 16px;
            padding: 15px;
            text-align: center;
            background: #fafafa;
        }
        .footer-social-icon tr td img {
            margin-left: 5px;
            margin-right: 5px;
        }
    </style>
</head>
<body style="margin: 20px auto;">
    <table align="center" border="0" cellpadding="0" cellspacing="0"
        style="padding: 0 30px;background-color: #fff; -webkit-box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);width: 100%;">
        <tbody>
            <tr>
                <td>
                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="text-align: left;"
                        width="100%">
                        <tr>
                            <td style="text-align: center;">
                                <img src="{{url('/') }}/{{ $wl_logo }}" width="250" alt="" style=";margin-bottom: 30px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p style="font-size: 14px;"><b>{{__('Hi')}} {!!$name!!},</b></p>
                                <p style="font-size: 14px;"> {{__('thank you for creating an account with Imedical! To activate your Imedical account, click or tap the button below.')}} <a href="{{url('user/verify', $verifyUsertoken)}}">{{__('Activate Account')}}</a>
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0" border="0" align="left"
                        style="width: 100%;margin-top: 10px; margin-bottom: 10px;">
                        <tbody>
                            <tr>
                                <td style="background-color: #fafafa;border: 1px solid #ddd;padding: 15px;letter-spacing: 0.3px;width: 50%;">
                                    <p style="text-align: left;font-weight: normal; font-size: 14px; color: #000000;line-height: 21px;    margin: 0;">{{__('Once activated, you can use the e-mail address and password you specified when you created your account to log in to Imedical.')}}</p>
                                </td>                              
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="main-bg-light text-center top-0" align="center" border="0" cellpadding="0" cellspacing="0"
        width="100%">
        <tr>
            <td style="padding: 30px;">
                <div>
                    <h4 class="title" style="margin:0;text-align: center;">{{__('Follow us')}}</h4>
                </div>
                <table border="0" cellpadding="0" cellspacing="0" class="footer-social-icon" align="center" class="text-center" style="margin-top:20px;">
                    @php
			        $generalSettings = generalSettings();
			        @endphp
                    <tr>
                    	@if(isset($generalSettings->fb_status) && $generalSettings->fb_status==1 &&
                        isset($generalSettings->fb_link) && !empty($generalSettings->fb_link) )
                        <td>
                            <a href="{{$generalSettings->fb_link}}"><img src="{{asset('frontend/assets/images/email-temp/facebook.png')}}" alt=""></a>
                        </td>
                        @endif
                        @if(isset($generalSettings->gp_status) && $generalSettings->gp_status==1 &&
                        isset($generalSettings->gp_link) && !empty($generalSettings->gp_link) )
                        <td>
                            <a href="{{$generalSettings->gp_link}}"><img src="{{asset('frontend/assets/images/email-temp/gplus.png')}}" alt=""></a>
                        </td>
                        @endif
                        @if(isset($generalSettings->tw_status) && $generalSettings->tw_status==1 &&
                        isset($generalSettings->tw_link) && !empty($generalSettings->tw_link) )
                        <td>
                            <a href="{{$generalSettings->tw_link}}"><img src="{{asset('frontend/assets/images/email-temp/twitter.png')}}" alt=""></a>
                        </td>
                        @endif
                        @if(isset($generalSettings->li_status) && $generalSettings->li_status==1 &&
                        isset($generalSettings->li_status) && !empty($generalSettings->li_status) )
                        <td>
                            <a href="{{$generalSettings->li_link}}"><img src="{{asset('frontend/assets/images/email-temp/linkedin.png')}}" alt=""></a>
                        </td>
                        @endif
                        @if(isset($generalSettings->in_status) && $generalSettings->in_status==1 &&
                        isset($generalSettings->in_status) && !empty($generalSettings->in_status) )
                        <td>
                            <a href="{{$generalSettings->in_link}}"><img src="{{asset('frontend/assets/images/email-temp/instagram.png')}}" alt=""></a>
                        </td>
                        @endif
                    </tr>
                </table>
                <div style="border-top: 1px solid #ddd; margin: 20px auto 0;"></div>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 20px auto 0;">                    
                    <tr>
                        <td>
                            <p style="font-size:13px; margin:0;">© {{date('Y')}} {{ __('ImedicalShop. All Rights Reserved.') }}</p>
                        </td>
                    </tr>                   
                </table>
            </td>
        </tr>
    </table>
</body>
</html>