@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>{!!$page_title!!}</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active">{!!$page_title!!}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- About Start -->
    <div id="faq">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-left">
                    {!!$pageData->content!!}
                </div>                
            </div>           
            <div class="row">
                <div class="col-sm-12">
                    <div class="bs-example">
                        <div class="accordion" id="accordionExample">
                            @if(!empty($faqs))
                                @php
                                    $x=0;
                                @endphp
                                @foreach($faqs as $data)
                                    @php
                                        $x++;
                                    @endphp
                                    <div class="card">
                                        <div class="card-header" id="headingTwo{{$data->id}}">
                                            <h2 class="mb-0">
                                                <button type="button" class="btn btn-success btn-block @if($x==1) collapsed @endif " data-toggle="collapse" data-target="#collapseTwo{{$data->id}}"><i class="fa fa-plus"></i> {!!$data->name!!}</button>
                                            </h2>
                                        </div>
                                        <div id="collapseTwo{{$data->id}}" class="collapse  @if($x==1) show @endif" aria-labelledby="headingTwo{{$data->id}}" data-parent="#accordionExample">
                                            <div class="card-body">
                                                {!!$data->content!!}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach                
                            @endif 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection