@if($subcategories)
	<ul class="mega-menu full-mega-menu">
    	@foreach ($subcategories as $singleHeaderSubCat)
            @if($singleHeaderSubCat->display_status==1)
                @php
                    $headerSubCatName = (isset($allLangCatsGlobal[$singleHeaderCat->id]) && !empty($allLangCatsGlobal[$singleHeaderCat->id]))?$allLangCatsGlobal[$singleHeaderCat->id]:$singleHeaderSubCat->name;
                @endphp
                <li>
                    <a href="{{url('shop/'.$parentSlug.'/'.$singleHeaderSubCat->slug)}}">{{__($headerSubCatName)}}</a>
                    @if(count($singleHeaderSubCat->subcategory))                    	
                        @include('headerSideCategory',['subcategories' => $singleHeaderSubCat->subcategory,'parent'=>$singleHeaderSubCat->name,'parentSlug'=>$parentSlug.'/'.$singleHeaderSubCat->slug,'allLangCatsGlobal'=>$allLangCatsGlobal])
                    @endif
                </li>
            @endif
        @endforeach
    </ul>                
@endif