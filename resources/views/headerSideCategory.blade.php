@if($subcategories)
<ul class="mega-menu full-mega-menu">

    @if(count($singleHeaderCat->subcategory))
    @foreach($singleHeaderCat->subcategory as
    $singleHeaderSubCat)
    @if($singleHeaderSubCat->display_status==1)
    @php
    $headerSubCatName =
    (isset($allLangCatsGlobal[$singleHeaderSubCat->id]) &&
    !empty($allLangCatsGlobal[$singleHeaderSubCat->id]))?$allLangCatsGlobal[$singleHeaderSubCat->id]:$singleHeaderSubCat->name;
    @endphp


    <li class="child-equipment @if(count($singleHeaderSubCat->subcategory)) show-child-arraw @endif"
        rel="{{$singleHeaderSubCat->image}}">
        <a
            href="{{url('shop/'.$singleHeaderCat->slug.'/'.$singleHeaderSubCat->slug)}}"><b>{{__($headerSubCatName)}}</b></a>
        @if(count($singleHeaderSubCat->subcategory))
        <ul class="third-level-cat">
            @foreach($singleHeaderSubCat->subcategory
            as $singleHeaderSubChildCat)
            @if($singleHeaderSubChildCat->display_status==1)
            @php
            $headerSubCatChildName =
            (isset($allLangCatsGlobal[$singleHeaderSubChildCat->id])
            &&
            !empty($allLangCatsGlobal[$singleHeaderSubChildCat->id]))?$allLangCatsGlobal[$singleHeaderSubChildCat->id]:$singleHeaderSubChildCat->name;
            @endphp
            <li class="child-equipment" rel="{{$singleHeaderSubChildCat->image}}">
                <a
                    href="{{url('shop/'.$singleHeaderCat->slug.'/'.$singleHeaderSubCat->slug.'/'.$singleHeaderSubChildCat->slug)}}">{{__($headerSubCatChildName)}}</a>
            </li>
            @endif
            @endforeach
        </ul>
        @endif
    </li>

    @endif
    @endforeach
    @endif


</ul>
@endif