@extends('layouts.app')

@section('content')
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>{!!$page_title!!}</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active">{!!$page_title!!}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <section>
        <div class="container"> 
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            @if (Session::has('error'))
                {!! errorMesaage(Session::get('error')) !!}   
            @endif
            <form method="POST" action="{{ route('import-data-upload') }}" class="row text-center" enctype="multipart/form-data">
                @csrf
                <div  class="col-sm-6">
                    <div class="form-group">
                       <select name="importType" id="importType" class="form-control">
                            <option value="1">Products</option>
                            <option value="2">Variant</option>
                            <option value="3">Category</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">                    
                    <div class="form-group text-left">
                        <div class="custom-file-browse choose">
                            <input type="file" class="custom-file-input" name="datafile" id="inputGroupFile01" accept=".csv">
                            <label class="custom-file-label" for="inputGroupFile01"></label>
                        </div>
                        <div class="path1" id="mess_inputGroupFile01"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <button class="btn btn-primary" type="submit">Upload File</button>
                </div>
            </form>
        </div>
    </section>
@endsection