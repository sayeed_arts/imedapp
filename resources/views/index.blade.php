@extends('layouts.app')

@section('content')
    <!-- Home slider -->
    <section class="p-0">
        @if(!empty($sliderData))        
            <div class="slide-1 home-slider">
                @foreach($sliderData as $singleSlide)
                    @php
                        $sliderLangData = getLanguageReconds('sliders_translations',array('name','content','button_text'),array('slider_id'=>$singleSlide->id));

                        if(!empty($sliderLangData->name)){
                            $sliderName = $sliderLangData->name;
                        }else{
                            $sliderName = $singleSlide->name;
                        }
                        if(!empty($sliderLangData->content)){
                            $sliderContent = $sliderLangData->content;
                        }else{
                            $sliderContent = $singleSlide->content;
                        }
                        if(!empty($sliderLangData->button_text)){
                            $sliderButtonText = $sliderLangData->button_text;
                        }else{
                            $sliderButtonText = $singleSlide->button_text;
                        }
                    @endphp
                    <div>
                        <div class="home">
                            @if(!empty($sliderName) || !empty($sliderContent) || (!empty($sliderButtonText) && !empty($singleSlide->button_link)))
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="slider-contain">
                                                <div class="caption">
                                                    @if(!empty($sliderName))
                                                    <h1>{!!nl2br($sliderName)!!}</h1>
                                                    @endif
                                                    @if(!empty($sliderContent))
                                                    <h4>{!!nl2br($sliderContent)!!}</h4>
                                                    @endif
                                                    @if(!empty($sliderButtonText) && !empty($singleSlide->button_link))
                                                        <a href="{{$singleSlide->button_link}}" target="_blank" class="btn btn-solid">
                                                            @if(!empty($sliderButtonText))
                                                                {{$sliderButtonText}}
                                                            @endif
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-5 text-center">
                                            <img src="{{ asset($singleSlide->image) }}" alt="" class="blur-up lazyload">
                                        </div>


                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </section>
    <!-- Home slider end -->

<div class="covid">
    <h2>Covid 19 Prevention Tips</h2>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="covid-card">
                    <div class="icon">
                        <img src="{{ asset('frontend/assets/images/covid-1.png') }}" alt="" srcset="">
                    </div>
                    <h4>Clean & Disinfect</h4>
                </div>
            </div>

            <div class="col-md-4">
                <div class="covid-card">
                    <div class="icon">
                        <img src="{{ asset('frontend/assets/images/covid-2.png') }}" alt="" srcset="">
                    </div>
                    <h4>Use Face Mask</h4>
                </div>
            </div>

            <div class="col-md-4">
                <div class="covid-card">
                    <div class="icon">
                        <img src="{{ asset('frontend/assets/images/covid-3.png') }}" alt="" srcset="">
                    </div>
                    <h4>Maintain Distance</h4>
                </div>
            </div>
        </div>
    </div>
</div>




    <!-- Paragraph-->
    @if(!empty($featuredProducts) && count($featuredProducts)>0)
        
        <section class="section-b-space p-t-0 pb-0">
            <div class="container">

            <div class="title1 title-new">
                <h2 class="title-inner1">{{__('New Arrivals')}}</h2>
            </div>

            
                <div class="row">
                    <div class="col">
                        <div class="product-4 product-m no-arrow">
                            @foreach($featuredProducts as $feaPro)
                                @php
                                    $fProLangData = getLanguageReconds('products_translations',array('name'),array('product_id'=>$feaPro->id));
                                    if(!empty($fProLangData->name)){
                                        $productName = $fProLangData->name;
                                    }else{
                                        $productName = $feaPro->name;
                                    }
                                @endphp
                                <div class="product-box">
                                    <div class="img-wrapper">
                                        <div class="front">
                                            <a href="{{url('product/'.$feaPro->slug)}}"><img src="{{asset($feaPro->image)}}" class="img-fluid blur-up lazyload " alt="{{$feaPro->name}}"></a>
                                        </div>
                                    </div>
                                    <div class="product-detail">
                                        <a class="prd-title" href="{{url('product/'.$feaPro->slug)}}">
                                            <h6>{{$productName}}</h6>
                                        </a>

                                        <div class="price-detail">
                                            <div class="price">
                                                <!-- <p>Electrosurgical Units</p> -->
                                                @if(!empty($feaPro->special_price) && $feaPro->special_price>0)
                                                    <h4>{{showPrice($feaPro->special_price)}} 
                                                        <span><del>{{showPrice($feaPro->price)}}</del></span>
                                                    </h4>
                                                @elseif($feaPro->price>0 && $feaPro->stock_availability==1)
                                                    <h4>{{showPrice($feaPro->price)}}</h4>
                                                @endif
                                            </div>

                                            <div class="buttons">
                                                @if($feaPro->stock_availability==0 || $feaPro->price<1)
                                                    <button type="button" class="btn get-quote" data-name="{{$feaPro->name}}" data-url="{{url('product/'.$feaPro->slug)}}" data-page ='listing' data-id="{{base64_encode($feaPro->id)}}">{{__('Get Quote')}}</button>
                                                @elseif(attributeExists($feaPro->id))
                                                    <a href="{{url('product/'.$feaPro->slug)}}">
                                                        <button type="button" class="btn view-details" data-id="{{base64_encode($feaPro->id)}}"> <img src="{{ asset('frontend/assets/images/info.png') }}" alt="" srcset=""></button>
                                                    </a>
                                                @else
                                                    <button type="button" class="btn add-to-cart" data-id="{{base64_encode($feaPro->id)}}"> <img src="{{ asset('frontend/assets/images/cart.jpg') }}" alt="" srcset=""></button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if(!empty($equipmentTypeSubCats) && count($equipmentTypeSubCats)>0)
        <!-- <div class="products-type">
            <div class="container">
                <div class="title1">
                    <h2 class="title-inner2">{{__('Find Products By Equipment Type')}}</h2>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8">
                        <ul class="equipment-ul">
                            @foreach($equipmentTypeSubCats as $singleEquipSubCats)
                                @php
                                    $equTypeLangData = getLanguageReconds('categories_translations',array('name'),array('category_id'=>$singleEquipSubCats->id));
                                    if(!empty($equTypeLangData->name)){
                                        $equTypeName = $equTypeLangData->name;
                                    }else{
                                        $equTypeName = $singleEquipSubCats->name;
                                    }
                                @endphp
                                <li class="child-equipment" rel="{{$singleEquipSubCats->image}}">
                                    <a href="{{url('shop/'.$equipmentTypeCat->slug.'/'.$singleEquipSubCats->slug)}}">{{$equTypeName}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <div class="equipment-imgcntr"></div>
                    </div>
                </div>
            </div>
        </div> -->
    @endif
    @if(isset($advertisementData->image) && $advertisementData->image!='')
        <div class="container cat-banner">
            @if($advertisementData->content!='')
            <a href="{{$advertisementData->content}}" target="_blank">
                <img src="{{ asset($advertisementData->image) }}" alt="{{$advertisementData->name}}" srcset="">
            </a>
            @else
            <img src="{{ asset($advertisementData->image) }}" alt="{{$advertisementData->name}}" srcset="">
            @endif
        </div>
    @endif
    @if(!empty($healthCareSubCats) && count($healthCareSubCats)>0)
        <div class="healthcare">
            <div class="container">
                <div class="title2">
                    <h2 class="title-inner2">{{__('Shop by Categories')}}</h2>
                </div>
                <ul class="healthcare-equipment">
                    @foreach($healthCareSubCats as $singleHealthCat)
                        @php
                            $healthLangData = getLanguageReconds('categories_translations',array('name'),array('category_id'=>$singleHealthCat->id));
                            if(!empty($healthLangData->name)){
                                $healthCareName = $healthLangData->name;
                            }else{
                                $healthCareName = $singleHealthCat->name;
                            }
                        @endphp
                    <li class="health-child-equipment">
                        <span> 
                            <a href="{{url('shop/'.$healthCareCats->slug.'/'.$singleHealthCat->slug)}}">
                                <img class="62 lazy" src="{{ asset($singleHealthCat->image) }}"  alt="{{$singleHealthCat->name}}">
                            </a>
                        </span>
                        <a href="{{url('shop/'.$healthCareCats->slug.'/'.$singleHealthCat->slug)}}">{{$healthCareName}}</a>
                    </li>
                    @endforeach
                </ul>
                <a href="javascript:void(0);" class="btn btn-solid view-all-cat">View All</a>
            </div>
        </div>
    @endif

    <!-- AUCTION LIST HERE -->
        @if(!empty($allAuctions) && count($allAuctions)>0)                    
        <section class="flash-auction">
            <div class="container">
            <h2><span>FLASH AUCTION</span></h2>          
                <div class="auctions-list">  
                @foreach($allAuctions as $auctiondata) 
                    <div class="auction-box">
                        <div class="thumb">
                            <img src="{{asset($auctiondata->image)}}" alt="{!! $auctiondata->name !!}" srcset="" />
                        </div>
                        <a href="{{url('product/'.$auctiondata->slug)}}" class="title">
                            <h3>{!! $auctiondata->name !!}</h3>
                        </a>
                        <div class="timer">
                            <span>Time left</span>
                            <p class="countdowntimer" id="auc_<?php echo $auctiondata->id; ?>"></p>
                        </div>
                        <div class="buttons">
                            <a href="{{url('auction/'.base64_encode($auctiondata->id))}}" class="more">Read More</a>
                            @guest
                            <a href="{{url('login')}}" class="bid">Login to Bid</a>
                            @else
                            <?php
                            $current_user = Auth::user();
                            if($current_user->is_admin == '0'){
                            ?>
                            <a href="javascript:void(0);" onclick="bidNow(<?php echo $auctiondata->id; ?>);" class="bid" data-toggle="modal" data-target="#biddingModal">Bid Now</a>
                            <?php } ?>
                            @endguest
                        </div>
                    </div>
                @endforeach                
                </div>
            </div>
        </section>
        <!-- Modal -->
        <div class="modal fade bidding-modal" id="biddingModal" tabindex="-1" role="dialog" aria-labelledby="biddingModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Place Your Bid <span>Note : Your bid should be greater than last bid</span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div role="alert" id="bidresponsemessage" style="display:none;"></div>
                        <div id="bidformcontent" style="display:none;">
                            <form name="bidform">
                                <input type="hidden" name="bid_auction_id" id="bid_auction_id" value="" />
                                <ul>
                                    <li><input type="text" name="bid_amount" id="bid_amount" class="form-control" value="" /></li>
                                    <li class="text-right"><button id="btn_submit_bid" type="button" class="btn btn-primary" onclick="submitBid();">Place Your bid</button></li>
                                </ul>
                            </form>
                            <div class="last-bid">
                                LAST BID VALUE <span id="last_bid_amount"></span>
                            </div>
                        </div>
                        <div id="bidformloading">
                            <p style="text-align:center;">Loading...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif                    
    <!-- AUCTION LIST END HERE -->


    <!-- BRANDS SECTION HERE -->

    <section class="brands-section">
        <div class="container">
            <div class="title2">
                <h2 class="title-inner2">Shop by Brands</h2>
            </div>

            <div class="brands-list">
                <div class="row align-items-center">
                    @foreach($featuredManufacturers as $singleManufacturer)
                        <div class="col-lg-2 col-md-4 col-6 col-sm-4">
                            <div class="single-brand-logo mb-30">
                                <a href="{{url('shop/brand-'.$singleManufacturer->slug)}}"><img src="{{ asset($singleManufacturer->image) }}" alt="{{$singleManufacturer->name}}"></a>
                            </div>
                        </div>        
                    @endforeach            
                </div>
            </div>

        </div>

    </section>

    <!-- BRANDS SECTION END HERE -->


    <!-- TESTIMONIAL SECTION HERE -->

    <div class="testimonal-section">
        <div class="container">
            <div class="title2">
                <h2 class="title-inner2">Customer Feedback</h2>
                <h3 class="subtitle-inner2">People love our products and 70% our customers are returned customers. We believe that only way to make a long-term business is helping people.</h3>
            </div>

            <div class="testimonal-list">
                <ul>
                    @foreach($latestTestimonials as $singleTestimonial)
                    <li>
                        <div class="text-block">
                        {{$singleTestimonial->content}}
                        </div>
                        <div class="client">
                            <div class="thumb">
                            <img src="{{ asset($singleTestimonial->image) }}" alt="">
                            </div>
                            <div class="name">
                                <h3>{{$singleTestimonial->name}}</h3>
                                <span>{{$singleTestimonial->location}}</span>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    
    <!-- TESTIMONIAL SECTION END HERE -->


    
    <!-- NEWS SECTION HERE -->

    <section class="news-main">
        <div class="container">
            <div class="title2">
                <h2 class="title-inner2">Latest News</h2>
                <h3 class="subtitle-inner2">Ut Enim Ad Minim Veniam, Quis Nostrud Exercitation Ullamco Laboris Nisi Ut Aliquip Ex Ea Commodo Consequat.</h3>
            </div>

            <div class="news-list">
                <ul>
                    @foreach($latestNews as $singleNews)
                    <li>
                        <div class="thumb">
                         <img src="{{ asset($singleNews->image) }}" alt="{{$singleNews->name}}">
                         <div class="date">
                            <h4><?php echo date("j", strtotime($singleNews->created_at))?></h4>
                            <span><?php echo date("F Y", strtotime($singleNews->created_at))?></span>
                         </div>
                        </div>
                        <div class="content">
                            <h5>{{$singleNews->name}}</h5>
                            <p>{!! \Illuminate\Support\Str::limit(strip_tags($singleNews->content), 200, $end='...') !!}</p>
                            <a href="{{url('news/'.$singleNews->slug)}}" class="more">Read More <i class="fa fa-angle-right"></i></a>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>

    <!-- NEWS SECTION END HERE -->


@endsection


