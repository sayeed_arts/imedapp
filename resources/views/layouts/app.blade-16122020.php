<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ (isset($meta_title))?$meta_title:config('app.name', __('IMedical Shop')) }}</title>
    <meta name="description" content="{{ (isset($meta_desc))?$meta_desc:config('app.meta_desc', 'IMedical Shop') }}">
    <meta name="keywords" content="{{ (isset($meta_key))?$meta_key:config('app.meta_key', 'IMedical Shop') }}">
    @if(isset($isProductPage) && $isProductPage=='Yes')
        <meta property="og:url" content="{{url('product/'.$currPage)}}" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="{{$page_title}}" />
        <meta property="og:description" content="{{$pageData->short_description}}" />
        <meta property="og:image" content="{{asset($pageData->image)}}" />
    @endif
    <link rel="icon" href="{{ asset('frontend/assets/images/favicon/1.png') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('frontend/assets/images/favicon/1.png') }}" type="image/x-icon">
    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/slick-theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/animate.css') }}">
    <link rel="stylesheet" type="text/css"
        href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/price-range.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/color1.css') }}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/media.css') }}" media="screen">
    <script type="text/javascript">
    var base_url = "{{ url('/')}}";
    </script>
</head>
<body>
    <!-- loader start -->
    <div class="loader_skeleton">
        <img src="{{ asset('frontend/assets/images/ajax-loader.gif') }}" alt="" srcset="">
    </div>
    <!-- loader end -->
    <!-- header start -->
    <header>
        @php
        $generalSettings = generalSettings();
        @endphp
        <div class="mobile-fix-option"></div>
        <div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="header-contact">
                            <ul>
                                <li>{{ __('Welcome to Our store IMedical Shop') }}</li>
                                <li><i class="fa fa-phone" aria-hidden="true"></i>{{ __('Call Us') }}: 
                                    <a href="tel:{{(isset($generalSettings->contact_phone))?$generalSettings->contact_phone:''}}">{{(isset($generalSettings->contact_phone))?$generalSettings->contact_phone:''}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 text-right">
                        <div class="header-contact-right">
                            <ul>
                                <li><a href="{{route('about-us')}}">{{ __('About us') }}</a></li>
                                @guest
                                    <li>
                                        <a href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('register') }}">{{ __('Sign Up') }}</a>
                                    </li>
                                @else
                                    <li><a href="{{ route('user') }}">{{ __('My Account') }}</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                @endguest
                                <li >
                                    <select class="form-control headerchangeLang" >
                                        @php
                                            $allLanguages = getLanguages();
                                        @endphp
                                        @if(!empty($allLanguages))
                                            @foreach($allLanguages as $singleLanguage)
                                                <option value="{{$singleLanguage->code}}" {{ session()->get('locale') == $singleLanguage->code ? 'selected' : '' }}>{{$singleLanguage->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </li>
                                <li>
                                    <select class="form-control headerchangeCurrency">
                                        @php
                                            $allCurrencies = getCurrencies();
                                        @endphp
                                        @if(!empty($allCurrencies))
                                            @foreach($allCurrencies as $singleCurrency)
                                                <option value="{{$singleCurrency['code']}}" {{ session()->get('appcurrency') == $singleCurrency['code'] ? 'selected' : '' }}>{{$singleCurrency['code']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </li>
                               <!--  <li class="google-translate">
                                    <p id="google_translate_element"></p>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-menu">
                        <div class="menu-left">
                            <div class="navbar">
                                <a href="javascript:void(0)" onclick="openNav()">
                                    <div class="bar-style"><i class="fa fa-bars sidebar-bar" aria-hidden="true"></i>
                                    </div>
                                </a>
                                <div id="mySidenav" class="sidenav">
                                    <a href="javascript:void(0)" class="sidebar-overlay" onclick="closeNav()"></a>
                                    <nav>
                                        <div onclick="closeNav()">
                                            <div class="sidebar-back text-left"><i class="fa fa-angle-left pr-2" aria-hidden="true"></i> {{ __('Back') }}</div>
                                        </div>
                                        <!-- <p id="google_translate_element"></p> -->
                                        <ul id="sub-menu" class="sm pixelstrap sm-vertical">
                                            @php
                                                createCartCookie();
                                                $allCatsGlobal          = getCategories();
                                                $allLangCatsGlobal      = getLangCategories();
                                                $allHeaderService       = getHeaderServices();
                                                $allHeaderLangService   = getHeaderLangServices();
                                            @endphp
                                            @if(!empty($allCatsGlobal))
                                                @foreach($allCatsGlobal as $singleHeaderCat)
                                                    @php
                                                        $headerCatName = (isset($allLangCatsGlobal[$singleHeaderCat->id]) && !empty($allLangCatsGlobal[$singleHeaderCat->id]))?$allLangCatsGlobal[$singleHeaderCat->id]:$singleHeaderCat->name;
                                                    @endphp
                                                    <li class="mega">
                                                        <a href="{{url('shop/'.$singleHeaderCat->slug)}}">{{__($headerCatName)}} <span class="sub-arrow"></span></a>
                                                        @if(count($singleHeaderCat->subcategory))
                                                            @include('headerSideCategory',['subcategories' => $singleHeaderCat->subcategory,'parent'=>$singleHeaderCat->name,'parentSlug'=>$singleHeaderCat->slug,'allLangCatsGlobal'=>$allLangCatsGlobal])
                                                        @endif
                                                    </li>
                                                @endforeach
                                            @endif
                                            <li><a href="{{url('projects')}}">{{ __('Projects') }}</a></li>
                                            @if(!empty($allHeaderService) && count($allHeaderService)>0)
                                            <li>
                                                <a href="javascript:void(0)">{{ __('Services') }} <span class="sub-arrow"></span></a>
                                                <ul>
                                                    @foreach($allHeaderService as $singleHeaderService)
                                                        @php
                                                            $headerServiceName = (isset($allHeaderLangService[$singleHeaderService->id]) && !empty($allHeaderLangService[$singleHeaderService->id]))?$allHeaderLangService[$singleHeaderService->id]:$singleHeaderService->name;
                                                        @endphp
                                                    <li>
                                                        @if(!empty($singleHeaderService->external_link))
                                                            <a href="{{$singleHeaderService->external_link}}" target="_blank">{{__($headerServiceName)}}</a>
                                                        @else
                                                            <a href="{{url('services/'.$singleHeaderService->slug)}}">{{__($headerServiceName)}}</a>
                                                        @endif
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                            @endif
                                            @php
                                                $allHeaderPages = getMenuPages('1');
                                            @endphp
                                            @if(!empty($allHeaderPages))
                                                @foreach($allHeaderPages as $singlePage)
                                                    @php
                                                    $pageLangData = getLanguageReconds('pages_translations',array('name','content'),array('page_id'=>$singlePage->id));
                                                    if(!empty($pageLangData->name)){
                                                        $pageTitle = $pageLangData->name;
                                                    }else{
                                                        $pageTitle = $singlePage->name;
                                                    }
                                                    @endphp
                                                    <li><a href="{{url('page/'.$singlePage->slug)}}">{{__($pageTitle)}}</a></li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="brand-logo">
                                <a href="{{ url('/')}}"><img src="{{ asset('frontend/assets/images/icon/logo.png') }}" class="img-fluid blur-up lazyload" alt=""></a>
                                <span class="text">{{ __('A healthcare solutions company') }}</span>
                            </div>
                        </div>
                        <div class="menu-right pull-right">
                            <div>
                                <nav id="main-nav">
                                    <div class="toggle-nav"><i class="fa fa-bars sidebar-bar"></i></div>
                                    <ul id="main-menu" class="sm pixelstrap sm-horizontal hover-unset">
                                        <li>
                                            <div class="mobile-back text-right">{{ __('Back') }}<i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                                        </li>
                                        @if(!empty($allCatsGlobal))
                                            @foreach($allCatsGlobal as $singleHeaderCat)
                                                @php
                                                    $headerCatName = (isset($allLangCatsGlobal[$singleHeaderCat->id]) && !empty($allLangCatsGlobal[$singleHeaderCat->id]))?$allLangCatsGlobal[$singleHeaderCat->id]:$singleHeaderCat->name;
                                                @endphp
                                                <li class="mega">
                                                    <a href="{{url('shop/'.$singleHeaderCat->slug)}}">{{__($headerCatName)}}<span class="sub-arrow"></span></a>
                                                    <ul class="mega-menu full-mega-menu">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-lg-8 col-md-8">
                                                                    <div class="menu-sub-category">
                                                                        @if(count($singleHeaderCat->subcategory))
                                                                            @foreach($singleHeaderCat->subcategory as $singleHeaderSubCat)
                                                                                @if($singleHeaderSubCat->display_status==1)
                                                                                    @php
                                                                                        $headerSubCatName = (isset($allLangCatsGlobal[$singleHeaderSubCat->id]) && !empty($allLangCatsGlobal[$singleHeaderSubCat->id]))?$allLangCatsGlobal[$singleHeaderSubCat->id]:$singleHeaderSubCat->name;
                                                                                    @endphp
                                                                                    <div class="mega-box">
                                                                                        <div class="link-section">
                                                                                            <div class="menu-content">
                                                                                                <ul>
                                                                                                    <li class="child-equipment" rel="{{$singleHeaderSubCat->image}}">
                                                                                                        <a href="{{url('shop/'.$singleHeaderCat->slug.'/'.$singleHeaderSubCat->slug)}}">{{__($headerSubCatName)}}</a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="equipment-imgcntr"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ul>
                                                </li>
                                            @endforeach
                                        @endif
                                        <li><a href="{{url('projects')}}">{{ __('Projects') }}</a></li>
                                        @if(!empty($allHeaderService) && count($allHeaderService)>0)
                                            <li>
                                                <a href="javascript:void(0)">{{ __('Services') }} <span class="sub-arrow"></span></a>
                                                <ul>
                                                    @foreach($allHeaderService as $singleHeaderService)
                                                        @php
                                                            $headerServiceName = (isset($allHeaderLangService[$singleHeaderService->id]) && !empty($allHeaderLangService[$singleHeaderService->id]))?$allHeaderLangService[$singleHeaderService->id]:$singleHeaderService->name;
                                                        @endphp
                                                        <li>
                                                            @if(!empty($singleHeaderService->external_link))
                                                                <a href="{{$singleHeaderService->external_link}}" target="_blank">{{__($headerServiceName)}}</a>
                                                            @else
                                                                <a href="{{url('services/'.$singleHeaderService->slug)}}">{{__($headerServiceName)}}</a>
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                        @if(!empty($allHeaderPages))
                                            @foreach($allHeaderPages as $singlePage)
                                                @php
                                                $pageLangData = getLanguageReconds('pages_translations',array('name','content'),array('page_id'=>$singlePage->id));
                                                if(!empty($pageLangData->name)){
                                                    $pageTitle = $pageLangData->name;
                                                }else{
                                                    $pageTitle = $singlePage->name;
                                                }
                                                @endphp
                                                <li><a href="{{url('page/'.$singlePage->slug)}}">{{__($pageTitle)}}</a></li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </nav>
                            </div>
                            <div>
                                <div class="icon-nav">
                                    <ul>
                                        <li class="onhover-div mobile-search">
                                            <div>
                                                <img src="{{ asset('frontend/assets/images/icon/search.png') }}" onclick="openSearch()" class="img-fluid blur-up lazyload" alt=""> <i class="ti-search" onclick="openSearch()"></i>
                                            </div>
                                            <div id="search-overlay" class="search-overlay">
                                                <div>
                                                    <span class="closebtn" onclick="closeSearch()">×</span>
                                                    <div class="overlay-content">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-xl-12">
                                                                    <form name="headersearch" action="{{route('search')}}" method="post">
                                                                        @csrf
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="{{__('Search a Product')}}" name="search" value="{{((isset($headerSearch))?$headerSearch:'')}}">
                                                                        </div>
                                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="onhover-div mobile-setting">
                                            <div>
                                                <img src="{{ asset('frontend/assets/images/icon/user.png') }}" class="img-fluid blur-up lazyload" alt=""> <i class="ti-settings"></i>
                                            </div>
                                            <div class="show-div setting">
                                                <h6>{{ __('Account') }}</h6>
                                                <ul>
                                                    @guest
                                                    <li>
                                                        <a href="{{ route('login') }}">{{ __('Login') }}</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('register') }}">{{ __('Sign Up') }}</a>
                                                    </li>
                                                    @else
                                                    <li><a href="{{ route('user') }}">{{ __('My Account') }}</a></li>
                                                    <li>
                                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                            {{ __('Logout') }}
                                                        </a>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </li>
                                                    @endguest
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="onhover-div mobile-cart">
                                            @php
                                            $allCartItems   = getCartIterms();
                                            $subTotalAmount = 0;
                                            @endphp
                                            <div>
                                                <img src="{{ asset('frontend/assets/images/icon/cart.png') }}" class="img-fluid blur-up lazyload" alt=""> <i class="ti-shopping-cart"></i>
                                                <span id="header_cart_counter"> {{count($allCartItems)}} </span>
                                            </div>
                                            <ul class="show-div shopping-cart showheadercartsection">
                                                @if(!empty($allCartItems) && count($allCartItems)>0)
                                                @foreach($allCartItems as $singleCartItem)
                                                @php
                                                $cartProLangData = getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleCartItem->product_id));
                                                if(!empty($cartProLangData->name)){
                                                    $cartProductName = $cartProLangData->name;
                                                }else{
                                                    $cartProductName = $singleCartItem->name;
                                                }
                                                $qty = $singleCartItem->qty;
                                                $price = $singleCartItem->price;
                                                $special_price = $singleCartItem->special_price;
                                                $stock_availability = $singleCartItem->stock_availability;
                                                $inventory_management = $singleCartItem->inventory_management;
                                                $availableQty = $singleCartItem->availableQty;
                                                if(!empty($special_price)){
                                                    $proPrice = $special_price;
                                                }else{
                                                    $proPrice = $price;
                                                }
                                                if(($inventory_management==0 && $stock_availability==1) ||
                                                ($inventory_management==1 && $stock_availability==1 &&
                                                $availableQty>=$qty)){
                                                    $cartQty = $qty;
                                                }else if($inventory_management==1 && $stock_availability==1 &&
                                                $availableQty<$qty){
                                                    $cartQty=$availableQty; 
                                                }else{ 
                                                    $cartQty='0' ; 
                                                }
                                                $totalPrice=$cartQty*$proPrice;
                                                $subTotalAmount=$subTotalAmount+$totalPrice;@endphp 
                                                <li>
                                                    <div class="media">
                                                        <a href="{{url('product/'.$singleCartItem->slug)}}"><img alt="{{$cartProductName}}" class="mr-3" src="{{ asset($singleCartItem->image) }}"></a>
                                                        <div class="media-body">
                                                            <a href="{{url('product/'.$singleCartItem->slug)}}">
                                                                <h4>{{__($cartProductName)}}</h4>
                                                            </a>
                                                            <h4><span>{{$cartQty}} x {{showPrice($proPrice)}}</span>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <div class="close-circle">
                                                        <a href="javascript:void(0)" class="delete-from-cart" data-id="{{$singleCartItem->product_id}}" data-loc="header">
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </li>
                                                @endforeach
                                                <li>
                                                    <div class="total">
                                                        <h5>{{ __('Subtotal') }} : <span> {{showPrice($subTotalAmount)}}</span>
                                                        </h5>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="buttons">
                                                        <a href="{{route('cart')}}" class="view-cart">{{ __('View Cart') }}</a>
                                                        <a href="{{route('checkout')}}" class="checkout">{{ __('Checkout') }}</a>
                                                    </div>
                                                </li>
                                                @else
                                                    <li>
                                                        <div class="total">
                                                            <h5>{{ __('Your Cart is Empty') }}</h5>
                                                        </div>
                                                    </li>
                                                @endif
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Pages Content will display here -->
    @yield('content')
    <!-- footer -->
    <footer class="footer-light">
        <section class="section-b-space dark-layout">
            <div class="container">
                <div class="row footer-theme partition-f">
                    <div class="col-lg-4 col-md-6">
                        <div class="footer-contant">
                            <div class="footer-title">
                                <h4>{{ __('SUBSCRIBE TO YOUR NEWSLETTER') }}</h4>
                                <h5>{{ __('imedical will never sell or give away your information.') }} </h5>
                                <span id="ajax-uploading"></span>
                            </div>
                            <div class="form-group subscribe-form">
                                <form action="" name="footer_newsletter" id="footer_newsletter" method="post">
                                    @csrf
                                    <input type="email" name="newsletter_email" id="newsletter_email"
                                        class="form-control" placeholder="{{__('Your e-mail address')}}" required>
                                    <button type="submit" class="btn">{{ __('Submit') }}</button>
                                </form>
                            </div>
                            <div class="footer-social">
                                <ul>
                                    @if(isset($generalSettings->fb_status) && $generalSettings->fb_status==1 &&
                                    isset($generalSettings->fb_link) && !empty($generalSettings->fb_link) )
                                        <li><a href="{{$generalSettings->fb_link}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    @endif
                                    @if(isset($generalSettings->gp_status) && $generalSettings->gp_status==1 &&
                                    isset($generalSettings->gp_link) && !empty($generalSettings->gp_link) )
                                        <li><a href="{{$generalSettings->gp_link}}"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                    @endif
                                    @if(isset($generalSettings->tw_status) && $generalSettings->tw_status==1 &&
                                    isset($generalSettings->tw_link) && !empty($generalSettings->tw_link) )
                                        <li><a href="{{$generalSettings->tw_link}}" aria-hidden="true"><i class="fa fa-twitter"></i></a></li>
                                    @endif
                                    @if(isset($generalSettings->li_status) && $generalSettings->li_status==1 &&
                                    isset($generalSettings->li_status) && !empty($generalSettings->li_status) )
                                        <li><a href="{{$generalSettings->li_link}}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    @endif
                                    @if(isset($generalSettings->in_status) && $generalSettings->in_status==1 &&
                                    isset($generalSettings->in_status) && !empty($generalSettings->in_status) )
                                        <li><a href="{{$generalSettings->in_link}}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    @endif
                                    <li><a href="https://www.youtube.com"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col offset-xl-1">
                        <div class="sub-title">
                            <div class="footer-title">
                                <h4>{{ __('My Account') }}</h4>
                            </div>
                            <div class="footer-contant">
                                <ul>
                                    @guest
                                        <li><a href="{{route('login')}}">{{ __('Login') }}</a></li>
                                        <li><a href="{{route('register')}}">{{ __('Sign Up') }}</a></li>
                                    @else
                                        <li><a href="{{route('user')}}">{{ __('My Account') }}</a></li>
                                        <li><a href="{{route('my-orders')}}">{{ __('My Orders') }}</a></li>
                                    @endguest
                                    @php
                                        $allFooter1Pages = getMenuPages('2');
                                    @endphp
                                    @if(!empty($allFooter1Pages))
                                        @foreach($allFooter1Pages as $singleFoot1)
                                            @php
                                            $pageLangData = getLanguageReconds('pages_translations',array('name','content'),array('page_id'=>$singleFoot1->id));
                                            if(!empty($pageLangData->name)){
                                                $pageTitle = $pageLangData->name;
                                            }else{
                                                $pageTitle = $singleFoot1->name;
                                            }
                                            @endphp
                                            <li><a href="{{url('page/'.$singleFoot1->slug)}}">{{__($pageTitle)}}</a></li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="sub-title">
                            <div class="footer-title">
                                <h4>{{ __('Usefull Links') }}</h4>
                            </div>
                            <div class="footer-contant">
                                <ul class="links">
                                    @php
                                        $allFooter2Pages = getMenuPages('3');
                                    @endphp
                                    @if(!empty($allFooter2Pages))
                                        @foreach($allFooter2Pages as $singleFoot2)
                                            @php
                                            $pageLangData = getLanguageReconds('pages_translations',array('name'),array('page_id'=>$singleFoot2->id));
                                            if(!empty($pageLangData->name)){
                                                $pageTitle = $pageLangData->name;
                                            }else{
                                                $pageTitle = $singleFoot2->name;
                                            }
                                            @endphp
                                            <li><a href="{{url('page/'.$singleFoot2->slug)}}">{{__($pageTitle)}}</a></li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="sub-title">
                            <div class="footer-title">
                                <h4>{{ __('Company Information') }}</h4>
                            </div>
                            <div class="footer-contant">
                                <ul class="contact-list">
                                    <li><i class="fa fa-map-marker"></i>
                                        <!-- {{(isset($generalSettings->contact_address))?$generalSettings->contact_address:''}} -->
                                        {{ __('Imedical Shop, 11351 International Dr North Chesterfield, VA 23236')}}
                                    </li>
                                    <li><i class="fa fa-phone"></i><a href="tel:{{(isset($generalSettings->contact_phone))?$generalSettings->contact_phone:''}}">{{(isset($generalSettings->contact_phone))?$generalSettings->contact_phone:''}}</a>
                                    </li>
                                    <li><i class="fa fa-envelope-o"></i><a href="mailto:{{(isset($generalSettings->contact_email))?$generalSettings->contact_email:''}}">{{(isset($generalSettings->contact_email))?$generalSettings->contact_email:''}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-sm-12">
                        <div class="footer-end">
                            <p>© {{date('Y')}} {{ __('ImedicalShop. All Rights Reserved.') }}</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-12">
                        <div class="payment-card-bottom">
                            <ul>
                                <li>
                                    <img src="{{ asset('frontend/assets/images/icon/visa.png') }}" alt="">
                                </li>
                                <li>
                                    <img src="{{ asset('frontend/assets/images/icon/mastercard.png') }}" alt="">
                                </li>
                                <li>
                                    <img src="{{ asset('frontend/assets/images/icon/paypal.png') }}" alt="">
                                </li>
                                <li>
                                    <img src="{{ asset('frontend/assets/images/icon/american-express.png') }}" alt="">
                                </li>
                                <li>
                                    <img src="{{ asset('frontend/assets/images/icon/discover.png') }}" alt="">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->
    <!-- tap to top -->
    <div class="tap-top top-cls">
        <div><i class="fa fa-angle-double-up"></i></div>
    </div>
    <!-- tap to top end -->
    <script src="{{ asset('frontend/assets/js/jquery-3.3.1.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="{{ asset('frontend/assets/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/slick.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/menu.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/lazysizes.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/price-range.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/bootstrap.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/bootstrap-notify.min.js') }}"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('frontend/assets/js/script.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/common.js') }}"></script>
    <script type="text/javascript">
        function googleTranslateElementInit() {
            setCookie('googtrans', '/en', 1);
            new google.translate.TranslateElement({
                pageLanguage: 'en'
            }, 'google_translate_element');
        }
        function setCookie(key, value, expiry) {
            var expires = new Date();
            expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
            document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
        }
        $(document).on('change', '.goog-te-combo', function() {
            //window.location.href = base_url + "/lang/" + $(this).val();
            $.ajax({ 
				type: 'GET',
				url: base_url + "/lang/" + $(this).val(),
				data: {}, 						
				success: function(data){
							
				} 
			});
        });
    </script>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit">
    </script>
</body>
</html>