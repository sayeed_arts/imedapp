<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ (isset($meta_title) && !empty($meta_title))?$meta_title:config('app.name', __('IMedical Shop')) }}</title>
    <meta name="description" content="{{ (isset($meta_desc))?$meta_desc:config('app.meta_desc', 'IMedical Shop') }}">
    <meta name="keywords" content="{{ (isset($meta_key))?$meta_key:config('app.meta_key', 'IMedical Shop') }}">
    @if(isset($isProductPage) && $isProductPage=='Yes')
    <meta property="og:url" content="{{url('product/'.$currPage)}}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{$page_title}}" />
    <meta property="og:description" content="{{$pageData->short_description}}" />
    <meta property="og:image" content="{{asset($pageData->image)}}" />
    @endif
    <link rel="icon" href="{{ asset('frontend/assets/images/favicon/1.png') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('frontend/assets/images/favicon/1.png') }}" type="image/x-icon">

    <script src="https://kit.fontawesome.com/8ca74604f3.js" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/slick-theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/forum/css/chatter.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/price-range.css') }}"> -->
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"> -->

    

    

    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/color1.css') }}" media="screen"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/main.css') }}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/media.css') }}" media="screen">
    <script type="text/javascript">
    var base_url = "{{ url('/')}}";
    </script>
</head>

<body style="top: 0px !important">
    <!-- loader start -->
    <!-- <div class="loader_skeleton">
        <img src="{{ asset('frontend/assets/images/ajax-loader.gif') }}" alt="" srcset="">
    </div> -->
    <!-- loader end -->
    <!-- header start -->
    <header class="header-main">
        @php
        $generalSettings = generalSettings();
        @endphp
        <div class="mobile-fix-option"></div>


        <!-- <div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="header-contact">
                            <ul>
                                <li>{{ __('Welcome to Our store IMedical Shop') }}</li>
                                <li><i class="fa fa-phone" aria-hidden="true"></i>{{ __('Call Us') }}:
                                    <a
                                        href="tel:{{(isset($generalSettings->contact_phone))?$generalSettings->contact_phone:''}}">{{(isset($generalSettings->contact_phone))?$generalSettings->contact_phone:''}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 text-right">
                        <div class="header-contact-right">
                            <ul>
                                <li><a href="{{route('about-us')}}">{{ __('About us') }}</a></li>
                                @guest
                                <li>
                                    <a href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                <li>
                                    <a href="{{ route('register') }}">{{ __('Sign Up') }}</a>
                                </li>
                                @else
                                <li><a href="{{ route('user') }}">{{ __('My Account') }}</a></li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                                @endguest
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="header-top">
            <div class="container">
                <div class="row ">
                    <div class="col-md-2">
                        <div class="brand-logo">
                            <a href="{{ url('/')}}"><img src="{{ asset('frontend/assets/images/icon/logo.png') }}"
                                    class="img-fluid blur-up lazyload" alt=""></a>
                            <!-- <span class="text">{{ __('A healthcare solutions company') }}</span> -->
                        </div>
                    </div>

                    <div class="col-md-5">
                        <form class="search-form" name="headersearch" action="{{route('search')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control"
                                    id="exampleInputPassword1"
                                    placeholder="{{__('Search a Product')}}"
                                    name="search"
                                    value="{{((isset($headerSearch))?$headerSearch:'')}}">
                            </div>
                            <button type="submit" class="btn btn-primary"><i
                                    class="fa fa-search"></i></button>
                        </form>
                    </div>

                    <div class="col-md-5 text-right">
                        <ul class="header-right">

                        @guest
                           
                            <li>                        
                                <a href="{{url('vendor-register')}} " class="btn sell">{{ __('Sell with us') }}</a>
                            </li>
                            
                        @else
                        
                        @endguest


                        
                        <li style="display:none">
                                    <select class="form-control headerchangeLang">
                                        @php
                                        $langStrForFooter = '';
                                        $allLanguages = getLanguages();
                                        @endphp
                                        @if(!empty($allLanguages))
                                        @foreach($allLanguages as $singleLanguage)
                                        @php
                                        $langStrForFooter .=$singleLanguage->code.',';
                                        @endphp
                                        <option value="{{$singleLanguage->code}}"
                                            {{ session()->get('locale') == $singleLanguage->code ? 'selected' : '' }}>
                                            {{$singleLanguage->name}}</option>
                                        @endforeach
                                        @endif
                                        @php
                                        $langStrForFooter = substr($langStrForFooter,0,-1);
                                        @endphp
                                    </select>
                                </li>
                                <li class="currency">
                                    <div class="custom-select-drop">
                                        <div class="select">
                                            <select class="headerchangeCurrency">
                                                @php
                                                $allCurrencies = getCurrencies();
                                                @endphp
                                                @if(!empty($allCurrencies))
                                                @foreach($allCurrencies as $singleCurrency)
                                                <option value="{{$singleCurrency['code']}}"
                                                    {{ session()->get('appcurrency') == $singleCurrency['code'] ? 'selected' : '' }}>
                                                    {{$singleCurrency['code']}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li class="google-translate">
                                    <p id="google_translate_element"></p>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row main-menu-section">
                <div class="col-md-9" style="position:static;">
                    <nav id="main-nav">
                        <div class="toggle-nav"><i class="fa fa-bars sidebar-bar"></i></div>
                        <ul id="main-menu" class="sm pixelstrap sm-horizontal hover-unset">
                            <li>
                                <div class="mobile-back text-right">{{ __('Back') }}<i
                                        class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                            </li>
                            @php
                                createCartCookie();
                                $allCatsGlobal = getCategories();
                                $allLangCatsGlobal = getLangCategories();
                                $allHeaderService = getHeaderServices();
                                $allHeaderLangService = getHeaderLangServices();
                                @endphp
                                @if(!empty($allCatsGlobal))
                                @foreach($allCatsGlobal as $singleHeaderCat)
                                @php
                                $headerCatName = (isset($allLangCatsGlobal[$singleHeaderCat->id]) &&
                                !empty($allLangCatsGlobal[$singleHeaderCat->id]))?$allLangCatsGlobal[$singleHeaderCat->id]:$singleHeaderCat->name;
                                @endphp
                            <li class="mega">
                                <a href="{{url('shop/'.$singleHeaderCat->slug)}}">{{__($headerCatName)}}
                                    @if(count($singleHeaderCat->subcategory))
                                    <span class="sub-arrow"></span>
                                    @endif
                                </a>
                                @if(count($singleHeaderCat->subcategory))
                                <ul class="mega-menu full-mega-menu">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-8 col-md-8">
                                                <div class="menu-sub-category">
                                                    @if(count($singleHeaderCat->subcategory))
                                                    @foreach($singleHeaderCat->subcategory as
                                                    $singleHeaderSubCat)
                                                    @if($singleHeaderSubCat->display_status==1)
                                                    @php
                                                    $headerSubCatName =
                                                    (isset($allLangCatsGlobal[$singleHeaderSubCat->id]) &&
                                                    !empty($allLangCatsGlobal[$singleHeaderSubCat->id]))?$allLangCatsGlobal[$singleHeaderSubCat->id]:$singleHeaderSubCat->name;
                                                    @endphp
                                                    <div class="mega-box">
                                                        <div class="link-section">
                                                            <div class="menu-content">
                                                                <ul>
                                                                    <li class="child-equipment @if(count($singleHeaderSubCat->subcategory)) show-child-arraw @endif"
                                                                        rel="{{$singleHeaderSubCat->image}}">
                                                                        <a
                                                                            href="{{url('shop/'.$singleHeaderCat->slug.'/'.$singleHeaderSubCat->slug)}}"><b>{{__($headerSubCatName)}}</b></a>
                                                                        @if(count($singleHeaderSubCat->subcategory))
                                                                        <ul class="third-level-cat">
                                                                            @foreach($singleHeaderSubCat->subcategory
                                                                            as $singleHeaderSubChildCat)
                                                                            @if($singleHeaderSubChildCat->display_status==1)
                                                                            @php
                                                                            $headerSubCatChildName =
                                                                            (isset($allLangCatsGlobal[$singleHeaderSubChildCat->id])
                                                                            &&
                                                                            !empty($allLangCatsGlobal[$singleHeaderSubChildCat->id]))?$allLangCatsGlobal[$singleHeaderSubChildCat->id]:$singleHeaderSubChildCat->name;
                                                                            @endphp
                                                                            <li class="child-equipment"
                                                                                rel="{{$singleHeaderSubChildCat->image}}">
                                                                                <a
                                                                                    href="{{url('shop/'.$singleHeaderCat->slug.'/'.$singleHeaderSubCat->slug.'/'.$singleHeaderSubChildCat->slug)}}">{{__($headerSubCatChildName)}}</a>
                                                                            </li>
                                                                            @endif
                                                                            @endforeach
                                                                        </ul>
                                                                        @endif
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="equipment-imgcntr"></div>
                                            </div>
                                        </div>
                                    </div>
                                </ul>
                                @endif
                            </li>
                            @endforeach
                            @endif
                            <li class="projects"><a href="{{url('projects')}}">{{ __('Projects') }}</a></li>
                            @if(!empty($allHeaderService) && count($allHeaderService)>0)
                            <li class="services">
                                <a href="javascript:void(0)">{{ __('Services') }} <span
                                        class="sub-arrow"></span></a>
                                <ul>
                                    @foreach($allHeaderService as $singleHeaderService)
                                    @php
                                    $headerServiceName =
                                    (isset($allHeaderLangService[$singleHeaderService->id]) &&
                                    !empty($allHeaderLangService[$singleHeaderService->id]))?$allHeaderLangService[$singleHeaderService->id]:$singleHeaderService->name;
                                    @endphp
                                    <li>
                                        @if(!empty($singleHeaderService->external_link))
                                        <a href="{{$singleHeaderService->external_link}}"
                                            target="_blank">{{__($headerServiceName)}}</a>
                                        @else
                                        <a
                                            href="{{url('services/'.$singleHeaderService->slug)}}">{{__($headerServiceName)}}</a>
                                        @endif
                                    </li>
                                    @endforeach
                                    <li><a href="{{url('/service-areas')}}">{{ __('Services Areas') }}</a></li>

                                </ul>
                            </li>
                            @endif
                            <li class="projects"><a href="{{url('news')}}">{{ __('News') }}</a></li>
                            <li>
                            <a href="javascript:void(0)">{{ __('Careers') }} <span class="sub-arrow"></span></a>
                            <ul>
                                <li> <a href="{{url('jobs')}}">{{ __('Jobs') }}</a></li>
                                <li> <a href="{{url('trainings')}}">{{ __('Service Trainings') }}</a></li>
                            </ul>

                           
                            </li>
                            @if(!empty($allHeaderPages))
                            @foreach($allHeaderPages as $singlePage)
                            @php
                            $pageLangData =
                            getLanguageReconds('pages_translations',array('name','content'),array('page_id'=>$singlePage->id));
                            if(!empty($pageLangData->name)){
                            $pageTitle = $pageLangData->name;
                            }else{
                            $pageTitle = $singlePage->name;
                            }
                            @endphp
                            <li><a href="{{url('page/'.$singlePage->slug)}}">{{__($pageTitle)}}</a></li>
                            @endforeach
                            @endif
                        </ul>
                    </nav>
                </div>
                <div class="col-md-3">
                    <div class="menu-right pull-right">
                        <div class="icon-nav">
                            <ul>
                                <li class="onhover-div mobile-setting" >
                                    <div>
                                        <img src="{{ asset('frontend/assets/images/login-icon.jpg') }}"
                                            class="img-fluid blur-up lazyload" alt="">
                                            @guest
                                            
                                                <a href="{{ route('login') }}">{{ __('Login') }}</a> / 
                                        
                                                <a href="{{ route('register') }}">{{ __('Sign Up') }}</a>
                                        
                                            @else
                                            <a href="{{ route('user') }}">{{ __('My Account') }}</a> / 
                                            
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault(); document.getElementById('logout-form2').submit();">
                                                    {{ __('Logout') }}
                                                </a>
                                                <form id="logout-form2" action="{{ route('logout') }}"
                                                    method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            
                                            @endguest

                                            <i class="ti-settings"></i>
                                    </div>
                                
                                </li>
                                <li class="mobile-cart">
                                    @php
                                    $allCartItems = getCartIterms();
                                    $subTotalAmount = 0;
                                    @endphp
                                    <div class="mini-cart" data-toggle="modal" data-target="#MinicartModal">
                                        <img src="{{ asset('frontend/assets/images/cart.png') }}"
                                            class="img-fluid blur-up lazyload" alt=""> <i
                                            class="ti-shopping-cart"></i>
                                        <span class="header_cart_counter"> {{count($allCartItems)}} </span>
                                    </div>



                                     <!-- MINI CART MODAL -->
                                        <div class="modal right fade mini-cart-modal" id="MinicartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel2">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <path d="M12 0.75C10.8066 0.75 9.66196 1.22411 8.81805 2.06802C7.97414 2.91193 7.50003 4.05653 7.50003 5.25V6H6.87753C6.24612 5.99934 5.63878 6.24222 5.18194 6.6781C4.72511 7.11398 4.45399 7.70925 4.42503 8.34L3.83253 20.6775C3.81734 21.0093 3.86949 21.3407 3.98582 21.6517C4.10216 21.9628 4.28028 22.2471 4.50944 22.4875C4.7386 22.7278 5.01407 22.9193 5.31923 23.0504C5.62439 23.1814 5.95292 23.2493 6.28503 23.25H17.715C18.0471 23.2493 18.3757 23.1814 18.6808 23.0504C18.986 22.9193 19.2615 22.7278 19.4906 22.4875C19.7198 22.2471 19.8979 21.9628 20.0142 21.6517C20.1306 21.3407 20.1827 21.0093 20.1675 20.6775L19.575 8.34C19.5461 7.70925 19.2749 7.11398 18.8181 6.6781C18.3613 6.24222 17.7539 5.99934 17.1225 6H16.5V5.25C16.5 4.05653 16.0259 2.91193 15.182 2.06802C14.3381 1.22411 13.1935 0.75 12 0.75V0.75ZM9.00003 5.25C9.00003 4.45435 9.3161 3.69129 9.87871 3.12868C10.4413 2.56607 11.2044 2.25 12 2.25C12.7957 2.25 13.5587 2.56607 14.1213 3.12868C14.684 3.69129 15 4.45435 15 5.25V6H9.00003V5.25ZM18.075 8.4075L18.6675 20.7525C18.6722 20.8815 18.6515 21.0102 18.6064 21.1311C18.5614 21.2521 18.493 21.363 18.405 21.4575C18.3153 21.5497 18.208 21.623 18.0895 21.6733C17.971 21.7235 17.8437 21.7496 17.715 21.75H6.28503C6.15635 21.7496 6.02904 21.7235 5.91056 21.6733C5.79208 21.623 5.68481 21.5497 5.59503 21.4575C5.50711 21.363 5.43867 21.2521 5.39363 21.1311C5.34859 21.0102 5.32783 20.8815 5.33253 20.7525L5.92503 8.4075C5.93661 8.16276 6.04204 7.9319 6.21943 7.76289C6.39682 7.59388 6.63252 7.49973 6.87753 7.5H17.1225C17.3675 7.49973 17.6032 7.59388 17.7806 7.76289C17.958 7.9319 18.0635 8.16276 18.075 8.4075V8.4075Z" fill="#395589"/>
                                                            <path d="M8.52002 10.8925C8.93423 10.8925 9.27002 10.5567 9.27002 10.1425C9.27002 9.72824 8.93423 9.39246 8.52002 9.39246C8.10581 9.39246 7.77002 9.72824 7.77002 10.1425C7.77002 10.5567 8.10581 10.8925 8.52002 10.8925Z" fill="#395589"/>
                                                            <path d="M15.4801 10.8925C15.8943 10.8925 16.2301 10.5567 16.2301 10.1425C16.2301 9.72824 15.8943 9.39246 15.4801 9.39246C15.0659 9.39246 14.7301 9.72824 14.7301 10.1425C14.7301 10.5567 15.0659 10.8925 15.4801 10.8925Z" fill="#395589"/>
                                                        </svg> 
                                                        <span class="header_cart_counter"> {{count($allCartItems)}}</span> <span>Cart Items</span>
                                                        </h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    </div>

                                                    <div class="modal-body">					
                                                        <div class="show-div shopping-cart showheadercartsection">
                                                            <!-- START UL -->
                                                            
                                                            @if(!empty($allCartItems) && count($allCartItems)>0)
                                                            <ul class="mini-cart-list">
                                                                @foreach($allCartItems as $singleCartItem)
                                                                @php
                                                                $cartProLangData =
                                                                getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleCartItem->product_id));
                                                                if(!empty($cartProLangData->name)){
                                                                $cartProductName = $cartProLangData->name;
                                                                }else{
                                                                $cartProductName = $singleCartItem->name;
                                                                }
                                                                $qty = $singleCartItem->qty;
                                                                $price = $singleCartItem->price;
                                                                $special_price = $singleCartItem->special_price;
                                                                $stock_availability = $singleCartItem->stock_availability;
                                                                $inventory_management = $singleCartItem->inventory_management;
                                                                $availableQty = $singleCartItem->availableQty;
                                                                if(!empty($special_price)){
                                                                $proPrice = $special_price;
                                                                }else{
                                                                $proPrice = $price;
                                                                }
                                                                $attrProNAme = '';
                                                                if(!empty($singleCartItem->product_attr_id)){
                                                                $getProAttrCart = DB::table('products_attributes')
                                                                ->where('id',$singleCartItem->product_attr_id)
                                                                ->where('product_id',$singleCartItem->product_id)
                                                                ->where('is_variation',1)
                                                                ->first();
                                                                $proAttribute_value
                                                                =(isset($getProAttrCart->attribute_value))?$getProAttrCart->attribute_value:'';
                                                                $getAttrCartData = array();

                                                                if(!empty($proAttribute_value)){
                                                                $getAttrCartData = DB::table('attribute_values')
                                                                ->where('id',$proAttribute_value)
                                                                ->first();
                                                                }
                                                                $attrProNAme =
                                                                (isset($getAttrCartData->name))?$getAttrCartData->name:'';
                                                                $proPrice = (isset($getProAttrCart->attribute_price) &&
                                                                $getProAttrCart->attribute_price>0)?$getProAttrCart->attribute_price:$proPrice;
                                                                }

                                                                if(($inventory_management==0 && $stock_availability==1) ||
                                                                ($inventory_management==1 && $stock_availability==1 &&
                                                                $availableQty>=$qty)){
                                                                $cartQty = $qty;
                                                                }else if($inventory_management==1 && $stock_availability==1 &&
                                                                $availableQty<$qty){ $cartQty=$availableQty; }else{ $cartQty='0' ; }
                                                                    $totalPrice=$cartQty*$proPrice;
                                                                    $subTotalAmount=$subTotalAmount+$totalPrice;@endphp 
                                                                <li>
                                                                    <div class="media">
                                                                        <a href="{{url('product/'.$singleCartItem->slug)}}"><img
                                                                                alt="{{$cartProductName}}" class="mr-3"
                                                                                src="{{ asset($singleCartItem->image) }}"></a>
                                                                        <div class="media-body">
                                                                            <a href="{{url('product/'.$singleCartItem->slug)}}">
                                                                                <h4>{{__($cartProductName)}}</h4>
                                                                                @if(!empty($attrProNAme))
                                                                                <!-- <h4>{{__($attrProNAme)}}</h4> -->
                                                                                @endif
                                                                            </a>
                                                                            <span class="mini-cart-qty">{{$cartQty}} x
                                                                                {{showPrice($proPrice)}}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="close-circle">
                                                                        <a href="javascript:void(0)" class="delete-from-cart"
                                                                            data-id="{{$singleCartItem->product_id}}"
                                                                            data-loc="header">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="none">
                                                                        <path d="M6.2253 4.81096C5.83477 4.42044 5.20161 4.42044 4.81108 4.81096C4.42056 5.20148 4.42056 5.83465 4.81108 6.22517L10.5858 11.9999L4.81114 17.7746C4.42062 18.1651 4.42062 18.7983 4.81114 19.1888C5.20167 19.5793 5.83483 19.5793 6.22535 19.1888L12 13.4141L17.7747 19.1888C18.1652 19.5793 18.7984 19.5793 19.1889 19.1888C19.5794 18.7983 19.5794 18.1651 19.1889 17.7746L13.4142 11.9999L19.189 6.22517C19.5795 5.83465 19.5795 5.20148 19.189 4.81096C18.7985 4.42044 18.1653 4.42044 17.7748 4.81096L12 10.5857L6.2253 4.81096Z" fill="black"/>
                                                                        </svg>
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                            <!-- END UL -->

                                                            <div class="bottom">
                                                                <ul>
                                                                    <li>
                                                                        <div class="total">
                                                                            <h5>{{ __('Subtotal') }} : <span>
                                                                                    {{showPrice($subTotalAmount)}}</span>
                                                                            </h5>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="buttons">
                                                                            <a href="{{route('cart')}}"
                                                                                class="view-cart">{{ __('View Cart') }}</a>
                                                                            <a href="{{route('checkout')}}"
                                                                                class="checkout">{{ __('Checkout') }}</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                            @else
                                                            <div class="empty">
                                                                <div class="icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24" fill="none">
                                                                        <path d="M12 0.75C10.8066 0.75 9.66196 1.22411 8.81805 2.06802C7.97414 2.91193 7.50003 4.05653 7.50003 5.25V6H6.87753C6.24612 5.99934 5.63878 6.24222 5.18194 6.6781C4.72511 7.11398 4.45399 7.70925 4.42503 8.34L3.83253 20.6775C3.81734 21.0093 3.86949 21.3407 3.98582 21.6517C4.10216 21.9628 4.28028 22.2471 4.50944 22.4875C4.7386 22.7278 5.01407 22.9193 5.31923 23.0504C5.62439 23.1814 5.95292 23.2493 6.28503 23.25H17.715C18.0471 23.2493 18.3757 23.1814 18.6808 23.0504C18.986 22.9193 19.2615 22.7278 19.4906 22.4875C19.7198 22.2471 19.8979 21.9628 20.0142 21.6517C20.1306 21.3407 20.1827 21.0093 20.1675 20.6775L19.575 8.34C19.5461 7.70925 19.2749 7.11398 18.8181 6.6781C18.3613 6.24222 17.7539 5.99934 17.1225 6H16.5V5.25C16.5 4.05653 16.0259 2.91193 15.182 2.06802C14.3381 1.22411 13.1935 0.75 12 0.75V0.75ZM9.00003 5.25C9.00003 4.45435 9.3161 3.69129 9.87871 3.12868C10.4413 2.56607 11.2044 2.25 12 2.25C12.7957 2.25 13.5587 2.56607 14.1213 3.12868C14.684 3.69129 15 4.45435 15 5.25V6H9.00003V5.25ZM18.075 8.4075L18.6675 20.7525C18.6722 20.8815 18.6515 21.0102 18.6064 21.1311C18.5614 21.2521 18.493 21.363 18.405 21.4575C18.3153 21.5497 18.208 21.623 18.0895 21.6733C17.971 21.7235 17.8437 21.7496 17.715 21.75H6.28503C6.15635 21.7496 6.02904 21.7235 5.91056 21.6733C5.79208 21.623 5.68481 21.5497 5.59503 21.4575C5.50711 21.363 5.43867 21.2521 5.39363 21.1311C5.34859 21.0102 5.32783 20.8815 5.33253 20.7525L5.92503 8.4075C5.93661 8.16276 6.04204 7.9319 6.21943 7.76289C6.39682 7.59388 6.63252 7.49973 6.87753 7.5H17.1225C17.3675 7.49973 17.6032 7.59388 17.7806 7.76289C17.958 7.9319 18.0635 8.16276 18.075 8.4075V8.4075Z" fill="#395589"/>
                                                                        <path d="M8.52002 10.8925C8.93423 10.8925 9.27002 10.5567 9.27002 10.1425C9.27002 9.72824 8.93423 9.39246 8.52002 9.39246C8.10581 9.39246 7.77002 9.72824 7.77002 10.1425C7.77002 10.5567 8.10581 10.8925 8.52002 10.8925Z" fill="#395589"/>
                                                                        <path d="M15.4801 10.8925C15.8943 10.8925 16.2301 10.5567 16.2301 10.1425C16.2301 9.72824 15.8943 9.39246 15.4801 9.39246C15.0659 9.39246 14.7301 9.72824 14.7301 10.1425C14.7301 10.5567 15.0659 10.8925 15.4801 10.8925Z" fill="#395589"/>
                                                                    </svg>
                                                                </div>
                                                              
                                                                <h5>{{ __('Your Cart is Empty') }}</h5>
                                                                
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>

                                                </div><!-- modal-content -->
                                            </div><!-- modal-dialog -->
                                        </div>
                                        <!-- MINI CART END MODAL -->
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <span id="appendheaderajaxmessage"></span>
    <div class="alert" id="headershowajaxmessage" role="alert" style="display: none;"></div>
    <!-- Pages Content will display here -->
    @yield('content')
    <!-- footer -->
    <div class="newsletter">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h4>{{ __('SIGN UP IMEDICAL NEWSLETTER') }}</h4>
                    <h5>{{ __('Be the first to get latest Offers, Deals and News.') }} </h5>
                    <span id="ajax-uploading"></span>
                </div>
                <div class="col-md-6">
                    <div class="form-group subscribe-form">
                        <form action="" name="footer_newsletter" id="footer_newsletter" method="post">
                            @csrf
                            <input type="email" name="newsletter_email" id="newsletter_email"
                                class="form-control" placeholder="{{__('Your e-mail address')}}" required>
                            <button type="submit" class="btn">{{ __('Submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer-light">
        <section class="pb-5">
            <div class="container">
                <div class="row footer-theme partition-f">
                    <div class="col-lg-4 col-md-6">
                        <div class="footer-contant">
                            <div class="footer-title">
                            <img src="{{ asset('frontend/assets/images/icon/logo.png') }}"
                                    class="img-fluid blur-up lazyload" alt="" width="200">
                            </div>

                            <p> iMedical Equipment and Service sells a wide variety of new, used and refurbished medical equipment to hospitals, surgery centers, urgent care facilities, doctor offices, long term care,</p>
                            
                            
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="sub-title">
                            <div class="footer-title">
                                <h4>{{ __('Contact Info') }}</h4>
                            </div>
                            <div class="footer-contant">
                                <ul class="contact-list">
                                    <li>
                                        <p>Call Us</p>
                                        <a class="call" href="tel:{{(isset($generalSettings->contact_phone))?$generalSettings->contact_phone:''}}">{{(isset($generalSettings->contact_phone))?$generalSettings->contact_phone:''}}</a></li>
                                    <li>{!!(isset($generalSettings->contact_address))?$generalSettings->contact_address:''!!}</li>
                                    <li><a href="mailto:{{(isset($generalSettings->contact_email))?$generalSettings->contact_email:''}}">{{(isset($generalSettings->contact_email))?$generalSettings->contact_email:''}}</a></li>
                                </ul>

                                <div class="footer-social">
                                    <ul>
                                        @if(isset($generalSettings->fb_status) && $generalSettings->fb_status==1 &&
                                        isset($generalSettings->fb_link) && !empty($generalSettings->fb_link) )
                                        <li><a href="{{$generalSettings->fb_link}}">
                                        <img src="{{ asset('frontend/assets/images/face.png') }}" class="img-fluid" alt="">
                                        </a></li>
                                        @endif
                                        @if(isset($generalSettings->gp_status) && $generalSettings->gp_status==1 &&
                                        isset($generalSettings->gp_link) && !empty($generalSettings->gp_link) )
                                        <li><a href="{{$generalSettings->gp_link}}"><i class="fa fa-google-plus"
                                                    aria-hidden="true"></i></a></li>
                                        @endif
                                        @if(isset($generalSettings->tw_status) && $generalSettings->tw_status==1 &&
                                        isset($generalSettings->tw_link) && !empty($generalSettings->tw_link) )
                                        <li><a href="{{$generalSettings->tw_link}}" aria-hidden="true">
                                        <img src="{{ asset('frontend/assets/images/twitter.png') }}" class="img-fluid" alt="">

                                        </a></li>
                                        @endif
                                        @if(isset($generalSettings->li_status) && $generalSettings->li_status==1 &&
                                        isset($generalSettings->li_status) && !empty($generalSettings->li_status) )
                                        <li><a href="{{$generalSettings->li_link}}">
                                        <img src="{{ asset('frontend/assets/images/linkedin.png') }}" class="img-fluid" alt="">
                                        </a></li>
                                        @endif
                                        @if(isset($generalSettings->in_status) && $generalSettings->in_status==1 &&
                                        isset($generalSettings->in_status) && !empty($generalSettings->in_status) )
                                        <li><a href="{{$generalSettings->in_link}}">
                                        <img src="{{ asset('frontend/assets/images/insta.png') }}" class="img-fluid" alt="">
                                        </a></li>
                                        @endif
                                        <li><a href="https://www.youtube.com">
                                        <img src="{{ asset('frontend/assets/images/youtube.png') }}" class="img-fluid" alt="">
                                        </a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="col-md-2">
                        <div class="sub-title">
                            <div class="footer-title">
                                <h4>{{ __('Information') }}</h4>
                            </div>
                            <div class="footer-contant">
                                <ul>
                                    @guest
                                    <li><a href="{{route('login')}}">{{ __('Login') }}</a></li>
                                    <li><a href="{{route('register')}}">{{ __('Sign Up') }}</a></li>
                                    @else
                                    <li><a href="{{route('user')}}">{{ __('My Account') }}</a></li>
                                    <li><a href="{{route('my-orders')}}">{{ __('My Orders') }}</a></li>
                                    @endguest
                                    @php
                                    $allFooter1Pages = getMenuPages('2');
                                    @endphp
                                    @if(!empty($allFooter1Pages))
                                    @foreach($allFooter1Pages as $singleFoot1)
                                    @php
                                    $pageLangData =
                                    getLanguageReconds('pages_translations',array('name','content'),array('page_id'=>$singleFoot1->id));
                                    if(!empty($pageLangData->name)){
                                    $pageTitle = $pageLangData->name;
                                    }else{
                                    $pageTitle = $singleFoot1->name;
                                    }
                                    @endphp
                                    <li><a href="{{url('page/'.$singleFoot1->slug)}}">{{__($pageTitle)}}</a></li>
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="sub-title">
                            <div class="footer-title">
                                <h4>{{ __('Quick Links') }}</h4>
                            </div>
                            <div class="footer-contant">
                                <ul class="links">
                                    @php
                                    $allFooter2Pages = getMenuPages('3');
                                    @endphp
                                    @if(!empty($allFooter2Pages))
                                    @foreach($allFooter2Pages as $singleFoot2)
                                    @php
                                    $pageLangData =
                                    getLanguageReconds('pages_translations',array('name'),array('page_id'=>$singleFoot2->id));
                                    if(!empty($pageLangData->name)){
                                    $pageTitle = $pageLangData->name;
                                    }else{
                                    $pageTitle = $singleFoot2->name;
                                    }
                                    @endphp
                                    <li><a href="{{url('page/'.$singleFoot2->slug)}}">{{__($pageTitle)}}</a></li>
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-sm-12 text-left">
                       
                            <img src="{{ asset('frontend/assets/images/payment.png') }}" alt="">                                
                      
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-12 text-right">
                    <div class="footer-end">
                            <p>© {{date('Y')}} {{ __('ImedicalShop. All Rights Reserved.') }}</p>
                        </div>

                        
                    </div>
                </div>
            </div>
        </div>
    </footer>

<?php $countrylist = countrylist(); ?>
    <div id="getTheQuote" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form name="getaquoteform"  class="jqueryValidate" id="getaquoteform" method="POST">
                    @csrf
                    <input type="hidden" name="quoteProductName" id="quoteProductName" value="">
                   <input type="hidden" name="quoteProductID" id="quoteProductID" value="">
                    <input type="hidden" name="quoteProductUrl" id="quoteProductUrl" value="">
                    <div class="modal-header">
                        <h4 class="modal-title">Get Quote</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <fieldset class="fieldset">
                            <span id="ajax-uploading-get-quote"></span>
                            <div class="form-group">
                                <label class="label" for="email"><span>Email:</span></label>
                                <input name="email" placeholder="User Email" id="email" value="" type="email"
                                    class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label class="label" for="organization"><span>Organization:</span></label>
                                <input name="organization" placeholder="Organization" id="organization" value=""
                                    type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label class="label" for="phone"><span>Phone:</span></label>
                                <input name="phone" id="phone" placeholder="Phone Number" value="" type="text"
                                    class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label class="label" for="comment"><span>Additional Details:</span></label>
                                <textarea id="comment"
                                    placeholder="List specific &amp; unique requirements here, such as: Configuration, software rev, options, accessories, etc."
                                    name="comment" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="label" for="qty"><span>Quantity:</span></label>
                                <input name="qty" placeholder="Quantity" id="qty" value="" type="number"
                                    class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label class="label" for="name"><span>First Name:</span></label>
                                <input name="firstname" placeholder="First Name" id="firstname" value=""
                                    class="form-control" type="text" required>
                            </div>
                            <div class="form-group">
                                <label class="label" for="lastname"><span>Last Name:</span></label>
                                <input name="lastname" placeholder="Last Name" id="lastname" value=""
                                    class="form-control" type="text" required>
                            </div>
                            <div class="form-group">
                                <label class="label" for="street"><span>Address:</span></label>
                                <input name="street" id="street" placeholder="Address" value="" type="text"
                                    class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label class="label" for="city"><span>City:</span></label>
                                <input name="city" id="city" placeholder="City" value="" type="text"
                                    class="form-control" required>
                            </div>
                            
                            <div class="form-group">
                                <label class="label" for="country"><span>Country:</span></label>
                                <select name="country" id="country_yo" placeholder="Country" class="form-control" required> <option value="">Select Country</option>
                                    @foreach($countrylist as $key=>$val)
                                    
                                    <option     @if(isset($vendordetail->country) && ($key == $vendordetail->country) )
                                            selected
                                            @endif value="<?=$key?>"><?=$val?></option>
                                    @endforeach</select>
                            </div>
                            <div class="form-group">
                                <label class="label" for="state"><span>State:</span></label>
                                <select name="state" id="state_yo"
                                    class="form-control" required><option value="">Select State</option></select>
                            </div>
                            <div class="form-group">
                                <label class="label" for="zipcode"><span>Zip:</span></label>
                                <input name="zip" id="zip" placeholder="Zip Code" value="" type="text"
                                    class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label class="label" for="personalnotes"><span>Personal Notes:</span></label>
                                <textarea id="personalnotes" placeholder="Personal Notes" class="form-control"
                                    name="personalnotes"></textarea>
                            </div>
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="Submit" class="btn btn-default">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- footer end -->


  



    <!-- tap to top -->
    <div class="tap-top top-cls">
        <div><i class="fa fa-angle-double-up"></i></div>
    </div>
    <!-- tap to top end -->
    <script src="{{ asset('frontend/assets/js/jquery-3.3.1.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="{{ asset('frontend/assets/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/slick.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/menu.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/lazysizes.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/price-range.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/bootstrap.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/bootstrap-notify.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('frontend/assets/js/script.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/common.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


    <script type="text/javascript">
    $(".jqueryValidate").validate();

    function googleTranslateElementInit() {
        var currLang = '{{App::getLocale()}}';
        var availLang = '{{$langStrForFooter}}';
        if (currLang != null)
            setCookie('googtrans', '/en/' + currLang, 1);

        new google.translate.TranslateElement({
            pageLanguage: 'en',
            includedLanguages: availLang
        }, 'google_translate_element');
    }

    function setCookie(key, value, expiry) {
        var expires = new Date();
        expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
    }
    $(document).on('change', '.goog-te-combo', function() {
        //window.location.href = base_url + "/lang/" + $(this).val();
        $.ajax({
            type: 'GET',
            url: base_url + "/lang/" + $(this).val(),
            data: {},
            success: function(data) {
            }
        });
    });
    </script>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit">
    </script>
</body>

</html>