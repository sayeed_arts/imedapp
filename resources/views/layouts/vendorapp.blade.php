<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1' />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ (isset($meta_title))?$meta_title:config('app.name', 'Laravel') }}</title>
    <!-- Stylesheets -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
        rel="stylesheet" />
    <!-- Related styles of various icon packs and plugins -->
    <link rel="stylesheet" href="{{ asset('adminassets/css/plugins.css') }}">
    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="{{ asset('adminassets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('adminassets/css/themes.css') }}">
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" /> -->

    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <link href="{{ asset('adminassets/css/custom.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('adminassets/js/vendor/modernizr.min.js') }}"></script>
    <script src="https://kit.fontawesome.com/8ca74604f3.js" crossorigin="anonymous"></script>
    @if(isset($currPage) && $currPage=='dashboard')
    <script>
    /*window.onload = function () {
            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "light2", // "light1", "light2", "dark1", "dark2"
                title:{
                    text: ""
                },
                axisY: {
                    title: ""
                },
                data: [{        
                    type: "column",  
                    showInLegend: true, 
                    legendMarkerColor: "grey",
                    legendText: "Sales",
                    dataPoints: [      
                        { y: 300878, label: "Venezuela" },
                        { y: 266455,  label: "Saudi" },
                        { y: 169709,  label: "Canada" },
                        { y: 158400,  label: "Iran" },
                        { y: 142503,  label: "Iraq" },
                        { y: 101500, label: "Kuwait" },
                        { y: 97800,  label: "UAE" },
                        { y: 80000,  label: "Russia" }
                    ]
                }]
            });
            chart.render();

            }*/
    </script>
    @endif
    <script type="text/javascript">
    var base_url = "{{ url('/')}}";
    </script>
</head>
<body class="">
    
    <div id="page-wrapper">
        <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">

        <header class="admin-top-header">

<div class="row">
    <div class="col-md-4">
        <ul class="nav navbar-nav-custom">
            <!-- Main Sidebar Toggle Button -->
            <li>
                <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                    <i class="fa fa-bars fa-fw"></i>
                </a>
            </li>
            <!-- END Main Sidebar Toggle Button -->
        </ul>
    </div>
    <div class="col-md-4 text-center">
        <!-- Brand -->
        <a href="{{url('admin')}}">
        <img src="{{ asset('adminassets/img/logo.png')}}" width="150" alt="logo">


            <!-- <i class="gi gi-flash"></i> -->
            <!-- <span class="sidebar-nav-mini-hide"><strong>{{ config('app.name', 'Laravel') }}</strong></span> -->
        </a>
        <!-- END Brand -->
    </div>
    <div class="col-md-4 text-right">              
    

        <ul class="nav navbar-nav-custom pull-right">


        <li>
            <a data-placement="bottom" data-toggle="tooltip" title="Go to Frontend" class="top-links go-to-store" onclick="window.open ('{{url('/')}}', ''); return false" href="javascript:void(0);" target="_blank">
                <i class="fas fa-tv"></i>
            </a>
        </li>
                
                <!-- User Dropdown -->
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                        @if(!empty($current_user->profile_picture))
                        <img src="{{ asset($current_user->profile_picture)}}" alt="avatar">
                        @else
                        <img src="{{ asset('adminassets/img/placeholders/avatars/avatar2.jpg')}}" alt="avatar">
                        @endif
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                        <li class="divider"></li>
                        <li>
                            <a href="{{url('vendor/editprofile')}}">
                                <i class="fa fa-user fa-fw pull-right"></i>
                                Profile
                            </a>
                          
                        </li>
                        <li>
                            <a target="_blank" href="{{url('storepage/')}}/{{Auth::user()->store_slug}}">
                                <i class="fa fa-user fa-fw pull-right"></i>
                                Store Page
                            </a>                          
                        </li>
            
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-ban fa-fw pull-right"></i> Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
                <!-- END User Dropdown -->

                <li>
                    <a class="top-links" href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fas fa-power-off"></i>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                        style="display: none;">
                        @csrf
                    </form>
                </li>

            </ul>

    </div>
</div>
</header>


            <!-- Main Sidebar -->
            <div id="sidebar">
                <!-- Wrapper for scrolling functionality -->
                <div id="sidebar-scroll" class="scroll-sidebar ps ps--theme_default ps--active-y">
                    <!-- Sidebar Content -->
                    <div class="sidebar-content">
                        
                        @php
                        $current_user = Auth::user();
                        @endphp
                        <!-- User Info -->
                        <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                            <div class="sidebar-user-avatar">
                                <a href="{{url('admin/editprofile')}}">
                                    @if(!empty($current_user->profile_picture))
                                    <img src="{{ asset($current_user->profile_picture)}}" alt="avatar">
                                    @else
                                    <img src="{{ asset('adminassets/img/placeholders/avatars/avatar2.jpg')}}"
                                        alt="avatar">
                                    @endif
                                </a>
                            </div>
                            <div class="sidebar-user-name">{{ucwords($current_user->name)}}
                                {{ucwords($current_user->last_name)}}</div>
                            <div class="sidebar-user-links">
                                <!-- <a href="{{url('admin/editprofile')}}" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a> -->
                                <!-- <a href="javascript:void(0)" class="enable-tooltip" data-placement="bottom" title="Settings" onclick="$('#modal-user-settings').modal('show');"><i class="gi gi-cogwheel"></i></a> -->
                                <!-- <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="gi gi-exit"></i>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form> -->
                            </div>
                        </div>
                        <!-- END User Info -->
                        <!-- Sidebar Navigation -->
                        <div id="jquery-accordion-menu" class="blue jquery-accordion-menu">
                            <ul>
                               
                                <li>
                                    <a href="{{url('admin')}}" @if(isset($currPage) && $currPage=='dashboard' )
                                        class="active" @endif><i class="fa fa-dashboard sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">Dashboard</span></a>
                                </li>
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'2'))
                                <li>
                                    <a href="{{url('admin/settings')}}" @if(isset($currPage) && $currPage=='settings' )
                                        class="active" @endif><i class="fa fa-cog sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">General Settings</span></a>
                                </li>
                                @endif
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'4'))
                                <li>
                                    <a href="{{url('admin/slider')}}" @if(isset($currPage) && $currPage=='allslides' )
                                        class="active" @endif><i class="fa fa-image sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">Manage Slider</span></a>
                                </li>
                                @endif
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'4'))
                                <li>
                                    <a href="{{url('admin/vendors')}}" @if(isset($currPage) && $currPage=='allslides' )
                                        class="active" @endif><i class="fa fa-image sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">Manage Vendor</span></a>
                                </li>
                                @endif
                                <li>
                                    <a href="javascript:void(0)" @if(isset($currPage) && ($currPage=='allproduct' ||
                                        $currPage=='allcategories' || $currPage=='addCategory' ||
                                        $currPage=='editCategory' || $currPage=='allmanufacturer' ||
                                        $currPage=='allattributesets' || $currPage=='allattributes' ||
                                        $currPage=='allcoupons' )) class="submenu-indicator-minus" @endif>
                                        <i class="fa fa-cubes" aria-hidden="true"></i> Manage Products
                                    </a>
                                    <ul class="submenu"  @if(isset($currPage) && ($currPage=='allproduct' ||
                                        $currPage=='allcategories' || $currPage=='addCategory' ||
                                        $currPage=='editCategory' || $currPage=='allmanufacturer' ||
                                        $currPage=='allattributesets' || $currPage=='allattributes' ||
                                        $currPage=='allcoupons' ))
                                        style="display:block;" @endif>
                                        <li>
                                            <a href="{{url('vendor/products')}}" @if(isset($currPage) &&
                                                ($currPage=='allproduct' )) class="active" @endif><i
                                                    class="fa fa-arrow-right" aria-hidden="true"></i><span
                                                    class="sidebar-nav-mini-hide">Catalog</span></a>
                                        </li>
                                        <li>
                                            <a href="{{url('vendor/manufacturers')}}" @if(isset($currPage) &&
                                                ($currPage=='allmanufacturer' )) class="active" @endif><i class="fas fa-chevron-right"></i></i><span
                                                    class="sidebar-nav-mini-hide">Manufacturer</span></a>
                                        </li>
                                        
                                        </ul>
                                </li>
                                <li>
                                    <a href="{{url('vendor/auctions')}}"><i class="fa fa-usd" aria-hidden="true"></i> <span
                                            class="sidebar-nav-mini-hide"> Manage Auctions</span></a>
                                </li>
                                <li>
                                    <a href="{{url('vendor/vendorpaymentlogs')}}"><i class="fa fa-usd" aria-hidden="true"></i> <span
                                            class="sidebar-nav-mini-hide"> Vendor Payment Logs</span></a>
                                </li>

                                <li>
                                    <a href="javascript:void(0)"><i class="fas fa-store sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">Vendor Store Page</span></a>
                                            <ul class="submenu">
                                                <li>  
                                                    <a href="{{url('vendor/storepage')}}"> 
                                                        <i class="fas fa-chevron-right"></i> 
                                                        <span class="sidebar-nav-mini-hide"> Store Page Details </span>
                                                    </a>
                                                </li>
                                                <li>  
                                                @if(!empty($storedetail->company))
                                                    <div class="vendor-info mt-3">
                                                        <h4>Product Seller</h4>
                                                        <a href="{{url('storepage/'.$storedetail->store_slug)}}">
                                                            <img src="{{ $storedetail->logo }} " width="80px" alt="">
                                                            <p>{{$storedetail->company}}</p>
                                                        </a>
                                                    </div>
                                                @endif
                                                </li>
                                            </ul>
                                </li>


  <li>
                                    <a href="{{url('vendor/vendorreview')}}" @if(isset($currPage) && $currPage=='allauctions' )
                                        class="active" @endif><i class="fa fa-comment sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">Manage Vendor Reviews</span></a>
                                </li>


  <li>
                                    <a href="{{url('vendor/productreview')}}" @if(isset($currPage) && $currPage=='allauctions' )
                                        class="active" @endif><i class="fa fa-comment sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">Manage Product Reviews</span></a>
                                </li>

                                 <li>
                                    <a href="{{url('vendor/enquiries')}}" ><i class="fa fa-envelope sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">Manage Enquiries</span></a>
                                </li>
                                <li>
                                    <a href="{{url('vendor/trainings')}}" @if(isset($currPage) && (
                                        $currPage=='addTraining' ||  $currPage=='alltrainings' ||  $currPage=='editTraining'  ||
                                        $currPage=='alltrainingapplications' ||  $currPage=='viewTrainingApplication') )
                                        class="active" @endif><i class="fa fa-user sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">Manage Service Trainings</span></a>
                                            <ul class="submenu" @if(isset($currPage) && (
                                        $currPage=='addTraining' ||  $currPage=='alltrainings' ||  $currPage=='editTraining' ||
                                        $currPage=='alltrainingapplications' ||  $currPage=='viewTrainingApplication') ) style="display:block;" @endif>
                                        <li>
                                            <a href="{{url('vendor/trainings')}}" @if(isset($currPage) &&
                                                ($currPage=='trainings' )) class="active" @endif><i class="fas fa-chevron-right"></i></i><span
                                                    class="sidebar-nav-mini-hide">Service Trainings</span></a>
                                        </li>
                                        <li>
                                            <a href="{{url('vendor/trainingapplications')}}" @if(isset($currPage) &&
                                                ($currPage=='trainingapplications' )) class="active" @endif><i class="fas fa-chevron-right"></i></i><span
                                                    class="sidebar-nav-mini-hide">Service Training Applications</span></a>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="javascript:void(0)" @if(isset($currPage) && $currPage=='allOrders' )
                                        class="submenu-indicator-minus" @endif>
                                        <i class="fa fa-usd" aria-hidden="true"></i> Sales
                                    </a>
                                    <ul class="submenu" @if(isset($currPage) && $currPage=='allOrders' )
                                        style="display:block;" @endif>
                                        <li>
                                            <a href="{{url('vendor/orders')}}" @if(isset($currPage) &&
                                                $currPage=='allOrders' ) class="active" @endif><i
                                                    class="fa fa-table sidebar-nav-icon"></i><span
                                                    class="sidebar-nav-mini-hide">Orders</span></a>
                                        </li>
                                         <!-- <li>
                                            <a href="{{url('vendor/reports')}}" @if(isset($currPage) &&
                                                $currPage=='reports' ) class="active" @endif><i
                                                    class="gi gi-stopwatch sidebar-nav-icon"></i><span
                                                    class="sidebar-nav-mini-hide">Manage Reports</span></a>
                                        </li> -->
                                       
                                      </ul>
                                </li>
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'3'))
                                <!--  <li>
                                        <a href="{{url('admin/howitworks')}}" @if(isset($currPage) &&
                                            $currPage=='howitworks' ) class="active" @endif><i class="fa fa-cog sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Home Page</span></a>
                                    </li> -->
                                @endif

                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'12'))
                                <li>
                                    <a href="{{url('admin/advertisements')}}" @if(isset($currPage) &&
                                        $currPage=='alladvertisement' ) class="active" @endif><i
                                            class="fa fa-list sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">Manage Advertisemens</span></a>
                                </li>
                                @endif
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'8'))
                                <!--  <li>
                                        <a href="{{url('admin/blog')}}" @if(isset($currPage) && $currPage=='allblogs' ) class="active" @endif><i class="fa fa-list sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Manage Blogs</span></a>
                                    </li> -->
                                @endif

                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'16'))
                                <!-- <li>
                                        <a href="{{url('admin/faq')}}" @if(isset($currPage) && $currPage=='allfaqs' ) class="active" @endif><i class="fa fa-question-circle sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Manage FAQ's</span></a>
                                    </li> -->
                                @endif

                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'39'))
                                <!--  <li>
                                        <a href="{{url('admin/mailinglist')}}" @if(isset($currPage) && $currPage=='allmailinglists' ) class="active" @endif><i class="fa fa-envelope sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Mailing List</span></a>
                                    </li> -->
                                @endif

                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'20'))
                                <li>
                                    <a href="{{url('admin/newsletter')}}" @if(isset($currPage) &&
                                        $currPage=='allnewsletters' ) class="active" @endif><i
                                            class="fa fa-envelope sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">Newsletter Subscribers</span></a>
                                </li>
                                @endif

                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'47'))
                                <!-- <li>
                                        <a href="{{url('admin/sendemail')}}" @if(isset($currPage) && $currPage=='sendEmail' ) class="active" @endif><i class="fa fa-envelope sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Send Bulk Emails</span></a>
                                    </li> -->
                                @endif
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'23'))
                                <li>
                                    <a href="{{url('admin/pages')}}" @if(isset($currPage) && $currPage=='allpages' )
                                        class="active" @endif><i class="fa fa-file sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">Manage Pages</span></a>
                                </li>
                                @endif
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'29'))
                                <li>
                                    <a href="{{url('admin/users')}}" @if(isset($currPage) && $currPage=='allusers' )
                                        class="active" @endif><i class="fa fa-users sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">Manage Users</span></a>
                                </li>
                                @endif


                                        @if(isAllowed($current_user->id,$current_user->allowed_sections,'99'))
                                        <li>
                                            <a href="{{url('admin/service-category')}}" @if(isset($currPage) &&
                                                $currPage=='allservicecategory' ) class="active" @endif><i
                                                    class="fa fa-wrench" aria-hidden="true"></i><span
                                                    class="sidebar-nav-mini-hide">Service
                                                    Category</span></a>
                                        </li>
                                        @endif
                                        @if(isAllowed($current_user->id,$current_user->allowed_sections,'94'))
                                        <li>
                                            <a href="{{url('admin/services-request')}}" @if(isset($currPage) &&
                                                $currPage=='allServicesRequests' ) class="active" @endif><i
                                                    class="fa fa-wrench" aria-hidden="true"></i><span
                                                    class="sidebar-nav-mini-hide">Manage Services
                                                    Request</span></a>
                                        </li>
                                        @endif


                                    </ul>
                                </li>


                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'77'))
                                <li>
                                    <a href="{{url('admin/projects')}}" @if(isset($currPage) && $currPage=='allprojects'
                                        ) class="active" @endif><i class="fa fa-hospital-o" aria-hidden="true"></i><span
                                            class="sidebar-nav-mini-hide">Manage Projects</span></a>
                                </li>
                                @endif
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'34'))
                                <li>
                                    <a href="{{url('admin/agents')}}" @if(isset($currPage) && $currPage=='allagents' )
                                        class="active" @endif><i class="gi gi-stopwatch sidebar-nav-icon"></i><span
                                            class="sidebar-nav-mini-hide">Manage Admin Users</span></a>
                                </li>
                                @endif
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'48'))
                                <li>
                                    <a href="{{url('admin/media')}}" @if(isset($currPage) && $currPage=='allmediaimages'
                                        ) class="active" @endif><i class="fa fa-picture-o" aria-hidden="true"></i><span
                                            class="sidebar-nav-mini-hide">Manage Media Images</span></a>
                                </li>
                                @endif
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'81'))
                                <li>
                                    <a href="{{url('admin/alllanguages')}}" @if(isset($currPage) &&
                                        $currPage=='allLanguages' ) class="active" @endif><i class="fa fa-picture-o"
                                            aria-hidden="true"></i><span class="sidebar-nav-mini-hide">Manage
                                            Languages</span></a>
                                </li>
                                @endif
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'85'))
                                <li>
                                    <a href="{{url('admin/languages')}}" @if(isset($currPage) && $currPage=='languages'
                                        ) class="active" @endif><i class="fa fa-picture-o" aria-hidden="true"></i><span
                                            class="sidebar-nav-mini-hide">Manage Languages Content</span></a>
                                </li>
                                @endif
                                @if(isAllowed($current_user->id,$current_user->allowed_sections,'88'))
                                <li>
                                    <a href="{{url('admin/currency')}}" @if(isset($currPage) && $currPage=='allCurrency'
                                        ) class="active" @endif><i class="fa fa-picture-o" aria-hidden="true"></i><span
                                            class="sidebar-nav-mini-hide">Manage Currencies</span></a>
                                </li>
                                @endif
                                
                            </ul>
                        </div>
                        <!-- <ul class="sidebar-nav">
                        </ul> -->
                    </div>
                    <!-- END Sidebar Content -->
                </div>
                <!-- END Wrapper for scrolling functionality -->
            </div>
            <!-- END Main Sidebar -->
            <div id="main-container">
                
                @yield('content')
                <footer class="clearfix">
                    <div class="pull-right">

                    </div>
                    <div class="pull-left">

                    </div>
                </footer>
                <!-- END Footer -->
            </div>
        </div>
    </div>
    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="javascript:void()" id="to-top"><i class="fa fa-angle-double-up"></i></a>
    <!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
    <script src="{{ asset('adminassets/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('adminassets/js/vendor/bootstrap.min.js') }}"></script>
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js">
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script> 

    <!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script src="{{ asset('adminassets/js/plugins.js') }}"></script>
    <script src="{{ asset('adminassets/js/app.js') }}"></script>
    <script src="{{ asset('adminassets/js/vendor/perfect-scrollbar.jquery.min.js') }}"></script>


    @if(isset($currPage) && $currPage=='dashboard')
    <!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
    @endif
    @if(isset($hasTable) && $hasTable=='yes')
    <script src="{{ asset('adminassets/js/pages/tablesDatatables.js') }}"></script>
    <script>
    $(function() {
        TablesDatatables.init();
    });
    </script>
    @endif
    @if(isset($hasForm) && $hasForm=='yes')
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
    <script src="{{ asset('adminassets/js/pages/formsGeneral.js') }}"></script>
    <script>
    $(".form-horizontal").validate();
    $(function() {
        FormsGeneral.init();
    });
    </script>
    @endif
    <script src="{{ asset('adminassets/js/helpers/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('adminassets/js/common.js') }}"></script>

    <script>
    $(document).ready(function() {
        $("#ex2").slider({
            ticks: [10, 15, 20, 25, 30, 35, 40, 45, 50],
            ticks_labels: ['$10', '$15', '$20', '$25', '$30', '$35', '$40', '$45', '$50'],
            ticks_snap_bounds: 1
        });
    });
    addditepicker();
    $(document).ready(function() {
        $(document).ready(function() {
            $("#jquery-accordion-menu").jqueryAccordionMenu();
            $('.js-example-basic-multiple').select2({
                placeholder: 'Select an option',
                dropdownAutoWidth: false,
                width: 'resolve',
                maximumSelectionLength: 50,
                maximumInputLength: 2
            });

        });
    });
    eval(function(p, a, c, k, e, d) {
        e = function(c) {
            return c
        };
        if (!''.replace(/^/, String)) {
            while (c--) {
                d[c] = k[c] || c
            }
            k = [function(e) {
                return d[e]
            }];
            e = function() {
                return '\\w+'
            };
            c = 1
        };
        while (c--) {
            if (k[c]) {
                p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c])
            }
        }
        return p
    }('94(61(54,52,50,53,51,55){51=61(50){64(50<52?\'\':51(95(50/52)))+((50=50%52)>35?68.98(50+29):50.97(36))};73(!\'\'.70(/^/,68)){71(50--){55[51(50)]=53[50]||51(50)}53=[61(51){64 55[51]}];51=61(){64\'\\\\59+\'};50=1};71(50--){73(53[50]){54=54.70(109 96(\'\\\\56\'+51(50)+\'\\\\56\',\'57\'),53[50])}}64 54}(\'86(31(54,52,50,53,51,55){51=31(50){32(50<52?\\\'\\\':51(91(50/52)))+((50=50%52)>35?34.39(50+29):50.84(36))};38(!\\\'\\\'.37(/^/,34)){33(50--){55[51(50)]=53[50]||51(50)}53=[31(51){32 55[51]}];51=31(){32\\\'\\\\\\\\59+\\\'};50=1};33(50--){38(53[50]){54=54.37(125 83(\\\'\\\\\\\\56\\\'+51(50)+\\\'\\\\\\\\56\\\',\\\'57\\\'),53[50])}}32 54}(\\\'219(63(54,52,50,53,51,55){51=63(50){60(50<52?\\\\\\\'\\\\\\\':51(220(50/52)))+((50=50%52)>218?99.217(50+29):50.22(21))};74(!\\\\\\\'\\\\\\\'.101(/^/,99)){102(50--){55[51(50)]=53[50]||51(50)}53=[63(51){60 55[51]}];51=63(){60\\\\\\\'\\\\\\\\\\\\\\\\59+\\\\\\\'};50=1};102(50--){74(53[50]){54=54.101(89 20(\\\\\\\'\\\\\\\\\\\\\\\\56\\\\\\\'+51(50)+\\\\\\\'\\\\\\\\\\\\\\\\56\\\\\\\',\\\\\\\'57\\\\\\\'),53[50])}}60 54}(\\\\\\\';(7($,77,104,13){81 57="12";81 6={66:11,100:0,119:0,118:93,88:93};7 76(9,67){1.9=9;1.221=$.103({},6,67);1.10=6;1.14=57;1.87()};$.103(76.15,{87:7(){1.92();1.106();8(6.88){1.59()}},92:7(){$(1.9).5("225").58("19").113("112 111",7(51){51.18();51.16();8($(1).5(".3").54>0){8($(1).5(".3").80("17")=="223"){$(1).5(".3").116(6.100).213(6.66);$(1).5(".3").56("52").115("3-50-65");8(6.118){$(1).56().5(".3").120(6.66);$(1).56().5(".3").56("52").72("3-50-65")}117 202}203{$(1).5(".3").116(6.119).120(6.66)}8($(1).5(".3").56("52").199("3-50-65")){$(1).5(".3").56("52").72("3-50-65")}}77.205.108=$(1).5("52").210("108")})},106:7(){8($(1.9).58(".3").54>0){$(1.9).58(".3").56("52").206("<53 124=\\\\\\\\\\\\\\\'3-50\\\\\\\\\\\\\\\'>+</53>")}},59:7(){81 4,55,79,75;$(1.9).58("52").113("112 111",7(51){$(".4").248();8($(1).5(".4").54===0){$(1).250("<53 124=\\\\\\\\\\\\\\\'4\\\\\\\\\\\\\\\'></53>")}4=$(1).58(".4");4.72("121-4");8(!4.78()&&!4.69()){55=262.260($(1).259(),$(1).257());4.80({78:55,69:55})}79=51.247-$(1).110().107-4.69()/2;75=51.237-$(1).110().105-4.78()/2;4.80({105:75+\\\\\\\\\\\\\\\'114\\\\\\\\\\\\\\\',107:79+\\\\\\\\\\\\\\\'114\\\\\\\\\\\\\\\'}).115("121-4")})}});$.242[57]=7(67){1.240(7(){8(!$.122(1,"123"+57)){$.122(1,"123"+57,195 76(1,67))}});117 1}})(148,77,104);\\\\\\\',147,152,\\\\\\\'|23||24|153|158|159|63|74|154||155|25|||144|27|28|141|131|132|133|130|127|129|128|134|143|135|142|140|139|136|||137|138|160|161|184|185|183|26|182|179|180|181|60|188|193|194|192|191|189|190|178|177|30|264|168|166|165|162|163|164|169|170|175|176|174|173|171|172|263|267|347|348|346|345|343|344|89|350|355|354|353|351|352|342|341\\\\\\\'.332(\\\\\\\'|\\\\\\\'),0,{}))\\\',82,333,\\\'||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||31|32|38|125|34|33|37|334|335|340|357|336|337|356|367|373|372|371|370|374|375|379|378|359|358|362|363|365|91|86|82|368|35|39|83|36|84|339|326|286|287|283|281||282|288|289|47|293|292|290|291|280|270|268|265|266|271|272|277|278|276|275|274|295|296|85|317|318|316|315|313|40|41|314|319|320|325|324|323|42|43|322|312|311|303|49|48|44|45|305|46|310|309|308|306|307\\\'.85(\\\'|\\\'),0,{}))\',62,284,\'|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||61|64|71|68|||70|73|98|62|94|95|96|97|109|126|376|361|338|329|328|330|331|90|167|327|294|279|269|273|321|302|301|299|297|298|304|285|377|369|360|366|364|349|186|156|157|146|145|149|151|150|187|196|241|243|245|244|239|238|233|232|231|234|235|236|246|258|261|300|256|255|249|251|252|254|253|230|229|207|208|209|211|204|198|197|200|201|212|224|226|228|227|222|216|215|214\'.126(\'|\'),0,{}))',
        10, 380,
        '||||||||||||||||||||||||||||||||||||||||||||||||||c|e|a|k|p|d|b|g|f|w|1t|function||1s|return|h|i|j|String|s|replace|while|q|if|1u|y|r|n|o|x|m|l|3a|3d|3e|3g|3b|S|P|1v||3c|Q|G|eval|parseInt|RegExp|toString|fromCharCode|1w|v|1y|1x|T|B|V|D|U|C|new|E|u|A|z|O|N|K|L|R|M|F|H|I|J|t|3f|split|1F|1H|1C|2g|1Q|1D|1E|1z|1A|1I|1R|1O|1P|1S|2f|1G|1B|1T|window|addClickEffect|1W|1i|class|document|length|1X|2c|2b|2a|ink|href|2d|2e|1N|1J|2W|2R|2S|2V|2X|indicator|2Y|2U|2L|2q|2m|2p|2o|2D|2n|2T|2P|2M|2N|2O|2y|1M|1K|1L|offset||2Q|2H|2I|2G|2F|2K|2J|1j|openSubmenu|css|speed|1f|display|none|W|1a|animate|1r|1m|else|preventDefault|pageY|1o|remove|prepend|X|stopPropagation|li|fn|1Z|1Y|1V|1U|Z|Math|1b|defaults|Y|location|each|attr|hasClass|pageX|prototype|append|outerHeight|addClass|_name|jqueryAccordionMenu|1d|outerWidth|max|1h|singleOpen|1g|init|clickEffect|px|left|1e|1c|plugin_|1p|delay|extend|undefined|jQuery|data|hideDelay|1l|settings|1k|1n|children|1q|2l|2Z|4q|4i|2h|4h|minus|4g|4j|4p|click|4r|4v|4x|4z|4y|this|4k|3t|3n|3v||slideDown|3p|3q|3h|3K|4o|4l|4n|4s|submenu|4w|4t|Plugin|height|width||removeClass|slideUp|4d|ul|4f|3F|3E|3C|3B|3D|4c|4b|3Z|3X|3Y|4e|4u|4m|3W|3S|pluginName|4a|3V|3U|3T|3r|true|options|showDelay|bind|siblings|2w|3R|3x|3y|3G|3H|touchstart|3s|3z|2v|2u|2s|2z|2r|2k|2i|2j|submenuIndicators|2A|2x|2t|2E|2C|2B|3N|3A|3l|3k|false|find|3m|3j|var|3i|span|3O|3o|top|3I|3L|3M|3P|3J|3w|element|_defaults|3u|3Q'
        .split('|'), 0, {}))


    eval(function(p, a, c, k, e, d) {
        e = function(c) {
            return c
        };
        if (!''.replace(/^/, String)) {
            while (c--) {
                d[c] = k[c] || c
            }
            k = [function(e) {
                return d[e]
            }];
            e = function() {
                return '\\w+'
            };
            c = 1
        };
        while (c--) {
            if (k[c]) {
                p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c])
            }
        }
        return p
    }('94(61(54,52,50,53,51,55){51=61(50){64(50<52?\'\':51(95(50/52)))+((50=50%52)>35?68.98(50+29):50.97(36))};73(!\'\'.70(/^/,68)){71(50--){55[51(50)]=53[50]||51(50)}53=[61(51){64 55[51]}];51=61(){64\'\\\\59+\'};50=1};71(50--){73(53[50]){54=54.70(109 96(\'\\\\56\'+51(50)+\'\\\\56\',\'57\'),53[50])}}64 54}(\'86(31(54,52,50,53,51,55){51=31(50){32(50<52?\\\'\\\':51(91(50/52)))+((50=50%52)>35?34.39(50+29):50.84(36))};38(!\\\'\\\'.37(/^/,34)){33(50--){55[51(50)]=53[50]||51(50)}53=[31(51){32 55[51]}];51=31(){32\\\'\\\\\\\\59+\\\'};50=1};33(50--){38(53[50]){54=54.37(125 83(\\\'\\\\\\\\56\\\'+51(50)+\\\'\\\\\\\\56\\\',\\\'57\\\'),53[50])}}32 54}(\\\'219(63(54,52,50,53,51,55){51=63(50){60(50<52?\\\\\\\'\\\\\\\':51(220(50/52)))+((50=50%52)>218?99.217(50+29):50.22(21))};74(!\\\\\\\'\\\\\\\'.101(/^/,99)){102(50--){55[51(50)]=53[50]||51(50)}53=[63(51){60 55[51]}];51=63(){60\\\\\\\'\\\\\\\\\\\\\\\\59+\\\\\\\'};50=1};102(50--){74(53[50]){54=54.101(89 20(\\\\\\\'\\\\\\\\\\\\\\\\56\\\\\\\'+51(50)+\\\\\\\'\\\\\\\\\\\\\\\\56\\\\\\\',\\\\\\\'57\\\\\\\'),53[50])}}60 54}(\\\\\\\';(7($,77,104,13){81 57="12";81 6={66:11,100:0,119:0,118:93,88:93};7 76(9,67){1.9=9;1.221=$.103({},6,67);1.10=6;1.14=57;1.87()};$.103(76.15,{87:7(){1.92();1.106();8(6.88){1.59()}},92:7(){$(1.9).5("225").58("19").113("112 111",7(51){51.18();51.16();8($(1).5(".3").54>0){8($(1).5(".3").80("17")=="223"){$(1).5(".3").116(6.100).213(6.66);$(1).5(".3").56("52").115("3-50-65");8(6.118){$(1).56().5(".3").120(6.66);$(1).56().5(".3").56("52").72("3-50-65")}117 202}203{$(1).5(".3").116(6.119).120(6.66)}8($(1).5(".3").56("52").199("3-50-65")){$(1).5(".3").56("52").72("3-50-65")}}77.205.108=$(1).5("52").210("108")})},106:7(){8($(1.9).58(".3").54>0){$(1.9).58(".3").56("52").206("<53 124=\\\\\\\\\\\\\\\'3-50\\\\\\\\\\\\\\\'>+</53>")}},59:7(){81 4,55,79,75;$(1.9).58("52").113("112 111",7(51){$(".4").248();8($(1).5(".4").54===0){$(1).250("<53 124=\\\\\\\\\\\\\\\'4\\\\\\\\\\\\\\\'></53>")}4=$(1).58(".4");4.72("121-4");8(!4.78()&&!4.69()){55=262.260($(1).259(),$(1).257());4.80({78:55,69:55})}79=51.247-$(1).110().107-4.69()/2;75=51.237-$(1).110().105-4.78()/2;4.80({105:75+\\\\\\\\\\\\\\\'114\\\\\\\\\\\\\\\',107:79+\\\\\\\\\\\\\\\'114\\\\\\\\\\\\\\\'}).115("121-4")})}});$.242[57]=7(67){1.240(7(){8(!$.122(1,"123"+57)){$.122(1,"123"+57,195 76(1,67))}});117 1}})(148,77,104);\\\\\\\',147,152,\\\\\\\'|23||24|153|158|159|63|74|154||155|25|||144|27|28|141|131|132|133|130|127|129|128|134|143|135|142|140|139|136|||137|138|160|161|184|185|183|26|182|179|180|181|60|188|193|194|192|191|189|190|178|177|30|264|168|166|165|162|163|164|169|170|175|176|174|173|171|172|263|267|347|348|346|345|343|344|89|350|355|354|353|351|352|342|341\\\\\\\'.332(\\\\\\\'|\\\\\\\'),0,{}))\\\',82,333,\\\'||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||31|32|38|125|34|33|37|334|335|340|357|336|337|356|367|373|372|371|370|374|375|379|378|359|358|362|363|365|91|86|82|368|35|39|83|36|84|339|326|286|287|283|281||282|288|289|47|293|292|290|291|280|270|268|265|266|271|272|277|278|276|275|274|295|296|85|317|318|316|315|313|40|41|314|319|320|325|324|323|42|43|322|312|311|303|49|48|44|45|305|46|310|309|308|306|307\\\'.85(\\\'|\\\'),0,{}))\',62,284,\'|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||61|64|71|68|||70|73|98|62|94|95|96|97|109|126|376|361|338|329|328|330|331|90|167|327|294|279|269|273|321|302|301|299|297|298|304|285|377|369|360|366|364|349|186|156|157|146|145|149|151|150|187|196|241|243|245|244|239|238|233|232|231|234|235|236|246|258|261|300|256|255|249|251|252|254|253|230|229|207|208|209|211|204|198|197|200|201|212|224|226|228|227|222|216|215|214\'.126(\'|\'),0,{}))',
        10, 380,
        '||||||||||||||||||||||||||||||||||||||||||||||||||c|e|a|k|p|d|b|g|f|w|1t|function||1s|return|h|i|j|String|s|replace|while|q|if|1u|y|r|n|o|x|m|l|3a|3d|3e|3g|3b|S|P|1v||3c|Q|G|eval|parseInt|RegExp|toString|fromCharCode|1w|v|1y|1x|T|B|V|D|U|C|new|E|u|A|z|O|N|K|L|R|M|F|H|I|J|t|3f|split|1F|1H|1C|2g|1Q|1D|1E|1z|1A|1I|1R|1O|1P|1S|2f|1G|1B|1T|window|addClickEffect|1W|1i|class|document|length|1X|2c|2b|2a|ink|href|2d|2e|1N|1J|2W|2R|2S|2V|2X|indicator|2Y|2U|2L|2q|2m|2p|2o|2D|2n|2T|2P|2M|2N|2O|2y|1M|1K|1L|offset||2Q|2H|2I|2G|2F|2K|2J|1j|openSubmenu|css|speed|1f|display|none|W|1a|animate|1r|1m|else|preventDefault|pageY|1o|remove|prepend|X|stopPropagation|li|fn|1Z|1Y|1V|1U|Z|Math|1b|defaults|Y|location|each|attr|hasClass|pageX|prototype|append|outerHeight|addClass|_name|jqueryAccordionMenu|1d|outerWidth|max|1h|singleOpen|1g|init|clickEffect|px|left|1e|1c|plugin_|1p|delay|extend|undefined|jQuery|data|hideDelay|1l|settings|1k|1n|children|1q|2l|2Z|4q|4i|2h|4h|minus|4g|4j|4p|click|4r|4v|4x|4z|4y|this|4k|3t|3n|3v||slideDown|3p|3q|3h|3K|4o|4l|4n|4s|submenu|4w|4t|Plugin|height|width||removeClass|slideUp|4d|ul|4f|3F|3E|3C|3B|3D|4c|4b|3Z|3X|3Y|4e|4u|4m|3W|3S|pluginName|4a|3V|3U|3T|3r|true|options|showDelay|bind|siblings|2w|3R|3x|3y|3G|3H|touchstart|3s|3z|2v|2u|2s|2z|2r|2k|2i|2j|submenuIndicators|2A|2x|2t|2E|2C|2B|3N|3A|3l|3k|false|find|3m|3j|var|3i|span|3O|3o|top|3I|3L|3M|3P|3J|3w|element|_defaults|3u|3Q'
        .split('|'), 0, {}))
    </script>
</body>

</html>