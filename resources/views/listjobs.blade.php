@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>Jobs</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active">Jobs</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- About Start -->
    <div class="job-list mt-5 mb-5">
        <div class="container">        
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="job-side-block">
                                <form action="{{url('jobs')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                                    {{ csrf_field() }}
                                    <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">                                           
                                                <select name="category_id" id="category_id" class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">    
                                                    <option value="0">Category</option>
                                                    @if(!empty($allCategories))
                                                        @foreach($allCategories as $singleCategory)
                                                            <option value="{{$singleCategory->id}}" <?php if($singleCategory->id==$category_id){
                                                                            echo "selected";
                                                                            } ?>>{{$singleCategory->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <select name="location_id" id="location_id" class="form-control">
                                                <option value="0">Location</option>
                                                @if(!empty($allLocations))
                                                    @foreach($allLocations as $singleLocation)
                                                        <option value="{{$singleLocation->id}}" <?php if($singleLocation->id==$location_id){
                                                                        echo "selected";
                                                                        } ?>>{{$singleLocation->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">                                            
                                            <select name="type_id" id="type_id" class="form-control">
                                                <option value="0">Type</option>
                                                @if(!empty($allTypes))
                                                    @foreach($allTypes as $singleType)
                                                        <option value="{{$singleType->id}}" <?php if($singleType->id==$type_id){
                                                                        echo "selected";
                                                                        } ?>>{{$singleType->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-sm btn-primary">Filter</button>    
                                    </div>
                                    </div>
                                </form>
                            </div>
                            <!-- <div class="job-side-block">
                                <h3>
                                    <a href="/trainings">Service Trainings</a>
                                </h3>
                            </div> -->
                        </div>
                        <div class="col-md-9">
                            <div class="job-block">
                                @if(!empty($allRecords) && count($allRecords)>0)
                                    @foreach($allRecords as $data)
                                        @php
                                            $applylink = "/view-job/".$data->slug;
                                        @endphp
                                        <div class="job-card">
                                            <div class="job-card-header" id="headingTwo{{$data->id}}">
                                                <h2>
                                                <a href="{{url('view-job/'.$data->slug)}}"> {!!$data->name!!}</a>
                                                </h2>

                                                <div class="meta">
                                                    <span>Location: {!! $data->joblocation !!}</span> &nbsp;|&nbsp; 
                                                    <span>Experience: {!! $data->experience !!}</span>
                                                </div>

                                               

                                            </div>
                                            <div>
                                                <div class="job-card-body">
                                                    <p>{!! \Illuminate\Support\Str::limit(strip_tags($data->content), 170, $end='...') !!}</p>
                                                    <a href="{{url('view-job/'.$data->slug)}}" class="more">View Details <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach               
                                @else
                                    <p>No results found </p>    
                                @endif
                            </div>
                            <div class="product-pagination">
                                <div class="theme-paggination-block">
                                    <div class="col-xl-12 col-md-12 col-sm-12">
                                        <nav aria-label="Page navigation">
                                            {{ $allRecords->links() }}
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection