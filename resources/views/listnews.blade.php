@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>News</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active">News</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- About Start -->
    <div class="job-list mt-5 mb-5">
        <div class="container">        
            <div class="row">
                <div class="col-md-12">
                    <div class="row">                        
                        <div class="col-md-3">
                            <div class="news-side-block">
                                <h3>Categories</h3>                                                    
                                <ul>
                                    <li style="width:100%;"><a href="{{url('/news')}}">All</a></li>
                                    @if(!empty($allCategories))
                                        @foreach($allCategories as $singleCategory)
                                            <li style="width:100%;" <?php if($category_id ==$singleCategory->id) { echo 'class="active"'; } ?>><a href="{{url('news-by-category/'.$singleCategory->slug)}}">{{$singleCategory->name}}</a></li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="news-blocks">
                                @if(!empty($allRecords) && count($allRecords)>0)
                                    @foreach($allRecords as $data)
                                        <div class="news-card">
                                            <div class="thumb"><a href="{{url('news/'.$data->slug)}}"><img src="{{ asset($data->image) }}" alt="{{$data->name}}"></a></div>
                                            <div class="news-card-header" id="headingTwo{{$data->id}}">
                                                <h2>
                                                    <a href="{{url('news/'.$data->slug)}}">{!!$data->name!!}</a>                                                
                                                </h2>
                                                <h3>
                                                    <a href="{{url('news-by-category/'.$data->blogslug)}}">{!!$data->blogcategory!!}</a>                                                
                                                </h3>
                                            </div>
                                            <div>
                                                <div class="news-card-body">
                                                    <div class="date">
                                                        <span><?php echo date("j", strtotime($data->created_at))?></span>
                                                        <span><?php echo date("F Y", strtotime($data->created_at))?></span>
                                                    </div>
                                                    <p>{!! \Illuminate\Support\Str::limit(strip_tags($data->content), 270, $end='...') !!}</p>
                                                    <a href="{{url('news/'.$data->slug)}}" class="more">Read More <i class="fa fa-angle-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach               
                                @else
                                    <p>No results found </p>    
                                @endif
                            </div>
                            <div class="product-pagination">
                                <div class="theme-paggination-block">
                                    <div class="col-xl-12 col-md-12 col-sm-12">
                                        <nav aria-label="Page navigation">
                                            {{ $allRecords->links() }}
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection