@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>Service Trainings</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active">Service Trainings</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- About Start -->
    <div class="job-list mt-5 mb-5">
        <div class="container">        
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="job-side-block">
                                <h3>Categories</h3>  

                                <ul>
                                    <li style="width:100%;"><a href="{{url('/trainings')}}">All</a></li>

                                    @if(!empty($allCategories))
                                        @foreach($allCategories as $singleCategory)
                                            <li style="width:100%;" <?php if($category_id ==$singleCategory->id) { echo 'class="active"'; } ?>><a href="{{url('training-by-category/'.$singleCategory->slug)}}">{{$singleCategory->name}}</a></li>
                                        @endforeach
                                    @endif
                                
                                </ul>


                                <!-- <form action="{{url('trainings')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                                    {{ csrf_field() }}
                                    <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">                                           
                                                <select name="category_id" id="category_id" class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">    
                                                    <option value="0">Category</option>
                                                    @if(!empty($allCategories))
                                                        @foreach($allCategories as $singleCategory)
                                                            <option value="{{$singleCategory->id}}" <?php if($singleCategory->id==$category_id){
                                                                            echo "selected";
                                                                            } ?>>{{$singleCategory->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                        </div>

                                        
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-sm btn-primary">Filter</button>    
                                    </div>
                                    </div>
                                </form> -->
                            </div>
                            <!-- <div class="job-side-block">
                                <h3>
                                    <a href="/trainings">Service Trainings</a>
                                </h3>
                            </div> -->
                        </div>
                        <div class="col-md-9">
                            <div class="job-block">
                                @if(!empty($allRecords) && count($allRecords)>0)
                                    @foreach($allRecords as $data)
                                        @php
                                            $applylink = "/view-job/".$data->slug;
                                        @endphp
                                        <div class="job-card">
                                            <div class="job-card-header" id="headingTwo{{$data->id}}">
                                                <h2>
                                                <a href="{{url('view-job/'.$data->slug)}}"> {!!$data->name!!}</a>
                                                </h2>

                                                <div class="meta">
                                                    <span>Location: {!! $data->location !!}</span> &nbsp;|&nbsp; 
                                                    <span>Start Date: {!! $data->start_date !!}</span>
                                                    <span>End Date: {!! $data->closing_date !!}</span>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="job-card-body">
                                                    <p>{!! \Illuminate\Support\Str::limit(strip_tags($data->content), 170, $end='...') !!}</p>
                                                    <a href="{{url('view-training/'.$data->slug)}}" class="more">View Details <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach               
                                @else
                                    <p>No results found </p>    
                                @endif
                            </div>
                            <div class="product-pagination">
                                <div class="theme-paggination-block">
                                    <div class="col-xl-12 col-md-12 col-sm-12">
                                        <nav aria-label="Page navigation">
                                            {{ $allRecords->links() }}
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection