@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>{{$page_title}}</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$page_title}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb End -->
<!-- section start -->
<section class="wishlist-section section-b-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="account-sidebar"><a class="popup-btn">{{__('My Account')}}</a></div>
                <div class="dashboard-left">
                    <div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left"
                                aria-hidden="true"></i> {{__('Back')}}</span></div>
                    <div class="block-content">
                    <ul>
                                <li @if(isset($currPage) && $currPage=='dashboard') class="active" @endif ><a href="{{route('user')}}"> <i><img src="{{ asset('frontend/assets/images/dashboard.png') }}" alt="" srcset=""></i> <span>{{__('Dashboard')}}</span> </a></li>
                                <li @if(isset($currPage) && $currPage=='myorders') class="active" @endif ><a href="{{route('my-orders')}}"><i><img src="{{ asset('frontend/assets/images/bag.png') }}" alt="" srcset=""></i> {{__('My Orders')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='mybids') class="active" @endif ><a href="{{route('my-bids')}}"><i><img src="{{ asset('frontend/assets/images/bag.png') }}" alt="" srcset=""></i> {{__('My Bids')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='mywishlist') class="active" @endif ><a href="{{route('my-wishlist')}}"><i><img src="{{ asset('frontend/assets/images/my-wishlist.png') }}" alt="" srcset=""></i> {{__('My Wishlist')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='editprofile') class="active" @endif ><a href="{{route('edit-profile')}}"> <i><img src="{{ asset('frontend/assets/images/my-account.png') }}" alt="" srcset=""></i>{{__('My Account')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='addressbook') class="active" @endif ><a href="{{route('address-book')}}"><i><img src="{{ asset('frontend/assets/images/address.png') }}" alt="" srcset=""></i> {{__('Address Book')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='changepassword') class="active" @endif ><a href="{{route('change-password')}}"> <i><img src="{{ asset('frontend/assets/images/password.png') }}" alt="" srcset=""></i>{{__('Change Password')}}</a></li>
                                <li class="last">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                       <i><img src="{{ asset('frontend/assets/images/logout.png') }}" alt="" srcset=""></i> <span>{{ __('Log Out') }}</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="page-title">
                    <h2>{{$page_title}}</h2>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table cart-table table-responsive-xs my-orders">
                            <thead>
                                <tr class="table-head">
                                    <th scope="col">{{__('Order ID')}}</th>
                                    <th scope="col">{{__('Order Date')}}</th>
                                    <th scope="col">{{__('Total Price')}}</th>
                                    <!-- <th scope="col">{{__('Order Status')}}</th> -->
                                    <th scope="col">{{__('Order Status')}}</th>
                                    <!-- <th scope="col"></th> -->
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($myOrders) && count($myOrders)>0)
                                @foreach($myOrders as $singleOrder)
                                <tr>
                                    <td><a target="_blank"
                                            href="{{url('user/order-details/'.$singleOrder->order_id)}}">PRO-IMED-{{$singleOrder->id}}</a>
                                    </td>
                                    <td>
                                        {{displayDateMD($singleOrder->ordered_on)}}
                                    </td>
                                    <td>
                                        {{showOrderPrice($singleOrder->total,$singleOrder->currency_code,$singleOrder->exchange_rate)}}
                                    </td>
                                    <!-- <td>
                                        <div class="text-secondary">
                                            @if($singleOrder->status=='0')
                                            {{__('Pending')}}
                                             <a href="{{url('pay-now/'.$singleOrder->order_id)}}" class="btn-solid btn">Pay Now</a> 
                                            @elseif($singleOrder->status=='1')
                                            {{__('Failed')}}
                                            @elseif($singleOrder->status=='2')
                                            {{__('Successfull')}}
                                            @endif 
                                        </div>
                                    </td> -->
                                    <td>
                                        <div class="text-secondary">
                                            @if($singleOrder->status=='2')
                                            @if($singleOrder->shipping_status=='0')
                                            {{__('Received')}}
                                            @elseif($singleOrder->shipping_status=='1')
                                            {{__('Shipped')}}
                                            @elseif($singleOrder->shipping_status=='2')
                                            {{__('Voided')}}
                                            @elseif($singleOrder->shipping_status=='3')
                                            {{__('Processing')}}
                                            @elseif($singleOrder->shipping_status=='4')
                                            {{__('Completed')}}
                                            @endif
                                            @endif
                                        </div>
                                    </td>
                                    <!-- <td>
                                                    @if($singleOrder->payment_option==1)
                                                        <a href="{{url('user/submit-po/'.$singleOrder->order_id)}}">{{__('Submit PO')}}</a>
                                                    @endif
                                                </td> -->
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="5" align="center">{{__('No record available to display')}}!</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="product-pagination">
                        <div class="theme-paggination-block">
                            <div class="col-xl-12 col-md-12 col-sm-12">
                                <nav aria-label="Page navigation">
                                    {{ $myOrders->links() }}
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section end -->
@endsection