@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>{{$page_title}}</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$page_title}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb End -->
    <!-- section start -->
    <section class="wishlist-section section-b-space">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="account-sidebar"><a class="popup-btn">{{__('My Account')}}</a></div>
                    <div class="dashboard-left">
                        <div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> {{__('Back')}}</span></div>
                        <div class="block-content">
                            <ul>
                                <li @if(isset($currPage) && $currPage=='dashboard') class="active" @endif ><a href="{{route('user')}}"> <i><img src="{{ asset('frontend/assets/images/dashboard.png') }}" alt="" srcset=""></i> <span>{{__('Dashboard')}}</span> </a></li>
                                <li @if(isset($currPage) && $currPage=='myorders') class="active" @endif ><a href="{{route('my-orders')}}"><i><img src="{{ asset('frontend/assets/images/bag.png') }}" alt="" srcset=""></i> {{__('My Orders')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='mybids') class="active" @endif ><a href="{{route('my-bids')}}"><i><img src="{{ asset('frontend/assets/images/bag.png') }}" alt="" srcset=""></i> {{__('My Bids')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='mywishlist') class="active" @endif ><a href="{{route('my-wishlist')}}"><i><img src="{{ asset('frontend/assets/images/my-wishlist.png') }}" alt="" srcset=""></i> {{__('My Wishlist')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='editprofile') class="active" @endif ><a href="{{route('edit-profile')}}"> <i><img src="{{ asset('frontend/assets/images/my-account.png') }}" alt="" srcset=""></i>{{__('My Account')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='addressbook') class="active" @endif ><a href="{{route('address-book')}}"><i><img src="{{ asset('frontend/assets/images/address.png') }}" alt="" srcset=""></i> {{__('Address Book')}}</a></li>
                                <li @if(isset($currPage) && $currPage=='changepassword') class="active" @endif ><a href="{{route('change-password')}}"> <i><img src="{{ asset('frontend/assets/images/password.png') }}" alt="" srcset=""></i>{{__('Change Password')}}</a></li>
                                <li class="last">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                       <i><img src="{{ asset('frontend/assets/images/logout.png') }}" alt="" srcset=""></i> <span>{{ __('Log Out') }}</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="page-title">
                        <h2>{{$page_title}}</h2>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            @if (Session::has('message'))
                                {!! successMesaage(Session::get('message')) !!}   
                            @endif 
                            @if (Session::has('error'))
                                {!! errorMesaage(Session::get('error')) !!}   
                            @endif
                            {!! validationError($errors) !!}
                            <table class="table cart-table table-responsive-xs">
                                <thead>
                                    <tr class="table-head">
                                        <th scope="col">{{__('Image')}}</th>
                                        <th scope="col">{{__('Product Name')}}</th>
                                        <th scope="col">{{__('Price')}}</th>
                                        <th scope="col">{{__('Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($myWishlist) && count($myWishlist)>0)
                                        @foreach($myWishlist as $singleItem)
                                            @php
                                            $orderProLangData = getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleItem->product_id));
                                            if(!empty($orderProLangData->name)){
                                                $productName = $orderProLangData->name;
                                            }else{
                                                $productName = $singleItem->name;
                                            }
                                            @endphp
                                            <tr>
                                                <td>
                                                    <a href="{{url('product/'.$singleItem->slug)}}"><img src="{{asset($singleItem->image)}}" alt="{{$productName}}"></a>
                                                </td>
                                                <td>
                                                    <a href="{{url('product/'.$singleItem->slug)}}">{{$productName}}</a>
                                                    <div class="mobile-cart-content row">
                                                        <!-- <div class="col-xs-3">
                                                            <p>{{__('In Stock')}}</p>
                                                        </div> -->
                                                        <div class="col-xs-3">
                                                            <h2 class="td-color">
                                                                @if(!empty($singleItem->special_price) && $singleItem->special_price>0)
                                                                    {{showPrice($singleItem->special_price)}} 
                                                                @else
                                                                    {{showPrice($singleItem->price)}} 
                                                                @endif
                                                            </h2>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <h2 class="td-color">
                                                                <a href="{{ url('user/remove-wishlist') }}" class="icon mr-1"
                                                                   onclick="event.preventDefault(); document.getElementById('remove-wishlist2-{{$singleItem->id}}').submit();"> <i class="ti-trash"></i> {{__('Remove')}}</a>
                                                                <form id="remove-wishlist2-{{$singleItem->id}}" action="{{ url('user/remove-wishlist') }}" method="POST" style="display: none;">
                                                                    <input type="hidden" name="product_id" value="{{$singleItem->id}}">
                                                                    @csrf
                                                                </form>
                                                                <!-- <a href="#" class="cart"><i class="ti-shopping-cart"></i></a> -->
                                                            </h2>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h2>
                                                        @if(!empty($singleItem->special_price) && $singleItem->special_price>0)
                                                            {{showPrice($singleItem->special_price)}} 
                                                        @else
                                                            {{showPrice($singleItem->price)}}
                                                        @endif
                                                    </h2>
                                                </td>                                       
                                                <td>
                                                    <a href="{{ url('user/remove-wishlist') }}" class="icon mr-3"
                                                       onclick="event.preventDefault(); document.getElementById('remove-wishlist-{{$singleItem->id}}').submit();"> <i class="ti-trash"></i> {{__('Remove')}}</a>
                                                    <form id="remove-wishlist-{{$singleItem->id}}" action="{{ url('user/remove-wishlist') }}" method="POST" style="display: none;">
                                                        <input type="hidden" name="product_id" value="{{$singleItem->id}}">
                                                        @csrf
                                                    </form>
                                                    <!-- <a href="#" class="cart"><i class="ti-shopping-cart"></i></a> -->
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4">{{__('No record available to display')}}</td>
                                        </tr>
                                    @endif
                                </tbody>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section end -->
@endsection