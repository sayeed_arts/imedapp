@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>{{$page_title}}</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$page_title}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb End -->
<!-- thank-you section start -->
<section class="section-b-space light-layout">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="success-text">


                    @if($orderData->payment_option==1)
                    <!-- PURCHASE ORDER -->

                    @if($orderData->status==2)
                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                    <h2>{{__('Thank you')}}</h2>
                    <p>{{__('Your Quote Request has Been Generated ')}}.</p>
                    @elseif($orderData->status==1)
                    <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                    <h2>{{__('Sorry')}}</h2>
                    <p>{{__('Your payment has been failed')}}.</p>
                    @endif
                    @if(!empty($orderData->transaction_id))
                    <p>{{__('Transaction ID')}}:{{$orderData->transaction_id}}</p>
                    @endif


                    @else

                    <!-- ONLINE PAYMENT -->

                    @if($orderData->status==2)
                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                    <h2>{{__('Thank you')}}</h2>
                    <p>{{__('Your order has been placed successfully')}}.</p>
                    @elseif($orderData->status==1)
                    <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                    <h2>{{__('Sorry')}}</h2>
                    <p>{{__('Your payment has been failed')}}.</p>
                    @endif
                    @if(!empty($orderData->transaction_id))
                    <p>{{__('Transaction ID')}}:{{$orderData->transaction_id}}</p>
                    @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Section ends -->
<!-- order-detail section start -->
<section class="section-b-space confirm-order">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="product-order">
                    <h3>{{__('Your order details')}}</h3>
                    @if(!empty($orderProducts))
                    @foreach($orderProducts as $singleProducts)
                    @php
                    $orderProLangData =
                    getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleProducts->product_id));
                    if(!empty($orderProLangData->name)){
                    $productName = $orderProLangData->name;
                    }else{
                    $productName = $singleProducts->name;
                    }
                    $proPrice = 0;
                    if(!empty($singleProducts->product_attr_price) && $singleProducts->product_attr_price>0){
                    $proPrice = $singleProducts->product_attr_price;
                    }
                    else if(!empty($singleProducts->saleprice) && $singleProducts->saleprice>0){
                    $proPrice = $singleProducts->saleprice;
                    }
                    else{
                    $proPrice = $singleProducts->price;
                    }
                    $subPrice = $proPrice*$singleProducts->quantity;
                    @endphp
                    <div class="row product-order-detail">
                        <div class="col-2">
                            <img src="{{asset($singleProducts->images)}}" alt="{{$productName}}"
                                class="img-fluid blur-up lazyload">
                        </div>
                        <div class="col-6 order_detail">
                            <div>
                                <h4>{{__('Product Name')}}</h4>
                                <h5>{{$productName}}</h5>
                                @if(!empty($singleProducts->product_attr_name))
                                <h5>
                                    {{__($singleProducts->product_attr_name)}}
                                </h5>
                                @endif
                            </div>
                        </div>
                        <div class="col-2 order_detail">
                            <div>
                                <h4>{{__('Quantity')}}</h4>
                                <h5>{{$singleProducts->quantity}}</h5>
                            </div>
                        </div>
                        <div class="col-2 order_detail">
                            <div>
                                <h4>{{__('Price')}}</h4>
                                <h5>{{showOrderPrice($subPrice,$orderData->currency_code,$orderData->exchange_rate)}}
                                </h5>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    <div class="total-sec">
                        <ul>
                            <li>{{__('Subtotal')}}
                                <span>{{showOrderPrice($orderData->subtotal,$orderData->currency_code,$orderData->exchange_rate)}}</span>
                            </li>
                            <li>{{__('Tax(GST)')}}
                                <span>{{showOrderPrice($orderData->tax,$orderData->currency_code,$orderData->exchange_rate)}}</span>
                            </li>
                            <li>{{__('Discount')}}
                                <span>{{showOrderPrice($orderData->coupon_discount,$orderData->currency_code,$orderData->exchange_rate)}}</span>
                            </li>
                            <li>{{__('Shipping')}}
                                <span>{{showOrderPrice($orderData->shipping,$orderData->currency_code,$orderData->exchange_rate)}}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="final-total">
                        <h3>{{__('Total')}}
                            <span>{{showOrderPrice($orderData->total,$orderData->currency_code,$orderData->exchange_rate)}}</span>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 order-success-sec">
                <div class="row">
                    <div class="col-sm-12 order-summary">
                        <h4>{{__('Summary')}}</h4>
                        <ul class="order-detail">
                            <li>{{__('Order ID')}}: {{$orderData->order_number}}</li>
                            <li>{{__('Order Date')}}: {{displayDateWithTimeMD($orderData->ordered_on)}}</li>
                            <!-- <li>Order Total: ${{number_format($orderData->total,2)}}</li> -->
                        </ul>
                    </div>
                    <div class="col-sm-12 order-summary">
                        <h4>{{__('Shipping Address')}}</h4>
                        <ul class="order-detail">
                            <li>{{$orderData->shipping_first_name.' '.$orderData->shipping_last_name}}</li>
                            @if(!empty($orderData->shipping_company))
                            <li>{{$orderData->shipping_company}}</li>
                            @endif
                            <li>{{$orderData->shipping_street_address.', '.$orderData->shipping_apartment}}</li>
                            <li>{{$orderData->shipping_town.', '.$shippingState}}</li>
                            <li>{{$shippingCountry.', '.$orderData->shipping_postcode}}</li>
                            <li>{{__('Contact No')}}. {{$orderData->shipping_phone}}</li>
                        </ul>
                    </div>
                    <div class="col-sm-12 payment-mode">
                        <h4>{{__('Payment Method')}}</h4>
                        @if($orderData->payment_option==1)
                        <p>{{__('Quote Requested')}}</p>
                        @else
                        <p>{{__('MasterCard/ Credit Card/ Debit Card')}}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Section ends -->
@endsection