@extends('layouts.app')

@section('content')
@php
$pageLangData = getLanguageReconds('pages_translations',array('name','content'),array('page_id'=>$pageData->id));
if(!empty($pageLangData->name)){
$pageTitle = $pageLangData->name;
}else{
$pageTitle = $pageData->name;
}
if(!empty($pageLangData->name)){
$pageContent = $pageLangData->content;
}else{
$pageContent = $pageData->content;
}
@endphp
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>{!!$pageTitle!!}</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active">{!!$pageTitle!!}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<section class="content-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-left">
                {!!$pageContent!!}
            </div>
        </div>
    </div>
</section>
@endsection