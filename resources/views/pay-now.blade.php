@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>{{$page_title}}</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active">{{$page_title}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb End -->
    <!--section start-->
    <section class="cart-section section-b-space">
        <div class="container">
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif 
            @if (Session::has('error'))
                {!! errorMesaage(Session::get('error')) !!}   
            @endif
            {!! validationError($errors) !!}

            <p>{{__('Please wait')}}...<br>{{__('We are transferring you to a secure payment gateway website')}}...</p>
            <form action="{{$paypalFormAction}}" method="post" id="submitpaypalform">
                <input type="hidden" name="cmd" value="_cart">
                <input type="hidden" name="rm" value="2">
                <input type="hidden" name="upload" value="1">
                <input type="hidden" name="business" value="{{$paypalEmail}}">';
                @php
                $xxx=0;
                @endphp
                @foreach ($orderProducts as $key => $singleProduct) {
                    @php
                    if(!empty($singleProduct->product_attr_price) && $singleProduct->product_attr_price>0){
                        $prc = $singleProduct->product_attr_price;
                    }else if(!empty($singleProduct->saleprice) && $singleProduct->saleprice>0){
                        $prc = $singleProduct->saleprice;
                    }else{
                        $prc = $singleProduct->price;
                    }
                    $productName = $singleProduct->name;
                    if(!empty($singleProduct->product_attr_name)){
                        $productName .=' - '.$singleProduct->product_attr_name;
                    }
                    $xxx++;
                    @endphp
                    <input type="hidden" name="quantity_{{$xxx}}" value="{{$singleProduct->quantity}}">
                    <input type="hidden" name="item_name_{{$xxx}}" value="{{$productName}}">
                    <input type="hidden" name="amount_{{$xxx}}" value="{{showPrice($prc,1)}}">
                @endforeach
                <input type="hidden" name="discount_rate_cart" value="{{showPrice($orderData->coupon_discount,1)}}">
                <input type="hidden" name="shipping_1" value="{{showPrice($orderData->shipping,1)}}">
                <input type="hidden" name="handling_1" value="{{showPrice($orderData->handling_fee,1)}}">
                <input type="hidden" name="tax_cart" value="{{showPrice($orderData->tax,1)}}">
                <input type="hidden" name="currency_code" value="{{$orderData->currency_code}}">
                <input type="hidden" name="custom" value="{{$orderData->order_id}}">
                <input type="hidden" name="address1" value="{{$orderData->street_address}}">
                <input type="hidden" name="address2" value="{{$orderData->apartment}}">
                <input type="hidden" name="city" value="{{$orderData->town}}">
                <input type="hidden" name="country" value="{{$orderData->country}}">
                <input type="hidden" name="email" value="{{$orderData->email_address}}">
                <input type="hidden" name="first_name" value="{{$orderData->first_name}}">
                <input type="hidden" name="last_name" value="{{$orderData->last_name}}">
                <input type="hidden" name="state" value="{{$orderData->state}}">
                <input type="hidden" name="zip" value="{{$orderData->postcode}}">
                <input type="hidden" name="shopping_url" value="{{url('shop')}}">
                <input type="hidden" name="notify_url" value="{{url('notify-url')}}">
                <input type="hidden" name="cancel_return" value="{{url('cancel-return')}}">
                <input type="hidden" name="return" value="{{url('return')}}">
            </form>         
        </div>
    </section>
    <!--section end-->
    <script>            
        document.addEventListener("DOMContentLoaded", function(event) {
            document.createElement('form').submit.call(document.getElementById('submitpaypalform'));
        });         
    </script>
@endsection