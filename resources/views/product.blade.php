@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a> <i
                                class=" fa fa-angle-right"></i></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$page_title}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb End -->
<!-- section start -->
<section class=" pt-30">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 prd-gallery">
                    @if(!empty($pageData->additional_images))
                    <div class="product-big-gallery product-slick">
                        @php
                        $allImagesArray = explode('##~~##',$pageData->additional_images);
                        @endphp
                        @foreach($allImagesArray as $singleImage)
                        <div class="gallery-item">
                            <img src="{{ asset($singleImage)}}" alt=""
                                class="img-fluid blur-up lazyload image_zoom_cls-0">
                        </div>
                        @endforeach
                    </div>
                    <div class="row product-thumb-gallery">
                        <div class="col-12 p-0">
                            <div class="slider-nav">
                                @foreach($allImagesArray as $singleImage)
                                <div>
                                    <img src="{{ asset($singleImage)}}" alt="" class="img-fluid blur-up lazyload">
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="col-lg-7 rtl-text">
                    <div class="product-right">
                        <h2 class="">{{$page_title}}</h2>
                        <div class="head-star">
                            <div class="rating">
                                <img src="{{ asset('frontend/assets/images/') }}/{{round($averagerating,1)}}star.png " alt="">
                            </div>
                            <span>{{round($averagerating,1)}} out of 5 - (Total {{$totalcount}} ratings)</span>
                        </div>





                        <div class="product-price">
                            @if(!empty($pageData->special_price) && $pageData->special_price>0)
                            <h3 id="attr-pro-price">{{showPrice($pageData->special_price)}}</h3>
                            <h4>
                                <del>{{showPrice($pageData->price)}}</del>
                                <!-- <span>55% off</span> -->
                            </h4>
                            @elseif($pageData->price>0 && $pageData->stock_availability==1)
                            <h3 id="attr-pro-price">{{showPrice($pageData->price)}}</h3>
                            @endif
                        </div>


                        @if($pageData->stock_availability==0 || $pageData->price<1) <div class="product-buttons">
                            <button type="button" class="btn get-quote" data-name="{{$pageData->name}}"
                                data-url="{{url('product/'.$pageData->slug)}}" data-page='details'
                                data-id="{{base64_encode($pageData->id)}}">{{__('Get Quote')}}</button>
                    </div>
                    @else
                    <div class="product-description border-product">
                       
                        <div class="qty-box">
                        <h6 class="product-title">{{__('Quantity')}}</h6>

                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <button type="button" class="btn quantity-left-minus" data-type="minus"
                                        data-field=""><i class="ti-angle-left"></i></button>
                                </span>
                                <input type="number" name="quantity" class="form-control input-number" value="1"
                                    id="details-qty">
                                <span class="input-group-prepend">
                                    <button type="button" class="btn quantity-right-plus" data-type="plus"
                                        data-field=""><i class="ti-angle-right"></i></button>
                                </span>
                            </div>

                        </div>

                        @if(!empty($productVariAttributes) && count($productVariAttributes)>0)
                        <div class="attribute-section">

                            <h6 class="product-title">{{__('Configuration')}}</h6>

                            <div class="custom-select-drop">
                            <div class="select">
                                <select name="variAttribute" id="variAttribute" class="">
                                    <option value="">Select</option>
                                    @foreach($productVariAttributes as $singleAttr)
                                    <option
                                        value="{{base64_encode($singleAttr->id)}}##~~##{{showPrice($singleAttr->attribute_price)}}">
                                        {{$singleAttr->attrValue}}</option>
                                    @endforeach
                                </select>
                            </div>
                            </div>
                            <span id="error-attribute-div"></span>
                        </div>
                        @endif


                    </div>
                    <div class="product-buttons">
                        <a href="javascript:void(0)" class="btn btn-solid details-add-to-cart"
                            data-id="{{base64_encode($pageData->id)}}">{{__('Add to Cart')}}</a>

                            <form class="d-inline-block">
                                <button type="button"
                                    class="wishlist-btn add-to-wishlist @if($wishlistExists>0) active @endif"
                                    data-id="{{base64_encode($pageData->id)}}"
                                    data-user={{(isset($currentUser->id)?base64_encode($currentUser->id):'')}}>
                                    @if($wishlistExists>0)
                                   <img src="{{ asset('frontend/assets/images/heart.png') }} " alt="">
                                    <span
                                        class="title-font">{{__('Remove From Wishlist')}}</span>
                                    @else
                                    <img src="{{ asset('frontend/assets/images/heart.png') }} " alt=""> <span class="title-font">{{__('Add To Wishlist')}}</span>
                                    @endif
                                </button>
                            </form>
                    </div>
                    @endif
                    <!-- <a href="#" class="btn btn-solid">buy now</a> -->

                    <!-- <div class="border-product">
                        <h6 class="product-title">{{__('Product Details')}}</h6>
                        <p>{!!$productShortDescription!!}</p>
                    </div> -->

                    


                    <div class="border-product">
                        
                        <div class="product-icon">
                            <h6 class="product-title">Lets Spread this </h6>
                                <ul class="product-social">
                                    <li><a href="https://www.facebook.com/sharer.php?t={{$page_title}}&u={{url('product/'.$pageData->slug)}}"
                                            target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <!-- <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li> -->
                                    <li><a href="https://twitter.com/intent/tweet?text={{$page_title}}&url={{url('product/'.$pageData->slug)}}"
                                            target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    <!-- <li><a href="javascript:void(0)"><i class="fa fa-instagram"></i></a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-rss"></i></a></li> -->
                                    <li><a href="https://www.linkedin.com/sharing/share-offsite/?url={{url('product/'.$pageData->slug)}}"
                                            target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a
                                            href="mailto:?subject={{__('I would like to share a link with you')}}&body={{__('Hi,I found this website and thought you might like it')}} {{url('product/'.$pageData->slug)}}"><i
                                                class="fa fa-envelope"></i></a></li>
                                </ul>
                                
                            </div>

                        @if(!empty($storedetail->company))
                            <div class="vendor-info mt-3">
                                <h4>Sold By</h4>
                                <a href="{{url('storepage/'.$storedetail->store_slug)}}">
                                    <img src="{{ $storedetail->logo }} " width="80px" alt="">
                                    <p>{{$storedetail->company}}</p>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<section class="tab-product mt-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <ul class="nav nav-tabs nav-material" id="top-tab" role="tablist">
                    <li class="nav-item"><a class="nav-link active" id="top-home-tab" data-toggle="tab" href="#top-home"
                            role="tab" aria-selected="true">{{__('Description')}}</a>
                        <!-- <div class="material-border"></div> -->
                    </li>
                    <li class="nav-item"><a class="nav-link" id="profile-top-tab" data-toggle="tab" href="#top-profile"
                            role="tab" aria-selected="false">
                            <?php if($showratings){?>
                                {{__('Review / Rating ')}}
                            <?php } else{?>
                                {{__('Review ')}}
                            <?php } ?>
                             <span>( {{$totalcount}} )</span> </a>
                        <!-- <div class="material-border"></div> -->
                    </li>
                </ul>
                <div class="tab-content nav-material" id="top-tabContent">
                    <div class="tab-pane fade show active p-4" id="top-home" role="tabpanel"
                        aria-labelledby="top-home-tab">
                        {!!$productContent!!}
                    </div>
                    <div class="tab-pane fade  p-4" id="top-profile" role="tabpanel" aria-labelledby="profile-top-tab">

                        <div class="customer-review">
                            <h3>Customer Reviews <span>{{round($averagerating,1)}} out of 5 - (Total {{$totalcount}} ratings)</span></h3>


                            <ul>

                            @if(!empty($ratinglist) && count($ratinglist)>0)

                                @foreach($ratinglist as $value)
                                <?php $rating = $value->rating.'star.png';?>
                                    <li>
                                        <h4>{{$value->name}} {{$value->last_name}} <span>on</span> {{date('M d, Y',strtotime($value->added_at))}}</h4>
                                        <div class="stars">
                                            <img src="{{ asset('frontend/assets/images/') }}/{{$rating}} " alt=""></a>
                                        </div>
                                        <p>{{$value->description}}</p>
                                    </li>
                                @endforeach

                            @else
                                
                                <h4>No Review</h4>

                            @endif
                            </ul>

                        </div>
                        <?php 
                        if($showratings){
                        ?>
                        <div class="rating-form mb-5">

                            <h3>Rating</h3>

                            <div class="row">
                                <div class="col-md-12 mb-3 rating-stars">
                                    <h5>Your Rating</h5>
                            <ul>
                                <li class="five-star">
                                    <input type="radio" id="5star" name="radiorating" value="5" checked="checked">
                                    <label for="5star"><span> 5 star</span> </label>
                                </li>
                                <li class="four-star">
                                    <input type="radio" id="4star" name="radiorating" value="4">
                                    <label for="4star"><span> 4 star</span> </label>
                                </li>
                                <li class="three-star">
                                    <input type="radio" id="3star" name="radiorating" value="3">
                                    <label for="3star"><span> 3 star</span> </label>
                                </li>
                                <li class="two-star">
                                    <input type="radio" id="2star" name="radiorating" value="2">
                                    <label for="2star"> <span>2 star</span> </label>
                                </li>
                                <li class="one-star">
                                    <input type="radio" id="1star" name="radiorating" value="1">
                                    <label for="1star"><span> 1 star</span> </label>
                                </li>

                            </ul>
                            <span style="color:red;" id="rating_err"></span>
                          
                          </div>

                                <div class="col-md-12">
                                <h5>Your Review * </h5>
                                   <textarea class="form-control mb-3" name="Comment" id="comment" cols="20" rows="4"></textarea>
                            <input type="hidden" name="slug" id="slug" value="{{$lastsegment}}">
                            <span style="color:red;" id="description_err"></span>
                            </div>

                                <div class="col-md-12">

                                    <button class="btn submitratingproduct">
                                        submit
                                    </button>
                                </div>
                            </div>

                        </div>
                        <?php 
                        }
                        ?>

                        <!-- {!!$productMoreInfo!!} -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- product-tab ends -->
<!-- product section start -->
@if(!empty($relatedProducts) && count($relatedProducts)>0)
<section class="section-b-space ratio_asos product-related">
    <div class="container">
        <div class="row">
            <div class="col-12 ">
                <h2>{{__('Related Products')}}</h2>
            </div>
        </div>
        <div class="row search-product">
            @foreach($relatedProducts as $singleRelPro)
            @php
            $relatedProLangData =
            getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleRelPro->id));
            if(!empty($relatedProLangData->name)){
            $relatedProductName = $relatedProLangData->name;
            }else{
            $relatedProductName = $singleRelPro->name;
            }
            @endphp
            <div class="col-xl-3 col-md-3 col-sm-6">
                <div class="product-box">
                    <div class="img-wrapper">
                        <div class="front">
                            <a href="{{url('product/'.$singleRelPro->slug)}}">
                                <img src="{{asset($singleRelPro->image)}}" class="img-fluid blur-up lazyload "
                                    alt="{{$relatedProductName}}">
                            </a>
                        </div>
                    </div>
                    <div class="product-detail">
                        <a class="prd-title" href="{{url('product/'.$singleRelPro->slug)}}">
                            <h6>{{$relatedProductName}}</h6>
                        </a>
                        <!-- <p>Electrosurgical Units</p> -->

                        <div class="price-detail">
                             <div class="price">
                                @if(!empty($singleRelPro->special_price) && $singleRelPro->special_price>0)
                                <h4>{{showPrice($singleRelPro->special_price)}}</h4>
                                <h4>
                                    <del>{{showPrice($singleRelPro->price)}}</del>
                                    <!-- <span>55% off</span> -->
                                </h4>
                                @elseif($singleRelPro->price>0 && $singleRelPro->stock_availability==1)
                                <h4>{{showPrice($singleRelPro->price)}} </h4>
                                @endif
                            </div>

                        <div class="buttons">
                        @if($singleRelPro->stock_availability==0 || $singleRelPro->price<1) <button type="button"
                            class="btn get-quote" data-name="{{$singleRelPro->name}}"
                            data-url="{{url('product/'.$singleRelPro->slug)}}" data-page='listing'
                            data-id="{{base64_encode($singleRelPro->id)}}">{{__('Get Quote')}}</button>
                            @elseif(attributeExists($singleRelPro->id))
                            <a href="{{url('product/'.$singleRelPro->slug)}}">
                                <button type="button" class="btn"
                                    data-id="{{base64_encode($singleRelPro->id)}}"> <img src="{{ asset('frontend/assets/images/info.png') }}" alt="" srcset=""></button>
                            </a>
                            @else
                            <button type="button" class="btn add-to-cart"
                                data-id="{{base64_encode($singleRelPro->id)}}"><img src="{{ asset('frontend/assets/images/cart.jpg') }}" alt="" srcset=""></button>
                            @endif
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif
@endsection