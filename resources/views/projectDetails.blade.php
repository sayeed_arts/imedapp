@extends('layouts.app')

@section('content')

    @php
    $proLangData = getLanguageReconds('projects_translations',array('name','content','client_info'),array('project_id'=>$pageData->id));
    if(!empty($proLangData->name)){
        $projectName = $proLangData->name;
    }else{
        $projectName = $pageData->name;
    }
    if(!empty($proLangData->content)){
        $projectContent = $proLangData->content;
    }else{
        $projectContent = $pageData->content;
    }

    if(!empty($proLangData->client_info)){
        $projectClientInfo = $proLangData->client_info;
    }else{
        $projectClientInfo = $pageData->client_info;
    }
    @endphp

<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>{!!$projectName!!}</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active">{!!$projectName!!}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<section class="project-details">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 text-left">
                <div>
                    <img src="{{asset($pageData->image)}}" alt="{{$pageData->name}}">
                </div>
            </div>
            <div class="col-sm-8 text-left summary">
                <h2>{!!$projectName!!}</h2>
                {!!$projectContent!!}
                <table border="0" width="100%">
                    <tr>
                        <th>{{__('Client Info')}}</th>
                        <td>{!!nl2br($projectClientInfo)!!}</td>
                    </tr>
                    <tr>
                        <th>{{__('Type')}}</th>
                        <td>
                            @if($pageData->type==0) {{__('Current Project')}}
                            @elseif($pageData->type==1) {{__('Past Project')}}
                            @elseif($pageData->type==2) {{__('Upcoming Project')}}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>{{__('Start Date')}}</th>
                        <td>{{displayDateMD($pageData->start_date)}}</td>
                    </tr>
                    <tr>
                        <th>{{__('End Date')}}</th>
                        <td>{{displayDateMD($pageData->end_date)}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</section>
@endsection