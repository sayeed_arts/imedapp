@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{((isset($page_title))?$page_title:'')}}
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb end -->
<!-- section start -->
<section class="section-n-space pt-30 ratio_asos">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="collection-content col">
                    <div class="page-main-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="top-banner-wrapper">
                                    <div class="top-banner-content small-section">
                                        <h4>{{((isset($page_title))?$page_title:'')}}</h4>
                                        {!!((isset($pageData->content))?$pageData->content:'')!!}
                                    </div>
                                </div>
                                <div class="project-list">
                                    <div class="collection-product-wrapper">


                                        <div class="product-wrapper-grid">
                                            <div class="row margin-res">

                                                <!-- <h4>{{__('Current Project')}}</h4> -->
                                                @if(!empty($allCurrentRecord) && count($allCurrentRecord)>0)

                                                @foreach($allCurrentRecord as $singleRecord)
                                                @php
                                                $currProLangData =
                                                getLanguageReconds('projects_translations',array('name'),array('project_id'=>$singleRecord->id));
                                                if(!empty($currProLangData->name)){
                                                $currProjectName = $currProLangData->name;
                                                }else{
                                                $currProjectName = $singleRecord->name;
                                                }
                                                @endphp
                                                <div class="col-xl-3 col-6 col-grid-box">
                                                    <div class="product-box">
                                                        <div class="img-wrapper">
                                                            <div class="front">
                                                                <a href="{{url('projects/'.$singleRecord->slug)}}"><img
                                                                        src="{{asset($singleRecord->image)}}"
                                                                        class="img-fluid blur-up lazyload "
                                                                        alt="{{$currProjectName}}"></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-detail">
                                                            <a href="{{url('projects/'.$singleRecord->slug)}}">
                                                                <h6>{{$currProjectName}}</h6>
                                                            </a>
                                                            <a class="btn btn-primary"
                                                                href="{{url('projects/'.$singleRecord->slug)}}">View
                                                                Details</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach

                                                @endif


                                                <!-- UPCOMING LOOP -->


                                                @if(!empty($allFutureRecord) && count($allFutureRecord)>0)

                                                @foreach($allFutureRecord as $singleRecord)
                                                @php
                                                $futProLangData =
                                                getLanguageReconds('projects_translations',array('name'),array('project_id'=>$singleRecord->id));
                                                if(!empty($futProLangData->name)){
                                                $futProjectName = $futProLangData->name;
                                                }else{
                                                $futProjectName = $singleRecord->name;
                                                }
                                                @endphp
                                                <div class="col-xl-3 col-6 col-grid-box">
                                                    <div class="product-box">
                                                        <div class="img-wrapper">
                                                            <div class="front">
                                                                <a href="{{url('projects/'.$singleRecord->slug)}}"><img
                                                                        src="{{asset($singleRecord->image)}}"
                                                                        class="img-fluid blur-up lazyload "
                                                                        alt="{{$futProjectName}}"></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-detail">
                                                            <a href="{{url('projects/'.$singleRecord->slug)}}">
                                                                <h6>{{$futProjectName}}</h6>
                                                            </a>
                                                            <a class="btn btn-primary"
                                                                href="{{url('projects/'.$singleRecord->slug)}}">View
                                                                Details</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach

                                                @endif

                                                <!-- PAST PROJECT LOOP -->

                                                @if(!empty($allPastRecord) && count($allPastRecord)>0)

                                                @foreach($allPastRecord as $singleRecord)
                                                @php
                                                $pastProLangData =
                                                getLanguageReconds('projects_translations',array('name'),array('project_id'=>$singleRecord->id));
                                                if(!empty($pastProLangData->name)){
                                                $pastProjectName = $pastProLangData->name;
                                                }else{
                                                $pastProjectName = $singleRecord->name;
                                                }
                                                @endphp
                                                <div class="col-xl-3 col-6 col-grid-box">
                                                    <div class="product-box">
                                                        <div class="img-wrapper">
                                                            <div class="front">
                                                                <a href="{{url('projects/'.$singleRecord->slug)}}"><img
                                                                        src="{{asset($singleRecord->image)}}"
                                                                        class="img-fluid blur-up lazyload "
                                                                        alt="{{$pastProjectName}}"></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-detail">
                                                            <a href="{{url('projects/'.$singleRecord->slug)}}">
                                                                <h6>{{$pastProjectName}}</h6>
                                                            </a>
                                                            <a class="btn btn-primary"
                                                                href="{{url('projects/'.$singleRecord->slug)}}">View
                                                                Details</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach

                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="collection-product-wrapper">
                                        <h4>{{__('Upcoming Project')}}</h4>
                                        
                                    </div>
                                    <div class="collection-product-wrapper">
                                        <h4>{{__('Past Project')}}</h4>
                                       
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection