@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>{!!$page_title!!}</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active">{!!$page_title!!}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="service-form">
    <div class="container">
        @if($pageData->display_form==1)
            @if(!empty($allSerCat))
                <div class="description1 service-category">
                    <ul class="category-list">
                        @foreach($allSerCat as $singleCat)
                        @php
                        $serviceCatLangData =
                        getLanguageReconds('service_category_translations',array('name','offered'),array('services_id'=>$singleCat->id));
                        if(!empty($serviceCatLangData->name)){
                        $serviceName = $serviceCatLangData->name;
                        }else{
                        $serviceName = $singleCat->name;
                        }
                        if(!empty($serviceCatLangData->offered)){
                        $equipStr = $serviceCatLangData->offered;
                        }else{
                        $equipStr = $singleCat->offered;
                        }
                        $equipmentType = array();
                        if(!empty($equipStr)) $equipmentType = explode("\n", str_replace("\r", "", $equipStr));
                        @endphp
                        <li>                            
                            <div class="form-group text-left">                      
                                <label for="cat_{{$singleCat->id}}">
                                    <a href="{{url('services/'.$pageData->slug.'/'.$singleCat->slug)}}">
                                        @if(!empty($singleCat->image))
                                            <img width="98" height="98" src="{{asset($singleCat->image)}}" alt="{{$serviceName}}">
                                        @endif
                                        <span>{{__($serviceName)}}</span>
                                    </a>
                                </label>                            
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        @else
            <div class="description">
                {!!$serviceContent!!}
            </div>
        @endif
    </div>
</div>
@endsection