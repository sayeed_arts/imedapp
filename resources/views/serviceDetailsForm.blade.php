@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>{!!$serviceCatName!!}</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{url('services/'.$pageData->slug)}}">{!!$page_title!!}</a>
                        </li>
                        <li class="breadcrumb-item active">{!!$serviceCatName!!}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="service-form">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="description">

                    @if(!empty($singleCat->image))                    
                    <img src="{{asset($singleCat->image)}}" alt="{{$serviceName}}">
                    @endif


                    {!!$serviceContent!!}
                </div>
                <div class="description">
                    <h3>{{__('Service Inquiry Form')}}</h3>
                </div>
                <form method="POST" class="row text-center jqueryValidate" action="{{ route('post-service') }}">
                    @csrf
                    @if (Session::has('message'))
                    {!! successMesaage(Session::get('message')) !!}
                    @endif
                    @if (Session::has('error'))
                    {!! errorMesaage(Session::get('error')) !!}
                    @endif
                    {!! validationError($errors) !!}
                    <input type="hidden" name="service_name" value="{{$pageData->name}}">
                    <input type="hidden" name="service_slug" value="{{$pageData->slug}}">
                    <input type="hidden" name="service_id" value="{{$pageData->id}}">
                    <input type="hidden" name="service_category" value="{{$singleServiceCat->name}}">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group text-left">
                                <label>{{__('Hospital/Company*')}}</label>
                                <input type="text" name="hospital_company" id="hospital_company" class="form-control"
                                    required value="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group text-left">
                                <label>{{__('Full Name*')}}</label>
                                <input type="text" name="name" id="name" class="form-control" required value="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group text-left">
                                <label>{{__('Phone*')}}</label>
                                <input type="text" name="phone" class="form-control" required value="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group text-left">
                                <label>{{__('Postal code*')}}</label>
                                <input type="text" name="postal_code" class="form-control" required value="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group text-left">
                                <label>{{__('Email*')}}</label>
                                <input type="email" name="email" class="form-control" required value="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group text-left">
                                <label>{{__('Extension')}}</label>
                                <input type="text" name="extension" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group text-left">
                                <label>{{__('PO')}} #</label>
                                <input type="text" name="PO" class="form-control" value="">
                            </div>
                        </div>
                    </div>
                    <div class="description">
                        <h3>{{__('ITEM FOR SERVICE')}}</h3>
                        <p>{{__('Please select one or more of the items below and indicate the problem next to the product')}}
                        </p>
                    </div>
                    @for($i=1;$i<=10;$i++) <div class="row display_pro_sec" id="display_pro_sec__{{$i}}" @if($i>
                        1)style="display:none"@endif >
                        <div class="col-sm-12">
                            <div class="form-group text-left">
                                <label>{{__('Equipment to service*')}}</label>
                                <select name="equipment_type[]" id="equipment_type__{{$i}}"
                                    class="form-control allcatequiptype" @if($i==1) required @endif>
                                    <option value="">{{__('Select')}}</option>
                                    @if(!empty($equipment_type))
                                    @foreach($equipment_type as $equipType)
                                    @if(!empty($equipType))
                                    <option value="{!!$equipType!!}">{!!$equipType!!}</option>
                                    @endif
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group text-left">
                                <label>{{__('Serial Number*')}}</label>
                                <input type="text" name="serial_number[]" class="form-control" value=""
                                    id="serial_number__{{$i}}" @if($i==1) required @endif>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group text-left">
                                <label>{{__('Quantity*')}}</label>
                                <input pattern="[0-9]" onkeypress="return !(event.charCode == 46)" type="number"
                                    step="1" name="amount[]" class="form-control" value="" id="amount__{{$i}}"
                                    @if($i==1) required @endif>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group text-left">
                                <label>{{__('Product Issue*')}}</label>
                                <textarea name="product_issue[]" class="form-control" rows="3"
                                    id="product_issue__{{$i}}" @if($i==1) required @endif></textarea>
                            </div>
                        </div>
            </div>
            @endfor
            <div class="row">
                <div class="col-md-12">
                    <div class="add-more-service-div">
                        <a href="javascript:void(0)" dataval="2" class="add-more-service-button">{{__('Add More')}}</a>
                    </div>
                </div>
                <div class="col-sm-12  text-center">
                    <button type="submit" class="btn btn-success getStarted btn-block">{{__('Submit')}}</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection