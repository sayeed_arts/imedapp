@extends('layouts.app')

@section('content')
<style ttpe="text/css">
.just-padding {
  padding: 15px;
}

.list-group.list-group-root {
  padding: 0;
  overflow: hidden;
}

.list-group.list-group-root .list-group {
  margin-bottom: 0;
}

.list-group.list-group-root .list-group-item {
  border-radius: 0;
  border-width: 1px 0 0 0;
}

.list-group.list-group-root > .list-group-item:first-child {
  border-top-width: 0;
}

.list-group.list-group-root > .list-group > .list-group-item {
  padding-left: 30px;
}

.list-group.list-group-root > .list-group > .list-group > .list-group-item {
  padding-left: 45px;
}
</style>
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Service Areas</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb end -->
    <!-- section start -->
    <section class="section-n-space pt-30 ratio_asos">
        <div class="collection-wrapper">
            <div class="container">
                <div class="row">
                    <div class="collection-content col">
                        <div class="page-main-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="top-banner-wrapper service-area-banner">
                                        <div>
                                            Service Areas
                                        </div>
                                    </div>
                                    <div class="service-area-wrapper">
                                        @if(!empty($allStates) && count($allStates)>0)
                                        <div class="list-group list-group-root well">
                                            <div class="row">
                                                @foreach($allStates as $singleState)
                                                <?php $cities = serviceCitiesByState($singleState->state); ?>
                                                <div class="col-md-3 mb-3">
                                                    <div class="state-name">
                                                        <span>{!! $singleState->stateName !!}</span>
                                                    </div>
                                                    <div class="list-group">
                                                        <ul class="city-list">
                                                        @foreach($cities as $singleCity)
                                                        <li><a href="/service-area/<?php echo $singleCity->slug; ?>">{!! $singleCity->cityname !!}</a></li>
                                                        @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection