@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{((isset($page_title))?$page_title:'')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb end -->
    <!-- section start -->
    <section class="section-n-space pt-30 ratio_asos">
        <div class="collection-wrapper">
            <div class="container">
                <div class="row">
                    <div class="collection-content col">
                        <div class="page-main-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="top-banner-wrapper">
                                        <div class="top-banner-content small-section">
                                            <h4>{{((isset($page_title))?$page_title:'')}}</h4>
                                            {!!((isset($pageData->content))?$pageData->content:'')!!}
                                        </div>
                                    </div>
                                    <div class="collection-product-wrapper">
                                        @if(!empty($allRecord) && count($allRecord)>0)
                                            <div class="product-wrapper-grid">
                                                <div class="row margin-res">
                                                    @foreach($allRecord as $singleRecord)
                                                        @php
                                                        $serLangData = getLanguageReconds('services_translations',array('name'),array('services_id'=>$singleRecord->id));
                                                        if(!empty($serLangData->name)){
                                                            $serviceName = $serLangData->name;
                                                        }else{
                                                            $serviceName = $singleRecord->name;
                                                        }
                                                        @endphp
                                                        <div class="col-xl-3 col-6 col-grid-box">
                                                            <div class="product-box">
                                                                <div class="img-wrapper">
                                                                    <div class="front">
                                                                    	@if(!empty($singleRecord->external_link))
                                                                    		<a href="{{$singleRecord->external_link}}" target="_blank"><img src="{{asset($singleRecord->image)}}" class="img-fluid blur-up lazyload " alt="{{$serviceName}}"></a>
                                                                    	@else
                                                                    		<a href="{{url('services/'.$singleRecord->slug)}}"><img src="{{asset($singleRecord->image)}}" class="img-fluid blur-up lazyload " alt="{{$serviceName}}"></a>
                                                                    	@endif      
                                                                    </div>
                                                                </div>
                                                                <div class="product-detail">
                                                                	@if(!empty($singleRecord->external_link))
                                                                		<a href="{{$singleRecord->external_link}}" target="_blank">
	                                                                        <h6>{{$serviceName}}</h6>
	                                                                    </a> 
                                                                	@else
	                                                                    <a href="{{url('services/'.$singleRecord->slug)}}">
	                                                                        <h6>{{$serviceName}}</h6>
	                                                                    </a>  
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection