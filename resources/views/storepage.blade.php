@extends('layouts.app')
@section('content')
<div class="store-page">
    <div class="row store-banner">
        <div class="col-md-12">
            @if(!empty($storedetail->banner))
                <img src="{{$storedetail->banner}}" alt="">
            @else
                <img src="{{asset('frontend/assets/images/default-store-banner.jpg')}}">
            @endif
        </div>
    </div>
    <div class="container">
        <div class="row top-row">
            <div class="col-md-3">
                <div class="store-logo">
                    @if(!empty($storedetail->logo))
                    <img src="{{$storedetail->logo}}" alt="">
                    @else
                    <img src="{{asset('frontend/assets/images/default-store-logo.jpg')}}">
                    @endif            
                </div>
            </div>
            <div class="col-md-4">
                <div class="store-name">
                    <h2>{{$storedetail->store_slug}}</h2>
                    <span><img src="{{asset('frontend/assets/images/')}}/{{round($averagerating,1)}}star.png">  {{round($averagerating,1)}} out of 5 - (Total {{$totalcount}} ratings)</span>
                </div>
            </div>

            <div class="col-md-5 text-right store-social">
                <a href="{{$storedetail->fb_link}}" target="_blank" rel="noopener noreferrer"><i class="fa fa-facebook"
                        aria-hidden="true"></i></a>
                <a href="{{$storedetail->insta_link}}" target="_blank" rel="noopener noreferrer"><i
                        class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="{{$storedetail->pinterest_link}}" target="_blank" rel="noopener noreferrer"><i
                        class="fa fa-pinterest" aria-hidden="true"></i></a>
                <a href="{{$storedetail->twitter_link}}" target="_blank" rel="noopener noreferrer"><i
                        class="fa fa-twitter"></i></a>
            </div>
        </div>
    </div>

    @if(!empty($products)  && count($products)>0 )           

        <div class="container">
            <div class="collection-product-wrapper mt-5">
                <div class="title2">
                    <h2 class="title-inner2">Store Products</h2>
                </div>
                <div class="product-wrapper-grid">
                    <div class="row margin-res">
                    
                        @foreach($products as $feaPro)
                            @php
                                $fProLangData = getLanguageReconds('products_translations',array('name'),array('product_id'=>$feaPro->id));
                                if(!empty($fProLangData->name)){
                                    $productName = $fProLangData->name;
                                }else{
                                    $productName = $feaPro->name;
                                }
                            @endphp
                            <div class="col-md-4">
                            <div class="product-box">
                                <div class="img-wrapper">
                                    <div class="front">
                                        <a href="{{url('product/'.$feaPro->slug)}}"><img src="{{asset($feaPro->image)}}" class="img-fluid blur-up lazyload " alt="{{$feaPro->name}}"></a>
                                    </div>
                                </div>
                                <div class="product-detail">
                                    <a class="prd-title" href="{{url('product/'.$feaPro->slug)}}">
                                        <h6>{{$productName}}</h6>
                                    </a>

                                    <div class="price-detail">
                                        <div class="price">
                                            <!-- <p>Electrosurgical Units</p> -->
                                            @if(!empty($feaPro->special_price) && $feaPro->special_price>0)
                                                <h4>{{showPrice($feaPro->special_price)}} 
                                                    <span><del>{{showPrice($feaPro->price)}}</del></span>
                                                </h4>
                                            @elseif($feaPro->price>0 && $feaPro->stock_availability==1)
                                                <h4>{{showPrice($feaPro->price)}}</h4>
                                            @endif
                                        </div>

                                        <div class="buttons">
                                            @if($feaPro->stock_availability==0 || $feaPro->price<1)
                                                <button type="button" class="btn get-quote" data-name="{{$feaPro->name}}" data-url="{{url('product/'.$feaPro->slug)}}" data-page ='listing' data-id="{{base64_encode($feaPro->id)}}">{{__('Get Quote')}}</button>
                                            @elseif(attributeExists($feaPro->id))
                                                <a href="{{url('product/'.$feaPro->slug)}}">
                                                    <button type="button" class="btn view-details" data-id="{{base64_encode($feaPro->id)}}"> <img src="{{ asset('frontend/assets/images/info.png') }}" alt="" srcset=""></button>
                                                </a>
                                            @else
                                                <button type="button" class="btn add-to-cart" data-id="{{base64_encode($feaPro->id)}}"> <img src="{{ asset('frontend/assets/images/cart.jpg') }}" alt="" srcset=""></button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        @endforeach
                       
                </div>
            </div>
        </div>
    
    @else

        <div class="empty-div products">
            <h4>NO PRODUCTS TO DISPLAY</h4>
        </div>        

    @endif

    @if(!empty($manufacturer)  && count($manufacturer)>0 )
            <section class="brands-section">
                <div class="container">
                    <div class="title2">
                        <h2 class="title-inner2">Shop by Brands</h2>
                    </div>

                    <div class="brands-list">
                        <div class="row align-items-center">
                                
                    @foreach($manufacturer as $value):
                        <div class="col-lg-2 col-md-4 col-6 col-sm-4">
                            <div class="single-brand-logo mb-30">
                                <a href="#"><img src="{{ $value->logo }} " alt=""></a>
                            </div>
                        </div>
                        @endforeach
                

                        </div>


                    </div>

                </div>

            </section>
    @else

            <!-- <div class="empty-div brands">
                <h4>NO BRANDS TO DISPLAY</h4>
            </div> -->

    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="customer-review">
                    <h2>Customer Reviews</h2>

                    <ul>
                    @foreach($ratinglist as $value)
                    <?php $rating = $value->rating.'star.png';?>
                        <li>
                            <h4>{{$value->name}} {{$value->last_name}} <span>on</span> {{date('M d, Y',strtotime($value->added_at))}}</h4>
                            <div class="stars">
                                <img src="{{ asset('frontend/assets/images/') }}/{{$rating}} " alt=""></a>
                            </div>
                            <p>{{$value->description}}</p>
                        </li>
                        @endforeach

                      </ul>

                </div>
            </div>
            <?php 
            if($showratings){
            ?>
            <div class="col-md-12">
                <div class="rating-form mb-5">

                    <h2>Rating</h2>

                    <div class="row">
                        <div class="col-md-12 mb-3 rating-stars">
                            <h5>Your Rating</h5>
                            <ul>
                                <li class="five-star">
                                    <input type="radio" id="5star" name="radiorating" value="5" checked="checked">
                                    <label for="5star"><span> 5 star</span> </label>
                                </li>
                                <li class="four-star">
                                    <input type="radio" id="4star" name="radiorating" value="4">
                                    <label for="4star"><span> 4 star</span> </label>
                                </li>
                                <li class="three-star">
                                    <input type="radio" id="3star" name="radiorating" value="3">
                                    <label for="3star"><span> 3 star</span> </label>
                                </li>
                                <li class="two-star">
                                    <input type="radio" id="2star" name="radiorating" value="2">
                                    <label for="2star"> <span>2 star</span> </label>
                                </li>
                                <li class="one-star">
                                    <input type="radio" id="1star" name="radiorating" value="1">
                                    <label for="1star"><span> 1 star</span> </label>
                                </li>

                            </ul>
                            <span style="color:red;" id="rating_err"></span>
                            

                        </div>

                        <div class="col-md-12">
                            <label for="comment">Your Review *</label>
                            <textarea class="form-control mb-3" name="Comment" id="comment" cols="20" rows="5"></textarea>
                            <input type="hidden" name="slug" id="slug" value="{{$lastsegment}}">
                            <span style="color:red;" id="description_err"></span>
                        </div>

                        <div class="col-md-12">

                            <button class="btn submitratingvendor" >
                                submit
                            </button>
                        </div>
                    </div>





                </div>
            </div>
            <?php } ?>
        </div>
    </div>








    </div>
</div>
@endsection