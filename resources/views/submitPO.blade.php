@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>{{$page_title}}</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$page_title}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb End -->
<!-- order-detail section start -->
<section class="section-b-space user-order-details" style="padding-top: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}
                @endif
                @if(empty($orderData->po_order_number))
                <form method="POST" class="row text-center jqueryValidate" action="{{ route('update-po') }}"
                    enctype="multipart/form-data">
                    <input type="hidden" name="currentorderid" value="{{base64_encode($orderData->order_id)}}">
                    <input type="hidden" name="old_po_order_file" value="{{$orderData->po_order_file}}">
                    @csrf
                    <div class="col-sm-6">
                        <div class="form-group text-left">
                            <label>{{__('Enter Customer Purchase Order Number')}}</label>
                            <input type="text" name="po_order_number" id="po_order_number"
                                class="form-control {{ $errors->has('po_order_number') ? ' is-invalid' : '' }}" required
                                value="{{$orderData->po_order_number }}">
                            @if ($errors->has('po_order_number'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('po_order_number') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group text-left">
                            <label>{{__('Upload Customer Purchase Order File')}}</label>
                            <input type="file" name="po_order_file" id="po_order_file"
                                class="form-control {{ $errors->has('po_order_file') ? ' is-invalid' : '' }}">
                            @if ($errors->has('po_order_file'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('po_order_file') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group text-left">
                            <label>{{__('Wire Bank information “if applicable”')}}</label>
                            <textarea name="po_order_qty" id="po_order_qty"
                                class="form-control {{ $errors->has('po_order_qty') ? ' is-invalid' : '' }}">{{ $orderData->po_order_qty }}</textarea>
                        </div>
                    </div>

                    <div class="col-sm-4 offset-md-4 text-center">
                        <button type="submit" class="btn btn-success getStarted btn-block">{{__('Submit')}}</button>
                    </div>
                </form>
                <p>&nbsp;</p>
                @else
                <div class="row" style="background: #f5f5f5; padding: 20px 20px 10px 20px;">
                    <div class="col-sm-4">
                        <div class="form-group text-left">
                            <label><b>{{__('Customer Purchase Order Number')}}</b></label>
                            <p>{{$orderData->po_order_number }}</p>
                            <!-- <input type="text" name="po_order_number" id="po_order_number" class="form-control"
                                value="{{$orderData->po_order_number }}" readonly> -->
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group text-left">
                            <label><b> {{__('Customer Purchase Order File')}}</b> </label><br>
                            @if(!empty($orderData->po_order_file))
                            <a class="btn btn-primary" href="{{url($orderData->po_order_file)}}"
                                target="_blank">View</a>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group text-left">
                            <label><b>{{__('Wire Bank information “if applicable”')}}</b></label>
                            <p>{{ $orderData->po_order_qty }}</p>
                            <!-- <textarea name="po_order_qty" id="po_order_qty" class="form-control"
                                readonly>{{ $orderData->po_order_qty }}</textarea> -->
                        </div>
                    </div>


                </div>
                <p>&nbsp;</p>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="product-order">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>{{__('Your order details')}}</h3>
                        </div>
                        <div class="col-md-6 text-right">

                        </div>
                    </div>
                    @if(!empty($orderProducts))
                    @foreach($orderProducts as $singleProducts)
                    @php
                    $orderProLangData =
                    getLanguageReconds('products_translations',array('name'),array('product_id'=>$singleProducts->product_id));
                    if(!empty($orderProLangData->name)){
                    $productName = $orderProLangData->name;
                    }else{
                    $productName = $singleProducts->name;
                    }

                    $proPrice = 0;
                    if(!empty($singleProducts->product_attr_price) && $singleProducts->product_attr_price>0){
                    $proPrice = $singleProducts->product_attr_price;
                    }
                    else if(!empty($singleProducts->saleprice) && $singleProducts->saleprice>0){
                    $proPrice = $singleProducts->saleprice;
                    }
                    else{
                    $proPrice = $singleProducts->price;
                    }
                    $subPrice = $proPrice*$singleProducts->quantity;
                    @endphp
                    <div class="row product-order-detail">
                        <div class="col-2">
                            <img src="{{asset($singleProducts->images)}}" alt="{{$productName}}"
                                class="img-fluid blur-up lazyload">
                        </div>
                        <div class="col-6 order_detail">
                            <div>
                                <h4>{{__('Product Name')}}</h4>
                                <h5>{{$productName}}</h5>
                                @if(!empty($singleProducts->product_attr_name))
                                <h5>
                                    {{__($singleProducts->product_attr_name)}}
                                </h5>
                                @endif
                            </div>
                        </div>
                        <div class="col-2 order_detail">
                            <div>
                                <h4>{{__('Quantity')}}</h4>
                                <h5>{{$singleProducts->quantity}}</h5>
                            </div>
                        </div>
                        <div class="col-2 order_detail">
                            <div>
                                <h4>{{__('Price')}}</h4>
                                <h5>{{showOrderPrice($subPrice,$orderData->currency_code,$orderData->exchange_rate)}}
                                </h5>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    <div class="total-sec">
                        <ul>
                            <li>{{__('Subtotal')}}
                                <span>{{showOrderPrice($orderData->subtotal,$orderData->currency_code,$orderData->exchange_rate)}}</span>
                            </li>
                            <li>{{__('Tax(GST)')}}
                                <span>{{showOrderPrice($orderData->tax,$orderData->currency_code,$orderData->exchange_rate)}}</span>
                            </li>
                            <li>{{__('Discount')}}
                                <span>{{showOrderPrice($orderData->coupon_discount,$orderData->currency_code,$orderData->exchange_rate)}}</span>
                            </li>
                            <li>{{__('Shipping')}}
                                <span>{{showOrderPrice($orderData->shipping,$orderData->currency_code,$orderData->exchange_rate)}}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="final-total">
                        <h3>{{__('Total')}}
                            <span>{{showOrderPrice($orderData->total,$orderData->currency_code,$orderData->exchange_rate)}}</span>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 order-success-sec">
                <div class="row ">
                    <div class="col-sm-12 order-summary">
                        <h4>{{__('Summary')}}</h4>
                        <ul class="order-detail">
                            <li>{{__('Order ID')}}: {{$orderData->order_number}}</li>
                            <li>{{__('Order Date')}}: {{displayDateWithTimeMD($orderData->ordered_on)}}</li>
                            <!-- <li>Order Total: ${{number_format($orderData->total,2)}}</li> -->
                            <li>{{__('Order Status')}}:
                                @if($orderData->status=='0')
                                {{__('Pending')}}
                                @elseif($orderData->status=='1')
                                {{__('Failed')}}
                                @elseif($orderData->status=='2')
                                {{__('Successfull')}}
                                @endif
                            </li>
                            @if($orderData->status=='2')
                            <li>{{__('Shipping Status')}}:
                                @if($orderData->shipping_status==0)
                                {{__('Received')}}
                                @elseif($orderData->shipping_status==1)
                                {{__('Shipped')}}
                                @elseif($orderData->shipping_status==2)
                                {{__('Voided')}}
                                @elseif($orderData->shipping_status==3)
                                {{__('Processing')}}
                                @elseif($orderData->shipping_status==4)
                                {{__('Completed')}}
                                @endif
                            </li>
                            @endif
                        </ul>
                    </div>
                    <div class="col-sm-12 order-summary">
                        <h4>{{__('Shipping Address')}}</h4>
                        <ul class="order-detail">
                            <li>{{$orderData->shipping_first_name.' '.$orderData->shipping_last_name}}</li>
                            @if(!empty($orderData->shipping_company))
                            <li>{{$orderData->shipping_company}}</li>
                            @endif
                            <li>{{$orderData->shipping_street_address.', '.$orderData->shipping_apartment}}</li>
                            <li>{{$orderData->shipping_town.', '.$shippingState}}</li>
                            <li>{{$shippingCountry.', '.$orderData->shipping_postcode}}</li>
                            <li>{{__('Contact No')}}. {{$orderData->shipping_phone}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Section ends -->
@endsection