@extends('layouts.vendorapp')

@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Vendor Profile
                   </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('vendor')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/vendors')}}"><i class="fa fa-table"></i> Vendor</a></li>
            <li>Edit Profile</li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <!-- <div class="block-title">
                        Add New
                    </div> -->
                    <!-- END Form Elements Title -->
                    @if (Session::has('message'))
                        {!! successMesaage(Session::get('message')) !!}   
                    @endif
                    {!! validationError($errors) !!}
                    <!-- Basic Form Elements Content -->
                    <form action="{{url('vendor/updatevendorprofile')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        {{ csrf_field() }}
                    <div class="row  text-center">
                        <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="name">First Name</label>
                            <div class="col-md-9">
                                <input type="text" id="name" name="name" class="form-control" required placeholder="First Name" value="<?=isset($vendordetail->name)?$vendordetail->name:''?>" tabindex="1">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="name">Address 1</label>
                            <div class="col-md-9">
                                <input type="text" id="address1" name="address1" class="form-control" required placeholder="Adress 1" tabindex="8" value="<?=isset($vendordetail->address1)?$vendordetail->address1:''?>">
                            </div>
                        </div> 
                       </div>

                       <div class="row  text-center">
                        <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="name">Last Name</label>
                            <div class="col-md-9">
                                <input type="text" id="last_name" name="last_name" class="form-control" required placeholder="Last Name" value="<?=isset($vendordetail->last_name)?$vendordetail->last_name:''?>" tabindex="1">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="name">City</label>
                            <div class="col-md-9">
                                <input type="text" id="city" name="city" class="form-control" required placeholder="city" value="<?=isset($vendordetail->city)?$vendordetail->city:''?>" tabindex="9">
                            </div>
                        </div> 
                       </div>


                       <div class="row  text-center">
                        <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="name">Title</label>
                            <div class="col-md-9">
                                <input type="text" id="title" name="title" class="form-control" required placeholder="Title" value="<?=isset($vendordetail->title)?$vendordetail->title:''?>" tabindex="2">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="name">Country</label>
                            <div class="col-md-9">
                                <select name="country" id="country_yo" class="form-control" tabindex="10">
                                    <option value="">Select Country</option>
                                    @foreach($countrylist as $key=>$val)
                                    
                                    <option     @if(isset($vendordetail->country) && ($key == $vendordetail->country) )
                                            selected
                                            @endif value="<?=$key?>"><?=$val?></option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 
                       </div>


                       <div class="row  text-center">
                        <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="name">Company</label>
                            <div class="col-md-9">
                                <input type="text" id="comp_name" name="comp_name" class="form-control" required placeholder="Company Name" value="<?=isset($vendordetail->company)?$vendordetail->company:''?>" tabindex="3">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="name">State</label>
                            <div class="col-md-9" id="statediv">
                                <select name="state" id="state_yo" class="form-control fetch_cities" tabindex="11">
                                    <option value="">Select State</option>
                             @if(!empty($allState))       
                                    @foreach($allState as $key=>$val)
                                    
                                    <option     @if(isset($vendordetail->state) && ($val->stateID == $vendordetail->state) )
                                            selected
                                            @endif value="<?=$val->stateID?>"><?=$val->stateName?></option>
                                    @endforeach
                                    @endif
                                    </select>
                                </div>
                        </div> 
                       </div>


                       <div class="row  text-center">
                        <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="name">Store</label>
                            <div class="col-md-9">
                                <input type="text" id="store" name="store" class="form-control" required placeholder="Store Name" value="<?=isset($vendordetail->Store)?$vendordetail->Store:''?>" tabindex="4">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="name">Zip Code</label>
                            <div class="col-md-9">
                                <input type="text" id="zipcode" name="zipcode" class="form-control" required placeholder="Zip Code" value="<?=isset($vendordetail->zipcode)?$vendordetail->zipcode:''?>" tabindex="12">
                            </div>
                        </div> 
                       </div>

                       <div class="row  text-center">
                        <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="name">Email</label>
                            <div class="col-md-9">
                                <input type="email" id="email" name="email" class="form-control" required placeholder="Email" value="<?=isset($vendordetail->email)?$vendordetail->email:''?>" tabindex="5">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                           <label class="col-md-3 control-label" for="phone_number">Phone No</label>
                            <div class="col-md-9">
                                <input type="text" id="phone_number" name="phone_number" class="form-control" placeholder="Phone Number" value="<?=isset($vendordetail->phone_number)?$vendordetail->phone_number:''?>" tabindex="12">
                                @if(!empty($vendordetail))
                                    <input type="hidden" name="vendor_id" value="<?=isset($vendordetail->id)?$vendordetail->id:''?>">
                                @endif


                            </div>
                                                    </div> 
                       </div>

                       <div class="row  text-center">
                       @if(empty($vendordetail))
                         <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="password">Password</label>
                            <div class="col-md-9">
                                <input type="password" id="password" name="password" class="form-control" placeholder="******" value="" tabindex="6" required>
                            </div>
                        </div>
                        @endif

                       @if(!empty($vendordetail))
                         <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="password">Status</label>
                            <div class="col-md-9">
                                <select id="is_approved" name="is_approved" class="form-control" tabindex="6" required>
                                <option @if(isset($vendordetail->is_approved) && ($vendordetail->is_approved == '0') )
                                            selected
                                            @endif value="0">Unapproved</option> 
                                <option @if(isset($vendordetail->is_approved) && ($vendordetail->is_approved == '1') )
                                            selected
                                            @endif value="1">Approved</option> 
                                </select>
                            </div>
                        </div>
                        @endif


                         <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="password">Website</label>
                            <div class="col-md-9">
                                <input type="text" id="compweb" name="compweb" class="form-control" placeholder="Company Website" value="<?=isset($vendordetail->company)?$vendordetail->company:''?>"  required tabindex="13">
                            </div>
                        </div>
                        
                        
                       </div>


                       <div class="row  text-center">
                         @if(empty($vendordetail)):
                         <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="confirm_password">Confirm Password</label>
                            <div class="col-md-9">
                                <input type="password" id="confirm_password" name="confirm_password" class="form-control" placeholder="******" value="" tabindex="7" required>
                            </div>
                        </div>
                        @endif;

                        <div class="form-group col-md-6">
                            <label class="col-md-3 control-label" for="confirm_password">Ext</label>
                            <div class="col-md-9">
                                <input type="ext" id="ext" name="ext" class="form-control"  placeholder="Ext." value="<?=isset($vendordetail->ext)?$vendordetail->ext:''?>" required tabindex="14">
                            </div>
                        </div>                      
                                              
                       </div>

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Form Example with Blocks in the Grid -->
    </div>
    <!-- END Page Content -->
@endsection

