@extends('layouts.vendorapp')

@section('content')
@php
$current_user = Auth::user();
@endphp
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                Orders
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('vendor')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Orders</li>
    </ul>
    <!-- END Datatables Header -->
    <!-- Datatables Content -->
    <div class="block full">
        <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
        @if (Session::has('message'))
        {!! successMesaage(Session::get('message')) !!}
        @endif
        {!! validationError($errors) !!}
        <div class="table-responsive">
            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">Customer</th>
                        <th class="text-center">Order No.</th>
                        <th class="text-center">Order Date</th>
                        <!-- <th class="text-center">Status</th> -->
                        <th class="text-center">Status</th>
                        <th class="text-center">Type</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if($allRecords)
                    @foreach ($allRecords as $singleData)
                    <tr>
                        <td class="text-center">
                            {{ $singleData->customer_first_name.' '.$singleData->customer_last_name }}</td>
                        <td class="text-center">SO-IMED-{{ $singleData->id }}</td>
                        <td class="text-center">{{ displayDateWithTimeMD($singleData->ordered_on) }}</td>
                        <!-- <td class="text-center">
                            @if($singleData->status=='3')
                            <span class="label label-warning">Canceled</span>
                            @elseif($singleData->status=='2')
                            <span class="label label-success">Successfull</span>
                            @elseif($singleData->status=='1')
                            <span class="label label-danger">Failed</span>
                            @elseif($singleData->status=='0')
                            <span class="label label-info">Pending</span>
                            @endif
                        </td> -->
                        <td class="text-center">
                            @if($singleData->status=='2')
                            @if($singleData->shipping_status=='0')
                            <span class="label label-info">Received</span>
                            @elseif($singleData->shipping_status=='1')
                            <span class="label label-warning">Shipped</span>
                            @elseif($singleData->shipping_status=='2')
                            <span class="label label-danger">Voided</span>
                            @elseif($singleData->shipping_status=='3')
                            <span class="label label-danger">Processing</span>
                            @elseif($singleData->shipping_status=='4')
                            <span class="label label-success">Completed</span>
                            @endif
                            @endif
                        </td>
                        <td class="text-center">
                            @if($singleData->payment_option==1)
                            PO
                            @else
                            Sales
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{url('vendor/orders/'.$singleData->order_id)}}" data-toggle="tooltip"
                                    title="View" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                </div>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="6" class="text-center">No Record Available to display.</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
<!-- END Page Content -->
@endsection