@extends('layouts.vendorapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header row">
            <div class="col-md-4 header-section">
                <h1>
                    Enquiries
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('vendor')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Enquiries</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Product Name</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Phone</th>
                            <th class="text-center">Created At</th>  
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>                                    
                                    
                                    <td class="text-center"><a href="{!!$singleData->quoteProductUrl!!}" target="_blank" rel="noopener noreferrer"> {!!$singleData->quoteProductName!!}</a></td>
                                    <td class="text-center">  {{ $singleData->firstname }} {{$singleData->lastname}}</td>
                                    <td class="text-center">{{ $singleData->email }}</td>
                                    <!-- <td class="text-center"> <b> {{ $singleData->organization }}</b></td>                             -->
                                    <td class="text-center">{{ $singleData->phone    }}</td>
                                    <td class="text-center">{{ date('m/d/Y H:i:s',strtotime( $singleData->created_at ))      }}</td>                                                            
                                    <td class="text-center">
                                        <div class="btn-group">

                                            <a href="{{url('vendor/enquiry_detail/'.$singleData->id)}}" data-toggle="tooltip" title="View Vendor Products" ><i class="fa fa-eye" style="color:blue;font-size:24px;"></i></a>

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="3" class="text-center">No Record Available to display.</td>
                            </tr>   
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->
@endsection