@extends('layouts.vendorapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header row">
            <div class="col-md-8 header-section">
                <h1>
                    Vendor Pyament Log ( {{ucfirst($userdetails->name)}} {{$userdetails->last_name}} ) 
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Vendor Payment Log</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            <!-- <div class="block-title">
                <h2><strong>All Blogs</strong></h2>
            </div> -->
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Transaction ID</th>
                            <th class="text-center">Trans Type</th>
                            <th class="text-center">Amount</th>

                            <th class="text-center">Commission (%)</th>
                            <th class="text-center">Commission ($)</th>
                            <th class="text-center">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($allRecords)
                            @foreach ($allRecords as $singleData)
                                <tr>                                    
                                    
                                    <td class="text-center"> {{ $singleData->transaction_id }}</td> 
                                    <td class="text-center">
                                        @if($singleData->transaction_type=='1')
                                            <span class="label label-success">Wired Bank Transfer</span>
                                        @elseif($singleData->transaction_type=='2')
                                            <span class="label label-success">Check</span>
                                            @elseif($singleData->transaction_type=='3')
                                            <span class="label label-success">Cash</span>

                                        @endif                                        
                                    </td>
                                    <td>{!!  $singleData->amount !!}</td>
                                    <td>{!!  $singleData->percent_commission !!} %</td>
                                    <td>${!!  $singleData->commission !!}</td>
                                    <td>{!!  date('m/d/Y H:i:s',strtotime($singleData->added_at)) !!}</td>

                                   </tr>
                            @endforeach
                            
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    <!-- END Page Content -->


    <!-- Button trigger modal -->


<!-- Modal -->

@endsection