@extends('layouts.vendorapp')

@section('content')
<style type="text/css">
.canvasjs-chart-credit {
    display: none;
}
</style>
<div id="page-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="block full">
                <!-- <div class="block-title">
                    <h2><strong>Total</strong> Records</h2>
                </div> -->
                
                                <div class="row text-center">
                    <div class="col-sm-6 col-lg-3">
                        <a href="{{url('vendor/products')}}" class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <div class="widget-icon pull-left themed-background-default animation-fadeIn">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <h3 class="widget-content text-right animation-pullDown">
                                    <strong>{{$total_vendor_products}}</strong><br>
                                    <small>Total Products</small>
                                </h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <a href="{{url('vendor/orders')}}" class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <div class="widget-icon pull-left themed-background-default animation-fadeIn">
                                    <i class="fa fa-cubes"></i>
                                </div>
                                <h3 class="widget-content text-right animation-pullDown">
                                    <strong>{{$total_vendor_orders}}</strong><br>
                                    <small>Total Orders</small>
                                </h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <a href="{{url('vendor/enquiries')}}" class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <div class="widget-icon pull-left themed-background-default animation-fadeIn">
                                    <i class="fa fa-users"></i>
                                </div>
                                <h3 class="widget-content text-right animation-pullDown">
                                    <strong>{{$total_vendor_enquiries}}</strong>
                                    <small>Total Enquiries</small>
                                </h3>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <a href="{{url('vendor/vendorpaymentlogs')}}" class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <div class="widget-icon pull-left themed-background-default animation-fadeIn">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <h3 class="widget-content text-right animation-pullDown">
                                    <strong>{{$total_vendor_pay_count}}</strong><br>
                                    <small>Total Payments</small>
                                </h3>
                            </div>
                        </a>
                    </div>
                        
                

                </div>



                 <div class="row text-center">
                    <div class="col-sm-6 col-lg-3">
                        <a href="{{url('vendor/products')}}" class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <div class="widget-icon pull-left themed-background-default animation-fadeIn">
                                    <i class="fa fa-users"></i>
                                </div>
                                <h3 class="widget-content text-right animation-pullDown">
                                    <strong>{{$total_vendor_pending_products}}</strong>
                                    <small>Pending Products for Approval</small>
                                </h3>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div  class="row">
            <div class="col-sm-12">
                <div class="block full">
                    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                </div>
            </div>
        </div> -->
    <div class="row">
        <div class="col-sm-12">
            <div class="block full">
                <div class="block-title">
                    <h2><strong>Recent</strong> Orders</h2>
                </div>

                <div class="table-responsive">
                    <table id="example-datatable1" class="table table-vcenter table-condensed table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">Customer</th>
                                <th class="text-center">Order No.</th>
                                <th class="text-center">Order Date</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Current Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($latest_orders)
                            @foreach($latest_orders as $singleData)
                            <tr>
                                <td class="text-center">
                                    {{ $singleData->customer_first_name.' '.$singleData->customer_last_name }}</td>
                                <td class="text-center">{{ $singleData->order_number }}</td>
                                <td class="text-center">{{ displayDateWithTimeMD($singleData->ordered_on) }}</td>
                                <td class="text-center">
                                    @if($singleData->status=='3')
                                    <span class="label label-warning">Canceled</span>
                                    @elseif($singleData->status=='2')
                                    <span class="label label-success">Successfull</span>
                                    @elseif($singleData->status=='1')
                                    <span class="label label-danger">Failed</span>
                                    @elseif($singleData->status=='0')
                                    <span class="label label-info">Pending</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($singleData->status=='2')
                                        @if($singleData->shipping_status=='0')
                                        <span class="label label-info">Received</span>
                                        @elseif($singleData->shipping_status=='1')
                                        <span class="label label-warning">Shipped</span>
                                        @elseif($singleData->shipping_status=='2')
                                        <span class="label label-danger">Voided</span>
                                        @elseif($singleData->shipping_status=='3')
                                        <span class="label label-danger">Processing</span>
                                        @elseif($singleData->shipping_status=='4')
                                        <span class="label label-success">Completed</span>
                                        @endif
                                    @endif
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                         <a href="{{url('vendor/orders/'.$singleData->order_id)}}" data-toggle="tooltip"
                                    title="View" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6" class="text-center">No Record Available to display.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection