@extends('layouts.vendorapp')

@section('content')
    <!-- Page content -->
    @php
    $current_user = Auth::user();
    @endphp
    <div id="page-content">
        <!-- Datatables Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    Reports
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">            
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li>Reports</li>
        </ul>
        <!-- END Datatables Header -->
        <!-- Datatables Content -->
        <div class="block full">
            @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}   
            @endif
            {!! validationError($errors) !!}
            <div class="table-responsive1">
                <form action="{{url('vendor/reports')}}" method="post">
                    @csrf
                    <table class="table table-vcenter">
                        <tbody>
                            <tr>
                                <td>From Date</td>
                                <td>
                                    <input type="date" id="start_date" name="start_date" required class="form-control" value="{{$start_date}}" placeholder="From Date">
                                </td>
                                <td>To Date</td>
                                <td> 
                                    <input type="date" id="end_date" name="end_date" required class="form-control" value="{{$end_date}}" placeholder="To Date">
                                </td>
                                <td>
                                    <select name="export_type" id="export_type" class="form-control" required>
                                        <option value="">Report Type</option>
                                        <option value="1" @if($export_type==1) selected @endif>All Users</option>
                                        <option value="2" @if($export_type==2) selected @endif>PO Orders</option>
                                        <option value="3" @if($export_type==3) selected @endif>Sales Orders</option>
                                    </select>
                                </td>
                                <td id="orderstatusdiv" @if($export_type==1 || $export_type=='') style="display: none" @endif >
                                    <select name="export_order_status" id="export_order_status" class="form-control">
                                        <option value="">Order Status</option>
                                        <option value="-1" @if($export_order_status==-1) selected @endif>Received</option>
                                        <option value="1" @if($export_order_status==1) selected @endif>Shipped</option>
                                        <option value="2" @if($export_order_status==2) selected @endif>Voided</option>
                                        <option value="3" @if($export_order_status==3) selected @endif>Processing</option>
                                        <option value="4" @if($export_order_status==4) selected @endif>Completed</option>
                                    </select>
                                </td>
                                <td><input type="submit" name="export" value="Search" class="btn btn-success"></td>
                                <td><input type="submit" name="export" value="Export" class="btn btn-success"></td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        <!-- END Datatables Content -->
    </div>
    
    <!-- END Page Content -->
@endsection