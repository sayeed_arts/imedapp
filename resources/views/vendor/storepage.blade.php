@extends('layouts.vendorapp')

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Forms General Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                Store Page
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('vendor')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Update Store Page</li>
        
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                @if (Session::has('message'))
                {!! successMesaage(Session::get('message')) !!}
                @endif
                {!! validationError($errors) !!}
                <!-- Basic Form Elements Content -->
                <form action="{{url('vendor/updatestorepage')}}" method="post" enctype="multipart/form-data"
                    class="form-horizontal form-bordered">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="image-input">Logo</label>
                        <div class="col-md-9">
                        @if(!empty($storedata->logo))
                            <img height="10%" width="10%" src="<?=$storedata->logo?>">
                            @else
                            <!-- <img height="10%" width="10%" src="{{'frontend/assets/images/default-store-logo.jpg'}}"> -->
                        @endif
                            <input type="file" id="image-input" name="logo" class="form-control">
                            <input type="hidden" id="old_logo_image" name="old_logo_image" value="<?=$storedata->logo?>">
                            <span><b style="font-size:12px;">(Best View Logo Size Width <i style="font-size:14px; color:green"> 300px x 150px </i> Height)</b></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="additional_images">Banner Image</label>
                        <div class="col-md-9">
                         @if(!empty($storedata->banner))
                            <img height="10%" width="10%" src="<?=$storedata->banner?>">
                        @else
                        <!-- <img height="10%" width="10%" src="{{'frontend/assets/images/default-store-banner.jpg'}}"> -->
                        @endif
                            <input type="file" id="banner_image" name="banner" class="form-control">
                            <input type="hidden" id="old_banner_image" name="old_banner_image" value="<?=$storedata->banner?>">
                            <span><b style="font-size:12px;">(Best View Banner Size <i style="font-size:14px; color:green"> Width 1600px x 450px </i> Height)</b></span>
                            
                        </div>
                    </div>
                                                         
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="fb_link">Facebook URL</label>
                        <div class="col-md-9">
                            <input type="url" id="fb_link" name="fb_link" class="form-control"
                                placeholder="https://www.facebook.com" value="{{$storedata->fb_link}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="pinterest_link">Pintersest URL</label>
                        <div class="col-md-9">
                            <input type="url" id="pinterest_link" name="pinterest_link" class="form-control"
                                placeholder="https://www.linkedin.com" value="{{$storedata->pinterest_link}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="twitter_link">Twitter URL</label>
                        <div class="col-md-9">
                            <input type="url" id="twitter_link" name="twitter_link" class="form-control"
                                placeholder="https://www.twitter.com" value="{{$storedata->twitter_link}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="insta_link">Instagram URL</label>
                        <div class="col-md-9">
                            <input type="url" id="insta_link" name="insta_link" class="form-control"
                                placeholder="https://www.instagram.com" value="{{$storedata->insta_link}}">
                        </div>
                    </div>

                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i>
                                Submit</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i>
                                Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
    <!-- END Form Example with Blocks in the Grid -->
</div>
<!-- END Page Content -->
@endsection