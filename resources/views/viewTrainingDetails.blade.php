@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>{!!$jobDetails->name!!}</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('/trainings')}}">Service Trainings</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- About Start -->
    <div class="job-detail mt-5 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card-body">
                        <h2>Service Training Description</h2>
                        <p>{!! $jobDetails->content !!}</p>
                    </div>
                </div>

                <div class="col-md-4 job-details-list">
                    <ul>
                        <li>Starting Date: {!!date('m/d/Y', strtotime($jobDetails->start_date))!!}</li>
                        <li>End Date: {!!date('m/d/Y', strtotime($jobDetails->closing_date))!!}</li>
                        <li>Category: {!! $jobDetails->trainingcategory !!}</li>
                        <li>Location: {!! $jobDetails->location !!}</li>
                    </ul>

                    <a href="{{url('apply-training/'.$jobDetails->slug)}}"><button type="button" class="btn btn-sm btn-primary">Apply Now</button></a>
                        <a href="{{url('trainings')}} " class="btn">Back to listings</a>
                        
                </div>
            </div>
        </div>        
    </div>
@endsection