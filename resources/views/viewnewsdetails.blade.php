@extends('layouts.app')

@section('content')
    <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2>{!!$newsDetails->name!!}</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb theme-breadcrumb-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('/news')}}">News</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- About Start -->
    <div class="job-list mt-5 mb-5">
        <div class="container">        
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                    <div class="col-md-3">
                            <div class="news-side-block">
                                <h3>Categories</h3>                                            
                                <ul>
                                    <li style="width:100%;"><a href="{{url('/news')}}">All</a></li>
                                    @if(!empty($allCategories))
                                        @foreach($allCategories as $singleCategory)
                                            <li style="width:100%;" <?php if($newsDetails->category_id ==$singleCategory->id) { echo 'class="active"'; } ?>><a href="{{url('news-by-category/'.$singleCategory->slug)}}">{{$singleCategory->name}}</a></li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-9">
                            <div class=" mb-3">
                               <div>
                                    <div class="card-body">
                                        <div class="thumb"><img src="{{ asset($newsDetails->image) }}" alt="{{$newsDetails->name}}"></div>
                                        <p>{!! $newsDetails->content !!}</p>
                                        <!-- <a href="{{url('news')}}" class="btn">Back to listings</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection