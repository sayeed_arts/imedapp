<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('index');
});*/
Route::get('/', 'HomeController@index');

Auth::routes();


Route::get('/images', 'ImageController@getImages')->name('images');
Route::get('/upload-images', 'ImageController@uploadImages')->name('upload-images');
Route::post('/upload', 'ImageController@postUpload')->name('uploadfile');



//language Route
Route::get('lang/{locale}', 'HomeController@lang');
Route::get('currency/{code}', 'HomeController@currency');
Route::get('update-currency', 'HomeController@updateCurrency');

Route::post('/logout', 'PageController@logout')->name('logout');
Route::get('/logout', 'PageController@logout')->name('logout');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Route::get('/vendor-register', 'Auth\RegisterController@vendor_register');
Route::post('/save_vendor', 'Auth\RegisterController@save');



Route::get('/getcitiesstates/{zipcode}', 'PageController@getcitiesstates')->where(['zipcode']);
Route::get('/getstates', 'PageController@getstates');
Route::get('/getstates/{countryId}', 'PageController@getstates')->where(['countryId' => '[A-Z0-9]+']);
Route::get('/getcities/{state_id}', 'PageController@getcities')->where(['state_id' => '[0-9]+']);
Route::get('/getzipcode/{city_id}', 'PageController@getzipcode')->where(['city_id' => '[0-9]+']);

Route::post('/newsletter', 'PageController@newsletter')->name('newsletter');
Route::post('/getaquote', 'PageController@getaquote')->name('getaquote');
Route::get('/shop/{slug}/{child_slug}/{sub_child_slug}', 'ShopController@getCategory')->where(['slug' => '[a-zA-z0-9-]+','child_slug'=>'[a-zA-z0-9-]+','sub_child_slug'=>'[a-zA-z0-9-]+']);
Route::get('/shop/{slug}/{child_slug}', 'ShopController@getCategory')->where(['slug' => '[a-zA-z0-9-]+','child_slug'=>'[a-zA-z0-9-]+']);
Route::get('/shop/{slug}/', 'ShopController@getCategory')->where(['slug' => '[a-zA-z0-9-]+']);
Route::get('/shop', 'ShopController@getCategory')->name('shop');
Route::get('/storepage/{slug}/', 'ShopController@storepage')->name('test');
Route::post('/submitvendorrating', 'ShopController@submitvendorrating')->name('submitvendorrating');

Route::get('/shop/brand/{slug}', 'ShopController@getBrandProducts')->where(['slug' => '[a-zA-z0-9-]+']);

Route::get('/news/{slug}', 'PageController@getNewsDetails')->where(['slug' => '[a-zA-z0-9-]+']);
Route::get('/news', 'PageController@listNews')->name('news');
Route::post('/news', 'PageController@listNews')->name('news');
Route::get('/news-by-category/{slug}', 'PageController@listNewsByCategory')->name('news');

Route::post('/search', 'ShopController@search')->name('search');
Route::get('/search', 'ShopController@search')->name('search');
Route::get('/product/{slug}', 'ShopController@product')->where(['slug' => '[a-zA-z0-9-]+']);
Route::post('/submitproductrating', 'ShopController@submitproductrating')->name('submitproductrating');


Route::get('/cart', 'ShopController@cart')->name('cart');
//Route::get('/checkout', 'ShopController@checkout')->name('checkout');
Route::get('/checkout', 'CheckoutController@index')->name('checkout');
Route::post('/place-order', 'ShopController@placeOrder')->name('place-order');
Route::get('/order-confirmation/{order_id}','ShopController@orderConfirmation')->where(['id' => '[a-zA-Z0-9=-]+'])->name('order-confirmation');
Route::get('/pay-now/{order_id}','ShopController@payNow')->where(['id' => '[a-zA-Z0-9=-]+'])->name('pay-now');
Route::get('/notify-url', 'ShopController@notifyUrl')->name('notify-url');
Route::get('/cancel-return', 'ShopController@cancelReturn')->name('cancel-return');
Route::get('/return', 'ShopController@return')->name('return');

Route::post('/notify-url', 'ShopController@notifyUrl')->name('notify-url');
Route::post('/cancel-return', 'ShopController@cancelReturn')->name('cancel-return');
Route::post('/return', 'ShopController@return')->name('return');

Route::post('/apply-coupon', 'ShopController@applyCoupon')->name('apply-coupon');
Route::post('/add-product', 'ShopController@addProducts')->name('add-product');
Route::post('/update-product', 'ShopController@updateProducts')->name('update-product');
Route::post('/delete-product', 'ShopController@deleteProducts')->name('delete-product');

Route::get('/service-areas', 'PageController@serviceareas')->name('serviceareas');
Route::get('/service-area/{slug}', 'PageController@serviceareascitydetails')->name('serviceareascitydetails');

Route::get('/auctions', 'PageController@auctions')->name('auctions');
Route::get('/auction/{id}', 'PageController@auctiondetail')->name('auctiondetail');
Route::get('/auctiontimer/{id}', 'PageController@auctiontimer')->name('auctiontimer');
Route::get('/bidnow/{id}', 'PageController@bidnow')->name('bidnow');
Route::post('/submitbid', 'PageController@submitbid')->name('submitbid');
Route::get('/saveforbidlater/{id}', 'PageController@saveforbidlater')->name('saveforbidlater');

Route::get('/services', 'PageController@services')->name('services');
Route::get('/faq', 'PageController@faq')->name('faq');
Route::get('/projects', 'PageController@projects')->name('projects');
Route::get('/projects/{slug}', 'PageController@projectDetails')->where(['slug' => '[a-zA-z0-9-]+']);
Route::get('/services/{slug}', 'PageController@serviceDetails')->where(['slug' => '[a-zA-z0-9-]+']);
Route::get('/services/{slug}/{category}', 'PageController@serviceDetailsForm')->where(['slug' => '[a-zA-z0-9-]+','category' => '[a-zA-z0-9-]+']);
Route::get('/page/about-us', 'PageController@aboutUs')->name('about-us');
Route::get('/page/contact-us', 'PageController@contact_us')->name('contact-us');
Route::post('/post-contact-us', 'PageController@post_contact_us')->name('post-contact-us');
Route::post('/post-service', 'PageController@postService')->name('post-service');
Route::get('/page/{slug}', 'PageController@textualPage')->where(['slug' => '[a-zA-z0-9-]+']);
Route::get('/page', 'HomeController@index');

Route::get('/jobs', 'PageController@listJobs')->name('jobs');
Route::post('/jobs', 'PageController@listJobs')->name('jobs');
Route::get('/view-job/{id}', 'PageController@viewJobDetails')->name('jobdetails');
Route::get('/apply-job/{id}', 'PageController@applyJob')->name('applyjob');
Route::post('/post-apply-job', 'PageController@applyJobSubmit')->name('post-apply-job');

Route::get('/trainings', 'PageController@listTrainings')->name('trainings');
Route::post('/trainings', 'PageController@listTrainings')->name('trainings');
Route::get('/view-training/{id}', 'PageController@viewTrainingDetails')->name('trainingdetails');
Route::get('/apply-training/{id}', 'PageController@applyTraining')->name('applytraining');
Route::post('/post-apply-training', 'PageController@applyTrainingSubmit')->name('post-apply-training');

Route::get('/training-by-category/{slug}', 'PageController@listTrainigsByCategory')->name('trainings');


Route::get('/process-data', 'ImportController@processData')->name('process-data');
Route::get('/import-data', 'ImportController@dataImporter')->name('import-data');
Route::post('/import-data-upload', 'ImportController@uploadData')->name('import-data-upload');

//User Routes
Route::group(['prefix'=>'user','middleware' => 'is-front'], function () { 
    Route::get('/', 'UserController@index')->name('user');
    Route::get('/my-orders','UserController@myOrders')->name('my-orders');
    Route::get('/my-bids','UserController@myBids')->name('my-bids');
    Route::get('/order-details/{order_id}','UserController@ordersDetails')->where(['id' => '[a-zA-Z0-9=]+'])->name('order-details');
    Route::get('/submit-po/{order_id}','UserController@submitPO')->where(['id' => '[a-zA-Z0-9=]+'])->name('submit-po');    
    Route::post('/update-po', 'UserController@updateSubmitPO')->name('update-po');
    Route::get('/get-invoice/{order_id}','UserController@invoiceDetails')->where(['id' => '[a-zA-Z0-9=]+'])->name('get-invoice');
    Route::get('/address-book','UserController@addressBook')->name('address-book');
    Route::get('/address-book/{id}','UserController@addressBook')->where(['id' => '[a-zA-Z0-9=]+'])->name('edit-address');
    Route::post('/update-address','UserController@updateAddress')->name('update-address');
    Route::post('/remove-address','UserController@removeAddress')->name('remove-address');
    Route::get('/my-wishlist','UserController@myWishlist')->name('my-wishlist');
    Route::post('/remove-wishlist','UserController@removeWishlist')->name('remove-wishlist');
    Route::post('/add-wishlist','UserController@addWishlist')->name('add-wishlist');
    Route::post('/delete-wishlist','UserController@deleteWishlist')->name('delete-wishlist');
    Route::get('/change-password','UserController@changePassword')->name('change-password');
    Route::post('/update-change-password','UserController@updateChangePassword')->name('update-change-password');
    Route::get('/edit-profile','UserController@editProfile')->name('edit-profile');
    Route::post('/update-profile', 'UserController@updateprofile')->name('update-profile');
});



//Admin Panel Routes Here
Route::group(['prefix'=>'admin','middleware' => 'is-admin'], function () {
    //Admin Default Route     
    Route::get('/', ['as'=>'admin','uses'=>'Admin\AdminController@index']); 
    Route::get('/editprofile', ['as'=>'editprofile','uses'=>'Admin\AdminController@editProfile']);
    Route::post('/updateprofile', ['as'=>'updateprofile','uses'=>'Admin\AdminController@updateprofile']);
    
    //Admin Settings Route
    Route::get('/settings', ['as'=>'settings','uses'=>'Admin\AdminController@settings']);
    Route::post('/updatesettings', ['as'=>'updatesettings','uses'=>'Admin\AdminController@updatesettings']); 
    
    //Admin Slider Route
    Route::get('/slider', ['as'=>'slider','uses'=>'Admin\SliderController@index']); 
    Route::get('/slider/edit/{id}', ['as'=>'edit','uses'=>'Admin\SliderController@edit'])->where(['id' => '[0-9]+']); 

   Route::get('/vendorreview', ['as'=>'slider','uses'=>'Admin\AdminController@vendorreview']); 


    Route::get('/productreview', ['as'=>'slider','uses'=>'Admin\AdminController@productreview']); 
 
    
    
    Route::post('/slider/update', ['as'=>'update','uses'=>'Admin\SliderController@update']);    
    Route::get('/slider/add', ['as'=>'add','uses'=>'Admin\SliderController@add']);     
    Route::post('/slider/save', ['as'=>'save','uses'=>'Admin\SliderController@save']); 
    Route::post('/slider/delete/{id}', ['as'=>'delete','uses'=>'Admin\SliderController@delete']);

    Route::get('/auctions', ['as'=>'add','uses'=>'Admin\AuctionsController@index']);
    Route::get('/auctions/add', ['as'=>'add','uses'=>'Admin\AuctionsController@add']);
    Route::post('/auctions/updatestatus', ['as'=>'add','uses'=>'Admin\AuctionsController@updatestatus']);
    Route::get('/auctions/viewbids/{id}', ['as'=>'add','uses'=>'Admin\AuctionsController@viewbids']);
    Route::get('/auction/award/{id}', ['as'=>'add','uses'=>'Admin\AuctionsController@award']);
    Route::get('/auction/winners', ['as'=>'add','uses'=>'Admin\AuctionsController@winners']);
    Route::post('/bidorder/update', ['as'=>'add','uses'=>'Admin\AuctionsController@updatebidorderstatus']);


    Route::post('/updateprodreviewstatus', ['as'=>'add','uses'=>'Admin\AdminController@updateprodreviewstatus']);
   
    Route::post('/updatevenreviewstatus', ['as'=>'add','uses'=>'Admin\AdminController@updatevenreviewstatus']);
   

    Route::get('/auctions/add/{id}', ['as'=>'add','uses'=>'Admin\AuctionsController@add']);
   
    Route::get('/auctions/viewauction/{id}', ['as'=>'add','uses'=>'Admin\AuctionsController@add']);

    Route::get('/auctions/getproduct/{id}', ['as'=>'add','uses'=>'Admin\AuctionsController@getproduct']);
    Route::post('/auctions/save', ['as'=>'save','uses'=>'Admin\AuctionsController@save']);

    Route::get('/enquiries',['as'=>'add','uses'=>'Admin\EnquiryController@index']);
    Route::get('/enquiry_detail/{id}',['as'=>'add','uses'=>'Admin\EnquiryController@detail']);

    Route::post('/auctions/deleteauctions', ['as'=>'delete','uses'=>'Admin\AuctionsController@deleteauction']);


    Route::get('/vendors/viewvendor/{id}', ['as'=>'add','uses'=>'Admin\VendorController@add']);

    Route::get('/vendors/add', ['as'=>'add','uses'=>'Admin\VendorController@add']);
    Route::get('/vendors/add/{id}', ['as'=>'add','uses'=>'Admin\VendorController@add']);
    Route::post('/vendors/deletevendor', ['as'=>'delete','uses'=>'Admin\VendorController@deletevendor']);
   
    Route::get('/vendors/getcountry', ['as'=>'add','uses'=>'Admin\VendorController@getcountry']);

    Route::post('/vendors/setcommision', ['as'=>'add','uses'=>'Admin\VendorController@setcommision']);

    Route::get('/vendors/vendorpayments', ['as'=>'add','uses'=>'Admin\VendorController@vendorpayment']);


    Route::get('/vendors/vendorpaymentlogs/{id}', ['as'=>'add','uses'=>'Admin\VendorController@vendorpaymentlogs']);

  Route::post('/vendors/payvendor', ['as'=>'add','uses'=>'Admin\VendorController@payvendor']);
  
    //Admin Job Category Route
    Route::get('/jobcategories', ['as'=>'jobcategories','uses'=>'Admin\JobCategoryController@index']); 
    Route::get('/jobcategories/edit/{id}', ['as'=>'edit','uses'=>'Admin\JobCategoryController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/jobcategories/update', ['as'=>'update','uses'=>'Admin\JobCategoryController@update']);    
    Route::get('/jobcategories/add', ['as'=>'add','uses'=>'Admin\JobCategoryController@add']);     
    Route::post('/jobcategories/save', ['as'=>'save','uses'=>'Admin\JobCategoryController@save']); 
    Route::post('/jobcategories/delete/{id}', ['as'=>'delete','uses'=>'Admin\JobCategoryController@delete']);
    Route::get('/category-job/{id}', ['as'=>'category-job','uses'=>'Admin\JobCategoryController@categoryJob'])->where(['id' => '[0-9]+']); 

    //Admin Job Location Route
    Route::get('/joblocations', ['as'=>'joblocations','uses'=>'Admin\JobLocationController@index']); 
    Route::get('/joblocations/edit/{id}', ['as'=>'edit','uses'=>'Admin\JobLocationController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/joblocations/update', ['as'=>'update','uses'=>'Admin\JobLocationController@update']);    
    Route::get('/joblocations/add', ['as'=>'add','uses'=>'Admin\JobLocationController@add']);     
    Route::post('/joblocations/save', ['as'=>'save','uses'=>'Admin\JobLocationController@save']); 
    Route::post('/joblocations/delete/{id}', ['as'=>'delete','uses'=>'Admin\JobLocationController@delete']);

    //Admin Job Type Route
    Route::get('/jobtypes', ['as'=>'jobtypes','uses'=>'Admin\JobTypeController@index']); 
    Route::get('/jobtypes/edit/{id}', ['as'=>'edit','uses'=>'Admin\JobTypeController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/jobtypes/update', ['as'=>'update','uses'=>'Admin\JobTypeController@update']);    
    Route::get('/jobtypes/add', ['as'=>'add','uses'=>'Admin\JobTypeController@add']);     
    Route::post('/jobtypes/save', ['as'=>'save','uses'=>'Admin\JobTypeController@save']); 
    Route::post('/jobtypes/delete/{id}', ['as'=>'delete','uses'=>'Admin\JobTypeController@delete']);

    //Admin Jobs Route
    Route::get('/jobs', ['as'=>'jobs','uses'=>'Admin\JobController@index']); 
    Route::get('/jobs/edit/{id}', ['as'=>'edit','uses'=>'Admin\JobController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/jobs/update', ['as'=>'update','uses'=>'Admin\JobController@update']);    
    Route::get('/jobs/add', ['as'=>'add','uses'=>'Admin\JobController@add']);     
    Route::post('/jobs/save', ['as'=>'save','uses'=>'Admin\JobController@save']); 
    Route::post('/jobs/delete/{id}', ['as'=>'delete','uses'=>'Admin\JobController@delete']);

    //Admin Job Applications Route
    Route::get('/jobapplications', ['as'=>'jobs','uses'=>'Admin\JobApplicationController@index']); 
    Route::get('/jobapplications/view/{id}', ['as'=>'view','uses'=>'Admin\JobApplicationController@view'])->where(['id' => '[0-9]+']);    
    Route::post('/jobapplications/delete/{id}', ['as'=>'delete','uses'=>'Admin\JobApplicationController@delete']); 

    //Admin Service Training Category Route
    Route::get('/trainingcategories', ['as'=>'trainingcategories','uses'=>'Admin\TrainingCategoryController@index']); 
    Route::get('/trainingcategories/edit/{id}', ['as'=>'edit','uses'=>'Admin\TrainingCategoryController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/trainingcategories/update', ['as'=>'update','uses'=>'Admin\TrainingCategoryController@update']);    
    Route::get('/trainingcategories/add', ['as'=>'add','uses'=>'Admin\TrainingCategoryController@add']);     
    Route::post('/trainingcategories/save', ['as'=>'save','uses'=>'Admin\TrainingCategoryController@save']); 
    Route::post('/trainingcategories/delete/{id}', ['as'=>'delete','uses'=>'Admin\TrainingCategoryController@delete']);
    Route::get('/category-training/{id}', ['as'=>'category-training','uses'=>'Admin\TrainingCategoryController@categoryTraining'])->where(['id' => '[0-9]+']); 

    //Admin Service Trainings Route
    Route::get('/trainings', ['as'=>'trainings','uses'=>'Admin\TrainingController@index']); 
    Route::get('/trainings/edit/{id}', ['as'=>'edit','uses'=>'Admin\TrainingController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/trainings/update', ['as'=>'update','uses'=>'Admin\TrainingController@update']);    
    Route::get('/trainings/add', ['as'=>'add','uses'=>'Admin\TrainingController@add']);     
    Route::post('/trainings/save', ['as'=>'save','uses'=>'Admin\TrainingController@save']); 
    Route::post('/trainings/delete/{id}', ['as'=>'delete','uses'=>'Admin\TrainingController@delete']);

    //Admin Service Trainings Applications Route
    Route::get('/trainingapplications', ['as'=>'trainingapplications','uses'=>'Admin\TrainingApplicationController@index']); 
    Route::get('/trainingapplications/view/{id}', ['as'=>'view','uses'=>'Admin\TrainingApplicationController@view'])->where(['id' => '[0-9]+']);    
    Route::post('/trainingapplications/delete/{id}', ['as'=>'delete','uses'=>'Admin\TrainingApplicationController@delete']); 


    //Admin Category Route
    Route::get('/categories', ['as'=>'categories','uses'=>'Admin\CategoriesController@index']); 
    Route::get('/categories/edit/{id}', ['as'=>'edit','uses'=>'Admin\CategoriesController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/categories/update', ['as'=>'update','uses'=>'Admin\CategoriesController@update']);    
    Route::get('/categories/add', ['as'=>'add','uses'=>'Admin\CategoriesController@add']);     
    Route::post('/categories/save', ['as'=>'save','uses'=>'Admin\CategoriesController@save']); 
    Route::post('/categories/delete/{id}', ['as'=>'delete','uses'=>'Admin\CategoriesController@delete']);
    Route::get('/category-product/{id}', ['as'=>'category-product','uses'=>'Admin\CategoriesController@categoryProduct'])->where(['id' => '[0-9]+']); 
    
    //Admin Manufacturer Route
    Route::get('/manufacturers', ['as'=>'manufacturer','uses'=>'Admin\ManufacturerController@index']);
    Route::get('/manufacturers/edit/{id}', ['as'=>'edit','uses'=>'Admin\ManufacturerController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/manufacturers/update', ['as'=>'update','uses'=>'Admin\ManufacturerController@update']);
    Route::get('/manufacturers/add', ['as'=>'add','uses'=>'Admin\ManufacturerController@add']);     
    Route::post('/manufacturers/save', ['as'=>'save','uses'=>'Admin\ManufacturerController@save']); 
    Route::post('/manufacturers/delete/{id}', ['as'=>'delete','uses'=>'Admin\ManufacturerController@delete']);

    //Admin Products Route
    Route::get('/products', ['as'=>'manufacturer','uses'=>'Admin\ProductController@index']); 
    Route::get('/vendorproducts/{id}', ['as'=>'manufacturer','uses'=>'Admin\ProductController@index']); 
    Route::get('/products/edit/{id}', ['as'=>'edit','uses'=>'Admin\ProductController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/products/update', ['as'=>'update','uses'=>'Admin\ProductController@update']);
    Route::get('/products/add', ['as'=>'add','uses'=>'Admin\ProductController@add']);     
    Route::post('/products/save', ['as'=>'save','uses'=>'Admin\ProductController@save']); 
    Route::post('/products/delete/{id}', ['as'=>'delete','uses'=>'Admin\ProductController@delete']);
    Route::get('/products/getsubcat/{id}', 'Admin\ProductController@getSubCat')->where(['id' => '[0-9]+']);
    Route::get('/products/getproducts/{id}', 'Admin\ProductController@getProducts')->where(['id' => '[0-9]+']);
    Route::get('/products/getsubcat', 'Admin\ProductController@getSubCat');

    Route::get('/products/copy', ['as'=>'copy','uses'=>'Admin\ProductController@copyProducts']);   
    Route::post('/products/copy-save', ['as'=>'copysave','uses'=>'Admin\ProductController@copySave']); 

    //Admin User Route
    Route::get('/users', ['as'=>'users','uses'=>'Admin\UsersController@index']); 
    Route::get('/vendors', ['as'=>'users','uses'=>'Admin\VendorController@index']); 
   
    Route::get('/users/edit/{id}', ['as'=>'edit','uses'=>'Admin\UsersController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/users/update', ['as'=>'update','uses'=>'Admin\UsersController@update']);    
    Route::get('/users/add', ['as'=>'add','uses'=>'Admin\UsersController@add']);     
    Route::post('/users/save', ['as'=>'save','uses'=>'Admin\UsersController@save']); 
    Route::post('/vendors/save', ['as'=>'save','uses'=>'Admin\VendorController@save']); 
    Route::post('/users/delete/{id}', ['as'=>'delete','uses'=>'Admin\UsersController@delete']);
    
    //Admin User Route
    Route::get('/agents', ['as'=>'agents','uses'=>'Admin\AgentsController@index']); 
    Route::get('/agents/edit/{id}', ['as'=>'edit','uses'=>'Admin\AgentsController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/agents/update', ['as'=>'update','uses'=>'Admin\AgentsController@update']);    
    Route::get('/agents/add', ['as'=>'add','uses'=>'Admin\AgentsController@add']);     
    Route::post('/agents/save', ['as'=>'save','uses'=>'Admin\AgentsController@save']); 
    Route::post('/agents/delete/{id}', ['as'=>'delete','uses'=>'Admin\AgentsController@delete']);

    //Admin Media Images Route
    Route::get('/media', ['as'=>'media','uses'=>'Admin\mediaController@index']); 
    Route::get('/media/edit/{id}', ['as'=>'edit','uses'=>'Admin\mediaController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/media/update', ['as'=>'update','uses'=>'Admin\mediaController@update']);
    Route::get('/media/add', ['as'=>'add','uses'=>'Admin\mediaController@add']);     
    Route::post('/media/save', ['as'=>'save','uses'=>'Admin\mediaController@save']); 
    Route::post('/media/delete/{id}', ['as'=>'delete','uses'=>'Admin\mediaController@delete']);
    
    //Admin Pages Route
    Route::get('/pages', ['as'=>'pages','uses'=>'Admin\PageController@index']); 
    Route::get('/pages/edit/{id}', ['as'=>'edit','uses'=>'Admin\PageController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/pages/update', ['as'=>'update','uses'=>'Admin\PageController@update']);    
    Route::get('/pages/add', ['as'=>'add','uses'=>'Admin\PageController@add']);     
    Route::post('/pages/save', ['as'=>'save','uses'=>'Admin\PageController@save']); 
    Route::post('/pages/delete/{id}', ['as'=>'save','uses'=>'Admin\PageController@delete'])->where(['id' => '[0-9]+']);      
    
    //Admin Newsletter Route
    Route::get('/newsletter', ['as'=>'newsletter','uses'=>'Admin\NewsletterController@index']); 
    Route::get('/newsletter/edit/{id}', ['as'=>'edit','uses'=>'Admin\NewsletterController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/newsletter/update', ['as'=>'update','uses'=>'Admin\NewsletterController@update']);    
    Route::post('/newsletter/delete/{id}', ['as'=>'delete','uses'=>'Admin\NewsletterController@delete']);

    //Admin Blog Route
    Route::get('/blog', ['as'=>'blogs','uses'=>'Admin\BlogController@index']); 
    Route::get('/blog/edit/{id}', ['as'=>'edit','uses'=>'Admin\BlogController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/blog/update', ['as'=>'update','uses'=>'Admin\BlogController@update']);    
    Route::get('/blog/add', ['as'=>'add','uses'=>'Admin\BlogController@add']);     
    Route::post('/blog/save', ['as'=>'save','uses'=>'Admin\BlogController@save']); 
    Route::post('/blog/delete/{id}', ['as'=>'delete','uses'=>'Admin\BlogController@delete']);

    //Admin Blog Categories Route
    Route::get('/blogcategories', ['as'=>'blogcategories','uses'=>'Admin\BlogCategoryController@index']); 
    Route::get('/blogcategories/edit/{id}', ['as'=>'edit','uses'=>'Admin\BlogCategoryController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/blogcategories/update', ['as'=>'update','uses'=>'Admin\BlogCategoryController@update']);    
    Route::get('/blogcategories/add', ['as'=>'add','uses'=>'Admin\BlogCategoryController@add']);     
    Route::post('/blogcategories/save', ['as'=>'save','uses'=>'Admin\BlogCategoryController@save']); 
    Route::post('/blogcategories/delete/{id}', ['as'=>'delete','uses'=>'Admin\BlogCategoryController@delete']);
            
    //Admin FAQ Route
    /*Route::get('/faq', ['as'=>'faqs','uses'=>'Admin\FaqController@index']); 
    Route::get('/faq/edit/{id}', ['as'=>'edit','uses'=>'Admin\FaqController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/faq/update', ['as'=>'update','uses'=>'Admin\FaqController@update']);    
    Route::get('/faq/add', ['as'=>'add','uses'=>'Admin\FaqController@add']);     
    Route::post('/faq/save', ['as'=>'save','uses'=>'Admin\FaqController@save']); 
    Route::post('/faq/delete/{id}', ['as'=>'delete','uses'=>'Admin\FaqController@delete']);*/

    //Admin Testimonials Route
    Route::get('/testimonial', ['as'=>'testimonials','uses'=>'Admin\TestimonialController@index']); 
    Route::get('/testimonial/edit/{id}', ['as'=>'edit','uses'=>'Admin\TestimonialController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/testimonial/update', ['as'=>'update','uses'=>'Admin\TestimonialController@update']);
    Route::get('/testimonial/add', ['as'=>'add','uses'=>'Admin\TestimonialController@add']);     
    Route::post('/testimonial/save', ['as'=>'save','uses'=>'Admin\TestimonialController@save']); 
    Route::post('/testimonial/delete/{id}', ['as'=>'delete','uses'=>'Admin\TestimonialController@delete']);

    //Admin Mailing List Route
    /*Route::get('/mailinglist', ['as'=>'mailinglist','uses'=>'Admin\NewsletterController@mailingList']); 
    Route::get('/mailinglist/add', ['as'=>'add','uses'=>'Admin\NewsletterController@addMailingList']);     
    Route::post('/mailinglist/save', ['as'=>'save','uses'=>'Admin\NewsletterController@saveMailingList']);
    Route::get('/mailinglist/edit/{id}', ['as'=>'edit','uses'=>'Admin\NewsletterController@editMailingList'])->where(['id' => '[0-9]+']);     
    Route::post('/mailinglist/update', ['as'=>'update','uses'=>'Admin\NewsletterController@updateMailingList']);    
    Route::post('/mailinglist/delete/{id}', ['as'=>'delete','uses'=>'Admin\NewsletterController@deleteMailingList']);
    
    //Admin Mailing List Records Route
    Route::get('/listrecords/{list_id}', ['as'=>'mailinglist','uses'=>'Admin\NewsletterController@mailingListRecords'])->where(['list_id' => '[0-9]+']); 
    Route::get('/listrecords/add/{list_id}', ['as'=>'add','uses'=>'Admin\NewsletterController@addMailingListRecords'])->where(['list_id' => '[0-9]+']);     
    Route::post('/listrecords/save', ['as'=>'save','uses'=>'Admin\NewsletterController@saveMailingListRecords']);
    Route::get('/listrecords/edit/{id}', ['as'=>'edit','uses'=>'Admin\NewsletterController@editMailingListRecords'])->where(['id' => '[0-9]+']);     
    Route::post('/listrecords/update', ['as'=>'update','uses'=>'Admin\NewsletterController@updateMailingListRecords']);    
    Route::post('/listrecords/delete/{id}', ['as'=>'delete','uses'=>'Admin\NewsletterController@deleteMailingListRecords']);
    Route::post('/listrecords/import', ['as'=>'import','uses'=>'Admin\NewsletterController@importMailingListRecords']);
    Route::get('/sendemail', ['as'=>'sendemail','uses'=>'Admin\NewsletterController@sendEmail']); 
    Route::post('/sendemail', ['as'=>'sendemail','uses'=>'Admin\NewsletterController@sendEmailPost']); */ 
    //Admin Advertisement Route
    Route::get('/advertisements', ['as'=>'advertisements','uses'=>'Admin\AdvertisementController@index']); 
    Route::get('/advertisements/edit/{id}', ['as'=>'edit','uses'=>'Admin\AdvertisementController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/advertisements/update', ['as'=>'update','uses'=>'Admin\AdvertisementController@update']);
    Route::get('/advertisements/add', ['as'=>'add','uses'=>'Admin\AdvertisementController@add']);     
    Route::post('/advertisements/save', ['as'=>'save','uses'=>'Admin\AdvertisementController@save']); 
    Route::post('/advertisements/delete/{id}', ['as'=>'delete','uses'=>'Admin\AdvertisementController@delete']);

    //Admin Service Areas Route
    Route::get('/serviceareas', ['as'=>'serviceareas','uses'=>'Admin\ServiceAreasController@index']); 
    Route::get('/serviceareas/edit/{id}', ['as'=>'edit','uses'=>'Admin\ServiceAreasController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/serviceareas/update', ['as'=>'update','uses'=>'Admin\ServiceAreasController@update']);
    Route::get('/serviceareas/add', ['as'=>'add','uses'=>'Admin\ServiceAreasController@add']);     
    Route::post('/serviceareas/save', ['as'=>'save','uses'=>'Admin\ServiceAreasController@save']); 
    Route::post('/serviceareas/delete/{id}', ['as'=>'delete','uses'=>'Admin\ServiceAreasController@delete']);

    //Admin Services Route
    Route::get('/services', ['as'=>'services','uses'=>'Admin\ServicesController@index']); 
    Route::get('/services/edit/{id}', ['as'=>'edit','uses'=>'Admin\ServicesController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/services/update', ['as'=>'update','uses'=>'Admin\ServicesController@update']);
    Route::get('/services/add', ['as'=>'add','uses'=>'Admin\ServicesController@add']);     
    Route::post('/services/save', ['as'=>'save','uses'=>'Admin\ServicesController@save']); 
    Route::post('/services/delete/{id}', ['as'=>'delete','uses'=>'Admin\ServicesController@delete']);

    //Admin Service Category Route
    Route::get('/service-category', ['as'=>'services','uses'=>'Admin\ServiceCategoryController@index']); 
    Route::get('/service-category/edit/{id}', ['as'=>'edit','uses'=>'Admin\ServiceCategoryController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/service-category/update', ['as'=>'update','uses'=>'Admin\ServiceCategoryController@update']);
    Route::get('/service-category/add', ['as'=>'add','uses'=>'Admin\ServiceCategoryController@add']);    
    Route::post('/service-category/save', ['as'=>'save','uses'=>'Admin\ServiceCategoryController@save']);
    Route::post('/service-category/delete/{id}', ['as'=>'delete','uses'=>'Admin\ServiceCategoryController@delete']);
    
    //Admin Services Route
    Route::get('/services-request', ['as'=>'services-request','uses'=>'Admin\ServicesController@viewRequests']); 
    Route::get('/services-request/edit/{id}', ['as'=>'edit','uses'=>'Admin\ServicesController@editRequests'])->where(['id' => '[0-9]+']);     
    //Route::post('/services-request/update', ['as'=>'update','uses'=>'Admin\ServicesController@update']);
    Route::post('/services-request/delete/{id}', ['as'=>'delete','uses'=>'Admin\ServicesController@deleteRequests']);
    
    //Admin Projects Route
    Route::get('/projects', ['as'=>'projects','uses'=>'Admin\ProjectsController@index']); 
    Route::get('/projects/edit/{id}', ['as'=>'edit','uses'=>'Admin\ProjectsController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/projects/update', ['as'=>'update','uses'=>'Admin\ProjectsController@update']);
    Route::get('/projects/add', ['as'=>'add','uses'=>'Admin\ProjectsController@add']);     
    Route::post('/projects/save', ['as'=>'save','uses'=>'Admin\ProjectsController@save']); 
    Route::post('/projects/delete/{id}', ['as'=>'delete','uses'=>'Admin\ProjectsController@delete']);
    
    Route::get('/orders', ['as'=>'orders','uses'=>'Admin\OrdersController@index']);
    Route::get('/orders/{order_id}', ['uses'=>'Admin\OrdersController@view'])->where(['order_id' => '[a-zA-Z0-9-]+']);
    Route::get('/get-invoice/{order_id}', ['uses'=>'Admin\OrdersController@getInvoice'])->where(['order_id' => '[a-zA-Z0-9-]+']);
    Route::get('/packing-slip/{order_id}', ['uses'=>'Admin\OrdersController@packingSlip'])->where(['order_id' => '[a-zA-Z0-9-]+']);
    Route::post('/order-shipping-status','Admin\OrdersController@shippingStatus')->name('order-shipping-status');
    Route::post('/admin-update-po','Admin\OrdersController@updatePO')->name('admin-update-po');

    //Admin Projects Route
    Route::get('/coupons', ['as'=>'coupons','uses'=>'Admin\CouponsController@index']); 
    Route::get('/coupons/edit/{id}', ['as'=>'edit','uses'=>'Admin\CouponsController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/coupons/update', ['as'=>'update','uses'=>'Admin\CouponsController@update']);
    Route::get('/coupons/add', ['as'=>'add','uses'=>'Admin\CouponsController@add']);     
    Route::post('/coupons/save', ['as'=>'save','uses'=>'Admin\CouponsController@save']); 
    Route::post('/coupons/delete/{id}', ['as'=>'delete','uses'=>'Admin\CouponsController@delete']);

    //Admin Attribute Sets Route
    Route::get('/attribute-sets', ['as'=>'attribute-sets','uses'=>'Admin\AttributeSetsController@index']); 
    Route::get('/attribute-sets/edit/{id}', ['as'=>'edit','uses'=>'Admin\AttributeSetsController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/attribute-sets/update', ['as'=>'update','uses'=>'Admin\AttributeSetsController@update']);
    Route::get('/attribute-sets/add', ['as'=>'add','uses'=>'Admin\AttributeSetsController@add']);     
    Route::post('/attribute-sets/save', ['as'=>'save','uses'=>'Admin\AttributeSetsController@save']); 
    Route::post('/attribute-sets/delete/{id}', ['as'=>'delete','uses'=>'Admin\AttributeSetsController@delete']);
    
    //Admin Attributes Route
    Route::get('/attributes', ['as'=>'attributes','uses'=>'Admin\AttributesController@index']); 
    Route::get('/attributes/edit/{id}', ['as'=>'edit','uses'=>'Admin\AttributesController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/attributes/update', ['as'=>'update','uses'=>'Admin\AttributesController@update']);
    Route::get('/attributes/add', ['as'=>'add','uses'=>'Admin\AttributesController@add']);     
    Route::post('/attributes/save', ['as'=>'save','uses'=>'Admin\AttributesController@save']); 
    Route::post('/attributes/delete/{id}', ['as'=>'delete','uses'=>'Admin\AttributesController@delete']);

    //Admin Currency Route
    Route::get('/currency', ['as'=>'currency','uses'=>'Admin\CurrencyController@index']); 
    Route::get('/currency/edit/{id}', ['as'=>'edit','uses'=>'Admin\CurrencyController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/currency/update', ['as'=>'update','uses'=>'Admin\CurrencyController@update']);
    Route::get('/currency/add', ['as'=>'add','uses'=>'Admin\CurrencyController@add']);     
    Route::post('/currency/save', ['as'=>'save','uses'=>'Admin\CurrencyController@save']); 
    Route::post('/currency/delete/{id}', ['as'=>'delete','uses'=>'Admin\CurrencyController@delete']);

    //Admin Languages Route
    Route::get('/alllanguages', ['as'=>'alllanguages','uses'=>'Admin\LanguageTranslationController@allLanguages']); 
    Route::get('/languages/edit/{id}', ['as'=>'edit','uses'=>'Admin\LanguageTranslationController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/languages/update', ['as'=>'update','uses'=>'Admin\LanguageTranslationController@update']);
    Route::get('/languages/add', ['as'=>'add','uses'=>'Admin\LanguageTranslationController@add']);     
    Route::post('/languages/save', ['as'=>'save','uses'=>'Admin\LanguageTranslationController@save']); 
    Route::post('/languages/delete/{id}', ['as'=>'delete','uses'=>'Admin\LanguageTranslationController@delete']);

    //Admin Language Route
    Route::get('languages', 'Admin\LanguageTranslationController@index')->name('languages');
    Route::post('translations/update', 'Admin\LanguageTranslationController@transUpdate')->name('translation.update.json');
    Route::post('translations/updateKey', 'Admin\LanguageTranslationController@transUpdateKey')->name('translation.update.json.key');
    Route::delete('translations/destroy/{key}', 'Admin\LanguageTranslationController@destroy')->name('translations.destroy');
    Route::post('translations/create', 'Admin\LanguageTranslationController@store')->name('translations.create');

    Route::get('/reports', ['as'=>'reports','uses'=>'Admin\AdminController@reports']);
    Route::post('/reports', ['as'=>'reports','uses'=>'Admin\AdminController@reports']);
});

//Vendor Panel Routes Here Starts


Route::group(['prefix'=>'vendor','middleware' => 'is-vendor'], function () {
    //Admin Default Route     
    Route::get('/', ['as'=>'vendor','uses'=>'Vendor\VendorController@index']);

    Route::get('/vendorreview', ['as'=>'vendorreview','uses'=>'Vendor\VendorController@vendorreview']); 

     Route::get('/productreview', ['as'=>'productreview','uses'=>'Vendor\VendorController@productreview']); 
 
    Route::post('/updateprodreviewstatus', ['as'=>'add','uses'=>'Vendor\VendorController@updateprodreviewstatus']);
   
    Route::post('/updatevenreviewstatus', ['as'=>'add','uses'=>'Vendor\VendorController@updatevenreviewstatus']);
   

    Route::get('/products', ['as'=>'manufacturer','uses'=>'Vendor\ProductController@index']); 

    Route::get('/enquiries',['as'=>'add','uses'=>'Vendor\EnquiryController@index']);
    Route::get('/products/edit/{id}', ['as'=>'edit','uses'=>'Vendor\ProductController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/products/update', ['as'=>'update','uses'=>'Vendor\ProductController@update']);
    Route::get('/products/add', ['as'=>'add','uses'=>'Vendor\ProductController@add']);     
    Route::post('/products/save', ['as'=>'save','uses'=>'Vendor\ProductController@save']); 
    Route::post('/products/delete/{id}', ['as'=>'delete','uses'=>'Vendor\ProductController@delete']);
    Route::get('/storepage', ['as'=>'vendor','uses'=>'Vendor\VendorController@storepage']);
    Route::post('/updatestorepage', ['as'=>'save','uses'=>'Vendor\VendorController@updatestorepage']);   

    Route::get('/products/getsubcat/{id}', 'Vendor\ProductController@getSubCat')->where(['id' => '[0-9]+']);
    Route::get('/products/getsubcat', 'vendor\ProductController@getSubCat');
    Route::get('/orders', ['as'=>'orders','uses'=>'Vendor\OrdersController@index']);
    Route::get('/orders/{order_id}', ['uses'=>'Vendor\OrdersController@view'])->where(['order_id' => '[a-zA-Z0-9-]+']);
    Route::post('/vendor-order-shipping-status','Vendor\OrdersController@shippingStatus')->name('vendor-order-shipping-status');
    Route::get('/get-invoice/{order_id}', ['uses'=>'Vendor\OrdersController@getInvoice'])->where(['order_id' => '[a-zA-Z0-9-]+']);
    Route::get('/packing-slip/{order_id}', ['uses'=>'Admin\OrdersController@packingSlip'])->where(['order_id' => '[a-zA-Z0-9-]+']);
    
    Route::get('/manufacturers', ['as'=>'manufacturer','uses'=>'Vendor\ManufacturerController@index']);
    Route::get('/manufacturers/edit/{id}', ['as'=>'edit','uses'=>'Vendor\ManufacturerController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/manufacturers/update', ['as'=>'update','uses'=>'Vendor\ManufacturerController@update']);
    Route::get('/manufacturers/add', ['as'=>'add','uses'=>'Vendor\ManufacturerController@add']);     
    Route::post('/manufacturers/save', ['as'=>'save','uses'=>'Vendor\ManufacturerController@save']); 
    Route::post('/manufacturers/delete/{id}', ['as'=>'delete','uses'=>'Vendor\ManufacturerController@delete']);
    Route::get('/vendorpaymentlogs', ['as'=>'add','uses'=>'Vendor\VendorController@vendorpaymentlogs']);


    Route::get('/editprofile', ['as'=>'add','uses'=>'Vendor\VendorController@add']);

    Route::post('/updatevendorprofile', ['as'=>'save','uses'=>'Vendor\VendorController@save']); 

    Route::get('/reports', ['as'=>'reports','uses'=>'Vendor\VendorController@reports']);
    Route::post('/reports', ['as'=>'reports','uses'=>'Vendor\VendorController@reports']);

     Route::get('/enquiry_detail/{id}',['as'=>'add','uses'=>'Vendor\EnquiryController@detail']);

     //Vendor Service Trainings Route
    Route::get('/trainings', ['as'=>'trainings','uses'=>'Vendor\TrainingController@index']); 
    Route::get('/trainings/edit/{id}', ['as'=>'edit','uses'=>'Vendor\TrainingController@edit'])->where(['id' => '[0-9]+']);     
    Route::post('/trainings/update', ['as'=>'update','uses'=>'Vendor\TrainingController@update']);    
    Route::get('/trainings/add', ['as'=>'add','uses'=>'Vendor\TrainingController@add']);     
    Route::post('/trainings/save', ['as'=>'save','uses'=>'Vendor\TrainingController@save']); 
    Route::post('/trainings/delete/{id}', ['as'=>'delete','uses'=>'Vendor\TrainingController@delete']);

    //Vendor Service Trainings Applications Route
    Route::get('/trainingapplications', ['as'=>'trainingapplications','uses'=>'Vendor\TrainingApplicationController@index']); 
    Route::get('/trainingapplications/view/{id}', ['as'=>'view','uses'=>'Vendor\TrainingApplicationController@view'])->where(['id' => '[0-9]+']);    
    Route::post('/trainingapplications/delete/{id}', ['as'=>'delete','uses'=>'Vendor\TrainingApplicationController@delete']); 

    Route::get('/auctions', ['as'=>'auctions','uses'=>'Vendor\AuctionsController@index']);
    Route::get('/auctions/viewbids/{id}', ['as'=>'add','uses'=>'Vendor\AuctionsController@viewbids']);
});

//Vendor Panel Routes Here Ends
