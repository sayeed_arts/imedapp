-- --------------------------------------------------------

--
-- Table structure for table `trainingapplications`
--

DROP TABLE IF EXISTS `trainingapplications`;
CREATE TABLE IF NOT EXISTS `trainingapplications` (
  `id` int NOT NULL AUTO_INCREMENT,
  `training_id` int DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `trainingapplications`
--

INSERT INTO `trainingapplications` (`id`, `training_id`, `fullname`, `email`, `phone`, `created_at`, `updated_at`) VALUES
(2, 1, 'Adam James1', 'adamjames@test.com', '9876543210', '2021-07-11 06:27:59', '2021-07-11 06:27:59'),
(4, 16, 'Adam James', 'adam@james.com', '3434234234', '2021-08-08 10:37:02', '2021-08-08 10:37:02');

-- --------------------------------------------------------

--
-- Table structure for table `trainingcategories`
--

DROP TABLE IF EXISTS `trainingcategories`;
CREATE TABLE IF NOT EXISTS `trainingcategories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `display_status` int DEFAULT '1',
  `display_order` float(10,2) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_key` text,
  `meta_desc` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `trainingcategories`
--

INSERT INTO `trainingcategories` (`id`, `slug`, `name`, `content`, `display_status`, `display_order`, `meta_title`, `meta_key`, `meta_desc`, `created_at`, `updated_at`) VALUES
(1, 'training-category-1-a', 'Training Category 1 A', '<p>Training Category 1 A</p>', 1, 1.00, 'Training Category 1 A', 'Training Category 1 A', 'Training Category 1 A', '2021-07-10 04:05:40', '2021-08-08 08:31:39'),
(4, 'aaaaaaaaaa', 'aaaaaaaaaa', '<p>aaaaaaa</p>', 1, 1.00, 'aaaaaaaaaaaaa', 'aaaaaaaaaaaaaaa', 'aaaaaaaaaa', '2021-08-08 08:29:26', '2021-08-08 08:29:26');

-- --------------------------------------------------------

--
-- Table structure for table `trainingcategories_translations`
--

DROP TABLE IF EXISTS `trainingcategories_translations`;
CREATE TABLE IF NOT EXISTS `trainingcategories_translations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_id` int DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `trainingcategories_translations`
--

INSERT INTO `trainingcategories_translations` (`id`, `category_id`, `language`, `name`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 'fr', 'thtghyt', '<p>utyu</p>', '2021-07-10 04:12:38', '2021-08-08 08:31:39');

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

DROP TABLE IF EXISTS `trainings`;
CREATE TABLE IF NOT EXISTS `trainings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `closing_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `category_id` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `location` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `vendor_id` int NOT NULL,
  `display_status` int DEFAULT '1',
  `display_order` float(10,2) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_key` text,
  `meta_desc` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trainings`
--

INSERT INTO `trainings` (`id`, `slug`, `name`, `content`, `start_date`, `category_id`, `location`, `vendor_id`, `display_status`, `display_order`, `meta_title`, `meta_key`, `meta_desc`, `created_at`, `updated_at`) VALUES
(1, 'sales-manager1', 'Sales Manager', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2021-08-07 18:30:00', '1', 'Alford FL', 41, 1, 1.00, NULL, NULL, NULL, '2021-07-11 01:05:44', '2021-08-08 10:08:55'),
(5, 'area-manager', 'Area Manager', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2021-08-08 14:03:25', '1,3', '1', 0, 1, 1.00, NULL, NULL, NULL, '2021-07-11 01:05:44', '2021-07-11 02:41:26'),
(6, 'project-manager', 'Project Manager', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2021-08-08 14:03:25', '1', '2', 0, 1, 1.00, NULL, NULL, NULL, '2021-07-11 01:05:44', '2021-07-11 02:41:14'),
(7, 'assistant-manager', 'Assistant Manager', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2021-08-08 14:03:25', '1,3', '1', 0, 1, 1.00, NULL, NULL, NULL, '2021-07-11 01:05:44', '2021-07-11 02:41:26'),
(8, 'sales-executive', 'Sales Executive', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2021-08-08 14:03:25', '1', '2', 0, 1, 1.00, NULL, NULL, NULL, '2021-07-11 01:05:44', '2021-07-11 02:41:14'),
(9, 'module-manager', 'Module Manager', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2021-08-08 14:03:25', '1,3', '1', 0, 1, 1.00, NULL, NULL, NULL, '2021-07-11 01:05:44', '2021-07-11 02:41:26'),
(10, 'associate-anager', 'Associate Manager', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2021-08-08 14:03:25', '1', '2', 0, 1, 1.00, NULL, NULL, NULL, '2021-07-11 01:05:44', '2021-07-11 02:41:14'),
(11, 'store-manager', 'Store Manager', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2021-08-08 14:03:25', '1,3', '1', 0, 1, 1.00, NULL, NULL, NULL, '2021-07-11 01:05:44', '2021-07-11 02:41:26'),
(12, 'store-keeper', 'Store Keeper', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2021-08-08 14:03:25', '1', '2', 0, 1, 1.00, NULL, NULL, NULL, '2021-07-11 01:05:44', '2021-07-11 02:41:14'),
(15, 'it-manager', 'IT Manager', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2021-08-08 14:03:25', '1,3', '1', 0, 1, 1.00, NULL, NULL, NULL, '2021-07-11 01:05:44', '2021-07-11 02:41:26'),
(16, 'v1', 'V1', '<p>sdffsdf</p>', '2021-08-18 18:30:00', '1,4', 'Alford FL', 41, 1, 2.00, NULL, NULL, NULL, '2021-08-08 09:16:39', '2021-08-08 10:01:27'),
(18, 'v2', 'V2', '<p>V2</p>', '2021-08-09 18:30:00', '4', 'dfg dfgdfg', 41, 1, 2.00, NULL, NULL, NULL, '2021-08-08 10:04:08', '2021-08-08 10:04:08');
COMMIT;
