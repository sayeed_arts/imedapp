-- --------------------------------------------------------

--
-- Table structure for table `serviceareas`
--

DROP TABLE IF EXISTS `serviceareas`;
CREATE TABLE IF NOT EXISTS `serviceareas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cityname` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci,
  `slug` varchar(255) DEFAULT NULL,
  `content` longtext,
  `image` varchar(255) DEFAULT NULL,
  `country` varchar(52) DEFAULT NULL,
  `state` int DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_key` mediumtext,
  `meta_desc` mediumtext,
  `display_status` int NOT NULL DEFAULT '1',
  `display_order` float(10,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `serviceareas`
--

INSERT INTO `serviceareas` (`id`, `cityname`, `slug`, `content`, `image`, `country`, `state`, `meta_title`, `meta_key`, `meta_desc`, `display_status`, `display_order`, `created_at`, `updated_at`) VALUES
(1, 'Abbeville', 'abbeville', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, 'USA', 176, 'Abbeville', 'Abbeville', 'Abbeville', 1, 1.00, '2021-08-08 10:51:35', '2021-08-08 12:03:34'),
(2, 'Adamsville', 'adamsville', '<p>fdsfsdf</p>', NULL, 'USA', 176, 'dgdfg', 'dfgdfg', 'dfgd', 1, 2.00, '2021-08-08 11:25:33', '2021-08-08 11:25:33'),
(3, 'Addison', 'addison', '<p>vdgfdg</p>', NULL, 'USA', 176, 'dfgdfgd', 'fgdfg', 'gfgdfgd', 1, 3.00, '2021-08-08 11:26:02', '2021-08-08 11:26:02'),
(4, 'Akron', 'akron', '<p>sfsdfsdf</p>', NULL, 'USA', 176, 'sdfs', 'sdfsd', 'fsdfsd', 1, 2.00, '2021-08-08 11:26:26', '2021-08-08 11:26:26'),
(5, 'Anderson', 'anderson', '<p>zdfsd</p>', NULL, 'USA', 1061, 'fsdfs', 'dfsdf', 'sdfsfsd', 1, 1.00, '2021-08-08 11:27:02', '2021-08-08 11:27:02'),
(6, 'Dillingham', 'dillingham', '<p>adasfsdf</p>', NULL, 'USA', 1061, 'sdfsdf', 'sdfsfsd', 'fsdfsdf', 1, 2.00, '2021-08-08 11:27:37', '2021-08-08 11:27:37');

-- --------------------------------------------------------

--
-- Table structure for table `serviceareas_translations`
--

DROP TABLE IF EXISTS `serviceareas_translations`;
CREATE TABLE IF NOT EXISTS `serviceareas_translations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `serviceareas_id` int DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `cityname` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci,
  `content` longtext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
COMMIT;
